/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.products;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "merchants")
@Entity
public class MoneywaveMerchant implements Serializable {

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the apiKey
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * @param apiKey the apiKey to set
     */
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * @return the secret
     */
    public String getSecret() {
        return secret;
    }

    /**
     * @param secret the secret to set
     */
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the deletedAt
     */
    public Date getDeletedAt() {
        return deletedAt;
    }

    /**
     * @param deletedAt the deletedAt to set
     */
    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * @return the environment
     */
    public String getEnvironment() {
        return environment;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    /**
     * @return the liveApproved
     */
    public boolean isLiveApproved() {
        return liveApproved;
    }

    /**
     * @param liveApproved the liveApproved to set
     */
    public void setLiveApproved(boolean liveApproved) {
        this.liveApproved = liveApproved;
    }

    /**
     * @return the bvn
     */
    public String getBvn() {
        return bvn;
    }

    /**
     * @param bvn the bvn to set
     */
    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the cacDocumentId
     */
    public String getCacDocumentId() {
        return cacDocumentId;
    }

    /**
     * @param cacDocumentId the cacDocumentId to set
     */
    public void setCacDocumentId(String cacDocumentId) {
        this.cacDocumentId = cacDocumentId;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(unique = true)
    private String name;
    private String accountNumber;
    private String apiKey;
    private String secret;
    @Column(name = "isActive")
    private boolean active;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    private String environment;
    @Column(name = "can_golive")
    private boolean liveApproved;
    private String bvn;
    private String country;
    @Column(name = "cac_document_id")
    private String cacDocumentId;
   
    
    public GenericMerchant getGeneric(){
        
        GenericMerchant genericMerchant = new GenericMerchant();
        genericMerchant.setCountry(country);
        genericMerchant.setId(id);
        genericMerchant.setName(name);
        genericMerchant.setCreatedAt(createdAt);
        genericMerchant.setUpdatedAt(updatedAt);
        genericMerchant.setDeletedAt(deletedAt);
        genericMerchant.setParent(null);
        genericMerchant.setLiveApproved(liveApproved);
        genericMerchant.setLive(environment == null ? false : "live".equalsIgnoreCase(environment));
        genericMerchant.setActive(isActive());
        genericMerchant.setSecret(secret);
//        genericMerchant.setPhone();
        
        return genericMerchant;
    }

    @Override
    public boolean equals(Object obj) {
        
        if(obj == null)
            return true;
        
        if(! (obj instanceof MoneywaveMerchant))
            return false;
        
        MoneywaveMerchant m = (MoneywaveMerchant) obj;
        
        return m.getId() == this.getId();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }
    
    
}

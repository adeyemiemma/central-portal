/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.Utility;
import java.util.Date;


/**
 *
 * @author emmanueladeyemi
 */
public class PosMerchantModel {

    /**
     * @return the feeCap
     */
    public double getFeeCap() {
        return feeCap;
    }

    /**
     * @param feeCap the feeCap to set
     */
    public void setFeeCap(double feeCap) {
        this.feeCap = feeCap;
    }

    /**
     * @return the feeType
     */
    public Utility.FeeType getFeeType() {
        return feeType;
    }

    /**
     * @param feeType the feeType to set
     */
    public void setFeeType(Utility.FeeType feeType) {
        this.feeType = feeType;
    }

    /**
     * @return the flatFee
     */
    public double getFlatFee() {
        return flatFee;
    }

    /**
     * @param flatFee the flatFee to set
     */
    public void setFlatFee(double flatFee) {
        this.flatFee = flatFee;
    }

    /**
     * @return the percentageFee
     */
    public double getPercentageFee() {
        return percentageFee;
    }

    /**
     * @param percentageFee the percentageFee to set
     */
    public void setPercentageFee(double percentageFee) {
        this.percentageFee = percentageFee;
    }

    /**
     * @return the relationshipMan
     */
    public String getRelationshipMan() {
        return relationshipMan;
    }

    /**
     * @param relationshipMan the relationshipMan to set
     */
    public void setRelationshipMan(String relationshipMan) {
        this.relationshipMan = relationshipMan;
    }

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * @param accountName the accountName to set
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * @return the parentMerchantCode
     */
    public String getParentMerchantCode() {
        return parentMerchantCode;
    }

    /**
     * @param parentMerchantCode the parentMerchantCode to set
     */
    public void setParentMerchantCode(String parentMerchantCode) {
        this.parentMerchantCode = parentMerchantCode;
    }

    /**
     * @return the posId
     */
    public String getPosId() {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(String posId) {
        this.posId = posId;
    }

    /**
     * @return the parentName
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * @param parentName the parentName to set
     */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
    
    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    private long id;
    private String name;
    private String merchantId; // This is the parent that own the merchant 
    private String merchantCode;
    private Date createdOn;
    private String phone;
    private String email;
    private Date modifiedOn;
    private String address;
    private String createdBy;
    private String parentName;
    private String parentMerchantCode;
    private String posId;
    private Utility.FeeType feeType;
    private double flatFee;
    private double percentageFee;
    private String relationshipMan;
    private String accountNo;
    private String bankName;
    private String accountName;
    private double feeCap;
    
    public static PosMerchantModel from(PosMerchant posMerchant){
        
        PosMerchantModel merchantModel = new PosMerchantModel();
        merchantModel.setAddress(posMerchant.getAddress());
        merchantModel.setCreatedBy(posMerchant.getCreatedBy());
        merchantModel.setCreatedOn(posMerchant.getCreatedOn());
        merchantModel.setEmail(posMerchant.getEmail());
        merchantModel.setMerchantCode(posMerchant.getMerchantCode());
        merchantModel.setName(posMerchant.getName());
        merchantModel.setPhone(posMerchant.getPhone());
        merchantModel.setId(posMerchant.getId());
        merchantModel.setMerchantId(posMerchant.getMerchantId());
        merchantModel.setModifiedOn(posMerchant.getModifiedOn());
        merchantModel.setPosId(posMerchant.getPosId());
        
        if(posMerchant.getParent() != null){
            merchantModel.setParentMerchantCode(posMerchant.getParent().getMerchantCode());
            merchantModel.setParentName(posMerchant.getParent().getName());
        }
        
        merchantModel.setAccountName(posMerchant.getAccountName());
        merchantModel.setAccountNo(posMerchant.getAccountNo());
        merchantModel.setRelationshipMan(posMerchant.getRelationshipMan());
        merchantModel.setFeeType(posMerchant.getFeeType());
        merchantModel.setFlatFee(posMerchant.getFlatFee());
        merchantModel.setPercentageFee(posMerchant.getPercentageFee());
        merchantModel.setBankName(posMerchant.getBankName());
        merchantModel.setFeeCap(posMerchant.getFeeCap());
        
        return merchantModel;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "pos_merchant_product_settlement")
@Entity
@NamedQueries(value = {
    @NamedQuery(name = "PosMerchantProductSettlement.findByMerchantCodeAndProductPrefix", query = "select pmp from PosMerchantProductSettlement pmp where pmp.merchantCode = :merchantCode and pmp.productPrefix = :productPrefix ")
})
public class PosMerchantProductSettlement implements Serializable {

    /**
     * @return the parentCommission
     */
    public BigDecimal getParentCommission() {
        return parentCommission;
    }

    /**
     * @param parentCommission the parentCommission to set
     */
    public void setParentCommission(BigDecimal parentCommission) {
        this.parentCommission = parentCommission;
    }

    /**
     * @return the productPrefix
     */
    public String getProductPrefix() {
        return productPrefix;
    }

    /**
     * @param productPrefix the productPrefix to set
     */
    public void setProductPrefix(String productPrefix) {
        this.productPrefix = productPrefix;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the subAccount
     */
    public String getSubAccount() {
        return subAccount;
    }

    /**
     * @param subAccount the subAccount to set
     */
    public void setSubAccount(String subAccount) {
        this.subAccount = subAccount;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String productPrefix;
    private String merchantCode;
    private String merchantId;
    private String merchantName;
    private String subAccount;
    private BigDecimal parentCommission;
    
}

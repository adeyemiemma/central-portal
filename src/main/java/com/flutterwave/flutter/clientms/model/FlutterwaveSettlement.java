/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "flutterwave_settlement")
@Entity
public class FlutterwaveSettlement implements Serializable {

    /**
     * @return the ruleState
     */
    public Utility.SettlementState getRuleState() {
        return ruleState;
    }

    /**
     * @param ruleState the ruleState to set
     */
    public void setRuleState(Utility.SettlementState ruleState) {
        this.ruleState = ruleState;
    }

    /**
     * @return the transactionState
     */
    public Utility.SettlementState getTransactionState() {
        return transactionState;
    }

    /**
     * @param transactionState the transactionState to set
     */
    public void setTransactionState(Utility.SettlementState transactionState) {
        this.transactionState = transactionState;
    }

    /**
     * @return the settlement
     */
    public Settlement getSettlement() {
        return settlement;
    }

    /**
     * @param settlement the settlement to set
     */
    public void setSettlement(Settlement settlement) {
        this.settlement = settlement;
    }

    /**
     * @return the internalReference
     */
    public String getInternalReference() {
        return internalReference;
    }

    /**
     * @param internalReference the internalReference to set
     */
    public void setInternalReference(String internalReference) {
        this.internalReference = internalReference;
    }

    /**
     * @return the transactionCreationTrial
     */
    public int getTransactionCreationTrial() {
        return transactionCreationTrial;
    }

    /**
     * @param transactionCreationTrial the transactionCreationTrial to set
     */
    public void setTransactionCreationTrial(int transactionCreationTrial) {
        this.transactionCreationTrial = transactionCreationTrial;
    }

    /**
     * @return the ruleCreationTrial
     */
    public int getRuleCreationTrial() {
        return ruleCreationTrial;
    }

    /**
     * @param ruleCreationTrial the ruleCreationTrial to set
     */
    public void setRuleCreationTrial(int ruleCreationTrial) {
        this.ruleCreationTrial = ruleCreationTrial;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the ruleCreatedOn
     */
    public Date getRuleCreatedOn() {
        return ruleCreatedOn;
    }

    /**
     * @param ruleCreatedOn the ruleCreatedOn to set
     */
    public void setRuleCreatedOn(Date ruleCreatedOn) {
        this.ruleCreatedOn = ruleCreatedOn;
    }

    /**
     * @return the transactionCreatedOn
     */
    public Date getTransactionCreatedOn() {
        return transactionCreatedOn;
    }

    /**
     * @param transactionCreatedOn the transactionCreatedOn to set
     */
    public void setTransactionCreatedOn(Date transactionCreatedOn) {
        this.transactionCreatedOn = transactionCreatedOn;
    }

    /**
     * @return the ruleStatus
     */
    public boolean isRuleStatus() {
        return ruleStatus;
    }

    /**
     * @param ruleStatus the ruleStatus to set
     */
    public void setRuleStatus(boolean ruleStatus) {
        this.ruleStatus = ruleStatus;
    }

    /**
     * @return the transactionStatus
     */
    public boolean isTransactionStatus() {
        return transactionStatus;
    }

    /**
     * @param transactionStatus the transactionStatus to set
     */
    public void setTransactionStatus(boolean transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the debitAccountNo
     */
    public String getDebitAccountNo() {
        return debitAccountNo;
    }

    /**
     * @param debitAccountNo the debitAccountNo to set
     */
    public void setDebitAccountNo(String debitAccountNo) {
        this.debitAccountNo = debitAccountNo;
    }

    /**
     * @return the debitBankCode
     */
    public String getDebitBankCode() {
        return debitBankCode;
    }

    /**
     * @param debitBankCode the debitBankCode to set
     */
    public void setDebitBankCode(String debitBankCode) {
        this.debitBankCode = debitBankCode;
    }

    /**
     * @return the debitCurrency
     */
    public String getDebitCurrency() {
        return debitCurrency;
    }

    /**
     * @param debitCurrency the debitCurrency to set
     */
    public void setDebitCurrency(String debitCurrency) {
        this.debitCurrency = debitCurrency;
    }

    /**
     * @return the debitCountry
     */
    public String getDebitCountry() {
        return debitCountry;
    }

    /**
     * @param debitCountry the debitCountry to set
     */
    public void setDebitCountry(String debitCountry) {
        this.debitCountry = debitCountry;
    }

    /**
     * @return the creditAccountNo
     */
    public String getCreditAccountNo() {
        return creditAccountNo;
    }

    /**
     * @param creditAccountNo the creditAccountNo to set
     */
    public void setCreditAccountNo(String creditAccountNo) {
        this.creditAccountNo = creditAccountNo;
    }

    /**
     * @return the creditBankCode
     */
    public String getCreditBankCode() {
        return creditBankCode;
    }

    /**
     * @param creditBankCode the creditBankCode to set
     */
    public void setCreditBankCode(String creditBankCode) {
        this.creditBankCode = creditBankCode;
    }

    /**
     * @return the creditCurrency
     */
    public String getCreditCurrency() {
        return creditCurrency;
    }

    /**
     * @param creditCurrency the creditCurrency to set
     */
    public void setCreditCurrency(String creditCurrency) {
        this.creditCurrency = creditCurrency;
    }

    /**
     * @return the creditCountry
     */
    public String getCreditCountry() {
        return creditCountry;
    }

    /**
     * @param creditCountry the creditCountry to set
     */
    public void setCreditCountry(String creditCountry) {
        this.creditCountry = creditCountry;
    }

    /**
     * @return the settlementToken
     */
    public String getSettlementToken() {
        return settlementToken;
    }

    /**
     * @param settlementToken the settlementToken to set
     */
    public void setSettlementToken(String settlementToken) {
        this.settlementToken = settlementToken;
    }

    /**
     * @return the transactionReference
     */
    public String getTransactionReference() {
        return transactionReference;
    }

    /**
     * @param transactionReference the transactionReference to set
     */
    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String debitAccountNo;
    private String debitBankCode;
    private String debitCurrency;
    private String debitCountry;
    private String creditAccountNo;
    private String creditBankCode;
    private String creditCurrency;
    private String creditCountry;
    private String settlementToken;
    private String transactionReference;
    private String narration;
    private String currency;
    private double amount;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    private String merchantId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date ruleCreatedOn;
    private int ruleCreationTrial;
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionCreatedOn;
    private boolean ruleStatus;
    private Utility.SettlementState ruleState;
    private boolean transactionStatus;
    private Utility.SettlementState transactionState;
    private int transactionCreationTrial;
    private String internalReference;
    @ManyToOne
    private Settlement settlement;
}

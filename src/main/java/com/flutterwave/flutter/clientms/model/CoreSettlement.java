/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(catalog = "settletransaction")
@Entity
public class CoreSettlement implements Serializable {

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the merchantid
     */
    public String getMerchantid() {
        return merchantid;
    }

    /**
     * @param merchantid the merchantid to set
     */
    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the flwreference
     */
    public String getFlwreference() {
        return flwreference;
    }

    /**
     * @param flwreference the flwreference to set
     */
    public void setFlwreference(String flwreference) {
        this.flwreference = flwreference;
    }

    /**
     * @return the merchantreference
     */
    public String getMerchantreference() {
        return merchantreference;
    }

    /**
     * @param merchantreference the merchantreference to set
     */
    public void setMerchantreference(String merchantreference) {
        this.merchantreference = merchantreference;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the transactiontype
     */
    public String getTransactiontype() {
        return transactiontype;
    }

    /**
     * @param transactiontype the transactiontype to set
     */
    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    /**
     * @return the authmodel
     */
    public String getAuthmodel() {
        return authmodel;
    }

    /**
     * @param authmodel the authmodel to set
     */
    public void setAuthmodel(String authmodel) {
        this.authmodel = authmodel;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the mask
     */
    public String getMask() {
        return mask;
    }

    /**
     * @param mask the mask to set
     */
    public void setMask(String mask) {
        this.mask = mask;
    }

    /**
     * @return the responsecode
     */
    public String getResponsecode() {
        return responsecode;
    }

    /**
     * @param responsecode the responsecode to set
     */
    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    /**
     * @return the responsemessage
     */
    public String getResponsemessage() {
        return responsemessage;
    }

    /**
     * @param responsemessage the responsemessage to set
     */
    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    /**
     * @return the transactiontime
     */
    public Date getTransactiontime() {
        return transactiontime;
    }

    /**
     * @param transactiontime the transactiontime to set
     */
    public void setTransactiontime(Date transactiontime) {
        this.transactiontime = transactiontime;
    }

    /**
     * @return the cardscheme
     */
    public String getCardscheme() {
        return cardscheme;
    }

    /**
     * @param cardscheme the cardscheme to set
     */
    public void setCardscheme(String cardscheme) {
        this.cardscheme = cardscheme;
    }

    /**
     * @return the merchantname
     */
    public String getMerchantname() {
        return merchantname;
    }

    /**
     * @param merchantname the merchantname to set
     */
    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    /**
     * @return the locale
     */
    public String getLocale() {
        return locale;
    }

    /**
     * @param locale the locale to set
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String merchantid;
    private BigDecimal amount;
    private String currency;
    private String flwreference;
    private String merchantreference;
    private String rrn;
    private String provider;
    private String transactiontype;
    private String authmodel;
    private String narration;
    private String country;
    private String mask;
    private String responsecode;
    private String responsemessage;
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactiontime;
    private String cardscheme;
    private String merchantname;
    private String locale;
}

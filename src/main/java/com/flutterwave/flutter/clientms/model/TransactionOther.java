/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author emmanueladeyemi
 */
public class TransactionOther {

    /**
     * @return the dialectCSCResultCode
     */
    public String getDialectCSCResultCode() {
        return dialectCSCResultCode;
    }

    /**
     * @param dialectCSCResultCode the dialectCSCResultCode to set
     */
    public void setDialectCSCResultCode(String dialectCSCResultCode) {
        this.dialectCSCResultCode = dialectCSCResultCode;
    }

    /**
     * @return the transactionNumber
     */
    public String getTransactionNumber() {
        return transactionNumber;
    }

    /**
     * @param transactionNumber the transactionNumber to set
     */
    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the orderReference
     */
    public String getOrderReference() {
        return orderReference;
    }

    /**
     * @param orderReference the orderReference to set
     */
    public void setOrderReference(String orderReference) {
        this.orderReference = orderReference;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the merchantTransactionReference
     */
    public String getMerchantTransactionReference() {
        return merchantTransactionReference;
    }

    /**
     * @param merchantTransactionReference the merchantTransactionReference to set
     */
    public void setMerchantTransactionReference(String merchantTransactionReference) {
        this.merchantTransactionReference = merchantTransactionReference;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the acquireId
     */
    public String getAcquireId() {
        return acquireId;
    }

    /**
     * @param acquireId the acquireId to set
     */
    public void setAcquireId(String acquireId) {
        this.acquireId = acquireId;
    }

    /**
     * @return the bankMerchantId
     */
    public String getBankMerchantId() {
        return bankMerchantId;
    }

    /**
     * @param bankMerchantId the bankMerchantId to set
     */
    public void setBankMerchantId(String bankMerchantId) {
        this.bankMerchantId = bankMerchantId;
    }

    /**
     * @return the batchNumber
     */
    public String getBatchNumber() {
        return batchNumber;
    }

    /**
     * @param batchNumber the batchNumber to set
     */
    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * @param currencyCode the currencyCode to set
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the acquirerResponseCode
     */
    public String getAcquirerResponseCode() {
        return acquirerResponseCode;
    }

    /**
     * @param acquirerResponseCode the acquirerResponseCode to set
     */
    public void setAcquirerResponseCode(String acquirerResponseCode) {
        this.acquirerResponseCode = acquirerResponseCode;
    }

    /**
     * @return the operatorId
     */
    public String getOperatorId() {
        return operatorId;
    }

    /**
     * @param operatorId the operatorId to set
     */
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * @return the merchantTransactionSource
     */
    public String getMerchantTransactionSource() {
        return merchantTransactionSource;
    }

    /**
     * @param merchantTransactionSource the merchantTransactionSource to set
     */
    public void setMerchantTransactionSource(String merchantTransactionSource) {
        this.merchantTransactionSource = merchantTransactionSource;
    }

    /**
     * @return the orderDate
     */
    public Date getOrderDate() {
        return orderDate;
    }

    /**
     * @param orderDate the orderDate to set
     */
    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType the cardType to set
     */
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * @return the cardExpiryMonth
     */
    public int getCardExpiryMonth() {
        return cardExpiryMonth;
    }

    /**
     * @param cardExpiryMonth the cardExpiryMonth to set
     */
    public void setCardExpiryMonth(int cardExpiryMonth) {
        this.cardExpiryMonth = cardExpiryMonth;
    }

    /**
     * @return the cardExpiryYear
     */
    public int getCardExpiryYear() {
        return cardExpiryYear;
    }

    /**
     * @param cardExpiryYear the cardExpiryYear to set
     */
    public void setCardExpiryYear(int cardExpiryYear) {
        this.cardExpiryYear = cardExpiryYear;
    }
    
    private String transactionNumber;
    private String transactionId;
    private Date date;
    private String merchantId;
    private String orderReference;
    private String orderId;
    private String merchantTransactionReference;
    private String transactionType;
    private String acquireId;
    private String bankMerchantId;
    private String batchNumber;
    private String currency;
    private String currencyCode;
    private double amount;
    private String rrn;
    private String responseCode;
    private String acquirerResponseCode;
    private String operatorId;
    private String merchantTransactionSource;
    private Date orderDate;
    private String cardType;
    private String pan;
    private int cardExpiryMonth;
    private int cardExpiryYear;
    private String dialectCSCResultCode;
    
    public String toJsonString(){
        
        try {
            ObjectMapper mapper = new ObjectMapper();
            String data = mapper.writeValueAsString(this);
            
            return data;
        } catch (JsonProcessingException ex) {
            Logger.getLogger(TransactionOther.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public Transaction getTransaction(String provider){
        
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setRrn(rrn);
        transaction.setDatetime(TransactionUtility.formatDateToStringWithTime(date));
        transaction.setCardMask(pan);
        transaction.setCreatedOn(new Date());
        transaction.setFlwTxnReference(transactionNumber);
        transaction.setMerchantId(merchantId);
        transaction.setMerchantTxnReference(merchantTransactionReference);
        transaction.setProvider(provider);
        transaction.setProcessorReference(orderId);
        transaction.setResponseCode(TransactionUtility.getResponseCode(responseCode, true));
        transaction.setResponseMessage(TransactionUtility.getResponseCode(responseCode, false));
        transaction.setRrn(rrn);
        transaction.setTransactionCurrency(currencyCode);
        transaction.setCardScheme(TransactionUtility.getSchemeCode(cardType));
        transaction.setCardMask(pan);
        transaction.setTransactionType(transactionType);
        transaction.setTransactionNarration(orderReference);
        transaction.setTransactionData(toJsonString());
        
        return transaction;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public class GenericTransaction {

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the merchantReference
     */
    public String getMerchantReference() {
        return merchantReference;
    }

    /**
     * @param merchantReference the merchantReference to set
     */
    public void setMerchantReference(String merchantReference) {
        this.merchantReference = merchantReference;
    }

    /**
     * @return the createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the chargeResponseCode
     */
    public String getChargeResponseCode() {
        return chargeResponseCode;
    }

    /**
     * @param chargeResponseCode the chargeResponseCode to set
     */
    public void setChargeResponseCode(String chargeResponseCode) {
        this.chargeResponseCode = chargeResponseCode;
    }

    /**
     * @return the vbvResponseMessage
     */
    public String getVbvResponseMessage() {
        return vbvResponseMessage;
    }

    /**
     * @param vbvResponseMessage the vbvResponseMessage to set
     */
    public void setVbvResponseMessage(String vbvResponseMessage) {
        this.vbvResponseMessage = vbvResponseMessage;
    }

    /**
     * @return the vbvResponseCode
     */
    public String getVbvResponseCode() {
        return vbvResponseCode;
    }

    /**
     * @param vbvResponseCode the vbvResponseCode to set
     */
    public void setVbvResponseCode(String vbvResponseCode) {
        this.vbvResponseCode = vbvResponseCode;
    }

    /**
     * @return the cardCountry
     */
    public String getCardCountry() {
        return cardCountry;
    }

    /**
     * @param cardCountry the cardCountry to set
     */
    public void setCardCountry(String cardCountry) {
        this.cardCountry = cardCountry;
    }

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the chargeResponseMessage
     */
    public String getChargeResponseMessage() {
        return chargeResponseMessage;
    }

    /**
     * @param chargeResponseMessage the chargeResponseMessage to set
     */
    public void setChargeResponseMessage(String chargeResponseMessage) {
        this.chargeResponseMessage = chargeResponseMessage;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the flutterReference
     */
    public String getFlutterReference() {
        return flutterReference;
    }

    /**
     * @param flutterReference the flutterReference to set
     */
    public void setFlutterReference(String flutterReference) {
        this.flutterReference = flutterReference;
    }

    /**
     * @return the transactionReference
     */
    public String getTransactionReference() {
        return transactionReference;
    }

    /**
     * @param transactionReference the transactionReference to set
     */
    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the deletedAt
     */
    public Date getDeletedAt() {
        return deletedAt;
    }

    /**
     * @param deletedAt the deletedAt to set
     */
    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * @return the linkingReference
     */
    public String getLinkingReference() {
        return linkingReference;
    }

    /**
     * @param linkingReference the linkingReference to set
     */
    public void setLinkingReference(String linkingReference) {
        this.linkingReference = linkingReference;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the fee
     */
    public Double getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(Double fee) {
        this.fee = fee;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }
    
    private long id;
    private Double amount;
    private String flutterReference;
    private String transactionReference;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;
    private String linkingReference;
    private Product product;
    private String productName;
    private String merchant;
    private String currency;
    private Double fee;
    private String status;
    private String transactionType;
    private String source;
    private String chargeResponseMessage;
    private String chargeResponseCode;
    private String vbvResponseMessage;
    private String vbvResponseCode;
    private String cardCountry;
    private String accountNo;
    private String createdOn;
    private String merchantReference;
    private String statusCode;
}

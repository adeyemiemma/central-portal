/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.recon;

import java.util.Date;
import javax.persistence.Temporal;

/**
 *
 * @author emmanueladeyemi
 */
public class PdfExportModel {

    /**
     * @return the age
     */
    public long getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(long age) {
        this.age = age;
    }

    /**
     * @return the currency
     */
    public int getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(int currency) {
        this.currency = currency;
    }

    /**
     * @return the merchantTitle
     */
    public String getMerchantTitle() {
        return merchantTitle;
    }

    /**
     * @param merchantTitle the merchantTitle to set
     */
    public void setMerchantTitle(String merchantTitle) {
        this.merchantTitle = merchantTitle;
    }

    /**
     * @return the approvalCode
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * @param approvalCode the approvalCode to set
     */
    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the externalResponseCode
     */
    public String getExternalResponseCode() {
        return externalResponseCode;
    }

    /**
     * @param externalResponseCode the externalResponseCode to set
     */
    public void setExternalResponseCode(String externalResponseCode) {
        this.externalResponseCode = externalResponseCode;
    }

    /**
     * @return the acquiredId
     */
    public String getAcquiredId() {
        return acquiredId;
    }

    /**
     * @param acquiredId the acquiredId to set
     */
    public void setAcquiredId(String acquiredId) {
        this.acquiredId = acquiredId;
    }

    /**
     * @return the aid
     */
    public String getAid() {
        return aid;
    }

    /**
     * @param aid the aid to set
     */
    public void setAid(String aid) {
        this.aid = aid;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the batchId
     */
    public long getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(long batchId) {
        this.batchId = batchId;
    }
    /**
     * @return the operationDate
     */
    public Date getOperationDate() {
        return operationDate;
    }

    /**
     * @param operationDate the operationDate to set
     */
    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the maskedPan
     */
    public String getMaskedPan() {
        return maskedPan;
    }

    /**
     * @param maskedPan the maskedPan to set
     */
    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }

    /**
     * @return the terminalName
     */
    public String getTerminalName() {
        return terminalName;
    }

    /**
     * @param terminalName the terminalName to set
     */
    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    /**
     * @return the cardAcceptorId
     */
    public String getCardAcceptorId() {
        return cardAcceptorId;
    }

    /**
     * @param cardAcceptorId the cardAcceptorId to set
     */
    public void setCardAcceptorId(String cardAcceptorId) {
        this.cardAcceptorId = cardAcceptorId;
    }
    
    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return the cardScheme
     */
    public String getCardScheme() {
        return cardScheme;
    }

    /**
     * @param cardScheme the cardScheme to set
     */
    public void setCardScheme(String cardScheme) {
        this.cardScheme = cardScheme;
    }

    /**
     * @return the settled
     */
    public boolean isSettled() {
        return settled;
    }

    /**
     * @param settled the settled to set
     */
    public void setSettled(boolean settled) {
        this.settled = settled;
    }

    /**
     * @return the settlementDate
     */
    public Date getSettlementDate() {
        return settlementDate;
    }

    /**
     * @param settlementDate the settlementDate to set
     */
    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }
    
    private long batchId;
    private Date operationDate;
    private String rrn;
    private int currency ;  
    private String maskedPan;
    private String terminalName;
    private String cardAcceptorId;
    private String merchantTitle;
    private String approvalCode;
    private String responseCode;
    private String externalResponseCode;    
    private String acquiredId;
    private String aid;
    private double amount;
    private String bank;
    private String cardScheme;
    private boolean settled;
    private Date settlementDate;
    private long age;
    
}

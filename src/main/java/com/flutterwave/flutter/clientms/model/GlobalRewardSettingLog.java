/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.EnumUtil;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "global_reward_setting_log")
@Entity
public class GlobalRewardSettingLog implements Serializable {

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the modifiedBy
     */
    public User getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the type
     */
    public EnumUtil.TransactionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EnumUtil.TransactionType type) {
        this.type = type;
    }

    /**
     * @return the weekDay
     */
    public EnumUtil.WeekDay getWeekDay() {
        return weekDay;
    }

    /**
     * @param weekDay the weekDay to set
     */
    public void setWeekDay(EnumUtil.WeekDay weekDay) {
        this.weekDay = weekDay;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the status
     */
    public EnumUtil.Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(EnumUtil.Status status) {
        this.status = status;
    }

    /**
     * @return the point
     */
    public double getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(double point) {
        this.point = point;
    }

    /**
     * @return the unit
     */
    public int getUnit() {
        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(int unit) {
        this.unit = unit;
    }
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Enumerated(EnumType.STRING)
    private EnumUtil.TransactionType type;
    @Enumerated(EnumType.STRING)
    private EnumUtil.WeekDay weekDay;
    private String currency;
    @Enumerated(EnumType.STRING)
    private EnumUtil.Status status;
    private double point;
    private int unit = 1;
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;
    @ManyToOne
    private User createdBy;
    @ManyToOne
    private User modifiedBy;
    
    public static GlobalRewardSettingLog from(GlobalRewardSetting rewardSetting){
        
        GlobalRewardSettingLog log = new GlobalRewardSettingLog();
        log.setCreatedBy(rewardSetting.getCreatedBy());
        log.setCreatedOn(rewardSetting.getCreatedOn());
        log.setCurrency(rewardSetting.getCurrency());
        log.setModifiedBy(rewardSetting.getModifiedBy());
        log.setModifiedOn(rewardSetting.getModifiedOn());
        log.setPoint(rewardSetting.getPoint());
        log.setStatus(rewardSetting.getStatus());
        log.setType(rewardSetting.getType());
        log.setUnit(rewardSetting.getUnit());
        log.setWeekDay(rewardSetting.getWeekDay());
        
        return log;
    }
    
}

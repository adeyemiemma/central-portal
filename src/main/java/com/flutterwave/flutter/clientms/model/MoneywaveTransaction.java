/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.model.products.MoneywaveMerchant;
import com.flutterwave.flutter.clientms.util.MoneywaveCurrency;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "transactions")
@Entity
public class MoneywaveTransaction implements Serializable {

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the systemType
     */
    public String getSystemType() {
        return systemType;
    }

    /**
     * @param systemType the systemType to set
     */
    public void setSystemType(String systemType) {
        this.systemType = systemType;
    }

    /**
     * @return the sourceId
     */
    public Long getSourceId() {
        return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * @return the destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * @param destination the destination to set
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * @return the destinationId
     */
    public Integer getDestinationId() {
        return destinationId;
    }

    /**
     * @param destinationId the destinationId to set
     */
    public void setDestinationId(Integer destinationId) {
        this.destinationId = destinationId;
    }

    /**
     * @return the flutterResponseMessage
     */
    public String getFlutterResponseMessage() {
        return flutterResponseMessage;
    }

    /**
     * @param flutterResponseMessage the flutterResponseMessage to set
     */
    public void setFlutterResponseMessage(String flutterResponseMessage) {
        this.flutterResponseMessage = flutterResponseMessage;
    }

    /**
     * @return the flutterResponseCode
     */
    public String getFlutterResponseCode() {
        return flutterResponseCode;
    }

    /**
     * @param flutterResponseCode the flutterResponseCode to set
     */
    public void setFlutterResponseCode(String flutterResponseCode) {
        this.flutterResponseCode = flutterResponseCode;
    }

    /**
     * @return the flutterReference
     */
    public String getFlutterReference() {
        return flutterReference;
    }

    /**
     * @param flutterReference the flutterReference to set
     */
    public void setFlutterReference(String flutterReference) {
        this.flutterReference = flutterReference;
    }

    /**
     * @return the meta
     */
    public String getMeta() {
        return meta;
    }

    /**
     * @param meta the meta to set
     */
    public void setMeta(String meta) {
        this.meta = meta;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the deletedAt
     */
    public Date getDeletedAt() {
        return deletedAt;
    }

    /**
     * @param deletedAt the deletedAt to set
     */
    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * @return the merchant
     */
    public MoneywaveMerchant getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(MoneywaveMerchant merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the currency
     */
    public MoneywaveCurrency getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(MoneywaveCurrency currency) {
        this.currency = currency;
    }

    /**
     * @return the fee
     */
    public Double getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(Double fee) {
        this.fee = fee;
    }

    /**
     * @return the ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * @param ref the ref to set
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

    /**
     * @return the linkingReference
     */
    public String getLinkingReference() {
        return linkingReference;
    }

    /**
     * @param linkingReference the linkingReference to set
     */
    public void setLinkingReference(String linkingReference) {
        this.linkingReference = linkingReference;
    }

//    /**
//     * @return the batchId
//     */
//    public String getBatchId() {
//        return batchId;
//    }
//
//    /**
//     * @param batchId the batchId to set
//     */
//    public void setBatchId(String batchId) {
//        this.batchId = batchId;
//    }

    /**
     * @return the disburseOrderId
     */
    public String getDisburseOrderId() {
        return disburseOrderId;
    }

    /**
     * @param disburseOrderId the disburseOrderId to set
     */
    public void setDisburseOrderId(String disburseOrderId) {
        this.disburseOrderId = disburseOrderId;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Double amount;
    
    private String status;
    @Column(name = "system_type")
    private String systemType;
    private String source;
    @Column(name = "source_id")
    private Long sourceId;
    @Column(name = "dest")
    private String destination;
    @Column(name = "dest_id")
    private Integer destinationId;
    private String flutterResponseMessage;
    private String flutterResponseCode;
    private String flutterReference;
    @Column(columnDefinition = "text")
    private String meta;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @JoinColumn(name = "merchantId")
    @ManyToOne
    private MoneywaveMerchant merchant;
    @JoinColumn(name = "currencyId")
    @ManyToOne
    private MoneywaveCurrency currency;
    private Double fee;
    @Column(name = "\"ref\"")
    private String ref;
    private String linkingReference;
//    private String batchId;
    private String disburseOrderId;
    
    public GenericTransaction getGeneric(){
        
        GenericTransaction genericTransaction = new GenericTransaction();
        genericTransaction.setAmount(amount);
        genericTransaction.setCreatedAt(createdAt);
        genericTransaction.setDeletedAt(deletedAt);
        genericTransaction.setFee(fee);
        genericTransaction.setFlutterReference(flutterReference);
        genericTransaction.setId(id);
        genericTransaction.setLinkingReference(linkingReference);
        genericTransaction.setMerchant(merchant.getName());
        genericTransaction.setSource(source);
        genericTransaction.setStatus(status);
        genericTransaction.setTransactionType(systemType);
        genericTransaction.setTransactionReference(ref);
        genericTransaction.setUpdatedAt(updatedAt);
        genericTransaction.setCurrency(currency.getShortCode());
        
        return genericTransaction;
    }
    
}

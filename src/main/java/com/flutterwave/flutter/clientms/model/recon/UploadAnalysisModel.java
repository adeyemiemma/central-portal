/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.recon;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author adeyemi
 */
public class UploadAnalysisModel {

    /**
     * @return the dates
     */
    public List<Date> getDates() {
        return dates;
    }

    /**
     * @param dates the dates to set
     */
    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return the cardScheme
     */
    public String getCardScheme() {
        return cardScheme;
    }

    /**
     * @param cardScheme the cardScheme to set
     */
    public void setCardScheme(String cardScheme) {
        this.cardScheme = cardScheme;
    }

    /**
     * @return the transactions
     */
    public List<ReconTransaction> getTransactions() {
        return transactions;
    }

    /**
     * @param transactions the transactions to set
     */
    public void setTransactions(List<ReconTransaction> transactions) {
        this.transactions = transactions;
    }

    /**
     * @return the merchantTransaction
     */
    public Map<String, Integer> getMerchantTransaction() {
        return merchantTransaction;
    }

    /**
     * @param merchantTransaction the merchantTransaction to set
     */
    public void setMerchantTransaction(Map<String, Integer> merchantTransaction) {
        this.merchantTransaction = merchantTransaction;
    }

    /**
     * @return the originCurrencies
     */
    public Map<String, Double> getOriginCurrencies() {
        return originCurrencies;
    }

    /**
     * @param originCurrencies the originCurrencies to set
     */
    public void setOriginCurrencies(Map<String, Double> originCurrencies) {
        this.originCurrencies = originCurrencies;
    }

    /**
     * @return the transactionCount
     */
    public long getTransactionCount() {
        return transactionCount;
    }

    /**
     * @param transactionCount the transactionCount to set
     */
    public void setTransactionCount(long transactionCount) {
        this.transactionCount = transactionCount;
    }

    /**
     * @return the destinationCurrencies
     */
    public Map<String, Double> getDestinationCurrencies() {
        return destinationCurrencies;
    }

    /**
     * @param destinationCurrencies the destinationCurrencies to set
     */
    public void setDestinationCurrencies(Map<String, Double> destinationCurrencies) {
        this.destinationCurrencies = destinationCurrencies;
    }

    /**
     * @return the settled
     */
    public String getSettled() {
        return settled;
    }

    /**
     * @param settled the settled to set
     */
    public void setSettled(String settled) {
        this.settled = settled;
    }
    
    private Map<String, Double> originCurrencies;
    private long transactionCount;
    private Map<String, Double> destinationCurrencies;
    private Map<String, Integer> merchantTransaction;
    private String settled;
    private List<ReconTransaction> transactions;
    private String cardScheme;
    private String bank;
    private List<Date> dates = new ArrayList<>();
}

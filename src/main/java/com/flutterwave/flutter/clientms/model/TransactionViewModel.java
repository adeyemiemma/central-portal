/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Entity
public class TransactionViewModel implements Serializable {

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the transactionCurrency
     */
    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    /**
     * @param transactionCurrency the transactionCurrency to set
     */
    public void setTransactionCurrency(String transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

    /**
     * @return the transactionCountry
     */
    public String getTransactionCountry() {
        return transactionCountry;
    }

    /**
     * @param transactionCountry the transactionCountry to set
     */
    public void setTransactionCountry(String transactionCountry) {
        this.transactionCountry = transactionCountry;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the merchantTxnReference
     */
    public String getMerchantTxnReference() {
        return merchantTxnReference;
    }

    /**
     * @param merchantTxnReference the merchantTxnReference to set
     */
    public void setMerchantTxnReference(String merchantTxnReference) {
        this.merchantTxnReference = merchantTxnReference;
    }

    /**
     * @return the flwTxnReference
     */
    public String getFlwTxnReference() {
        return flwTxnReference;
    }

    /**
     * @param flwTxnReference the flwTxnReference to set
     */
    public void setFlwTxnReference(String flwTxnReference) {
        this.flwTxnReference = flwTxnReference;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the authenticationModel
     */
    public String getAuthenticationModel() {
        return authenticationModel;
    }

    /**
     * @param authenticationModel the authenticationModel to set
     */
    public void setAuthenticationModel(String authenticationModel) {
        this.authenticationModel = authenticationModel;
    }

    /**
     * @return the transactionNarration
     */
    public String getTransactionNarration() {
        return transactionNarration;
    }

    /**
     * @param transactionNarration the transactionNarration to set
     */
    public void setTransactionNarration(String transactionNarration) {
        this.transactionNarration = transactionNarration;
    }

    /**
     * @return the cardCountry
     */
    public String getCardCountry() {
        return cardCountry;
    }

    /**
     * @param cardCountry the cardCountry to set
     */
    public void setCardCountry(String cardCountry) {
        this.cardCountry = cardCountry;
    }

    /**
     * @return the cardMask
     */
    public String getCardMask() {
        return cardMask;
    }

    /**
     * @param cardMask the cardMask to set
     */
    public void setCardMask(String cardMask) {
        this.cardMask = cardMask;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the cardScheme
     */
    public String getCardScheme() {
        return cardScheme;
    }

    /**
     * @param cardScheme the cardScheme to set
     */
    public void setCardScheme(String cardScheme) {
        this.cardScheme = cardScheme;
    }
    
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private long id;
    private String rrn;
//    @JoinColumn(name = "provider_id")
    private String provider;
    private String merchantId;
    private String transactionCurrency;
    private String transactionCountry;
    private double amount;
    private String merchantTxnReference;
    private String flwTxnReference;
    private String transactionType;
    private String authenticationModel;
    private String transactionNarration;
    private String cardCountry;
    private String cardMask;
    private String datetime;
    private String responseCode;
    private String responseMessage;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Column(name = "cardscheme")
    private String cardScheme;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.Utility;
import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public class DateModel extends Date{
    
    @Override
    public String toString() {
        return Utility.formatDate(this, "dd-MM");
    }

    @Override
    public int compareTo(Date anotherDate) {
        Date d = this;
        return d.compareTo(anotherDate) >= 0 ? 1 : 0;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.model.maker.*;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.util.Status;
import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public abstract class BaseModel {
    
    public abstract User getApprovedBy();
    public abstract void setApprovedBy(User user);
    public abstract Date getApprovedOn();
    public abstract void setApprovedOn(Date approvedOn);
    public abstract Date getCreatedOn();
    public abstract void setCreatedOn(Date date);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Cacheable(true)
@Table(name = "\"transaction\"")
@Entity
public class Transaction implements Serializable {

    /**
     * @return the disburseRetryCount
     */
    public int getDisburseRetryCount() {
        return disburseRetryCount;
    }

    /**
     * @param disburseRetryCount the disburseRetryCount to set
     */
    public void setDisburseRetryCount(int disburseRetryCount) {
        this.disburseRetryCount = disburseRetryCount;
    }

    /**
     * @return the lastRetried
     */
    public Date getLastRetried() {
        return lastRetried;
    }

    /**
     * @param lastRetried the lastRetried to set
     */
    public void setLastRetried(Date lastRetried) {
        this.lastRetried = lastRetried;
    }

    /**
     * @return the lastRetriedBy
     */
    public User getLastRetriedBy() {
        return lastRetriedBy;
    }

    /**
     * @param lastRetriedBy the lastRetriedBy to set
     */
    public void setLastRetriedBy(User lastRetriedBy) {
        this.lastRetriedBy = lastRetriedBy;
    }

    /**
     * @return the settled
     */
    public Boolean getSettled() {
        return settled;
    }

    /**
     * @return the disburseResponseCode
     */
    public String getDisburseResponseCode() {
        return disburseResponseCode;
    }

    /**
     * @param disburseResponseCode the disburseResponseCode to set
     */
    public void setDisburseResponseCode(String disburseResponseCode) {
        this.disburseResponseCode = disburseResponseCode;
    }

    /**
     * @return the disburseResponseMessage
     */
    public String getDisburseResponseMessage() {
        return disburseResponseMessage;
    }

    /**
     * @param disburseResponseMessage the disburseResponseMessage to set
     */
    public void setDisburseResponseMessage(String disburseResponseMessage) {
        this.disburseResponseMessage = disburseResponseMessage;
    }

    /**
     * @return the cardScheme
     */
    public String getCardScheme() {
        return cardScheme;
    }

    /**
     * @param cardScheme the cardScheme to set
     */
    public void setCardScheme(String cardScheme) {
        this.cardScheme = cardScheme;
    }

    /**
     * @return the settlementState
     */
    public Utility.SettlementState getSettlementState() {
        return settlementState;
    }

    /**
     * @param settlementState the settlementState to set
     */
    public void setSettlementState(Utility.SettlementState settlementState) {
        this.settlementState = settlementState;
    }

    /**
     * @return the settlement
     */
    public Settlement getSettlement() {
        return settlement;
    }

    /**
     * @param settlement the settlement to set
     */
    public void setSettlement(Settlement settlement) {
        this.settlement = settlement;
    }

    /**
     * @return the settledOn
     */
    public Date getSettledOn() {
        return settledOn;
    }

    /**
     * @param settledOn the settledOn to set
     */
    public void setSettledOn(Date settledOn) {
        this.settledOn = settledOn;
    }

    /**
     * @return the settledBy
     */
    public User getSettledBy() {
        return settledBy;
    }

    /**
     * @param settledBy the settledBy to set
     */
    public void setSettledBy(User settledBy) {
        this.settledBy = settledBy;
    }

    /**
     * @return the settled
     */
    public Boolean isSettled() {
        return getSettled();
    }

    /**
     * @param settled the settled to set
     */
    public void setSettled(Boolean settled) {
        this.settled = settled;
    }

    /**
     * @return the transactionCountry
     */
    public String getTransactionCountry() {
        return transactionCountry;
    }

    /**
     * @param transactionCountry the transactionCountry to set
     */
    public void setTransactionCountry(String transactionCountry) {
        this.transactionCountry = transactionCountry;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the transactionCurrency
     */
    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    /**
     * @param transactionCurrency the transactionCurrency to set
     */
    public void setTransactionCurrency(String transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the merchantTxnReference
     */
    public String getMerchantTxnReference() {
        return merchantTxnReference;
    }

    /**
     * @param merchantTxnReference the merchantTxnReference to set
     */
    public void setMerchantTxnReference(String merchantTxnReference) {
        this.merchantTxnReference = merchantTxnReference;
    }

    /**
     * @return the flwTxnReference
     */
    public String getFlwTxnReference() {
        return flwTxnReference;
    }

    /**
     * @param flwTxnReference the flwTxnReference to set
     */
    public void setFlwTxnReference(String flwTxnReference) {
        this.flwTxnReference = flwTxnReference;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the authenticationModel
     */
    public String getAuthenticationModel() {
        return authenticationModel;
    }

    /**
     * @param authenticationModel the authenticationModel to set
     */
    public void setAuthenticationModel(String authenticationModel) {
        this.authenticationModel = authenticationModel;
    }

    /**
     * @return the transactionNarration
     */
    public String getTransactionNarration() {
        return transactionNarration;
    }

    /**
     * @param transactionNarration the transactionNarration to set
     */
    public void setTransactionNarration(String transactionNarration) {
        this.transactionNarration = transactionNarration;
    }

    /**
     * @return the cardCountry
     */
    public String getCardCountry() {
        return cardCountry;
    }

    /**
     * @param cardCountry the cardCountry to set
     */
    public void setCardCountry(String cardCountry) {
        this.cardCountry = cardCountry;
    }

    /**
     * @return the cardMask
     */
    public String getCardMask() {
        return cardMask;
    }

    /**
     * @param cardMask the cardMask to set
     */
    public void setCardMask(String cardMask) {
        this.cardMask = cardMask;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the transactionData
     */
    public String getTransactionData() {
        return transactionData;
    }

    /**
     * @param transactionData the transactionData to set
     */
    public void setTransactionData(String transactionData) {
        this.transactionData = transactionData;
    }

    /**
     * @return the processorReference
     */
    public String getProcessorReference() {
        return processorReference;
    }

    /**
     * @param processorReference the processorReference to set
     */
    public void setProcessorReference(String processorReference) {
        this.processorReference = processorReference;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private long id;
    private String rrn;
//    @JoinColumn(name = "provider_id")
    private String provider;
    private String merchantId;
    private String transactionCurrency;
    private String transactionCountry;
    private double amount;
    private String merchantTxnReference;
    private String flwTxnReference;
    private String transactionType;
    private String authenticationModel;
    private String transactionNarration;
    private String cardCountry;
    private String cardMask;
    private String datetime;
    private String responseCode;
    private String responseMessage;
    private String transactionData; // This is an array
    private String processorReference; // This is an array
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date settledOn;
    @Column(name = "cardscheme")
    private String cardScheme;
    @ManyToOne
    private User settledBy;
    private Boolean settled;
//    private String merchantName;
    @ManyToOne
    private Settlement settlement;
    private Utility.SettlementState settlementState;
    private String disburseResponseCode;
    private String disburseResponseMessage;
    private int disburseRetryCount;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastRetried;
    @ManyToOne(fetch = FetchType.LAZY)
    private User lastRetriedBy;
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "chargeback_dispute")
@Entity
public class ChargeBackDispute extends BaseModel implements Serializable {

    /**
     * @return the applied
     */
    public boolean isApplied() {
        return applied;
    }

    /**
     * @param applied the applied to set
     */
    public void setApplied(boolean applied) {
        this.applied = applied;
    }

    /**
     * @return the appliedBy
     */
    public User getAppliedBy() {
        return appliedBy;
    }

    /**
     * @param appliedBy the appliedBy to set
     */
    public void setAppliedBy(User appliedBy) {
        this.appliedBy = appliedBy;
    }

    /**
     * @return the appliedOn
     */
    public Date getAppliedOn() {
        return appliedOn;
    }

    /**
     * @param appliedOn the appliedOn to set
     */
    public void setAppliedOn(Date appliedOn) {
        this.appliedOn = appliedOn;
    }

    /**
     * @return the apiUsername
     */
    public String getApiUsername() {
        return apiUsername;
    }

    /**
     * @param apiUsername the apiUsername to set
     */
    public void setApiUsername(String apiUsername) {
        this.apiUsername = apiUsername;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the chargeBack
     */
    public ChargeBack getChargeBack() {
        return chargeBack;
    }

    /**
     * @param chargeBack the chargeBack to set
     */
    public void setChargeBack(ChargeBack chargeBack) {
        this.chargeBack = chargeBack;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the approvedBy
     */
    public User getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @ManyToOne
    private ChargeBack chargeBack;
    @Column(columnDefinition = "text")
    private String description;
    @Column(columnDefinition = "text")
    private String image;
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvedOn;
    @ManyToOne
    private User approvedBy;
    @ManyToOne
    private User createdBy;
    private String apiUsername;
    private boolean applied;
    @ManyToOne
    private User appliedBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date appliedOn;
}

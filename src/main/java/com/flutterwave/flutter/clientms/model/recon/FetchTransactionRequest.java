/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.recon;

import java.io.Serializable;

/**
 *
 * @author emmanueladeyemi
 */
public class FetchTransactionRequest implements Serializable{

    /**
     * @return the transactionBatch
     */
    public String getTransactionBatch() {
        return transactionBatch;
    }

    /**
     * @param transactionBatch the transactionBatch to set
     */
    public void setTransactionBatch(String transactionBatch) {
        this.transactionBatch = transactionBatch;
    }

    /**
     * @return the doSettlement
     */
    public boolean isDoSettlement() {
        return doSettlement;
    }

    /**
     * @param doSettlement the doSettlement to set
     */
    public void setDoSettlement(boolean doSettlement) {
        this.doSettlement = doSettlement;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the start
     */
    public long getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(long start) {
        this.start = start;
    }

    /**
     * @return the limit
     */
    public long getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(long limit) {
        this.limit = limit;
    }

    /**
     * @return the retry
     */
    public int getRetry() {
        return retry;
    }

    /**
     * @param retry the retry to set
     */
    public void setRetry(int retry) {
        this.retry = retry;
    }

    /**
     * @return the schedule
     */
    public long getSchedule() {
        return schedule;
    }

    /**
     * @param schedule the schedule to set
     */
    public void setSchedule(long schedule) {
        this.schedule = schedule;
    }
    
    private String startDate;
    private String endDate;
    private long start;
    private long limit;
    private int retry;
    private long schedule;
    private boolean doSettlement;
    private String transactionBatch;

    @Override
    public String toString() {
        
        StringBuilder builder = new StringBuilder();
        builder.append("")
                .append(startDate)
                .append(endDate)
                .append(start)
                .append(limit)
                .append(retry)
                .append(retry)
                .append(doSettlement)
                .append(schedule);
        
        return builder.toString();
    }
    
    
}

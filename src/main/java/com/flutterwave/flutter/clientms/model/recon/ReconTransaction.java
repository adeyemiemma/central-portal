/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.recon;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author adeyemi
 */
@Entity
@Table(name = "recon_transaction")
public class ReconTransaction implements Serializable {

    /**
     * @return the flutterTransaction
     */
    public FlutterTransaction getFlutterTransaction() {
        return flutterTransaction;
    }

    /**
     * @param flutterTransaction the flutterTransaction to set
     */
    public void setFlutterTransaction(FlutterTransaction flutterTransaction) {
        this.flutterTransaction = flutterTransaction;
    }

    /**
     * @return the batch
     */
    public Batch getBatch() {
        return batch;
    }

    /**
     * @param batch the batch to set
     */
    public void setBatch(Batch batch) {
        this.batch = batch;
    }

    /**
     * @return the settled
     */
    public boolean isSettled() {
        return settled;
    }

    /**
     * @param settled the settled to set
     */
    public void setSettled(boolean settled) {
        this.settled = settled;
    }

    /**
     * @return the settlementDate
     */
    public Date getSettlementDate() {
        return settlementDate;
    }

    /**
     * @param settlementDate the settlementDate to set
     */
    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return the cardScheme
     */
    public String getCardScheme() {
        return cardScheme;
    }

    /**
     * @param cardScheme the cardScheme to set
     */
    public void setCardScheme(String cardScheme) {
        this.cardScheme = cardScheme;
    }

    /**
     * @return the rejectMessage
     */
    public String getRejectMessage() {
        return rejectMessage;
    }

    /**
     * @param rejectMessage the rejectMessage to set
     */
    public void setRejectMessage(String rejectMessage) {
        this.rejectMessage = rejectMessage;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the fileId
     */
    public long getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the batchId
     */
    public long getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(long batchId) {
        this.batchId = batchId;
    }

    /**
     * @return the direction
     */
    public int getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(int direction) {
        this.direction = direction;
    }

    /**
     * @return the expId
     */
    public String getExpId() {
        return expId;
    }

    /**
     * @param expId the expId to set
     */
    public void setExpId(String expId) {
        this.expId = expId;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the associationId
     */
    public int getAssociationId() {
        return associationId;
    }

    /**
     * @param associationId the associationId to set
     */
    public void setAssociationId(int associationId) {
        this.associationId = associationId;
    }

    /**
     * @return the batchImportLocationId
     */
    public String getBatchImportLocationId() {
        return batchImportLocationId;
    }

    /**
     * @param batchImportLocationId the batchImportLocationId to set
     */
    public void setBatchImportLocationId(String batchImportLocationId) {
        this.batchImportLocationId = batchImportLocationId;
    }

    /**
     * @return the batchExportLocationId
     */
    public String getBatchExportLocationId() {
        return batchExportLocationId;
    }

    /**
     * @param batchExportLocationId the batchExportLocationId to set
     */
    public void setBatchExportLocationId(String batchExportLocationId) {
        this.batchExportLocationId = batchExportLocationId;
    }

    /**
     * @return the operationDate
     */
    public Date getOperationDate() {
        return operationDate;
    }

    /**
     * @param operationDate the operationDate to set
     */
    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    /**
     * @return the groupCount
     */
    public long getGroupCount() {
        return groupCount;
    }

    /**
     * @param groupCount the groupCount to set
     */
    public void setGroupCount(long groupCount) {
        this.groupCount = groupCount;
    }

    /**
     * @return the transactionCount
     */
    public long getTransactionCount() {
        return transactionCount;
    }

    /**
     * @param transactionCount the transactionCount to set
     */
    public void setTransactionCount(long transactionCount) {
        this.transactionCount = transactionCount;
    }

    /**
     * @return the messageCount
     */
    public long getMessageCount() {
        return messageCount;
    }

    /**
     * @param messageCount the messageCount to set
     */
    public void setMessageCount(long messageCount) {
        this.messageCount = messageCount;
    }

    /**
     * @return the transactionImportLocationId
     */
    public int getTransactionImportLocationId() {
        return transactionImportLocationId;
    }

    /**
     * @param transactionImportLocationId the transactionImportLocationId to set
     */
    public void setTransactionImportLocationId(int transactionImportLocationId) {
        this.transactionImportLocationId = transactionImportLocationId;
    }

    /**
     * @return the transactionExportLocationId
     */
    public int getTransactionExportLocationId() {
        return transactionExportLocationId;
    }

    /**
     * @param transactionExportLocationId the transactionExportLocationId to set
     */
    public void setTransactionExportLocationId(int transactionExportLocationId) {
        this.transactionExportLocationId = transactionExportLocationId;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the originClearanceAmount
     */
    public long getOriginClearanceAmount() {
        return originClearanceAmount;
    }

    /**
     * @param originClearanceAmount the originClearanceAmount to set
     */
    public void setOriginClearanceAmount(long originClearanceAmount) {
        this.originClearanceAmount = originClearanceAmount;
    }

    /**
     * @return the originClearanceCurrency
     */
    public long getOriginClearanceCurrency() {
        return originClearanceCurrency;
    }

    /**
     * @param originClearanceCurrency the originClearanceCurrency to set
     */
    public void setOriginClearanceCurrency(int originClearanceCurrency) {
        this.originClearanceCurrency = originClearanceCurrency;
    }

    /**
     * @return the originalAmount
     */
    public double getOriginalAmount() {
        return originalAmount;
    }

    /**
     * @param originalAmount the originalAmount to set
     */
    public void setOriginalAmount(double originalAmount) {
        this.originalAmount = originalAmount;
    }

    /**
     * @return the originalCurrency
     */
    public String getOriginalCurrency() {
        return originalCurrency;
    }

    /**
     * @param originalCurrency the originalCurrency to set
     */
    public void setOriginalCurrency(String originalCurrency) {
        this.originalCurrency = originalCurrency;
    }

    /**
     * @return the destinationClearanceAmount
     */
    public long getDestinationClearanceAmount() {
        return destinationClearanceAmount;
    }

    /**
     * @param destinationClearanceAmount the destinationClearanceAmount to set
     */
    public void setDestinationClearanceAmount(long destinationClearanceAmount) {
        this.destinationClearanceAmount = destinationClearanceAmount;
    }

    /**
     * @return the destinationClearanceCurrency
     */
    public long getDestinationClearanceCurrency() {
        return destinationClearanceCurrency;
    }

    /**
     * @param destinationClearanceCurrency the destinationClearanceCurrency to set
     */
    public void setDestinationClearanceCurrency(int destinationClearanceCurrency) {
        this.destinationClearanceCurrency = destinationClearanceCurrency;
    }

    /**
     * @return the destinationReconAmount
     */
    public double getDestinationReconAmount() {
        return destinationReconAmount;
    }

    /**
     * @param destinationReconAmount the destinationReconAmount to set
     */
    public void setDestinationReconAmount(double destinationReconAmount) {
        this.destinationReconAmount = destinationReconAmount;
    }

    /**
     * @return the destinationReconCurrency
     */
    public double getDestinationReconCurrency() {
        return destinationReconCurrency;
    }

    /**
     * @param destinationReconCurrency the destinationReconCurrency to set
     */
    public void setDestinationReconCurrency(int destinationReconCurrency) {
        this.destinationReconCurrency = destinationReconCurrency;
    }

    /**
     * @return the issCurrency
     */
    public int getIssCurrency() {
        return issCurrency;
    }

    /**
     * @param issCurrency the issCurrency to set
     */
    public void setIssCurrency(int issCurrency) {
        this.issCurrency = issCurrency;
    }

    /**
     * @return the originClearDate
     */
    public Date getOriginClearDate() {
        return originClearDate;
    }

    /**
     * @param originClearDate the originClearDate to set
     */
    public void setOriginClearDate(Date originClearDate) {
        this.originClearDate = originClearDate;
    }

    /**
     * @return the originTime
     */
    public Date getOriginTime() {
        return originTime;
    }

    /**
     * @param originTime the originTime to set
     */
    public void setOriginTime(Date originTime) {
        this.originTime = originTime;
    }

    /**
     * @return the maskedPan
     */
    public String getMaskedPan() {
        return maskedPan;
    }

    /**
     * @param maskedPan the maskedPan to set
     */
    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }

    /**
     * @return the terminalName
     */
    public String getTerminalName() {
        return terminalName;
    }

    /**
     * @param terminalName the terminalName to set
     */
    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    /**
     * @return the cardAcceptorId
     */
    public String getCardAcceptorId() {
        return cardAcceptorId;
    }

    /**
     * @param cardAcceptorId the cardAcceptorId to set
     */
    public void setCardAcceptorId(String cardAcceptorId) {
        this.cardAcceptorId = cardAcceptorId;
    }

    /**
     * @return the merchantTitle
     */
    public String getMerchantTitle() {
        return merchantTitle;
    }

    /**
     * @param merchantTitle the merchantTitle to set
     */
    public void setMerchantTitle(String merchantTitle) {
        this.merchantTitle = merchantTitle;
    }

    /**
     * @return the merchantLocation
     */
    public String getMerchantLocation() {
        return merchantLocation;
    }

    /**
     * @param merchantLocation the merchantLocation to set
     */
    public void setMerchantLocation(String merchantLocation) {
        this.merchantLocation = merchantLocation;
    }

    /**
     * @return the merchantCity
     */
    public String getMerchantCity() {
        return merchantCity;
    }

    /**
     * @param merchantCity the merchantCity to set
     */
    public void setMerchantCity(String merchantCity) {
        this.merchantCity = merchantCity;
    }

    /**
     * @return the merchantRegion
     */
    public String getMerchantRegion() {
        return merchantRegion;
    }

    /**
     * @param merchantRegion the merchantRegion to set
     */
    public void setMerchantRegion(String merchantRegion) {
        this.merchantRegion = merchantRegion;
    }

    /**
     * @return the mcc
     */
    public long getMcc() {
        return mcc;
    }

    /**
     * @param mcc the mcc to set
     */
    public void setMcc(long mcc) {
        this.mcc = mcc;
    }

    /**
     * @return the approvalCode
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * @param approvalCode the approvalCode to set
     */
    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the externalResponseCode
     */
    public String getExternalResponseCode() {
        return externalResponseCode;
    }

    /**
     * @param externalResponseCode the externalResponseCode to set
     */
    public void setExternalResponseCode(String externalResponseCode) {
        this.externalResponseCode = externalResponseCode;
    }

    /**
     * @return the expArn
     */
    public String getExpArn() {
        return expArn;
    }

    /**
     * @param expArn the expArn to set
     */
    public void setExpArn(String expArn) {
        this.expArn = expArn;
    }

    /**
     * @return the expTransactionId
     */
    public String getExpTransactionId() {
        return expTransactionId;
    }

    /**
     * @param expTransactionId the expTransactionId to set
     */
    public void setExpTransactionId(String expTransactionId) {
        this.expTransactionId = expTransactionId;
    }

    /**
     * @return the acquiredId
     */
    public String getAcquiredId() {
        return acquiredId;
    }

    /**
     * @param acquiredId the acquiredId to set
     */
    public void setAcquiredId(String acquiredId) {
        this.acquiredId = acquiredId;
    }

    /**
     * @return the impBatchId
     */
    public long getImpBatchId() {
        return impBatchId;
    }

    /**
     * @param impBatchId the impBatchId to set
     */
    public void setImpBatchId(long impBatchId) {
        this.impBatchId = impBatchId;
    }

    /**
     * @return the expBatchId
     */
    public long getExpBatchId() {
        return expBatchId;
    }

    /**
     * @param expBatchId the expBatchId to set
     */
    public void setExpBatchId(long expBatchId) {
        this.expBatchId = expBatchId;
    }

    /**
     * @return the notificationBatchId
     */
    public long getNotificationBatchId() {
        return notificationBatchId;
    }

    /**
     * @param notificationBatchId the notificationBatchId to set
     */
    public void setNotificationBatchId(long notificationBatchId) {
        this.notificationBatchId = notificationBatchId;
    }

    /**
     * @return the settlerBatchId
     */
    public long getSettlerBatchId() {
        return settlerBatchId;
    }

    /**
     * @param settlerBatchId the settlerBatchId to set
     */
    public void setSettlerBatchId(long settlerBatchId) {
        this.settlerBatchId = settlerBatchId;
    }

    /**
     * @return the origIIN
     */
    public String getOrigIIN() {
        return origIIN;
    }

    /**
     * @param origIIN the origIIN to set
     */
    public void setOrigIIN(String origIIN) {
        this.origIIN = origIIN;
    }

    /**
     * @return the destIIN
     */
    public String getDestIIN() {
        return destIIN;
    }

    /**
     * @param destIIN the destIIN to set
     */
    public void setDestIIN(String destIIN) {
        this.destIIN = destIIN;
    }

    /**
     * @return the aid
     */
    public String getAid() {
        return aid;
    }

    /**
     * @param aid the aid to set
     */
    public void setAid(String aid) {
        this.aid = aid;
    }

    /**
     * @return the tranType
     */
    public String getTranType() {
        return tranType;
    }

    /**
     * @param tranType the tranType to set
     */
    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    /**
     * @return the tranTypeDesc
     */
    public String getTranTypeDesc() {
        return tranTypeDesc;
    }

    /**
     * @param tranTypeDesc the tranTypeDesc to set
     */
    public void setTranTypeDesc(String tranTypeDesc) {
        this.tranTypeDesc = tranTypeDesc;
    }

    /**
     * @return the tranCode
     */
    public int getTranCode() {
        return tranCode;
    }

    /**
     * @param tranCode the tranCode to set
     */
    public void setTranCode(int tranCode) {
        this.tranCode = tranCode;
    }

    /**
     * @return the tranCodeDesc
     */
    public String getTranCodeDesc() {
        return tranCodeDesc;
    }

    /**
     * @param tranCodeDesc the tranCodeDesc to set
     */
    public void setTranCodeDesc(String tranCodeDesc) {
        this.tranCodeDesc = tranCodeDesc;
    }

    /**
     * @return the batchPhase
     */
    public long getBatchPhase() {
        return batchPhase;
    }

    /**
     * @param batchPhase the batchPhase to set
     */
    public void setBatchPhase(long batchPhase) {
        this.batchPhase = batchPhase;
    }

    /**
     * @return the tranPhase
     */
    public long getTranPhase() {
        return tranPhase;
    }

    /**
     * @param tranPhase the tranPhase to set
     */
    public void setTranPhase(long tranPhase) {
        this.tranPhase = tranPhase;
    }

    /**
     * @return the accountAmount
     */
    public double getAccountAmount() {
        return accountAmount;
    }

    /**
     * @param accountAmount the accountAmount to set
     */
    public void setAccountAmount(double accountAmount) {
        this.accountAmount = accountAmount;
    }

    /**
     * @return the accountCurrency
     */
    public int getAccountCurrency() {
        return accountCurrency;
    }

    /**
     * @param accountCurrency the accountCurrency to set
     */
    public void setAccountCurrency(int accountCurrency) {
        this.accountCurrency = accountCurrency;
    }

    /**
     * @return the settleAmount
     */
    public double getSettleAmount() {
        return settleAmount;
    }

    /**
     * @param settleAmount the settleAmount to set
     */
    public void setSettleAmount(double settleAmount) {
        this.settleAmount = settleAmount;
    }

    /**
     * @return the settleCurrency
     */
    public int getSettleCurrency() {
        return settleCurrency;
    }

    /**
     * @param settleCurrency the settleCurrency to set
     */
    public void setSettleCurrency(int settleCurrency) {
        this.settleCurrency = settleCurrency;
    }

    /**
     * @return the feeRule
     */
    public String getFeeRule() {
        return feeRule;
    }

    /**
     * @param feeRule the feeRule to set
     */
    public void setFeeRule(String feeRule) {
        this.feeRule = feeRule;
    }
    
    
    @Id
//    @GenericGenerator(name = "id-gen", strategy = "increment")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long fileId;
    private long batchId;
    private int direction;
    private String expId;
    private String fileName;
    private int associationId;
    private String batchImportLocationId;
    private String batchExportLocationId;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date operationDate;
    private long groupCount;
    private long transactionCount;
    private long messageCount;
    private int transactionImportLocationId;
    private int transactionExportLocationId;
    
    @Column(columnDefinition = "varchar(15)")
    private String rrn;
    
    private long originClearanceAmount;
    private int originClearanceCurrency;
    private double originalAmount;
    
    private String originalCurrency;
    
    private long destinationClearanceAmount;
    private int destinationClearanceCurrency;
    private double destinationReconAmount;
    private int destinationReconCurrency;
    private int issCurrency;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date originClearDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date originTime;
    private String maskedPan;
    private String terminalName;
    private String cardAcceptorId;
    private String merchantTitle;
    private String merchantLocation;
    private String merchantCity;
    private String merchantRegion;
    private long mcc;
    private String approvalCode;
    private String responseCode;
    private String externalResponseCode;
    private String expArn;
    private String expTransactionId;
    private String acquiredId;
    private long impBatchId;
    private long expBatchId;
    private long notificationBatchId;
    private long settlerBatchId;
    private String rejectMessage;
    private String origIIN;
    private String destIIN;
    private String aid;
    private String tranType;
    private String tranTypeDesc;
    private int tranCode;
    private String tranCodeDesc;
    private long batchPhase;
    private long tranPhase;
    private double accountAmount;
    private int accountCurrency;
    private double settleAmount;
    private int settleCurrency;
    private String feeRule; 
    
    private String bank;
    private String cardScheme;
    private boolean settled;
    
    @OneToOne
    private FlutterTransaction flutterTransaction;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date settlementDate;
    
    @OneToOne
    private Batch batch;
}

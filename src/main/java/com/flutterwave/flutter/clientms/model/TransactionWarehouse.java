/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "transaction_warehouse")
@Entity
public class TransactionWarehouse implements Serializable{

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the transactionNumber
     */
    public String getTransactionNumber() {
        return transactionNumber;
    }

    /**
     * @param transactionNumber the transactionNumber to set
     */
    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the transactionCurrency
     */
    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    /**
     * @param transactionCurrency the transactionCurrency to set
     */
    public void setTransactionCurrency(String transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

    /**
     * @return the transactionCountry
     */
    public String getTransactionCountry() {
        return transactionCountry;
    }

    /**
     * @param transactionCountry the transactionCountry to set
     */
    public void setTransactionCountry(String transactionCountry) {
        this.transactionCountry = transactionCountry;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the merchantTxnReference
     */
    public String getMerchantTxnReference() {
        return merchantTxnReference;
    }

    /**
     * @param merchantTxnReference the merchantTxnReference to set
     */
    public void setMerchantTxnReference(String merchantTxnReference) {
        this.merchantTxnReference = merchantTxnReference;
    }

    /**
     * @return the flwTxnReference
     */
    public String getFlwTxnReference() {
        return flwTxnReference;
    }

    /**
     * @param flwTxnReference the flwTxnReference to set
     */
    public void setFlwTxnReference(String flwTxnReference) {
        this.flwTxnReference = flwTxnReference;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the authenticationModel
     */
    public String getAuthenticationModel() {
        return authenticationModel;
    }

    /**
     * @param authenticationModel the authenticationModel to set
     */
    public void setAuthenticationModel(String authenticationModel) {
        this.authenticationModel = authenticationModel;
    }

    /**
     * @return the transactionNarration
     */
    public String getTransactionNarration() {
        return transactionNarration;
    }

    /**
     * @param transactionNarration the transactionNarration to set
     */
    public void setTransactionNarration(String transactionNarration) {
        this.transactionNarration = transactionNarration;
    }

    /**
     * @return the cardCountry
     */
    public String getCardCountry() {
        return cardCountry;
    }

    /**
     * @param cardCountry the cardCountry to set
     */
    public void setCardCountry(String cardCountry) {
        this.cardCountry = cardCountry;
    }

    /**
     * @return the cardMask
     */
    public String getCardMask() {
        return cardMask;
    }

    /**
     * @param cardMask the cardMask to set
     */
    public void setCardMask(String cardMask) {
        this.cardMask = cardMask;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the transactionData
     */
    public String getTransactionData() {
        return transactionData;
    }

    /**
     * @param transactionData the transactionData to set
     */
    public void setTransactionData(String transactionData) {
        this.transactionData = transactionData;
    }

    /**
     * @return the processorReference
     */
    public String getProcessorReference() {
        return processorReference;
    }

    /**
     * @param processorReference the processorReference to set
     */
    public void setProcessorReference(String processorReference) {
        this.processorReference = processorReference;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the settledOn
     */
    public Date getSettledOn() {
        return settledOn;
    }

    /**
     * @param settledOn the settledOn to set
     */
    public void setSettledOn(Date settledOn) {
        this.settledOn = settledOn;
    }

    /**
     * @return the cardScheme
     */
    public String getCardScheme() {
        return cardScheme;
    }

    /**
     * @param cardScheme the cardScheme to set
     */
    public void setCardScheme(String cardScheme) {
        this.cardScheme = cardScheme;
    }

    /**
     * @return the settledBy
     */
    public String getSettledBy() {
        return settledBy;
    }

    /**
     * @param settledBy the settledBy to set
     */
    public void setSettledBy(String settledBy) {
        this.settledBy = settledBy;
    }

    /**
     * @return the settled
     */
    public Boolean getSettled() {
        return settled;
    }

    /**
     * @param settled the settled to set
     */
    public void setSettled(Boolean settled) {
        this.settled = settled;
    }

    /**
     * @return the settlementId
     */
    public String getSettlementId() {
        return settlementId;
    }

    /**
     * @param settlementId the settlementId to set
     */
    public void setSettlementId(String settlementId) {
        this.settlementId = settlementId;
    }

    /**
     * @return the settlementState
     */
    public Utility.SettlementState getSettlementState() {
        return settlementState;
    }

    /**
     * @param settlementState the settlementState to set
     */
    public void setSettlementState(Utility.SettlementState settlementState) {
        this.settlementState = settlementState;
    }

    /**
     * @return the disburseResponseCode
     */
    public String getDisburseResponseCode() {
        return disburseResponseCode;
    }

    /**
     * @param disburseResponseCode the disburseResponseCode to set
     */
    public void setDisburseResponseCode(String disburseResponseCode) {
        this.disburseResponseCode = disburseResponseCode;
    }

    /**
     * @return the disburseResponseMessage
     */
    public String getDisburseResponseMessage() {
        return disburseResponseMessage;
    }

    /**
     * @param disburseResponseMessage the disburseResponseMessage to set
     */
    public void setDisburseResponseMessage(String disburseResponseMessage) {
        this.disburseResponseMessage = disburseResponseMessage;
    }

    /**
     * @return the disburseRetryCount
     */
    public int getDisburseRetryCount() {
        return disburseRetryCount;
    }

    /**
     * @param disburseRetryCount the disburseRetryCount to set
     */
    public void setDisburseRetryCount(int disburseRetryCount) {
        this.disburseRetryCount = disburseRetryCount;
    }

    /**
     * @return the lastRetried
     */
    public Date getLastRetried() {
        return lastRetried;
    }

    /**
     * @param lastRetried the lastRetried to set
     */
    public void setLastRetried(Date lastRetried) {
        this.lastRetried = lastRetried;
    }

    /**
     * @return the lastRetriedBy
     */
    public String getLastRetriedBy() {
        return lastRetriedBy;
    }

    /**
     * @param lastRetriedBy the lastRetriedBy to set
     */
    public void setLastRetriedBy(String lastRetriedBy) {
        this.lastRetriedBy = lastRetriedBy;
    }

    /**
     * @return the reprocessedResponseCode
     */
    public String getReprocessedResponseCode() {
        return reprocessedResponseCode;
    }

    /**
     * @param reprocessedResponseCode the reprocessedResponseCode to set
     */
    public void setReprocessedResponseCode(String reprocessedResponseCode) {
        this.reprocessedResponseCode = reprocessedResponseCode;
    }

    /**
     * @return the reprocessReference
     */
    public String getReprocessReference() {
        return reprocessReference;
    }

    /**
     * @param reprocessReference the reprocessReference to set
     */
    public void setReprocessReference(String reprocessReference) {
        this.reprocessReference = reprocessReference;
    }

    /**
     * @return the internalReference
     */
    public String getInternalReference() {
        return internalReference;
    }

    /**
     * @param internalReference the internalReference to set
     */
    public void setInternalReference(String internalReference) {
        this.internalReference = internalReference;
    }

    /**
     * @return the vpcmerchant
     */
    public String getVpcmerchant() {
        return vpcmerchant;
    }

    /**
     * @param vpcmerchant the vpcmerchant to set
     */
    public void setVpcmerchant(String vpcmerchant) {
        this.vpcmerchant = vpcmerchant;
    }

    /**
     * @return the maskedExpiry
     */
    public String getMaskedExpiry() {
        return maskedExpiry;
    }

    /**
     * @param maskedExpiry the maskedExpiry to set
     */
    public void setMaskedExpiry(String maskedExpiry) {
        this.maskedExpiry = maskedExpiry;
    }

    /**
     * @return the maskedCvv
     */
    public String getMaskedCvv() {
        return maskedCvv;
    }

    /**
     * @param maskedCvv the maskedCvv to set
     */
    public void setMaskedCvv(String maskedCvv) {
        this.maskedCvv = maskedCvv;
    }

    /**
     * @return the batchnumber
     */
    public String getBatchnumber() {
        return batchnumber;
    }

    /**
     * @param batchnumber the batchnumber to set
     */
    public void setBatchnumber(String batchnumber) {
        this.batchnumber = batchnumber;
    }

    /**
     * @return the sourceAccount
     */
    public String getSourceAccount() {
        return sourceAccount;
    }

    /**
     * @param sourceAccount the sourceAccount to set
     */
    public void setSourceAccount(String sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    /**
     * @return the senderName
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * @param senderName the senderName to set
     */
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    /**
     * @return the sourceBankCode
     */
    public String getSourceBankCode() {
        return sourceBankCode;
    }

    /**
     * @param sourceBankCode the sourceBankCode to set
     */
    public void setSourceBankCode(String sourceBankCode) {
        this.sourceBankCode = sourceBankCode;
    }

    /**
     * @return the recipientName
     */
    public String getRecipientName() {
        return recipientName;
    }

    /**
     * @param recipientName the recipientName to set
     */
    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    /**
     * @return the customerPhone
     */
    public String getCustomerPhone() {
        return customerPhone;
    }

    /**
     * @param customerPhone the customerPhone to set
     */
    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    /**
     * @return the recipientAccount
     */
    public String getRecipientAccount() {
        return recipientAccount;
    }

    /**
     * @param recipientAccount the recipientAccount to set
     */
    public void setRecipientAccount(String recipientAccount) {
        this.recipientAccount = recipientAccount;
    }

    /**
     * @return the paymentReference
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * @param paymentReference the paymentReference to set
     */
    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    /**
     * @return the recipientBankCode
     */
    public String getRecipientBankCode() {
        return recipientBankCode;
    }

    /**
     * @param recipientBankCode the recipientBankCode to set
     */
    public void setRecipientBankCode(String recipientBankCode) {
        this.recipientBankCode = recipientBankCode;
    }

    /**
     * @return the paymentId
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the postResponseCode
     */
    public String getPostResponseCode() {
        return postResponseCode;
    }

    /**
     * @param postResponseCode the postResponseCode to set
     */
    public void setPostResponseCode(String postResponseCode) {
        this.postResponseCode = postResponseCode;
    }

    /**
     * @return the fee
     */
    public BigDecimal getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the merchantPhone
     */
    public String getMerchantPhone() {
        return merchantPhone;
    }

    /**
     * @param merchantPhone the merchantPhone to set
     */
    public void setMerchantPhone(String merchantPhone) {
        this.merchantPhone = merchantPhone;
    }
    
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private long id;
    private String rrn;
    private String provider;
    private String merchantId;
    private String transactionCurrency;
    private String transactionCountry;
    private BigDecimal amount;
    private String merchantTxnReference;
    private String flwTxnReference;
    private String transactionType;
    private String authenticationModel;
    private String transactionNarration;
    private String cardCountry;
    private String cardMask;
    private String datetime;
    private String responseCode;
    private String responseMessage;
    private String transactionData; // This is an array
    private String processorReference; // This is an array
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date settledOn;
    @Column(name = "cardscheme")
    private String cardScheme;
    private String settledBy;
    private Boolean settled;
    private String settlementId;
    private Utility.SettlementState settlementState;
    private String disburseResponseCode;
    private String disburseResponseMessage;
    private int disburseRetryCount;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastRetried;
    private String lastRetriedBy;
    private String reprocessedResponseCode;
    private String reprocessReference;
    private String internalReference;
    private String vpcmerchant;
    private String maskedExpiry;
    private String maskedCvv;
    private String batchnumber;
    private String sourceAccount;
    private String senderName;
    private String sourceBankCode;
    private String recipientName;
    private String customerPhone;
    private String recipientAccount;
    private String paymentReference;
    private String recipientBankCode;
    private String paymentId;
    private String postResponseCode;
    private BigDecimal fee;
    private String customerName;
    private String merchantCode;
    private String merchantPhone;
    private String transactionNumber;
    private String merchantName;
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;
    
    
}

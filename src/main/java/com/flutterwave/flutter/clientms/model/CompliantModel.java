/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.CompanyType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "compliant")
@Entity
public class CompliantModel implements Serializable {

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the industry
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * @param industry the industry to set
     */
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the registeredName
     */
    public String getRegisteredName() {
        return registeredName;
    }

    /**
     * @param registeredName the registeredName to set
     */
    public void setRegisteredName(String registeredName) {
        this.registeredName = registeredName;
    }

    /**
     * @return the registeredNumber
     */
    public String getRegisteredNumber() {
        return registeredNumber;
    }

    /**
     * @param registeredNumber the registeredNumber to set
     */
    public void setRegisteredNumber(String registeredNumber) {
        this.registeredNumber = registeredNumber;
    }

    /**
     * @return the tradingName
     */
    public String getTradingName() {
        return tradingName;
    }

    /**
     * @param tradingName the tradingName to set
     */
    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    /**
     * @return the registeredAddress
     */
    public String getRegisteredAddress() {
        return registeredAddress;
    }

    /**
     * @param registeredAddress the registeredAddress to set
     */
    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    /**
     * @return the operationalAddress
     */
    public String getOperationalAddress() {
        return operationalAddress;
    }

    /**
     * @param operationalAddress the operationalAddress to set
     */
    public void setOperationalAddress(String operationalAddress) {
        this.operationalAddress = operationalAddress;
    }

    /**
     * @return the companyType
     */
    public CompanyType getCompanyType() {
        return companyType;
    }

    /**
     * @param companyType the companyType to set
     */
    public void setCompanyType(CompanyType companyType) {
        this.companyType = companyType;
    }

    /**
     * @return the companyRegDocumentPath
     */
    public String getCompanyRegDocumentPath() {
        return companyRegDocumentPath;
    }

    /**
     * @param companyRegDocumentPath the companyRegDocumentPath to set
     */
    public void setCompanyRegDocumentPath(String companyRegDocumentPath) {
        this.companyRegDocumentPath = companyRegDocumentPath;
    }

    /**
     * @return the directorIdPath
     */
    public String getDirectorIdPath() {
        return directorIdPath;
    }

    /**
     * @param directorIdPath the directorIdPath to set
     */
    public void setDirectorIdPath(String directorIdPath) {
        this.directorIdPath = directorIdPath;
    }

    /**
     * @return the operatingLicencePath
     */
    public String getOperatingLicencePath() {
        return operatingLicencePath;
    }

    /**
     * @param operatingLicencePath the operatingLicencePath to set
     */
    public void setOperatingLicencePath(String operatingLicencePath) {
        this.operatingLicencePath = operatingLicencePath;
    }

    /**
     * @return the amlPolicyPath
     */
    public String getAmlPolicyPath() {
        return amlPolicyPath;
    }

    /**
     * @param amlPolicyPath the amlPolicyPath to set
     */
    public void setAmlPolicyPath(String amlPolicyPath) {
        this.amlPolicyPath = amlPolicyPath;
    }

    /**
     * @return the shareHolder1
     */
    public String getShareHolder1() {
        return shareHolder1;
    }

    /**
     * @param shareHolder1 the shareHolder1 to set
     */
    public void setShareHolder1(String shareHolder1) {
        this.shareHolder1 = shareHolder1;
    }

    /**
     * @return the shareHolder2
     */
    public String getShareHolder2() {
        return shareHolder2;
    }

    /**
     * @param shareHolder2 the shareHolder2 to set
     */
    public void setShareHolder2(String shareHolder2) {
        this.shareHolder2 = shareHolder2;
    }

    /**
     * @return the dateofIncorporation
     */
    public String getDateofIncorporation() {
        return dateofIncorporation;
    }

    /**
     * @param dateofIncorporation the dateofIncorporation to set
     */
    public void setDateofIncorporation(String dateofIncorporation) {
        this.dateofIncorporation = dateofIncorporation;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String registeredName;
    private String registeredNumber;
    private String dateofIncorporation;
    private String tradingName;
    private String registeredAddress;
    private String operationalAddress;
    private CompanyType companyType;
    private String companyRegDocumentPath;
    private String directorIdPath;
    private String operatingLicencePath;
    private String amlPolicyPath;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    
    // share holder's information
    private String shareHolder1;
    private String shareHolder2;
    
    private String industry;
    
    // contact information
    private String name;
    private String telephone;
    private String email;
    private String address;
}

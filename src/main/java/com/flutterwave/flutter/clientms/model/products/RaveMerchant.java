/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.products;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "Accounts")
@Entity
public class RaveMerchant implements Serializable {

    /**
     * @return the parent
     */
    public RaveMerchant getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(RaveMerchant parent) {
        this.parent = parent;
    }

    /**
     * @return the subscription
     */
    public RaveSubscription getSubscription() {
        return subscription;
    }

    /**
     * @param subscription the subscription to set
     */
    public void setSubscription(RaveSubscription subscription) {
        this.subscription = subscription;
    }
    
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the businessName
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     * @param businessName the businessName to set
     */
    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    /**
     * @return the contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * @param contactPerson the contactPerson to set
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the bearsfee
     */
    public String getBearsfee() {
        return bearsfee;
    }

    /**
     * @param bearsfee the bearsfee to set
     */
    public void setBearsfee(String bearsfee) {
        this.bearsfee = bearsfee;
    }

//    /**
//     * @return the feeMarkup
//     */
//    public String getFeeMarkup() {
//        return feeMarkup;
//    }
//
//    /**
//     * @param feeMarkup the feeMarkup to set
//     */
//    public void setFeeMarkup(String feeMarkup) {
//        this.feeMarkup = feeMarkup;
//    }

    /**
     * @return the markupFee
     */
    public String getMarkupFee() {
        return markupFee;
    }

    /**
     * @param markupFee the markupFee to set
     */
    public void setMarkupFee(String markupFee) {
        this.markupFee = markupFee;
    }

    /**
     * @return the live
     */
    public boolean isLive() {
        return live;
    }

    /**
     * @param live the live to set
     */
    public void setLive(boolean live) {
        this.live = live;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the deletedAt
     */
    public Date getDeletedAt() {
        return deletedAt;
    }

    /**
     * @param deletedAt the deletedAt to set
     */
    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }


    /**
     * @return the liveApproved
     */
    public boolean isLiveApproved() {
        return liveApproved;
    }

    /**
     * @param liveApproved the liveApproved to set
     */
    public void setLiveApproved(boolean liveApproved) {
        this.liveApproved = liveApproved;
    }

    /**
     * @return the vpcmerchant
     */
    public String getVpcmerchant() {
        return vpcmerchant;
    }

    /**
     * @param vpcmerchant the vpcmerchant to set
     */
    public void setVpcmerchant(String vpcmerchant) {
        this.vpcmerchant = vpcmerchant;
    }

    /**
     * @return the UUID
     */
    public String getUUID() {
        return UUID;
    }

    /**
     * @param UUID the UUID to set
     */
    public void setUUID(String UUID) {
        this.UUID = UUID;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "business_name")
    private String businessName;
    @Column(name = "contact_person")
    private String contactPerson;
    private String  country;
    private String bearsfee;
//    private String feeMarkup;
    private String markupFee;
    @ManyToOne
    @JoinColumn(name = "parent_account_id")
    private RaveMerchant parent;
    @Column(name = "is_live")
    private boolean live;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @ManyToOne
    @JoinColumn(name = "subscription_id")
    private RaveSubscription subscription;
    @Column(name = "is_live_approved")
    private boolean liveApproved;
    private String vpcmerchant;
    private String UUID;
    
    public GenericMerchant getGeneric(){
        
        GenericMerchant genericMerchant = new GenericMerchant();
        genericMerchant.setCountry(country);
        genericMerchant.setId(id);
        genericMerchant.setName(businessName);
        genericMerchant.setCreatedAt(createdAt);
        genericMerchant.setUpdatedAt(updatedAt);
        genericMerchant.setDeletedAt(deletedAt);
        genericMerchant.setParent(parent != null ? parent.getGeneric() : null);
        genericMerchant.setLiveApproved(liveApproved);
        genericMerchant.setLive(live == false);
        genericMerchant.setEmail(UUID);
//        genericMerchant.setPhone(UUID);
        
        return genericMerchant;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.maker;

import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.util.Status;
import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public abstract class BaseMaker {
    
    public abstract User getApprovedBy();
    public abstract void setApprovedBy(User user);
    public abstract Date getApprovedOn();
    public abstract void setApprovedOn(Date approvedOn);
    // This is the id of the model to be modified
    public abstract long getModelId();
    public abstract void setModelId(long modelId);
    public abstract Status getStatus();
    public abstract void setStatus(Status status);
    public abstract void setRejectionReason(String reason);
    public abstract String getRejectionReason();
    
}

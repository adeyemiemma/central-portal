/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.clientms.util.RaveUtil;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "\"txwarehouse\"")
@Entity
public class RaveTransactionWH implements Serializable {

    /**
     * @return the parent
     */
    public long getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(long parent) {
        this.parent = parent;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the deletedAt
     */
    public Date getDeletedAt() {
        return deletedAt;
    }

    /**
     * @param deletedAt the deletedAt to set
     */
    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * @return the customerid
     */
    public String getCustomerid() {
        return customerid;
    }

    /**
     * @param customerid the customerid to set
     */
    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    /**
     * @return the accountid
     */
    public String getAccountid() {
        return accountid;
    }

    /**
     * @param accountid the accountid to set
     */
    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the cardCountry
     */
    public String getCardCountry() {
        return cardCountry;
    }

    /**
     * @param cardCountry the cardCountry to set
     */
    public void setCardCountry(String cardCountry) {
        this.cardCountry = cardCountry;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType the cardType to set
     */
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the cardName
     */
    public String getCardName() {
        return cardName;
    }

    /**
     * @param cardName the cardName to set
     */
    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    /**
     * @return the merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the vbvmessage
     */
    public String getVbvmessage() {
        return vbvmessage;
    }

    /**
     * @param vbvmessage the vbvmessage to set
     */
    public void setVbvmessage(String vbvmessage) {
        this.vbvmessage = vbvmessage;
    }

    /**
     * @return the vbvcode
     */
    public String getVbvcode() {
        return vbvcode;
    }

    /**
     * @param vbvcode the vbvcode to set
     */
    public void setVbvcode(String vbvcode) {
        this.vbvcode = vbvcode;
    }

    /**
     * @return the chargeMessage
     */
    public String getChargeMessage() {
        return chargeMessage;
    }

    /**
     * @param chargeMessage the chargeMessage to set
     */
    public void setChargeMessage(String chargeMessage) {
        this.chargeMessage = chargeMessage;
    }

    /**
     * @return the chargeCode
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * @param chargeCode the chargeCode to set
     */
    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * @return the cardFirst6
     */
    public String getCardFirst6() {
        return cardFirst6;
    }

    /**
     * @param cardFirst6 the cardFirst6 to set
     */
    public void setCardFirst6(String cardFirst6) {
        this.cardFirst6 = cardFirst6;
    }

    /**
     * @return the cardLast4
     */
    public String getCardLast4() {
        return cardLast4;
    }

    /**
     * @param cardLast4 the cardLast4 to set
     */
    public void setCardLast4(String cardLast4) {
        this.cardLast4 = cardLast4;
    }

    /**
     * @return the id 
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the transactionReference
     */
    public String getTransactionReference() {
        return transactionReference;
    }

    /**
     * @param transactionReference the transactionReference to set
     */
    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    /**
     * @return the flutterReference
     */
    public String getFlutterReference() {
        return flutterReference;
    }

    /**
     * @param flutterReference the flutterReference to set
     */
    public void setFlutterReference(String flutterReference) {
        this.flutterReference = flutterReference;
    }

//    /**
//     * @return the transactionType
//     */
//    public String getTransactionType() {
//        return transactionType;
//    }
//
//    /**
//     * @param transactionType the transactionType to set
//     */
//    public void setTransactionType(String transactionType) {
//        this.transactionType = transactionType;
//    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the appfee
     */
    public Double getAppfee() {
        return appfee;
    }

    /**
     * @param appfee the appfee to set
     */
    public void setAppfee(Double appfee) {
        this.appfee = appfee;
    }

    /**
     * @return the merchantfee
     */
    public Double getMerchantfee() {
        return merchantfee;
    }

    /**
     * @param merchantfee the merchantfee to set
     */
    public void setMerchantfee(Double merchantfee) {
        this.merchantfee = merchantfee;
    }

    /**
     * @return the merchantbearsfee
     */
    public int getMerchantbearsfee() {
        return merchantbearsfee;
    }

    /**
     * @param merchantbearsfee the merchantbearsfee to set
     */
    public void setMerchantbearsfee(int merchantbearsfee) {
        this.merchantbearsfee = merchantbearsfee;
    }

    /**
     * @return the chargedAmount
     */
    public Double getChargedAmount() {
        return chargedAmount;
    }

    /**
     * @param chargedAmount the chargedAmount to set
     */
    public void setChargedAmount(Double chargedAmount) {
        this.chargedAmount = chargedAmount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the paymentEntity
     */
    public String getPaymentEntity() {
        return paymentEntity;
    }

    /**
     * @param paymentEntity the paymentEntity to set
     */
    public void setPaymentEntity(String paymentEntity) {
        this.paymentEntity = paymentEntity;
    }

    /**
     * @return the paymentId
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the fraudStatus
     */
    public String getFraudStatus() {
        return fraudStatus;
    }

    /**
     * @param fraudStatus the fraudStatus to set
     */
    public void setFraudStatus(String fraudStatus) {
        this.fraudStatus = fraudStatus;
    }

    /**
     * @return the chargeType
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * @param chargeType the chargeType to set
     */
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    /**
     * @return the cycle
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * @param cycle the cycle to set
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "txref")
    private String transactionReference;
    @Column(name = "flwref")
    private String flutterReference;
//    @Column(name = "transaction_type")
//    private String transactionType;
//    @Column(name = "transaction_processor")
//    private String transactionProcessor;
    private String status;
    private String ip;
    @Column(columnDefinition = "text")
    private String narration;
    private Double amount;
    private Double appfee;
    private Double merchantfee;
    private int merchantbearsfee;
    @Column(name = "chargedamount")
    private Double chargedAmount;
    @Column(name = "currency")
    private String currency;
//    @Column(name = "system_type")
//    private String systemType;
    @Column(name = "paymenttype")
    private String paymentEntity;
    @Column(name = "paymentid")
    private String paymentId;
    @Column(name = "fraudstatus")
    private String fraudStatus;
    @Column(name = "chargetype")
    private String chargeType;
//    @Column(name = "is_live")
//    private Boolean live;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    private String customerid;
    private String accountid;
    @Column(name = "paccountbank")
    private String bankCode;
    @Column(name = "paccountnumber")
    private String accountNumber;
    @Column(name = "pcardcountry")
    private String cardCountry;
    @Column(name="paccountbankname")
    private String bankName;
    @Column(name="pcardtype")
    private String cardType;
    @Column(name="pcardname")
    private String cardName;
    @Column(name = "acctbusinessname")
    private String merchant;
    private String vbvmessage;
    private String vbvcode;
    @Column(name = "chargemessage")
    private String chargeMessage;
    @Column(name = "chargecode")
    private String chargeCode;
    @Column(name = "\"cycle\"")
    private String cycle;
    @Column(name = "pcard6")
    private String cardFirst6;
    @Column(name = "pcardl4")
    private String cardLast4;
    @Column(name = "acctcountry")
    private String country;
    @Column(name = "acctparent")
    private long parent;
    
    public GenericTransaction getGeneric(){
        
        GenericTransaction genericTransaction = new GenericTransaction();
        genericTransaction.setAmount(getAmount());
        genericTransaction.setFee(getAppfee());
        genericTransaction.setCreatedAt(getCreatedAt());
        genericTransaction.setCurrency(getCurrency());
        genericTransaction.setDeletedAt(getDeletedAt());
        genericTransaction.setFlutterReference(getFlutterReference());
        genericTransaction.setMerchant(getMerchant());
        genericTransaction.setStatus(getStatus());
        genericTransaction.setTransactionType(getPaymentEntity());
        genericTransaction.setUpdatedAt(getUpdatedAt());
        genericTransaction.setTransactionReference(getTransactionReference());
        genericTransaction.setId(getId());
        genericTransaction.setChargeResponseMessage(getChargeMessage());
        genericTransaction.setAccountNo(getAccountNumber());
        genericTransaction.setProductName("Rave");
        genericTransaction.setVbvResponseCode(getVbvcode());
        genericTransaction.setVbvResponseMessage(getVbvmessage());
        genericTransaction.setChargeResponseCode(getChargeCode());
        genericTransaction.setCardCountry(getCardCountry());
        
        
        return genericTransaction;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.recon;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "flutter_transaction")
@Entity
public class FlutterTransaction implements Serializable {

    /**
     * @return the merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the trnxamount
     */
    public double getTrnxamount() {
        return trnxamount;
    }

    /**
     * @param trnxamount the trnxamount to set
     */
    public void setTrnxamount(double trnxamount) {
        this.trnxamount = trnxamount;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the currencyin
     */
    public String getCurrencyin() {
        return currencyin;
    }

    /**
     * @param currencyin the currencyin to set
     */
    public void setCurrencyin(String currencyin) {
        this.currencyin = currencyin;
    }

    /**
     * @return the timein
     */
    public String getTimein() {
        return timein;
    }

    /**
     * @param timein the timein to set
     */
    public void setTimein(String timein) {
        this.timein = timein;
    }

    /**
     * @return the vpcinmerchtxnref
     */
    public String getVpcinmerchtxnref() {
        return vpcinmerchtxnref;
    }

    /**
     * @param vpcinmerchtxnref the vpcinmerchtxnref to set
     */
    public void setVpcinmerchtxnref(String vpcinmerchtxnref) {
        this.vpcinmerchtxnref = vpcinmerchtxnref;
    }

    /**
     * @return the vpcintransactionno
     */
    public String getVpcintransactionno() {
        return vpcintransactionno;
    }

    /**
     * @param vpcintransactionno the vpcintransactionno to set
     */
    public void setVpcintransactionno(String vpcintransactionno) {
        this.vpcintransactionno = vpcintransactionno;
    }

    /**
     * @return the vpcinorderinfo
     */
    public String getVpcinorderinfo() {
        return vpcinorderinfo;
    }

    /**
     * @param vpcinorderinfo the vpcinorderinfo to set
     */
    public void setVpcinorderinfo(String vpcinorderinfo) {
        this.vpcinorderinfo = vpcinorderinfo;
    }

    /**
     * @return the vpcavsresultcode
     */
    public String getVpcavsresultcode() {
        return vpcavsresultcode;
    }

    /**
     * @param vpcavsresultcode the vpcavsresultcode to set
     */
    public void setVpcavsresultcode(String vpcavsresultcode) {
        this.vpcavsresultcode = vpcavsresultcode;
    }

    /**
     * @return the vpcacqavsrespcode
     */
    public String getVpcacqavsrespcode() {
        return vpcacqavsrespcode;
    }

    /**
     * @param vpcacqavsrespcode the vpcacqavsrespcode to set
     */
    public void setVpcacqavsrespcode(String vpcacqavsrespcode) {
        this.vpcacqavsrespcode = vpcacqavsrespcode;
    }

    /**
     * @return the vpcacqcscrespcode
     */
    public String getVpcacqcscrespcode() {
        return vpcacqcscrespcode;
    }

    /**
     * @param vpcacqcscrespcode the vpcacqcscrespcode to set
     */
    public void setVpcacqcscrespcode(String vpcacqcscrespcode) {
        this.vpcacqcscrespcode = vpcacqcscrespcode;
    }

    /**
     * @return the vpcacqresponsecode
     */
    public String getVpcacqresponsecode() {
        return vpcacqresponsecode;
    }

    /**
     * @param vpcacqresponsecode the vpcacqresponsecode to set
     */
    public void setVpcacqresponsecode(String vpcacqresponsecode) {
        this.vpcacqresponsecode = vpcacqresponsecode;
    }

    /**
     * @return the vpcamount
     */
    public String getVpcamount() {
        return vpcamount;
    }

    /**
     * @param vpcamount the vpcamount to set
     */
    public void setVpcamount(String vpcamount) {
        this.vpcamount = vpcamount;
    }

    /**
     * @return the vpcauthorizeid
     */
    public String getVpcauthorizeid() {
        return vpcauthorizeid;
    }

    /**
     * @param vpcauthorizeid the vpcauthorizeid to set
     */
    public void setVpcauthorizeid(String vpcauthorizeid) {
        this.vpcauthorizeid = vpcauthorizeid;
    }

    /**
     * @return the vpcbatchno
     */
    public String getVpcbatchno() {
        return vpcbatchno;
    }

    /**
     * @param vpcbatchno the vpcbatchno to set
     */
    public void setVpcbatchno(String vpcbatchno) {
        this.vpcbatchno = vpcbatchno;
    }

    /**
     * @return the vpccscresultcode
     */
    public String getVpccscresultcode() {
        return vpccscresultcode;
    }

    /**
     * @param vpccscresultcode the vpccscresultcode to set
     */
    public void setVpccscresultcode(String vpccscresultcode) {
        this.vpccscresultcode = vpccscresultcode;
    }

    /**
     * @return the vpccard
     */
    public String getVpccard() {
        return vpccard;
    }

    /**
     * @param vpccard the vpccard to set
     */
    public void setVpccard(String vpccard) {
        this.vpccard = vpccard;
    }

    /**
     * @return the vpccommand
     */
    public String getVpccommand() {
        return vpccommand;
    }

    /**
     * @param vpccommand the vpccommand to set
     */
    public void setVpccommand(String vpccommand) {
        this.vpccommand = vpccommand;
    }

    /**
     * @return the vpccurrency
     */
    public String getVpccurrency() {
        return vpccurrency;
    }

    /**
     * @param vpccurrency the vpccurrency to set
     */
    public void setVpccurrency(String vpccurrency) {
        this.vpccurrency = vpccurrency;
    }

    /**
     * @return the vpcmerchtxnref
     */
    public String getVpcmerchtxnref() {
        return vpcmerchtxnref;
    }

    /**
     * @param vpcmerchtxnref the vpcmerchtxnref to set
     */
    public void setVpcmerchtxnref(String vpcmerchtxnref) {
        this.vpcmerchtxnref = vpcmerchtxnref;
    }

    /**
     * @return the vpcmerchant
     */
    public String getVpcmerchant() {
        return vpcmerchant;
    }

    /**
     * @param vpcmerchant the vpcmerchant to set
     */
    public void setVpcmerchant(String vpcmerchant) {
        this.vpcmerchant = vpcmerchant;
    }

    /**
     * @return the vpcmessage
     */
    public String getVpcmessage() {
        return vpcmessage;
    }

    /**
     * @param vpcmessage the vpcmessage to set
     */
    public void setVpcmessage(String vpcmessage) {
        this.vpcmessage = vpcmessage;
    }

    /**
     * @return the vpcorderinfo
     */
    public String getVpcorderinfo() {
        return vpcorderinfo;
    }

    /**
     * @param vpcorderinfo the vpcorderinfo to set
     */
    public void setVpcorderinfo(String vpcorderinfo) {
        this.vpcorderinfo = vpcorderinfo;
    }

    /**
     * @return the vpcreceiptno
     */
    public String getVpcreceiptno() {
        return vpcreceiptno;
    }

    /**
     * @param vpcreceiptno the vpcreceiptno to set
     */
    public void setVpcreceiptno(String vpcreceiptno) {
        this.vpcreceiptno = vpcreceiptno;
    }

    /**
     * @return the vpctransactionno
     */
    public String getVpctransactionno() {
        return vpctransactionno;
    }

    /**
     * @param vpctransactionno the vpctransactionno to set
     */
    public void setVpctransactionno(String vpctransactionno) {
        this.vpctransactionno = vpctransactionno;
    }

    /**
     * @return the vpctxnresponsecode
     */
    public String getVpctxnresponsecode() {
        return vpctxnresponsecode;
    }

    /**
     * @param vpctxnresponsecode the vpctxnresponsecode to set
     */
    public void setVpctxnresponsecode(String vpctxnresponsecode) {
        this.vpctxnresponsecode = vpctxnresponsecode;
    }

    /**
     * @return the mask
     */
    public String getMask() {
        return mask;
    }

    /**
     * @param mask the mask to set
     */
    public void setMask(String mask) {
        this.mask = mask;
    }

    /**
     * @return the uniqueref
     */
    public String getUniqueref() {
        return uniqueref;
    }

    /**
     * @param uniqueref the uniqueref to set
     */
    public void setUniqueref(String uniqueref) {
        this.uniqueref = uniqueref;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(unique = true)
    private String uniqueref;
    private String merchant;
    private double trnxamount;
    private String description;
    private String currencyin;
    private String timein;
    private String vpcinmerchtxnref;
    private String vpcintransactionno;
    private String vpcinorderinfo;
    private String vpcavsresultcode;
    private String vpcacqavsrespcode;
    private String vpcacqcscrespcode;
    private String vpcacqresponsecode;
    private String vpcamount;
    private String vpcauthorizeid;
    private String vpcbatchno;
    private String vpccscresultcode;
    private String vpccard;
    private String vpccommand;
    private String vpccurrency;
    private String vpcmerchtxnref;
    private String vpcmerchant;
    private String vpcmessage;
    private String vpcorderinfo;
//    @Column(unique = true)
    private String vpcreceiptno;
//    @Column(unique = true)
    private String vpctransactionno;
    private String vpctxnresponsecode;
    private String mask;
}

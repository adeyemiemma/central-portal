/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
//@Table("flutterwave_merchant_core")
//@Entity
public class FlutterwaveCoreMerchant implements Serializable {

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the merchantEmail
     */
    public String getMerchantEmail() {
        return merchantEmail;
    }

    /**
     * @param merchantEmail the merchantEmail to set
     */
    public void setMerchantEmail(String merchantEmail) {
        this.merchantEmail = merchantEmail;
    }

    /**
     * @return the merchantAddress
     */
    public String getMerchantAddress() {
        return merchantAddress;
    }

    /**
     * @param merchantAddress the merchantAddress to set
     */
    public void setMerchantAddress(String merchantAddress) {
        this.merchantAddress = merchantAddress;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the creatdOn
     */
    public Date getCreatdOn() {
        return creatdOn;
    }

    /**
     * @param creatdOn the creatdOn to set
     */
    public void setCreatdOn(Date creatdOn) {
        this.creatdOn = creatdOn;
    }

    /**
     * @return the rcNumber
     */
    public String getRcNumber() {
        return rcNumber;
    }

    /**
     * @param rcNumber the rcNumber to set
     */
    public void setRcNumber(String rcNumber) {
        this.rcNumber = rcNumber;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the bankExtraInfo
     */
    public String getBankExtraInfo() {
        return bankExtraInfo;
    }

    /**
     * @param bankExtraInfo the bankExtraInfo to set
     */
    public void setBankExtraInfo(String bankExtraInfo) {
        this.bankExtraInfo = bankExtraInfo;
    }
        
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String merchantId;
    private String merchantName;
    private String merchantEmail;
    private String merchantAddress;
    private String status;
    private String accountNo;
    private String bankCode;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creatdOn;
    private String rcNumber;
    private String bankName;
    private String bankExtraInfo;
    
}

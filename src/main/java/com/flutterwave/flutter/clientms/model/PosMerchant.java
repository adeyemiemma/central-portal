/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.PosMerchantCategory;
import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "mpos_merchant")
@Entity
public class PosMerchant implements Serializable {

    /**
     * @return the subAccount
     */
    public String getSubAccount() {
        return subAccount;
    }

    /**
     * @param subAccount the subAccount to set
     */
    public void setSubAccount(String subAccount) {
        this.subAccount = subAccount;
    }

    /**
     * @return the notificationUrl
     */
    public String getNotificationUrl() {
        return notificationUrl;
    }

    /**
     * @param notificationUrl the notificationUrl to set
     */
    public void setNotificationUrl(String notificationUrl) {
        this.notificationUrl = notificationUrl;
    }

    /**
     * @return the category
     */
    public PosMerchantCategory getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(PosMerchantCategory category) {
        this.category = category;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the feeCap
     */
    public double getFeeCap() {
        return feeCap;
    }

    /**
     * @param feeCap the feeCap to set
     */
    public void setFeeCap(double feeCap) {
        this.feeCap = feeCap;
    }

    /**
     * @return the feeType
     */
    public Utility.FeeType getFeeType() {
        return feeType;
    }

    /**
     * @param feeType the feeType to set
     */
    public void setFeeType(Utility.FeeType feeType) {
        this.feeType = feeType;
    }

    /**
     * @return the flatFee
     */
    public double getFlatFee() {
        return flatFee;
    }

    /**
     * @param flatFee the flatFee to set
     */
    public void setFlatFee(double flatFee) {
        this.flatFee = flatFee;
    }

    /**
     * @return the percentageFee
     */
    public double getPercentageFee() {
        return percentageFee;
    }

    /**
     * @param percentageFee the percentageFee to set
     */
    public void setPercentageFee(double percentageFee) {
        this.percentageFee = percentageFee;
    }

    /**
     * @return the relationshipMan
     */
    public String getRelationshipMan() {
        return relationshipMan;
    }

    /**
     * @param relationshipMan the relationshipMan to set
     */
    public void setRelationshipMan(String relationshipMan) {
        this.relationshipMan = relationshipMan;
    }

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * @param accountName the accountName to set
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * @return the parent
     */
    public PosMerchant getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(PosMerchant parent) {
        this.parent = parent;
    }

    /**
     * @return the posId
     */
    public String getPosId() {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(String posId) {
        this.posId = posId;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String merchantId; // This is the parent that own the merchant 
    @Column(unique = true)
    private String merchantCode;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    private String phone;
    private String email;
//    private String location;
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedOn;
    private String address;
    private String createdBy;
    private String posId;
    @ManyToOne
    private PosMerchant parent;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType feeType;
    private double flatFee;
    private double percentageFee;
    private String relationshipMan;
    private String accountNo;
    private String bankName;
    private String accountName;
    private double feeCap;
    private String street;
    private String state;
    private String city;
    private boolean enabled = true;
    private PosMerchantCategory category = PosMerchantCategory.GENERAL;
    private String notificationUrl;
    private String subAccount;
}

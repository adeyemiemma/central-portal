/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.products;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Cacheable
@Table(name = "users")
@Entity
public class CoreMerchant implements Serializable {

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the md5ID
     */
    public String getMd5ID() {
        return md5ID;
    }

    /**
     * @param md5ID the md5ID to set
     */
    public void setMd5ID(String md5ID) {
        this.md5ID = md5ID;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the merchantID
     */
    public String getMerchantID() {
        return merchantID;
    }

    /**
     * @param merchantID the merchantID to set
     */
    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the companyname
     */
    public String getCompanyname() {
        return companyname;
    }

    /**
     * @param companyname the companyname to set
     */
    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the userlevel
     */
    public int getUserlevel() {
        return userlevel;
    }

    /**
     * @param userlevel the userlevel to set
     */
    public void setUserlevel(int userlevel) {
        this.userlevel = userlevel;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the activationcode
     */
    public String getActivationcode() {
        return activationcode;
    }

    /**
     * @param activationcode the activationcode to set
     */
    public void setActivationcode(String activationcode) {
        this.activationcode = activationcode;
    }

    /**
     * @return the validationHash
     */
    public String getValidationHash() {
        return validationHash;
    }

    /**
     * @param validationHash the validationHash to set
     */
    public void setValidationHash(String validationHash) {
        this.validationHash = validationHash;
    }

    /**
     * @return the live
     */
    public boolean isLive() {
        return live;
    }

    /**
     * @param live the live to set
     */
    public void setLive(boolean live) {
        this.live = live;
    }

    /**
     * @return the livefixed
     */
    public boolean isLivefixed() {
        return livefixed;
    }

    /**
     * @param livefixed the livefixed to set
     */
    public void setLivefixed(boolean livefixed) {
        this.livefixed = livefixed;
    }

    /**
     * @return the liveToken
     */
    public String getLiveToken() {
        return liveToken;
    }

    /**
     * @param liveToken the liveToken to set
     */
    public void setLiveToken(String liveToken) {
        this.liveToken = liveToken;
    }

    /**
     * @return the liveApikey
     */
    public String getLiveApikey() {
        return liveApikey;
    }

    /**
     * @param liveApikey the liveApikey to set
     */
    public void setLiveApikey(String liveApikey) {
        this.liveApikey = liveApikey;
    }

    /**
     * @return the testToken
     */
    public String getTestToken() {
        return testToken;
    }

    /**
     * @param testToken the testToken to set
     */
    public void setTestToken(String testToken) {
        this.testToken = testToken;
    }

    /**
     * @return the testApikey
     */
    public String getTestApikey() {
        return testApikey;
    }

    /**
     * @param testApikey the testApikey to set
     */
    public void setTestApikey(String testApikey) {
        this.testApikey = testApikey;
    }

    /**
     * @return the owner
     */
    public int getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(int owner) {
        this.owner = owner;
    }

    /**
     * @return the ownerID
     */
    public Integer getOwnerID() {
        return ownerID;
    }

    /**
     * @param ownerID the ownerID to set
     */
    public void setOwnerID(Integer ownerID) {
        this.ownerID = ownerID;
    }

    /**
     * @return the permissions
     */
    public int getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(int permissions) {
        this.permissions = permissions;
    }

    /**
     * @return the suspend
     */
    public int getSuspend() {
        return suspend;
    }

    /**
     * @param suspend the suspend to set
     */
    public void setSuspend(int suspend) {
        this.suspend = suspend;
    }

    /**
     * @return the pause
     */
    public int getPause() {
        return pause;
    }

    /**
     * @param pause the pause to set
     */
    public void setPause(int pause) {
        this.pause = pause;
    }

    /**
     * @return the category
     */
    public int getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(int category) {
        this.category = category;
    }

    /**
     * @return the ilimit
     */
    public Integer getIlimit() {
        return ilimit;
    }

    /**
     * @param ilimit the ilimit to set
     */
    public void setIlimit(Integer ilimit) {
        this.ilimit = ilimit;
    }

    /**
     * @return the ifees
     */
    public Double getIfees() {
        return ifees;
    }

    /**
     * @param ifees the ifees to set
     */
    public void setIfees(Double ifees) {
        this.ifees = ifees;
    }

    /**
     * @return the ifeestype
     */
    public String getIfeestype() {
        return ifeestype;
    }

    /**
     * @param ifeestype the ifeestype to set
     */
    public void setIfeestype(String ifeestype) {
        this.ifeestype = ifeestype;
    }

    /**
     * @return the datecreated
     */
    public Date getDatecreated() {
        return datecreated;
    }

    /**
     * @param datecreated the datecreated to set
     */
    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    /**
     * @return the lastlogin
     */
    public Date getLastlogin() {
        return lastlogin;
    }

    /**
     * @param lastlogin the lastlogin to set
     */
    public void setLastlogin(Date lastlogin) {
        this.lastlogin = lastlogin;
    }

    /**
     * @return the refcode
     */
    public String getRefcode() {
        return refcode;
    }

    /**
     * @param refcode the refcode to set
     */
    public void setRefcode(String refcode) {
        this.refcode = refcode;
    }

    /**
     * @return the mpesastatus
     */
    public boolean isMpesastatus() {
        return mpesastatus;
    }

    /**
     * @param mpesastatus the mpesastatus to set
     */
    public void setMpesastatus(boolean mpesastatus) {
        this.mpesastatus = mpesastatus;
    }

    /**
     * @return the mpesabusiness
     */
    public String getMpesabusiness() {
        return mpesabusiness;
    }

    /**
     * @param mpesabusiness the mpesabusiness to set
     */
    public void setMpesabusiness(String mpesabusiness) {
        this.mpesabusiness = mpesabusiness;
    }

    /**
     * @return the mpesacallback
     */
    public String getMpesacallback() {
        return mpesacallback;
    }

    /**
     * @param mpesacallback the mpesacallback to set
     */
    public void setMpesacallback(String mpesacallback) {
        this.mpesacallback = mpesacallback;
    }

    /**
     * @return the referrer
     */
    public String getReferrer() {
        return referrer;
    }

    /**
     * @param referrer the referrer to set
     */
    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    /**
     * @return the ckey
     */
    public String getCkey() {
        return ckey;
    }

    /**
     * @param ckey the ckey to set
     */
    public void setCkey(String ckey) {
        this.ckey = ckey;
    }

    /**
     * @return the ctime
     */
    public String getCtime() {
        return ctime;
    }

    /**
     * @param ctime the ctime to set
     */
    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    /**
     * @return the settlementlocation
     */
    public boolean isSettlementlocation() {
        return settlementlocation;
    }

    /**
     * @param settlementlocation the settlementlocation to set
     */
    public void setSettlementlocation(boolean settlementlocation) {
        this.settlementlocation = settlementlocation;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userID")
    private long id;
    private String md5ID;
    private String username;
    private String email;
    private String password;
    private String merchantID;
    private String firstname;
    private String lastname;
    private String companyname;
    private String phone;
    private int userlevel;
    private boolean active;
    private String activationcode;
    @Column(name = "validation_hash")
    private String validationHash;
    private boolean live;
    private boolean livefixed;
    @Column(name = "live_token")
    private String liveToken;
    @Column(name = "live_apikey")
    private String liveApikey;
    @Column(name = "test_token")
    private String testToken;
    @Column(name = "test_apikey")
    private String testApikey;
    private int owner;
    private Integer ownerID;
    private int permissions;
    private int suspend;
    private int pause;
    private int category;
    private Integer ilimit;
    private Double ifees;
    private String ifeestype;
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreated;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastlogin;
    private String refcode;
    private boolean mpesastatus;
    private String mpesabusiness;
    private String mpesacallback;
    private String referrer;
    private String ckey;
    private String ctime;
    private boolean settlementlocation;
    
    public GenericMerchant getGeneric(){
        
        GenericMerchant genericMerchant = new GenericMerchant();
//        genericMerchant.setCountry(country);
        genericMerchant.setName(companyname);
        genericMerchant.setId(id);
        genericMerchant.setCreatedAt(datecreated);
        genericMerchant.setParent(null);
        genericMerchant.setLiveApproved(live);
        genericMerchant.setLive(live);
        genericMerchant.setMerchantId(merchantID);
        genericMerchant.setActive(active);
        genericMerchant.setEmail(email);
        genericMerchant.setPhone(phone);
        
        return genericMerchant;
    }
    
    @Override
    public boolean equals(Object obj) {
        
        if(obj == null)
            return true;
        
        if(! (obj instanceof CoreMerchant))
            return false;
        
        CoreMerchant m = (CoreMerchant) obj;
        
        return m.getId() == this.getId();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 29 * hash + Objects.hashCode(this.companyname);
        return hash;
    }
}

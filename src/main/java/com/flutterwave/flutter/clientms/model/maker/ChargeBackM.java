/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.maker;

import com.flutterwave.flutter.clientms.model.*;
import com.flutterwave.flutter.clientms.util.ChargeBackStatus;
import com.flutterwave.flutter.clientms.util.Status;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "chargeback_m")
@Entity
public class ChargeBackM extends BaseMaker implements Serializable {

//    /**
//     * @return the responseDate
//     */
//    public Date getResponseDate() {
//        return responseDate;
//    }
//
//    /**
//     * @param responseDate the responseDate to set
//     */
//    public void setResponseDate(Date responseDate) {
//        this.responseDate = responseDate;
//    }
//
//    /**
//     * @return the disputeReason
//     */
//    public String getDisputeReason() {
//        return disputeReason;
//    }
//
//    /**
//     * @param disputeReason the disputeReason to set
//     */
//    public void setDisputeReason(String disputeReason) {
//        this.disputeReason = disputeReason;
//    }
//
//    /**
//     * @return the productMerchantName
//     */
//    public String getProductMerchantName() {
//        return productMerchantName;
//    }
//
//    /**
//     * @param productMerchantName the productMerchantName to set
//     */
//    public void setProductMerchantName(String productMerchantName) {
//        this.productMerchantName = productMerchantName;
//    }

    /**
     * @return the productParentMid
     */
    public Long getProductParentMid() {
        return productParentMid;
    }

    /**
     * @param productParentMid the productParentMid to set
     */
    public void setProductParentMid(Long productParentMid) {
        this.productParentMid = productParentMid;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the productMid
     */
    public long getProductMid() {
        return productMid;
    }

    /**
     * @param productMid the productMid to set
     */
    public void setProductMid(long productMid) {
        this.productMid = productMid;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the chargeBackStatus
     */
    public ChargeBackStatus getChargeBackStatus() {
        return chargeBackStatus;
    }

    /**
     * @param chargeBackStatus the chargeBackStatus to set
     */
    public void setChargeBackStatus(ChargeBackStatus chargeBackStatus) {
        this.chargeBackStatus = chargeBackStatus;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the modelId
     */
    public long getModelId() {
        return modelId;
    }

    /**
     * @param modelId the modelId to set
     */
    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    /**
     * @return the rejectionReason
     */
    public String getRejectionReason() {
        return rejectionReason;
    }

    /**
     * @param rejectionReason the rejectionReason to set
     */
    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    /**
     * @return the amountSettled
     */
    public double getAmountSettled() {
        return amountSettled;
    }

    /**
     * @param amountSettled the amountSettled to set
     */
    public void setAmountSettled(double amountSettled) {
        this.amountSettled = amountSettled;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the flwReference
     */
    public String getFlwReference() {
        return flwReference;
    }

    /**
     * @param flwReference the flwReference to set
     */
    public void setFlwReference(String flwReference) {
        this.flwReference = flwReference;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the authenticationType
     */
    public String getAuthenticationType() {
        return authenticationType;
    }

    /**
     * @param authenticationType the authenticationType to set
     */
    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the approvedBy
     */
    public User getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private Product product;
    private String merchantId;
    private double amount;
    private String currency;
    private String flwReference;
    private String rrn;
    private String authenticationType;
    @ManyToOne
    private User createdBy;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date approvedOn;
    @ManyToOne
    private User approvedBy;
    private ChargeBackStatus chargeBackStatus;
    private double amountSettled;
    private Status status;
    private long modelId;
    private String rejectionReason;
    private long productMid;
    private Long productParentMid;
    private String merchantName;
//    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
//    private Date responseDate;
//    private String disputeReason;
//    private String productMerchantName;
}

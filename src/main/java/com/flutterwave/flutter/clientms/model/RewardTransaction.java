/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "reward_transaction")
@Entity
public class RewardTransaction implements Serializable {

    /**
     * @return the pointAddedOn
     */
    public Date getPointAddedOn() {
        return pointAddedOn;
    }

    /**
     * @param pointAddedOn the pointAddedOn to set
     */
    public void setPointAddedOn(Date pointAddedOn) {
        this.pointAddedOn = pointAddedOn;
    }

    /**
     * @return the point
     */
    public int getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(int point) {
        this.point = point;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the customerId
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the transactionReference
     */
    public String getTransactionReference() {
        return transactionReference;
    }

    /**
     * @param transactionReference the transactionReference to set
     */
    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    /**
     * @return the externalReference
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * @param externalReference the externalReference to set
     */
    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the timein
     */
    public String getTimein() {
        return timein;
    }

    /**
     * @param timein the timein to set
     */
    public void setTimein(String timein) {
        this.timein = timein;
    }

    /**
     * @return the mask
     */
    public String getMask() {
        return mask;
    }

    /**
     * @param mask the mask to set
     */
    public void setMask(String mask) {
        this.mask = mask;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String merchantId;
    private String customerId;
    @Column(unique = true)
    private String transactionReference;
    private String externalReference;
    private double amount;
    private String currency;
    private String narration;
    private String timein;
    private String mask;
    private String responseCode;
    private String responseMessage;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date pointAddedOn;
    private int point;
    
    public static RewardTransaction from(JSONObject jSONObject){
        
        RewardTransaction transaction = new RewardTransaction();
        
        String amount = jSONObject.optString("amount", "0.0");
        try{
        
        if(amount == null)
            amount = jSONObject.optDouble("amount", 0.0) + "";

        amount = amount.replace(",", "");
        
        if(!"null".equalsIgnoreCase(amount))
            transaction.setAmount(new BigDecimal(amount).doubleValue());
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        transaction.setCreatedOn(new Date());
        transaction.setCustomerId(jSONObject.optString("customerid", null));
        transaction.setMerchantId(jSONObject.optString("merchant", null));
        transaction.setCurrency(jSONObject.optString("currency", null));
        transaction.setExternalReference(jSONObject.optString("externalreference", null));
        transaction.setMask(jSONObject.optString("mask", null));
        transaction.setNarration(jSONObject.optString("narration", null));
        transaction.setResponseCode(jSONObject.optString("responsecode", null));
        transaction.setResponseMessage(jSONObject.optString("responsemessage", null));
        transaction.setTimein(jSONObject.optString("timein", null));
        transaction.setTransactionReference(jSONObject.optString("transactionreference", null));
        
        return transaction;
    }
    
}

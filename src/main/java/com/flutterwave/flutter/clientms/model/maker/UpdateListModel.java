/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.maker;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author emmanueladeyemi
 */
public class UpdateListModel {

    /**
     * @param id the id to set
     */
    public void setId(String[] id) {
        this.id = id;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the range
     */
    public String getRange() {
        return range;
    }

    /**
     * @param range the range to set
     */
    public void setRange(String range) {
        this.range = range;
    }

    /**
     * @return the rejectionMessage
     */
    public String getRejectionMessage() {
        return rejectionMessage;
    }

    /**
     * @param rejectionMessage the rejectionMessage to set
     */
    public void setRejectionMessage(String rejectionMessage) {
        this.rejectionMessage = rejectionMessage;
    }

    /**
     * @return the operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * @return the id
     */
    public String[] getId() {
        return id;
    }

    @NotNull(message = "operation must be provided")
    private String operation;
    @Size(min = 1)
    private String[] id;
    private String rejectionMessage;
    private String currency;
    private String range;
}

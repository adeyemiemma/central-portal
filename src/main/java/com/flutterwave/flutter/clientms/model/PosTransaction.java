/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "pos_transaction")
@Entity
public class PosTransaction implements Serializable {

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the transRef
     */
    public String getTransRef() {
        return transRef;
    }

    /**
     * @param transRef the transRef to set
     */
    public void setTransRef(String transRef) {
        this.transRef = transRef;
    }

//    /**
//     * @return the cardholder
//     */
//    public String getCardholder() {
//        return cardholder;
//    }
//
//    /**
//     * @param cardholder the cardholder to set
//     */
//    public void setCardholder(String cardholder) {
//        this.cardholder = cardholder;
//    }
//
//    /**
//     * @return the orderId
//     */
//    public String getOrderId() {
//        return orderId;
//    }
//
//    /**
//     * @param orderId the orderId to set
//     */
//    public void setOrderId(String orderId) {
//        this.orderId = orderId;
//    }
//
//    /**
//     * @return the acquirerBank
//     */
//    public String getAcquirerBank() {
//        return acquirerBank;
//    }
//
//    /**
//     * @param acquirerBank the acquirerBank to set
//     */
//    public void setAcquirerBank(String acquirerBank) {
//        this.acquirerBank = acquirerBank;
//    }

    /**
     * @return the traceref
     */
    public String getTraceref() {
        return traceref;
    }

    /**
     * @param traceref the traceref to set
     */
    public void setTraceref(String traceref) {
        this.traceref = traceref;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the reversed
     */
    public boolean isReversed() {
        return reversed;
    }

    /**
     * @param reversed the reversed to set
     */
    public void setReversed(boolean reversed) {
        this.reversed = reversed;
    }

    /**
     * @return the reversedOn
     */
    public Date getReversedOn() {
        return reversedOn;
    }

    /**
     * @param reversedOn the reversedOn to set
     */
    public void setReversedOn(Date reversedOn) {
        this.reversedOn = reversedOn;
    }

    /**
     * @return the loggedBy
     */
    public String getLoggedBy() {
        return loggedBy;
    }

    /**
     * @param loggedBy the loggedBy to set
     */
    public void setLoggedBy(String loggedBy) {
        this.loggedBy = loggedBy;
    }

    /**
     * @return the posId
     */
    public String getPosId() {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(String posId) {
        this.posId = posId;
    }

    /**
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the rescheduleOn
     */
    public Date getRescheduleOn() {
        return rescheduleOn;
    }

    /**
     * @param rescheduleOn the rescheduleOn to set
     */
    public void setRescheduleOn(Date rescheduleOn) {
        this.rescheduleOn = rescheduleOn;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the callBack
     */
    public boolean isCallBack() {
        return callBack;
    }

    /**
     * @param callBack the callBack to set
     */
    public void setCallBack(boolean callBack) {
        this.callBack = callBack;
    }

    /**
     * @return the callbackOn
     */
    public Date getCallbackOn() {
        return callbackOn;
    }

    /**
     * @param callbackOn the callbackOn to set
     */
    public void setCallbackOn(Date callbackOn) {
        this.callbackOn = callbackOn;
    }

    /**
     * @return the callBackResponse
     */
    public String getCallBackResponse() {
        return callBackResponse;
    }

    /**
     * @param callBackResponse the callBackResponse to set
     */
    public void setCallBackResponse(String callBackResponse) {
        this.callBackResponse = callBackResponse;
    }

    /**
     * @return the reschedule
     */
    public boolean isReschedule() {
        return reschedule;
    }

    /**
     * @param reschedule the reschedule to set
     */
    public void setReschedule(boolean reschedule) {
        this.reschedule = reschedule;
    }

    /**
     * @return the scheme
     */
    public String getScheme() {
        return scheme;
    }

    /**
     * @param scheme the scheme to set
     */
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * @param currencyCode the currencyCode to set
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * @return the refCode
     */
    public String getRefCode() {
        return refCode;
    }

    /**
     * @param refCode the refCode to set
     */
    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the requestDate
     */
    public Date getRequestDate() {
        return requestDate;
    }

    /**
     * @param requestDate the requestDate to set
     */
    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    /**
     * @return the responseDate
     */
    public Date getResponseDate() {
        return responseDate;
    }

    /**
     * @param responseDate the responseDate to set
     */
    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String rrn;
    private String source;
    private String terminalId;
    private String pan;
    private double amount;
    private String currency;
    private String currencyCode;
    private String refCode;
    private String type;
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date responseDate;
    private String responseCode;
    private String responseMessage;
    private String status;
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    private String scheme;
    private boolean callBack;
    @Temporal(TemporalType.TIMESTAMP)
    private Date callbackOn;
    private String callBackResponse;
    private boolean reschedule;
    private String fileName;
    @Temporal(TemporalType.TIMESTAMP)
    private Date rescheduleOn;
    private String fileId;
    private String posId;
    private String loggedBy;
    private boolean reversed;
    @Temporal(TemporalType.TIMESTAMP)
    private Date reversedOn;
    private String merchantName;
    private String traceref;
    private String transRef;
    private String productId;
    private String productName;
    private String  merchantCode;
//    private String cardholder;
//    private String orderId;
//    private String acquirerBank;
    
}

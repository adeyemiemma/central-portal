/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.ChargeBackState;
import com.flutterwave.flutter.clientms.util.ChargeBackStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "chargeback")
@Entity
public class ChargeBack extends BaseModel implements Serializable {

//    /**
//     * @return the disputeReason
//     */
//    public String getDisputeReason() {
//        return disputeReason;
//    }
//
//    /**
//     * @param disputeReason the disputeReason to set
//     */
//    public void setDisputeReason(String disputeReason) {
//        this.disputeReason = disputeReason;
//    }
//
//    /**
//     * @return the productMerchantName
//     */
//    public String getProductMerchantName() {
//        return productMerchantName;
//    }
//
//    /**
//     * @param productMerchantName the productMerchantName to set
//     */
//    public void setProductMerchantName(String productMerchantName) {
//        this.productMerchantName = productMerchantName;
//    }
//
//    /**
//     * @return the responseDate
//     */
//    public Date getResponseDate() {
//        return responseDate;
//    }
//
//    /**
//     * @param responseDate the responseDate to set
//     */
//    public void setResponseDate(Date responseDate) {
//        this.responseDate = responseDate;
//    }

    /**
     * @return the productParentMid
     */
    public Long getProductParentMid() {
        return productParentMid;
    }

    /**
     * @param productParentMid the productParentMid to set
     */
    public void setProductParentMid(Long productParentMid) {
        this.productParentMid = productParentMid;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the chargeBackState
     */
    public ChargeBackState getChargeBackState() {
        return chargeBackState;
    }

    /**
     * @param chargeBackState the chargeBackState to set
     */
    public void setChargeBackState(ChargeBackState chargeBackState) {
        this.chargeBackState = chargeBackState;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the applied
     */
    public boolean isApplied() {
        return applied;
    }

    /**
     * @param applied the applied to set
     */
    public void setApplied(boolean applied) {
        this.applied = applied;
    }

    /**
     * @return the appliedBy
     */
    public User getAppliedBy() {
        return appliedBy;
    }

    /**
     * @param appliedBy the appliedBy to set
     */
    public void setAppliedBy(User appliedBy) {
        this.appliedBy = appliedBy;
    }

    /**
     * @return the appliedOn
     */
    public Date getAppliedOn() {
        return appliedOn;
    }

    /**
     * @param appliedOn the appliedOn to set
     */
    public void setAppliedOn(Date appliedOn) {
        this.appliedOn = appliedOn;
    }

    /**
     * @return the productMid
     */
    public long getProductMid() {
        return productMid;
    }

    /**
     * @param productMid the productMid to set
     */
    public void setProductMid(long productMid) {
        this.productMid = productMid;
    }

    /**
     * @return the chargeBackStatus
     */
    public ChargeBackStatus getChargeBackStatus() {
        return chargeBackStatus;
    }

    /**
     * @param chargeBackStatus the chargeBackStatus to set
     */
    public void setChargeBackStatus(ChargeBackStatus chargeBackStatus) {
        this.chargeBackStatus = chargeBackStatus;
    }

    /**
     * @return the amountSettled
     */
    public double getAmountSettled() {
        return amountSettled;
    }

    /**
     * @param amountSettled the amountSettled to set
     */
    public void setAmountSettled(double amountSettled) {
        this.amountSettled = amountSettled;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the flwReference
     */
    public String getFlwReference() {
        return flwReference;
    }

    /**
     * @param flwReference the flwReference to set
     */
    public void setFlwReference(String flwReference) {
        this.flwReference = flwReference;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the authenticationType
     */
    public String getAuthenticationType() {
        return authenticationType;
    }

    /**
     * @param authenticationType the authenticationType to set
     */
    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the approvedBy
     */
    public User getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private Product product;
    private String merchantId;
    private double amount;
    private String flwReference;
    private String rrn;
    private String authenticationType;
    @ManyToOne
    private User createdBy;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date approvedOn;
    @ManyToOne
    private User approvedBy;
    private ChargeBackStatus chargeBackStatus;
    private double amountSettled;
    private long productMid;
    private String currency;
    private boolean applied;
    @ManyToOne
    private User appliedBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date appliedOn;
    private ChargeBackState chargeBackState;
    private Long productParentMid;
    private String merchantName;
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date responseDate;
//    private String disputeReason;
//    private String productMerchantName;
}

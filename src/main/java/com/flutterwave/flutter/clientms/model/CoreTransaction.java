/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;


import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "transaction")
@Entity
public class CoreTransaction implements Serializable {

    /**
     * @return the merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

//    /**
//     * @return the merchant
//     */
//    public CoreMerchant getMerchant() {
//        return merchant;
//    }
//
//    /**
//     * @param merchant the merchant to set
//     */
//    public void setMerchant(CoreMerchant merchant) {
//        this.merchant = merchant;
//    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the transactionDate
     */
    public Date getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

//    /**
//     * @return the usertoken
//     */
//    public String getUsertoken() {
//        return usertoken;
//    }
//
//    /**
//     * @param usertoken the usertoken to set
//     */
//    public void setUsertoken(String usertoken) {
//        this.usertoken = usertoken;
//    }

    /**
     * @return the transactiontoken
     */
    public String getTransactiontoken() {
        return transactiontoken;
    }

    /**
     * @param transactiontoken the transactiontoken to set
     */
    public void setTransactiontoken(String transactiontoken) {
        this.transactiontoken = transactiontoken;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the transactionIdentifier
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * @param transactionIdentifier the transactionIdentifier to set
     */
    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    /**
     * @return the transactionReference
     */
    public String getTransactionReference() {
        return transactionReference;
    }

    /**
     * @param transactionReference the transactionReference to set
     */
    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * @return the statuscode
     */
    public String getStatuscode() {
        return statuscode;
    }

    /**
     * @param statuscode the statuscode to set
     */
    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    /**
     * @return the custidref
     */
    public String getCustidref() {
        return custidref;
    }

    /**
     * @param custidref the custidref to set
     */
    public void setCustidref(String custidref) {
        this.custidref = custidref;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column( name = "date_of_transaction")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;
    private String transactiontoken;
    private String provider;
    private BigDecimal amount; 
    private String currency;
    private String transactionIdentifier;
    private String transactionReference;
    private String status;
    @Column(length = 45)
    private String pan;
    private String statuscode;
    private String custidref;
    @Column(name = "usertoken")
    private String merchant;
    
    public GenericTransaction getGeneric(){
        
        GenericTransaction genericTransaction = new GenericTransaction();
        genericTransaction.setAmount(amount.doubleValue());
        genericTransaction.setCreatedAt(transactionDate); 
        genericTransaction.setFlutterReference(transactionReference);
        genericTransaction.setId(id);
        genericTransaction.setLinkingReference(transactionReference);
        genericTransaction.setMerchant(getMerchant());
//        genericTransaction.setSource(source);
        genericTransaction.setStatus(status);
        genericTransaction.setTransactionReference(transactionReference);
//        genericTransaction.setUpdatedAt(updatedAt);
        genericTransaction.setCurrency(currency == null ? "NGN" : currency);
        
        return genericTransaction;
    }
}

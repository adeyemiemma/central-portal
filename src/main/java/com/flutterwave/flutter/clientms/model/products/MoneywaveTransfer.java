/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.products;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "transfers")
@Entity
public class MoneywaveTransfer implements Serializable {

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the deletedAt
     */
    public Date getDeletedAt() {
        return deletedAt;
    }

    /**
     * @param deletedAt the deletedAt to set
     */
    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the cardValidationSuccessful
     */
    public boolean isCardValidationSuccessful() {
        return cardValidationSuccessful;
    }

    /**
     * @param cardValidationSuccessful the cardValidationSuccessful to set
     */
    public void setCardValidationSuccessful(boolean cardValidationSuccessful) {
        this.cardValidationSuccessful = cardValidationSuccessful;
    }

    /**
     * @return the deliverySuccessful
     */
    public boolean isDeliverySuccessful() {
        return deliverySuccessful;
    }

    /**
     * @param deliverySuccessful the deliverySuccessful to set
     */
    public void setDeliverySuccessful(boolean deliverySuccessful) {
        this.deliverySuccessful = deliverySuccessful;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the medium
     */
    public String getMedium() {
        return medium;
    }

    /**
     * @param medium the medium to set
     */
    public void setMedium(String medium) {
        this.medium = medium;
    }

    /**
     * @return the amountToSend
     */
    public Double getAmountToSend() {
        return amountToSend;
    }

    /**
     * @param amountToSend the amountToSend to set
     */
    public void setAmountToSend(Double amountToSend) {
        this.amountToSend = amountToSend;
    }

    /**
     * @return the amountToCharge
     */
    public Double getAmountToCharge() {
        return amountToCharge;
    }

    /**
     * @param amountToCharge the amountToCharge to set
     */
    public void setAmountToCharge(Double amountToCharge) {
        this.amountToCharge = amountToCharge;
    }

    /**
     * @return the flutterChargeResponseCode
     */
    public String getFlutterChargeResponseCode() {
        return flutterChargeResponseCode;
    }

    /**
     * @param flutterChargeResponseCode the flutterChargeResponseCode to set
     */
    public void setFlutterChargeResponseCode(String flutterChargeResponseCode) {
        this.flutterChargeResponseCode = flutterChargeResponseCode;
    }

    /**
     * @return the flutterChargeResponseMessage
     */
    public String getFlutterChargeResponseMessage() {
        return flutterChargeResponseMessage;
    }

    /**
     * @param flutterChargeResponseMessage the flutterChargeResponseMessage to set
     */
    public void setFlutterChargeResponseMessage(String flutterChargeResponseMessage) {
        this.flutterChargeResponseMessage = flutterChargeResponseMessage;
    }

    /**
     * @return the flutterDisburseResponseMessage
     */
    public String getFlutterDisburseResponseMessage() {
        return flutterDisburseResponseMessage;
    }

    /**
     * @param flutterDisburseResponseMessage the flutterDisburseResponseMessage to set
     */
    public void setFlutterDisburseResponseMessage(String flutterDisburseResponseMessage) {
        this.flutterDisburseResponseMessage = flutterDisburseResponseMessage;
    }

    /**
     * @return the flutterChargeReference
     */
    public String getFlutterChargeReference() {
        return flutterChargeReference;
    }

    /**
     * @param flutterChargeReference the flutterChargeReference to set
     */
    public void setFlutterChargeReference(String flutterChargeReference) {
        this.flutterChargeReference = flutterChargeReference;
    }

    /**
     * @return the flutterDisburseReference
     */
    public String getFlutterDisburseReference() {
        return flutterDisburseReference;
    }

    /**
     * @param flutterDisburseReference the flutterDisburseReference to set
     */
    public void setFlutterDisburseReference(String flutterDisburseReference) {
        this.flutterDisburseReference = flutterDisburseReference;
    }

    /**
     * @return the flutterDisburseResponseCode
     */
    public String getFlutterDisburseResponseCode() {
        return flutterDisburseResponseCode;
    }

    /**
     * @param flutterDisburseResponseCode the flutterDisburseResponseCode to set
     */
    public void setFlutterDisburseResponseCode(String flutterDisburseResponseCode) {
        this.flutterDisburseResponseCode = flutterDisburseResponseCode;
    }

    /**
     * @return the merchantCommission
     */
    public Double getMerchantCommission() {
        return merchantCommission;
    }

    /**
     * @param merchantCommission the merchantCommission to set
     */
    public void setMerchantCommission(Double merchantCommission) {
        this.merchantCommission = merchantCommission;
    }

    /**
     * @return the chargedFee
     */
    public Double getChargedFee() {
        return chargedFee;
    }

    /**
     * @param chargedFee the chargedFee to set
     */
    public void setChargedFee(Double chargedFee) {
        this.chargedFee = chargedFee;
    }

    /**
     * @return the receiptNumber
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber the receiptNumber to set
     */
    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the chargeCurrency
     */
    public String getChargeCurrency() {
        return chargeCurrency;
    }

    /**
     * @param chargeCurrency the chargeCurrency to set
     */
    public void setChargeCurrency(String chargeCurrency) {
        this.chargeCurrency = chargeCurrency;
    }

    /**
     * @return the disburseCurrency
     */
    public String getDisburseCurrency() {
        return disburseCurrency;
    }

    /**
     * @param disburseCurrency the disburseCurrency to set
     */
    public void setDisburseCurrency(String disburseCurrency) {
        this.disburseCurrency = disburseCurrency;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the merchant
     */
    public MoneywaveMerchant getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(MoneywaveMerchant merchant) {
        this.merchant = merchant;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private double amount;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    @Column(name = "isCardValidationSuccessful")
    private boolean cardValidationSuccessful;
    @Column(name = "isDeliverySuccessful")
    private boolean deliverySuccessful;
    private String status;
    private String medium;
    private Double amountToSend;
    private Double amountToCharge;
    private String flutterChargeResponseCode;
    private String flutterChargeResponseMessage;
    private String flutterDisburseResponseMessage;
    private String flutterChargeReference;
    private String flutterDisburseReference;
    private String flutterDisburseResponseCode;
    private Double merchantCommission;
    private Double chargedFee;
    private String receiptNumber;
    private String chargeCurrency;
    private String disburseCurrency;
    private String accountId;
    @ManyToOne
    @JoinColumn(name = "merchantId")
    private MoneywaveMerchant merchant;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.maker;

import com.flutterwave.flutter.clientms.model.*;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "merchant_limit_m")
@Entity
public class MerchantLimitM extends BaseMaker implements Serializable {

    /**
     * @return the dailyLimit
     */
    public double getDailyLimit() {
        return dailyLimit;
    }

    /**
     * @param dailyLimit the dailyLimit to set
     */
    public void setDailyLimit(double dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    /**
     * @return the value
     */
    public long getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(long value) {
        this.value = value;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the approvedBy
     */
    public User getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the modelId
     */
    public long getModelId() {
        return modelId;
    }

    /**
     * @param modelId the modelId to set
     */
    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the rejectionReason
     */
    public String getRejectionReason() {
        return rejectionReason;
    }

    /**
     * @param rejectionReason the rejectionReason to set
     */
    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the merchantId
     */
    public long getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the limitCategory
     */
    public Utility.LimitCategory getLimitCategory() {
        return limitCategory;
    }

    /**
     * @param limitCategory the limitCategory to set
     */
    public void setLimitCategory(Utility.LimitCategory limitCategory) {
        this.limitCategory = limitCategory;
    }

    /**
     * @return the minLimit
     */
    public double getMinLimit() {
        return minLimit;
    }

    /**
     * @param minLimit the minLimit to set
     */
    public void setMinLimit(double minLimit) {
        this.minLimit = minLimit;
    }

    /**
     * @return the maxLimit
     */
    public double getMaxLimit() {
        return maxLimit;
    }

    /**
     * @param maxLimit the maxLimit to set
     */
    public void setMaxLimit(double maxLimit) {
        this.maxLimit = maxLimit;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long merchantId;
    private String merchantName;
    @ManyToOne
    @JoinColumn(name = "productid")
    private Product product;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;
    private Utility.LimitCategory limitCategory;
    @Column(name = "\"value\"")
    private long value;
    private double minLimit;
    private double maxLimit;
    private double dailyLimit;
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvedOn;
    @ManyToOne
    @JoinColumn(name = "approved_by")
    private User approvedBy;
    private long modelId;
    private Status status;
    private String rejectionReason;
}

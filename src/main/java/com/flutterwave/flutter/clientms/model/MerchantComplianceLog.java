/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.Status;
import java.io.Serializable;
import java.util.Date;
import java.util.stream.Stream;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "merchant_compliance_log")
@Entity
public class MerchantComplianceLog implements Serializable {

    /**
     * @return the bvnChecked
     */
    public boolean isBvnChecked() {
        return bvnChecked;
    }

    /**
     * @param bvnChecked the bvnChecked to set
     */
    public void setBvnChecked(boolean bvnChecked) {
        this.bvnChecked = bvnChecked;
    }

    /**
     * @return the bvnCheckedOn
     */
    public Date getBvnCheckedOn() {
        return bvnCheckedOn;
    }

    /**
     * @param bvnCheckedOn the bvnCheckedOn to set
     */
    public void setBvnCheckedOn(Date bvnCheckedOn) {
        this.bvnCheckedOn = bvnCheckedOn;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the rejectionReason
     */
    public String getRejectionReason() {
        return rejectionReason;
    }

    /**
     * @param rejectionReason the rejectionReason to set
     */
    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the apiUserId
     */
    public String getApiUserId() {
        return apiUserId;
    }

    /**
     * @param apiUserId the apiUserId to set
     */
    public void setApiUserId(String apiUserId) {
        this.apiUserId = apiUserId;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the approvedBy
     */
    public User getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the scumlCertificatePath
     */
    public String getScumlCertificatePath() {
        return scumlCertificatePath;
    }

    /**
     * @param scumlCertificatePath the scumlCertificatePath to set
     */
    public void setScumlCertificatePath(String scumlCertificatePath) {
        this.scumlCertificatePath = scumlCertificatePath;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the modified
     */
    public Date getModified() {
        return modified;
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(Date modified) {
        this.modified = modified;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the registeredName
     */
    public String getRegisteredName() {
        return registeredName;
    }

    /**
     * @param registeredName the registeredName to set
     */
    public void setRegisteredName(String registeredName) {
        this.registeredName = registeredName;
    }

    /**
     * @return the registeredNumber
     */
    public String getRegisteredNumber() {
        return registeredNumber;
    }

    /**
     * @param registeredNumber the registeredNumber to set
     */
    public void setRegisteredNumber(String registeredNumber) {
        this.registeredNumber = registeredNumber;
    }

    /**
     * @return the dateofIncorporation
     */
    public String getDateofIncorporation() {
        return dateofIncorporation;
    }

    /**
     * @param dateofIncorporation the dateofIncorporation to set
     */
    public void setDateofIncorporation(String dateofIncorporation) {
        this.dateofIncorporation = dateofIncorporation;
    }

    /**
     * @return the tradingName
     */
    public String getTradingName() {
        return tradingName;
    }

    /**
     * @param tradingName the tradingName to set
     */
    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    /**
     * @return the registeredAddress
     */
    public String getRegisteredAddress() {
        return registeredAddress;
    }

    /**
     * @param registeredAddress the registeredAddress to set
     */
    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    /**
     * @return the operationalAddress
     */
    public String getOperationalAddress() {
        return operationalAddress;
    }

    /**
     * @param operationalAddress the operationalAddress to set
     */
    public void setOperationalAddress(String operationalAddress) {
        this.operationalAddress = operationalAddress;
    }

    /**
     * @return the companyType
     */
    public String getCompanyType() {
        return companyType;
    }

    /**
     * @param companyType the companyType to set
     */
    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    /**
     * @return the companyRegDocumentPath
     */
    public String getCompanyRegDocumentPath() {
        return companyRegDocumentPath;
    }

    /**
     * @param companyRegDocumentPath the companyRegDocumentPath to set
     */
    public void setCompanyRegDocumentPath(String companyRegDocumentPath) {
        this.companyRegDocumentPath = companyRegDocumentPath;
    }

    /**
     * @return the directorIdPath
     */
    public String getDirectorIdPath() {
        return directorIdPath;
    }

    /**
     * @param directorIdPath the directorIdPath to set
     */
    public void setDirectorIdPath(String directorIdPath) {
        this.directorIdPath = directorIdPath;
    }

    /**
     * @return the contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * @param contactName the contactName to set
     */
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    /**
     * @return the contactPhone
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * @param contactPhone the contactPhone to set
     */
    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    /**
     * @return the contactAddress
     */
    public String getContactAddress() {
        return contactAddress;
    }

    /**
     * @param contactAddress the contactAddress to set
     */
    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    /**
     * @return the contactEmail
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * @param contactEmail the contactEmail to set
     */
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     * @return the industry
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * @param industry the industry to set
     */
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    /**
     * @return the products
     */
    public String getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(String products) {
        this.products = products;
    }

    /**
     * @return the productWebUrl
     */
    public String getProductWebUrl() {
        return productWebUrl;
    }

    /**
     * @param productWebUrl the productWebUrl to set
     */
    public void setProductWebUrl(String productWebUrl) {
        this.productWebUrl = productWebUrl;
    }

    /**
     * @return the operatingLicencePath
     */
    public String getOperatingLicencePath() {
        return operatingLicencePath;
    }

    /**
     * @param operatingLicencePath the operatingLicencePath to set
     */
    public void setOperatingLicencePath(String operatingLicencePath) {
        this.operatingLicencePath = operatingLicencePath;
    }

    /**
     * @return the amlPolicyPath
     */
    public String getAmlPolicyPath() {
        return amlPolicyPath;
    }

    /**
     * @param amlPolicyPath the amlPolicyPath to set
     */
    public void setAmlPolicyPath(String amlPolicyPath) {
        this.amlPolicyPath = amlPolicyPath;
    }

    /**
     * @return the shareHolder1
     */
    public String getShareHolder1() {
        return shareHolder1;
    }

    /**
     * @param shareHolder1 the shareHolder1 to set
     */
    public void setShareHolder1(String shareHolder1) {
        this.shareHolder1 = shareHolder1;
    }

    /**
     * @return the shareHolder2
     */
    public String getShareHolder2() {
        return shareHolder2;
    }

    /**
     * @param shareHolder2 the shareHolder2 to set
     */
    public void setShareHolder2(String shareHolder2) {
        this.shareHolder2 = shareHolder2;
    }
    
    private String registeredName;
    private String registeredNumber;
    private String dateofIncorporation;  
    private String tradingName;
    @Column(columnDefinition = "Text")
    private String registeredAddress;
    @Column(columnDefinition = "Text")
    private String operationalAddress;
    private String companyType;
    @Column(columnDefinition = "Text")
    private String companyRegDocumentPath;
    @Column(columnDefinition = "Text")
    private String directorIdPath;
    private String contactName;
    private String contactPhone;  
    @Column(columnDefinition = "Text")
    private String contactAddress;
    private String contactEmail;    
    private String industry;
    private String products;
    private String productWebUrl;
    @Column(columnDefinition = "Text")
    private String operatingLicencePath;
    @Column(columnDefinition = "Text")
    private String amlPolicyPath;
    private String shareHolder1;
    private String shareHolder2;
    private String country;
    private String merchantId;
    private String scumlCertificatePath;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvedOn;
    @ManyToOne
    private User approvedBy;
    private String apiUserId;
    @Enumerated(EnumType.STRING)
    private Status status;
    
    private String rejectionReason;
    private String description;
    private boolean bvnChecked;
    @Temporal(TemporalType.TIMESTAMP)
    private Date bvnCheckedOn;
    
    @Override
    public String toString() {
        
        StringBuilder builder = new StringBuilder();
        builder.append(registeredName).append(" ")
                .append(registeredNumber).append(" ")
                .append(dateofIncorporation).append(" ")
                .append(tradingName).append(" ")
                .append(registeredAddress).append(" ")
                .append(operationalAddress).append(" ")
                .append(companyType).append(" ")
                .append(companyRegDocumentPath).append(" ")
                .append(directorIdPath).append(" ")
                .append(contactName).append(" ")
                .append(contactPhone).append(" ")
                .append(contactAddress).append(" ")
                .append(contactEmail).append(" ")
                .append(industry).append(" ")
                .append(Stream.of(products).reduce("", (preview, current ) -> preview+=" "+current))
                .append(productWebUrl).append(" ")
                .append(operatingLicencePath).append(" ")
                .append(amlPolicyPath).append(" ")
                .append(shareHolder1).append(" ")
                .append(shareHolder2).append(" ")
                .append(modified).append(" ")
                .append(createdOn).append(" ")
                .append(country).append(" ")
                .append(status).append(" ")
                .append(scumlCertificatePath).append(" ");
                
        
        return builder.toString();
    }
    
    public static MerchantComplianceLog fromCompliance(MerchantCompliance compliance){
        
        MerchantComplianceLog complianceModel = new MerchantComplianceLog();
        complianceModel.setRegisteredAddress(compliance.getRegisteredAddress());
        complianceModel.setAmlPolicyPath(compliance.getAmlPolicyPath());
        complianceModel.setApiUserId(compliance.getApiUserId());
        complianceModel.setApprovedOn(compliance.getApprovedOn());
        complianceModel.setApprovedBy(compliance.getApprovedBy());
        complianceModel.setCompanyRegDocumentPath(compliance.getCompanyRegDocumentPath());
        complianceModel.setCompanyType(compliance.getCompanyType());
        complianceModel.setContactAddress(compliance.getContactAddress());
        complianceModel.setContactName(compliance.getContactName());
        complianceModel.setContactEmail(compliance.getContactEmail());
        complianceModel.setContactPhone(compliance.getContactPhone());
        complianceModel.setCountry(compliance.getCountry());
        complianceModel.setCreatedOn(compliance.getCreatedOn());
        complianceModel.setDateofIncorporation(compliance.getDateofIncorporation());
        complianceModel.setDirectorIdPath(compliance.getDirectorIdPath());
//        complianceModel.setId(compliance.getId());
        complianceModel.setIndustry(compliance.getIndustry());
        complianceModel.setMerchantId(compliance.getMerchantId());
        complianceModel.setModified(compliance.getModified());
        complianceModel.setOperatingLicencePath(compliance.getOperatingLicencePath());
        complianceModel.setOperationalAddress(compliance.getOperationalAddress());
        complianceModel.setProductWebUrl(compliance.getProductWebUrl());
        complianceModel.setProducts(compliance.getProducts());
        complianceModel.setRegisteredAddress(compliance.getRegisteredAddress());
        complianceModel.setRegisteredName(compliance.getRegisteredName());
        complianceModel.setRegisteredNumber(compliance.getRegisteredNumber());
        complianceModel.setScumlCertificatePath(compliance.getScumlCertificatePath());
        complianceModel.setShareHolder1(compliance.getShareHolder1());
        complianceModel.setShareHolder2(compliance.getShareHolder2());
        complianceModel.setTradingName(compliance.getTradingName());
        complianceModel.setStatus(compliance.getStatus());
        complianceModel.setRejectionReason(compliance.getRejectionReason());
        complianceModel.setDescription(compliance.getDescription());
        
        return complianceModel;
    }
    
    
}

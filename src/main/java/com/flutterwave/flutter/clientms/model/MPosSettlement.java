/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "mpos_settlement")
@Entity
public class MPosSettlement implements Serializable {

    /**
     * @return the raveCalled
     */
    public boolean isRaveCalled() {
        return raveCalled;
    }

    /**
     * @param raveCalled the raveCalled to set
     */
    public void setRaveCalled(boolean raveCalled) {
        this.raveCalled = raveCalled;
    }

    /**
     * @return the raveCalledOn
     */
    public Date getRaveCalledOn() {
        return raveCalledOn;
    }

    /**
     * @param raveCalledOn the raveCalledOn to set
     */
    public void setRaveCalledOn(Date raveCalledOn) {
        this.raveCalledOn = raveCalledOn;
    }

    /**
     * @return the raveResponse
     */
    public String getRaveResponse() {
        return raveResponse;
    }

    /**
     * @param raveResponse the raveResponse to set
     */
    public void setRaveResponse(String raveResponse) {
        this.raveResponse = raveResponse;
    }

    /**
     * @return the actualData
     */
    public String getActualData() {
        return actualData;
    }

    /**
     * @param actualData the actualData to set
     */
    public void setActualData(String actualData) {
        this.actualData = actualData;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the payerPhoneNumber
     */
    public String getPayerPhoneNumber() {
        return payerPhoneNumber;
    }

    /**
     * @param payerPhoneNumber the payerPhoneNumber to set
     */
    public void setPayerPhoneNumber(String payerPhoneNumber) {
        this.payerPhoneNumber = payerPhoneNumber;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the transactionRef
     */
    public String getTransactionRef() {
        return transactionRef;
    }

    /**
     * @param transactionRef the transactionRef to set
     */
    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the uniqueref
     */
    public String getUniqueref() {
        return uniqueref;
    }

    /**
     * @param uniqueref the uniqueref to set
     */
    public void setUniqueref(String uniqueref) {
        this.uniqueref = uniqueref;
    }

    /**
     * @return the paymentReference
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * @param paymentReference the paymentReference to set
     */
    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    /**
     * @return the statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage the statusMessage to set
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private double amount;
    private String terminalId;
    private String payerPhoneNumber;
    private String merchantCode;
    private String transactionRef;
    private String narration;
    private String uniqueref;
    private String paymentReference;
    private String statusMessage;
    private String statusCode;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    private String actualData;
    private boolean raveCalled;
    @Temporal(TemporalType.TIMESTAMP)
    private Date raveCalledOn;
    private String raveResponse;
    
}

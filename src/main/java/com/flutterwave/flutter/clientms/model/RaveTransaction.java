/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.clientms.util.RaveUtil;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "\"Transaction\"")
@Entity
public class RaveTransaction implements Serializable {

    /**
     * @return the id 
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the transactionReference
     */
    public String getTransactionReference() {
        return transactionReference;
    }

    /**
     * @param transactionReference the transactionReference to set
     */
    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    /**
     * @return the flutterReference
     */
    public String getFlutterReference() {
        return flutterReference;
    }

    /**
     * @param flutterReference the flutterReference to set
     */
    public void setFlutterReference(String flutterReference) {
        this.flutterReference = flutterReference;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the transactionProcessor
     */
    public String getTransactionProcessor() {
        return transactionProcessor;
    }

    /**
     * @param transactionProcessor the transactionProcessor to set
     */
    public void setTransactionProcessor(String transactionProcessor) {
        this.transactionProcessor = transactionProcessor;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the appfee
     */
    public Double getAppfee() {
        return appfee;
    }

    /**
     * @param appfee the appfee to set
     */
    public void setAppfee(Double appfee) {
        this.appfee = appfee;
    }

    /**
     * @return the merchantfee
     */
    public Double getMerchantfee() {
        return merchantfee;
    }

    /**
     * @param merchantfee the merchantfee to set
     */
    public void setMerchantfee(Double merchantfee) {
        this.merchantfee = merchantfee;
    }

    /**
     * @return the merchantbearsfee
     */
    public int getMerchantbearsfee() {
        return merchantbearsfee;
    }

    /**
     * @param merchantbearsfee the merchantbearsfee to set
     */
    public void setMerchantbearsfee(int merchantbearsfee) {
        this.merchantbearsfee = merchantbearsfee;
    }

    /**
     * @return the chargedAmount
     */
    public Double getChargedAmount() {
        return chargedAmount;
    }

    /**
     * @param chargedAmount the chargedAmount to set
     */
    public void setChargedAmount(Double chargedAmount) {
        this.chargedAmount = chargedAmount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the systemType
     */
    public String getSystemType() {
        return systemType;
    }

    /**
     * @param systemType the systemType to set
     */
    public void setSystemType(String systemType) {
        this.systemType = systemType;
    }

    /**
     * @return the paymentEntity
     */
    public String getPaymentEntity() {
        return paymentEntity;
    }

    /**
     * @param paymentEntity the paymentEntity to set
     */
    public void setPaymentEntity(String paymentEntity) {
        this.paymentEntity = paymentEntity;
    }

    /**
     * @return the paymentId
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the fraudStatus
     */
    public String getFraudStatus() {
        return fraudStatus;
    }

    /**
     * @param fraudStatus the fraudStatus to set
     */
    public void setFraudStatus(String fraudStatus) {
        this.fraudStatus = fraudStatus;
    }

    /**
     * @return the chargeType
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * @param chargeType the chargeType to set
     */
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    /**
     * @return the live
     */
    public Boolean getLive() {
        return live;
    }

    /**
     * @param live the live to set
     */
    public void setLive(Boolean live) {
        this.live = live;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the deletedAt
     */
    public Date getDeletedAt() {
        return deletedAt;
    }

    /**
     * @param deletedAt the deletedAt to set
     */
    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     * @return the merchant
     */
    public RaveMerchant getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(RaveMerchant merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the raveAddon
     */
    public RaveAddon getRaveAddon() {
        return raveAddon;
    }

    /**
     * @param raveAddon the raveAddon to set
     */
    public void setRaveAddon(RaveAddon raveAddon) {
        this.raveAddon = raveAddon;
    }

    /**
     * @return the deviceFingerPrint
     */
    public String getDeviceFingerPrint() {
        return deviceFingerPrint;
    }

    /**
     * @param deviceFingerPrint the deviceFingerPrint to set
     */
    public void setDeviceFingerPrint(String deviceFingerPrint) {
        this.deviceFingerPrint = deviceFingerPrint;
    }

    /**
     * @return the markupFee
     */
    public Double getMarkupFee() {
        return markupFee;
    }

    /**
     * @param markupFee the markupFee to set
     */
    public void setMarkupFee(Double markupFee) {
        this.markupFee = markupFee;
    }

    /**
     * @return the cycle
     */
    public String getCycle() {
        return cycle;
    }

    /**
     * @param cycle the cycle to set
     */
    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    /**
     * @return the customer
     */
    public RaveCustomer getCustomer() {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(RaveCustomer customer) {
        this.customer = customer;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "tx_ref")
    private String transactionReference;
    @Column(name = "flw_ref")
    private String flutterReference;
    @Column(name = "transaction_type")
    private String transactionType;
    @Column(name = "transaction_processor")
    private String transactionProcessor;
    private String status;
    private String ip;
    @Column(columnDefinition = "text")
    private String narration;
    private Double amount;
    private Double appfee;
    private Double merchantfee;
    private int merchantbearsfee;
    @Column(name = "charged_amount")
    private Double chargedAmount;
    @Column(name = "transaction_currency")
    private String currency;
    @Column(name = "system_type")
    private String systemType;
    @Column(name = "payment_entity")
    private String paymentEntity;
    @Column(name = "payment_id")
    private String paymentId;
    @Column(name = "fraud_status")
    private String fraudStatus;
    @Column(name = "charge_type")
    private String chargeType;
    @Column(name = "is_live")
    private Boolean live;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedAt;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private RaveCustomer customer;
    @ManyToOne
    @JoinColumn(name = "merchant_id", referencedColumnName = "id")
    private RaveMerchant merchant;
    @ManyToOne
    @JoinColumn(name = "addon_id")
    private RaveAddon raveAddon;
    @Column(name = "device_fingerprint")
    private String deviceFingerPrint;
    private Double markupFee;
    @Column(name = "\"cycle\"")
    private String cycle;
    
    public GenericTransaction getGeneric(){
        
        GenericTransaction genericTransaction = new GenericTransaction();
        genericTransaction.setAmount(amount);
        genericTransaction.setFee(appfee);
        genericTransaction.setCreatedAt(createdAt);
        genericTransaction.setCurrency(currency);
        genericTransaction.setDeletedAt(deletedAt);
        genericTransaction.setFlutterReference(flutterReference);
        genericTransaction.setMerchant(merchant.getBusinessName());
        genericTransaction.setStatus(status);
        genericTransaction.setTransactionType(transactionType);
        genericTransaction.setUpdatedAt(updatedAt);
        genericTransaction.setTransactionReference(transactionReference);
        genericTransaction.setId(id);
        
        return genericTransaction;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author emmanueladeyemi
 */
@Entity
public class CoreSettlementMerchant implements Serializable {

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * @return the count
     */
    public long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(long count) {
        this.count = count;
    }

    /**
     * @return the merchantname
     */
    public String getMerchantname() {
        return merchantname;
    }

    /**
     * @param merchantname the merchantname to set
     */
    public void setMerchantname(String merchantname) {
        this.merchantname = merchantname;
    }

    /**
     * @return the merchantid
     */
    public String getMerchantid() {
        return merchantid;
    }

    /**
     * @param merchantid the merchantid to set
     */
    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }
    
    @Id
    private long id;
    private BigDecimal total;
    private long count;
    private String merchantname;
    private String merchantid;
    private String currency;
    
}

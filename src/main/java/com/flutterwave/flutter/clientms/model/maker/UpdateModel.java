/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.maker;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 *
 * @author emmanueladeyemi
 */
public class UpdateModel {

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the rejectionMessage
     */
    public String getRejectionMessage() {
        return rejectionMessage;
    }

    /**
     * @param rejectionMessage the rejectionMessage to set
     */
    public void setRejectionMessage(String rejectionMessage) {
        this.rejectionMessage = rejectionMessage;
    }

    /**
     * @return the operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }
    
    @NotNull(message = "operation must be provided")
    private String operation;
    @Min(value = 1, message = "id must be greater than 0")
    private long id;
    private String rejectionMessage;
    private String product;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.maker;

import com.flutterwave.flutter.clientms.model.*;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "provider_m")
@Entity
public class ProviderM extends BaseMaker implements Serializable {

    /**
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /**
     * @return the hasAmex
     */
    public boolean isHasAmex() {
        return hasAmex;
    }

    /**
     * @param hasAmex the hasAmex to set
     */
    public void setHasAmex(boolean hasAmex) {
        this.hasAmex = hasAmex;
    }

    /**
     * @return the amexFee
     */
    public double getAmexFee() {
        return amexFee;
    }

    /**
     * @param amexFee the amexFee to set
     */
    public void setAmexFee(double amexFee) {
        this.amexFee = amexFee;
    }

    /**
     * @return the amexFeeType
     */
    public Utility.FeeType getAmexFeeType() {
        return amexFeeType;
    }

    /**
     * @param amexFeeType the amexFeeType to set
     */
    public void setAmexFeeType(Utility.FeeType amexFeeType) {
        this.amexFeeType = amexFeeType;
    }

    /**
     * @return the amexFeeCap
     */
    public double getAmexFeeCap() {
        return amexFeeCap;
    }

    /**
     * @param amexFeeCap the amexFeeCap to set
     */
    public void setAmexFeeCap(double amexFeeCap) {
        this.amexFeeCap = amexFeeCap;
    }

    /**
     * @return the amexFeeExtra
     */
    public double getAmexFeeExtra() {
        return amexFeeExtra;
    }

    /**
     * @param amexFeeExtra the amexFeeExtra to set
     */
    public void setAmexFeeExtra(double amexFeeExtra) {
        this.amexFeeExtra = amexFeeExtra;
    }

    /**
     * @return the internetBankingFeeCap
     */
    public double getInternetBankingFeeCap() {
        return internetBankingFeeCap;
    }

    /**
     * @param internetBankingFeeCap the internetBankingFeeCap to set
     */
    public void setInternetBankingFeeCap(double internetBankingFeeCap) {
        this.internetBankingFeeCap = internetBankingFeeCap;
    }

    /**
     * @return the InternetBankingFeeExtra
     */
    public double getInternetBankingFeeExtra() {
        return InternetBankingFeeExtra;
    }

    /**
     * @param InternetBankingFeeExtra the InternetBankingFeeExtra to set
     */
    public void setInternetBankingFeeExtra(double InternetBankingFeeExtra) {
        this.InternetBankingFeeExtra = InternetBankingFeeExtra;
    }

    /**
     * @return the ussdFeeCap
     */
    public double getUssdFeeCap() {
        return ussdFeeCap;
    }

    /**
     * @param ussdFeeCap the ussdFeeCap to set
     */
    public void setUssdFeeCap(double ussdFeeCap) {
        this.ussdFeeCap = ussdFeeCap;
    }

    /**
     * @return the ussdFeeExtra
     */
    public double getUssdFeeExtra() {
        return ussdFeeExtra;
    }

    /**
     * @param ussdFeeExtra the ussdFeeExtra to set
     */
    public void setUssdFeeExtra(double ussdFeeExtra) {
        this.ussdFeeExtra = ussdFeeExtra;
    }

    /**
     * @return the internetBankingFeeType
     */
    public Utility.FeeType getInternetBankingFeeType() {
        return internetBankingFeeType;
    }

    /**
     * @param internetBankingFeeType the internetBankingFeeType to set
     */
    public void setInternetBankingFeeType(Utility.FeeType internetBankingFeeType) {
        this.internetBankingFeeType = internetBankingFeeType;
    }


    /**
     * @return the ussdFeeType
     */
    public Utility.FeeType getUssdFeeType() {
        return ussdFeeType;
    }

    /**
     * @param ussdFeeType the ussdFeeType to set
     */
    public void setUssdFeeType(Utility.FeeType ussdFeeType) {
        this.ussdFeeType = ussdFeeType;
    }
    
    /**
     * @return the hasInterCard
     */
    public boolean isHasInterCard() {
        return hasInterCard;
    }

    /**
     * @param hasInterCard the hasInterCard to set
     */
    public void setHasInterCard(boolean hasInterCard) {
        this.hasInterCard = hasInterCard;
    }

    /**
     * @return the hasInterAccount
     */
    public boolean isHasInterAccount() {
        return hasInterAccount;
    }

    /**
     * @param hasInterAccount the hasInterAccount to set
     */
    public void setHasInterAccount(boolean hasInterAccount) {
        this.hasInterAccount = hasInterAccount;
    }

    /**
     * @return the hasLocalCard
     */
    public boolean isHasLocalCard() {
        return hasLocalCard;
    }

    /**
     * @param hasLocalCard the hasLocalCard to set
     */
    public void setHasLocalCard(boolean hasLocalCard) {
        this.hasLocalCard = hasLocalCard;
    }

    /**
     * @return the hasLocalAccount
     */
    public boolean isHasLocalAccount() {
        return hasLocalAccount;
    }

    /**
     * @param hasLocalAccount the hasLocalAccount to set
     */
    public void setHasLocalAccount(boolean hasLocalAccount) {
        this.hasLocalAccount = hasLocalAccount;
    }

    /**
     * @return the hasUssd
     */
    public boolean isHasUssd() {
        return hasUssd;
    }

    /**
     * @param hasUssd the hasUssd to set
     */
    public void setHasUssd(boolean hasUssd) {
        this.hasUssd = hasUssd;
    }

    /**
     * @return the hasInternetBanking
     */
    public boolean isHasInternetBanking() {
        return hasInternetBanking;
    }

    /**
     * @param hasInternetBanking the hasInternetBanking to set
     */
    public void setHasInternetBanking(boolean hasInternetBanking) {
        this.hasInternetBanking = hasInternetBanking;
    }

    /**
     * @return the InternetBankingFee
     */
    public double getInternetBankingFee() {
        return InternetBankingFee;
    }

    /**
     * @param InternetBankingFee the InternetBankingFee to set
     */
    public void setInternetBankingFee(double InternetBankingFee) {
        this.InternetBankingFee = InternetBankingFee;
    }

    /**
     * @return the USSDFee
     */
    public double getUSSDFee() {
        return USSDFee;
    }

    /**
     * @param USSDFee the USSDFee to set
     */
    public void setUSSDFee(double USSDFee) {
        this.USSDFee = USSDFee;
    }

    /**
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return the interCardFeeType
     */
    public Utility.FeeType getInterCardFeeType() {
        return interCardFeeType;
    }

    /**
     * @param interCardFeeType the interCardFeeType to set
     */
    public void setInterCardFeeType(Utility.FeeType interCardFeeType) {
        this.interCardFeeType = interCardFeeType;
    }

    /**
     * @return the interCardFee
     */
    public double getInterCardFee() {
        return interCardFee;
    }

    /**
     * @param interCardFee the interCardFee to set
     */
    public void setInterCardFee(double interCardFee) {
        this.interCardFee = interCardFee;
    }

    /**
     * @return the interCardFeeExtra
     */
    public double getInterCardFeeExtra() {
        return interCardFeeExtra;
    }

    /**
     * @param interCardFeeExtra the interCardFeeExtra to set
     */
    public void setInterCardFeeExtra(double interCardFeeExtra) {
        this.interCardFeeExtra = interCardFeeExtra;
    }

    /**
     * @return the interCardFeeCap
     */
    public double getInterCardFeeCap() {
        return interCardFeeCap;
    }

    /**
     * @param interCardFeeCap the interCardFeeCap to set
     */
    public void setInterCardFeeCap(double interCardFeeCap) {
        this.interCardFeeCap = interCardFeeCap;
    }

    /**
     * @return the interAccountFeeType
     */
    public Utility.FeeType getInterAccountFeeType() {
        return interAccountFeeType;
    }

    /**
     * @param interAccountFeeType the interAccountFeeType to set
     */
    public void setInterAccountFeeType(Utility.FeeType interAccountFeeType) {
        this.interAccountFeeType = interAccountFeeType;
    }

    /**
     * @return the interAccountFee
     */
    public double getInterAccountFee() {
        return interAccountFee;
    }

    /**
     * @param interAccountFee the interAccountFee to set
     */
    public void setInterAccountFee(double interAccountFee) {
        this.interAccountFee = interAccountFee;
    }

    /**
     * @return the interAccountFeeExtra
     */
    public double getInterAccountFeeExtra() {
        return interAccountFeeExtra;
    }

    /**
     * @param interAccountFeeExtra the interAccountFeeExtra to set
     */
    public void setInterAccountFeeExtra(double interAccountFeeExtra) {
        this.interAccountFeeExtra = interAccountFeeExtra;
    }

    /**
     * @return the interAccountFeeCap
     */
    public double getInterAccountFeeCap() {
        return interAccountFeeCap;
    }

    /**
     * @param interAccountFeeCap the interAccountFeeCap to set
     */
    public void setInterAccountFeeCap(double interAccountFeeCap) {
        this.interAccountFeeCap = interAccountFeeCap;
    }

    /**
     * @return the localCardFeeType
     */
    public Utility.FeeType getLocalCardFeeType() {
        return localCardFeeType;
    }

    /**
     * @param localCardFeeType the localCardFeeType to set
     */
    public void setLocalCardFeeType(Utility.FeeType localCardFeeType) {
        this.localCardFeeType = localCardFeeType;
    }

    /**
     * @return the localCardFee
     */
    public double getLocalCardFee() {
        return localCardFee;
    }

    /**
     * @param localCardFee the localCardFee to set
     */
    public void setLocalCardFee(double localCardFee) {
        this.localCardFee = localCardFee;
    }

    /**
     * @return the localCardFeeExtra
     */
    public double getLocalCardFeeExtra() {
        return localCardFeeExtra;
    }

    /**
     * @param localCardFeeExtra the localCardFeeExtra to set
     */
    public void setLocalCardFeeExtra(double localCardFeeExtra) {
        this.localCardFeeExtra = localCardFeeExtra;
    }

    /**
     * @return the localCardFeeCap
     */
    public double getLocalCardFeeCap() {
        return localCardFeeCap;
    }

    /**
     * @param localCardFeeCap the localCardFeeCap to set
     */
    public void setLocalCardFeeCap(double localCardFeeCap) {
        this.localCardFeeCap = localCardFeeCap;
    }

    /**
     * @return the localAccountFeeType
     */
    public Utility.FeeType getLocalAccountFeeType() {
        return localAccountFeeType;
    }

    /**
     * @param localAccountFeeType the localAccountFeeType to set
     */
    public void setLocalAccountFeeType(Utility.FeeType localAccountFeeType) {
        this.localAccountFeeType = localAccountFeeType;
    }

    /**
     * @return the localAccountFee
     */
    public double getLocalAccountFee() {
        return localAccountFee;
    }

    /**
     * @param localAccountFee the localAccountFee to set
     */
    public void setLocalAccountFee(double localAccountFee) {
        this.localAccountFee = localAccountFee;
    }

    /**
     * @return the localAccountFeeExtra
     */
    public double getLocalAccountFeeExtra() {
        return localAccountFeeExtra;
    }

    /**
     * @param localAccountFeeExtra the localAccountFeeExtra to set
     */
    public void setLocalAccountFeeExtra(double localAccountFeeExtra) {
        this.localAccountFeeExtra = localAccountFeeExtra;
    }

    /**
     * @return the localAccountFeeCap
     */
    public double getLocalAccountFeeCap() {
        return localAccountFeeCap;
    }

    /**
     * @param localAccountFeeCap the localAccountFeeCap to set
     */
    public void setLocalAccountFeeCap(double localAccountFeeCap) {
        this.localAccountFeeCap = localAccountFeeCap;
    }

    /**
     * @return the modelId
     */
    public long getModelId() {
        return modelId;
    }

    /**
     * @param modelId the modelId to set
     */
    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the modified
     */
    public Date getModified() {
        return modified;
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(Date modified) {
        this.modified = modified;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the approvedBy
     */
    public User getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String shortName;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvedOn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;
    @Column(unique = true)
    private String name;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType interCardFeeType;
    private double interCardFee;
    private double interCardFeeExtra;
    private double interCardFeeCap;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType interAccountFeeType;
    private double interAccountFee;
    private double interAccountFeeExtra;
    private double interAccountFeeCap;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType localCardFeeType ;
    private double localCardFee;
    private double localCardFeeExtra;
    private double localCardFeeCap;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType localAccountFeeType ;
    private double localAccountFee;
    private double localAccountFeeExtra;
    private double localAccountFeeCap;
    private double InternetBankingFee;
    private double USSDFee;

    @ManyToOne
    private User approvedBy;
    @ManyToOne
    private User createdBy;
    private long modelId;
    private Status status;
    private String rejectionReason;
    
    private boolean hasInterCard;
    private boolean hasInterAccount;
    private boolean hasLocalCard;
    private boolean hasLocalAccount;
    private boolean hasUssd;
    private boolean hasInternetBanking;
    private boolean hasAmex;
    
    @Enumerated(EnumType.STRING)
    private Utility.FeeType internetBankingFeeType ;
    private double internetBankingFeeCap;
    private double InternetBankingFeeExtra;
    
    @Enumerated(EnumType.STRING)
    private Utility.FeeType ussdFeeType ;
    private double ussdFeeCap;
    private double ussdFeeExtra;
  
    private double amexFee;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType amexFeeType;
    private double amexFeeCap;
    private double amexFeeExtra;
    
    @ManyToOne
    private Currency currency;
    
    @Override
    public void setRejectionReason(String reason) {
        this.rejectionReason = reason;
    }

    @Override
    public String getRejectionReason() {
        return rejectionReason;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "product_limit")
@Entity
public class ProductLimit implements Serializable{

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the limitType
     */
    public String getLimitType() {
        return limitType;
    }

    /**
     * @param limitType the limitType to set
     */
    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    /**
     * @return the dailyTransactionLimit
     */
    public double getDailyTransactionLimit() {
        return dailyTransactionLimit;
    }

    /**
     * @param dailyTransactionLimit the dailyTransactionLimit to set
     */
    public void setDailyTransactionLimit(double dailyTransactionLimit) {
        this.dailyTransactionLimit = dailyTransactionLimit;
    }

    /**
     * @return the singleTransactionLimit
     */
    public double getSingleTransactionLimit() {
        return singleTransactionLimit;
    }

    /**
     * @param singleTransactionLimit the singleTransactionLimit to set
     */
    public void setSingleTransactionLimit(double singleTransactionLimit) {
        this.singleTransactionLimit = singleTransactionLimit;
    }

    /**
     * @return the limitCreatedOn
     */
    public String getLimitCreatedOn() {
        return limitCreatedOn;
    }

    /**
     * @param limitCreatedOn the limitCreatedOn to set
     */
    public void setLimitCreatedOn(String limitCreatedOn) {
        this.limitCreatedOn = limitCreatedOn;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the lastUpdated
     */
    public String getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the meta
     */
    public String getMeta() {
        return meta;
    }

    /**
     * @param meta the meta to set
     */
    public void setMeta(String meta) {
        this.meta = meta;
    }
            
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String limitType;
    private double dailyTransactionLimit;
    private double singleTransactionLimit;
    private String limitCreatedOn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    private String merchant;
    private String lastUpdated;
    @ManyToOne
    private User createdBy;
    private String meta;
    private String productName;
}

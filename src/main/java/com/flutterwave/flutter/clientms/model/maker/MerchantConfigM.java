/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.maker;

import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.model.products.MoneywaveMerchant;
import com.flutterwave.flutter.clientms.util.SettlementType;
import com.flutterwave.flutter.clientms.util.Status;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "merchant_config_m")
@Entity
public class MerchantConfigM extends BaseMaker implements Serializable {

    /**
     * @return the coreMerchantId
     */
    public long getCoreMerchantId() {
        return coreMerchantId;
    }

    /**
     * @param coreMerchantId the coreMerchantId to set
     */
    public void setCoreMerchantId(long coreMerchantId) {
        this.coreMerchantId = coreMerchantId;
    }

    /**
     * @return the moneywaveMerchant
     */
    public long getMoneywaveMerchant() {
        return moneywaveMerchant;
    }

    /**
     * @param moneywaveMerchant the moneywaveMerchant to set
     */
    public void setMoneywaveMerchant(long moneywaveMerchant) {
        this.moneywaveMerchant = moneywaveMerchant;
    }

    /**
     * @return the coreMerchantName
     */
    public String getCoreMerchantName() {
        return coreMerchantName;
    }

    /**
     * @param coreMerchantName the coreMerchantName to set
     */
    public void setCoreMerchantName(String coreMerchantName) {
        this.coreMerchantName = coreMerchantName;
    }

    /**
     * @return the modelId
     */
    public long getModelId() {
        return modelId;
    }

    /**
     * @param modelId the modelId to set
     */
    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the rejectionReason
     */
    public String getRejectionReason() {
        return rejectionReason;
    }

    /**
     * @param rejectionReason the rejectionReason to set
     */
    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the settlementCycle
     */
    public int getSettlementCycle() {
        return settlementCycle;
    }

    /**
     * @param settlementCycle the settlementCycle to set
     */
    public void setSettlementCycle(int settlementCycle) {
        this.settlementCycle = settlementCycle;
    }

    /**
     * @return the settlementType
     */
    public SettlementType getSettlementType() {
        return settlementType;
    }

    /**
     * @param settlementType the settlementType to set
     */
    public void setSettlementType(SettlementType settlementType) {
        this.settlementType = settlementType;
    }


    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the approvedBy
     */
    public User getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int settlementCycle;
    private SettlementType settlementType;
    private long coreMerchantId;
    private long moneywaveMerchant;
    private String coreMerchantName;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date approvedOn;
    @ManyToOne
    private User createdBy;
    @ManyToOne
    private User approvedBy;
    private String currency;
    private long modelId;
    private Status status;
    private String rejectionReason;
}

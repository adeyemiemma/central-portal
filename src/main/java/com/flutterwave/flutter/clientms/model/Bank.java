/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "bank")
@Entity
public class Bank extends BaseModel implements Serializable {

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return the modified
     */
//    public Date getModified() {
//        return modified;
//    }
//
//    /**
//     * @param modified the modified to set
//     */
//    public void setModified(Date modified) {
//        this.modified = modified;
//    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the approvedBy
     */
    public User getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String code;
    private String name;
    private String product;
    private String productId;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date approvedOn;
    @ManyToOne
    private User approvedBy;
    @ManyToOne
    private User createdBy;

    @Override
    public boolean equals(Object obj) {
        
        if(obj == null)
            return false;
        
        if(!(obj instanceof Bank)){
            
            return false;
        }
        
        Bank bank = (Bank) obj;
        
        if(bank.getName().equalsIgnoreCase(getName()) && bank.getCode().equalsIgnoreCase(bank.getCode()))
            return true;
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.code);
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }
    
}

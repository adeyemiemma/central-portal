/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "teller_point_transaction")
@Entity
public class TellerPointTransaction implements Serializable {

    /**
     * @return the reversalResponseRef
     */
    public String getReversalResponseRef() {
        return reversalResponseRef;
    }

    /**
     * @param reversalResponseRef the reversalResponseRef to set
     */
    public void setReversalResponseRef(String reversalResponseRef) {
        this.reversalResponseRef = reversalResponseRef;
    }

    /**
     * @return the responseRef
     */
    public String getResponseRef() {
        return responseRef;
    }

    /**
     * @param responseRef the responseRef to set
     */
    public void setResponseRef(String responseRef) {
        this.responseRef = responseRef;
    }

    /**
     * @return the reversed
     */
    public boolean isReversed() {
        return reversed;
    }

    /**
     * @param reversed the reversed to set
     */
    public void setReversed(boolean reversed) {
        this.reversed = reversed;
    }

    /**
     * @return the reversedOn
     */
    public Date getReversedOn() {
        return reversedOn;
    }

    /**
     * @param reversedOn the reversedOn to set
     */
    public void setReversedOn(Date reversedOn) {
        this.reversedOn = reversedOn;
    }

    /**
     * @return the posId
     */
    public String getPosId() {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(String posId) {
        this.posId = posId;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the transactionCharge
     */
    public double getTransactionCharge() {
        return transactionCharge;
    }

    /**
     * @param transactionCharge the transactionCharge to set
     */
    public void setTransactionCharge(double transactionCharge) {
        this.transactionCharge = transactionCharge;
    }

    /**
     * @return the approvalCode
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * @param approvalCode the approvalCode to set
     */
    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    /**
     * @return the acquiringBank
     */
    public String getAcquiringBank() {
        return acquiringBank;
    }

    /**
     * @param acquiringBank the acquiringBank to set
     */
    public void setAcquiringBank(String acquiringBank) {
        this.acquiringBank = acquiringBank;
    }

    /**
     * @return the cardHolderIssuingBank
     */
    public String getCardHolderIssuingBank() {
        return cardHolderIssuingBank;
    }

    /**
     * @param cardHolderIssuingBank the cardHolderIssuingBank to set
     */
    public void setCardHolderIssuingBank(String cardHolderIssuingBank) {
        this.cardHolderIssuingBank = cardHolderIssuingBank;
    }

    /**
     * @return the maskedCardPan
     */
    public String getMaskedCardPan() {
        return maskedCardPan;
    }

    /**
     * @param maskedCardPan the maskedCardPan to set
     */
    public void setMaskedCardPan(String maskedCardPan) {
        this.maskedCardPan = maskedCardPan;
    }

    /**
     * @return the cardholderName
     */
    public String getCardholderName() {
        return cardholderName;
    }

    /**
     * @param cardholderName the cardholderName to set
     */
    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    /**
     * @return the traceRef
     */
    public String getTraceRef() {
        return traceRef;
    }

    /**
     * @param traceRef the traceRef to set
     */
    public void setTraceRef(String traceRef) {
        this.traceRef = traceRef;
    }

    /**
     * @return the providerRef
     */
    public String getProviderRef() {
        return providerRef;
    }

    /**
     * @param providerRef the providerRef to set
     */
    public void setProviderRef(String providerRef) {
        this.providerRef = providerRef;
    }

    /**
     * @return the tellerpointRef
     */
    public String getTellerpointRef() {
        return tellerpointRef;
    }

    /**
     * @param tellerpointRef the tellerpointRef to set
     */
    public void setTellerpointRef(String tellerpointRef) {
        this.tellerpointRef = tellerpointRef;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the creationTime
     */
    public Date getCreationTime() {
        return creationTime;
    }

    /**
     * @param creationTime the creationTime to set
     */
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String posId;
    private String terminalId;
    private String merchantName;
    private double amount;
    private double transactionCharge;
    private String approvalCode;
    private String acquiringBank;
    private String cardHolderIssuingBank;
    private String maskedCardPan;
    private String cardholderName;
    private String traceRef;
    private String providerRef;
    private String tellerpointRef;
    private String status;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;
    private String createdBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    private boolean reversed;
    @Temporal(TemporalType.TIMESTAMP)
    private Date reversedOn;
    private String responseRef;
    private String reversalResponseRef;
    
}

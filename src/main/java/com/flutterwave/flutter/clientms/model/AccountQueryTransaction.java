/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "account_query_transaction")
@Entity
public class AccountQueryTransaction implements Serializable {

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the accountQuery
     */
    public AccountQuery getAccountQuery() {
        return accountQuery;
    }

    /**
     * @param accountQuery the accountQuery to set
     */
    public void setAccountQuery(AccountQuery accountQuery) {
        this.accountQuery = accountQuery;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the status
     */
    public Utility.QueryStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Utility.QueryStatus status) {
        this.status = status;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String accountNo;
    @ManyToOne
    private AccountQuery accountQuery;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Enumerated(EnumType.STRING)
    private Utility.QueryStatus status;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "log")
@Entity
public class Log implements Serializable {

    /**
     * @return the logDomain
     */
    public LogDomain getLogDomain() {
        return logDomain;
    }

    /**
     * @param logDomain the logDomain to set
     */
    public void setLogDomain(LogDomain logDomain) {
        this.logDomain = logDomain;
    }

    /**
     * @return the logDomainId
     */
    public long getLogDomainId() {
        return logDomainId;
    }

    /**
     * @param logDomainId the logDomainId to set
     */
    public void setLogDomainId(long logDomainId) {
        this.logDomainId = logDomainId;
    }

    /**
     * @return the logState
     */
    public LogState getLogState() {
        return logState;
    }

    /**
     * @param logState the logState to set
     */
    public void setLogState(LogState logState) {
        this.logState = logState;
    }

    /**
     * @return the level
     */
    public LogLevel getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(LogLevel level) {
        this.level = level;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        
        if(createdOn == null)
            createdOn = new Date();
        
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the status
     */
    public LogStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(LogStatus status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "text")
    private String description;
    private String username;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date();
    @Column(name = "\"action\"")
    private String action;
    private LogStatus status = LogStatus.SUCCESSFUL;
    @Enumerated(EnumType.STRING)
    @Column(name = "\"level\"")
    private LogLevel level = LogLevel.Info;
    @Enumerated(EnumType.STRING)
    private LogState logState = LogState.STARTED;
    @Enumerated
    private LogDomain logDomain = LogDomain.ADMIN;
    private long logDomainId;
    
    @Override
    public String toString(){
        
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("created : %s ,", getCreatedOn().toString()));
        builder.append(String.format("username : %s ,", username));
        builder.append(String.format("action : %s ,", action));
        builder.append(String.format("description : %s ,", description));
        builder.append(String.format("status :  %s ,", status));
        builder.append(String.format("level %s ", level));
        builder.append(String.format("state %s ", logState));
        builder.append(String.format("domain %s ", logDomain));
        builder.append(String.format("domain id %s ", logDomainId));
        
        return builder.toString();
    }
}

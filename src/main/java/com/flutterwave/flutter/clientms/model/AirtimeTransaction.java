/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Cacheable
@Table(name = "\"airtime_transaction\"")
@Entity
public class AirtimeTransaction implements Serializable {

    /**
     * @return the clientTxnRef
     */
    public String getClientTxnRef() {
        return clientTxnRef;
    }

    /**
     * @param clientTxnRef the clientTxnRef to set
     */
    public void setClientTxnRef(String clientTxnRef) {
        this.clientTxnRef = clientTxnRef;
    }

    /**
     * @return the transactResultResponseCode
     */
    public String getTransactResultResponseCode() {
        return transactResultResponseCode;
    }

    /**
     * @param transactResultResponseCode the transactResultResponseCode to set
     */
    public void setTransactResultResponseCode(String transactResultResponseCode) {
        this.transactResultResponseCode = transactResultResponseCode;
    }

    /**
     * @return the sourceIdentifier
     */
    public String getSourceIdentifier() {
        return sourceIdentifier;
    }

    /**
     * @param sourceIdentifier the sourceIdentifier to set
     */
    public void setSourceIdentifier(String sourceIdentifier) {
        this.sourceIdentifier = sourceIdentifier;
    }

    /**
     * @return the targetIdentifier
     */
    public String getTargetIdentifier() {
        return targetIdentifier;
    }

    /**
     * @param targetIdentifier the targetIdentifier to set
     */
    public void setTargetIdentifier(String targetIdentifier) {
        this.targetIdentifier = targetIdentifier;
    }

    /**
     * @return the reverseResponseCode
     */
    public String getReverseResponseCode() {
        return reverseResponseCode;
    }

    /**
     * @param reverseResponseCode the reverseResponseCode to set
     */
    public void setReverseResponseCode(String reverseResponseCode) {
        this.reverseResponseCode = reverseResponseCode;
    }

    /**
     * @return the reverseResponseMessage
     */
    public String getReverseResponseMessage() {
        return reverseResponseMessage;
    }

    /**
     * @param reverseResponseMessage the reverseResponseMessage to set
     */
    public void setReverseResponseMessage(String reverseResponseMessage) {
        this.reverseResponseMessage = reverseResponseMessage;
    }

    /**
     * @return the cardScheme
     */
    public String getCardScheme() {
        return cardScheme;
    }

    /**
     * @param cardScheme the cardScheme to set
     */
    public void setCardScheme(String cardScheme) {
        this.cardScheme = cardScheme;
    }


    /**
     * @return the transactionCountry
     */
    public String getTransactionCountry() {
        return transactionCountry;
    }

    /**
     * @param transactionCountry the transactionCountry to set
     */
    public void setTransactionCountry(String transactionCountry) {
        this.transactionCountry = transactionCountry;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the transactionCurrency
     */
    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    /**
     * @param transactionCurrency the transactionCurrency to set
     */
    public void setTransactionCurrency(String transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the merchantTxnReference
     */
    public String getMerchantTxnReference() {
        return merchantTxnReference;
    }

    /**
     * @param merchantTxnReference the merchantTxnReference to set
     */
    public void setMerchantTxnReference(String merchantTxnReference) {
        this.merchantTxnReference = merchantTxnReference;
    }

    /**
     * @return the flwTxnReference
     */
    public String getFlwTxnReference() {
        return flwTxnReference;
    }

    /**
     * @param flwTxnReference the flwTxnReference to set
     */
    public void setFlwTxnReference(String flwTxnReference) {
        this.flwTxnReference = flwTxnReference;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the authenticationModel
     */
    public String getAuthenticationModel() {
        return authenticationModel;
    }

    /**
     * @param authenticationModel the authenticationModel to set
     */
    public void setAuthenticationModel(String authenticationModel) {
        this.authenticationModel = authenticationModel;
    }

    /**
     * @return the transactionNarration
     */
    public String getTransactionNarration() {
        return transactionNarration;
    }

    /**
     * @param transactionNarration the transactionNarration to set
     */
    public void setTransactionNarration(String transactionNarration) {
        this.transactionNarration = transactionNarration;
    }

    /**
     * @return the cardCountry
     */
    public String getCardCountry() {
        return cardCountry;
    }

    /**
     * @param cardCountry the cardCountry to set
     */
    public void setCardCountry(String cardCountry) {
        this.cardCountry = cardCountry;
    }

    /**
     * @return the cardMask
     */
    public String getCardMask() {
        return cardMask;
    }

    /**
     * @param cardMask the cardMask to set
     */
    public void setCardMask(String cardMask) {
        this.cardMask = cardMask;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the transactionData
     */
    public String getTransactionData() {
        return transactionData;
    }

    /**
     * @param transactionData the transactionData to set
     */
    public void setTransactionData(String transactionData) {
        this.transactionData = transactionData;
    }

    /**
     * @return the processorReference
     */
    public String getProcessorReference() {
        return processorReference;
    }

    /**
     * @param processorReference the processorReference to set
     */
    public void setProcessorReference(String processorReference) {
        this.processorReference = processorReference;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private long id;
    private String rrn;
//    @JoinColumn(name = "provider_id")
    private String provider;
    private String merchantId;
    private String transactionCurrency;
    private String transactionCountry;
    private double amount;
    private String merchantTxnReference;
    private String flwTxnReference;
    private String transactionType;
    private String authenticationModel;
    private String transactionNarration;
    private String cardCountry;
    private String cardMask;
    private String datetime;
    private String responseCode;
    private String responseMessage;
    private String transactionData; // This is an array
    private String processorReference; // This is an array
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Column(name = "cardscheme")
    private String cardScheme;
    private String clientTxnRef;
    private String transactResultResponseCode;
    private String sourceIdentifier;
    private String targetIdentifier;
    private String reverseResponseCode;
    private String reverseResponseMessage;
    
    
}

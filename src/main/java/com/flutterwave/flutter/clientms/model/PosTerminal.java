/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Cacheable
@Table(name = "mpos_terminal")
@Entity
public class PosTerminal implements Serializable {

    /**
     * @return the flaggedOn
     */
    public Date getFlaggedOn() {
        return flaggedOn;
    }

    /**
     * @param flaggedOn the flaggedOn to set
     */
    public void setFlaggedOn(Date flaggedOn) {
        this.flaggedOn = flaggedOn;
    }

    /**
     * @return the flagged
     */
    public boolean isFlagged() {
        return flagged;
    }

    /**
     * @param flagged the flagged to set
     */
    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the assignedBy
     */
    public String getAssignedBy() {
        return assignedBy;
    }

    /**
     * @param assignedBy the assignedBy to set
     */
    public void setAssignedBy(String assignedBy) {
        this.assignedBy = assignedBy;
    }

    /**
     * @return the apiKey
     */
    public String getApiKey() {
        return apiKey;
    }

    /**
     * @param apiKey the apiKey to set
     */
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the fccId
     */
    public String getFccId() {
        return fccId;
    }

    /**
     * @param fccId the fccId to set
     */
    public void setFccId(String fccId) {
        this.fccId = fccId;
    }

    /**
     * @return the batchNo
     */
    public String getBatchNo() {
        return batchNo;
    }

    /**
     * @param batchNo the batchNo to set
     */
    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    /**
     * @return the receivedBy
     */
    public String getReceivedBy() {
        return receivedBy;
    }

    /**
     * @param receivedBy the receivedBy to set
     */
    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }

    /**
     * @return the dateIssued
     */
    public String getDateIssued() {
        return dateIssued;
    }

    /**
     * @param dateIssued the dateIssued to set
     */
    public void setDateIssued(String dateIssued) {
        this.dateIssued = dateIssued;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the serialNo
     */
    public String getSerialNo() {
        return serialNo;
    }

    /**
     * @param serialNo the serialNo to set
     */
    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    /**
     * @return the provider
     */
    public PosProvider getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(PosProvider provider) {
        this.provider = provider;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the activationResponse
     */
    public String getActivationResponse() {
        return activationResponse;
    }

    /**
     * @param activationResponse the activationResponse to set
     */
    public void setActivationResponse(String activationResponse) {
        this.activationResponse = activationResponse;
    }

    /**
     * @return the assignedOn
     */
    public Date getAssignedOn() {
        return assignedOn;
    }

    /**
     * @param assignedOn the assignedOn to set
     */
    public void setAssignedOn(Date assignedOn) {
        this.assignedOn = assignedOn;
    }

    /**
     * @return the activated
     */
    public boolean isActivated() {
        return activated;
    }

    /**
     * @param activated the activated to set
     */
    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    /**
     * @return the activatedBy
     */
    public String getActivatedBy() {
        return activatedBy;
    }

    /**
     * @param activatedBy the activatedBy to set
     */
    public void setActivatedBy(String activatedBy) {
        this.activatedBy = activatedBy;
    }

    /**
     * @return the activatedOn
     */
    public Date getActivatedOn() {
        return activatedOn;
    }

    /**
     * @param activatedOn the activatedOn to set
     */
    public void setActivatedOn(Date activatedOn) {
        this.activatedOn = activatedOn;
    }

    /**
     * @return the activationCode
     */
    public String getActivationCode() {
        return activationCode;
    }

    /**
     * @param activationCode the activationCode to set
     */
    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the merchant
     */
    public PosMerchant getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(PosMerchant merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the lastModified
     */
    public Date getLastModified() {
        return lastModified;
    }

    /**
     * @param lastModified the lastModified to set
     */
    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(unique = true)
    private String terminalId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @ManyToOne
    private PosMerchant merchant;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;
    private String activationCode;
    @Temporal(TemporalType.TIMESTAMP)
    private Date activatedOn;
    private String activatedBy; /// This can take the ip address of the device sending the request
    private boolean activated;
    @Temporal(TemporalType.TIMESTAMP)
    private Date assignedOn;
    private String activationResponse;
    private boolean enabled;
    @ManyToOne
    private PosProvider provider;
    private String fccId;
    private String serialNo;
    private String batchNo;
    private String receivedBy;
    private String dateIssued;
    private String model;
    @ManyToOne
    private User createdBy;
    private String apiKey;
    private String assignedBy;
    private String bankName;
    @Temporal(TemporalType.TIMESTAMP)
    private Date flaggedOn;
    private boolean flagged;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.model.maker;

import com.flutterwave.flutter.clientms.model.*;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
@Table(name = "merchant_fee_m")
@Entity
public class MerchantFeeM extends BaseMaker implements Serializable {

    /**
     * @return the bvnFee
     */
    public double getBvnFee() {
        return bvnFee;
    }

    /**
     * @param bvnFee the bvnFee to set
     */
    public void setBvnFee(double bvnFee) {
        this.bvnFee = bvnFee;
    }

    /**
     * @return the interCardFeeExtra
     */
    public double getInterCardFeeExtra() {
        return interCardFeeExtra;
    }

    /**
     * @param interCardFeeExtra the interCardFeeExtra to set
     */
    public void setInterCardFeeExtra(double interCardFeeExtra) {
        this.interCardFeeExtra = interCardFeeExtra;
    }

    /**
     * @return the pobCardFeeExtra
     */
    public double getPobCardFeeExtra() {
        return pobCardFeeExtra;
    }

    /**
     * @param pobCardFeeExtra the pobCardFeeExtra to set
     */
    public void setPobCardFeeExtra(double pobCardFeeExtra) {
        this.pobCardFeeExtra = pobCardFeeExtra;
    }

    /**
     * @return the pobAccountFeeExtra
     */
    public double getPobAccountFeeExtra() {
        return pobAccountFeeExtra;
    }

    /**
     * @param pobAccountFeeExtra the pobAccountFeeExtra to set
     */
    public void setPobAccountFeeExtra(double pobAccountFeeExtra) {
        this.pobAccountFeeExtra = pobAccountFeeExtra;
    }

    /**
     * @return the interAccountFeeExtra
     */
    public double getInterAccountFeeExtra() {
        return interAccountFeeExtra;
    }

    /**
     * @param interAccountFeeExtra the interAccountFeeExtra to set
     */
    public void setInterAccountFeeExtra(double interAccountFeeExtra) {
        this.interAccountFeeExtra = interAccountFeeExtra;
    }

    /**
     * @return the microTransactionAmount
     */
    public double getMicroTransactionAmount() {
        return microTransactionAmount;
    }

    /**
     * @param microTransactionAmount the microTransactionAmount to set
     */
    public void setMicroTransactionAmount(double microTransactionAmount) {
        this.microTransactionAmount = microTransactionAmount;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the merchantId
     */
    public long getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the interCardFeeType
     */
    public Utility.FeeType getInterCardFeeType() {
        return interCardFeeType;
    }

    /**
     * @param interCardFeeType the interCardFeeType to set
     */
    public void setInterCardFeeType(Utility.FeeType interCardFeeType) {
        this.interCardFeeType = interCardFeeType;
    }

    /**
     * @return the interCardFee
     */
    public double getInterCardFee() {
        return interCardFee;
    }

    /**
     * @param interCardFee the interCardFee to set
     */
    public void setInterCardFee(double interCardFee) {
        this.interCardFee = interCardFee;
    }

    /**
     * @return the interCardFeeCap
     */
    public double getInterCardFeeCap() {
        return interCardFeeCap;
    }

    /**
     * @param interCardFeeCap the interCardFeeCap to set
     */
    public void setInterCardFeeCap(double interCardFeeCap) {
        this.interCardFeeCap = interCardFeeCap;
    }

    /**
     * @return the interAccountFeeType
     */
    public Utility.FeeType getInterAccountFeeType() {
        return interAccountFeeType;
    }

    /**
     * @param interAccountFeeType the interAccountFeeType to set
     */
    public void setInterAccountFeeType(Utility.FeeType interAccountFeeType) {
        this.interAccountFeeType = interAccountFeeType;
    }

    /**
     * @return the interAccountFee
     */
    public double getInterAccountFee() {
        return interAccountFee;
    }

    /**
     * @param interAccountFee the interAccountFee to set
     */
    public void setInterAccountFee(double interAccountFee) {
        this.interAccountFee = interAccountFee;
    }

    /**
     * @return the interAccountFeeCap
     */
    public double getInterAccountFeeCap() {
        return interAccountFeeCap;
    }

    /**
     * @param interAccountFeeCap the interAccountFeeCap to set
     */
    public void setInterAccountFeeCap(double interAccountFeeCap) {
        this.interAccountFeeCap = interAccountFeeCap;
    }

    /**
     * @return the pobCardFeeType
     */
    public Utility.FeeType getPobCardFeeType() {
        return pobCardFeeType;
    }

    /**
     * @param pobCardFeeType the pobCardFeeType to set
     */
    public void setPobCardFeeType(Utility.FeeType pobCardFeeType) {
        this.pobCardFeeType = pobCardFeeType;
    }

    /**
     * @return the pobCardFee
     */
    public double getPobCardFee() {
        return pobCardFee;
    }

    /**
     * @param pobCardFee the pobCardFee to set
     */
    public void setPobCardFee(double pobCardFee) {
        this.pobCardFee = pobCardFee;
    }

    /**
     * @return the pobCardFeeCap
     */
    public double getPobCardFeeCap() {
        return pobCardFeeCap;
    }

    /**
     * @param pobCardFeeCap the pobCardFeeCap to set
     */
    public void setPobCardFeeCap(double pobCardFeeCap) {
        this.pobCardFeeCap = pobCardFeeCap;
    }

    /**
     * @return the pobAccountFeeType
     */
    public Utility.FeeType getPobAccountFeeType() {
        return pobAccountFeeType;
    }

    /**
     * @param pobAccountFeeType the pobAccountFeeType to set
     */
    public void setPobAccountFeeType(Utility.FeeType pobAccountFeeType) {
        this.pobAccountFeeType = pobAccountFeeType;
    }

    /**
     * @return the pobAccountFee
     */
    public double getPobAccountFee() {
        return pobAccountFee;
    }

    /**
     * @param pobAccountFee the pobAccountFee to set
     */
    public void setPobAccountFee(double pobAccountFee) {
        this.pobAccountFee = pobAccountFee;
    }

    /**
     * @return the pobAccountFeeCap
     */
    public double getPobAccountFeeCap() {
        return pobAccountFeeCap;
    }

    /**
     * @param pobAccountFeeCap the pobAccountFeeCap to set
     */
    public void setPobAccountFeeCap(double pobAccountFeeCap) {
        this.pobAccountFeeCap = pobAccountFeeCap;
    }

    /**
     * @return the microTransactionAccountFeeType
     */
    public Utility.FeeType getMicroTransactionAccountFeeType() {
        return microTransactionAccountFeeType;
    }

    /**
     * @param microTransactionAccountFeeType the microTransactionAccountFeeType to set
     */
    public void setMicroTransactionAccountFeeType(Utility.FeeType microTransactionAccountFeeType) {
        this.microTransactionAccountFeeType = microTransactionAccountFeeType;
    }

    /**
     * @return the microTransactionCardFee
     */
    public double getMicroTransactionCardFee() {
        return microTransactionCardFee;
    }

    /**
     * @param microTransactionCardFee the microTransactionCardFee to set
     */
    public void setMicroTransactionCardFee(double microTransactionCardFee) {
        this.microTransactionCardFee = microTransactionCardFee;
    }

    /**
     * @return the microTransactionAccountFee
     */
    public double getMicroTransactionAccountFee() {
        return microTransactionAccountFee;
    }

    /**
     * @param microTransactionAccountFee the microTransactionAccountFee to set
     */
    public void setMicroTransactionAccountFee(double microTransactionAccountFee) {
        this.microTransactionAccountFee = microTransactionAccountFee;
    }

    /**
     * @return the microTransactionCardFeeType
     */
    public Utility.FeeType getMicroTransactionCardFeeType() {
        return microTransactionCardFeeType;
    }

    /**
     * @param microTransactionCardFeeType the microTransactionCardFeeType to set
     */
    public void setMicroTransactionCardFeeType(Utility.FeeType microTransactionCardFeeType) {
        this.microTransactionCardFeeType = microTransactionCardFeeType;
    }

    /**
     * @return the createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the approvedBy
     */
    public User getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long merchantId;
    @ManyToOne
    @JoinColumn(name = "productid")
    private Product product;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType interCardFeeType;
    private double interCardFee;
    private double interCardFeeExtra;
    private double interCardFeeCap;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType interAccountFeeType;
    private double interAccountFee;
    private double interAccountFeeExtra;
    private double interAccountFeeCap;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType pobCardFeeType ;
    private double pobCardFee;
    private double pobCardFeeExtra;
    private double pobCardFeeCap;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType pobAccountFeeType ;
    private double pobAccountFee;
    private double pobAccountFeeExtra;
    private double pobAccountFeeCap;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType microTransactionAccountFeeType ;
    private double microTransactionCardFee;
    private double microTransactionAccountFee;
    private double microTransactionAmount;
    @Enumerated(EnumType.STRING)
    private Utility.FeeType microTransactionCardFeeType ;
    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy; 
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvedOn;
    @ManyToOne
    @JoinColumn(name = "approved_by")
    private User approvedBy;
    private long modelId;
    private Status status;
    private String rejectionReason;
    private String merchantName;
    
    private double bvnFee;

    @Override
    public long getModelId() {
        return modelId;
    }

    @Override
    public void setModelId(long modelId) {
        this.modelId = modelId;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public void setRejectionReason(String reason) {
        this.rejectionReason = reason;
    }

    @Override
    public String getRejectionReason() {
        return rejectionReason;
    }
    
}

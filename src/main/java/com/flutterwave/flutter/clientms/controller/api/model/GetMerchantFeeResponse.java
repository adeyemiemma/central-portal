/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

/**
 *
 * @author emmanueladeyemi
 */
public class GetMerchantFeeResponse {

    /**
     * @return the bvn
     */
    public double getBvn() {
        return bvn;
    }

    /**
     * @param bvn the bvn to set
     */
    public void setBvn(double bvn) {
        this.bvn = bvn;
    }

    /**
     * @return the card
     */
    public Map<String, Object> getCard() {
        return card;
    }

    /**
     * @param card the card to set
     */
    public void setCard(Map<String, Object> card) {
        this.card = card;
    }

    /**
     * @return the account
     */
    public Map<String, Object> getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(Map<String, Object> account) {
        this.account = account;
    }

    /**
     * @return the createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    private Map<String, Object> card;
    private Map<String, Object> account;
    private double bvn;
    
    @JsonProperty("created_on")
    private String createdOn;
    private String token;
   
}

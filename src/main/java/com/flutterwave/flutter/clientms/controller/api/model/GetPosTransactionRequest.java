/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.util.Utility;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class GetPosTransactionRequest {

    /**
     * @return the posId
     */
    public String getPosId() {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(String posId) {
        this.posId = posId;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the search
     */
    public String getSearch() {
        return search;
    }

    /**
     * @param search the search to set
     */
    public void setSearch(String search) {
        this.search = search;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the pageNum
     */
    public int getPageNum() {
        return pageNum;
    }

    /**
     * @param pageNum the pageNum to set
     */
    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @JsonProperty(value = "terminalid")
    private String terminalId;
    private String search;
    @JsonProperty(value = "merchantcode")
    private String merchantCode;
    @NotBlank(message = "Merchant id must be provided")
    @JsonProperty(value = "merchantid")
    private String merchantId;
    @Min(value = 0, message = "Minimum page no value allowed is 1")
    @JsonProperty(value = "pageno")
    private int pageNum;
    @Max(value = 1000, message = "Maximum value allowed is 1000")
    @Min(value = 0, message = "Minimum value allowed is 1")
    @JsonProperty(value = "pagesize")
    private int pageSize = 50;
    @JsonProperty(value = "startdate")
    private String startDate;
    @JsonProperty(value = "enddate")
    private String endDate;
    @NotBlank(message = "Hash value must be provided")
    private String hash;
    @JsonProperty(value = "posid")
    private String posId;
    

    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject()
                .put("terminalId", Utility.nullToEmpty(terminalId))
                .put("search", Utility.nullToEmpty(search))
                .put("merchantCode", Utility.nullToEmpty(merchantCode))
                .put("merchantId", Utility.nullToEmpty(merchantId))
                .put("pageNum", pageNum)
                .put("pageSize", pageSize)
                .put("startDate", Utility.nullToEmpty(startDate))
                .put("hash", Utility.nullToEmpty(hash))
                .put("posid", Utility.nullToEmpty(posId))
                .put("endDate", Utility.nullToEmpty(endDate));
        
        return jSONObject.toString();
    }
    
    public String toHashable(){
        
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(merchantId);
        
        return stringBuilder.toString();
    }
}

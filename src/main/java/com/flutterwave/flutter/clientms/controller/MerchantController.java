/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.controller.api.model.GetComplianceModel;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.MerchantCompliantDao;
import com.flutterwave.flutter.clientms.model.MerchantConfig;
import com.flutterwave.flutter.clientms.dao.MerchantConfigDao;
import com.flutterwave.flutter.clientms.dao.MerchantDao;
import com.flutterwave.flutter.clientms.dao.MerchantFeeDao;
import com.flutterwave.flutter.clientms.dao.MerchantLimitDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.RaveAddressBookDao;
import com.flutterwave.flutter.clientms.dao.RaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.MerchantConfigMDao;
import com.flutterwave.flutter.clientms.dao.maker.MerchantFeeMDao;
import com.flutterwave.flutter.clientms.dao.maker.MerchantLimitMDao;
import com.flutterwave.flutter.clientms.dao.maker.MerchantMDao;
import com.flutterwave.flutter.clientms.model.GenericTransaction;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Merchant;
import com.flutterwave.flutter.clientms.model.MerchantCompliance;
import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.MerchantLimit;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.MerchantConfigM;
import com.flutterwave.flutter.clientms.model.maker.MerchantFeeM;
import com.flutterwave.flutter.clientms.model.maker.MerchantLimitM;
import com.flutterwave.flutter.clientms.model.maker.MerchantM;
import com.flutterwave.flutter.clientms.model.maker.ProductM;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.model.products.GenericMerchant;
import com.flutterwave.flutter.clientms.model.products.MoneywaveMerchant;
import com.flutterwave.flutter.clientms.model.products.RaveAddressBook;
import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.CsvUtil;
import com.flutterwave.flutter.clientms.util.FillManager;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.Layouter1;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.MerchantType;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.PdfUtilExport;
import com.flutterwave.flutter.clientms.util.SettlementType;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.util.IOUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "merchantController")
@SessionScoped
public class MerchantController implements Serializable {

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @EJB
    private RaveMerchantDao raveMerchantDao;
    @EJB
    private MoneywaveMerchantDao moneywaveMerchantDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private LogService logService;
    @EJB
    private MerchantFeeMDao merchantFeeMDao;
    @EJB
    private MerchantFeeDao merchantFeeDao;
    @EJB
    private UserDao userDao;
    @EJB
    private MerchantLimitMDao merchantLimitMDao;
    @EJB
    private MerchantLimitDao merchantLimitDao;
    @EJB
    private MerchantMDao merchantMDao;
    @EJB
    private MerchantConfigMDao merchantConfigMDao;
    @EJB
    private MerchantConfigDao merchantConfigDao;
    @EJB
    private RaveAddressBookDao addressBookDao;
    @EJB
    private MerchantCompliantDao merchantCompliantDao;

    private PaginationHelper pagination, paginationMerchantConfig, paginationUnauth, paginationUnauthMerchant, paginationUnauthMerchantConfig;
    private DataModel dataModel, dataModelConfig, unauthdataModel, unauthdataModelMerchant;

    private PaginationHelper paginationLimit, paginationUnauthLimit, paginationUnauthConfig;
    private DataModel dataModelLimit, unauthdataModelLimit, unauthdataModelConfig;

    private long total = 0, moneywaveTotal = 0, raveTotal = 0;
    private Map<String, Long> productStart = new HashMap<>();

//    private long productCount;
    private MerchantFee merchantFee;
    private MerchantLimit merchantLimit;

    public MerchantLimit getMerchantLimit() {

        if (merchantLimit == null) {
            merchantLimit = new MerchantLimit();
        }

        return merchantLimit;
    }

    public void setMerchantLimit(MerchantLimit merchantLimit) {

        this.merchantLimit = merchantLimit;
    }

    public MerchantFee getMerchantFee() {

        if (merchantFee == null) {
            merchantFee = new MerchantFee();
        }

        return merchantFee;
    }

    public void setMerchantFee(MerchantFee merchantFee) {

        this.merchantFee = merchantFee;
    }

    public String getAllMerchant() {

        try {
            List<Product> products = productDao.findAll();

//            productCount = 2;
            for (Product product : products) {

                if (product.getName().equalsIgnoreCase("rave")) {
                    raveTotal = raveMerchantDao.count();
                    total += raveTotal;
                } else if (product.getName().equalsIgnoreCase("moneywave")) {
                    moneywaveTotal = moneywaveMerchantDao.count();
                    total += moneywaveTotal;
                }
            }

            return "/merchant/all";
        } catch (DatabaseException ex) {
            Logger.getLogger(MerchantController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "/merchant/all";
    }

    CoreMerchant parent;

    public CoreMerchant getParent() {

        return parent;
    }

    public String getSubMerchant(long merchant) {

        try {
            parent = coreMerchantDao.find(merchant);

            if (parent != null) {

                return "/merchant/sublist";
            }

            JsfUtil.addErrorMessage("Submerchant only exists on core");

            return "/merchant/list";
        } catch (DatabaseException ex) {
            Logger.getLogger(MerchantController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "/merchant/list";
    }

    // This is the beginning of fee section
    public PaginationHelper getPagination() {

        if (pagination == null) {
            
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {

                    System.out.println(getPageFirstItem()+"  : "+getPageSize());
                    
                    long count = merchantFeeDao.count();
                    
                    System.out.println(count);
                    
                    return count;
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        
                        System.out.println(getPageFirstItem()+"  : "+getPageSize());
                        
                        return new ListDataModel(merchantFeeDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(MerchantFeeDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }else
            pagination.createPageDataModel();

        return pagination;
    }

    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("feeupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            recreate();
            session.setAttribute("feeupdated", false);
        }

        if (dataModel == null) {
            dataModel = getPagination().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Fees");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Fees");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        return dataModel;
    }

    public String nextUnauth() {
        getPaginationUnauth().nextPage();
        recreate();
        
        return "unauth";
    }

    public String previousUnauth() {
        getPaginationUnauth().previousPage();
        recreate();
        return "unauth";
    }

    public String next() {
//        pagination = null;
        getPagination().nextPage();
        recreate();
        return "list";
    }

    public String previous() {
//        pagination = null;
        getPagination().previousPage();
        recreate();
        return "list";
    }

    public void export(String product, String search, String deleted, String live, String country, String createdBetween, String format) throws DatabaseException {

        Date startDate = null, endDate = null;

        if (createdBetween != null && !"".equalsIgnoreCase(createdBetween)) {

            try {
                String[] splitDate = createdBetween.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                startDate = calendar.getTime();

                endDate = dateFormat.parse(splitDate[1]);

                calendar.setTime(endDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
//                calendar.set(Calendar.MILLISECOND, 59);

                endDate = calendar.getTime();

            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

        List<Product> products = productDao.findAll();

        if (products == null || products.isEmpty()) {
            return;
        }

        List<GenericMerchant> merchants = new ArrayList<>();

        long totalCount = 0;

        if ("rave".equalsIgnoreCase(product)) {

            Page<RaveMerchant> raveMerchant = raveMerchantDao.search(0, 0, search, deleted == null || "any".equalsIgnoreCase(deleted) ? null : "yes".equalsIgnoreCase(deleted),
                    live == null || "any".equalsIgnoreCase(live) ? null : !"yes".equalsIgnoreCase(live), country == null || "any".equalsIgnoreCase(country) ? null : country, startDate, endDate);

            totalCount += raveMerchant.getCount();

            if (raveMerchant.getContent() != null && !raveMerchant.getContent().isEmpty()) {

                merchants.addAll(raveMerchant.getContent().stream().map((RaveMerchant rm) -> {
                    GenericMerchant gm = rm.getGeneric();
                    try {
                        RaveAddressBook addressBook = addressBookDao.findByMerchant(rm);

                        if (addressBook != null) {
                            gm.setActive((addressBook.getBlocked() == 0));
                            gm.setEmail(addressBook.getEmail());
                            gm.setPhone(addressBook.getPhoneNumber());
                        }

                        gm.setProduct(product);
                        gm.setEditing(isEditing(product, rm.getId()));

                    } catch (Exception ex) {
                        if (ex != null) {
                            ex.printStackTrace();
                        }
                    }
                    return gm;
                }).collect(Collectors.<GenericMerchant>toList()));

//                    extraData.put("rave", "" + raveMerchant.getContent().size());
            }
        } else if ("moneywave".equalsIgnoreCase(product)) {

            Page<MoneywaveMerchant> waveMerchant = moneywaveMerchantDao.search(0, 0, search, deleted == null || "any".equalsIgnoreCase(deleted) ? null : "yes".equalsIgnoreCase(deleted),
                    live == null || "any".equalsIgnoreCase(live) ? null : "yes".equalsIgnoreCase(live), country == null || "any".equalsIgnoreCase(country) ? null : country, startDate, endDate);

            totalCount += waveMerchant.getCount();

            if (waveMerchant.getContent() != null && !waveMerchant.getContent().isEmpty()) {

                merchants.addAll(waveMerchant.getContent().stream().map((MoneywaveMerchant rm) -> {
                    GenericMerchant gm = rm.getGeneric();
                    try {
                        gm.setProduct(product);
                        gm.setEditing(isEditing(product, rm.getId()));
                    } catch (Exception ex) {
                        if (ex != null) {
                            ex.printStackTrace();
                        }
                    }

                    return gm;
                }).collect(Collectors.<GenericMerchant>toList()));
            }
        } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

            Page<CoreMerchant> coreMerchant = coreMerchantDao.search(0, 0, search,
                    live == null || "any".equalsIgnoreCase(live) ? null : "yes".equalsIgnoreCase(live), country == null || "any".equalsIgnoreCase(country) ? null : country, startDate, endDate);

            totalCount += coreMerchant.getCount();

            if (coreMerchant.getContent() != null && !coreMerchant.getContent().isEmpty()) {

                merchants.addAll(coreMerchant.getContent().stream().map((CoreMerchant rm) -> {

                    GenericMerchant gm = rm.getGeneric();

//                        genericMerchant.setProduct(product);
                    try {
                        gm.setProduct(product);
                        gm.setEditing(isEditing(product, rm.getId()));
                    } catch (Exception ex) {
                        if (ex != null) {
                            ex.printStackTrace();
                        }
                    }

                    return gm;
                }).collect(Collectors.<GenericMerchant>toList()));
            }
        }

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        if ("pdf".equalsIgnoreCase(format)) {
            // export to pdf here

            List<String> headers = (List) Utility.getClassFields(GenericMerchant.class);

            ServletOutputStream out = null;

            try {

                String fileName = PdfUtilExport.generatePdf("Flutterwave", "Merchants", headers, (List) merchants);

                response.setHeader("Content-Disposition", "attachment; filename=merchantrecord.pdf");
                response.setContentType("application/pdf");

                FileInputStream fileInputStream = new FileInputStream(fileName);
                out = response.getOutputStream();

                try {
                    IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                    out.flush();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }

//                out.flush();
                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } // end of pdf export
        else if ("csv".equalsIgnoreCase(format)) {

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
            List<String> headers = (List) Utility.getClassFields(GenericMerchant.class);

            String output = CsvUtil.writeLine(headers, (List) merchants);

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=merchantrecord.csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        } else {

            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("MerchantRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

//        FillManager.fillTransactionReport(worksheet, startRowIndex, startColIndex, datasource);
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) TransactionUtility.getClassFields(GenericMerchant.class), "Merchant Record", null);

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
//            List<String> headers = (List) TransactionUtility.getClassFields(PdfExportModel.class);
            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) merchants);

            //HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//       String fileName = "Card-Batch.xls";
            response.setHeader("Content-Disposition", "attachment; filename=merchantrecord.xls");
            response.setContentType("application/vnd.ms-excel");

            ServletOutputStream out = null;

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }
    }

    private boolean isEditing(String product, long id) throws DatabaseException {

        List<MerchantM> list = merchantMDao.find("modelId", id);

        if (list != null && !list.isEmpty()) {

            MerchantM cM = list.stream().filter(x -> x.getApprovedOn() == null && x.getProduct().getName().equalsIgnoreCase(product)).findFirst().orElse(null);

            if (cM != null) {

//                    JsfUtil.addErrorMessage("A pending update exists");
                return true;
            }
        }

        return false;
    }

    /**
     * This creates fee for merchant
     *
     * @return
     */
    public String createFee() {

        try {
            MerchantFee p = merchantFeeDao.findByKey("merchantName", merchantFee.getMerchantName());

            if (p != null) {

                Log log = new Log();
                log.setAction("creating merchant fee");
                log.setCreatedOn(new Date());
                log.setDescription("created merchant fee");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Merchant Fee creation on product " + merchantFee.getProduct().getName() + " for merchant with  id " + p.getId() + " exists");

                JsfUtil.addErrorMessage("Fee exists for selected merchant ");
                return "/fee/create";
            }

            merchantFee.setCreated(new Date());
            merchantFee.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));

            //productDao.create(product);
            MerchantFeeM merchantFeeM = new MerchantFeeM();
            merchantFeeM.setCreatedBy(merchantFee.getCreatedBy());
            merchantFeeM.setCreatedOn(merchantFee.getCreated());

            // This section is for international fee
            merchantFeeM.setInterAccountFee(merchantFee.getInterAccountFee());
            merchantFeeM.setInterAccountFeeExtra(merchantFee.getInterAccountFeeExtra());
            merchantFeeM.setInterAccountFeeCap(merchantFee.getInterAccountFeeCap());
            merchantFeeM.setInterAccountFeeType(merchantFee.getInterAccountFeeType());
            merchantFeeM.setInterCardFee(merchantFee.getInterCardFee());
            merchantFeeM.setInterCardFeeCap(merchantFee.getInterCardFeeCap());
            merchantFeeM.setInterCardFeeType(merchantFee.getInterCardFeeType());
            merchantFeeM.setInterCardFeeExtra(merchantFee.getInterCardFeeExtra());
            // end of setting intertional fee

            // This section is for pob fee
            merchantFeeM.setPobAccountFee(merchantFee.getPobAccountFee());
            merchantFeeM.setPobAccountFeeExtra(merchantFee.getPobAccountFeeExtra());
            merchantFeeM.setPobAccountFeeCap(merchantFee.getPobAccountFeeCap());
            merchantFeeM.setPobAccountFeeType(merchantFee.getPobAccountFeeType());
            merchantFeeM.setPobCardFee(merchantFee.getPobCardFee());
            merchantFeeM.setPobCardFeeExtra(merchantFee.getPobCardFeeExtra());
            merchantFeeM.setPobCardFeeCap(merchantFee.getPobCardFeeCap());
            merchantFeeM.setPobCardFeeType(merchantFee.getPobCardFeeType());
            // end of setting pob fee

            // This section is for micro transaction  fee
            merchantFeeM.setMicroTransactionAccountFee(merchantFee.getMicroTransactionAccountFee());
            merchantFeeM.setMicroTransactionAccountFeeType(merchantFee.getMicroTransactionAccountFeeType());
            merchantFeeM.setMicroTransactionCardFee(merchantFee.getMicroTransactionCardFee());
            merchantFeeM.setMicroTransactionCardFeeType(merchantFee.getMicroTransactionCardFeeType());
            merchantFeeM.setMicroTransactionAmount(merchantFee.getMicroTransactionAmount());
            // This section is for micro transaction  fee

            merchantFeeM.setBvnFee(merchantFee.getBvnFee());

            String merchantName = merchantFee.getMerchantName();

            String[] merchantNamewithId = merchantName.split("-");

            merchantFeeM.setMerchantName(merchantNamewithId[0].trim());
            merchantFeeM.setStatus(Status.PENDING);
            merchantFeeM.setProduct(merchantFee.getProduct());
            merchantFeeM.setMerchantId(Long.parseLong(merchantNamewithId[1].trim()));

            merchantFeeM.setBvnFee(merchantFee.getBvnFee());

            merchantFeeMDao.create(merchantFeeM);

            Log log = new Log();
            log.setAction("creating merchant fee");
            log.setCreatedOn(new Date());
            log.setDescription("created merchant fee");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Merchant Fee has been created successfully and sent for approval");

            logService.log(log);

            merchantFee = new MerchantFee();

            recreate();

            return "/fee/create";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);

            JsfUtil.addSuccessMessage("Merchant Fee cannot be created at the moment, please try again later ");
            try {
                Log log = new Log();
                log.setAction("create merchant fee");
                log.setCreatedOn(new Date());
                log.setDescription("create merchant fee");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "/fee/create";
    }

    public void prepareCreateFee() {
        this.merchantFee = new MerchantFee();
    }

    public String prepareFeeUpdate(long id) {

        try {
            merchantFee = merchantFeeDao.find(id);

            if (merchantFee == null) {

                JsfUtil.addErrorMessage("unable to find fee for merchant");
                return "/fee/list";
            }

            setMerchantFee(merchantFee);

            return "/fee/edit";
        } catch (DatabaseException ex) {
            Logger.getLogger(MerchantController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "/fee/list";
    }

    public String updateFee() {

        try {
//            MerchantFee p = merchantFeeDao.findByKey("merchantName", merchantFee.getMerchantName());

//            if (p != null) {
//
//                Log log = new Log();
//                log.setAction("creating merchant fee");
//                log.setCreatedOn(new Date());
//                log.setDescription("created merchant fee");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
//                log.setLogDomain(LogDomain.ADMIN);
//                log.setLogState(LogState.FINISH);
//
//                logService.log(log, "Merchant Fee updated on product "+ merchantFee.getProduct().getName() +" for merchant with  id " + p.getId() + " exists");
//
//                JsfUtil.addErrorMessage("Fee exists for selected merchant ");
//                return "/fee/edit";
//            }
            List<MerchantFeeM> pendingFees = merchantFeeMDao.find("modelId", merchantFee.getId());

            if (pendingFees != null && !pendingFees.isEmpty()) {

                
                List<MerchantFeeM> pendings = pendingFees.stream().filter(x -> x.getStatus() == Status.PENDING).collect(Collectors.toList());
                
                if(pendings != null && !pendings.isEmpty()){
                    JsfUtil.addErrorMessage("A Pending update exists for this merchant");
                    return "/fee/edit";
                }
            }

//            merchantFee.setCreated(new Date());
//            merchantFee.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            //productDao.create(product);
            MerchantFeeM merchantFeeM = new MerchantFeeM();
            merchantFeeM.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            merchantFeeM.setCreatedOn(new Date());

            // This section is for international fee
            merchantFeeM.setInterAccountFee(merchantFee.getInterAccountFee());
            merchantFeeM.setInterAccountFeeExtra(merchantFee.getInterAccountFeeExtra());
            merchantFeeM.setInterAccountFeeCap(merchantFee.getInterAccountFeeCap());
            merchantFeeM.setInterAccountFeeType(merchantFee.getInterAccountFeeType());
            merchantFeeM.setInterCardFee(merchantFee.getInterCardFee());
            merchantFeeM.setInterCardFeeCap(merchantFee.getInterCardFeeCap());
            merchantFeeM.setInterCardFeeType(merchantFee.getInterCardFeeType());
            merchantFeeM.setInterCardFeeExtra(merchantFee.getInterCardFeeExtra());
            // end of setting intertional fee

            // This section is for pob fee
            merchantFeeM.setPobAccountFee(merchantFee.getPobAccountFee());
            merchantFeeM.setPobAccountFeeExtra(merchantFee.getPobAccountFeeExtra());
            merchantFeeM.setPobAccountFeeCap(merchantFee.getPobAccountFeeCap());
            merchantFeeM.setPobAccountFeeType(merchantFee.getPobAccountFeeType());
            merchantFeeM.setPobCardFee(merchantFee.getPobCardFee());
            merchantFeeM.setPobCardFeeExtra(merchantFee.getPobCardFeeExtra());
            merchantFeeM.setPobCardFeeCap(merchantFee.getPobCardFeeCap());
            merchantFeeM.setPobCardFeeType(merchantFee.getPobCardFeeType());
            // end of setting pob fee

            // This section is for micro transaction  fee
            merchantFeeM.setMicroTransactionAccountFee(merchantFee.getMicroTransactionAccountFee());
            merchantFeeM.setMicroTransactionAccountFeeType(merchantFee.getMicroTransactionAccountFeeType());
            merchantFeeM.setMicroTransactionCardFee(merchantFee.getMicroTransactionCardFee());
            merchantFeeM.setMicroTransactionCardFeeType(merchantFee.getMicroTransactionCardFeeType());
            merchantFeeM.setMicroTransactionAmount(merchantFee.getMicroTransactionAmount());
            // This section is for micro transaction  fee

            // This section is for BVN fee
            merchantFeeM.setBvnFee(merchantFee.getBvnFee());

            // end of BVN fee
            String merchantName = merchantFee.getMerchantName();

            String[] merchantNamewithId = merchantName.split("-");

            merchantFeeM.setMerchantName(merchantNamewithId[0].trim());
            merchantFeeM.setStatus(Status.PENDING);
            merchantFeeM.setProduct(merchantFee.getProduct());
            merchantFeeM.setMerchantId(merchantFee.getMerchantId());
            merchantFeeM.setModelId(merchantFee.getId());

            merchantFeeMDao.create(merchantFeeM);

            Log log = new Log();
            log.setAction("updating merchant fee");
            log.setCreatedOn(new Date());
            log.setDescription("updating merchant fee");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Merchant Fee has been updated successfully and sent for approval");

            logService.log(log);

//            merchantFee = new MerchantFee();
            pagination = null;

            return "/fee/edit";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);

            JsfUtil.addSuccessMessage("Merchant Fee cannot be updated at the moment, please try again later ");
            try {
                Log log = new Log();
                log.setAction("create merchant fee");
                log.setCreatedOn(new Date());
                log.setDescription("create merchant fee");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "/fee/edit";
    }

    public void recreate() {
        dataModel = null;
        unauthdataModel = null;
    }

    public void recreateMerchant() {
        paginationUnauthMerchant = null;
    }

    public void recreateLimit() {
        paginationLimit = null;
        paginationUnauthLimit = null;
    }

    public void recreateConfig() {
        paginationMerchantConfig = null;
        paginationUnauthMerchantConfig = null;
    }

    // end of fee section 
    // This section is for unauthorized fees
    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("feeupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauth = null;
            recreate();
            session.setAttribute("feeupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth Merchant fees");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth Merchant fees");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        if (unauthdataModel == null) {
            unauthdataModel = getPaginationUnauth().createPageDataModel();
        }

        return unauthdataModel;
    }

    public PaginationHelper getPaginationUnauth() {

        if (paginationUnauth == null) {
            paginationUnauth = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return merchantFeeMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(merchantFeeMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }
        else
            paginationUnauth.createPageDataModel();

        return paginationUnauth;
    }

    /* This section is for limit*/
    public String createLimit() {

        try {

            String merchantName = merchantLimit.getMerchantName();

            String[] merchantNamewithId = merchantName.split("-");

            Map<String, Object> searchParams = new HashMap<>();
            searchParams.put("merchantId", merchantNamewithId[1]);
            searchParams.put("limitCategory", Utility.LimitCategory.Merchant);

            List<MerchantLimit> limits = merchantLimitDao.findRaw(searchParams);

            if (limits != null && !limits.isEmpty()) {

                Log log = new Log();
                log.setAction("creating merchant limit");
                log.setCreatedOn(new Date());
                log.setDescription("created merchant limit");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Merchant limit creation on product " + merchantLimit.getProduct().getName() + " for merchant with name " + merchantNamewithId[0] + " exists");

                JsfUtil.addErrorMessage("Limit exists for selected merchant ");
                return "/limit/create";
            }

            if (merchantLimit.getMinLimit() > merchantLimit.getMaxLimit()) {

                Log log = new Log();
                log.setAction("creating merchant limit");
                log.setCreatedOn(new Date());
                log.setDescription("created merchant limit");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Merchant limit creation on product " + merchantLimit.getProduct().getName() + " for merchant with name " + merchantNamewithId[0] + " exists");

                JsfUtil.addErrorMessage("Minimum Limit must be less than Maxumum limit");
                return "/limit/create";
            }

            merchantLimit.setCreatedOn(new Date());
            merchantLimit.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));

            //productDao.create(product);
            MerchantLimitM merchantLimitM = new MerchantLimitM();
            merchantLimitM.setCreatedBy(merchantLimit.getCreatedBy());
            merchantLimitM.setCreatedOn(merchantLimit.getCreatedOn());

            merchantLimitM.setDailyLimit(merchantLimit.getDailyLimit());
            merchantLimitM.setMinLimit(merchantLimit.getMinLimit());
            merchantLimitM.setMaxLimit(merchantLimit.getMaxLimit());
            merchantLimitM.setLimitCategory(Utility.LimitCategory.Merchant);
            merchantLimitM.setProduct(merchantLimit.getProduct());
            merchantLimitM.setValue(merchantLimit.getValue());

            merchantLimitM.setMerchantName(merchantNamewithId[0].trim());
            merchantLimitM.setStatus(Status.PENDING);
            merchantLimitM.setProduct(merchantLimit.getProduct());
            merchantLimitM.setMerchantId(Long.parseLong(merchantNamewithId[1].trim()));

            merchantLimitMDao.create(merchantLimitM);

            Log log = new Log();
            log.setAction("creating merchant limit");
            log.setCreatedOn(new Date());
            log.setDescription("created merchant limit");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Merchant limit has been created successfully and sent for approval");

            logService.log(log);

            merchantLimit = new MerchantLimit();

            recreateLimit();

            return "/limit/list";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);

            try {
                Log log = new Log();
                log.setAction("create merchant limit");
                log.setCreatedOn(new Date());
                log.setDescription("create merchant limit");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "/limit/create";
    }

    public PaginationHelper getPaginationLimit() {

        if (paginationLimit == null) {
            paginationLimit = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {

                    return total;
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(merchantLimitDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(MerchantFeeDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationLimit;
    }

    public DataModel getUnauthLimit() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("limitupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauthLimit = null;
            session.setAttribute("limitupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth Merchant Limit");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth Merchant Limit");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        if (paginationUnauthLimit == null) {
            unauthdataModelLimit = getPaginationUnauthLimit().createPageDataModel();
        }

        return unauthdataModelLimit;
    }

    public PaginationHelper getPaginationUnauthLimit() {

        if (paginationUnauthLimit == null) {
            paginationUnauthLimit = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return merchantLimitMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(merchantLimitMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauthLimit;
    }

    public DataModel getAllLimit() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("limitupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("limitupdated", false);
        }

        if (paginationLimit == null) {
            dataModelLimit = getPaginationLimit().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Limits");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Limits");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        return dataModelLimit;
    }

    public String prepareLimitUpdate(long id) throws DatabaseException {

        MerchantLimit mLimit = merchantLimitDao.find(id);

        if (mLimit == null) {

            JsfUtil.addErrorMessage("Record for limit not found");
            return "/limit/list";
        }

        setMerchantLimit(mLimit);

        return "/limit/edit";
    }

    public String updateLimit() {

        try {

            String merchantName = merchantLimit.getMerchantName();

            Map<String, Object> searchParams = new HashMap<>();
            searchParams.put("merchantId", merchantLimit.getMerchantId());
            searchParams.put("limitCategory", merchantLimit.getLimitCategory());

            List<MerchantLimitM> pendingLimits = merchantLimitMDao.find("modelId", merchantLimit.getId());

            if (pendingLimits != null && !pendingLimits.isEmpty()) {

                JsfUtil.addErrorMessage("A pending update exist.");
                return "/limit/edit";
            }

//            List<MerchantLimit> limits = merchantLimitDao.findRaw(searchParams);
//                    
//            
//            if (limits != null && !limits.isEmpty() && limits.get(0).) {
//
//                Log log = new Log();
//                log.setAction("updating merchant limit");
//                log.setCreatedOn(new Date());
//                log.setDescription("update merchant limit");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
//                log.setLogDomain(LogDomain.ADMIN);
//                log.setLogState(LogState.FINISH);
//
//                logService.log(log, "Merchant limit update on product "+ merchantLimit.getProduct().getName() +" for merchant with name");
//
//                JsfUtil.addErrorMessage("Limit exists for selected merchant ");
//                return "/limit/edit";
//            }
            if (merchantLimit.getMinLimit() > merchantLimit.getMaxLimit()) {

                Log log = new Log();
                log.setAction("creating merchant limit");
                log.setCreatedOn(new Date());
                log.setDescription("created merchant limit");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Merchant limit update on product " + merchantLimit.getProduct().getName() + " for merchant with name ");

                JsfUtil.addErrorMessage("Minimum Limit must be less than Maxumum limit");
                return "/limit/edit";
            }

            //merchantLimit.setCreatedOn(new Date());
            //merchantLimit.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            //productDao.create(product);
            MerchantLimitM merchantLimitM = new MerchantLimitM();
            merchantLimitM.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            merchantLimitM.setCreatedOn(new Date());

            merchantLimitM.setDailyLimit(merchantLimit.getDailyLimit());
            merchantLimitM.setMinLimit(merchantLimit.getMinLimit());
            merchantLimitM.setMaxLimit(merchantLimit.getMaxLimit());
            merchantLimitM.setLimitCategory(merchantLimit.getLimitCategory());
            merchantLimitM.setProduct(merchantLimit.getProduct());
            merchantLimitM.setValue(merchantLimit.getValue());

            merchantLimitM.setMerchantName(merchantLimit.getMerchantName());
            merchantLimitM.setStatus(Status.PENDING);
            merchantLimitM.setProduct(merchantLimit.getProduct());
            merchantLimitM.setMerchantId(merchantLimit.getMerchantId());
            merchantLimitM.setModelId(merchantLimit.getId());

            merchantLimitMDao.create(merchantLimitM);

            Log log = new Log();
            log.setAction("creating merchant limit");
            log.setCreatedOn(new Date());
            log.setDescription("created merchant limit");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Merchant limit has been updated successfully and sent for approval");

            logService.log(log);

//            merchantLimit = new MerchantLimit();
            //recreateLimit();
            return "/limit/edit";

        } catch (Exception ex) {
            JsfUtil.addErrorMessage("Unable to update merchant limit, please try again later");
            return "/limit/edit";
        }
    }

    /* end of limit section*/
    // helper section
    // This is called to get product merchant base on the id
    public GenericMerchant getMerchant(String product, long id) {

        try {
            if ("rave".equalsIgnoreCase(product)) {
                RaveMerchant raveMerchant = raveMerchantDao.find(id);

                if (raveMerchant == null) {
                    return null;
                }

                return raveMerchant.getGeneric();
            } else if ("moneywave".equalsIgnoreCase(product)) {
                MoneywaveMerchant merchant = moneywaveMerchantDao.find(id);

                if (merchant == null) {
                    return null;
                }

                return merchant.getGeneric();
            } else if ("core".equalsIgnoreCase(product)) {
                CoreMerchant merchant = coreMerchantDao.find(id);

                if (merchant == null) {
                    return null;
                }

                return merchant.getGeneric();
            }

        } catch (Exception ex) {

            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return null;
    }

    public String viewTransactions(String idString, String product) {

        if (idString == null || "0".equalsIgnoreCase(idString)) {
            JsfUtil.addErrorMessage("Merchant record not found");
            return "/merchant/list";
        }

        try {

            long id = Long.parseLong(idString);

            if ("moneywave".equalsIgnoreCase(product)) {
                MoneywaveMerchant moneywaveMerchant = moneywaveMerchantDao.find(id);
                genericMerchant = moneywaveMerchant.getGeneric();
            } else if ("rave".equalsIgnoreCase(product)) {
                RaveMerchant raveMerchant = raveMerchantDao.find(id);
                genericMerchant = raveMerchant.getGeneric();
            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {
                CoreMerchant coreMerchant = coreMerchantDao.find(id);
                genericMerchant = coreMerchant.getGeneric();
            }

            if (genericMerchant != null) {
                genericMerchant.setProductName(product);
            }

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return "transaction";
    }

    public String viewChargebacks(long id, String product) {

        if (id <= 0) {
            JsfUtil.addErrorMessage("Merchant record not found");
            return "/merchant/list";
        }

        try {

            if ("moneywave".equalsIgnoreCase(product)) {
                MoneywaveMerchant moneywaveMerchant = moneywaveMerchantDao.find(id);
                genericMerchant = moneywaveMerchant.getGeneric();
            } else if ("rave".equalsIgnoreCase(product)) {
                RaveMerchant raveMerchant = raveMerchantDao.find(id);
                genericMerchant = raveMerchant.getGeneric();
            } else if ("flutterwave core".equalsIgnoreCase(product) || "core".equalsIgnoreCase(product)) {
                CoreMerchant coreMerchant = coreMerchantDao.find(id);
                genericMerchant = coreMerchant.getGeneric();
            } else {
                genericMerchant = new GenericMerchant();
            }

            if (genericMerchant != null) {
                genericMerchant.setProductName(product);
            }

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return "chargeback";
    }

//    @Transactional
    public String create() {

        if (merchant != null) {
            merchant.setCreatedOn(new Date());
        }

        try {

            if ("rave".equalsIgnoreCase(merchant.getProduct().getName())) {

                RaveMerchant raveMerchant = raveMerchantDao.findByKey("businessName", merchant.getCompanyName());

                if (raveMerchant != null) {

                    JsfUtil.addErrorMessage("merchant with name " + merchant.getCompanyName() + " exists");
                    return "/merchant/create";
                }
            } else if ("moneywave".equalsIgnoreCase(merchant.getProduct().getName())) {

                MoneywaveMerchant moneywaveMerchant = moneywaveMerchantDao.findByKey("name", merchant.getCompanyName());

                if (moneywaveMerchant != null) {
                    JsfUtil.addErrorMessage("merchant with name " + merchant.getCompanyName() + " exists");
                    return "/merchant/create";
                }
            } else if ("core".equalsIgnoreCase(merchant.getProduct().getName()) || "flutterwave core".equalsIgnoreCase(merchant.getProduct().getName())) {

                CoreMerchant coreMerchant = coreMerchantDao.findByKey("companyname", merchant.getCompanyName());

                if (coreMerchant != null) {
                    JsfUtil.addErrorMessage("merchant with name " + merchant.getCompanyName() + " exists");
                    return "/merchant/create";
                }
            }

            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");

            MerchantM merchantM = new MerchantM();
            merchantM.setCreatedOn(new Date());
            merchantM.setCreatedBy(user);
            merchantM.setEnabled(true);
            merchantM.setStatus(Status.PENDING);
            merchantM.setRcNumber(merchant.getRcNumber());

//            if("Rave".equalsIgnoreCase(merchant.getProduct().getName())){
            merchantM.setFirstName(merchant.getFirstName());
            merchantM.setLastName(merchant.getLastName());
            merchantM.setCompanyName(merchant.getCompanyName());
            merchantM.setAddressLine1(merchant.getAddressLine1());
            merchantM.setAddressLine2(merchant.getAddressLine2());
            merchantM.setBank(merchant.getBank());
            merchantM.setContactPerson(merchant.getContactPerson());
            merchantM.setCountry(merchant.getCountry());
            merchantM.setEmail(merchant.getEmail());
            merchantM.setEnabled(true);
            merchantM.setPhone(merchant.getPhone());
            merchantM.setCurrency(merchant.getCurrency());
            merchantM.setAccountNo(merchant.getAccountNo());
            merchantM.setProduct(merchant.getProduct());
            merchantM.setMerchantType(MerchantType.Corporate);
            merchantM.setSector(merchant.getSector());
//                merchantM.set
//            }

            createMerchant(merchantM);
//            merchantMDao.create(merchantM);

            Log log = new Log();
            log.setAction("Create merchant: " + merchant.getCompanyName());
            log.setCreatedOn(new Date());
            log.setDescription("merchant has been created and submitted for approval successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            merchant = new Merchant();

            logService.log(log);

            recreateMerchant();
            JsfUtil.addSuccessMessage(String.format("Merchant %s has been added successfully waiting for approval", merchantM.getCompanyName()));

        } catch (DatabaseException de) {

            try {
                Log log = new Log();
                log.setAction("create merchant " + merchant.getCompanyName());
                log.setCreatedOn(new Date());
                log.setDescription("creation of merchant");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (de != null) {

                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception ex) {
            }

            if (de != null) {
                de.printStackTrace();
                JsfUtil.addErrorMessage("Unable to create merchant, plese try again later");
            }
        }

        recreate();

        return "/merchant/create";
    }

    @Transactional
    public void createMerchant(MerchantM merchant) throws DatabaseException {

        merchantMDao.create(merchant);
    }

    public GenericMerchant getGenericMerchant() {

        return genericMerchant;
    }

    // THis is merchant section
    public DataModel getUnauthMerchant() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("merchantupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauthMerchant = null;
            session.setAttribute("merchantupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth Merchant");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth Merchant");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        if (paginationUnauthMerchant == null) {
            unauthdataModel = getPaginationUnauthMerchant().createPageDataModel();
        }

        return unauthdataModel;
    }

    public PaginationHelper getPaginationUnauthMerchant() {

        if (paginationUnauthMerchant == null) {
            paginationUnauthMerchant = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return merchantMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(merchantMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauthMerchant;
    }

    // end of merchant section
    /* This section is for merchant config*/
    public String createMerchantConfig() {

        try {

            List<MerchantConfig> merchantConfigs = merchantConfigDao.findByMerchant(merchantConfig.getCoreMerchantId(), merchantConfig.getCurrency());

            if (merchantConfigs != null && !merchantConfigs.isEmpty()) {

                Log log = new Log();
                log.setAction("creating merchant configuration");
                log.setCreatedOn(new Date());
                log.setDescription("creating merchant configuration");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Merchant configuration creation on merchant " + merchantConfig.getCoreMerchantName() + " exists");

                JsfUtil.addErrorMessage("Config exists for selected merchant ");
                return "/config/create";
            }

            merchantConfig.setCreatedOn(new Date());
            merchantConfig.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));

            //productDao.create(product);
            MerchantConfigM merchantConfigM = new MerchantConfigM();
            merchantConfigM.setCreatedBy(merchantConfig.getCreatedBy());
            merchantConfigM.setCreatedOn(merchantConfig.getCreatedOn());

            CoreMerchant coreMerchant = coreMerchantDao.find(merchantConfig.getCoreMerchantId());

            merchantConfigM.setMoneywaveMerchant(merchantConfig.getMoneywaveMerchant());
            merchantConfigM.setStatus(Status.PENDING);
            merchantConfigM.setCoreMerchantId(merchantConfig.getCoreMerchantId());
            merchantConfigM.setCoreMerchantName(coreMerchant.getCompanyname());
            merchantConfigM.setSettlementCycle(merchantConfig.getSettlementCycle());
            merchantConfigM.setSettlementType(merchantConfig.getSettlementType());
            merchantConfigM.setCurrency(merchantConfig.getCurrency());

            if (merchantConfigM.getMoneywaveMerchant() > 0 && merchantConfig.getSettlementType() != SettlementType.Wallet) {
                merchantConfigM.setMoneywaveMerchant(0);
            }

            merchantConfigMDao.create(merchantConfigM);

            Log log = new Log();
            log.setAction("creating merchant config");
            log.setCreatedOn(new Date());
            log.setDescription("created merchant config");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Merchant config has been created successfully and sent for approval");

            logService.log(log);

            merchantConfig = new MerchantConfig();

            merchantConfigM = new MerchantConfigM();

            recreateConfig();

            return "/merchant/config/create";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);

            try {
                Log log = new Log();
                log.setAction("create merchant config");
                log.setCreatedOn(new Date());
                log.setDescription("create merchant config");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "/merchant/config/create";
    }

    public PaginationHelper getPaginationConfig() {

        if (paginationMerchantConfig == null) {
            paginationMerchantConfig = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {

                    return total;
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(merchantConfigDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(MerchantFeeDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationMerchantConfig;
    }

    public DataModel getUnauthConfig() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("configupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauthConfig = null;
            session.setAttribute("configupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth Merchant Config");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth Merchant Config");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        if (paginationUnauthConfig == null) {
            unauthdataModelConfig = getPaginationUnauthConfig().createPageDataModel();
        }

        return unauthdataModelConfig;
    }

    public PaginationHelper getPaginationUnauthConfig() {

        if (paginationUnauthConfig == null) {
            paginationUnauthConfig = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return merchantConfigMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(merchantConfigMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauthConfig;
    }

    public DataModel getAllConfig() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("configupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationMerchantConfig = null;
            session.setAttribute("configupdated", false);
        }

        if (paginationMerchantConfig == null) {
            dataModelConfig = getPaginationConfig().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Merchant config");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Merchant config");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        return dataModelConfig;
    }

    /* end of merchant config section*/
    public Merchant getMerchant() {
        if (merchant == null) {
            merchant = new Merchant();
        }

        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public MerchantConfig getMerchantConfig() {

        if (merchantConfig == null) {
            merchantConfig = new MerchantConfig();
        }

        return merchantConfig;
    }

    public void setMerchantConfig(MerchantConfig merchantConfig) {
        this.merchantConfig = merchantConfig;
    }

    private GenericMerchant genericMerchant;
    private Merchant merchant;
    private MerchantConfig merchantConfig;

    public SettlementType[] getSettlementTypes() {

        return SettlementType.values();
    }

    public CoreMerchant getCoreMerchant(long id) throws DatabaseException {
        return coreMerchantDao.find(id);
    }

    public MoneywaveMerchant getMoneywaveMerchant(long id) throws DatabaseException {
        return moneywaveMerchantDao.find(id);
    }

    public String merchantAnalysis(String product, String merchantId) {

        this.product = product;
        this.merchantId = merchantId;

        return "/merchant/index";
    }

    public String getSelectedProduct() {
        return product;
    }

    public String getSelectedMerchantId() {
        return merchantId;
    }

    public String getMerchantName() {

        return merchantName;
    }

    private String product;
    private String merchantId;
    private String merchantName;

    @FacesConverter("coreMerchantConverter")
    public static class coreMerchantConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {

            if (value == null || value.isEmpty()) {
                return null;
            }

            try {

                MerchantController merchantController = (MerchantController) context.getApplication()
                        .getELResolver().getValue(context.getELContext(), null, "merchantController");

                CoreMerchant p = merchantController.getCoreMerchant(Long.parseLong(value));
                return p;

            } catch (Exception ex) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return null;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {

            if (value == null) {
                return null;
            }

            if (!(value instanceof CoreMerchant)) {
                return null;
            }

            CoreMerchant m = (CoreMerchant) value;

            return m.getId() + "";
        }
    }

    @FacesConverter("moneywaveMerchantConverter")
    public static class moneywaveMerchantConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {

            if (value == null || value.isEmpty()) {
                return null;
            }

            try {

                MerchantController merchantController = (MerchantController) context.getApplication()
                        .getELResolver().getValue(context.getELContext(), null, "merchantController");

                MoneywaveMerchant p = merchantController.getMoneywaveMerchant(Long.parseLong(value));
                return p;

            } catch (Exception ex) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }

            return null;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {

            if (value == null) {
                return null;
            }

            if (!(value instanceof CoreMerchant)) {
                return null;
            }

            MoneywaveMerchant m = (MoneywaveMerchant) value;

            return m.getId() + "";
        }
    }

    public void exportCompliance(String status, String search, String product, String country, String createdBetween, String format) {

        status = Utility.emptyToNull(status);

        if ("any".equalsIgnoreCase(status)) {
            status = null;
        }

        if ("any".equalsIgnoreCase(country)) {
            country = null;
        }

        Date startDate = null, endDate = null;

        if (Utility.emptyToNull(createdBetween) != null) {

            try {
                String[] splitDate = createdBetween.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                startDate = calendar.getTime();

                endDate = dateFormat.parse(splitDate[1]);

                calendar.setTime(endDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
//                calendar.set(Calendar.MILLISECOND, 59);

                endDate = calendar.getTime();

            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        Page<MerchantCompliance> page = merchantCompliantDao.findMerchant(0, 0, startDate, endDate, Utility.emptyToNull(product), null, Utility.emptyToNull(search),
                status == null ? null : ("YES".equalsIgnoreCase(status) || "REJECTED".equalsIgnoreCase(status)),
                Utility.emptyToNull(country), "REJECTED".equalsIgnoreCase(status));

        List<MerchantCompliance> compliances = page.getContent();

        if (compliances == null) {
            compliances = new ArrayList<>();
        }

        List<GetComplianceModel> cModel = compliances.stream().map((MerchantCompliance x) -> {
            return GetComplianceModel.getComplianceModel(x);
        })
                .collect(Collectors.<GetComplianceModel>toList());

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        if ("csv".equalsIgnoreCase(format)) {

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
            List<String> headers = (List) Utility.getClassFields(GetComplianceModel.class);

            String output = CsvUtil.writeLine(headers, (List) cModel);

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=compliance.csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } else {

            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("TransactionRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) Utility.getClassFields(GetComplianceModel.class), "Transaction Export", null);

            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) cModel);

            response.setHeader("Content-Disposition", "attachment; filename=compliance.xls");
            response.setContentType("application/vnd.ms-excel");

            ServletOutputStream out = null;

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }
    }

}

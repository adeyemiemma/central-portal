/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.util.Utility;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResendComplianceInformation {

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the contactAddress
     */
    public String getContactAddress() {
        return contactAddress;
    }

    /**
     * @param contactAddress the contactAddress to set
     */
    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the rcNumber
     */
    public String getRcNumber() {
        return rcNumber;
    }

    /**
     * @param rcNumber the rcNumber to set
     */
    public void setRcNumber(String rcNumber) {
        this.rcNumber = rcNumber;
    }

    /**
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * @param companyName the companyName to set
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    
    @NotBlank(message = "Registration Number must not blank")
    @JsonProperty(value = "reg_number")
    private String rcNumber;
    @JsonProperty(value = "company_name")
    private String companyName;
    private String email;
    @JsonProperty(value = "contact_address")
    private String contactAddress;
    private String country;
    private String product;

    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject()
                .put("reg_number", getRcNumber())
                .put("company_name", Utility.nullToEmpty(getCompanyName()));
        
        return jSONObject.toString();
    }
    
}

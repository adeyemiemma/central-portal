/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.util.CsvUtil;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.FetchRRNModel;
import com.flutterwave.flutter.clientms.viewmodel.UploadModel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "cardController")
@SessionScoped
public class CardController implements Serializable {
    
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private ConfigurationDao configurationDao; 
    
    public UploadModel getUploadModel() {

        if (uploadModel == null) {
            uploadModel = new UploadModel();
        }

        return uploadModel;
    }

    public void setUploadModel(UploadModel uploadModel) {
        this.uploadModel = uploadModel;
    }

    private UploadModel uploadModel;
    
    public String saveCards(){
        
        try{
        String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            if (fileName.toLowerCase().endsWith("csv")) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(uploadModel.getFile().getInputStream()));

                String line = null;

                
                JSONArray jSONArray = new JSONArray();
                
                reader.lines().skip(1).forEach(lineString -> {
                    
                     if(lineString != null){
                    
                        try{
                            String[] data = lineString.split(",");
                         
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("first_name", data[0].trim())
                                    .put("last_name", data[1].trim())
                                    .put("pan", data[2].trim().replace(" ", ""))
                                    .put("expiry", data[3].trim())
                                    .put("cvv", data[4].trim())
                                    .put("card_id", data[5].trim());
                            
                            jSONArray.put(jSONObject);
                            
                        }catch(Exception ex){
                            
                        }
                     }
                            
                });
                
                String cardUrl = configurationDao.getConfig("card_mgnt_url");
                
                if(cardUrl == null)
                    cardUrl = "http://ec2-35-177-176-67.eu-west-2.compute.amazonaws.com:8080";
                
                cardUrl += "/flw-utility/api/v1/upload/card" ;
                
                String cardKey = configurationDao.getConfig("card_mgnt_api_key");
                
                if(cardKey == null)
                    cardKey = "NDg0YzM2OTgyY2RhN2VhOWY3MDRlMjk3ZGMyZjNiM2MwZThmNGM0ZGI1YjQxYmM1MjdjMWRlZWExOTllZjQ1Yg==";

                JSONObject data = new JSONObject();
                data.put("provider", "bento");
                data.put("items", jSONArray);
                
                Map<String, String> header = new HashMap<>();
                header.put("Content-Type", "application/json");
                header.put("Authorization", "Bearer "+cardKey);
                
                String response = httpUtil.doHttpPost(cardUrl, data.toString(), header);
                
                if(response == null){
                    JsfUtil.addErrorMessage("FIle upload not successful");
                }else{
                    
                    data = new JSONObject(response);
                    
                    String status = data.optString("status", null);
                    
                    if(!"success".equalsIgnoreCase(status)){
                        
                        JsfUtil.addErrorMessage("FIle upload not successful");
                    }else
                        JsfUtil.addSuccessMessage("FIle upload successful");
                }
                    
                

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to get records");
        }
        
        setUploadModel(new UploadModel());
        
        return "/card/upload";
    }
    
    public String uploadTransactions(){
        
        try{
        String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            if (fileName.toLowerCase().endsWith("csv")) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(uploadModel.getFile().getInputStream()));

                String line = null;

                
                JSONArray jSONArray = new JSONArray();
                
                reader.lines().skip(1).forEach(lineString -> {
                    
                     if(lineString != null){
                    
                        try{
                            String[] data = lineString.split(",");
                         
                            JSONObject jSONObject = new JSONObject();
                            
//                            private String pan;
//    @JsonProperty(value = "transaction_id")
//    private String transactionId;
//    @NotBlank(message = "Request date must be provided")
//    @JsonProperty(value = "request_date")
//    private String requestDate;
//    @JsonProperty(value = "purchase_amount")
//    private double purchaseAmount;
//    @NotBlank(message = "Purchase Currency must be provided")
//    @JsonProperty(value = "purchase_currency")
//    private String purchaseCurrency;
//    @JsonProperty(value = "cleared_date")
//    private String postingDate;
//    @JsonProperty(value = "cleared_amount")
//    private double clearedAmount;
//    @JsonProperty(value = "cleared_currency")
//    private String clearedCurrency;
//    private double tax;
//    private String status;
//5563374003000756	5563385645396626	Created by SVB API v0.16.147-gddbd3ca	12/08/2017	12/08/2017	GOOGLE *VAN DUKE,G.CO/HELPPAY#,CA,94043	0.08	1.00
                            

                            String typeId = data[6];
                            
                            
                            
                            String type = "Purchase";
                            
                            if("C".equalsIgnoreCase(typeId))
                                type = "Refund";
                            
                            double merchantAmount = Double.parseDouble(data[8].replace("|", "").replace(",", ""));
                            double billingAmount = Double.parseDouble(data[16].replace("|", "").replace(",", ""));
                            
                            jSONObject.put("pan", data[1].trim())
                                    .put("status", "Approved")
                                    .put("type", type)
                                    .put("request_date", data[19].trim())
                                    .put("description", data[26].trim()+" : "+data[20].trim())
                                    .put("purchase_amount", merchantAmount)
                                    .put("purchase_currency", data[10].trim())
                                    .put("authorized_amount", billingAmount)
                                    .put("authorized_currency", "USD")
                                    .put("cleared_date", data[12].trim())
                                    .put("cleared_amount", billingAmount)
                                    .put("mcc", data[25].trim())
                                    .put("mcc_description", data[26].trim())
                                    .put("merchant_name", data[20].trim())
                                    .put("merchant_id", data[27].trim())
                                    .put("cleared_currency", "USD");
                            
                            jSONArray.put(jSONObject);
                            
//                            String amount = data[7];
//                            
//                            double amountValue = Double.parseDouble(amount);
//                            
//                            String type = "Purchase";
//                            
//                            if(amountValue < 0.0)
//                                type = "Refund";
//                            
//                            jSONObject.put("pan", data[1].trim())
//                                    .put("status", "Approved")
//                                    .put("type", type)
//                                    .put("request_date", data[3].trim())
//                                    .put("request_date", data[3].trim())
//                                    .put("description", data[5].trim())
//                                    .put("purchase_amount", amount)
//                                    .put("purchase_currency", "USD")
//                                    .put("authorized_amount", amount)
//                                    .put("authorized_currency", "USD")
//                                    .put("cleared_date", data[4].trim())
//                                    .put("cleared_amount", amount)
//                                    .put("merchant_name", amount)
//                                    .put("cleared_currency", "USD");
//                            
//                            jSONArray.put(jSONObject);
                            
                        }catch(Exception ex){
                            
                        }
                     }
                            
                });
                
                String cardUrl = configurationDao.getConfig("card_mgnt_url");
                
                if(cardUrl == null)
                    cardUrl = "http://ec2-35-177-176-67.eu-west-2.compute.amazonaws.com:8080";
                
                cardUrl += "/flw-utility/api/v1/upload/card/transaction/new" ;
                
                String cardKey = configurationDao.getConfig("card_mgnt_api_key");
                
                if(cardKey == null)
                    cardKey = "NDg0YzM2OTgyY2RhN2VhOWY3MDRlMjk3ZGMyZjNiM2MwZThmNGM0ZGI1YjQxYmM1MjdjMWRlZWExOTllZjQ1Yg==";

                JSONObject data = new JSONObject();
                data.put("provider", "svb");
                data.put("items", jSONArray);
                
                Map<String, String> header = new HashMap<>();
                header.put("Content-Type", "application/json");
                header.put("Authorization", "Bearer "+cardKey);
                
                String response = httpUtil.doHttpPost(cardUrl, data.toString(), header);
                
                if(response == null){
                    JsfUtil.addErrorMessage("FIle upload not successful");
                }else{
                    
                    data = new JSONObject(response);
                    
                    String status = data.optString("status", null);
                    
                    if(!"success".equalsIgnoreCase(status)){
                        
                        JsfUtil.addErrorMessage("FIle upload not successful");
                    }else
                        JsfUtil.addSuccessMessage("FIle upload successful");
                }
                    
                

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to get records");
        }
        
        setUploadModel(new UploadModel());
        
        return "/card/upload_transaction";
    }
}

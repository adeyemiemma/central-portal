/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.api.model.ActivateMposTerminalRequest;
import com.flutterwave.flutter.clientms.controller.api.model.AttachPosIdRequest;
import com.flutterwave.flutter.clientms.controller.api.model.CreateMposMerchantRequest;
import com.flutterwave.flutter.clientms.controller.api.model.CreatePosOrderRequest;
import com.flutterwave.flutter.clientms.controller.api.model.GenericPosTransactionRequest;
import com.flutterwave.flutter.clientms.controller.api.model.GetMposTerminalRequest;
import com.flutterwave.flutter.clientms.controller.api.model.GetPosMerchantRequest;
import com.flutterwave.flutter.clientms.controller.api.model.GetPosTransactionRequest;
import com.flutterwave.flutter.clientms.controller.api.model.GetTerminalInfo;
import com.flutterwave.flutter.clientms.controller.api.model.LogPosTransactionRequest;
import com.flutterwave.flutter.clientms.controller.api.model.PosSettlementReportRequest;
import com.flutterwave.flutter.clientms.controller.api.model.PosTerminalMappingRequest;
import com.flutterwave.flutter.clientms.controller.api.model.PosTransactionFromProviderRequest;
import com.flutterwave.flutter.clientms.controller.api.model.PosTransactionRescheduleRequest;
import com.flutterwave.flutter.clientms.controller.api.model.PosTransactionVerificationResponse;
import com.flutterwave.flutter.clientms.controller.api.model.PosVerificationRequest;
import com.flutterwave.flutter.clientms.controller.api.model.ProvisionTerminalRequest;
import com.flutterwave.flutter.clientms.controller.api.model.QueryPosTransactionRequest;
import com.flutterwave.flutter.clientms.controller.api.model.RegisterTerminalRequest;
import com.flutterwave.flutter.clientms.controller.api.model.UpdateTerminalRequest;
import com.flutterwave.flutter.clientms.dao.ApiUserDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.dao.PosOrderDao;
//import com.flutterwave.flutter.clientms.dao.PosOrderDao;
import com.flutterwave.flutter.clientms.dao.PosTerminalDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionNewDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantCustomerDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantProductDao;
import com.flutterwave.flutter.clientms.model.ApiUser;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Merchant;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosMerchantCustomer;
import com.flutterwave.flutter.clientms.model.PosMerchantProduct;
import com.flutterwave.flutter.clientms.model.PosOrder;
//import com.flutterwave.flutter.clientms.model.PosOrder;
//import com.flutterwave.flutter.clientms.model.PosOrder;
//import com.flutterwave.flutter.clientms.model.PosOrder;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.model.PosTransactionNew;
import com.flutterwave.flutter.clientms.service.ETopPosService;
import com.flutterwave.flutter.clientms.service.GAPosService;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.PosReversalCache;
import com.flutterwave.flutter.clientms.service.RaveApiService;
import com.flutterwave.flutter.clientms.service.SendBoxPosService;
import com.flutterwave.flutter.clientms.service.TerminalService;
import com.flutterwave.flutter.clientms.service.UtilityService;
import com.flutterwave.flutter.clientms.service.WakanowService;
import com.flutterwave.flutter.clientms.service.model.RaveKeyItem;
import com.flutterwave.flutter.clientms.service.model.SendBoxQueryModel;
import com.flutterwave.flutter.clientms.service.model.WakanowCustomer;
import com.flutterwave.flutter.clientms.service.model.WakanowCustomerInformationResponse;
import com.flutterwave.flutter.clientms.util.CryptoUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PosMerchantCategory;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.PosTransactionViewModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Level;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Path(value = "/v1")
@RequestScoped
@Produces(value = MediaType.APPLICATION_JSON)
public class PosApiController {

    @EJB
    private PosMerchantDao posMerchantDao;
    @EJB
    private PosTerminalDao posTerminalDao;
    @EJB
    private ApiUserDao apiUserDao;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private TerminalService terminalService;
    @EJB
    private LogService logService;
    @EJB
    private PosTransactionDao posTransactionDao;
    @EJB
    private UtilityService utilityService;
    @EJB
    private ETopPosService eTopPosService;
    @EJB
    private PosOrderDao posOrderDao;
    @EJB
    private PosTransactionNewDao posTransactionNewDao;
    @EJB
    private GAPosService gaPosService;
    @EJB
    private PosMerchantCustomerDao posMerchantCustomerDao;
    @EJB
    private WakanowService wakanowService;
    
    @EJB
    private PosMerchantProductDao posMerchantProductDao;
    
    @Inject
    private SendBoxPosService sendBoxPosService;
    
    @Inject 
    private PosReversalCache posReversalCache;
    
    @Inject
    private RaveApiService raveApiService;

    @Context
    private HttpServletRequest servletRequest;

    @Path(value = "pos/merchant/register")
    @POST
    public Response registerMerchant(@HeaderParam(value = "uniqueid") String uniqueId, @Valid CreateMposMerchantRequest request) {

        Map<String, String> response = new HashMap<>();
        try {
            Log log = new Log();
            log.setAction("Creating MPOS Merchant");
            log.setDescription(request.toString());
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setUsername("API-" + uniqueId);

            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided in header");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Create MPOS Merchant", uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Create MPOS Merchant", uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashableString();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Create MPOS Merchant", uniqueId);
            }

            PosMerchant mposMerchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());

            if (mposMerchant != null) {

//                response.put("responsecode", "06");
//                response.put("responsemessage", "Duplicate merchant with code " + request.getMerchantCode());

                response.put("responsecode", "00");
                response.put("responsemessage", "Merchant information retrieved successfully ");
                response.put("merchantcode", request.getMerchantCode());
                response.put("merchantid", request.getMerchantId());
                response.put("posid", mposMerchant.getPosId());
            
                return returnResponse(Response.Status.OK, response, "Create MPOS Merchant", uniqueId);
            }

            if(request.getAddress().equalsIgnoreCase("NA") || request.getAddress().equalsIgnoreCase("N/A")){

                response.put("responsecode", "08");
                response.put("responsemessage", "Valid merchant address must be provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Create MPOS Merchant", uniqueId);
            }
            
            if(request.getName().equalsIgnoreCase("NA") || request.getName().equalsIgnoreCase("N/A")){

                response.put("responsecode", "08");
                response.put("responsemessage", "Valid merchant name must be provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Create MPOS Merchant", uniqueId);
            }
                
            
            mposMerchant = new PosMerchant();
            mposMerchant.setAddress(request.getAddress());
            mposMerchant.setCreatedOn(new Date());
            mposMerchant.setMerchantCode(request.getMerchantCode());
            mposMerchant.setMerchantId(request.getMerchantId());
            mposMerchant.setName(request.getName());
            mposMerchant.setPhone(request.getPhone());
            mposMerchant.setCreatedBy(uniqueId);

            String posId = eTopPosService.createMerchant(request.getMerchantCode(), request.getName());

            if (posId == null) {

                response.put("responsecode", "07");
                response.put("responsemessage", "Unable to create merchant");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Create MPOS Merchant", uniqueId);
            }

            mposMerchant.setPosId(posId);
            mposMerchant = posMerchantDao.create(mposMerchant);

            if (mposMerchant == null) {

                response.put("responsecode", "07");
                response.put("responsemessage", "Unable to create merchant");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Create MPOS Merchant", uniqueId);
            }

            response.put("responsecode", "00");
            response.put("responsemessage", "Merchant has been created successfully");
            response.put("merchantcode", request.getMerchantCode());
            response.put("merchantid", request.getMerchantId());
            response.put("posid", posId);

            return returnResponse(Response.Status.OK, response, "Create MPOS Merchant", uniqueId);
        } catch (DatabaseException ex) {
            Logger.getLogger(PosApiController.class.getName()).error("Error when creating mpos merchant", ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path(value = "pos/terminal/provision")
    @POST
    public Response requestTerminal(@HeaderParam(value = "uniqueid") String uniqueId, @Valid ProvisionTerminalRequest request) {

        try {
            Map<String, String> response = new HashMap<>();

            Log log = new Log();
            log.setAction("Provisioning MPOS Terminal");
            log.setDescription(request.toString());
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setUsername("API-" + uniqueId);

            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Provision MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Provision MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashableString();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Provision MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            PosMerchant mposMerchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());

            if (mposMerchant == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "unknown merchant with code " + request.getMerchantCode());

                return returnResponse(Response.Status.BAD_REQUEST, response, "Provision MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            if (!request.getMerchantId().equalsIgnoreCase(mposMerchant.getMerchantId())) {

                response.put("responsecode", "06");
                response.put("responsemessage", "unknown merchant with code " + request.getMerchantCode());

                return returnResponse(Response.Status.BAD_REQUEST, response, "Provision MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            Map<String, Object> value = terminalService.provisionTerminal(uniqueId, request, mposMerchant);

            return returnResponse(Response.Status.OK, value, "Provision MPOS Terminal for " + request.getMerchantCode(), uniqueId);
        } catch (Exception ex) {
            Logger.getLogger(PosApiController.class.getName()).error("Error provisioning terminal", ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path(value = "pos/terminals")
    @POST
    public Response getMerchantTerminals(@HeaderParam(value = "uniqueid") String uniqueId, @Valid GetMposTerminalRequest request) {

        try {
            Map<String, Object> response = new HashMap<>();

            Log log = new Log();
            log.setAction("Listing MPOS Terminal");
            log.setDescription(request.toString());
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setUsername("API-" + uniqueId);

            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, "List MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Listing MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashableString();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Listing MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            PosMerchant mposMerchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());

            if (mposMerchant == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "unknown merchant with code " + request.getMerchantCode());

                return returnResponse(Response.Status.BAD_REQUEST, response, "Listing MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            if (!request.getMerchantId().equalsIgnoreCase(mposMerchant.getMerchantId())) {

                response.put("responsecode", "06");
                response.put("responsemessage", "unknown merchant with code " + request.getMerchantCode());

                return returnResponse(Response.Status.BAD_REQUEST, response, "Provision MPOS Terminal for " + request.getMerchantCode(), uniqueId);
            }

            List<PosTerminal> list = posTerminalDao.findByMerchant(mposMerchant);

            List terminalList = new ArrayList<>();

            if (list != null && !list.isEmpty()) {

                terminalList = list.stream().map((PosTerminal terminal) -> {
                    Map<String, Object> terminalMap = new HashMap<>();
                    terminalMap.put("terminalid", terminal.getTerminalId());
                    terminalMap.put("pwcmerchantid", terminal.getMerchant().getMerchantId());
                    terminalMap.put("merchantcode", terminal.getMerchant().getMerchantCode());
                    terminalMap.put("activatedon", Utility.formatDate(terminal.getActivatedOn(), "yyyy-MM-dd HH:mm:ss"));
                    terminalMap.put("assignedon", Utility.formatDate(terminal.getAssignedOn(), "yyyy-MM-dd HH:mm:ss"));
                    terminalMap.put("enabled", terminal.isEnabled());
                    terminalMap.put("serialno", terminal.getSerialNo());
                    terminalMap.put("model", terminal.getModel());
                    terminalMap.put("activationcode", terminal.getActivationCode());
                    terminalMap.put("activated", terminal.isActivated());

                    return terminalMap;
                }).collect(Collectors.<Map>toList());
            }

            response.put("responsecode", "00");
            response.put("responsemessage", "Successful");
            response.put("merchantcode", request.getMerchantCode());
            response.put("pwcmerchantid", request.getMerchantId());
            response.put("terminals", terminalList);

            return returnResponse(Response.Status.OK, response, "Listing MPOS Terminal for " + request.getMerchantCode(), uniqueId);
        } catch (DatabaseException ex) {
            Logger.getLogger(PosApiController.class.getName()).error("Error list mpos terminals", ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path(value = "pos/terminal/activate")
    @POST
    public Response activateTerminal(@HeaderParam(value = "uniqueid") String uniqueId, @Valid ActivateMposTerminalRequest request) {

        try {
            Map<String, Object> response = new HashMap<>();

            Log log = new Log();
            log.setAction("Activating MPOS Terminal");
            log.setDescription(request.toString());
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setUsername("API-" + uniqueId);

            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Activate MPOS Terminal for " + request.getActivationCode(), uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Activating MPOS Terminal with code " + request.getActivationCode(), uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashableString();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Listing MPOS Terminal for " + request.getActivationCode(), uniqueId);
            }

            PosTerminal mposTerminal = posTerminalDao.findByKey("activationCode", request.getActivationCode());

            if (mposTerminal == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "unknown terminal with code " + request.getActivationCode());

                return returnResponse(Response.Status.BAD_REQUEST, response, "Activate MPOS Terminal with code " + request.getActivationCode(), uniqueId);
            }

            PosMerchant mposMerchant = mposTerminal.getMerchant();

            if (mposMerchant == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "unknown terminal not provisioned yet");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Activate MPOS Terminal with code " + request.getActivationCode(), uniqueId);
            }

//            if (request.getMerchantId().equalsIgnoreCase(mposMerchant.getMerchantId())) {
//                
//                response.put("responsecode", "06");
//                response.put("responsemessage", "Terminal does not exist");
//                
//                return returnResponse(Response.Status.BAD_REQUEST, response, "Activate MPOS Terminal with code " + request.getActivationCode(), uniqueId);
//            }
            String terminalId = mposTerminal.getTerminalId();

            JSONObject jSONObject = terminalService.activateTerminal(terminalId);

            if (jSONObject == null) {
                response.put("responsecode", "10");
                response.put("responsemessage", "Unable to activate terminal");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Activate MPOS Terminal with code " + request.getActivationCode(), uniqueId);
            }

            jSONObject.put("agent_name", mposMerchant.getName());

            String responseCode = jSONObject.optString("responseCode", "99");

            String ipAddress = servletRequest.getRemoteAddr();

            String remoteAddr = servletRequest.getHeader("X-FORWARDED-FOR");

            ipAddress += " : " + remoteAddr;

            mposTerminal.setActivatedBy(ipAddress);
            mposTerminal.setActivationResponse(jSONObject.toString());

            if ("00".equalsIgnoreCase(responseCode)) {
//                response.put("responsecode", "00");
//                response.put("responsemessage", "Terminal activated successfully");
//                response.put("data", jSONObject.toMap());
                mposTerminal.setActivated(true);
                mposTerminal.setActivatedOn(new Date());
                mposTerminal.setEnabled(true);
            }

            posTerminalDao.update(mposTerminal);

            return returnResponse(Response.Status.OK, jSONObject.toMap(), "Activate MPOS Terminal with code " + request.getActivationCode(), uniqueId);
        } catch (Exception ex) {
            Logger.getLogger(PosApiController.class.getName()).error("Error activating terminal", ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path(value = "pos/terminal/register")
    @POST
    public Response registerTerminal(@Valid RegisterTerminalRequest request, @HeaderParam(value = "uniqueid") String uniqueId) {

        Map<String, Object> response = new HashMap<>();

        try {
            Log log = new Log();
            log.setAction("Activating MPOS Terminal");
            log.setDescription(request.toString());
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setUsername("API-" + uniqueId);

            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Registering MPOS Terminal for ", uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Registering POS Terminal ", uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashable();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Registering MPOS Terminal for ", uniqueId);
            }

            PosMerchant merchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());

            if (merchant == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Merchant with code does not exists");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Registering MPOS Terminal for ", uniqueId);
            }

            if (!request.getMerchantId().equalsIgnoreCase(merchant.getMerchantId())) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Terminal does not exist for merchant");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Registering MPOS Terminal for ", uniqueId);
            }

            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", request.getTerminalId());

            if (posTerminal != null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Terminal with TID " + request.getTerminalId() + "  exists");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Registering MPOS Terminal for ", uniqueId);
            }

            posTerminal = posTerminalDao.findByKey("serialNo", request.getSerialNo());

            if (posTerminal != null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Terminal with Serial No " + request.getSerialNo() + "  exists");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Registering MPOS Terminal for ", uniqueId);
            }

            posTerminal = new PosTerminal();
            posTerminal.setAssignedOn(new Date());
            posTerminal.setCreatedOn(new Date());
            posTerminal.setBatchNo(request.getBatchNo());
            posTerminal.setDateIssued(request.getDateIssued());
            posTerminal.setFccId(request.getFccId());
            posTerminal.setTerminalId(request.getTerminalId());
            posTerminal.setMerchant(merchant);
            posTerminal.setSerialNo(request.getSerialNo());
            posTerminal.setReceivedBy(request.getReceivedBy());
            posTerminal.setModel(request.getModel());
            posTerminal.setEnabled(false);

            posTerminalDao.create(posTerminal);

            response.put("responsecode", "00");
            response.put("responsemessage", "Terminal has been registered successfully");

            return returnResponse(Response.Status.OK, response, "Registering MPOS Terminal for ", uniqueId);
        } catch (DatabaseException ex) {
            Logger.getLogger(PosApiController.class.getName()).error(null, ex);
        }

        response.put("responsecode", "RR");
        response.put("responsemessage", "Generic Error");

        return returnResponse(Response.Status.INTERNAL_SERVER_ERROR, response, "Registering MPOS Terminal for ", uniqueId);
    }

    @Path(value = "pos/transaction")
    @POST
    public Response getTransaction(@Valid GetPosTransactionRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        try {
            Map<String, Object> response = new HashMap<>();
            Log log = new Log();
            log.setAction("Get Transaction");
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername(uniqueId);
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);
            String title = "Fetching MPOS Transaction";
            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);
            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            String requestString = apiUser.getPrivateKey() + "" + request.toHashable();
            String hashString = Utility.sha512(requestString);
            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            if (request.getPosId() != null) {
                PosMerchant merchant = posMerchantDao.findByKey("posId", request.getPosId());

                if (merchant == null) {

                    response.put("responsecode", "06");
                    response.put("responsemessage", "Merchant with pos id does not exists");

                    return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                }
            }

            if (request.getTerminalId() != null) {
                PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", request.getTerminalId());

                if (posTerminal == null) {

                    response.put("responsecode", "06");
                    response.put("responsemessage", "Terminal id does not exist");

                    return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                }

                if (request.getPosId() == null) {

                    response.put("responsecode", "06");
                    response.put("responsemessage", "Pos Id must be provided");

                    return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                }
            }

            Date startDate = null, endDate = null;

            if (request.getStartDate() == null && request.getEndDate() != null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Both start and end date must be provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            if (request.getStartDate() != null && request.getEndDate() == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Both start and end date must be provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            if (request.getStartDate() != null && !request.getStartDate().isEmpty()) {

                startDate = Utility.parseDate(request.getStartDate(), "yyyy-MM-dd HH:mm:ss");

                if (startDate == null) {
                    response.put("responsecode", "06");
                    response.put("responsemessage", "Invalid start date (yyyy-MM-dd HH:mm:ss)");

                    return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                }
            }

            if (request.getEndDate() != null && !request.getEndDate().isEmpty()) {

                endDate = Utility.parseDate(request.getEndDate(), "yyyy-MM-dd HH:mm:ss");

                if (endDate == null) {
                    response.put("responsecode", "06");
                    response.put("responsemessage", "Invalid end date (yyyy-MM-dd HH:mm:ss)");

                    return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                }
            }

            int pageNo = request.getPageNum();

            if (request.getPageNum() > 0) {
                pageNo = request.getPageNum() - 1;
            }

            Page<PosTransaction> page = posTransactionDao.find(pageNo * request.getPageSize(), request.getPageSize(),
                    request.getTerminalId(), request.getMerchantId(), null, request.getMerchantCode(),
                    startDate, endDate, null, null, null, null, request.getPosId(), null, request.getSearch());
            response.put("pageno", request.getPageNum());
            response.put("pagesize", request.getPageSize());
            response.put("responsecode", "00");
            response.put("responsemessage", "successful");
            if (page == null || page.getCount() <= 0) {

                response.put("total", 0);
                response.put("transactions", new ArrayList());
            } else {

                response.put("total", page.getCount());

                List<PosTransactionViewModel> list = page.getContent().stream().map((PosTransaction posTransaction) -> {

                    PosTransactionViewModel model = PosTransactionViewModel.from(posTransaction);
                    model.setMerchantId(request.getMerchantId());
//                    model.setMerchantCode(hashString);

                    try {
                        PosTerminal terminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());

                        if (terminal != null) {

                            if (model.getPosId() != null) {

                                PosMerchant merchant = posMerchantDao.findByKey("posId", model.getPosId());

                                if (merchant != null) {
                                    model.setMerchantCode(merchant.getMerchantCode());
                                }
                            } else if (terminal.getMerchant() != null) {
                                model.setMerchantCode(terminal.getMerchant().getMerchantCode());
                            }

                        }

                    } catch (DatabaseException ex) {
                        Logger.getLogger(PosApiController.class.getName()).error(null, ex);
                    }

                    return model;
                }).collect(Collectors.<PosTransactionViewModel>toList());

                response.put("transactions", list);
            }
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(PosApiController.class.getName()).error(null, ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path(value = "pos/transaction/provider/log")
    @POST
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response logProviderTransaction(@Valid GenericPosTransactionRequest request, @HeaderParam(value = "uniqueid") String uniqueId) {

        try {

            Map<String, Object> response = new HashMap<>();

            Log log = new Log();
            log.setAction("Logging Transaction");
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername(uniqueId);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log);

            String title = "Logging MPOS Transaction";

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.getProvider();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            PosMerchant merchant = null;

            String orderId = null, customerId = null;

            PosMerchantCustomer posMerchantCustomer = null;

            String merchantReference = null;

            PosTerminal posTerminal;

            JSONObject object = new JSONObject(request.getData());
            
            PosMerchantProduct posMerchantProduct = null;

            switch (request.getProvider().toLowerCase()) {

                case "itex": {

                    String terminalId = object.optString("terminalid");

                    response.put("terminalid", terminalId);

                    posTerminal = posTerminalDao.findByKey("terminalId", terminalId);

                    String posId = object.optString("posid");

                    if (Utility.emptyToNull(posId) != null) {

                        merchant = posMerchantDao.findByKey("posId", posId);

                        if (merchant == null) {

                            PosOrder order = posOrderDao.findByKey("posOrderId", posId);

                            if (order != null) {

                                merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());

                                orderId = order.getCustomerOrderId();

                            } else {

                                posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", posId);

                                if (posMerchantCustomer == null) {

                                    PosMerchant posMerchant = posTerminal.getMerchant();

                                    if (posMerchant != null) {

//                                        if (posMerchant.getCategory() == PosMerchantCategory.NPP) {

                                            merchant = posMerchant;

                                            merchantReference = posId;
//                                        }

                                    } else {

                                        response.put("responsecode", "06");
                                        response.put("responsemessage", "Merchant with pos id does not exists");

                                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                                    }
                                }

                                merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());

                                customerId = posMerchantCustomer.getCustomerId();

                            }
                        }
                    }

                    if (posTerminal == null) {

                        response.put("responsecode", "06");
                        response.put("responsemessage", "Terminal does not exist");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                    
                    if(merchant == null)
                        merchant = posTerminal.getMerchant();

                    String amountString = object.optString("amount");

                    double amount = Double.parseDouble(amountString);

                    amount = amount / 100;
                    
                    PosTransaction posTransaction = new PosTransaction();

                    posTransaction.setAmount(amount);

                    posTransaction.setCreatedOn(new Date());
                    posTransaction.setCurrency("NGN");
                    posTransaction.setCurrencyCode("566");

                    String requestDateString = object.optString("transactiontime");

//                    Date requestDate = Utility.parseDate(requestDateString, "yyyy-MM-dd HH:mm:ss");
                    
                    Date requestDate =  Utility.parseDate(requestDateString, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    
//                    if(requestDate == null || Utility.isExpired(requestDate) == true){
//                        requestDate = Utility.parseDate(requestDateString, "yyyy-MM-dd HH:mm:ss");
//                    }
                              
                    if(requestDate == null || Utility.isExpired(requestDate) == true)
                        requestDate = Utility.parseDate(requestDateString, "dd-MM-yyyy HH:mm:ss");

                    String rrn = object.optString("rrn");

                    try {

                        Long rrnLong = Long.parseLong(rrn);

                        rrn = String.valueOf(rrnLong);

                    } catch (Exception ex) {

                        response.put("responsecode", "07");
                        response.put("responsemessage", "RRN must be numeric " + rrn);

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    
                    PosTransaction transaction;
                    
                    if(requestDate == null)
                        transaction = posTransactionDao.findRRNTerminal(rrn, terminalId, requestDate);
                    else
                        transaction = posTransactionDao.findRRNTerminalOnly(rrn, terminalId);
                    
                    Boolean reversal = object.getBoolean("reversal");
                    
                    if(Objects.equals(reversal, Boolean.TRUE) && transaction == null){

                        String transRef = terminalId + "" + rrn + "" + amountString + "" + Utility.formatDate(requestDate, "yyyyMMdd");
                        
                        posReversalCache.add(transRef, request.getData());
                        
                        response.put("responsecode", "08");
                        response.put("responsemessage", "Transaction does not exist");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    if (transaction != null) {

                        if (Objects.equals(reversal, Boolean.TRUE) && !transaction.isReversed()) {

                            transaction.setReversed(true);
                            transaction.setReversedOn(requestDate);

                            posTransactionDao.update(transaction);
                            
                            utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);

                            response.put("responsecode", "00");
                            response.put("responsemessage", "Reversal logged successfully");

                            return returnResponse(Response.Status.OK, response, title, uniqueId);
                        }

                        response.put("responsecode", "09");
                        response.put("responsemessage", "Duplicate transaction reference " + rrn + " for terminal " + terminalId);

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    String productId = object.optString("productid");

                    posTransaction.setFileId(productId);
                    posTransaction.setLoggedBy(uniqueId);
                    posTransaction.setPan(object.optString("pan"));
                    posTransaction.setLoggedBy(uniqueId);
//                    posTransaction.setPan(request.getPan());
//                    posTransaction.setPosId(request.getPosId());

                    String transRef = terminalId + "" + rrn + "" + posTransaction.getAmount() + "" + Utility.formatDate(requestDate, "yyyyMMdd");

                    posTransaction.setTransRef(transRef);

                    String parentName = null;

                    if (merchant != null) {
                        posTransaction.setPosId(merchant.getPosId());
                        posTransaction.setMerchantName(merchant.getName());

                        if (merchant.getParent() != null) {
                            parentName = merchant.getParent().getName();
                        }
                    }
                    
                    

                    String statusCode = object.optString("statuscode");

                    String status = "Success";

                    if (!"00".equalsIgnoreCase(statusCode)) {

                        status = "Failed";
                    }

                    posTransaction.setRefCode(transRef);
                    posTransaction.setRequestDate(requestDate);
                    posTransaction.setResponseDate(requestDate);
                    posTransaction.setResponseCode(statusCode);
                    posTransaction.setResponseMessage("00".equalsIgnoreCase(statusCode) ? "Transaction Successful" : "Transaction Failed");
                    posTransaction.setScheme("");
                    posTransaction.setStatus(status);
                    posTransaction.setRrn(rrn);
                    posTransaction.setSource(request.getProvider());
                    posTransaction.setTerminalId(terminalId);
                    posTransaction.setType(object.optString("transactiontype", "PURCHASE"));

                    posTransaction = posTransactionDao.create(posTransaction);

                    PosTransactionNew posTransactionNew = null;

                    if (merchant != null) {
                        posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                                object.optString("cardholder", null), null, parentName, merchant.getMerchantId(),
                                merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null, merchant, posTerminal.getProvider(), posMerchantCustomer);
                    } else {
                        posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                                object.optString("cardholder", null), null, parentName, null,
                                null, posTerminal.getProvider().getName(), null, null, merchant, posTerminal.getProvider(), posMerchantCustomer);
                    }

                    PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(rrn,
                            terminalId, posTransaction.getRequestDate());

                    String fee = object.optString("fee", null);
                    
                    if(Utility.emptyToNull(fee) != null ){
                        
                        
                        posTransactionNew.setMeta("fee:"+fee);
                    }

                    
                    
                    if (posTransactionN == null) {
                        
                        posTransactionNew.setMerchantBank(object.optString("bank", null));
                        posTransactionNewDao.create(posTransactionNew);
                    } else {

                        posTransactionNew.setMerchantBank(object.optString("bank", null));
                        posTransactionNew.setId(posTransactionN.getId());
                        posTransactionNewDao.update(posTransactionNew);
                    }

//                    if (merchant != null && merchant.getCategory() == PosMerchantCategory.NPP) {
//                        nppPosService.logTransaction(posTransactionNew);
//                    }

                    utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);

                    response.put("responsecode", "00");
                    response.put("responsemessage", "Transaction logged successfully");

                    return returnResponse(Response.Status.OK, response, title, uniqueId);
                }
                case "gamobile": {

                    String terminalId = object.optString("terminalid");

                    response.put("terminalid", terminalId);

                    posTerminal = posTerminalDao.findByKey("terminalId", terminalId);

                    String posId = object.optString("posid");

                    if (Utility.emptyToNull(posId) != null && Utility.emptyToNull(posId) != "0") {

                        merchant = posMerchantDao.findByKey("posId", posId);

                        if (merchant == null) {

                            PosOrder order = posOrderDao.findByKey("posOrderId", posId);

                            if (order != null) {

                                merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());

                                orderId = order.getCustomerOrderId();

                            } else {

                                posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", posId);

                                if (posMerchantCustomer == null) {

                                    PosMerchant posMerchant = posTerminal.getMerchant();

                                    if (posMerchant != null) {

//                                        if (posMerchant.getCategory() == PosMerchantCategory.NPP) {

                                            merchant = posMerchant;

                                            merchantReference = posId;
//                                        }

                                    } else {

                                        response.put("responsecode", "06");
                                        response.put("responsemessage", "Merchant with pos id does not exists");

                                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                                    }
                                }

                                merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());

                                customerId = posMerchantCustomer.getCustomerId();

                            }
                        }
                    }

                    if (posTerminal == null) {

                        response.put("responsecode", "06");
                        response.put("responsemessage", "Terminal does not exist");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                    
                    if(merchant == null)
                        merchant = posTerminal.getMerchant();

                    String amountString = object.optString("amount");

                    double amount = Double.parseDouble(amountString);

//                    amount = amount / 100;
                    
                    PosTransaction posTransaction = new PosTransaction();

                    posTransaction.setAmount(amount);

                    posTransaction.setCreatedOn(new Date());
                    posTransaction.setCurrency("NGN");
                    posTransaction.setCurrencyCode("566");

                    String requestDateString = object.optString("datetime");

//                    Date requestDate = Utility.parseDate(requestDateString, "yyyy-MM-dd HH:mm:ss");
                    
                    Date requestDate =  Utility.parseDate(requestDateString, "yyyy-MM-dd HH:mm:ss");
                    
//                    if(requestDate == null || Utility.isExpired(requestDate) == true){
//                        requestDate = Utility.parseDate(requestDateString, "yyyy-MM-dd HH:mm:ss");
//                    }
                              
//                    if(requestDate == null || Utility.isExpired(requestDate) == true)
//                        requestDate = Utility.parseDate(requestDateString, "dd-MM-yyyy HH:mm:ss");

                    String rrn = object.optString("rrn");

//                    try {
//
//                        Long rrnLong = Long.parseLong(rrn);
//
//                        rrn = String.valueOf(rrnLong);
//
//                    } catch (Exception ex) {
//
//                        response.put("responsecode", "07");
//                        response.put("responsemessage", "RRN must be numeric " + rrn);
//
//                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
//                    }

                    
                    PosTransaction transaction;
                    
                    if(requestDate == null)
                        transaction = posTransactionDao.findRRNTerminal(rrn, terminalId, requestDate);
                    else
                        transaction = posTransactionDao.findRRNTerminalOnly(rrn, terminalId);
                    
//                    Boolean reversal = object.getBoolean("reversal");
//                    
//                    if(Objects.equals(reversal, Boolean.TRUE) && transaction == null){
//
//                        String transRef = terminalId + "" + rrn + "" + amountString + "" + Utility.formatDate(requestDate, "yyyyMMdd");
//                        
//                        posReversalCache.add(transRef, request.getData());
//                        
//                        response.put("responsecode", "08");
//                        response.put("responsemessage", "Transaction does not exist");
//
//                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
//                    }

                    if (transaction != null) {

//                        if (Objects.equals(reversal, Boolean.TRUE) && !transaction.isReversed()) {
//
//                            transaction.setReversed(true);
//                            transaction.setReversedOn(requestDate);
//
//                            posTransactionDao.update(transaction);
//                            
//                            utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);
//
//                            response.put("responsecode", "00");
//                            response.put("responsemessage", "Reversal logged successfully");
//
//                            return returnResponse(Response.Status.OK, response, title, uniqueId);
//                        }

                        response.put("responsecode", "09");
                        response.put("responsemessage", "Duplicate transaction reference " + rrn + " for terminal " + terminalId);

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    String productId = object.optString("stan");

                    posTransaction.setFileId(productId);
                    posTransaction.setLoggedBy(uniqueId);
                    posTransaction.setPan(object.optString("maskedpan"));
                    posTransaction.setLoggedBy(uniqueId);
//                    posTransaction.setPan(request.getPan());
//                    posTransaction.setPosId(request.getPosId());

                    String transRef = terminalId + "" + rrn + "" + posTransaction.getAmount() + "" + Utility.formatDate(requestDate, "yyyyMMdd");

                    posTransaction.setTransRef(transRef);

                    String parentName = null;

                    if (merchant != null) {
                        posTransaction.setPosId(merchant.getPosId());
                        posTransaction.setMerchantName(merchant.getName());

                        if (merchant.getParent() != null) {
                            parentName = merchant.getParent().getName();
                        }
                    }
                    
                    

                    String statusCode = object.optString("statuscode");

                    String status = "Success";

                    if (!"00".equalsIgnoreCase(statusCode)) {

                        status = "Failed";
                    }

                    posTransaction.setRefCode(transRef);
                    posTransaction.setRequestDate(requestDate);
                    posTransaction.setResponseDate(requestDate);
                    posTransaction.setResponseCode(statusCode);
                    posTransaction.setResponseMessage("00".equalsIgnoreCase(statusCode) ? "Transaction Successful" : "Transaction Failed");
                    posTransaction.setScheme("");
                    posTransaction.setStatus(status);
                    posTransaction.setRrn(rrn);
                    posTransaction.setSource(request.getProvider());
                    posTransaction.setTerminalId(terminalId);
                    posTransaction.setType(object.optString("transactiontype", "PURCHASE"));

                    posTransaction = posTransactionDao.create(posTransaction);

                    PosTransactionNew posTransactionNew = null;

                    if (merchant != null) {
                        posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                                object.optString("cardholdername", null), null, parentName, merchant.getMerchantId(),
                                merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null, merchant, posTerminal.getProvider(), posMerchantCustomer);
                    } else {
                        posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                                object.optString("cardholdername", null), null, parentName, null,
                                null, posTerminal.getProvider().getName(), null, null, merchant, posTerminal.getProvider(), posMerchantCustomer);
                    }

                    PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(rrn,
                            terminalId, posTransaction.getRequestDate());

                    String fee = object.optString("fee", null);
                    
                    if(Utility.emptyToNull(fee) != null ){
                        
                        posTransactionNew.setMeta("fee:"+fee);
                    }

                    posTransactionNew.setFileId(object.optString("stan", null));
                    posTransactionNew.setScheme(object.optString("appLabel", null));
                    posTransactionNew.setTraceref(object.optString("authcode", null));
                    
                    
                    if (posTransactionN == null) {
                        
//                        posTransactionNew.setMerchantBank(object.optString("bank", null));
                        posTransactionNewDao.create(posTransactionNew);
                    } else {

//                        posTransactionNew.setMerchantBank(object.optString("bank", null));
                        posTransactionNew.setId(posTransactionN.getId());
                        posTransactionNewDao.update(posTransactionNew);
                    }

//                    if (merchant != null && merchant.getCategory() == PosMerchantCategory.NPP) {
//                        nppPosService.logTransaction(posTransactionNew);
//                    }

                    utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);

                    response.put("responsecode", "00");
                    response.put("responsemessage", "Transaction logged successfully");

                    return returnResponse(Response.Status.OK, response, title, uniqueId);
                }
                case "xpresspayment" : {
                    
                    String terminalId = object.optString("terminalid");

                    response.put("terminalid", terminalId);

                    posTerminal = posTerminalDao.findByKey("terminalId", terminalId);

                    String posId = object.optString("posid");
                    
                    String merchantTrannsactionId = object.optString("revenuecode");

                    if (Utility.emptyToNull(posId) != null) {

                        merchant = posMerchantDao.findByKey("posId", posId);

                        if (merchant == null) {

                            PosOrder order = posOrderDao.findByKey("posOrderId", posId);

                            if (order != null) {

                                merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());

                                orderId = order.getCustomerOrderId();

                            } else {

                                posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", posId);

                                if (posMerchantCustomer == null) {

                                    PosMerchant posMerchant = posTerminal.getMerchant();

                                    if (posMerchant != null) {

                                        if (posMerchant.getCategory() == PosMerchantCategory.NPP) {

                                            merchant = posMerchant;

                                            merchantReference = posId;
                                        }

                                    } else {

                                        response.put("responsecode", "06");
                                        response.put("responsemessage", "Merchant with pos id does not exists");

                                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                                    }
                                }

                                merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());

                                customerId = posMerchantCustomer.getCustomerId();

                            }
                        }
                    }

                    if (posTerminal == null) {

                        response.put("responsecode", "06");
                        response.put("responsemessage", "Terminal does not exist");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                    
                    if(merchant == null)
                        merchant = posTerminal.getMerchant();

//                    String amountString = object.optString("amount");

                    double amount = object.optDouble("amount");

                    PosTransaction posTransaction = new PosTransaction();

                    posTransaction.setAmount(amount);

                    posTransaction.setTraceref(merchantTrannsactionId);
                    posTransaction.setCreatedOn(new Date());
                    posTransaction.setCurrency("NGN");

                    String requestDateString = object.optString("transactiondate");

                    Date requestDate = Utility.parseDate(requestDateString, "MMM dd yyyy hh:mma");
                    
                    if(requestDate == null){
                        
                        response.put("responsecode", "10");
                        response.put("responsemessage", "Transaction Date not provided / invalid");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                        

                    String rrn = object.optString("transactionid");

                    try {

                        Long rrnLong = Long.parseLong(rrn);

                        rrn = String.valueOf(rrnLong);

                    } catch (Exception ex) {

                        response.put("responsecode", "07");
                        response.put("responsemessage", "RRN must be numeric " + rrn);

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    PosTransaction transactions = posTransactionDao.findRRNTerminal(rrn, terminalId, requestDate);
                    
                    String reversalId = object.optString("reverseid", "");
                    
                    if("1".equalsIgnoreCase(reversalId)){

                        response.put("responsecode", "08");
                        response.put("responsemessage", "Transaction to be reversed does not exist");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    if (transactions != null) {

                        if ("1".equalsIgnoreCase(reversalId) && !transactions.isReversed()) {

                            transactions.setReversed(true);
                            transactions.setReversedOn(requestDate);

                            posTransactionDao.update(transactions);
                            
                            utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);

                            response.put("responsecode", "00");
                            response.put("responsemessage", "Reversal logged successfully");

                            return returnResponse(Response.Status.OK, response, title, uniqueId);
                        }

                        response.put("responsecode", "09");
                        response.put("responsemessage", "Duplicate transaction reference " + rrn + " for terminal " + terminalId);

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

//                    String productId = object.optString("productid");
//
//                    posTransaction.setFileId(productId);
                    posTransaction.setLoggedBy(uniqueId);
                    posTransaction.setPan(object.optString("maskedpan"));

                    String transRef = terminalId + "" + rrn + "" + posTransaction.getAmount() + "" + Utility.formatDate(requestDate, "yyyyMMdd");

                    posTransaction.setTransRef(transRef);

                    String parentName = null;

                    if (merchant != null) {
                        posTransaction.setPosId(merchant.getPosId());
                        posTransaction.setMerchantName(merchant.getName());

                        if (merchant.getParent() != null) {
                            parentName = merchant.getParent().getName();
                        }
                    }

                    String statusCode = object.optString("status");
                    

                    String status = "Success";

                    if (!"00".equalsIgnoreCase(statusCode)) {

                        status = "Failed";
                    }
                    
                    String statusMessage = object.optString("message", status);

                    posTransaction.setRefCode(transRef);
                    posTransaction.setRequestDate(requestDate);
                    posTransaction.setResponseDate(requestDate);
                    posTransaction.setResponseCode(statusCode);
                    posTransaction.setResponseMessage(statusMessage);
                    posTransaction.setScheme("");
                    posTransaction.setResponseMessage("00".equalsIgnoreCase(statusCode) ? "Transaction Successful" : "Transaction Failed");
                    posTransaction.setSource(request.getProvider());
                    posTransaction.setStatus(status);
                    posTransaction.setRrn(rrn);
                    posTransaction.setTerminalId(terminalId);
                    posTransaction. setFileId(object.optString("uniquereferenceno"));
                    posTransaction.setType(object.optString("transactiontype", "Purchase"));
//                    posTransaction.setResponseMessage(statusMessage);

                    posTransaction = posTransactionDao.create(posTransaction);

                    PosTransactionNew posTransactionNew = null;

                    if (merchant != null) {
                        posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                                object.optString("name", null), null, parentName, merchant.getMerchantId(),
                                merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null, merchant, posTerminal.getProvider(), posMerchantCustomer);
                    } else {
                        posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                                object.optString("name", null), null, parentName, null,
                                null, posTerminal.getProvider().getName(), null, null, merchant, posTerminal.getProvider(), posMerchantCustomer);
                    }

                    PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(rrn,
                            terminalId, posTransaction.getRequestDate());

//                    posTransactionNew.setMerchantReference(merchantReference);

                    if (posTransactionN == null) {
                        posTransactionNewDao.create(posTransactionNew);
                    } else {

                        posTransactionNew.setId(posTransactionN.getId());
                        posTransactionNewDao.update(posTransactionNew);
                    }

                    notifyMerchant(merchant, posTransactionNew);

                    utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);

                    response.put("responsecode", "00");
                    response.put("responsemessage", "Transaction logged successfully");

                    return returnResponse(Response.Status.OK, response, title, uniqueId);
                }
                case "internal" :
                case "internalmobile" :{
                    
                    String terminalId = object.optString("terminalid");

                    response.put("terminalid", terminalId);

                    posTerminal = posTerminalDao.findByKey("terminalId", terminalId);

                    if (posTerminal == null) {

                        response.put("responsecode", "06");
                        response.put("responsemessage", "Terminal does not exist");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                    
                    String posId = object.optString("posid");
                    
                    merchantReference = posId;

                    if (Utility.emptyToNull(posId) != null) {

                        merchant = posMerchantDao.findByKey("posId", posId);

                        if (merchant == null) {

                            PosOrder order = posOrderDao.findByKey("posOrderId", posId);

                            if (order != null) {

                                merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());

                                orderId = order.getCustomerOrderId();

                            } else {

                                posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", posId);

                                if (posMerchantCustomer == null) {

                                    PosMerchant posMerchant = posTerminal.getMerchant();

                                    if (posMerchant != null) {

                                        if(null == posMerchant.getCategory() ){
                                            
                                            
                                            merchant = posMerchant;

                                            merchantReference = posId;
                                        } else switch (posMerchant.getCategory()) {
                                            
                                            case WAKANOW:
                                                merchant = posMerchant;
                                                String[] references = posId.split("-");
                                                if(references.length > 1)
                                                    merchantReference = references[1];
                                                else
                                                    merchantReference = posId;
                                                break;
                                            case HOSPITAL:
                                                posMerchantProduct
                                                        = posMerchantProductDao.findByMerchantAndProduct(posMerchant.getMerchantCode(), posId);
                                                
                                                if(posMerchantProduct != null) {
                                                    
                                                    merchantReference = posMerchantProduct.getProductCode() + " : "+posMerchantProduct.getMerchantName();
                                                }
                                                
                                                        
                                                break;
                                            default:
                                                merchant = posMerchant;
                                                merchantReference = posId;
                                                break;
                                        }

                                    } else {

                                        response.put("responsecode", "06");
                                        response.put("responsemessage", "Merchant with pos id does not exists");

                                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                                    }
                                }
                                else{
                                    merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());

                                    customerId = posMerchantCustomer.getCustomerId();
                                }

                            }
                        }
                    }


                    if(merchant == null){
                        merchant = posTerminal.getMerchant();
                    }
//                    String amountString = object.optString("amount");

                    double amount = object.optDouble("amount");

                    PosTransaction posTransaction = new PosTransaction();

                    posTransaction.setAmount(amount);

//                    posTransaction.setTraceref(object.optString("revenuecode"));
                    posTransaction.setCreatedOn(new Date());
                    posTransaction.setCurrency("NGN");

                    String requestDateString = object.optString("requestdate");

                    Date requestDate = Utility.parseDate(requestDateString, "yyyy-MM-dd HH:mm:ss");
                    
                    if(requestDate == null){
                        
                        response.put("responsecode", "10");
                        response.put("responsemessage", "Transaction Date not provided / invalid");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                        

                    String rrn = object.optString("rrn");

                    try {

                        Long rrnLong = Long.parseLong(rrn);

                        rrn = String.valueOf(rrnLong);

                    } catch (Exception ex) {

                        response.put("responsecode", "07");
                        response.put("responsemessage", "RRN must be numeric " + rrn);

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    PosTransaction transactions = posTransactionDao.findRRNTerminal(rrn, terminalId, requestDate);
                    
                    Boolean reversal = object.optBoolean("reversal");
                    
                    if(Objects.equals(reversal, Boolean.TRUE) && transactions == null){

                        response.put("responsecode", "08");
                        response.put("responsemessage", "Transaction does not exist");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    if (transactions != null) {

                        if (Objects.equals(reversal, Boolean.TRUE) && !transactions.isReversed()) {

                            transactions.setReversed(true);
                            transactions.setReversedOn(requestDate);

                            posTransactionDao.update(transactions);
                            
                            utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);

                            response.put("responsecode", "00");
                            response.put("responsemessage", "Reversal logged successfully");

                            return returnResponse(Response.Status.OK, response, title, uniqueId);
                        }

                        response.put("responsecode", "09");
                        response.put("responsemessage", "Duplicate transaction reference " + rrn + " for terminal " + terminalId);

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

//                    String productId = object.optString("productid");
//
//                    posTransaction.setFileId(productId);
                    posTransaction.setLoggedBy(uniqueId);
                    posTransaction.setPan(object.optString("pan"));

                    String transRef = terminalId + "" + rrn + "" + posTransaction.getAmount() + "" + Utility.formatDate(requestDate, "yyyyMMdd");

                    posTransaction.setTransRef(transRef);

                    String parentName = null;

                    if (merchant != null) {
                        posTransaction.setPosId(merchant.getPosId());
                        posTransaction.setMerchantName(merchant.getName());

                        if (merchant.getParent() != null) {
                            parentName = merchant.getParent().getName();
                        }
                    }

                    String statusCode = object.optString("responsecode");
                    
                    String statusMessage = object.optString("responsemessage");

                    String status = "Success";

                    if (!"00".equalsIgnoreCase(statusCode)) {

                        status = "Failed";
                    }

                    posTransaction.setRefCode(transRef);
                    posTransaction.setRequestDate(requestDate);
                    posTransaction.setResponseDate(requestDate);
                    posTransaction.setResponseCode(statusCode);
                    posTransaction.setResponseMessage(statusMessage);
                    posTransaction.setScheme(object.optString("cardtype"));
                    posTransaction.setSource(request.getProvider());
                    posTransaction.setStatus(status);
                    posTransaction.setRrn(rrn);
                    posTransaction.setTerminalId(terminalId);
                    posTransaction.setFileId(object.optString("refcode", transRef));
                    posTransaction.setType(object.optString("type"));

                    posTransaction = posTransactionDao.create(posTransaction);

                    PosTransactionNew posTransactionNew = null;

                    if (merchant != null) {
                        posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, merchant.getBankName(),
                                object.optString("customername", null), null, parentName, merchant.getMerchantId(),
                                merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null, merchant, posTerminal.getProvider(), posMerchantCustomer);
                    } else {
                        posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, null,
                                object.optString("customername", null), null, parentName, null,
                                null, posTerminal.getProvider().getName(), null, null, merchant, posTerminal.getProvider(), posMerchantCustomer);
                    }

                    PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(rrn,
                            terminalId, posTransaction.getRequestDate());

                    posTransactionNew.setOrderId(merchantReference);
                    
                    posTransactionNew.setTraceref(object.optString("refcode"));
                    posTransactionNew.setProviderRef(object.optString("host"));
                    
                    String stan = object.optString("stan");
                    
                    
                    if(Utility.emptyToNull(stan) != null)
                        posTransactionNew.setMeta("stan : "+stan);

                   
                    if (merchant != null) {
                        
                        if( null != merchant.getCategory() )
                            
                            switch (merchant.getCategory()) {
//                            case NPP:
//                                String result = nppPosService.logTransaction(posTransactionNew);
//                                
//                                if(Utility.emptyToNull(result) != null)
//                                    response.put("validationreference", result);
//                                
//                                break;
                            case SENDBOX:
                                sendBoxPosService.logTransaction(posTransactionNew);
                                break;
                            case WAKANOW:
                                if(posId != null){
                                    
                                    String[] ids = posId.split("-");
                                    
                                    if(ids.length > 1)
                                        posTransactionNew.setTraceref(ids[1]);
                                    
                                }   wakanowService.logPayment(posTransactionNew);
                                break;
                            case HOSPITAL:
                                
                                JSONObject patient = object.optJSONObject("patient");
                                
                                if(patient != null)
                                    posTransactionNew.setMeta(patient.toString());
                                
                                break;
                                
                            default:
                                break;
                        }
                    }
                    
                    if (posTransactionN == null) {
                        posTransactionNewDao.create(posTransactionNew);
                    } else {

                        posTransactionNew.setId(posTransactionN.getId());
                        posTransactionNewDao.update(posTransactionNew);
                    }
                    
                     if(merchant != null && merchant.getCategory() == PosMerchantCategory.HOSPITAL)
                        utilityService.callBackUserHospital(posTransactionNew, merchantReference,  posMerchantProduct);
                    else
                        utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);

//                    utilityService.callBackUser(posTransaction, orderId, posTransactionNew.getMeta());

                    response.put("responsecode", "00");
                    response.put("responsemessage", "Transaction logged successfully");

                    return returnResponse(Response.Status.OK, response, title, uniqueId);
                }
                default: {
                    
                    response.put("responsecode", "08");
                    response.put("responsemessage", "Unknown provider");

                    return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                }
                    
            }

            
        } catch (Exception ex) {
            ex.printStackTrace();
//            Logger.getLogger(PosApiController.class.getName()).error(null, ex);
            
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    
    @Path(value = "pos/transaction/validate")
    @POST
    public Response getTransactionInfo(@Valid QueryPosTransactionRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        try {
            String title = "Merchant Transaction Enquiry";

            Map<String, Object> response = new HashMap<>();
            Log log = new Log();
            log.setAction(title);
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername(uniqueId);
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            String requestString = apiUser.getPrivateKey() + "" + request.getTransactionId();
            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", request.getTerminalId());

            if (posTerminal == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Unknown terminal information");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            PosMerchant merchant = posTerminal.getMerchant();

            if (merchant == null || merchant.getCategory() == PosMerchantCategory.GENERAL) {

                PosMerchant posMerchant = posMerchantDao.findByKey("posId", request.getTransactionId());
                
                if(posMerchant == null){
                
                    response.put("responsecode", "06");
                    response.put("responsemessage", "Unknown transaction record");

                    return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                
                }else {
                    
                    response.put("responsecode", "00");
                    response.put("responsemessage", "Successful");
                    response.put("merchantname", ""+posMerchant.getName());
                    response.put("amount", 0.0);
                    response.put("allow_amount", true);
                    
                    return returnResponse(Response.Status.OK, response, title, uniqueId);
                }
            }

//            if (merchant.getCategory() != PosMerchantCategory.NPP && merchant.getCategory() != PosMerchantCategory.WAKANOW ) {
//
//                
//            }
            
            switch(merchant.getCategory()){
                
                case SENDBOX:{
                    
                    SendBoxQueryModel nppQueryModel = null;

                    try {
                        nppQueryModel = sendBoxPosService.validateTransaction(request.getTransactionId());
                    } catch (Exception ex) {
                        java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (nppQueryModel == null || !"0".equalsIgnoreCase(nppQueryModel.getStatusCode())) {

                        response.put("responsecode", "07");
                        response.put("responsemessage", "Unable to validate record");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                    
                    response.put("responsecode", "00");
                    response.put("responsemessage", "Successful");
                    response.put("merchantname", nppQueryModel.getCustomerName());
                    response.put("amount", nppQueryModel.getTotalAmount());
                    response.put("allow_amount", false);
                    
                    break;
                }
                case WAKANOW : {
                
                    
                    String[] arrays = request.getTransactionId().split("-");
                    
                    if(arrays.length <= 1){
                        
                        response.put("responsecode", "08");
                        response.put("responsemessage", "Invalid Transaction Id");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                    
                    String options = arrays[0];
                    
                    String reference = arrays[1];
                    
                            
                    WakanowCustomerInformationResponse informationResponse = wakanowService.validateReference(reference);

                    if(informationResponse == null ){

                        response.put("responsecode", "08");
                        response.put("responsemessage", "Invalid Transaction Id");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    WakanowCustomer wakanowCustomer =  informationResponse.getWakanowCustomers().get(0);

                    if(wakanowCustomer == null || !Objects.equals(wakanowCustomer.getStatus(), Integer.parseInt("0"))){

                        response.put("responsecode", "08");
                        response.put("responsemessage", "Invalid Transaction Id");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }

                    response.put("responsecode", "00");
                    response.put("responsemessage", "Successful");
                    response.put("merchantname", wakanowCustomer.getCustomerReferenceDescription());
                    response.put("amount", wakanowCustomer.getAmount());
                    response.put("allow_amount", false);
                            
                    break;
                } 
                case HOSPITAL: {
                
                    
                    PosMerchantProduct merchantProduct = posMerchantProductDao.findByMerchantAndProduct(merchant.getMerchantCode(), request.getTransactionId());
                    
                    if(merchantProduct == null) {
                        response.put("responsecode", "08");
                        response.put("responsemessage", "Invalid Product Id");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                    
                    response.put("responsecode", "00");
                    response.put("responsemessage", "Successful");
                    response.put("merchantname", merchantProduct.getProductName());
                    response.put("allow_amount", true);
                    response.put("amount", 0);
                    
                    break;
                }
                
                default:{
                    
                    response.put("responsecode", "00");
                    response.put("responsemessage", "Successful");
                    response.put("merchantname", ""+merchant.getName());
                    response.put("amount", 0.0);
                    response.put("allow_amount", true);
                    
                    break;
                }

            }

            return returnResponse(Response.Status.OK, response, title, uniqueId);
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    
    private Response returnResponse(Response.Status status, Object obj, String action, String uniqueId) {

        Log log = new Log();
        log.setAction(action);
        log.setCreatedOn(new Date());
        log.setDescription(obj.toString());
        log.setLevel(status == Response.Status.INTERNAL_SERVER_ERROR ? LogLevel.Severe : LogLevel.Info);
        log.setLogState(LogState.FINISH);
        log.setUsername("API KEY - " + uniqueId);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setLogDomain(LogDomain.API);

        logService.log(log);

        return Response.status(status).entity(obj).build();
    }

    @Path(value = "pos/merchant/info")
    @POST
    public Response getMerchantInfo(@Valid GetPosMerchantRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        try {
            String title = "Merchant Information Inquiry";

            Map<String, Object> response = new HashMap<>();
            Log log = new Log();
            log.setAction(title);
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername(uniqueId);
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            String requestString = apiUser.getPrivateKey() + "" + request.getPosId();
            String hashString = Utility.sha512(requestString);
            
            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            if("0".equalsIgnoreCase(request.getPosId())){
                
                
//                if(pos)
                
                response.put("posid", request.getPosId());
            
                response.put("merchantname", "Welcome");
                response.put("responsecode", "00");
                response.put("responsemessage", "Successful");

                return returnResponse(Response.Status.OK, response, title, uniqueId);
            }
            
            PosMerchant merchant = posMerchantDao.findByKey("posId", request.getPosId());

            if (merchant == null) {
                
                PosOrder order = posOrderDao.findByKey("posOrderId", request.getPosId());

                if(order == null){
                    
                    PosMerchantCustomer posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", request.getPosId());
                    
                    if(posMerchantCustomer == null){
                    
                        response.put("responsecode", "06");
                        response.put("responsemessage", "Merchant / user with pos id does not exists");

                        return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                    }
                    
                    response.put("posid", request.getPosId());
            
                    response.put("merchantname", posMerchantCustomer.getCustomerName());
                    response.put("responsecode", "00");
                    response.put("responsemessage", "Successful");

                    return returnResponse(Response.Status.OK, response, title, uniqueId);
                    
                }else
                    merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());
                
                if(merchant == null){
                    response.put("responsecode", "06");
                    response.put("responsemessage", "Merchant with pos id does not exists.");

                    return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                }
                
//                response.put("posid", request.getPosId());
                
            }
            
            if(merchant.isEnabled() == false){
                
                response.put("responsecode", "05");
                response.put("responsemessage", "Merchant verification failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
//                
            response.put("posid", request.getPosId());
            
            response.put("merchantname", merchant.getName());
            response.put("responsecode", "00");
            response.put("responsemessage", "Successful");

            return returnResponse(Response.Status.OK, response, title, uniqueId);
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    
    @Path(value = "pos/terminal/info")
    @POST
    public Response getTerminalInfo(@Valid GetTerminalInfo request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        try {
            String title = "Terminal Information Inquiry";

            Map<String, Object> response = new HashMap<>();
            Log log = new Log();
            log.setAction(title);
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername(uniqueId);
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            String requestString = apiUser.getPrivateKey() + "" + request.getTerminalId();
            String hashString = Utility.sha512(requestString);
            
            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            PosTerminal posTerminal = terminalService.getTerminal(request.getTerminalId());

            if (posTerminal == null) {
                

                response.put("responsecode", "06");
                response.put("responsemessage", "Terminal info does not exists.");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                
            }
//                
            if(posTerminal.getMerchant() != null){
                
                JSONObject merchantInfo = new JSONObject()
                        .put("posid", posTerminal.getMerchant().getPosId())
                        .put("merchantname", posTerminal.getMerchant().getName());
                
                if("144163".equalsIgnoreCase(posTerminal.getMerchant().getMerchantId())){
                
                    RaveKeyItem raveKeyItem = raveApiService.getKey(posTerminal.getMerchant().getMerchantCode());
                    
                    if(raveKeyItem != null) {
                        
                        merchantInfo.put("publickey", raveKeyItem.getPublicKey());
                        merchantInfo.put("encryptionkey", raveKeyItem.getEncryptionKey());
                    }
                        
                }
                
                response.put("merchant_info", merchantInfo.toMap());
                
            }
            else
                response.put("merchant_info", null);
            
            
            response.put("responsecode", "00");
            response.put("responsemessage", "Operation processed successfully");

            return returnResponse(Response.Status.OK, response, title, uniqueId);
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path(value = "pos/terminal/map")
    @POST
    public Response attachTerminalToMerchant(@Valid PosTerminalMappingRequest request, @HeaderParam(value = "uniqueid") String uniqueId ){
    
        try {
            String title = "Terminal Mapping";

            Map<String, Object> response = new HashMap<>();
            Log log = new Log();
            log.setAction(title);
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername(uniqueId);
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            String requestString = apiUser.getPrivateKey() + "" + request.getTerminalId() + "" + request.getMerchantCode();
            String hashString = Utility.sha512(requestString);
            
            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            PosTerminal posTerminal = terminalService.getTerminal(request.getTerminalId());

            if (posTerminal == null) {
                

                response.put("responsecode", "06");
                response.put("responsemessage", "Terminal info does not exists.");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            PosMerchant posMerchant =  posMerchantDao.findByKey("merchantCode", request.getMerchantCode());
            
            if(posMerchant == null){
                
                response.put("responsecode", "06");
                response.put("responsemessage", "Merchant info does not exists.");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                
            }
            
            posTerminal.setMerchant(posMerchant);
            
            posTerminalDao.update(posTerminal);
            
            response.put("responsecode", "00");
            response.put("responsemessage", "Operation processed successfully");

            return returnResponse(Response.Status.OK, response, title, uniqueId);
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    } 
    
    @Path(value = "pos/transaction/log")
    @POST
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response logTransaction(@Valid LogPosTransactionRequest request, @HeaderParam(value = "uniqueid") String uniqueId) {

        try {

            Map<String, Object> response = new HashMap<>();

            response.put("terminalid", request.getTerminalId());
            response.put("posid", request.getPosId());

            Log log = new Log();
            log.setAction("Logging Transaction");
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername(uniqueId);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log);

            String title = "Logging MPOS Transaction";

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashableString();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            Date requestDate = null, responseDate = null;

            requestDate = Utility.parseDate(request.getRequestDate(), "yyyy-MM-dd HH:mm:ss");

            if (requestDate == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid request date (yyyy-MM-dd HH:mm:ss)");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            responseDate = Utility.parseDate(request.getResponseDate(), "yyyy-MM-dd HH:mm:ss");

            if (responseDate == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid response date (yyyy-MM-dd HH:mm:ss)");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            PosMerchant merchant = null;

            String orderId = null, customerId = null;
            
            PosMerchantCustomer posMerchantCustomer = null;
            
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", request.getTerminalId());

            if (posTerminal == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Terminal does not exist");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            String posId = request.getPosId();
            
            if (Utility.emptyToNull(posId) != null && !"0".equalsIgnoreCase(posId)) {
                merchant = posMerchantDao.findByKey("posId", request.getPosId());

                if (merchant == null) {

                    PosOrder order = posOrderDao.findByKey("posOrderId", request.getPosId());
                
                    if(order != null){

                        merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());
                        
                        orderId = order.getCustomerOrderId();

                    }else {
                        
                        posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", request.getPosId());
                        
                        if(posMerchantCustomer == null){
                            response.put("responsecode", "06");
                            response.put("responsemessage", "Merchant with pos id does not exists");

                            return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                        }
                        
                        merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());
                        
                        customerId = posMerchantCustomer.getCustomerId();
                        
                    }
                }
            }else{
                
                merchant = posTerminal.getMerchant();
                
                if(merchant != null)
                    posId = merchant.getPosId();
            }

            PosTransaction posTransaction = new PosTransaction();
            
            if( posTerminal.getProvider().isAmountInKobo() == true){
                posTransaction.setAmount(request.getAmount() / 100);
            }else
                posTransaction.setAmount(request.getAmount());
            
            posTransaction.setCreatedOn(new Date());
//            posTransaction.setCurrency(request.getCurrencyCode());

            try {
                Currency currency = Currency.getInstance(request.getCurrencyCode());
                posTransaction.setCurrency(currency.getNumericCode() + "");
            } catch (Exception ex) {
                ex.printStackTrace();

                posTransaction.setCurrency("NGN");
            }

            String rrn = request.getRrn();

            try{
                Long rrnLong = Long.parseLong(rrn);

                rrn = String.valueOf(rrnLong);
            }catch(Exception ex){
                
                response.put("responsecode", "07");
                response.put("responsemessage", "RRN must be numeric " + request.getRrn());

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
//            posTransactionDao.f
            PosTransaction transactions = posTransactionDao.findRRNTerminal(rrn, request.getTerminalId(), requestDate);

            if (transactions != null) {

                response.put("responsecode", "07");
                response.put("responsemessage", "Duplicate transaction reference " + request.getRrn() + " for terminal " + request.getTerminalId());

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            

            posTransaction.setFileId(request.getFileId());
            posTransaction.setLoggedBy(uniqueId);
            posTransaction.setPan(request.getPan());
            posTransaction.setPosId(posId);
            
            String transRef = request.getTerminalId()+""+rrn+""+posTransaction.getAmount()+""+Utility.formatDate(requestDate, "yyyyMMdd");
            
            posTransaction.setTransRef(transRef);
            
            
            String parentName = null;
            
            
            if(merchant != null){
                posTransaction.setPosId(merchant.getPosId());
                posTransaction.setMerchantName(merchant.getName());
                
                if(merchant.getParent() != null){
                    parentName = merchant.getParent().getName();
                }
            }
//            PosMerchant merchant = posMerchantDao.findByKey("posId", request.getMerchantId());
            
            
            posTransaction.setRefCode(transRef);
            posTransaction.setRequestDate(requestDate);
            posTransaction.setResponseDate(responseDate);
            posTransaction.setResponseCode(request.getResponseCode());
            posTransaction.setResponseMessage(request.getResponseMessage());
//            posTransaction.setRrn(request.getRrn());
            posTransaction.setScheme(request.getScheme());
            posTransaction.setSource(request.getSource());
            posTransaction.setStatus(request.getStatus());
            posTransaction.setRrn(rrn);
            posTransaction.setScheme(request.getScheme());
            posTransaction.setSource(request.getSource());
            posTransaction.setTerminalId(request.getTerminalId());
            posTransaction.setType(request.getType());

            posTransaction = posTransactionDao.create(posTransaction);
            
            PosTransactionNew posTransactionNew = null;
            
            if(merchant != null){
                posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                        request.getChName(), null, parentName, merchant.getMerchantId(),
                        merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null,merchant,posTerminal.getProvider(), posMerchantCustomer);
            }else{
                posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                        request.getChName(), null, parentName, null,
                        null, posTerminal.getProvider().getName(), null, null, merchant,posTerminal.getProvider(), posMerchantCustomer );
            }
            
            PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(rrn, 
                    request.getTerminalId(), posTransaction.getRequestDate());
            
            if(Utility.emptyToNull(request.getFee()) != null ){
                        
                        
                posTransactionNew.setMeta("fee:"+request.getFee());
            }
            
            if(posTransactionN == null){
                
                posTransactionNewDao.create(posTransactionNew);
            }else{
                
                posTransactionNew.setId(posTransactionN.getId());
                posTransactionNewDao.update(posTransactionNew);
            }
            
            

            utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);

            response.put("responsecode", "00");
            response.put("responsemessage", "Transaction logged successfully");

            return returnResponse(Response.Status.OK, response, title, uniqueId);
        } catch (Exception ex) {
            Logger.getLogger(PosApiController.class.getName()).error(null, ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    
    
    @Path(value = "pos/transaction/log/etop")
    @POST
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response logETOPTransaction(@Valid LogPosTransactionRequest request, @HeaderParam(value = "uniqueid") String uniqueId) {

        try {

            Map<String, Object> response = new HashMap<>();

            response.put("terminalid", request.getTerminalId());
            response.put("posid", request.getPosId());

            Log log = new Log();
            log.setAction("Logging ETOP POS Transaction");
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername(uniqueId);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log);

            String title = "Logging MPOS Transaction";

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashableString();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            Date requestDate = null, responseDate = null;

            requestDate = Utility.parseDate(request.getRequestDate(), "yyyy-MM-dd HH:mm:ss");

            if (requestDate == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid request date (yyyy-MM-dd HH:mm:ss)");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            responseDate = Utility.parseDate(request.getResponseDate(), "yyyy-MM-dd HH:mm:ss");

            if (responseDate == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid response date (yyyy-MM-dd HH:mm:ss)");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            PosMerchant merchant = null;

            String orderId = null, customerId = null;
            
            PosMerchantCustomer posMerchantCustomer = null;
            
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", request.getTerminalId());

            if (posTerminal == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Terminal does not exist");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            if (request.getPosId() != null && !"0".equalsIgnoreCase(request.getPosId())) {
                merchant = posMerchantDao.findByKey("posId", request.getPosId());

                if (merchant == null) {

                    PosOrder order = posOrderDao.findByKey("posOrderId", request.getPosId());
                
                    if(order != null){

                        merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());
                        
                        orderId = order.getCustomerOrderId();

                    }else {
                        
                        posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", request.getPosId());
                        
                        if(posMerchantCustomer == null){
                            response.put("responsecode", "06");
                            response.put("responsemessage", "Merchant with pos id does not exists");

                            return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                        }
                        
                        merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());
                        
                        customerId = posMerchantCustomer.getCustomerId();
                        
                    }
                }
            }else{
                
                merchant = posTerminal.getMerchant();
            }


            PosTransaction posTransaction = new PosTransaction();
            
//            if( posTerminal.getProvider().isAmountInKobo() == true){
                posTransaction.setAmount(request.getAmount() / 100);
//            }else
//                posTransaction.setAmount(request.getAmount());
//            
            posTransaction.setCreatedOn(new Date());
//            posTransaction.setCurrency(request.getCurrencyCode());

            try {
                Currency currency = Currency.getInstance(request.getCurrencyCode());
                posTransaction.setCurrency(currency.getNumericCode() + "");
            } catch (Exception ex) {
                ex.printStackTrace();

                posTransaction.setCurrency("NGN");
            }

            String rrn = request.getRrn();

            try{
                Long rrnLong = Long.parseLong(rrn);

                rrn = String.valueOf(rrnLong);
            }catch(Exception ex){
                
                response.put("responsecode", "07");
                response.put("responsemessage", "RRN must be numeric " + request.getRrn());

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
//            posTransactionDao.f
            PosTransaction transactions = posTransactionDao.findRRNTerminal(rrn, request.getTerminalId(), requestDate);

            if (transactions != null) {

                response.put("responsecode", "07");
                response.put("responsemessage", "Duplicate transaction reference " + request.getRrn() + " for terminal " + request.getTerminalId());

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            posTransaction.setFileId(request.getFileId());
            posTransaction.setLoggedBy(uniqueId);
            posTransaction.setPan(request.getPan());
            posTransaction.setPosId(request.getPosId());
            
            String transRef = request.getTerminalId()+""+rrn+""+posTransaction.getAmount()+""+Utility.formatDate(requestDate, "yyyyMMdd");
            
            posTransaction.setTransRef(transRef);
            
            
            String parentName = null;
            
            
            if(merchant != null){
                posTransaction.setPosId(merchant.getPosId());
                posTransaction.setMerchantName(merchant.getName());
                
                if(merchant.getParent() != null){
                    parentName = merchant.getParent().getName();
                }
            }   
            
            posTransaction.setRefCode(transRef);
            posTransaction.setRequestDate(requestDate);
            posTransaction.setResponseDate(responseDate);
            posTransaction.setResponseCode(request.getResponseCode());
            posTransaction.setResponseMessage(request.getResponseMessage());
//            posTransaction.setRrn(request.getRrn());
            posTransaction.setScheme(request.getScheme());
            posTransaction.setSource(request.getSource());
            posTransaction.setStatus(request.getStatus());
            posTransaction.setRrn(rrn);
            posTransaction.setScheme(request.getScheme());
            posTransaction.setSource(request.getSource());
            posTransaction.setTerminalId(request.getTerminalId());
            posTransaction.setType("1".equalsIgnoreCase(request.getType() ) ? "PURCHASE" : request.getType());

            posTransaction.setTraceref(request.getPaymentReference());
            
            posTransaction = posTransactionDao.create(posTransaction);
            
            PosTransactionNew posTransactionNew = null;
            
            if(merchant != null){
                posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                        request.getChName(), null, parentName, merchant.getMerchantId(),
                        merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null,merchant,posTerminal.getProvider(), posMerchantCustomer);
            }else{
                posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, posTerminal.getBankName(),
                        request.getChName(), null, parentName, null,
                        null, posTerminal.getProvider().getName(), null, null, merchant,posTerminal.getProvider(), posMerchantCustomer );
            }
            
            PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(rrn, 
                    request.getTerminalId(), posTransaction.getRequestDate());
            
            if(Utility.emptyToNull(request.getFee()) != null ){
                        
                        
                posTransactionNew.setMeta("fee:"+request.getFee());
            }
            
            posTransactionNew.setTraceref(request.getPaymentReference());
            
            
            if(posTransactionN == null){
                posTransactionNewDao.create(posTransactionNew);
            }else{
                
                posTransactionNew.setId(posTransactionN.getId());
                posTransactionNewDao.update(posTransactionNew);
            }
            
            
            utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);
            
//            if(!posTerminal.isFlagged()){
//                
////                utilityService.callSubscribers(posTransactionNew, posTransaction,posTerminal, orderId, posMerchantCustomer);
//            }

            response.put("responsecode", "00");
            response.put("responsemessage", "Transaction logged successfully");

            return returnResponse(Response.Status.OK, response, title, uniqueId);
        } catch (Exception ex) {
            Logger.getLogger(PosApiController.class.getName()).error(null, ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path(value = "pos/transaction/reschedule")
    @POST
    public Response rescheduleTransaction(@Valid PosTransactionRescheduleRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        Date date = null, endDate = null;

        Map<String, String> response = new HashMap<>();

        if (request.getDate() != null && !request.getDate().isEmpty()) {

            date = Utility.parseDate(request.getDate(), "yyyy-MM-dd");

            if (date == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid start date");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Reschedule", uniqueId);
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            date = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 59);

            endDate = calendar.getTime();
        }
        
        if(Utility.nullToEmpty(request.getEndDate()) != null){
            
            
            Date tempDate = Utility.parseDate(request.getEndDate(), "yyyy-MM-dd");

            if (tempDate == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid end date");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Reschedule", uniqueId);
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(tempDate);

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 59);

            endDate = calendar.getTime();
        }

        Page<PosTransaction> page = posTransactionDao.find(0, 0, null, Utility.emptyToNull(request.getMerchantId()),
                null, Utility.emptyToNull(request.getMerchantCode()), date, endDate, null, null, null, null,
                request.getPosId(), null, Utility.emptyToNull(request.getReference()), request.getAll());

        if (page == null || page.getCount() <= 0) {
            response.put("responsecode", "01");
            response.put("responsemessage", "No transaction to queue");

            return returnResponse(Response.Status.BAD_REQUEST, response, "Reschedule", uniqueId);
        }

        utilityService.queueTransactions(page.getContent());

        response.put("responsecode", "00");
        response.put("responsemessage", "Transactions Queued");

        return returnResponse(Response.Status.OK, response, "Reschedule", uniqueId);
    }

    @Path(value = "pos/provider/transaction/fetch")
    @POST
    public Response pullTransaction(@Valid PosTransactionFromProviderRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        Date date = null, endDate = null;

        Map<String, String> response = new HashMap<>();

        if (Utility.emptyToNull(request.getStartDate()) != null || Utility.emptyToNull(request.getEndDate()) != null) {

            date = Utility.parseDate(request.getStartDate(), "yyyy-MM-dd HH:mm:ss");

            endDate = Utility.parseDate(request.getEndDate(), "yyyy-MM-dd HH:mm:ss");

            if (date == null || endDate == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid date provided (format yyyy-MM-dd HH:mm:ss)");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Provider Transactions", uniqueId);
            }
        }

        PosMerchant merchant = null;

        if (Utility.emptyToNull(request.getPosId()) != null) {
            try {
                merchant = posMerchantDao.findByKey("posId", request.getPosId());
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (merchant == null && Utility.emptyToNull(request.getMerchantCode()) != null) {
            try {
                merchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        utilityService.fetchTransationFromProvider(date, endDate, merchant == null ? null : merchant.getPosId());

        response.put("responsecode", "00");
        response.put("responsemessage", "Transactions Queued");

        return returnResponse(Response.Status.OK, response, "Reschedule", uniqueId);
    }
    
    @Path(value = "pos/provider/transaction/ga/fetch")
    @POST
    public Response pullTransactionGA(@Valid PosTransactionFromProviderRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        Date date = null, endDate = null;

        Map<String, String> response = new HashMap<>();

        if (Utility.emptyToNull(request.getStartDate()) != null || Utility.emptyToNull(request.getEndDate()) != null) {

            date = Utility.parseDate(request.getStartDate(), "yyyy-MM-dd HH:mm:ss");

            endDate = Utility.parseDate(request.getEndDate(), "yyyy-MM-dd HH:mm:ss");

            if (date == null || endDate == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid date provided (format yyyy-MM-dd HH:mm:ss)");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Provider Transactions", uniqueId);
            }
        }

        PosMerchant merchant = null;

        if (Utility.emptyToNull(request.getPosId()) != null) {
            try {
                merchant = posMerchantDao.findByKey("posId", request.getPosId());
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (merchant == null && Utility.emptyToNull(request.getMerchantCode()) != null) {
            try {
                merchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        gaPosService.getTransactionAndSave(date, endDate,request.getTerminalId());

        response.put("responsecode", "00");
        response.put("responsemessage", "Transactions Queued");

        return returnResponse(Response.Status.OK, response, "Reschedule", uniqueId);
    }

    @Path(value = "pos/provider/transaction/failednotification")
    @POST
    public Response pullFailedTransaction(@Valid PosTransactionFromProviderRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        Date date = null, endDate = null;

        Map<String, String> response = new HashMap<>();

        if (Utility.emptyToNull(request.getStartDate()) != null || Utility.emptyToNull(request.getEndDate()) != null) {

            date = Utility.parseDate(request.getStartDate(), "yyyy-MM-dd HH:mm:ss");

            endDate = Utility.parseDate(request.getEndDate(), "yyyy-MM-dd HH:mm:ss");

            if (date == null || endDate == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid date provided (format yyyy-MM-dd HH:mm:ss)");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Provider Transactions", uniqueId);
            }
        }

        PosMerchant merchant = null;

        if (Utility.emptyToNull(request.getPosId()) != null) {
            try {
                merchant = posMerchantDao.findByKey("posId", request.getPosId());
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (merchant == null && Utility.emptyToNull(request.getMerchantCode()) != null) {
            try {
                merchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        utilityService.fetchFailedNotificationTransationFromProvider(date, endDate, merchant == null ? null : merchant.getPosId());

        response.put("responsecode", "00");
        response.put("responsemessage", "Transactions Queued");

        return returnResponse(Response.Status.OK, response, "Reschedule", uniqueId);
    }

    @Path(value = "pos/merchant/attach")
    @POST
    public Response attachPosId(@Valid AttachPosIdRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        try {
            Map<String, String> response = new HashMap<>();

            String title = "Update Pos id";

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.getMerchantCode() + "" + request.getPosId();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            PosMerchant merchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());

            if (merchant == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Merchant not found");

                return returnResponse(Response.Status.BAD_REQUEST, response, "POS ID", uniqueId);
            }

            if (merchant.getPosId() != null && request.getForce() == 0) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Merchant already has a pos id");

                return returnResponse(Response.Status.BAD_REQUEST, response, "POS ID", uniqueId);
            }

            PosMerchant posMerchant = posMerchantDao.findByKey("posId", request.getPosId());

            if (posMerchant != null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Merchant with pos id exist");

                return returnResponse(Response.Status.BAD_REQUEST, response, "POS ID", uniqueId);
            }

            merchant.setPosId(request.getPosId());
            merchant.setModifiedOn(new Date());

            posMerchantDao.update(merchant);

            response.put("responsecode", "00");
            response.put("responsemessage", "Record updated successfully");

            return returnResponse(Response.Status.OK, response, "POS ID Update", uniqueId);
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path(value = "pos/terminal/attachbank")
    @POST
    public Response attachBankToTerminal(@Valid UpdateTerminalRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        try {
            Map<String, String> response = new HashMap<>();

            String title = "Update Pos id";

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.getBankName() + "" + request.getPrefix();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            List<PosTerminal> posTerminals = posTerminalDao.findTerminaWithBankCode(request.getPrefix());

            if (posTerminals == null || posTerminals.isEmpty()) {
                response.put("responsecode", "06");
                response.put("responsemessage", "No Terminal found");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            for (PosTerminal posTerminal : posTerminals) {

                posTerminal.setBankName(request.getBankName());

                posTerminalDao.update(posTerminal);
            }

            response.put("responsecode", "00");
            response.put("responsemessage", "Record updated successfully");

            return returnResponse(Response.Status.OK, response, "POS ID Update", uniqueId);
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path(value = "pos/order")
    @POST
    public Response createOrder(@Valid CreatePosOrderRequest request, @HeaderParam(value = "uniqueid") String uniqueId) {

        Map<String, String> response = new HashMap<>();
        
        try {
            
            String title = "Create POS order";
            
            Log log = new Log();
            log.setAction(title);
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername(uniqueId);
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);
            
            if (uniqueId == null) {
                
                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided");
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);
            
            if (apiUser == null) {
                
                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            String requestString = apiUser.getPrivateKey() + "" + request.getMerchantCode() + "" + request.getCustomerOrderId();
            
            String hashString = Utility.sha512(requestString);
            
            if (!hashString.equalsIgnoreCase(request.getHash())) {
                
                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            PosMerchant posMerchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());
            
            if (posMerchant == null) {
                
                response.put("responsecode", "06");
                response.put("responsemessage", "Merchant not found");
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            List<PosOrder> orders = posOrderDao.findByOrder(request.getMerchantCode(), request.getCustomerOrderId(), request.getPosOrderId());
            
            if (orders != null && !orders.isEmpty()) {
                
                response.put("responsecode", "07");
                response.put("responsemessage", "Duplicate order id exists for merchant");
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            PosOrder order = new PosOrder();
            order.setActive(true);
            order.setCreatedOn(new Date());
            order.setCustomerOrderId(request.getCustomerOrderId());
            order.setMerchantCode(request.getMerchantCode());
            order.setPosOrderId(request.getPosOrderId());
            
            order = posOrderDao.create(order);
            
            response.put("responsecode", "00");
            response.put("responsemessage", "Order created succesfully");
            
            return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.put("responsecode", "RR");
        response.put("responsemessage", "Generic Error");

        return returnResponse(Response.Status.BAD_REQUEST, response, "Create Pos Order", uniqueId);
    }
    
    @Path(value = "pos/report/settlement")
    @POST
    public Response sendSettlementReport(@Valid PosSettlementReportRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        Date date = null, endDate = null;

        Map<String, String> response = new HashMap<>();

        if (request.getDate() != null && !request.getDate().isEmpty()) {

            date = Utility.parseDate(request.getDate(), "yyyy-MM-dd");

            if (date == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid start date");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Reschedule", uniqueId);
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            date = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 59);

            endDate = calendar.getTime();
        }

        
        PosMerchant posMerchant = null;
        
        try {
            posMerchant = posMerchantDao.findByKey("merchantCode", request.getMerchantCode());
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(posMerchant == null){
            
            response.put("responsecode", "01");
            response.put("responsemessage", "No merchant found");

            return returnResponse(Response.Status.BAD_REQUEST, response, "Reschedule", uniqueId);
        }
         String emails[];
         
        if(Utility.emptyToNull(request.getEmails()) == null)
            emails = new String[]{posMerchant.getEmail()};
        else
            emails = request.getEmails().split(",");
            
        utilityService.sendPOSReport(posMerchant, date, endDate, emails);

        response.put("responsecode", "00");
        response.put("responsemessage", "Settlement report sent successfully");

        return returnResponse(Response.Status.OK, response, "Reschedule", uniqueId);
    }

    @Path(value = "pos/transaction/notifymerchant")
    @POST
    public Response notifyTransaction(@Valid PosTransactionRescheduleRequest request,
            @HeaderParam(value = "uniqueid") String uniqueId) {

        Date date = null, endDate = null;

        Map<String, String> response = new HashMap<>();

        if (request.getDate() != null && !request.getDate().isEmpty()) {

            date = Utility.parseDate(request.getDate(), "yyyy-MM-dd");

            if (date == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid start date");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Reschedule", uniqueId);
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            date = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 59);

            endDate = calendar.getTime();
        }
        
        if(Utility.nullToEmpty(request.getEndDate()) != null){
            
            
            Date tempDate = Utility.parseDate(request.getEndDate(), "yyyy-MM-dd");

            if (tempDate == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid end date");

                return returnResponse(Response.Status.BAD_REQUEST, response, "Reschedule", uniqueId);
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(tempDate);

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 59);

            endDate = calendar.getTime();
        }

        Page<PosTransactionNew> page = posTransactionNewDao.find(0, 0, null, Utility.emptyToNull(request.getMerchantId()),
                null, Utility.emptyToNull(request.getMerchantCode()), date, endDate, null, null, null, null,
                request.getPosId(), null, Utility.emptyToNull(request.getReference()), request.getAll());

        if (page == null || page.getCount() <= 0) {
            response.put("responsecode", "01");
            response.put("responsemessage", "No transaction to queue");

            return returnResponse(Response.Status.BAD_REQUEST, response, "Reschedule", uniqueId);
        }

        for(int i = 0; i < page.getCount() ; i++){
        
            PosTransactionNew transactionNew = page.getContent().get(i);
            
            PosMerchant posMerchant = null;
            
            try {
                posMerchant = posMerchantDao.findByKey("posId", transactionNew.getPosId());
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if(posMerchant == null)
                continue;
            
            notifyMerchant(posMerchant, transactionNew);
        }

        response.put("responsecode", "00");
        response.put("responsemessage", "Transactions Queued");

        return returnResponse(Response.Status.OK, response, "Reschedule", uniqueId);
    }
    
    @Path(value = "/pos/transactions/sendbox/verify_payment")
    @POST
    public Response verifyPosTransaction(@Valid PosVerificationRequest request){
    
        Log log = new Log();
        log.setAction("Verify Pos Transaction");
        log.setCreatedOn(new Date());
        log.setDescription(request.toString());
        log.setLogState(LogState.STARTED);
        log.setLogDomain(LogDomain.API);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername("NONE");
        
        logService.log(log);
        
        Properties properties = Utility.getConfigProperty();
        
        String hashKey = properties.getProperty("sendbox_hash_key");
        
        String computedHash = CryptoUtil.hmacSHA512(request.getTransactionReference().getBytes(), hashKey.getBytes());
              
        logService.log("Sendbox Computed Hash "+computedHash);
        
        PosTransactionVerificationResponse verificationResponse = new PosTransactionVerificationResponse();
        
        if(!computedHash.equalsIgnoreCase(request.getHash())) {
            
            verificationResponse.setResponseCode("01");
            verificationResponse.setResponseMessage("Invalid Hash");
            return  Response.status(Response.Status.BAD_REQUEST).entity(verificationResponse).build();
        }
        
        PosTransactionNew posTransactionNew = null;
        
        try {
            posTransactionNew = posTransactionNewDao.findByKey("rrn", request.getTransactionReference());
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).error(ex);
        }
        
        if(posTransactionNew == null){
            
            verificationResponse.setResponseCode("03");
            verificationResponse.setResponseMessage("Invalid transaction");
            return  Response.status(422).entity(verificationResponse).build();
        }
        
        verificationResponse.setAmount(posTransactionNew.getAmount()+"");
        verificationResponse.setResponseCode(posTransactionNew.getResponseCode());
        verificationResponse.setResponseMessage(posTransactionNew.getResponseMessage());
        verificationResponse.setTransStatus(posTransactionNew.getStatus());
        verificationResponse.setReference(posTransactionNew.getTransRef());
        
        log = new Log();
        log.setAction("Verify Pos Transaction Response");
        log.setCreatedOn(new Date());
        log.setDescription(verificationResponse.toString());
        log.setLogState(LogState.STARTED);
        log.setLogDomain(LogDomain.API);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername("NONE");
        
        logService.log(log);
        
        return  Response.status(Response.Status.OK).entity(verificationResponse).build();
        
    }
    
    private void notifyMerchant(PosMerchant merchant, PosTransactionNew posTransactionNew){
        
        if(!"00".equalsIgnoreCase(posTransactionNew.getResponseCode()))
                return;
        
        if (merchant != null) {
                        
            if( null != merchant.getCategory() )

                switch (merchant.getCategory()) {
                case WAKANOW:

                    wakanowService.logPayment(posTransactionNew);
                    break;
                case SENDBOX:

                    sendBoxPosService.logTransaction(posTransactionNew);
                    break;    
                default:
                    break;
            }
        }
        
    }
        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.recon;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.flutterwave.flutter.clientms.controller.api.model.recon.SummaryReportModel;
import com.flutterwave.flutter.clientms.dao.recon.FlutterTransactionDao;
import com.flutterwave.flutter.clientms.dao.recon.ReconTransactionDao;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.viewmodel.AnalysisModel;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author emmanueladeyemi
 */
@RequestScoped
@Path("/recon/report")
public class ReconReportApiController {
    
    @EJB
    private FlutterTransactionDao flutterTransactionDao;
    @EJB
    private ReconTransactionDao transactionDao;
    @Inject
    private Global global;
    
     double totalSum = 0, totalSettled;
    
    @Path(value = "/data")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map getReport(@QueryParam(value = "daterange") String range,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "merchant") String merchant,
            @QueryParam(value = "status") String status){
        
        
        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        List<Object[]> list = flutterTransactionDao.getReport(startDate, endDate, "ALL".equalsIgnoreCase(currency) ? null : currency, 
                "".equalsIgnoreCase(status) == true ? null : "successful".equalsIgnoreCase(status) , "all".equalsIgnoreCase(merchant) ? null : merchant);
        
        Map<String, AnalysisModel> values = new HashMap<>();
        
        list.forEach((obj) -> {
            Double sum = (Double) obj[0];
            String response = (String) obj[1];
            BigInteger count = (BigInteger) obj[2];
            String currencyName = (String) obj[3];
            String scheme = (String)obj[4];

//            Map<String, AnalysisModel> temp = values.getOrDefault(currencyName, new HashMap<>());
            long value = 0;
            
            AnalysisModel model = values.getOrDefault(currencyName, new AnalysisModel());
            
            if(! ("0".equalsIgnoreCase(response) || "00".equalsIgnoreCase(response))){
                
                if("mc".equalsIgnoreCase(scheme)){
                    model.setMcFailedAmount(model.getMcFailedAmount()+ (sum == null ? 0 : sum));
                    model.setMcFailedVolume(model.getMcFailedVolume() +(count == null ? 0 : count.longValue()));
                }else if("vc".equalsIgnoreCase(scheme)){
                    model.setVcFailedAmount(model.getVcFailedAmount()+ (sum == null ? 0 : sum));
                    model.setVcFailedVolume(model.getVcFailedVolume() +(count == null ? 0 : count.longValue()));
                }
                
            }else{
                
                if("mc".equalsIgnoreCase(scheme)){
                    model.setMcSuccessAmount(model.getMcSuccessAmount()+ (sum == null ? 0 : sum));
                    model.setMcSuccessVolume(model.getMcSuccessVolume() +(count == null ? 0 : count.longValue()));
                }else if("vc".equalsIgnoreCase(scheme)){
                    model.setVcSuccessAmount(model.getVcSuccessAmount()+ (sum == null ? 0 : sum));
                    model.setVcSuccessVolume(model.getVcSuccessVolume() +(count == null ? 0 : count.longValue()));
                }
                
            }
            
            values.put(currencyName, model);
            

        });
        
        return values;
    }
    
    @Path(value = "/rsummary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public SummaryReportModel getTotalSettlementReport(@QueryParam(value = "currency") String currency){
        
        SummaryReportModel reportModel = new SummaryReportModel();
        
        currency = TransactionUtility.getCurrencyCode(currency);
        
        List list = transactionDao.getSummary(null, null, currency, global.getDomainId(), null);
        
        Object[] data =  (Object[]) list.get(0);
        
        Double amount = (Double)data[0];
        
        reportModel.setTotal(amount == null ? 0 : amount);
        
        Calendar calendar = Calendar.getInstance();
        
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        
        Date startDate = calendar.getTime();
        
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        
        Date endDate = calendar.getTime();
        
        list = transactionDao.getSummary(startDate, endDate, currency, global.getDomainId(), null);
        
        data =  (Object[]) list.get(0);
        
        amount = (Double)data[0];
        
        reportModel.setMonthTotal(amount == null ? 0 : amount);
        reportModel.setCurrency(TransactionUtility.getCurrencyName(currency));
        
        double[] sum = transactionDao.getSum(startDate, endDate, null, null, currency, true);
        
        reportModel.setMonthSettled(sum[0]);
        
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        
        startDate = calendar.getTime();
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        
        endDate = calendar.getTime();
        
        list = transactionDao.getSummary(startDate, endDate, currency, global.getDomainId(), null);
        
        data =  (Object[]) list.get(0);
        
        amount = (Double)data[0];
        
        reportModel.setAmountAt(amount == null ? 0 : amount);
        reportModel.setDate(endDate);
        
        sum = transactionDao.getSum(startDate, endDate, null, null, currency, true);
        
        reportModel.setSettledAt(sum[0]);
        
        return reportModel;
    }
    
    @Path(value = "/worth")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map<String, Object> getWorth(@QueryParam("currency") String currency,
            @QueryParam("daterange") String range, @QueryParam("merchant") String merchant ){
        
        try{
            Date startDate = null, endDate = null;

            String currencyInt = TransactionUtility.getCurrencyCode(currency);
            
            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }
            }
            
            List<Object[]> list = transactionDao.getSumSettlement(startDate, endDate, global.getDomainId(), null, currencyInt,
                    merchant == null ? null : "all".equalsIgnoreCase(merchant) ? null : merchant , null);
            
            Map<String, Object> values = new HashMap<>();
            
//            long currencyC = Integer.parseInt(currency);
            NumberFormat formatter = new DecimalFormat("#0.00");
            
            totalSum = 0; totalSettled = 0;
            
            list.forEach((obj) -> {
                Double sum = (Double) obj[0];
                BigInteger count = (BigInteger) obj[1];
                int currencyCode = (Integer) obj[2];
                String scheme = (String)obj[3];
                boolean status = (Boolean)obj[4];
                if (currency.equalsIgnoreCase(TransactionUtility.getCurrencyName(currencyCode+""))) {
                    
                    Map<String, String> temp = (Map<String, String>) values.getOrDefault(scheme, new HashMap<>());
                    temp.put(true == status ? "success" : "failure", formatter.format(sum));
                    
                    totalSum += sum;
                    if(true == status)
                        totalSettled += sum;
                    
                    values.put(scheme, temp);
                }
            });
            
            List<String> schemes = TransactionUtility.getScheme();
            
            schemes.forEach((s) -> {
                Map<String, String> k = (Map<String, String>)values.getOrDefault(s, null);
                
                if(k == null){
                    k = new HashMap<>();
                    k.put("success", "0");
                    k.put("failure", "0");
                    values.put(s, k);
                }else{
                    String value = k.getOrDefault("success", null);
                    
                    if(value == null){
                        k.put("success", "0");                        
                    }
                    
                    value = k.getOrDefault("failure", null);
                    
                    if(value == null){
                        k.put("failure", "0");                        
                    }
                    
                    values.put(s, k);
                }
                
            });
            
            values.put("totalSettled", totalSettled);
            values.put("totalSum", totalSum);
            
            return values;
            
        }catch(Exception ex){
            if(ex != null)
                ex.printStackTrace();
        }
        
        return new HashMap();
    }
    
    public String getSettlementReport(@QueryParam(value = "daterange") String range,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "merchant") String merchant,
            @QueryParam(value = "status") String status){
        
        return null;
    }
}

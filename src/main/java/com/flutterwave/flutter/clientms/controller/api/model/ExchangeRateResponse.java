/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author emmanueladeyemi
 */
public class ExchangeRateResponse {

    /**
     * @return the rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(double rate) {
        this.rate = rate;
    }

    /**
     * @return the lastUpdated
     */
    public String getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * @return the originCurrency
     */
    public String getOriginCurrency() {
        return originCurrency;
    }

    /**
     * @param originCurrency the originCurrency to set
     */
    public void setOriginCurrency(String originCurrency) {
        this.originCurrency = originCurrency;
    }

    /**
     * @return the destinationCurrency
     */
    public String getDestinationCurrency() {
        return destinationCurrency;
    }

    /**
     * @param destinationCurrency the destinationCurrency to set
     */
    public void setDestinationCurrency(String destinationCurrency) {
        this.destinationCurrency = destinationCurrency;
    }
    
    @JsonProperty(value = "origincurrency")
    private String originCurrency;
    @JsonProperty(value = "destinationcurrency")
    private String destinationCurrency;
    private double rate;
    @JsonProperty(value = "lastupdated")
    private String lastUpdated;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class ActivateMposTerminalRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the activationCode
     */
    public String getActivationCode() {
        return activationCode;
    }

    /**
     * @param activationCode the activationCode to set
     */
    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }
    
//    @NotBlank(message = "Merchant id must be provided")
    @JsonProperty(value = "pwcmerchantid")
    private String merchantId;
    @NotBlank(message = "Activation code must be provided")
    @JsonProperty(value = "activationcode")
    private String activationCode;
    @NotBlank(message = "Hash must be provided")
    private String hash;

    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("merchantid", merchantId == null ? "" : merchantId)
                .put("activationCode", activationCode)
                .put("hash", getHash());
        
        return jSONObject.toString();
    }
    
    public String toHashableString(){
        
        StringBuilder builder = new StringBuilder();
        builder.append(merchantId == null ? "" : merchantId)
                .append(activationCode) ;       
        
        return builder.toString();
    }
                
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.api.model.ReversalRequest;
import com.flutterwave.flutter.clientms.controller.api.model.TellerPointCallbackRequest;
import com.flutterwave.flutter.clientms.controller.api.model.TellerPointReversalRequest;
import com.flutterwave.flutter.clientms.dao.ApiUserDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.MPosSettlementDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantCustomerDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.dao.PosOrderDao;
//import com.flutterwave.flutter.clientms.dao.PosOrderDao;
//import com.flutterwave.flutter.clientms.dao.PosOrderDao;
import com.flutterwave.flutter.clientms.dao.PosTerminalDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionNewDao;
//import com.flutterwave.flutter.clientms.dao.PosTransactionNewDao;
import com.flutterwave.flutter.clientms.dao.TellerPointTransactionDao;
//import com.flutterwave.flutter.clientms.dao.TellerPointTransactionDao;
import com.flutterwave.flutter.clientms.model.ApiUser;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.MPosSettlement;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosMerchantCustomer;
import com.flutterwave.flutter.clientms.model.PosOrder;
//import com.flutterwave.flutter.clientms.model.PosOrder;
//import com.flutterwave.flutter.clientms.model.PosOrder;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.model.PosTransactionNew;
import com.flutterwave.flutter.clientms.model.TellerPointTransaction;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.UtilityService;
import com.flutterwave.flutter.clientms.util.CryptoUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.RequestScoped;
import javax.json.Json;
import javax.json.JsonObject;
import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author emmanueladeyemi
 */
@RequestScoped
@Path(value = "/v1/callback")
public class CallBackApiController {
    
    @EJB
    private LogService logService;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private MPosSettlementDao mPosSettlementDao;
    @EJB
    private UtilityService utilityService;
    @EJB
    private ApiUserDao apiUserDao;
    @EJB
    private PosTransactionDao posTransactionDao;
    @EJB
    private TellerPointTransactionDao pointTransactionDao;
    @EJB
    private PosTerminalDao posTerminalDao;
    @EJB
    private PosMerchantDao posMerchantDao;
    @EJB
    private PosTransactionNewDao posTransactionNewDao;
    @EJB
    private PosOrderDao posOrderDao;
    @EJB
    private PosMerchantCustomerDao posMerchantCustomerDao;
    
    
    @Path(value = "/mpos")
    @POST
    @Produces(value =  MediaType.APPLICATION_JSON)
    public Response mposCallback(@HeaderParam(value = "x-paypad-signature") String header, 
            String request){
    
        try {
            Log log = new Log();
            log.setAction("MPOS Callback");
            log.setCreatedOn(new Date());
            log.setDescription(request);
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(header);
            
            logService.log(log);
            
            String key = configurationDao.getConfig("mpos_key");
            
            JsonObject requestObject = Json.createReader(new StringReader(request)).readObject();
            
            String resString = requestObject.toString();
            
            String hash = CryptoUtil.hmacSHA512(resString.getBytes(), key.getBytes());
            
            if(header == null){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "header x-paypad-signature must be provided");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            if(!header.equalsIgnoreCase(hash)){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "Invalid hash value");
                
                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }
            
            
            String uniqueReference = requestObject.getString("uniqueref", null);
            
            MPosSettlement mPosSettlement =  mPosSettlementDao.findByKey("uniqueref", uniqueReference);
            
            if(mPosSettlement != null){
                
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Duplicate transaction");
                
                return Response.status(Response.Status.CONFLICT).entity(response).build();
            }
            
            mPosSettlement = new MPosSettlement();
            try{
                mPosSettlement.setAmount(requestObject.getJsonNumber("amount").doubleValue());
            } catch(Exception ex){
                
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Invalid amount provided");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            mPosSettlement.setCreatedOn(new Date());
            mPosSettlement.setMerchantCode(requestObject.getString("merchantcode", null));
            mPosSettlement.setNarration(requestObject.getString("narration", null));
            mPosSettlement.setPayerPhoneNumber(requestObject.getString("payerphonenumber", null));
            mPosSettlement.setTerminalId(requestObject.getString("terminalId", null));
            mPosSettlement.setStatusCode(requestObject.getString("merchantcode", null));
            mPosSettlement.setStatusMessage(requestObject.getString("statusmessage", null));
            mPosSettlement.setUniqueref(requestObject.getString("uniqueref", null));
            mPosSettlement.setPaymentReference(requestObject.getString("paymentreference", null));
            mPosSettlement.setTransactionRef(requestObject.getString("transactionref", null));
            mPosSettlement.setActualData(request);
            
            mPosSettlementDao.create(mPosSettlement);
            
            Map<String, String> response = new HashMap<>();
            response.put("status", "success");
            response.put("description", "Callback successful");
            
            String callbackUrl = configurationDao.getConfig("rave_mpos_callback");
            
            if(callbackUrl != null)
                utilityService.makeCallBackAsync(request, header, uniqueReference, callbackUrl);
            
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(CallBackApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    
    @Path(value = "/pos/tellerpoint")
    @POST
    @Produces(value =  MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response tellerPointPosCallback(@HeaderParam(value = "uniqueid") String header, 
            @Valid  TellerPointCallbackRequest request){
    
        try {
            Log log = new Log();
            log.setAction("Teller Point Callback");
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(header);
            
            logService.log(log);
            
            if(header == null){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "header uniqueid must be provided");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            ApiUser apiUser = apiUserDao.findByKey("uniqueId", header);
            
            if(apiUser == null){
               
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "Api user cannot be authorized");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            String hashParam = apiUser.getPrivateKey()+""+request.toHashable();
            
            String hash = CryptoUtil.sha512(hashParam, null);
            
            if(!request.getHash().equalsIgnoreCase(hash)){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "Invalid hash value");
                
                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }
            
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", request.getTerminalId());
            
            if(posTerminal == null){
                
                Map<String, String> response = new HashMap<>();
                response.put("responsecode", "06");
                response.put("responsemessage", "Terminal does not exist");

                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

            
            List<TellerPointTransaction> transactions = pointTransactionDao.findByTerminalAndReference(request.getTerminalId(), request.getMerchantId(), request.getTellerpointRef());
            
            if(transactions != null && !transactions.isEmpty()){
                
                Map<String, String> response = new HashMap<>();
                response.put("status", "success");
                response.put("description", "Transaction already saved");
                
                return Response.status(Response.Status.OK).entity(response).build();
            }
            
            TellerPointTransaction pointTransaction = new TellerPointTransaction();
            pointTransaction.setAcquiringBank(request.getAcquiringBank());
            pointTransaction.setAmount(request.getAmount());
            pointTransaction.setApprovalCode(request.getApprovalCode());
            pointTransaction.setCreatedBy(header);
            pointTransaction.setCardHolderIssuingBank(request.getCardHolderIssuingBank());
            pointTransaction.setCardholderName(request.getCardholderName());
            pointTransaction.setCreatedOn(new Date());
            pointTransaction.setCreationTime(Utility.parseDate(request.getCreationTime(), "yyyy-MM-dd HH:mm:ss Z"));
            
            PosTransaction posTransaction = posTransactionDao.findRRNTerminal(request.getTraceRef(), 
                    request.getTerminalId(), pointTransaction.getCreationTime() );
            
            if(posTransaction != null){
                Map<String, String> response = new HashMap<>();
                response.put("status", "success");
                response.put("description", "Transaction already saved.");
                
                return Response.status(Response.Status.OK).entity(response).build();
            }
            
            pointTransaction.setMaskedCardPan(request.getMaskedCardPan());
            pointTransaction.setMerchantName(request.getMaskedCardPan());
            pointTransaction.setPosId(request.getMerchantId());
            pointTransaction.setProviderRef(request.getProviderRef());
            pointTransaction.setStatus(request.getStatus());
            pointTransaction.setTellerpointRef(request.getTellerpointRef());
            pointTransaction.setTerminalId(request.getTerminalId());
            pointTransaction.setTraceRef(request.getTraceRef());
            pointTransaction.setTransactionCharge(request.getTransactionCharge());
            
            String responseRef = "flw"+Utility.generateReference();
            
            pointTransaction.setReversalResponseRef(responseRef);
            
            pointTransactionDao.create(pointTransaction);
            
            String rrn = request.getTraceRef();
            
            Long rrnLong = Long.parseLong(rrn);
                    
            rrn = String.valueOf(rrnLong);
            
            Map<String, String> response = new HashMap<>();
            response.put("status", "success");
            response.put("description", "Callback successful");
            response.put("responseref", responseRef );
            
            PosMerchant merchant = posMerchantDao.findByKey("posId", request.getMerchantId());
            
            PosMerchantCustomer posMerchantCustomer = null;
            PosOrder order = null;
            
            if(merchant == null){
                
                order = posOrderDao.findByKey("posOrderId", request.getMerchantId());
                
                if(order != null){
                    
                    merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());
                }
                
                if(merchant == null){
                    
                    posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", request.getMerchantId());
                    
                    if(posMerchantCustomer != null){
                        merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());
                    }
                }
            }
            
            posTransaction = new PosTransaction();
            posTransaction.setAmount(pointTransaction.getAmount());
            posTransaction.setCreatedBy(null);
            posTransaction.setCreatedOn(new Date());
            posTransaction.setCurrency("NGN");
            posTransaction.setLoggedBy(header);
            posTransaction.setPan(pointTransaction.getMaskedCardPan());
            posTransaction.setRrn(rrn);
            posTransaction.setTerminalId(pointTransaction.getTerminalId());
            posTransaction.setRefCode(pointTransaction.getTellerpointRef());
            posTransaction.setFileId(pointTransaction.getProviderRef());
            posTransaction.setFileName("HexTremeLab");
            posTransaction.setCurrencyCode("566");
            
            posTransaction.setPosId(pointTransaction.getPosId());
            
            if(merchant != null){
                posTransaction.setPosId(merchant.getPosId());
                posTransaction.setMerchantName(merchant.getName());
            }
            
            posTransaction.setRequestDate(pointTransaction.getCreationTime());
            posTransaction.setResponseDate(pointTransaction.getCreationTime());
            posTransaction.setResponseCode("00");
            posTransaction.setResponseMessage(pointTransaction.getStatus());
            posTransaction.setType("PURCHASE");
            posTransaction.setSource("HexTremeLab "+pointTransaction.getTraceRef() + " "+ responseRef);
            posTransaction.setStatus(pointTransaction.getStatus());
            posTransaction.setTraceref(pointTransaction.getTraceRef());
            
            String transRef = request.getTerminalId()+""+rrn+""+posTransaction.getAmount()+""+Utility.formatDate(posTransaction.getRequestDate(), "yyyyMMdd");
            
            posTransaction.setTransRef(transRef);
            
            posTransaction = posTransactionDao.create(posTransaction);
           
            
            try{
            PosTransactionNew posTransactionNew = null;
            
            if(merchant != null){
                posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, null, request.getAcquiringBank(),
                        request.getCardholderName(), null, merchant.getParent() == null ? null : merchant.getParent().getName(), merchant.getMerchantId(),
                        merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), request.getCardHolderIssuingBank(),
                        merchant,posTerminal.getProvider(), posMerchantCustomer);
            }else{
                posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, null, request.getAcquiringBank(),
                        request.getCardholderName(), null, null , null,
                        null, posTerminal.getProvider().getName(), null, request.getCardHolderIssuingBank(),
                        merchant,posTerminal.getProvider(), posMerchantCustomer);
            }
            
            PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(request.getTraceRef(), 
                    request.getTerminalId(), pointTransaction.getCreationTime() );
            
            if(posTransactionN == null){
                posTransactionNewDao.create(posTransactionNew);
            }else{
                
                posTransactionNew.setId(posTransactionN.getId());
                posTransactionNewDao.update(posTransactionNew);
            }
            
            }catch(Exception ex){ex.printStackTrace();}
            
            if(posTransaction != null)
                utilityService.callBackUserETOP(posTransaction, pointTransaction.getCardholderName(), false);
            
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(CallBackApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    
    @Path(value = "/pos/tellerpoint")
    @DELETE
    @Produces(value =  MediaType.APPLICATION_JSON)
    public Response tellerPointPosReversalCallback(@HeaderParam(value = "uniqueid") String header,
            @Valid TellerPointReversalRequest request){
    
        try {
            Log log = new Log();
            log.setAction("Teller Point Reversal Callback");
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(header);
            
            logService.log(log);
            
            if(header == null){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "header uniqueid must be provided");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            
            ApiUser apiUser = apiUserDao.findByKey("uniqueId", header);
            
            if(apiUser == null){
               
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "Api user cannot be authorized");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            String hashParam = apiUser.getPrivateKey()+""+request.getReference();
            
            String hash = CryptoUtil.sha512(hashParam, null);
            
            
            if(!request.getHash().equalsIgnoreCase(hash)){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "Invalid hash value");
                
                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }
            
            
            TellerPointTransaction pointTransaction = pointTransactionDao.findByKey("tellerpointRef", request.getReference());
            
            if(pointTransaction == null){
                
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Transaction not found");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            String responseRef = "flwr"+Utility.generateReference();
            
            pointTransaction.setReversed(true);
            pointTransaction.setReversedOn(new Date());
            pointTransaction.setReversalResponseRef(responseRef);
            
            pointTransactionDao.update(pointTransaction);
            
            Map<String, String> response = new HashMap<>();
            response.put("status", "success");
            response.put("description", "Callback successful");
            response.put("responseref", responseRef );
                        
            PosTransaction posTransaction = posTransactionDao.findByKey("refCode", request.getReference());
            
            if(posTransaction != null){
                
                posTransaction.setReversed(true);
                posTransaction.setReversedOn(pointTransaction.getReversedOn());
                
                posTransactionDao.update(posTransaction);
                
                utilityService.callBackUserETOP(posTransaction, "", true);
                
                try{
                    PosTransactionNew posTransactionNew =  posTransactionNewDao.findRRNTerminal(posTransaction.getRrn(), 
                        posTransaction.getTerminalId(), posTransaction.getPan());
                    
                    if(posTransactionNew != null){
                        
                        posTransactionNew.setReversed(true);
                        posTransactionNew.setReversedOn(new Date());
                        
                        posTransactionNewDao.update(posTransactionNew);
                    }
                    
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            
            
            
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(CallBackApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    
    @Path(value = "/pos/reversal")
    @DELETE
    @Produces(value =  MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response posReversalCallback(@HeaderParam(value = "uniqueid") String header,
            @Valid ReversalRequest request){
    
        try {
            Log log = new Log();
            log.setAction("Reversal Callback");
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(header);
            
            logService.log(log);
            
            if(header == null){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "header uniqueid must be provided");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            ApiUser apiUser = apiUserDao.findByKey("uniqueId", header);
            
            if(apiUser == null){
               
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "Api user cannot be authorized");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            String hashParam = apiUser.getPrivateKey()+""+request.toHashable();
            
            String hash = CryptoUtil.sha512(hashParam, null);
            
            if(!request.getHash().equalsIgnoreCase(hash)){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "Invalid hash value");
                
                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }
            
            double amount = 0.0;
            
            try{                
                amount = Double.parseDouble(request.getAmount());
            }catch(NumberFormatException ex){
                
                amount = 0.0;
            }            

            try{
                long rrn = Long.parseLong(request.getRrn());
                
                request.setRrn(rrn+"");
                
            }catch(NumberFormatException ex){
                
                 Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "RRN must be numeric");
                
                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }
            
            
            PosTransaction posTransaction = posTransactionDao.findRRNTerminal(request.getRrn(), 
                    request.getTerminalId(), request.getMaskedPan());
            
            if(posTransaction == null){
            
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Transaction not found");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            if(posTransaction.getAmount() != amount){
                
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Transaction not found");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            if(!posTransaction.getPan().equalsIgnoreCase(request.getMaskedPan())){
                
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Transaction not found");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

            if(posTransaction.isReversed() == true){
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Transaction already reversed");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
//            String responseRef = "flwr"+Utility.generateReference();
            
            Map<String, String> response = new HashMap<>();
            response.put("status", "success");
            response.put("description", "Callback successful");
                        
            
//            if(posTransaction != null){
                
                posTransaction.setReversed(true);
                posTransaction.setReversedOn(new Date());
                
                posTransactionDao.update(posTransaction);
                
                try{
                    PosTransactionNew posTransactionNew =  posTransactionNewDao.findRRNTerminal(request.getRrn(), 
                        request.getTerminalId(), request.getMaskedPan());
                    
                    if(posTransactionNew != null){
                        
                        posTransactionNew.setReversed(true);
                        posTransactionNew.setReversedOn(new Date());
                        
                        posTransactionNewDao.update(posTransactionNew);
                    }
                    
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                
                utilityService.callBackUserReschedule(posTransaction);
//            }
            
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(CallBackApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    
    @Path(value = "/pos/etop/reversal")
    @DELETE
    @Produces(value =  MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response posETPReversalCallback(@HeaderParam(value = "uniqueid") String header,
            @Valid ReversalRequest request){
    
        try {
            Log log = new Log();
            log.setAction("ETOP Reversal Callback");
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(header);
            
            logService.log(log);
            
            if(header == null){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "header uniqueid must be provided");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            ApiUser apiUser = apiUserDao.findByKey("uniqueId", header);
            
            if(apiUser == null){
               
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "Api user cannot be authorized");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            String hashParam = apiUser.getPrivateKey()+""+request.toHashable();
            
            String hash = CryptoUtil.sha512(hashParam, null);
            
            if(!request.getHash().equalsIgnoreCase(hash)){
                
                Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "Invalid hash value");
                
                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }
            
            double amount = 0.0;
            
            try{                
                amount = Double.parseDouble(request.getAmount());
            }catch(NumberFormatException ex){
                
                amount = 0.0;
            } 
            
//            amount = amount / 100;

            try{
                long rrn = Long.parseLong(request.getRrn());
                
                request.setRrn(rrn+"");
                
            }catch(NumberFormatException ex){
                
                 Map<String, String> response = new HashMap<>();
                
                response.put("status", "failed");
                response.put("description", "RRN must be numeric");
                
                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }
            
            
            PosTransaction posTransaction = posTransactionDao.findRRNTerminal(request.getRrn(), 
                    request.getTerminalId(), request.getMaskedPan());
            
            if(posTransaction == null){
            
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Transaction not found");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            if(posTransaction.getAmount() != amount){
                
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Transaction not found");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
            if(!posTransaction.getPan().equalsIgnoreCase(request.getMaskedPan())){
                
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Transaction not found");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

            if(posTransaction.isReversed() == true){
                Map<String, String> response = new HashMap<>();
                response.put("status", "failed");
                response.put("description", "Transaction already reversed");
                
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            
//            String responseRef = "flwr"+Utility.generateReference();
            
            Map<String, String> response = new HashMap<>();
            response.put("status", "success");
            response.put("description", "Callback successful");
                        
            
//            if(posTransaction != null){
                
                posTransaction.setReversed(true);
                posTransaction.setReversedOn(new Date());
                
                posTransactionDao.update(posTransaction);
                
                try{
                    PosTransactionNew posTransactionNew =  posTransactionNewDao.findRRNTerminal(request.getRrn(), 
                        request.getTerminalId(), request.getMaskedPan());
                    
                    if(posTransactionNew != null){
                        
                        posTransactionNew.setReversed(true);
                        posTransactionNew.setReversedOn(new Date());
                        
                        posTransactionNewDao.update(posTransactionNew);
                    }
                    
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                
                utilityService.callBackUserReschedule(posTransaction);
//            }
            
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(CallBackApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
//    
    @Path(value = "/core/transaction/log")
    @DELETE
    @Produces(value =  MediaType.APPLICATION_JSON)
    public Response transactionLog(){
        
        return null;
    }
}

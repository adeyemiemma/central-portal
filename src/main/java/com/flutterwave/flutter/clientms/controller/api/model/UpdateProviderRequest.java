/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author emmanueladeyemi
 */
public class UpdateProviderRequest {

    /**
     * @return the network
     */
    public String getNetwork() {
        return network;
    }

    /**
     * @param network the network to set
     */
    public void setNetwork(String network) {
        this.network = network;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }
    
    @NotBlank(message = "Network must be provided")
    private String network;
    @NotBlank(message = "Provider must be provided")
    private String provider;
}

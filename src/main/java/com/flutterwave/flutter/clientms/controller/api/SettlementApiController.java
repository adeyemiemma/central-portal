/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.api.model.SettlementCycleModel;
import com.flutterwave.flutter.clientms.controller.api.model.UpdateSettlementCycleRequest;
import com.flutterwave.flutter.clientms.dao.AirtimeTransactionDao;
import com.flutterwave.flutter.clientms.dao.ChargeBackDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationFeeDao;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CountryDao;
import com.flutterwave.flutter.clientms.dao.CurrencyDao;
import com.flutterwave.flutter.clientms.dao.ExchangeRateDao;
import com.flutterwave.flutter.clientms.dao.FlutterwaveSettlementDao;
import com.flutterwave.flutter.clientms.dao.MerchantCompliantDao;
import com.flutterwave.flutter.clientms.dao.MerchantConfigDao;
import com.flutterwave.flutter.clientms.dao.MerchantFeeDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.ProviderDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionWHDao;
import com.flutterwave.flutter.clientms.dao.SettlementCycleDao;
import com.flutterwave.flutter.clientms.dao.SettlementDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.WalletDao;
import com.flutterwave.flutter.clientms.dao.maker.MerchantConfigMDao;
import com.flutterwave.flutter.clientms.dao.maker.SettlementMDao;
import com.flutterwave.flutter.clientms.model.ConfigurationFee;
import com.flutterwave.flutter.clientms.model.Country;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.model.FlutterwaveSettlement;
import com.flutterwave.flutter.clientms.model.Settlement;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Merchant;
import com.flutterwave.flutter.clientms.model.MerchantCompliance;
import com.flutterwave.flutter.clientms.model.MerchantConfig;
import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosSettlementModel;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.RaveTransactionWH;
import com.flutterwave.flutter.clientms.model.SettlementCycle;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.MerchantConfigM;
import com.flutterwave.flutter.clientms.model.maker.SettlementM;
import com.flutterwave.flutter.clientms.model.maker.UpdateListModel;
import com.flutterwave.flutter.clientms.model.maker.UpdateModel;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.service.FlutterwaveSettlementQueueManager;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.SettlementService;
import com.flutterwave.flutter.clientms.service.TerminalService;
import com.flutterwave.flutter.clientms.service.WalletService;
import com.flutterwave.flutter.clientms.util.CreateRuleModel;
import com.flutterwave.flutter.clientms.util.CreateTransactionModel;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.RaveSettlementModel;
import com.flutterwave.flutter.clientms.viewmodel.RaveSettlementViewModel;
import com.flutterwave.flutter.clientms.viewmodel.SettlementViewModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Path(value = "/settlement")
@RequestScoped
public class SettlementApiController {

    @EJB
    private ProviderDao providerDao;
    @EJB
    private TransactionDao transactionDao;
    @EJB
    private LogService logService;
    @EJB
    private SettlementDao settlementDao;
    @EJB
    private SettlementMDao settlementMDao;
    @EJB
    private UserDao userDao;
    @EJB
    private MerchantConfigDao merchantConfigDao;
    @EJB
    private MerchantConfigMDao merchantConfigMDao;
    @EJB
    private FlutterwaveSettlementDao flutterwaveSettlementDao;
    @EJB
    private FlutterwaveSettlementQueueManager settlementQueueManager;
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private ConfigurationFeeDao configurationFeeDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private WalletService walletService;
    @EJB
    private WalletDao walletDao;
    @EJB
    private CurrencyDao currencyDao;
    @EJB
    private ChargeBackDao chargeBackDao;
    @EJB
    private MerchantFeeDao merchantFeeDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private MerchantCompliantDao merchantCompliantDao;
    @EJB
    private CountryDao countryDao;
    @EJB
    private ExchangeRateDao exchangeRateDao;
    @EJB
    private RaveTransactionDao raveTransactionDao;
    @EJB
    private RaveTransactionWHDao raveTransactionWHDao;
    @EJB
    private SettlementCycleDao settlementCycleDao;
    @EJB
    private AirtimeTransactionDao airtimeTransactionDao;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private PosTransactionDao posTransactionDao;
    @EJB
    private TerminalService terminalService;
    @EJB
    private SettlementService settlementService;
    
    @Inject
    private Global global;
    

//    BigDecimal totalFee, totalCost, totalValue;

    @Path("/provider")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getData(@QueryParam(value = "provider") String provider, @QueryParam(value = "category") String category, 
            @QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency,
            @QueryParam(value = "settled") String settled, @QueryParam(value = "merchant") String merchant, @QueryParam(value = "grouping") String grouping) {

        try {

            if (currency == null || "".equals(currency)) {
                currency = "NGN";
            }

            List<SettlementViewModel> transactions = new ArrayList<>();

            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }

            List<Product> list = productDao.findAll();

            if (list == null) {
                return null;
            }

            Product product = list.stream().filter(x -> "core".equalsIgnoreCase(x.getName()) || "flutterwave core".equalsIgnoreCase(x.getName())).findFirst().orElse(null);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            if ("any".equalsIgnoreCase(provider)) {

                List<Provider> providers = providerDao.findAll();

                for (Provider p : providers) {

                    if(merchant != null && !"".equalsIgnoreCase(merchant)){
                        
                        CoreMerchant coreMerchant = coreMerchantDao.find(Long.parseLong(merchant));
                        
                        if(coreMerchant != null)
                            merchant = coreMerchant.getMerchantID();
                        
                    }else
                        merchant = null;
                    
                    List<Transaction> transaction;
                    
                    if("ACCOUNT".equalsIgnoreCase(category)){
                        transaction = transactionDao.getUnsettledTransanctionAccount(startDate, endDate,
                            p, currency, merchant);
                    }else if("EBILLS".equalsIgnoreCase(category)){
                        transaction = transactionDao.getUnsettledTransanctionAccount(startDate, endDate,
                            p, currency, merchant);
                    }
                    else    
                        transaction = transactionDao.getUnsettledTransanctions(startDate, endDate,
                            p, currency, merchant);

                    Map<String, Map<String, List<Transaction>>> dataMapping = transaction.stream().collect(Collectors
                            .groupingBy(Transaction::getMerchantId, Collectors.groupingBy(Transaction::getCardCountry)));

                    ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);
                    
                    for (Map.Entry<String, Map<String, List<Transaction>>> entry : dataMapping.entrySet()) {

                        int volume = 0;
                        
                        String mid = entry.getKey();
                        Map<String, List<Transaction>> values = entry.getValue();

                        List<CoreMerchant> coreMer = coreMerchantDao.find("merchantID", mid);

                        CoreMerchant coreMerchant = null;

                        if(coreMer != null){
                            coreMerchant = coreMer.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
                        }

                        double fee = 0.0, cost = 0.0, value = 0.0;

                        MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(product, coreMerchant == null ? 0 : coreMerchant.getId());

                        Currency providerCurrency = p.getCurrency();

                        String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                        Country currencyCountry = null;

                        if (providerCurrency != null) {
                            currencyCountry = countryDao.findByCurrency(providerCurrency);
                        }

                        String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                        String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                        for (Map.Entry<String, List<Transaction>> val : values.entrySet()) {

                            String cardCountry = val.getKey();

                            Map<String, List<Transaction>> currencyValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency));

//                            Map<String, Long> countValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency, Collectors.counting()));
//                            boolean local = false;
                            
                            for (String currencyS : currencyValues.keySet()) {

//                                long count = countValues.getOrDefault(currencyS, 1L);
                                boolean local = currencyS.equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));
                                
                                List<Transaction> txns = currencyValues.get(currencyS);
                                
                                for(Transaction t : txns){
                                    
                                    cost += getCost(p, t.getAmount(), local, 1);
                                    
                                    double configFee = 0.0;
                                    double merchantFe = 0.0;
                                    
                                    if("ACCOUNT".equalsIgnoreCase(category) || "EBILLS".equalsIgnoreCase(category)){
                                        configFee = getAccountFee(configurationFee, t.getAmount(), local, currencyS);
                                        merchantFe = getAccountFee(merchantFee, t.getAmount(), local, currencyS);
                                    }else{
                                        configFee = getFee(configurationFee, t.getAmount(), local, currencyS, 1);
                                        merchantFe = getFee(merchantFee, t.getAmount(), local, currencyS, 1);
                                    }
                                    
                                    value += t.getAmount();
                                    
                                    volume += 1;
                                    
                                    fee += merchantFe == 0 ? configFee : merchantFe;
                                }
                                
                            }
//                        value = val.getValue().stream().mapToDouble(x -> x.getAmount()).sum();

                            //fee = getFee(configurationFee, value, cardCountry.equalsIgnoreCase(merchantCountry));  
                        }

                        SettlementViewModel settlementViewModel = new SettlementViewModel();
                        settlementViewModel.setAmount(value);
                        settlementViewModel.setAmountToSettled(value - fee);
                        settlementViewModel.setMerchantId(mid);
                        settlementViewModel.setFee(fee);
                        settlementViewModel.setMerchantName(coreMerchant != null ? coreMerchant.getCompanyname() : "");
                        settlementViewModel.setId(p.getId());
                        settlementViewModel.setCost(cost);
                        settlementViewModel.setProvider(p.getName());
                        settlementViewModel.setVolume(volume);
                        
//                        Map<String, Object> data = new HashMap<>();
//                        data.put("name", p.getName());
//                        data.put("merchantId", mid);
//                        data.put("merchantName", coreMerchant != null ? coreMerchant.getCompanyname() : "");
//                        data.put("amount", totalValue.doubleValue());
//                        data.put("amountToSettled", (totalValue.doubleValue() - totalFee.doubleValue()));
//                        data.put("id", p.getId());
//                        data.put("fee", totalFee.doubleValue());
//                        data.put("cost", totalCost.doubleValue());

                        transactions.add(settlementViewModel);
                    }
                    
                    if("merchant".equalsIgnoreCase(grouping)){
                
                        Map<String, List<SettlementViewModel>> groupedTransactions = transactions.stream().collect(Collectors.groupingBy(SettlementViewModel::getMerchantId));

                        transactions = new ArrayList<>();

                        for(Map.Entry<String, List<SettlementViewModel>> m : groupedTransactions.entrySet()){

                            String merchantName = "";

                            double dCost = 0, dFee = 0, dValue = 0, dSettlementAmount = 0;
                            
                            int dVolume = 0;

                            for(SettlementViewModel model : m.getValue()){

                                merchantName = model.getMerchantName();

                                dCost += model.getCost();
                                dValue += model.getAmount();
                                dSettlementAmount += model.getAmountToSettled();
                                dFee += model.getFee();
                                dVolume += model.getVolume();
                            }   

                            SettlementViewModel viewModel = new SettlementViewModel();
                            viewModel.setAmount(dValue);
                            viewModel.setAmountToSettled(dSettlementAmount);
                            viewModel.setId(0);
                            viewModel.setProvider("All");
                            viewModel.setMerchantName(merchantName);
                            viewModel.setFee(dFee);
                            viewModel.setCost(dCost);
                            viewModel.setMerchantId(m.getKey());
                            viewModel.setVolume(dVolume);

                            transactions.add(viewModel);
                        }

                    }

                }
            } else {

                Provider p = providerDao.findByKey("name", provider);

                 if(merchant != null && !"".equalsIgnoreCase(merchant)){
                        
                    CoreMerchant coreMerchant = coreMerchantDao.find(Long.parseLong(merchant));

                    if(coreMerchant != null)
                        merchant = coreMerchant.getMerchantID();

                }else
                    merchant = null;
                 
                List<Transaction> transaction;
                         
                if("ACCOUNT".equalsIgnoreCase(category)){
                    transaction = transactionDao.getUnsettledTransanctionAccount(startDate, endDate,
                        p, currency, merchant);
                }else
                    transaction = transactionDao.getUnsettledTransanctions(startDate, endDate,
                        p, currency, merchant);
//                List<Transaction> transaction = transactionDao.getUnsettledTransanctions(startDate, endDate,
//                        p, currency, merchant);

                //Map<String, Double> dataMerchant = transaction.stream().collect(Collectors.groupingBy(Transaction::getMerchantId, Collectors.summingDouble(Transaction::getAmount)));
                // This is called ot group by merchant id and then by country
                Map<String, Map<String, List<Transaction>>> dataMapping = transaction.stream().collect(Collectors
                        .groupingBy(Transaction::getMerchantId, Collectors.groupingBy(Transaction::getCardCountry)));

                ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);

                for (Map.Entry<String, Map<String, List<Transaction>>> entry : dataMapping.entrySet()) {

                    String mid = entry.getKey();
                    Map<String, List<Transaction>> values = entry.getValue();

                    List<CoreMerchant> coreMer = coreMerchantDao.find("merchantID", mid);

                    CoreMerchant coreMerchant = null;
                    
                    if(coreMer != null){
                        coreMerchant = coreMer.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
                    }
                    
//                    JsonObject jsonObject = httpUtil.getMerchantInformation(mid);
                    double fee = 0.0, cost = 0.0, value = 0.0;
                    
                    int volume =0 ;
//                    totalFee = new BigDecimal(0.0);
//                    totalCost = totalValue = totalFee;

                    MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(product, coreMerchant == null ? 0 : coreMerchant.getId());

                    Currency providerCurrency = p.getCurrency();

                    String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                    Country currencyCountry = null;

                    if (providerCurrency != null) {
                        currencyCountry = countryDao.findByCurrency(providerCurrency);
                    }

                    String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                    String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                    for (Map.Entry<String, List<Transaction>> val : values.entrySet()) {

                            String cardCountry = val.getKey();

                            Map<String, List<Transaction>> currencyValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency));

//                            Map<String, Long> countValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency, Collectors.counting()));
//                            boolean local = false;
                            
                            for (String currencyS : currencyValues.keySet()) {

//                                long count = countValues.getOrDefault(currencyS, 1L);
                                boolean local = currencyS.equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));
                                
                                List<Transaction> txns = currencyValues.get(currencyS);
                                
                                for(Transaction t : txns){
                                    
                                    cost += getCost(p, t.getAmount(), local, 1);
                                    
                                    double merchantFe, configFee;
                                    
                                    if("ACCOUNT".equalsIgnoreCase(category)){
                                        configFee = getAccountFee(configurationFee, t.getAmount(), local, currencyS);
                                        merchantFe = getAccountFee(merchantFee, t.getAmount(), local, currencyS);
                                    }else{
                                        configFee = getFee(configurationFee, t.getAmount(), local, currencyS, 1);
                                        merchantFe = getFee(merchantFee, t.getAmount(), local, currencyS, 1);
                                    }
                                    
//                                    double configFee = getFee(configurationFee, t.getAmount(), local, currencyS, 1);
//                                    double merchantFe = getFee(merchantFee, t.getAmount(), local, currencyS, 1);
                                    
                                    value += t.getAmount();
                                    
                                    fee += merchantFe == 0 ? configFee : merchantFe;
                                    
                                    volume += 1;
                                }
                                
                            }
//                        value = val.getValue().stream().mapToDouble(x -> x.getAmount()).sum();

                            //fee = getFee(configurationFee, value, cardCountry.equalsIgnoreCase(merchantCountry));  
                        }
//                        value = val.getValue().stream().mapToDouble(x -> x.getAmount()).sum();

                        //fee = getFee(configurationFee, value, cardCountry.equalsIgnoreCase(merchantCountry));  
                    

//                    Map<String, Object> data = new HashMap<>();
//                    data.put("name", p.getName());
//                    data.put("merchantId", mid);
//                    data.put("merchantName", coreMerchant != null ? coreMerchant.getCompanyname() : "");
//                    data.put("amount", totalValue.doubleValue());
//                    data.put("amountToSettled", (totalValue.doubleValue() - totalFee.doubleValue()));
//                    data.put("id", p.getId());
//                    data.put("fee", totalFee.doubleValue());
//                    data.put("cost", totalCost.doubleValue());

                    SettlementViewModel settlementViewModel = new SettlementViewModel();
                    settlementViewModel.setAmount(value);
                    settlementViewModel.setAmountToSettled(value - fee);
                    settlementViewModel.setMerchantId(mid);
                    settlementViewModel.setFee(fee);
                    settlementViewModel.setMerchantName(coreMerchant != null ? coreMerchant.getCompanyname() : "");
                    settlementViewModel.setId(p.getId());
                    settlementViewModel.setCost(cost);
                    settlementViewModel.setProvider(p.getName());
                    settlementViewModel.setVolume(volume);
                    
//                    settlementViewModel.setRefund(value);

                    transactions.add(settlementViewModel);

                }

            }
            
            global.setSettlementReport(transactions);

            PageResult pageResult = new PageResult(transactions, Long.parseLong(transactions.size() + ""), Long.parseLong(transactions.size() + ""));

            return Response.status(Response.Status.OK).entity(pageResult).build();
        } catch (Exception ex) {
            Logger.getLogger(ReportApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).entity(new PageResult()).build();
    }

    @Path("/provider/download")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response downloadSettlementData(@QueryParam(value = "provider") String provider, @QueryParam(value = "category") String category, 
            @QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency,
            @QueryParam(value = "settled") String settled, @QueryParam(value = "merchant") String merchant, @QueryParam(value = "grouping") String grouping) {

        try {

            if (currency == null || "".equals(currency)) {
                currency = "NGN";
            }

            List<SettlementViewModel> transactions = new ArrayList<>();

            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }
            }

            global.setDownloadCheck(settlementService.processSettlementTransaction(provider, merchant, currency, category, startDate, endDate, grouping));

        }finally{
        }

        
        Map<String, String> response = new HashMap<>();
        response.put("status", "success");
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
   
    
    @Path("/provider/download/check")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response checkDownloadStatus(){
        
        Map<String, String> response = new HashMap<>();
        
        if(global.getDownloadCheck() == null){
            
            response.put("status", "failed");
            response.put("message", "No file available for download");
            
            return Response.status(Response.Status.OK).entity(response).build();
        }
        
        if(global.getDownloadCheck().isDone()){
            
            try {
                if("Done".equalsIgnoreCase(global.getDownloadCheck().get())){
                    
                    response.put("status", "success");
                    response.put("message", "File is ready for download");
                    
//                return Response.status(Response.Status.OK).entity(response).build();

                }else{
                       
                    response.put("status", "success");
                    response.put("message", "File is ready for download");
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SettlementApiController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(SettlementApiController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            global.setDownloadCheck(null);
            

            
        }else{
            
            response.put("status", "pending");
            response.put("message", "preparing file for download");
        }
        
        
        return Response.status(Response.Status.OK).entity(response).build(); 
    }
    
    @Path(value = "/create")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response createSettlement(@Valid UpdateListModel listModel) {

        Map<String, String> response = new HashMap<>();
        try {

            String[] ids = listModel.getId();
            String range = listModel.getRange();
            String currency = listModel.getCurrency();
            Date startDate = null, endDate = null;
            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }

            List<Product> listProduct = productDao.findAll();

            Product product = listProduct.stream().filter(x -> "core".equalsIgnoreCase(x.getName()) || "flutterwave core".equalsIgnoreCase(x.getName())).findFirst().orElse(null);

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            List<SettlementM> list = new ArrayList<>();
            for (String id : ids) {

                String[] splittedIds = id.split(",");

                String merchantId = splittedIds[0];

                Provider prov = providerDao.find(Long.parseLong(splittedIds[1]));

                List<Transaction> transaction = transactionDao.getUnsettledTransanctions(startDate, endDate,
                        prov, currency, merchantId);

//                Double value = transaction.stream().mapToDouble(x -> x.getAmount()).sum();
                ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);

                Map<String, Map<String, List<Transaction>>> dataMapping = transaction.stream()
                        .collect(Collectors.groupingBy(Transaction::getMerchantId, Collectors.groupingBy(Transaction::getCardCountry)));

                for (Map.Entry<String, Map<String, List<Transaction>>> entry : dataMapping.entrySet()) {

                    String mid = entry.getKey();
                    Map<String, List<Transaction>> values = entry.getValue();

                    CoreMerchant coreMerchant = coreMerchantDao.findByKey("merchantID", mid);

//                    JsonObject jsonObject = httpUtil.getMerchantInformation(mid);

//                    String merchantCountry = "";
//
//                    String merchantAccount = null;
//
//                    if (jsonObject != null) {
//                        MerchantCompliance compliance = merchantCompliantDao.findByKey("registeredNumber", jsonObject.getString("merchant_registration_number", null));
//                        merchantCountry = compliance == null ? "" : compliance.getCountry() == null ? "" : compliance.getCountry();
//                    }
                    MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(product, coreMerchant == null ? 0 : coreMerchant.getId());

                    Currency providerCurrency = prov.getCurrency();

                    String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                    Country currencyCountry = null;

                    if (providerCurrency != null) {
                        currencyCountry = countryDao.findByCurrency(providerCurrency);
                    }

                    String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                    String countryName = currencyCountry != null ? currencyCountry.getName() : null;

//                    totalFee = new BigDecimal(0.0);
//                    totalCost = totalValue = totalFee;
                    double fee = 0.0, cost = 0.0, value = 0.0;

                    for (Map.Entry<String, List<Transaction>> val : values.entrySet()) {

                        String cardCountry = val.getKey();

                        Map<String, List<Transaction>> currencyValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency));

//                            Map<String, Long> countValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency, Collectors.counting()));
//                            boolean local = false;

                        for (String currencyS : currencyValues.keySet()) {

//                                long count = countValues.getOrDefault(currencyS, 1L);
                            boolean local = currencyS.equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));

                            List<Transaction> txns = currencyValues.get(currencyS);

                            for(Transaction t : txns){

                                cost += getCost(prov, t.getAmount(), local, 1);

                                double configFee = getFee(configurationFee, t.getAmount(), local, currencyS, 1);

                                double merchantFe = getFee(merchantFee, t.getAmount(), local, currencyS, 1);

                                value += t.getAmount();

                                fee += merchantFe == 0 ? configFee : merchantFe;
                            }

                        }
//                        value = val.getValue().stream().mapToDouble(x -> x.getAmount()).sum();

                        //fee = getFee(configurationFee, value, cardCountry.equalsIgnoreCase(merchantCountry));  
                    }

                    SettlementM settlementM = new SettlementM();
                    settlementM.setAmount(value);
                    settlementM.setCreatedOn(new Date());
                    settlementM.setCreatedBy(user);
                    settlementM.setCurrency(currency);
                    settlementM.setEndDate(endDate);
                    settlementM.setProvider(prov);
                    settlementM.setStatus(Status.PENDING);
                    settlementM.setStartDate(startDate);
                    settlementM.setFee(fee);
                    settlementM.setCost(cost);
                    settlementM.setMerchantId(mid);
                    settlementM.setProduct(product);
//                    settlementM.setCountry(card`);

                    list.add(settlementM);
                }

                // This is called to mark those transactions pending
                transaction.stream().forEach(x -> x.setSettlementState(Utility.SettlementState.PENDING));
                transactionDao.update(transaction);
            }

            settlementMDao.create(list);

            response.put("status-code", "00");
            response.put("status", "succesful");

            return Response.status(Response.Status.OK).entity(response).build();

        } catch (DatabaseException ex) {
            Logger.getLogger(SettlementApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status-code", "RR");
            response.put("status", "Generic Error");
        }

        return Response.status(Response.Status.OK).entity(response).build();

    }
    
    
    @Path(value = "/rave/mcash")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getRaveOfflineSettlement(@QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency){
        
        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        List result = raveTransactionDao.getMCashSummaryResult(startDate, endDate, "".equalsIgnoreCase(currency) ? null : currency);
        
        List<RaveSettlementViewModel> models = new ArrayList<>();
        
        for(Object obj : result){
            Object[] objects = (Object[])obj;
            
//            sum(amount), count(*), Accounts.business_name, Accounts.id, sum(appfee), settlement_account.account_number, settlement_account.bankCode
            Double sum = (Double) objects[0];
            BigInteger count = (BigInteger) objects[1];            
            String merchantName = (String) objects[2];
            String merchantId = ((String)objects[3])+"";
            Double fee = (Double) objects[4];
            String accountNo = (String) objects[5];
            String bank = (String) objects[6];
            
            RaveSettlementViewModel model = new RaveSettlementViewModel();
            model.setMerchantName(merchantName);
            model.setAmount(sum);
            model.setVolume(count.intValue());
            model.setFee(fee == null ? 0 : fee);
            model.setBankCode(bank);
            model.setAccountNo(accountNo);
            model.setMerchantId(merchantId);
            
            models.add(model);
        }
        
        global.setRaveSettlementReport(models);
        
        PageResult<RaveSettlementViewModel> pageResult = new PageResult<>(models, Long.valueOf(models.size()+""), Long.valueOf(models.size()+""));
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path(value = "/rave/transaction")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getRaveTransactionSettlement(@QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency,
             @QueryParam(value = "paymenttype") String paymentType){
        
        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        if(Utility.emptyToNull(currency) == null){
            currency = "NGN";
        }
        
        List result = raveTransactionWHDao.getSettlement(startDate, endDate, currency, paymentType);
        
        List<RaveSettlementModel> models = new ArrayList<>();
        
        for(Object obj : result){
            Object[] objects = (Object[])obj;
            
//            acctbusinessname, paymenttype, sum(chargedAmount), sum(appFee), sum(merchantFee),  count(*), currency, acctparentbusinessname, " +
//"settlement_account.account_number, settlement_account.bankCode, preference_value

            String merchantName = (String) objects[0];
            
            if(merchantName == null)
                continue;
                
            Double sum = (Double) objects[2];
            String type = (String) objects[1];
            Double appFee = (Double) objects[3];
            Double merchantFee = (Double) objects[4];
            BigInteger count = (BigInteger) objects[5];            
            String currencyName = ((String)objects[6])+"";
            String parentName = (String) objects[7];
            String accountNo = (String) objects[8];
            String bank = (String) objects[9];
            String settlementClass = (String) objects[10];
            
            RaveSettlementModel model = new RaveSettlementModel();
            model.setBusinessName(merchantName);
            model.setAmount(sum);
            model.setVolume(count.intValue());
            model.setAppFee(appFee);
            model.setMerchantFee(merchantFee);
            model.setBankCode(bank);
            model.setAccountNumber(accountNo);
            model.setCurrency(currencyName);
            model.setParent(parentName);
            model.setParentSettlementType(settlementClass);
            model.setType(type);
            
            models.add(model);
        }
        
        global.setRaveTransactionSettlement(models);
        
        PageResult<RaveSettlementModel> pageResult = new PageResult<>(models, Long.valueOf(models.size()+""), Long.valueOf(models.size()+""));
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path(value = "/pos")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response posSettlementReport(@QueryParam(value = "range") String range,
            @QueryParam(value = "merchantid") String merchantId,
            @QueryParam(value = "merchantcode") String merchantcode){
     
        Date startDate = null, endDate = null;

        if (range != null && !"".equalsIgnoreCase(range)) {

            try {
//                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                startDate = dateFormat.parse(range);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                startDate = calendar.getTime();

                endDate = dateFormat.parse(range);

                calendar.setTime(endDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));

                endDate = calendar.getTime();

            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        Page<PosTransaction> page = posTransactionDao.find(0, 0, Utility.emptyToNull(merchantId), Utility.emptyToNull(merchantcode), 
                    null, startDate, endDate, "00", "NGN", 
                    "PURCHASE", null, null, null);
        
        List<PosTransaction> list = page.getContent();
        
        List<PosSettlementModel> models;
        
        if(list == null)
            models = new ArrayList<>();
        else{
            
            models = list.stream().map((PosTransaction transaction) -> {
                 
                double fee = 0.0;
                
                PosSettlementModel model = new PosSettlementModel();
                model.setAmount(transaction.getAmount());
                model.setCurrency(transaction.getCurrency());
                
                model.setName(transaction.getMerchantName());
                model.setTerminalId(transaction.getTerminalId());
                model.setTransactionDate(transaction.getRequestDate());
                model.setType(transaction.getType());
                
                if(transaction.getPosId() != null){
                   fee = terminalService.getFee(transaction.getPosId(),null, transaction.getAmount()); 
                   
                    PosMerchant merchant = terminalService.getMerchantName(transaction.getPosId(), null);
                   
                    if(merchant != null)
                        model.setName(merchant.getName());
                  
                           
                }else{
                    
                    PosTerminal terminal = terminalService.getTerminal(transaction.getTerminalId());
                    
                    if(terminal != null){
                        
                        if(terminal.getMerchant() != null){
                            fee = terminalService.getFee(transaction.getPosId(),terminal.getMerchant().getMerchantCode(), transaction.getAmount()); 
                            model.setName(terminal.getMerchant().getName());
                        }
                    }
                    
                }
                
                model.setFee(fee);
                
                return model;
            }).collect(Collectors.<PosSettlementModel>toList());
        }
            
        global.setPosSettlementReport(models);
         
        PageResult<PosSettlementModel> pageResult = new PageResult<>(models, Long.valueOf(models.size()+""), Long.valueOf(models.size()+""));
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
        
    }
    
    @Path("/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorize(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            SettlementM settlementM = settlementMDao.find(id);

            if (settlementM == null) {
                response.put("status-code", "03");
                response.put("status", "settlement charge record not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Merchant Charge back Authorization with id " + settlementM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            Map<String, String> settlementState = new HashMap<>();

            if (status == true) {
                if (settlementM.getModelId() <= 0) {

//                Settlement settlement = settlementDao.findByQuery(.());
//
//                if (settlement != null) {
//
//                    response.put("status-code", "02");
//                    response.put("status", "Duplicate charge back for transaction " + settlementM.getFlwReference());
//                    return response;
//                }
                    Settlement settlement = new Settlement();

                    settlement.setCreatedBy(settlementM.getCreatedBy());
                    settlement.setCreatedOn(settlementM.getCreatedOn());
                    settlement.setCurrency(settlementM.getCurrency());
                    settlement.setAmount(settlementM.getAmount());
                    settlement.setApprovedBy(user);
                    settlement.setApprovedOn(new Date());
                    settlement.setStartDate(settlementM.getStartDate());
                    settlement.setEndDate(settlementM.getEndDate());
                    settlement.setProvider(settlementM.getProvider());
                    settlement.setStatus(Status.PENDING);
                    settlement.setFee(settlementM.getFee());
                    settlement.setCost(settlementM.getCost());
                    settlement.setProduct(settlementM.getProduct());
                    settlement.setMerchantId(settlementM.getMerchantId());
                    settlement.setProvider(settlementM.getProvider());
                    settlement.setCountry(settlementM.getCountry());

                    FlutterwaveSettlement flutterwaveSettlement = new FlutterwaveSettlement();
                    flutterwaveSettlement.setAmount(settlement.getAmount() - settlement.getFee()); // This is the total amount less fee

                    JsonObject jsonObject = httpUtil.getMerchantInformation(settlementM.getMerchantId());

                    String accountNo = null, bankCode = null;

                    if (jsonObject != null) {
                        accountNo = jsonObject.getString("merchant_account_number", null);
                        bankCode = jsonObject.getString("bank_code", null);
                    }

                    flutterwaveSettlement.setCreatedOn(new Date());
                    flutterwaveSettlement.setCreditAccountNo(accountNo == null ? "0690000003" : accountNo);
                    // This has to change for live
                    flutterwaveSettlement.setCreditBankCode(bankCode == null ? "044" : bankCode);
                    flutterwaveSettlement.setCreditCountry(settlement.getCountry() == null ? "NG" : settlement.getCountry());
                    flutterwaveSettlement.setCreditCurrency(settlement.getCurrency());
                    flutterwaveSettlement.setDebitAccountNo("0690000002");
                    flutterwaveSettlement.setDebitBankCode("044");
                    flutterwaveSettlement.setDebitCountry("NG");
                    flutterwaveSettlement.setDebitCurrency("NGN");
                    flutterwaveSettlement.setTransactionReference("FLWMS" + (new Date()).getTime());
                    flutterwaveSettlement.setRuleState(Utility.SettlementState.PENDING);
                    flutterwaveSettlement.setTransactionState(Utility.SettlementState.PENDING);

                    flutterwaveSettlement = flutterwaveSettlementDao.create(flutterwaveSettlement);

                    CreateRuleModel createRuleModel = new CreateRuleModel();
                    createRuleModel.setCreditAccountNo(flutterwaveSettlement.getCreditAccountNo());
                    createRuleModel.setCreditBankCode(flutterwaveSettlement.getCreditBankCode());
                    createRuleModel.setCreditCountry(flutterwaveSettlement.getCreditCountry());
                    createRuleModel.setCreditCurrency(flutterwaveSettlement.getCreditCurrency());
                    createRuleModel.setDebitAccountNo(flutterwaveSettlement.getDebitAccountNo());
                    createRuleModel.setDebitBankCode(flutterwaveSettlement.getDebitBankCode());
                    createRuleModel.setDebitCountry(flutterwaveSettlement.getDebitCountry());
                    createRuleModel.setDebitCurrency(flutterwaveSettlement.getDebitCurrency());

                    settlement.setActualAmount(0);
                    settlement = settlementDao.create(settlement);

                    String token = httpUtil.createRule(createRuleModel);

                    if (token == null) {
                        settlementState.put(settlementM.getMerchantName(), "unable to settle transactions");
                    }

                    flutterwaveSettlement.setSettlementToken(token);
                    flutterwaveSettlement.setRuleCreatedOn(new Date());
                    flutterwaveSettlement.setSettlementToken(token);
                    flutterwaveSettlement.setRuleStatus(true);
                    flutterwaveSettlement.setSettlement(settlement);

                    flutterwaveSettlementDao.update(flutterwaveSettlement);

                    // This is called to get rule status
                    Map<String, String> res = httpUtil.getRuleStatus(token);

                    String responseCode = res.getOrDefault("responsecode", "99");
//                    String reference = res.getOrDefault("transactionreference", null);

                    if ("00".equalsIgnoreCase(responseCode)) {
                        //settlementState.put(merchant, "waiting for approval");

                        flutterwaveSettlement.setRuleState(Utility.SettlementState.APPROVED);

                        CreateTransactionModel model = new CreateTransactionModel();
                        model.setTransactionReference(flutterwaveSettlement.getTransactionReference());
                        model.setSettlementToken(token);
                        model.setNarration("Settlement");
                        model.setAmount(flutterwaveSettlement.getAmount() + "");
                        model.setCurrency(settlement.getCurrency());

                        res = httpUtil.createTransaction(model);

                        responseCode = res.getOrDefault("responsecode", "99");
                        String reference = res.getOrDefault("reference", null);

                        flutterwaveSettlement.setInternalReference(reference);

                        if ("00".equalsIgnoreCase(responseCode)) {

                            flutterwaveSettlement.setTransactionState(Utility.SettlementState.APPROVED);
//                        flutterwaveSettlement.setInternalReference(reference);
//                        flutterwaveSettlement.setTra(Utility.SettlementState.APPROVED);
//                        settlementState.put(merchant, "settled");

                            List<Transaction> transactionMerchant = transactionDao.getUnsettledPendingTransanctions(settlement.getStartDate(), settlement.getEndDate(),
                                    settlement.getProvider(), settlement.getCurrency(), settlement.getMerchantId());

                            for (Transaction t : transactionMerchant) {
                                t.setSettled(true);
                                t.setSettledBy(user);
                                t.setSettledOn(new Date());
                                t.setSettlement(settlement);
                                t.setSettlementState(Utility.SettlementState.APPROVED);
                                transactionDao.update(t);
                            }

                            Currency currency = currencyDao.findByKey("shortName", settlement.getCurrency());

                            walletDao.findByQuery(null, settlement.getProvider().getShortName(), Utility.WalletCategory.SOURCE, currency);
                            // Fund provider 
                            walletService.fundWallet(settlement.getProvider(), settlement.getCost(), currency, null);

                            // Fund merchant
                            String merchantId = settlement.getMerchantId();

                            walletService.fundWallet(settlement.getProduct(), merchantId, settlement.getCost(), currency, null);

                        } else if ("R00".equalsIgnoreCase(responseCode) || "R404".equalsIgnoreCase(responseCode)) {

                            flutterwaveSettlement.setTransactionState(Utility.SettlementState.PENDING);

                            List<Transaction> transactionMerchant = transactionDao.getUnsettledTransanctions(settlement.getStartDate(), settlement.getEndDate(),
                                    settlement.getProvider(), settlement.getCurrency(), settlement.getMerchantId());

                            for (Transaction t : transactionMerchant) {
                                t.setSettled(true);
                                t.setSettledBy(user);
                                t.setSettledOn(new Date());
                                t.setSettlement(settlement);
                                transactionDao.update(t);
                            }
//                        flutterwaveSettlement.setTra(Utility.SettlementState.APPROVED);
                        }

                        flutterwaveSettlement.setTransactionStatus(true);
                        flutterwaveSettlement.setTransactionReference(res.getOrDefault("reference", null));

                        flutterwaveSettlementDao.update(flutterwaveSettlement);

                        settlementDao.update(settlement);
                    } else {

                        response.put("status-code", "00");
                        response.put("status", "Success");

                        log = new Log();
                        log.setAction("Settlement for id " + settlementM.getId() + " with merchant " + settlementM.getMerchantId());
                        log.setCreatedOn(new Date());
                        if (status == true) {
                            log.setDescription("Authorization Completed Successfully");
                        } else {
                            log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
                        }
                        log.setLevel(LogLevel.Info);
                        log.setStatus(LogStatus.SUCCESSFUL);
                        log.setLogState(LogState.FINISH);
                        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                        logService.log(log);
                    }

//                if (status == true) {
//                }
                } else {

//                Settlement settlement = settlementDao.findByKey("flwReference",settlementM.getFlwReference());
//
//                if (settlement != null && settlement.getId() != settlementM.getModelId()) {
//                    response.put("status-code", "02");
//                    response.put("status", "Duplicate settlement with for settlement with reference " + settlement.getFlwReference());
//                    return response;
//                }
//
//                if (status == true) {
//
//                    
//                    // end of setting pob settlement
//
//                    settlementDao.update(settlement);
//
//                }
                }
            } else {

                List<Transaction> transactionMerchant = transactionDao.getUnsettledTransanctions(settlementM.getStartDate(), settlementM.getEndDate(),
                        settlementM.getProvider(), settlementM.getCurrency(), settlementM.getMerchantId());

                for (Transaction t : transactionMerchant) {
                    t.setSettlementState(Utility.SettlementState.NONE);
                    transactionDao.update(t);
                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            settlementM.setApprovedOn(new Date());
            settlementM.setApprovedBy(user);
            settlementM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                settlementM.setStatus(Status.APPROVED);
            } else {
                settlementM.setStatus(Status.REJECTED);
            }

            settlementMDao.updateE(settlementM);

            log = new Log();
            log.setAction("Merchant Authorization for " + settlementM.getId());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("settlementupdated", true);
            session.setAttribute("settlementupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Settlement Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Settlement Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path("/merchant/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeMerchantConfig(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            MerchantConfigM merchantConfigM = merchantConfigMDao.find(id);

            if (merchantConfigM == null) {
                response.put("status-code", "03");
                response.put("status", "merchantConfig merchantConfig record not found");
                return response;
            }

            Log log = new Log();
            log.setAction("MerchantConfig Fee Authorization with id " + merchantConfigM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (merchantConfigM.getModelId() <= 0 && status == true) {

                List<MerchantConfig> merchantConfigList = merchantConfigDao.findByMerchant(merchantConfigM.getCoreMerchantId(), merchantConfigM.getCurrency());

                if (merchantConfigList != null && !merchantConfigList.isEmpty()) {

                    response.put("status-code", "02");
                    response.put("status", "Duplicate merchantConfig for merchantConfig " + merchantConfigM.getCoreMerchantName());
                    return response;
                }

                MerchantConfig merchantConfig = new MerchantConfig();

                merchantConfig.setCreatedBy(merchantConfigM.getCreatedBy());
                merchantConfig.setCreatedOn(merchantConfigM.getCreatedOn());
                merchantConfig.setCoreMerchantId(merchantConfigM.getCoreMerchantId());
                merchantConfig.setMoneywaveMerchant(merchantConfig.getMoneywaveMerchant());
                merchantConfig.setApprovedOn(new Date());
                merchantConfig.setApprovedBy(user);
                merchantConfig.setCurrency(merchantConfigM.getCurrency());
                merchantConfig.setSettlementCycle(merchantConfigM.getSettlementCycle());
                merchantConfig.setSettlementType(merchantConfigM.getSettlementType());
                merchantConfig.setCoreMerchantName(merchantConfigM.getCoreMerchantName());

                // end of setting intertional merchantConfig
//                if (status == true) {
                merchantConfigDao.create(merchantConfig);
//                }

            } else {

                List<MerchantConfig> merchantConfigList = merchantConfigDao.findByMerchant(merchantConfigM.getCoreMerchantId(), merchantConfigM.getCurrency());

                if (merchantConfigList != null && !merchantConfigList.isEmpty()) {
                    response.put("status-code", "02");
                    response.put("status", "Duplicate merchantConfig with for merchantConfig with name ");
                    return response;
                }

//                MerchantConfig merchantConfig = merchantConfigList.get(0);
                if (status == true) {

                    MerchantConfig merchantConfig = merchantConfigDao.find(merchantConfigM.getModelId());

//                    merchantConfig.setModifiedBy(merchantConfigM.getCreatedBy());
                    merchantConfig.setCoreMerchantId(merchantConfigM.getCoreMerchantId());
                    merchantConfig.setMoneywaveMerchant(merchantConfig.getMoneywaveMerchant());
                    merchantConfig.setApprovedOn(new Date());
                    merchantConfig.setApprovedBy(user);
                    merchantConfig.setCurrency(merchantConfigM.getCurrency());
                    merchantConfig.setSettlementCycle(merchantConfigM.getSettlementCycle());
                    merchantConfig.setSettlementType(merchantConfigM.getSettlementType());
                    merchantConfig.setCoreMerchantName(merchantConfigM.getCoreMerchantName());

                    merchantConfigDao.update(merchantConfig);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            merchantConfigM.setApprovedOn(new Date());
            merchantConfigM.setApprovedBy(user);
            merchantConfigM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                merchantConfigM.setStatus(Status.APPROVED);
            } else {
                merchantConfigM.setStatus(Status.REJECTED);
            }

            merchantConfigMDao.updateE(merchantConfigM);

            log = new Log();
            log.setAction("MerchantConfig Authorization for " + merchantConfigM.getId());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("configupdated", true);
            session.setAttribute("configupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Fee Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Fee Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path(value = "/find/unauth")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getProviderById(@QueryParam(value = "id") long id) {

        try {
            if (id <= 0) {
                return null;
            }

            SettlementM provider = settlementMDao.find(id);

            Map<String, Object> response = new HashMap<>();

            if (provider == null) {
                response.put("responsecode", "01");
                return Response.status(Response.Status.OK).entity(response).build();
            }

            response.put("responsecode", "00");
            response.put("data", provider);

            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Path(value = "/summary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getSettlementSummary(@QueryParam(value = "range") String range,
            @QueryParam(value = "merchantId") long merchantId,
            @QueryParam(value = "productmid") long productmid,
            @QueryParam(value = "product") String product,
            @QueryParam(value = "currency") String currency) {

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

//        settlementDao.findByQuery(startDate, endDate, provider);
        return null;
    }

    private double getCost(Provider p, double value, boolean local, long count) {

        double cost = 0.0;
        if (local == true) {

            if (null == p.getLocalCardFeeType()) {
                cost = p.getLocalCardFee() * count;
            } else {
                switch (p.getLocalCardFeeType()) {
                    case BOTH:
                        cost = p.getLocalCardFee() * count + (p.getLocalCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        cost = (p.getLocalCardFee() * value) / 100;
                        break;
                    default:
                        cost = p.getLocalCardFee();
                        break;
                }
            }

            double configFee = p.getLocalCardFeeCap() * count;

            if (cost > configFee && p.getLocalCardFeeCap() != 0.0) {
                cost = configFee;
            }

        } else {

            if (null == p.getInterCardFeeType()) {
                cost = p.getInterCardFee() * count;
            } else {
                switch (p.getInterCardFeeType()) {
                    case BOTH:
                        cost = p.getInterCardFee() * count + (p.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        cost = (p.getInterCardFee() * value) / 100;
                        break;
                    default:
                        cost = p.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = p.getInterCardFeeCap() * count;

            if (cost > configFee && p.getInterCardFeeCap() != 0.0) {
                cost = configFee;
            }
        }

        return cost;
    }
    
    private double getAccountFee(ConfigurationFee configurationFee, double value, boolean local, String currency) throws DatabaseException{
        
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobAccountFeeType()) {
                fee = configurationFee.getPobAccountFee() ;
            } else {
                switch (configurationFee.getPobAccountFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobAccountFee()+ (configurationFee.getPobAccountFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobAccountFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobAccountFee() ;
                        break;
                }
            }

            double configFee = configurationFee.getPobAccountFeeCap();

            if (fee > configFee && configurationFee.getPobAccountFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterAccountFeeType()) {
                fee = configurationFee.getInterAccountFee() ;
            } else {
                switch (configurationFee.getInterAccountFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterAccountFee() * rate ) + (configurationFee.getInterAccountFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterAccountFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterAccountFee();
                        break;
                }
            }

            double configFee = configurationFee.getInterAccountFeeCap();

            if (fee > configFee && configurationFee.getInterAccountFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
        
    }
    
    private double getAccountFee(MerchantFee configurationFee, double value, boolean local, String currency) throws DatabaseException{
        
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobAccountFeeType()) {
                fee = configurationFee.getPobAccountFee() ;
            } else {
                switch (configurationFee.getPobAccountFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobAccountFee()+ (configurationFee.getPobAccountFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobAccountFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobAccountFee() ;
                        break;
                }
            }

            double configFee = configurationFee.getPobAccountFeeCap();

            if (fee > configFee && configurationFee.getPobAccountFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterAccountFeeType()) {
                fee = configurationFee.getInterAccountFee() ;
            } else {
                switch (configurationFee.getInterAccountFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterAccountFee() * rate ) + (configurationFee.getInterAccountFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterAccountFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterAccountFee();
                        break;
                }
            }

            double configFee = configurationFee.getInterAccountFeeCap();

            if (fee > configFee && configurationFee.getInterAccountFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    private double getFee(ConfigurationFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100 );
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    private double getFee(MerchantFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    @Path(value = "/provider/summary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getReportSummary(@QueryParam("provider") String provider, @QueryParam("currency") String currency,
            @QueryParam(value = "range") String range) throws DatabaseException {

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

        Map<String, Object> result = new HashMap<>();

        Map<String, Object> resultQuery = transactionDao.getTransactionSummary(startDate, endDate, null, currency, provider, null, false);

        result.put("totalvalue", resultQuery.getOrDefault("amount", resultQuery));

        resultQuery = transactionDao.getTransactionSummary(startDate, endDate, null, currency, provider, null, true);

        result.put("refund", resultQuery.getOrDefault("amount", resultQuery));

        List list = chargeBackDao.findSummaryByProvider(startDate, endDate, provider, currency);

        result.put("chargeback", ((Object[]) list.get(0))[0]);

        Provider p = providerDao.findByKey("shortName", provider);

        List settlements = settlementDao.findSummaryByQuery(startDate, endDate, p, currency, null, null);

//        Map<String, Object> response = new HashMap<>();
        result.put("settlement", ((Object[]) settlements.get(0))[0]);

        return Response.status(Response.Status.OK).entity(result).build();
    }
    
    @Path(value = "/cycle")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getSettlementCycle(@QueryParam(value = "product") String product,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "merchant") String merchant){
        
        if("".equals(merchant)){
            merchant = null;
        }
        
        if("".equals(product))
            product = null;
        
        PageResult<SettlementCycleModel> pageResult;
        
        Page<SettlementCycle> page =  settlementCycleDao.find(start, length, product, merchant);
        
        List<SettlementCycle> cycles = page.getContent();
        
        if(cycles == null)
            pageResult = new PageResult<>(new ArrayList<>(),0L,0L);
        else{
            
            List<SettlementCycleModel> cycleModels = cycles.stream().map((SettlementCycle cycle) -> {
                
                SettlementCycleModel cycleModel = new SettlementCycleModel();
                cycleModel.setId(cycle.getId());
                cycleModel.setCreatedOn(cycle.getCreatedOn());
                cycleModel.setMerchantId(cycle.getMerchantId());
                cycleModel.setMerchantName(cycle.getMerchantName());
                cycleModel.setProduct(cycle.getProduct());
                cycleModel.setCycle(cycle.getCycle());
                
                return cycleModel;
            }).collect(Collectors.<SettlementCycleModel>toList());
            
            pageResult = new PageResult<>(cycleModels, page.getCount(), page.getCount());
        }
        
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }

    
    @Path(value = "/update/cycle")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response updateSettlementCycle(@Valid UpdateSettlementCycleRequest request){
        
        try {
            Page<SettlementCycle> page = settlementCycleDao.find(0, 0, request.getProduct(), request.getMerchantId());
            
            if(page.getContent() == null || page.getContent().isEmpty()){
                
                Map<String, String> response = new HashMap<>();
                response.put("responsecode", "01");
                response.put("responsemessage", "record not found");
                
                return Response.status(Response.Status.OK).entity(response).build();
            }
            
            SettlementCycle cycle = page.getContent().get(0);
            
            cycle.setCycle(request.getCycle());
            cycle.setModifiedOn(new Date());
            cycle.setModifiedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal()+""));
            
            settlementCycleDao.update(cycle);
            
            Map<String, String> response = new HashMap<>();
            response.put("responsecode", "00");
            response.put("responsemessage", "Record updated successfully");

            return Response.status(Response.Status.OK).entity(response).build();
            
        } catch (Exception ex) {
            Logger.getLogger(SettlementApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Map<String, String> response = new HashMap<>();
        response.put("responsecode", "02");
        response.put("responsemessage", "Unable to update");

        return Response.status(Response.Status.OK).entity(response).build();
        
    }
    
    @Path(value = "/airtime")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAirtimeSettlementReport(@QueryParam(value = "range") String range){
        
        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        List settlementReport = airtimeTransactionDao.getSettlementSummary(startDate, endDate);
        
        Object[] settlement = (Object[])settlementReport.get(0);
        
        Double totalValue = (Double) settlement[0];
        Long count =  new BigDecimal(settlement[1].toString()).longValue();
        
        String settlementParties = configurationDao.getConfig("airtime_settlement");
        
        Map<String, Double> settlementList = new HashMap<>();
        
        if(settlementParties != null){
            
            String[] parties = settlementParties.split(",");
            
            for(String p : parties){
                
                String[] item = p.split(":");
                
                settlementList.put(item[0], new BigDecimal(item[1]).doubleValue());
            }
        }else{
        
            settlementList.put("Clickatell", 96.0);
            settlementList.put("Flutterwave", 2.0);
            settlementList.put("PWC", 2.0);
        }
        
        Map<String, Object> response = new HashMap<>();
        
        Map<String, Double> data = new HashMap<>();
        
        if(totalValue == null)
            totalValue = 0.0;
        
        
        for(Map.Entry<String, Double> entry : settlementList.entrySet()){

            double value = totalValue *  entry.getValue() / 100 ;
            
            value = Utility.convertTo2DP(value);
            
            data.put(entry.getKey(), value);

        }
        
        response.put("data", data);
        response.put("totalValue", totalValue);
        response.put("totalVolume", count);
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
}

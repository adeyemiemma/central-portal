/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.ControlDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.ControlModelMDao;
import com.flutterwave.flutter.clientms.model.ControlModel;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.ControlModelM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "controlController")
@SessionScoped
public class ControlController implements Serializable {
    
    private ControlModel controlModel;

    private DataModel allControl = null;
    private DataModel allUnauthControl = null;

    private static final Logger LOGGER = Logger.getLogger(ControlController.class.getName());

    private PaginationHelper pagination, paginationHelper;
    
    @EJB
    private ControlDao controlDao;
    @EJB
    private ControlModelMDao controlMDao;
    @EJB
    private LogService logService;
    @EJB
    private UserDao userDao;
    
     public ControlModel getSelected() {
        if (controlModel == null) {
            controlModel = new ControlModel();
        }
        return controlModel;
    }

    public void setSelected(ControlModel controlModel) {

        this.controlModel = controlModel;
    }
    
    public String create() {

        if (controlModel != null) {
            controlModel.setCreatedOn(new Date());
        }

        try {
            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");

            ControlModel c = controlDao.findByKey("name", controlModel.getName());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create controlModel: " + controlModel.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate controlModel with name " + c.getName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("ControlModel with name %s exists", controlModel.getName()));

                return "create";
            }

//            List<ControlModelM> countries = controlModelMDao.find("name", controlModel.getName());
            boolean status = findUnauthorizedControlModel(controlModel.getName());

            if (status == true) {

                Log log = new Log();
                log.setAction("Create controlModel: " + controlModel.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Pending controlModel with name " + controlModel.getName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Pending approval on ControlModel with  name %s exists", controlModel.getName()));

                return "create";
            }

            ControlModelM controlModelM = new ControlModelM();
            controlModelM.setCreatedOn(new Date());
            controlModelM.setCreatedBy(user);
            controlModelM.setName(controlModel.getName());
            controlModelM.setState(true);
            controlModelM.setStatus(Status.PENDING);

            controlMDao.create(controlModelM);

            Log log = new Log();
            log.setAction("Create controlModel: " + controlModel.getName());
            log.setCreatedOn(new Date());
            log.setDescription("controlModel has been created and submitted for approval successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

            JsfUtil.addSuccessMessage(String.format("ControlModel %s has been added successfully waiting for approval", controlModel.getName()));
            
            controlModel = new ControlModel();
            
            recreate();
            
            prepareCreate();
            
            return "/control/list";
            
        } catch (DatabaseException de) {

            try {
                Log log = new Log();
                log.setAction("create controlModel " + controlModel.getName());
                log.setCreatedOn(new Date());
                log.setDescription("creation of controlModel");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (de != null) {

                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception ex) {
            }

            if (de != null) {
                de.printStackTrace();
                JsfUtil.addErrorMessage(de.getMessage());
            }
        }

        return "create";

    }
    
    public boolean findUnauthorizedControlModel(String data) {

        try {
            List<ControlModelM> countries = controlMDao.find("name", controlModel.getName());

            if (countries != null && !countries.isEmpty()) {

                ControlModelM cM = countries.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {
                    return true;
                }
            }

        } catch (Exception exception) {
            Logger.getLogger(ControlController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return false;
    }
    
    public void recreate() {
        pagination = null;
        paginationHelper = null;
    }
    
    public String prepareCreate() {

        controlModel = new ControlModel();

        return "create";
    }
    
    public PaginationHelper getPaginationHelper() {

        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public long getItemsCount() {
                    return controlDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(controlDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ControlController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }

    public PaginationHelper getPaginationHelperUnauth() {

        if (paginationHelper == null) {
            paginationHelper = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return controlMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(controlMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ControlController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationHelper;
    }

    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("controlupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("controlupdated", false);
        }

        if (pagination == null || pagination.getItemsCount() <= 0) {
            allControl = getPaginationHelper().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Controls");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Controls");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

        logService.log(log);

        return allControl;
    }

    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("controlupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationHelper = null;
            session.setAttribute("controlupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch Unauthorized Controls");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching Unauthorized Controls");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

        logService.log(log);

        if (paginationHelper == null) {
            allUnauthControl = getPaginationHelperUnauth().createPageDataModel();
        }

        return allUnauthControl;
    }
    
    public String prepareUpdate() throws DatabaseException{
        
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String param = params.get("editParam") + "";
        
        long id = Long.parseLong(param);
        
        if(id <= 0){
            JsfUtil.addErrorMessage("Data not found") ;           
            return "list";
        }
        
        controlModel = controlDao.find(id);
        
        return "edit";
    }
    
    
    public String update() {

        try {

            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");

            ControlModel curr = controlDao.findByKey("name", controlModel.getName());

            if (curr != null && curr.getId() != controlModel.getId()) {

                JsfUtil.addErrorMessage("Short name exists");

                return "edit";
            }

            List<ControlModelM> list = controlMDao.find("modelId", controlModel.getId());

            if (list != null && !list.isEmpty()) {

                ControlModelM cM = list.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {

                    JsfUtil.addErrorMessage("A pending update exists");
                    return "edit";
                }
            }

            ControlModelM controlM = new ControlModelM();
            controlM.setModelId(controlModel.getId());
            controlM.setCreatedOn(new Date());
            controlM.setState(controlModel.isState());
            controlM.setName(controlM.getName());
            controlM.setCreatedBy(user);

            controlM = controlMDao.create(controlM);

            Log log = new Log();
            log.setAction("Update ControlModel " + controlM.getName());
            log.setCreatedOn(new Date());
            log.setDescription("ControlModel updated has been created and successfully updated");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

            if (controlM == null) {
                throw new DatabaseException("Unable to update record");
            }

            JsfUtil.addSuccessMessage("ControlModel has been updated succesfully waiting for authorization");

            paginationHelper = null;

            return "/control/list";

        } catch (DatabaseException ex) {
            Logger.getLogger(ControlController.class.getName()).log(Level.SEVERE, null, ex);

            if (ex != null) {
                JsfUtil.addErrorMessage("" + ex.getMessage());
            } else {
                JsfUtil.addErrorMessage("Unable to update record , please try again later");
            }

            try {
                Log log = new Log();
                log.setAction("update control " + controlModel.getName());
                log.setCreatedOn(new Date());
                log.setDescription("update control");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {
//                    log.setStatusMessage(ex.getMessage());

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return "edit";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;


import com.flutterwave.flutter.clientms.dao.PermissionDao;
import com.flutterwave.flutter.clientms.model.Permission;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "permissionController")
@RequestScoped
public class PermissionController {

    @EJB
    private PermissionDao permissionDao;
    /**
     * @return the permission
     */
    public Permission getPermission() {
        
        if(permission == null)
            permission = new Permission();
        
        return permission;
    }

    /**
     * @param permission the permission to set
     */
    public void setPermission(Permission permission) {
        this.permission = permission;
    }
    
    public String create(){
        
        try {
            
            permission.setName(permission.getName().trim().replaceAll(" ", "_").toUpperCase());
            
            Permission p = permissionDao.findByKey("name", permission.getName());
            
            if(p != null){
                JsfUtil.addErrorMessage("Permission with name "+permission.getName()+" exists");
                return "/permission/create";
            }
            
            permission.setCreated(new Date());
            permission = permissionDao.create(permission);
            
            JsfUtil.addSuccessMessage("Permission has been created successfully");
            
            permission = new Permission();
            
            return "/permission/create";
        } catch (Exception ex) {
            Logger.getLogger(PermissionController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        
        return "/permission/create";
    }

    
    public List<Permission> getAllPermissions(){
        
        try {
            return permissionDao.findAll();
        } catch (DatabaseException ex) {
            Logger.getLogger(PermissionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return new ArrayList<>();
    }
    
    
    private Permission permission;
    
}

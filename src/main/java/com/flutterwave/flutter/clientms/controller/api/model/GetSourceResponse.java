/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;

/**
 *
 * @author emmanueladeyemi
 */
public class GetSourceResponse {

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the sources
     */
    public List<Map> getSources() {
        return sources;
    }

    /**
     * @param sources the sources to set
     */
    public void setSources(List<Map> sources) {
        this.sources = sources;
    }
    
    @JsonProperty(value = "responsecode")
    private String responseCode;
    @JsonProperty(value = "responsemessage")
    private String responseMessage;
    private List<Map> sources;
}

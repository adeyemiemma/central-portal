/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.flutterwave.flutter.clientms.model.MerchantCompliance;
import com.flutterwave.flutter.clientms.model.MerchantComplianceLog;
import com.flutterwave.flutter.clientms.util.Status;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
public class GetComplianceModel {

    /**
     * @return the bvn
     */
    public String getBvn() {
        return bvn;
    }

    /**
     * @param bvn the bvn to set
     */
    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    /**
     * @return the bvnChecked
     */
    public boolean isBvnChecked() {
        return bvnChecked;
    }

    /**
     * @param bvnChecked the bvnChecked to set
     */
    public void setBvnChecked(boolean bvnChecked) {
        this.bvnChecked = bvnChecked;
    }

    /**
     * @return the bvnCheckedOn
     */
    public Date getBvnCheckedOn() {
        return bvnCheckedOn;
    }

    /**
     * @param bvnCheckedOn the bvnCheckedOn to set
     */
    public void setBvnCheckedOn(Date bvnCheckedOn) {
        this.bvnCheckedOn = bvnCheckedOn;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the rejectionReason
     */
    public String getRejectionReason() {
        return rejectionReason;
    }

    /**
     * @param rejectionReason the rejectionReason to set
     */
    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    /**
     * @return the resubmitted
     */
    public int getResubmitted() {
        return resubmitted;
    }

    /**
     * @param resubmitted the resubmitted to set
     */
    public void setResubmitted(int resubmitted) {
        this.resubmitted = resubmitted;
    }

    /**
     * @return the lastResubmitted
     */
    public Date getLastResubmitted() {
        return lastResubmitted;
    }

    /**
     * @param lastResubmitted the lastResubmitted to set
     */
    public void setLastResubmitted(Date lastResubmitted) {
        this.lastResubmitted = lastResubmitted;
    }

    /**
     * @return the lastProcessed
     */
    public Date getLastProcessed() {
        return lastProcessed;
    }

    /**
     * @param lastProcessed the lastProcessed to set
     */
    public void setLastProcessed(Date lastProcessed) {
        this.lastProcessed = lastProcessed;
    }

    /**
     * @return the processingCounter
     */
    public int getProcessingCounter() {
        return processingCounter;
    }

    /**
     * @param processingCounter the processingCounter to set
     */
    public void setProcessingCounter(int processingCounter) {
        this.processingCounter = processingCounter;
    }

    /**
     * @return the registeredName
     */
    public String getRegisteredName() {
        return registeredName;
    }

    /**
     * @param registeredName the registeredName to set
     */
    public void setRegisteredName(String registeredName) {
        this.registeredName = registeredName;
    }

    /**
     * @return the registeredNumber
     */
    public String getRegisteredNumber() {
        return registeredNumber;
    }

    /**
     * @param registeredNumber the registeredNumber to set
     */
    public void setRegisteredNumber(String registeredNumber) {
        this.registeredNumber = registeredNumber;
    }

    /**
     * @return the dateofIncorporation
     */
    public String getDateofIncorporation() {
        return dateofIncorporation;
    }

    /**
     * @param dateofIncorporation the dateofIncorporation to set
     */
    public void setDateofIncorporation(String dateofIncorporation) {
        this.dateofIncorporation = dateofIncorporation;
    }

    /**
     * @return the tradingName
     */
    public String getTradingName() {
        return tradingName;
    }

    /**
     * @param tradingName the tradingName to set
     */
    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    /**
     * @return the registeredAddress
     */
    public String getRegisteredAddress() {
        return registeredAddress;
    }

    /**
     * @param registeredAddress the registeredAddress to set
     */
    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    /**
     * @return the operationalAddress
     */
    public String getOperationalAddress() {
        return operationalAddress;
    }

    /**
     * @param operationalAddress the operationalAddress to set
     */
    public void setOperationalAddress(String operationalAddress) {
        this.operationalAddress = operationalAddress;
    }

    /**
     * @return the companyType
     */
    public String getCompanyType() {
        return companyType;
    }

    /**
     * @param companyType the companyType to set
     */
    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    /**
     * @return the companyRegDocumentPath
     */
    public String getCompanyRegDocumentPath() {
        return companyRegDocumentPath;
    }

    /**
     * @param companyRegDocumentPath the companyRegDocumentPath to set
     */
    public void setCompanyRegDocumentPath(String companyRegDocumentPath) {
        this.companyRegDocumentPath = companyRegDocumentPath;
    }

    /**
     * @return the directorIdPath
     */
    public String getDirectorIdPath() {
        return directorIdPath;
    }

    /**
     * @param directorIdPath the directorIdPath to set
     */
    public void setDirectorIdPath(String directorIdPath) {
        this.directorIdPath = directorIdPath;
    }

    /**
     * @return the contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * @param contactName the contactName to set
     */
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    /**
     * @return the contactPhone
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * @param contactPhone the contactPhone to set
     */
    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    /**
     * @return the contactAddress
     */
    public String getContactAddress() {
        return contactAddress;
    }

    /**
     * @param contactAddress the contactAddress to set
     */
    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    /**
     * @return the contactEmail
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * @param contactEmail the contactEmail to set
     */
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     * @return the industry
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * @param industry the industry to set
     */
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    /**
     * @return the products
     */
    public String getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(String products) {
        this.products = products;
    }

    /**
     * @return the productWebUrl
     */
    public String getProductWebUrl() {
        return productWebUrl;
    }

    /**
     * @param productWebUrl the productWebUrl to set
     */
    public void setProductWebUrl(String productWebUrl) {
        this.productWebUrl = productWebUrl;
    }

    /**
     * @return the operatingLicencePath
     */
    public String getOperatingLicencePath() {
        return operatingLicencePath;
    }

    /**
     * @param operatingLicencePath the operatingLicencePath to set
     */
    public void setOperatingLicencePath(String operatingLicencePath) {
        this.operatingLicencePath = operatingLicencePath;
    }

    /**
     * @return the amlPolicyPath
     */
    public String getAmlPolicyPath() {
        return amlPolicyPath;
    }

    /**
     * @param amlPolicyPath the amlPolicyPath to set
     */
    public void setAmlPolicyPath(String amlPolicyPath) {
        this.amlPolicyPath = amlPolicyPath;
    }

    /**
     * @return the shareHolder1
     */
    public String getShareHolder1() {
        return shareHolder1;
    }

    /**
     * @param shareHolder1 the shareHolder1 to set
     */
    public void setShareHolder1(String shareHolder1) {
        this.shareHolder1 = shareHolder1;
    }

    /**
     * @return the shareHolder2
     */
    public String getShareHolder2() {
        return shareHolder2;
    }

    /**
     * @param shareHolder2 the shareHolder2 to set
     */
    public void setShareHolder2(String shareHolder2) {
        this.shareHolder2 = shareHolder2;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the scumlCertificatePath
     */
    public String getScumlCertificatePath() {
        return scumlCertificatePath;
    }

    /**
     * @param scumlCertificatePath the scumlCertificatePath to set
     */
    public void setScumlCertificatePath(String scumlCertificatePath) {
        this.scumlCertificatePath = scumlCertificatePath;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the modified
     */
    public Date getModified() {
        return modified;
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(Date modified) {
        this.modified = modified;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the approvedBy
     */
    public String getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the apiUserId
     */
    public String getApiUserId() {
        return apiUserId;
    }

    /**
     * @param apiUserId the apiUserId to set
     */
    public void setApiUserId(String apiUserId) {
        this.apiUserId = apiUserId;
    }
    
    
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    
    
    private String registeredName;
    private String registeredNumber;
    private String dateofIncorporation;  
    private String tradingName;
    private String registeredAddress;
    private String operationalAddress;
    private String companyType;
    private String companyRegDocumentPath;
    private String directorIdPath;
    private String contactName;
    private String contactPhone;      
    private String contactAddress;
    private String contactEmail;    
    private String industry;
    private String products;
    private String productWebUrl;
    private String operatingLicencePath;
    private String amlPolicyPath;
    private String shareHolder1;
    private String shareHolder2;
    private String country;
    private String merchantId;
    private String scumlCertificatePath;
    private long id;
    private Date createdOn;
    private Date modified;
    private Date approvedOn;
    private String approvedBy;
    private String apiUserId;
    private Status status;
    private int resubmitted;
    private Date lastResubmitted;
    private Date lastProcessed;
    private int processingCounter;
    private String rejectionReason;
    private String description;
    private boolean bvnChecked;
    private Date bvnCheckedOn;
    private String bvn;
    
    public static GetComplianceModel getComplianceModel(MerchantCompliance compliance){
        
        GetComplianceModel complianceModel = new GetComplianceModel();
        complianceModel.setRegisteredAddress(compliance.getRegisteredAddress());
        complianceModel.setAmlPolicyPath(compliance.getAmlPolicyPath());
        complianceModel.setApiUserId(compliance.getApiUserId());
        complianceModel.setApprovedOn(compliance.getApprovedOn());
        complianceModel.setApprovedBy(compliance.getApprovedBy() == null ? null : compliance.getApprovedBy().getFirstName()+" "+compliance.getApprovedBy().getLastName());
        complianceModel.setCompanyRegDocumentPath(compliance.getCompanyRegDocumentPath());
        complianceModel.setCompanyType(compliance.getCompanyType());
        complianceModel.setContactAddress(compliance.getContactAddress());
        complianceModel.setContactName(compliance.getContactName());
        complianceModel.setContactEmail(compliance.getContactEmail());
        complianceModel.setContactPhone(compliance.getContactPhone());
        complianceModel.setCountry(compliance.getCountry());
        complianceModel.setCreatedOn(compliance.getCreatedOn());
        complianceModel.setDateofIncorporation(compliance.getDateofIncorporation());
        complianceModel.setDirectorIdPath(compliance.getDirectorIdPath());
        complianceModel.setId(compliance.getId());
        complianceModel.setIndustry(compliance.getIndustry());
        complianceModel.setMerchantId(compliance.getMerchantId());
        complianceModel.setModified(compliance.getModified());
        complianceModel.setOperatingLicencePath(compliance.getOperatingLicencePath());
        complianceModel.setOperationalAddress(compliance.getOperationalAddress());
        complianceModel.setProductWebUrl(compliance.getProductWebUrl());
        complianceModel.setProducts(compliance.getProducts());
        complianceModel.setRegisteredAddress(compliance.getRegisteredAddress());
        complianceModel.setRegisteredName(compliance.getRegisteredName());
        complianceModel.setRegisteredNumber(compliance.getRegisteredNumber());
        complianceModel.setScumlCertificatePath(compliance.getScumlCertificatePath());
        complianceModel.setShareHolder1(compliance.getShareHolder1());
        complianceModel.setShareHolder2(compliance.getShareHolder2());
        complianceModel.setTradingName(compliance.getTradingName());
        complianceModel.setLastProcessed(compliance.getLastProcessed());
        complianceModel.setStatus(compliance.getStatus());
        complianceModel.setProcessingCounter(compliance.getProcessingCounter());
        complianceModel.setResubmitted(compliance.getResubmitted());
        complianceModel.setLastResubmitted(compliance.getLastResubmitted());
        complianceModel.setRejectionReason(compliance.getRejectionReason());
        complianceModel.setDescription(compliance.getDescription());
        complianceModel.setBvnChecked(compliance.isBvnChecked());
        complianceModel.setBvnCheckedOn(compliance.getBvnCheckedOn());
        complianceModel.setBvn(compliance.getBvn());
        
        return complianceModel;
    }
    
    public static GetComplianceModel getComplianceModel(MerchantComplianceLog compliance){
        
        GetComplianceModel complianceModel = new GetComplianceModel();
        complianceModel.setRegisteredAddress(compliance.getRegisteredAddress());
        complianceModel.setAmlPolicyPath(compliance.getAmlPolicyPath());
        complianceModel.setApiUserId(compliance.getApiUserId());
        complianceModel.setApprovedOn(compliance.getApprovedOn());
        complianceModel.setApprovedBy(compliance.getApprovedBy() == null ? null : compliance.getApprovedBy().getFirstName()+" "+compliance.getApprovedBy().getLastName());
        complianceModel.setCompanyRegDocumentPath(compliance.getCompanyRegDocumentPath());
        complianceModel.setCompanyType(compliance.getCompanyType());
        complianceModel.setContactAddress(compliance.getContactAddress());
        complianceModel.setContactName(compliance.getContactName());
        complianceModel.setContactEmail(compliance.getContactEmail());
        complianceModel.setContactPhone(compliance.getContactPhone());
        complianceModel.setCountry(compliance.getCountry());
        complianceModel.setCreatedOn(compliance.getCreatedOn());
        complianceModel.setDateofIncorporation(compliance.getDateofIncorporation());
        complianceModel.setDirectorIdPath(compliance.getDirectorIdPath());
        complianceModel.setId(compliance.getId());
        complianceModel.setIndustry(compliance.getIndustry());
        complianceModel.setMerchantId(compliance.getMerchantId());
        complianceModel.setModified(compliance.getModified());
        complianceModel.setOperatingLicencePath(compliance.getOperatingLicencePath());
        complianceModel.setOperationalAddress(compliance.getOperationalAddress());
        complianceModel.setProductWebUrl(compliance.getProductWebUrl());
        complianceModel.setProducts(compliance.getProducts());
        complianceModel.setRegisteredAddress(compliance.getRegisteredAddress());
        complianceModel.setRegisteredName(compliance.getRegisteredName());
        complianceModel.setRegisteredNumber(compliance.getRegisteredNumber());
        complianceModel.setScumlCertificatePath(compliance.getScumlCertificatePath());
        complianceModel.setShareHolder1(compliance.getShareHolder1());
        complianceModel.setShareHolder2(compliance.getShareHolder2());
        complianceModel.setTradingName(compliance.getTradingName());
        complianceModel.setStatus(compliance.getStatus());
        complianceModel.setRejectionReason(compliance.getRejectionReason());
        complianceModel.setDescription(compliance.getDescription());
        complianceModel.setBvnChecked(compliance.isBvnChecked());
        complianceModel.setBvnCheckedOn(compliance.getBvnCheckedOn());
        
        return complianceModel;
    }
}

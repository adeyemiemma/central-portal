/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProvisionTerminalRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }
    
    @JsonProperty(value = "pwcmerchantid")
    @NotBlank(message = "PWC merchant id must be provided")
    private String merchantId;
    @JsonProperty(value = "merchantcode")
    @NotBlank(message = "Merchant code must be provided")
    private String merchantCode;
    @NotBlank(message = "Hash value must be provided")
    private String hash;

    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("merchantId", merchantId);
        jSONObject.put("merchantcode", merchantCode);
        jSONObject.put("hash", hash);
        
        return jSONObject.toString();
    }
    
    public String toHashableString(){
        
        StringBuilder builder = new StringBuilder();
        builder.append(merchantId)
                .append(merchantCode);
        
        return builder.toString();
    }
    
}

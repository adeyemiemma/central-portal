/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.api.model.UpdateProviderRequest;
import com.flutterwave.flutter.clientms.service.AirtimeHelper;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.util.PageResult;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Path(value = "/airtime")
@RequestScoped
@Produces(value = MediaType.APPLICATION_JSON)
public class AirtimeApiController {
    
    @EJB
    private AirtimeHelper airtimeHelper;
    
    
    @Path(value = "/status")
    @GET
    public Response getStatus(){
        
        List<String> data = airtimeHelper.getStatus();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/balance")
    @GET
    public Response getBalance(){
        
        List data = airtimeHelper.getBalance();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/network")
    @GET
    public Response getNetwork(){
        
        List data = airtimeHelper.getNetwork();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/provider")
    @GET
    public Response getProvider(){
        
        List data = airtimeHelper.getProvider();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/provider/others")
    @GET
    public Response getProviderOthers(){
        
        List data = airtimeHelper.getProviderOthers();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/provider/config")
    @GET
    public Response getProviderConfiguration(){
        
        List data = airtimeHelper.getProviderConfigurations();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/transaction/all")
    @GET
    public Response getTransactions(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "network") String network,
            @QueryParam(value = "range") String date,
            @QueryParam(value = "searchValue") String search,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "status") String status){
        
        if("any".equalsIgnoreCase(network)){
            network = null;
        }
        
        if("any".equalsIgnoreCase(status)){
            status = null;
        }
        
        if("any".equalsIgnoreCase(provider)){
            provider = null;
        }
        
        Date startDate = null, endDate = null;
        
        String startDateStr = null, endDateStr = null;

        if (Utility.emptyToNull(date) != null) {

            try {
                String[] splitDate = date.split("-");
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                
                startDateStr = Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss");

                endDate = dateFormat.parse(splitDate[1]);
                
                endDateStr = Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        PageResult data = airtimeHelper.getTransactions(start, length, network , status, startDateStr, endDateStr, search, provider);
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/transaction/others")
    @GET
    public Response getMobileTransactions(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "network") String network,
            @QueryParam(value = "range") String date,
            @QueryParam(value = "searchValue") String search,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "status") String status){
        
        if("any".equalsIgnoreCase(network)){
            network = null;
        }
        
        if("any".equalsIgnoreCase(status)){
            status = null;
        }
        
        if("any".equalsIgnoreCase(provider)){
            provider = null;
        }
        
        Date startDate = null, endDate = null;
        
        String startDateStr = null, endDateStr = null;

        if (Utility.emptyToNull(date) != null) {

            try {
                String[] splitDate = date.split("-");
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                
                startDateStr = Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss");

                endDate = dateFormat.parse(splitDate[1]);
                
                endDateStr = Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        PageResult data = airtimeHelper.getTransactionOtherss(start, length, network , status, startDateStr, endDateStr, search, provider);
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/transaction/download")
    @GET
    public Response downloadTransaction(@QueryParam(value = "network") String network,
            @QueryParam(value = "range") String date,
            @QueryParam(value = "searchValue") String search,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "status") String status){
        
        if("any".equalsIgnoreCase(network)){
            network = null;
        }
        
        if("any".equalsIgnoreCase(status)){
            status = null;
        }
        
        if("any".equalsIgnoreCase(provider)){
            provider = null;
        }
        
        Date startDate = null, endDate = null;
        
        String startDateStr = null, endDateStr = null;

        if (Utility.emptyToNull(date) != null) {

            try {
                String[] splitDate = date.split("-");
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                
                startDateStr = Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss");

                endDate = dateFormat.parse(splitDate[1]);
                
                endDateStr = Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        long age = Math.abs(Utility.age(endDate, startDate, true));
        
        if(age > 7){
            
            Map<String, String> response = new HashMap<>();
            response.put("status", "failed");
            response.put("description", "Maximum of 7 day(s) of transactions is permitted");
            
            return Response.status(Response.Status.OK).entity(response).build();
        }
        
        String email = SecurityUtils.getSubject().getPrincipal().toString();
        
        if("admin@flutterwavego.com".equalsIgnoreCase(email)){
            email = "emmanuel@flutterwavego.com";
        }
        
        JSONObject jSONObject = airtimeHelper.downloadTransaction(network, status, startDateStr, endDateStr, search, provider, email);
        
        return Response.status(Response.Status.OK).entity(jSONObject.toMap()).build();
    }
    
    @Path(value = "/transaction/others/download")
    @GET
    public Response downloadOtherTransaction(@QueryParam(value = "network") String network,
            @QueryParam(value = "range") String date,
            @QueryParam(value = "searchValue") String search,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "status") String status){
        
        if("any".equalsIgnoreCase(network)){
            network = null;
        }
        
        if("any".equalsIgnoreCase(status)){
            status = null;
        }
        
        if("any".equalsIgnoreCase(provider)){
            provider = null;
        }
        
        Date startDate = null, endDate = null;
        
        String startDateStr = null, endDateStr = null;

        if (Utility.emptyToNull(date) != null) {

            try {
                String[] splitDate = date.split("-");
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                
                startDateStr = Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss");

                endDate = dateFormat.parse(splitDate[1]);
                
                endDateStr = Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        long age = Math.abs(Utility.age(endDate, startDate, true));
        
        if(age > 7){
            
            Map<String, String> response = new HashMap<>();
            response.put("status", "failed");
            response.put("description", "Maximum of 7 day(s) of transactions is permitted");
            
            return Response.status(Response.Status.OK).entity(response).build();
        }
        
        String email = SecurityUtils.getSubject().getPrincipal().toString();
        
        if("admin@flutterwavego.com".equalsIgnoreCase(email)){
            email = "emmanuel@flutterwavego.com";
        }
        
        JSONObject jSONObject = airtimeHelper.downloadTransactionMobileApp(network, status, startDateStr, endDateStr, search, provider, email);
        
        return Response.status(Response.Status.OK).entity(jSONObject.toMap()).build();
    }
    
    
    @Path(value = "/provider/update")
    @POST
    public Response updateDefaultProvider(@Valid UpdateProviderRequest request){
        
        JSONObject object = airtimeHelper.setDefaultProvider(request.getProvider(), request.getNetwork());
        
        if(object == null){
            
            Map<String, String> response = new HashMap<>();
            response.put("status", "failed");
            response.put("description", "Unable to set default provider");
            
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }
        
        return Response.status(Response.Status.OK).entity(object.toMap()).build();
    }
    
    @Path(value = "/merchants")
    @GET
    public Response getVasMerchant(){
        
        return airtimeHelper.getMerchant();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.BankDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.BankMDao;
import com.flutterwave.flutter.clientms.model.Bank;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.BankM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.viewmodel.UploadModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import javax.transaction.Transactional;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author adeyemi
 */
@Named(value = "bankController")
@SessionScoped
public class BankController implements Serializable {

    private Bank bank;

    private DataModel allBanks = null;
    private DataModel allUnauthBanks = null;

    private static final Logger LOGGER = Logger.getLogger(BankController.class.getName());

    @EJB
    private BankDao bankDao;
    @EJB
    private BankMDao bankMDao;
    @EJB
    private LogService logService;
    @EJB
    private UserDao userDao;

    private PaginationHelper pagination, paginationHelper;

    private UploadModel uploadModel;

    public UploadModel getUploadModel() {

        if (uploadModel == null) {
            uploadModel = new UploadModel();
        }

        return uploadModel;
    }

    public void setUploadModel(UploadModel uploadModel) {
        this.uploadModel = uploadModel;
    }

    public BankController() {
    }

    public Bank getSelected() {
        if (bank == null) {
            bank = new Bank();
        }
        return bank;
    }

    public void setSelected(Bank bank) {

        this.bank = bank;
    }

    public String prepareCreate() {

        bank = new Bank();

        return "create";
    }

    public String prepateUpdate() {

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String param = params.get("editParam") + "";

        try {
            bank = bankDao.find(Long.parseLong(param));

            Log log = new Log();
            log.setAction("Update Bank " + bank.getName());
            log.setCreatedOn(new Date());
            log.setDescription("about to update bank");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

        } catch (DatabaseException de) {
            Logger.getLogger(BankController.class.getName()).log(Level.SEVERE, null, de);

            try {
                Log log = new Log();
                log.setAction("update bank " + bank.getName());
                log.setCreatedOn(new Date());
                log.setDescription("update bank");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (de != null) {
//                    log.setStatusMessage(de.getMessage());

                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception ex) {
            }
        }

        return "edit";
    }

    public String update() {

        try {

            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");

            Bank curr = bankDao.findByKey("code", bank.getCode());

            if (curr != null && curr.getId() != bank.getId()) {

                JsfUtil.addErrorMessage("Short name exists");

                return "edit";
            }

            List<BankM> list = bankMDao.find("modelId", bank.getId());

            if (list != null && !list.isEmpty()) {

                BankM cM = list.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {

                    JsfUtil.addErrorMessage("A pending update exists");
                    return "edit";
                }
            }

            BankM bankM = new BankM();
            bankM.setModelId(bank.getId());
            bankM.setCreatedOn(new Date());
            bankM.setName(bank.getName());
            bankM.setCode(bank.getCode());
            bankM.setCreatedBy(user);

            bankM = bankMDao.create(bankM);

            Log log = new Log();
            log.setAction("Update Bank " + bank.getName());
            log.setCreatedOn(new Date());
            log.setDescription("Bank updated has been created and successfully updated");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

            if (bankM == null) {
                throw new DatabaseException("Unable to update record");
            }

            JsfUtil.addSuccessMessage("Bank has been updated succesfully, waiting for authorization");

            paginationHelper = null;

            bankM = new BankM();
            
            return "/bank/list";

        } catch (DatabaseException ex) {
            Logger.getLogger(BankController.class.getName()).log(Level.SEVERE, null, ex);

            if (ex != null) {
                JsfUtil.addErrorMessage("" + ex.getMessage());
            } else {
                JsfUtil.addErrorMessage("Unable to update record , please try again later");
            }

            try {
                Log log = new Log();
                log.setAction("update bank " + bank.getName());
                log.setCreatedOn(new Date());
                log.setDescription("update bank");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {
//                    log.setStatusMessage(ex.getMessage());

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return "edit";
    }

    @Transactional
    public String create() {

        if (bank != null) {
            bank.setCreatedOn(new Date());
        }

        try {
            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");

            Bank c = bankDao.findByKey("name", bank.getName());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create bank: " + bank.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate bank with name " + c.getName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Bank with name %s exists", bank.getName()));

                return "create";
            }

            c = bankDao.findByKey("code", bank.getCode());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create bank: " + bank.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate bank with short name " + c.getCode());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Bank with short name %s exists", bank.getCode()));

                return "create";
            }

//            List<BankM> countries = bankMDao.find("name", bank.getName());
            boolean status = findUnauthorizedBank(bank.getName(), true);

            if (status == true) {

                Log log = new Log();
                log.setAction("Create bank: " + bank.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Pending bank with name " + bank.getCode());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Pending approval on Bank with  name %s exists", bank.getCode()));

                return "create";
            }

            status = findUnauthorizedBank(bank.getCode(), false);

            if (status == true) {

                Log log = new Log();
                log.setAction("Create bank: " + bank.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Pending bank with name " + bank.getCode());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Pending approval on Bank with  name %s exists", bank.getCode()));

                return "create";
            }

            BankM bankM = new BankM();
            bankM.setCreatedOn(new Date());
            bankM.setCreatedBy(user);
            bankM.setName(bank.getName());
            bankM.setCode(bank.getCode());
            bankM.setStatus(Status.PENDING);
            bankM.setProduct(bank.getProduct());
            bankM.setProductId(bank.getProductId());

            bankMDao.create(bankM);

            Log log = new Log();
            log.setAction("Create bank: " + bank.getName());
            log.setCreatedOn(new Date());
            log.setDescription("bank has been created and submitted for approval successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

            JsfUtil.addSuccessMessage(String.format("Bank %s (%s) has been added successfully waiting for approval", bank.getName(), bank.getCode()));
            
            bank = new Bank();
            
            recreate();
            
            prepareCreate();
            
            return "/bank/list";
            
        } catch (DatabaseException de) {

            try {
                Log log = new Log();
                log.setAction("create bank " + bank.getName());
                log.setCreatedOn(new Date());
                log.setDescription("creation of bank");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (de != null) {

                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception ex) {
            }

            if (de != null) {
                de.printStackTrace();
                JsfUtil.addErrorMessage(de.getMessage());
            }
        }

        return "create";
    }

    public Bank getBank(long id) {

        try {
            return bankDao.find(id);
        } catch (DatabaseException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public PaginationHelper getPaginationHelper() {

        if (pagination == null) {
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return bankDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(bankDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(BankController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }

    public PaginationHelper getPaginationHelperUnauth() {

        if (paginationHelper == null) {
            paginationHelper = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return bankMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(bankMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(BankController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationHelper;
    }

    public void recreate() {
        allBanks = null;
        allUnauthBanks = null;
    }

    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("bankupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            recreate();
            session.setAttribute("bankupdated", false);
        }

        if (allBanks == null) {
            allBanks = getPaginationHelper().createPageDataModel();
        }

//        Log log = new Log();
//        log.setAction("Fetch All Banks");
//        log.setCreatedOn(new Date());
//        log.setDescription("Fetching All Banks");
//        log.setLevel(LogLevel.Info);
//        log.setStatus(LogStatus.SUCCESSFUL);
//        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
//
//        logService.log(log);

        return allBanks;
    }

    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("bankupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationHelper = null;
            recreate();
            session.setAttribute("bankupdatedm", false);
        }

//        Log log = new Log();
//        log.setAction("Fetch Unauthorized Banks");
//        log.setCreatedOn(new Date());
//        log.setDescription("Fetching Unauthorized Banks");
//        log.setLevel(LogLevel.Info);
//        log.setStatus(LogStatus.SUCCESSFUL);
//        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
//
//        logService.log(log);

        if (allUnauthBanks == null) {
            allUnauthBanks = getPaginationHelperUnauth().createPageDataModel();
        }

        return allUnauthBanks;
    }
    
    public String nextUnauth() {
        getPaginationHelperUnauth().nextPage();
        recreate();
        return "unauth";
    }
    
    public String previousUnauth() {
        getPaginationHelperUnauth().previousPage();
        recreate();
        return "unauth";
    }
    
    public String next() {
        getPaginationHelper().nextPage();
        recreate();
        return "list";
    }
    
    public String previous() {
        getPaginationHelper().previousPage();
        recreate();
        return "list";
    }

    public List<Bank> getAllBank() throws DatabaseException {

        List<Bank> banks = bankDao.findAll();
        return banks;
    }

    /**
     *
     * @param data
     * @param isName - used to control if it is name code
     * @return
     */
    public boolean findUnauthorizedBank(String data, boolean isName) {

        try {
            List<BankM> countries = bankMDao.find(isName == true ? "name" : "code", isName == true ? bank.getName() : bank.getCode());

            if (countries != null && !countries.isEmpty()) {

                BankM cM = countries.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {
                    return true;
                }
            }

        } catch (Exception exception) {
            Logger.getLogger(BankController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return false;
    }

//    @Transactional
    public String uploadBank() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            List<BankM> list = new ArrayList<>();

            if (fileName.toLowerCase().endsWith("csv")) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(uploadModel.getFile().getInputStream()));

                String line = null;

                Object userString = SecurityUtils.getSubject().getPrincipal();
                User user = userDao.findByKey("email", userString + "");

                int countUpload = 0, countDuplicate = 0;

                Map<String, String> map = new HashMap<>();

                int i = 0;
                while ((line = reader.readLine()) != null) {

                    if(i++ == 0)
                        continue;
                    
                    final String[] data = line.split(",");

                    if (data.length < 2) {
                        continue;
                    }
                    
                    String name = data[0].replaceAll("\"", "");
                    String code = data[1].replaceAll("\"", "");

                    if (bankDao.find("name", name) != null || bankDao.find("code", code) != null) {
                        countDuplicate++;
                        continue;
                    }
                    
                    if (bankMDao.find("name", name) != null || bankMDao.find("code", code) != null) {
                        countDuplicate++;
                        continue;
                    }

                    boolean result = map.entrySet().stream().anyMatch(x -> x.getKey().equalsIgnoreCase(data[0]) || x.getValue().equalsIgnoreCase(data[1]));

                    if (result == true) {
                        countDuplicate++;
                        continue;
                    }

                    map.put(data[0], data[1]);

                    BankM cnty = new BankM();
                    cnty.setName(data[0].replaceAll("\"", ""));
                    cnty.setCode(data[1].replaceAll("\"", ""));
                    cnty.setCreatedBy(user);
                    cnty.setCreatedOn(new Date());
                    cnty.setStatus(Status.PENDING);
                    
                    countUpload++;
                    list.add(cnty);
                }

                if(! list.isEmpty())
                    bankMDao.create(list);

                JsfUtil.addSuccessMessage(countUpload+" bank record(s) has been upload and submitted for approval, ("+ countDuplicate +") Duplicate found");
                
            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
            
            JsfUtil.addErrorMessage("unable to upload record please try again later");
        }

        return "upload";
    }
    
    public List<String> getBankNames(){
        
        try {
            List<String> banks = bankDao.findAll().stream()
                    .map(x -> x.getName()).collect(Collectors.toList());
            
            return banks;
        } catch (DatabaseException ex) {
            Logger.getLogger(BankController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return new ArrayList<>();
    }

    @FacesConverter(value = "bankConverter")
    public static class BankConverter implements javax.faces.convert.Converter {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {

            if (value == null || value.length() == 0) {
                return null;
            }

            BankController bankController = (BankController) context.getApplication()
                    .getELResolver().getValue(context.getELContext(), null, "bankController");

            return bankController.getBank(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            if (value == null) {
                return null;
            }
            if (value instanceof Bank) {
                Bank o = (Bank) value;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + value + " is of type " + value.getClass().getName() + "; expected type: " + Bank.class.getName());
            }
        }
    }

}

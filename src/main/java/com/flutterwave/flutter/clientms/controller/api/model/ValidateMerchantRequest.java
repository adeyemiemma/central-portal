/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author emmanueladeyemi
 */
public class ValidateMerchantRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the registrationCode
     */
    public String getRegistrationCode() {
        return registrationCode;
    }

    /**
     * @param registrationCode the registrationCode to set
     */
    public void setRegistrationCode(String registrationCode) {
        this.registrationCode = registrationCode;
    }
    
    @JsonProperty(value = "registerednumber")
    @NotBlank(message = "Registered number be provided")
    private String registrationCode;
//    @NotBlank(message = "Hash value be provided")
    private String hash = "";
}

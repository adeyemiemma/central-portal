/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.api.model.FetchRewardRequest;
import com.flutterwave.flutter.clientms.controller.api.model.RewardTransactionRequest;
import com.flutterwave.flutter.clientms.dao.ApiUserDao;
import com.flutterwave.flutter.clientms.model.ApiUser;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.RewardTransaction;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.RewardService;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.validation.Valid;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Path(value = "/v1")
@RequestScoped
@Produces(value = MediaType.APPLICATION_JSON)
public class RewardApiController {

    @EJB
    private RewardService rewardService;
    @EJB
    private ApiUserDao apiUserDao;
    @EJB
    private LogService logService;

    @Path(value = "transaction/fetch/all")
    @POST
    public Response getTransactions(@Valid FetchRewardRequest request, @HeaderParam(value = "uniqueid") String uniqueId) {

        Map<String, String> response = new HashMap<>();

        String title = "Fetching Transaction For Reward";

        try {

            Log log = new Log();
            log.setAction("Fetching Transaction For Reward");
            log.setDescription(request.toString());
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setUsername("API-" + uniqueId);

            logService.log(log);

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided in header");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            String requestString = apiUser.getPrivateKey() + "" + request.getSourceId();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            if ((request.getStartDate() != null && request.getEndDate() == null)) {

                response.put("responsecode", "failed");
                response.put("responsemessage", "Both start and end date must be provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            if ((request.getStartDate() == null && request.getEndDate() != null)) {

                response.put("responsecode", "failed");
                response.put("responsemessage", "Both start and end date must be provided");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            Date startDate, endDate;
            
            if (!(request.getStartDate() == null && request.getEndDate() == null)) {
                startDate = Utility.parseDate(request.getStartDate(), "yyyy-MM-dd");
                endDate = Utility.parseDate(request.getEndDate(), "yyyy-MM-dd");
            }else{
                
                startDate = endDate = new Date();
            }

            if (startDate == null || endDate == null) {

                response.put("responsecode", "failed");
                response.put("responsemessage", "Both start and end date be in format yyyy-MM-dd e.g 2017-01-20");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
             
            if(Utility.compare(startDate, endDate, true) > 0){
                
                response.put("responsecode", "failed");
                response.put("responsemessage", "start date must not be after enddate");

                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }

            String startDateString = Utility.formatDate(startDate, "yyyy-MMM-dd");
            String endDateString = Utility.formatDate(endDate, "yyyy-MMM-dd");

            JSONObject jSONObject = rewardService.getService(request.getSourceId(), startDateString, endDateString);

            return Response.status(Response.Status.OK).entity(jSONObject.toMap()).build();
        } catch (Exception ex) {
            Logger.getLogger(RewardApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.put("code", "RR");
        response.put("status", "Generic error");

        return returnResponse(Response.Status.INTERNAL_SERVER_ERROR, response, title, uniqueId);
    }
    
    @Path(value = "transaction/reward")
    @POST
    public Response rewardTransaction(@Valid RewardTransactionRequest request, @HeaderParam(value = "uniqueid") String uniqueId){
        
        Map<String, String> response = new HashMap<>();
        String title = "Rewaring Transaction";
        
        try {
            
            Log log = new Log();
            log.setAction(title);
            log.setDescription(request.toString());
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setUsername("API-" + uniqueId);
            
            logService.log(log);
            
            if (uniqueId == null) {
                
                response.put("responsecode", "03");
                response.put("responsemessage", "unique id not provided in header");
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);
            
            if (apiUser == null) {
                
                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            String requestString = apiUser.getPrivateKey() + "" + request.getTransactionReference();
            
            String hashString = Utility.sha512(requestString);
            
            if (!hashString.equalsIgnoreCase(request.getHash())) {
                
                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            RewardTransaction rewardTransaction = rewardService.getTransaction(request.getTransactionReference());
            
            if(rewardTransaction == null){
                response.put("responsecode", "06");
                response.put("responsemessage", "No transaction with id found");
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
            }
            
            if(rewardTransaction.getPointAddedOn() != null){
                response.put("responsecode", "07");
                response.put("responsemessage", "Transaction was rewarded on "+Utility.formatDate(rewardTransaction.getPointAddedOn(), "yyyy-MM-dd HH:mm"));
                
                return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
                
            }
            
            rewardTransaction.setPoint(request.getPoint());
            rewardTransaction.setPointAddedOn(new Date());
            
            rewardService.updateransaction(rewardTransaction);
            
            JSONObject responseObject = new JSONObject();
            responseObject.put("responsecode", "00")
                    .put("transactionreference", request.getTransactionReference())
                    .put("responsemessage", "Transaction has been rewarded successfully");
            
            
            return returnResponse(Response.Status.OK, responseObject.toMap(), title, uniqueId);
        } catch (Exception ex) {
            Logger.getLogger(RewardApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.put("code", "RR");
        response.put("status", "Generic error");

        return returnResponse(Response.Status.INTERNAL_SERVER_ERROR, response, title, uniqueId);
    }

    private Response returnResponse(Response.Status status, Object obj, String action, String uniqueId) {

        Log log = new Log();
        log.setAction(action);
        log.setCreatedOn(new Date());
        log.setDescription(obj.toString());
        log.setLevel(status == Response.Status.INTERNAL_SERVER_ERROR ? LogLevel.Severe : LogLevel.Info);
        log.setLogState(LogState.FINISH);
        log.setUsername("API KEY - " + uniqueId);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setLogDomain(LogDomain.API);

        logService.log(log);

        return Response.status(status).entity(obj).build();
    }
}

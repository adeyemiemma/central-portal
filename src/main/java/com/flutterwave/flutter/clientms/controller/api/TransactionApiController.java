/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.flutterwave.flutter.clientms.controller.api.model.DashboardGraph;
import com.flutterwave.flutter.clientms.dao.AirtimeTransactionDao;
import com.flutterwave.flutter.clientms.dao.ChargeBackDao;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CoreTransactionDao;
import com.flutterwave.flutter.clientms.dao.CurrencyDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.NipTransactionDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.RaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionWHDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.dao.TransactionNewDao;
import com.flutterwave.flutter.clientms.dao.TransactionWarehouseDao;
//import com.flutterwave.flutter.clientms.dao.TransactionNewDao;
//import com.flutterwave.flutter.clientms.dao.TransactionWarehouseDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.WalletDao;
import com.flutterwave.flutter.clientms.dao.WalletTransactionDao;
import com.flutterwave.flutter.clientms.model.AirtimeTransaction;
import com.flutterwave.flutter.clientms.model.CoreTransaction;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.GenericTransaction;
import com.flutterwave.flutter.clientms.model.MoneywaveTransaction;
import com.flutterwave.flutter.clientms.model.NipTransaction;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.RaveTransactionWH;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.TransactionListModel;
import com.flutterwave.flutter.clientms.model.TransactionNew;
import com.flutterwave.flutter.clientms.model.TransactionWarehouse;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.Wallet;
import com.flutterwave.flutter.clientms.model.WalletTransaction;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.model.products.MoneywaveMerchant;
import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.clientms.service.FlutterwaveApiService;
import com.flutterwave.flutter.clientms.service.TransactionUtil;
import com.flutterwave.flutter.clientms.util.CsvUtil;
import com.flutterwave.flutter.clientms.util.ExcelFileProcessor;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.MoneywaveReportBean;
import com.flutterwave.flutter.clientms.util.MoneywaveUtil;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.util.WalletTransactionType;
import com.flutterwave.flutter.clientms.viewmodel.CoreV2TransactionViewModel;
import com.flutterwave.flutter.clientms.viewmodel.TransViewModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import com.monitorjbl.xlsx.StreamingReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author emmanueladeyemi
 */
@SessionScoped
@Path(value = "/product/transaction")
public class TransactionApiController implements Serializable {

    @EJB
    private MoneywaveTransactionDao moneywaveTransactionDao;
    @EJB
    private RaveTransactionWHDao raveTransactionDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private CoreTransactionDao coreTransactionDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private TransactionDao transactionDao;
    @EJB
    private WalletDao walletDao;
    @EJB
    private WalletTransactionDao walletTransactionDao;
    @EJB
    private CurrencyDao currencyDao;
    @EJB
    private ChargeBackDao chargeBackDao;
    @EJB
    private MoneywaveMerchantDao moneywaveMerchantDao;
    @EJB
    private RaveMerchantDao raveMerchantDao;
    @Inject
    private Global global;
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private UserDao userDao;
    @EJB
    private AirtimeTransactionDao airtimeTransactionDao;
    @EJB
    private TransactionWarehouseDao transactionWarehouseDao;
    @EJB
    private TransactionNewDao transactionNewDao;
    @EJB
    private FlutterwaveApiService flutterwaveApiService;  
    @EJB
    private NipTransactionDao nipTransactionDao;
    @EJB
    private TransactionUtil transactionService;        
   
    
    String token_string = Global.TOKEN_STRING_C; // test_token for test and live_token for live
    
//    @ManagedProperty(value="#{moneywaveReport}")
    private MoneywaveReportBean moneywaveReportBean;      
    
//    List<String> moneywaveExceptions, bankExceptions;
    
    public MoneywaveReportBean getMoneywaveManageBean(){
        
        return this.moneywaveReportBean;
    }
    
    public void setMoneywaveReportBean(MoneywaveReportBean moneywaveReportBean){
        this.moneywaveReportBean = moneywaveReportBean;
    }
    
    @Path(value = "/list")
    @Produces(value = MediaType.APPLICATION_JSON)
    @GET
    public Response getTransaction(@QueryParam(value = "product") String product,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "transactionType") String transactionType,
            @QueryParam(value = "transactionStatus") String transactionStatus,
            @QueryParam(value = "cycle") String cycle,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "fraudStatus") String fraudStatus,
            @QueryParam(value = "chargeType") String chargeType,
            @QueryParam(value = "country") String country,
            @QueryParam(value = "merchant") String merchant,
            @QueryParam(value = "isMerchantId") Boolean isMerchantId,
            @QueryParam(value = "paymentEntity") String paymentEntity,
            @QueryParam(value = "destination") String destination,
            @QueryParam(value = "flutterResponse") String flutterResponse) {

        try {

            PageResult<GenericTransaction> pageResult = new PageResult<>();

            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }

            if (product == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<Product> products = productDao.findAll();

            if (products == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            if (products.isEmpty()) {

                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<GenericTransaction> transactions = new ArrayList<>();

            long totalCount = 0;

            if ("rave".equalsIgnoreCase(product)) {

                Page<RaveTransactionWH> raveTransaction = raveTransactionDao.search(start, length, startDate, endDate, "any".equalsIgnoreCase(transactionType) ? null : transactionType,
                        "any".equalsIgnoreCase(fraudStatus) ? null : fraudStatus, "any".equalsIgnoreCase(chargeType) ? null : chargeType,
                        "any".equalsIgnoreCase(cycle) ? null : cycle, "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity, "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus,
                        search, country, "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId,"any".equalsIgnoreCase(currency) ? null : currency);

                totalCount += raveTransaction.getCount();

                if (raveTransaction.getContent() != null && !raveTransaction.getContent().isEmpty()) {

                    transactions.addAll(raveTransaction.getContent().stream().map((RaveTransactionWH rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericTransaction>toList()));

                }
            } else if ("moneywave".equalsIgnoreCase(product)) {

                Page<MoneywaveTransaction> waveTransaction = moneywaveTransactionDao.getTransactions(start, length, startDate, endDate,
                        "any".equalsIgnoreCase(transactionType) ? null : transactionType, "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus,
                        "any".equalsIgnoreCase(destination) ? null : destination,
                        "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity, search, "any".equalsIgnoreCase(flutterResponse) ? null : flutterResponse,
                        "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency);

                totalCount += waveTransaction.getCount();

                if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                    transactions.addAll(waveTransaction.getContent().stream().map((MoneywaveTransaction rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericTransaction>toList()));
                }
            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                Page<CoreTransaction> waveTransaction = coreTransactionDao.getTransactions(start, length, startDate, endDate,
                        "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus, search, "".equalsIgnoreCase(merchant) ? null : merchant,isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency);

                totalCount += waveTransaction.getCount();

                if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                    transactions.addAll(waveTransaction.getContent().stream().map((CoreTransaction rm) -> {
                        
                        GenericTransaction gt = rm.getGeneric();
                        
                        try{
                            List<CoreMerchant> merchants = coreMerchantDao.find(""+token_string, gt.getMerchant());
                            
                            gt.setMerchant( merchants == null || merchants.isEmpty() ? null : merchants.get(0).getCompanyname());
                        }catch(Exception ex){
                            if(ex != null)
                                ex.printStackTrace();
                            
                            gt.setMerchant(null);
                        }
                        
                        return gt;
                    }).collect(Collectors.<GenericTransaction>toList()));
                }
            }else if ("corev2".equalsIgnoreCase(product) || "flutterwave corev2".equalsIgnoreCase(product)) {

                PageResult<CoreV2TransactionViewModel> result = flutterwaveApiService.getTransactions(merchant, 
                        startDate, endDate, (start % length), length, "any".equalsIgnoreCase(currency) ? null : currency, "any".equalsIgnoreCase(transactionStatus) ? "all" : transactionStatus);

                totalCount += result.getRecordsTotal();

                if (result.getData()!= null && !result.getData().isEmpty()) {

                    transactions.addAll(result.getData().stream().map((CoreV2TransactionViewModel rm) -> {
                        
                        GenericTransaction gt = rm.getGeneric();
                        
                        try{
                            List<CoreMerchant> merchants = coreMerchantDao.find(""+token_string, gt.getMerchant());
                            
                            gt.setMerchant( merchants == null || merchants.isEmpty() ? rm.getMerchantName() : merchants.get(0).getCompanyname());
                        }catch(Exception ex){
                            if(ex != null)
                                ex.printStackTrace();
                            
                            gt.setMerchant(null);
                        }
                        
                        return gt;
                    }).collect(Collectors.<GenericTransaction>toList()));
                }
            }
            

            pageResult = new PageResult(transactions, totalCount, totalCount);

            return Response.status(Response.Status.OK).entity(pageResult).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
    }
    
    
    @Path(value = "/list/v2")
    @Produces(value = MediaType.APPLICATION_JSON)
    @GET
    public Response getTransactionV2(@QueryParam(value = "product") String product,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "transactionType") String transactionType,
            @QueryParam(value = "transactionStatus") String transactionStatus,
            @QueryParam(value = "cycle") String cycle,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "fraudStatus") String fraudStatus,
            @QueryParam(value = "chargeType") String chargeType,
            @QueryParam(value = "country") String country,
            @QueryParam(value = "merchant") String merchant,
            @QueryParam(value = "isMerchantId") Boolean isMerchantId,
            @QueryParam(value = "paymentEntity") String paymentEntity,
            @QueryParam(value = "destination") String destination,
            @QueryParam(value = "flutterResponse") String flutterResponse) {

        try {

            PageResult<GenericTransaction> pageResult = new PageResult<>();

            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }

            if (product == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<Product> products = productDao.findAll();

            if (products == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            if (products.isEmpty()) {

                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<GenericTransaction> transactions = new ArrayList<>();

            long totalCount = 0;

            if ("rave".equalsIgnoreCase(product)) {

                Page<RaveTransactionWH> raveTransaction = raveTransactionDao.search(start, length, startDate, endDate, "any".equalsIgnoreCase(transactionType) ? null : transactionType,
                        "any".equalsIgnoreCase(fraudStatus) ? null : fraudStatus, "any".equalsIgnoreCase(chargeType) ? null : chargeType,
                        "any".equalsIgnoreCase(cycle) ? null : cycle, "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity, "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus,
                        search, country, "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId,"any".equalsIgnoreCase(currency) ? null : currency);

                totalCount += raveTransaction.getCount();

                if (raveTransaction.getContent() != null && !raveTransaction.getContent().isEmpty()) {

                    transactions.addAll(raveTransaction.getContent().stream().map((RaveTransactionWH rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericTransaction>toList()));

                }
            } else if ("moneywave".equalsIgnoreCase(product)) {

                Page<MoneywaveTransaction> waveTransaction = moneywaveTransactionDao.getTransactions(start, length, startDate, endDate,
                        "any".equalsIgnoreCase(transactionType) ? null : transactionType, "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus,
                        "any".equalsIgnoreCase(destination) ? null : destination,
                        "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity, search, "any".equalsIgnoreCase(flutterResponse) ? null : flutterResponse,
                        "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency);

                totalCount += waveTransaction.getCount();

                if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                    transactions.addAll(waveTransaction.getContent().stream().map((MoneywaveTransaction rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericTransaction>toList()));
                }
            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                Page<CoreTransaction> waveTransaction = coreTransactionDao.getTransactions(start, length, startDate, endDate,
                        "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus, search, "".equalsIgnoreCase(merchant) ? null : merchant,isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency);

                totalCount += waveTransaction.getCount();

                if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                    transactions.addAll(waveTransaction.getContent().stream().map((CoreTransaction rm) -> {
                        
                        GenericTransaction gt = rm.getGeneric();
                        
                        try{
                            List<CoreMerchant> merchants = coreMerchantDao.find(""+token_string, gt.getMerchant());
                            
                            gt.setMerchant( merchants == null || merchants.isEmpty() ? null : merchants.get(0).getCompanyname());
                        }catch(Exception ex){
                            if(ex != null)
                                ex.printStackTrace();
                            
                            gt.setMerchant(null);
                        }
                        
                        return gt;
                    }).collect(Collectors.<GenericTransaction>toList()));
                }
            }

            pageResult = new PageResult(transactions, totalCount, totalCount);

            return Response.status(Response.Status.OK).entity(pageResult).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
    }
    
    @Path(value = "/bank/list")
    @Produces(value = MediaType.APPLICATION_JSON)
    @GET
    public Response getBankTransaction(@QueryParam(value = "product") String product,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "transactionType") String transactionType,
            @QueryParam(value = "transactionStatus") String transactionStatus,
            @QueryParam(value = "cycle") String cycle,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "fraudStatus") String fraudStatus,
            @QueryParam(value = "chargeType") String chargeType,
            @QueryParam(value = "country") String country,
            @QueryParam(value = "merchant") String merchant,
            @QueryParam(value = "isMerchantId") Boolean isMerchantId,
            @QueryParam(value = "paymentEntity") String paymentEntity,
            @QueryParam(value = "destination") String destination,
            @QueryParam(value = "flutterResponse") String flutterResponse) {

        try {

            PageResult<GenericTransaction> pageResult = new PageResult<>();

            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }

            if (product == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<Product> products = productDao.findAll();

            if (products == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            if (products.isEmpty()) {

                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<GenericTransaction> transactions = new ArrayList<>();

            long totalCount = 0;

            if ("rave".equalsIgnoreCase(product)) {

                Session session =  SecurityUtils.getSubject().getSession();
                Object productIdObj = session.getAttribute("productId");


                String productId = productIdObj.toString();
                
                Page<RaveTransactionWH> raveTransaction = raveTransactionDao.searchBank(start, length, startDate, endDate, "any".equalsIgnoreCase(transactionType) ? null : transactionType,
                        "any".equalsIgnoreCase(fraudStatus) ? null : fraudStatus, "any".equalsIgnoreCase(chargeType) ? null : chargeType,
                        "any".equalsIgnoreCase(cycle) ? null : cycle, "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity, "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus,
                        search, country, "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId,"any".equalsIgnoreCase(currency) ? null : currency, productId);

                totalCount += raveTransaction.getCount();

                if (raveTransaction.getContent() != null && !raveTransaction.getContent().isEmpty()) {

                    transactions.addAll(raveTransaction.getContent().stream().map((RaveTransactionWH rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericTransaction>toList()));

                }
            } else if ("moneywave".equalsIgnoreCase(product)) {

                Page<MoneywaveTransaction> waveTransaction = moneywaveTransactionDao.getTransactions(start, length, startDate, endDate,
                        "any".equalsIgnoreCase(transactionType) ? null : transactionType, "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus,
                        "any".equalsIgnoreCase(destination) ? null : destination,
                        "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity, search, "any".equalsIgnoreCase(flutterResponse) ? null : flutterResponse,
                        "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency);

                totalCount += waveTransaction.getCount();

                if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                    transactions.addAll(waveTransaction.getContent().stream().map((MoneywaveTransaction rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericTransaction>toList()));
                }
            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                Page<CoreTransaction> waveTransaction = coreTransactionDao.getTransactions(start, length, startDate, endDate,
                        "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus, search, "".equalsIgnoreCase(merchant) ? null : merchant,isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency);

                totalCount += waveTransaction.getCount();

                if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                    transactions.addAll(waveTransaction.getContent().stream().map((CoreTransaction rm) -> {
                        
                        GenericTransaction gt = rm.getGeneric();
                        
                        try{
                            List<CoreMerchant> merchants = coreMerchantDao.find(""+token_string, gt.getMerchant());
                            
                            gt.setMerchant( merchants == null || merchants.isEmpty() ? null : merchants.get(0).getCompanyname());
                        }catch(Exception ex){
                            if(ex != null)
                                ex.printStackTrace();
                            
                            gt.setMerchant(null);
                        }
                        
                        return gt;
                    }).collect(Collectors.<GenericTransaction>toList()));
                }
            }

            pageResult = new PageResult(transactions, totalCount, totalCount);

            return Response.status(Response.Status.OK).entity(pageResult).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
    }
    
    @Path(value = "/nip")
    @Produces(value = MediaType.APPLICATION_JSON)
    @GET
    public Response getNIPTransactions(@QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "searchoption") String searchOption,
            @QueryParam(value = "status") String status,
            @QueryParam(value = "searchString") String search){
        
        
        PageResult<NipTransaction> pageResult = new PageResult<>();

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        String title = null;
        
        switch(searchOption){
            
            case "Originator": 
                title = "originator";
                break;
            case "AccountName" : 
                title = "accountName";
                break;
            case "AccountNo" : 
                title = "accountNo";
                break;    
            case "PaymentReference":
                title = "paymentReference";
                break;
            case "Narration":
                title = "narration";
                break;
            case "Bank":
                title = "bank";
                break;
            case "SessionId":
                title = "sessionId";
                break;    
        }
        
        Page<NipTransaction> page = nipTransactionDao.getTransactions(start, length, startDate, endDate, "any".equalsIgnoreCase(status) ? null : status, title, search, false);
        
        pageResult = new PageResult<>(page.getContent(), page.getCount(), page.getCount());

        return  Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path(value = "/list/status")
    @Produces(value = MediaType.APPLICATION_JSON)
    @GET
    public Response getTransactionByStatus(@QueryParam(value = "product") String product,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "transactionType") String transactionType,
            @QueryParam(value = "transactionStatus") String transactionStatus,
            @QueryParam(value = "cycle") String cycle,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "fraudStatus") String fraudStatus,
            @QueryParam(value = "chargeType") String chargeType,
            @QueryParam(value = "country") String country,
            @QueryParam(value = "merchant") String merchant,
            @QueryParam(value = "isMerchantId") Boolean isMerchantId,
            @QueryParam(value = "paymentEntity") String paymentEntity,
            @QueryParam(value = "destination") String destination,
            @QueryParam(value = "flutterResponse") String flutterResponse) {

        try {

            PageResult<GenericTransaction> pageResult = new PageResult<>();

            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }

            if (product == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<Product> products = productDao.findAll();

            if (products == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            if (products.isEmpty()) {

                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<GenericTransaction> transactions = new ArrayList<>();

            long totalCount = 0;

            if ("rave".equalsIgnoreCase(product)) {

                Page<RaveTransactionWH> raveTransaction = raveTransactionDao.searchByStatus(start, length, startDate, endDate, "any".equalsIgnoreCase(transactionType) ? null : transactionType,
                        "any".equalsIgnoreCase(fraudStatus) ? null : fraudStatus, "any".equalsIgnoreCase(chargeType) ? null : chargeType,
                        "any".equalsIgnoreCase(cycle) ? null : cycle, "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity,
                        search, country, null , false,"any".equalsIgnoreCase(currency) ? null : currency, !"successful".equalsIgnoreCase(transactionStatus) );

                totalCount += raveTransaction.getCount();

                if (raveTransaction.getContent() != null && !raveTransaction.getContent().isEmpty()) {

                    transactions.addAll(raveTransaction.getContent().stream().map((RaveTransactionWH rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericTransaction>toList()));

                }
            } else if ("moneywave".equalsIgnoreCase(product)) {

                Page<MoneywaveTransaction> waveTransaction = moneywaveTransactionDao.getTransactionsByStatus(start, length, startDate, endDate,
                        "any".equalsIgnoreCase(transactionType) ? null : transactionType,
                        "any".equalsIgnoreCase(destination) ? null : destination,
                        "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity, search, "any".equalsIgnoreCase(flutterResponse) ? null : flutterResponse,
                        "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency,
                        !"successful".equalsIgnoreCase(transactionStatus));

                totalCount += waveTransaction.getCount();

                if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                    transactions.addAll(waveTransaction.getContent().stream().map((MoneywaveTransaction rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericTransaction>toList()));
                }
            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                Page<CoreTransaction> waveTransaction = coreTransactionDao.getTransactionsByStatus(start, length, startDate, endDate,
                         search, "".equalsIgnoreCase(merchant) ? null : merchant, "any".equalsIgnoreCase(currency) ? null : currency, !"successful".equalsIgnoreCase(transactionStatus));

                totalCount += waveTransaction.getCount();

                if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                    transactions.addAll(waveTransaction.getContent().stream().map((CoreTransaction rm) -> {
                        
                        GenericTransaction gt = rm.getGeneric();
                        
                        try{
                            List<CoreMerchant> merchants = coreMerchantDao.find(token_string, gt.getMerchant());
                            
                            gt.setMerchant( merchants == null || merchants.isEmpty() ? null : merchants.get(0).getCompanyname());
                        }catch(Exception ex){
                            if(ex != null)
                                ex.printStackTrace();
                            
                            gt.setMerchant(null);
                        }
                        
                        return gt;
                    }).collect(Collectors.<GenericTransaction>toList()));
                }
            }

            pageResult = new PageResult(transactions, totalCount, totalCount);

            return Response.status(Response.Status.OK).entity(pageResult).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>(new ArrayList(), 0L, 0L)).build();
    }

    @Path(value = "/status")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getStatus(@QueryParam(value = "product") String product) {

        String[] array = null;
        if ("moneywave".equalsIgnoreCase(product)) {
            array = new String[]{"pending", "processing", "retrying", "completed", "failed", "escalate"};
        } else if ("rave".equalsIgnoreCase(product)) {
            array = raveTransactionDao.getStatus().toArray(new String[]{});
        } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {
            array = coreTransactionDao.getResponse().toArray(new String[]{});
        }else if ("corev2".equalsIgnoreCase(product) || "flutterwave corev2".equalsIgnoreCase(product)) {
            array = new String[]{"Success", "Failed", "Pending"};
        }

        return Response.status(Response.Status.OK).entity(array).build();
    }

    @Path(value = "/fresponses")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getFlutterResponses() {

        String[] array = moneywaveTransactionDao.getFlutterResponse().toArray(new String[]{});

        return Response.status(Response.Status.OK).entity(array).build();
    }
            
    @Path(value = "/disbursestatus")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON) 
    public Response getDisburseResponses(){
        
        Map<String, String> array = transactionDao.getDisburseResponse();
        
        return Response.status(Response.Status.OK).entity(array).build();
    }
    
    
    @Path(value = "/cresponse")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON) 
    public Response getCoreResponses(@QueryParam(value = "category") String category){
        
        Map<String, String> array = transactionService.getCoreResponses(category);
        
        return Response.status(Response.Status.OK).entity(array).build();
    }
    
    @Path(value = "/rave/paymenttype")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON) 
    public Response getRaveTypes(){
        
        Map<String, String> array = raveTransactionDao.getPaymentType();
        
        return Response.status(Response.Status.OK).entity(array).build();
    }
    
    @Path(value = "/rave/currency")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON) 
    public Response getRaveCurrencies(){
        
        Map<String, String> array = raveTransactionDao.getPaymentCurrencies();
        
        return Response.status(Response.Status.OK).entity(array).build();
    }
    
    
    @Path(value = "/airtime/cresponse")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON) 
    public Response getAirtimeResponses(){
        
        Map<String, String> array = airtimeTransactionDao.getResponse(null);
        
        return Response.status(Response.Status.OK).entity(array).build();
    }
    
    @Path(value = "/airtime/transres")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON) 
    public Response getAirtimeTransactionStatus(){
        
        Map<String, String> array = airtimeTransactionDao.getTransactionResponse();
        
        return Response.status(Response.Status.OK).entity(array).build();
    }
    
    @Path(value = "/airtime/revres")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON) 
    public Response getAirtimeReverseTransactionStatus(){
        
        Map<String, String> array = airtimeTransactionDao.getReverseTransactionResponse();
        
        return Response.status(Response.Status.OK).entity(array).build();
    }
    
    @Path(value = "/types")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON) 
    public Response getCoreTransactiontypes(@QueryParam(value = "category") String category){
        
        List<String> array = transactionDao.getTransactionType("any".equalsIgnoreCase(category) ? null : category.toLowerCase());

        if(array != null){
            //filter(x -> !x.toLowerCase().contains("refund"))
            array = array.stream().collect(Collectors.toList());
        }
        
        return Response.status(Response.Status.OK).entity(array).build();
    }
    
    @Path(value = "/dashboard/graph")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getDashboardSummaryGraph(@QueryParam(value = "product") String product,
            @QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency,
            @QueryParam(value = "merchant") String merchantId) throws DatabaseException {

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

        DashboardGraph dashboardGraph = new DashboardGraph();
        List summaryList = new ArrayList();
        List statusSummary = new ArrayList();

        String merchant = null;
        if ("moneywave".equalsIgnoreCase(product)) {
            
            
            if(merchantId != null){
                MoneywaveMerchant moneywaveMerchant = moneywaveMerchantDao.find(Long.parseLong(merchantId));
                
                if(moneywaveMerchant != null)
                    merchant = moneywaveMerchant.getName();
            }
            
            summaryList = moneywaveTransactionDao.getSummaryWithDate(startDate, endDate, currency, merchant);
//            summaryList = moneywaveTransactionDao.getTotalSummary(startDate, endDate, currency, null);
            statusSummary = moneywaveTransactionDao.getSummaryStatus(startDate, endDate, currency, merchant);
        } else if ("rave".equalsIgnoreCase(product)) {
            
            if(merchantId != null){
                RaveMerchant moneywaveMerchant = raveMerchantDao.find(Long.parseLong(merchantId));
                
                if(moneywaveMerchant != null)
                    merchant = moneywaveMerchant.getBusinessName();
            }
            
            summaryList = raveTransactionDao.getSummaryWithDate(startDate, endDate, currency, merchant);
            statusSummary = raveTransactionDao.getSummaryByStatus(startDate, endDate, currency, merchant);
            
            
        } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {
            
            if(merchantId != null){
                CoreMerchant moneywaveMerchant = coreMerchantDao.find(Long.parseLong(merchantId));
                
                if(moneywaveMerchant != null)
                    merchant = moneywaveMerchant.getCompanyname();
            }
            
            summaryList = coreTransactionDao.getSummaryWithDate(startDate, endDate, currency, merchant);
            statusSummary = coreTransactionDao.getSummaryStatus(startDate, endDate, currency, merchant);
        }

        Map<String, Double> map;
        Map<Date, Object> params = new TreeMap<>();
        Map<String, String> statusCodeMap = new HashMap<>();        

        double totalSum = 0, totalFee = 0, totalSuccessful = 0;
        long totalVolume = 0L;

        for (Object obj : summaryList) {

            Object[] list = (Object[]) obj;

            Double amount;
            try {
                amount = (Double) list[0];
            } catch (Exception ex) {
                amount = ((BigDecimal) list[0]) == null ? 0.0 : ((BigDecimal) list[0]).doubleValue();
            }
            Double fee;

            try {
                fee = (Double) list[1];
            } catch (Exception ex) {
                fee = ((BigInteger) list[1]) == null ? 0.0 : ((BigInteger) list[1]).doubleValue();
            }

            BigInteger count = (BigInteger) list[2];
            Date date = (Date) list[3];
            String response = (String) list[4];

            String status = "successful";

            if (response == null || !(response.toLowerCase().contains("success") || response.toLowerCase().contains("approved"))
                    && !"successful".equalsIgnoreCase(response) && !"completed".equalsIgnoreCase(response)) {
                status = "failed";
            } else {
                totalSuccessful += amount == null ? 0 : amount;
            }

            map = (HashMap) params.getOrDefault(date, new HashMap<>());

            double value = map.getOrDefault(status, 0.0);

            value = value + (amount == null ? 0 : amount.longValue());

            totalSum += amount == null ? 0 : amount;
            totalFee += fee == null ? 0 : fee;
            totalVolume += (count == null ? 0 : count.longValue());

            map.put(status, value);

            params.put(date, map);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        Date tempDate = startDate;

        while (tempDate.before(endDate) || tempDate.equals(endDate)) {

            Map<String, Double> m = (HashMap<String, Double>) params.getOrDefault(tempDate, null);

            if (m == null) {
                m = new HashMap<>();
                m.put("successful", 0.0);
                m.put("failed", 0.0);
            } else {

                Double d = m.getOrDefault("successful", null);

                if (d == null) {
                    m.put("successful", 0.0);
                }

                d = m.getOrDefault("failed", null);

                if (d == null) {
                    m.put("failed", 0.0);
                }
            }

            params.put(tempDate, m);

            calendar.add(Calendar.DAY_OF_MONTH, 1);
            tempDate = calendar.getTime();
        }

        Map<String, Double> statusValue = new HashMap<>();

        for (Object obj : statusSummary) {
            Object[] list = (Object[]) obj;

            Double amount;

            try {
                amount = (Double) list[0];
            } catch (Exception ex) {
                amount = ((BigDecimal) list[0]) == null ? 0.0 : ((BigDecimal) list[0]).doubleValue();
            }
            String status = (String) list[1];
            
            if(list.length > 2){
                statusCodeMap.put(status, (String)list[2]);
            }

            statusValue.put(status, amount);
        }

        dashboardGraph.setDateData(params);
        dashboardGraph.setTotalFee(totalFee);
        dashboardGraph.setTotalSum(totalSum);
        dashboardGraph.setTotalSuccessful(totalSuccessful);
        dashboardGraph.setTotalVolume(totalVolume);
        dashboardGraph.setStatusData(statusValue);
        dashboardGraph.setStatusCodeMapper(statusCodeMap);

        return Response.status(Response.Status.OK).entity(dashboardGraph).build();
    }

    @Path(value = "/dashboard/summary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getDashboardSummary(@QueryParam(value = "product") String product,
            @QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency) {

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

        List summaryList;

        summaryList = moneywaveTransactionDao.getTotalSummary(startDate, endDate, currency, null);

        Map<String, Double> map;
        Map<String, Object> data = new HashMap<>();

        for (Object obj : summaryList) {

            Object[] list = (Object[]) obj;

            Double amount = (Double) list[0];
//            Double fee = (Double) list[1];
            String response = (String) list[2];

            String status = "successful";

            if (response == null || !(response.toLowerCase().contains("success") || response.toLowerCase().contains("approved"))
                    && !"successful".equalsIgnoreCase(response) && !"completed".equalsIgnoreCase(response)) {
                status = "failed";
            }

            map = (HashMap) data.getOrDefault("moneywave", new HashMap<>());

            double value = map.getOrDefault(status, 0.0);

            value = value + (amount == null ? 0 : amount);

            map.put(status, value);

            data.put("moneywave", map);
        }

        map = (Map) data.getOrDefault("moneywave", null);

        if (map == null) {
            map = new HashMap<>();
            map.put("successful", 0.0);
            map.put("failed", 0.0);
        } else {

            Double d = map.getOrDefault("successful", null);

            if (d == null) {
                map.put("successful", 0.0);
            }

            d = map.getOrDefault("failed", null);

            if (d == null) {
                map.put("failed", 0.0);
            }
        }

        data.put("moneywave", map);

        summaryList = raveTransactionDao.getTotalSummary(startDate, endDate, currency, null);

        for (Object obj : summaryList) {

            Object[] list = (Object[]) obj;

            Double amount = (Double) list[0];
//            Double fee = (Double) list[1];
            String response = (String) list[2];

            String status = "successful";

            if (!"successful".equalsIgnoreCase(response) && !"completed".equalsIgnoreCase(response)) {
                status = "failed";
            }

            map = (HashMap) data.getOrDefault("rave", new HashMap<>());

            double value = map.getOrDefault(status, 0.0);

            value = value + (amount == null ? 0 : amount);

            map.put(status, value);

            data.put("rave", map);
        }

        map = (Map) data.getOrDefault("rave", null);

        if (map == null) {
            map = new HashMap<>();
            map.put("successful", 0.0);
            map.put("failed", 0.0);
        } else {

            Double d = map.getOrDefault("successful", null);

            if (d == null) {
                map.put("successful", 0.0);
            }

            d = map.getOrDefault("failed", null);

            if (d == null) {
                map.put("failed", 0.0);
            }
        }

        data.put("rave", map);

        summaryList = coreTransactionDao.getTotalSummary(startDate, endDate, currency, null);

        analyseData(summaryList, "core", data);

        map = (Map) data.getOrDefault("core", null);

        if (map == null) {
            map = new HashMap<>();
            map.put("successful", 0.0);
            map.put("failed", 0.0);
        } else {

            Double d = map.getOrDefault("successful", null);

            if (d == null) {
                map.put("successful", 0.0);
            }

            d = map.getOrDefault("failed", null);

            if (d == null) {
                map.put("failed", 0.0);
            }
        }

        data.put("core", map);

        return Response.status(Response.Status.OK).entity(data).build();
    }

    private Map<String, Object> analyseData(List summaryList, String name, Map<String, Object> data) {

        for (Object obj : summaryList) {

            Object[] list = (Object[]) obj;

            Double amount = ((BigDecimal) list[0]) == null ? 0.0 : ((BigDecimal) list[0]).doubleValue();
//            Double fee = (Double) list[1];
            String response = (String) list[2];

            String status = "successful";

            if (response == null || !(response.toLowerCase().contains("success") || response.toLowerCase().contains("approved")) && !"successful".equalsIgnoreCase(response) && !"completed".equalsIgnoreCase(response)) {
                status = "failed";
            }

            Map<String, Double> map = (HashMap) data.getOrDefault(name, new HashMap<>());

            double value = map.getOrDefault(status, 0.0);

            value = value + (amount == null ? 0 : amount);

            map.put(status, value);

            data.put(name, map);
        }

        return data;
    }

    @Path(value = "/report")
    @POST
    @Consumes("multipart/form-data")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getReport(@Valid  MultipartFormDataInput request) throws IOException {

        Date startDate = null, endDate = null;

//        String range = request.getRange();

        Map<String, InputPart> map = request.getFormData();
        
        String range = map.get("range").getBodyAsString();
        String sourceType = map.get("sourceType").getBodyAsString();
        String currency = map.get("currency").getBodyAsString();
        String format = map.get("format").getBodyAsString();
        
        byte[] fileSource = map.get("file").getBody(byte[].class, null);
        byte[] cardSource = map.get("card").getBody(byte[].class, null);
        byte[] walletSource = map.get("wallet").getBody(byte[].class, null);
        
        MoneywaveReportBean reportBean = getMoneywaveManageBean();
        
        if(reportBean == null)
            reportBean = new MoneywaveReportBean();

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        InputStream inputStream = null;
        
        Map<String, Double> bankExceptions, moneywaveExceptions;
        
        Map<String, Double> allBankReference, allMoneywaveReference;

        try {
            inputStream = new ByteArrayInputStream(fileSource);

//            OPCPackage container;
            Workbook workbook = StreamingReader.builder()
                    .rowCacheSize(100)
                    .bufferSize(4096)
                    .open(inputStream);

            Map<String, Object> result = new LinkedHashMap<>();

//            // This assumes the sheet is first sheet
            Sheet sheet = workbook.getSheetAt(0);
//        
            
            Map<String, Object> value = new HashMap<>();
                    
            if(format.equalsIgnoreCase("format2"))
                value = ExcelFileProcessor.processFileFormat2(sheet.rowIterator(), startDate, endDate, currency );
            else
                value = ExcelFileProcessor.processFile(sheet.rowIterator(), startDate, endDate, currency );
//            

            allBankReference = (HashMap)value.getOrDefault("Reference", new HashMap<>());
            
            System.out.println(value);

            Map<String, Object> queryAmount = new LinkedHashMap<>();

            Long moneywaneyTransactionCount = 0L;
            
            Long bankTotal = 0L;
            
            double disburseCard = 0.0;

            allMoneywaveReference = new HashMap<>();
            
            // This get card funding transactions from moneywave
            if (Utility.ReportSource.valueOf(sourceType) == Utility.ReportSource.Database) {
                
                List card = moneywaveTransactionDao.getReportSummary(startDate, startDate, currency, null,null);

                if (!card.isEmpty()) {

                    Object[] tempData = (Object[]) card.toArray(new Object[]{});

                    Object[] valueData = (Object[]) tempData[0];

                    Double cardAmount = (Double) (valueData[0]);

                    queryAmount.put("Moneywave Portal Value", cardAmount == null ? 0.0 : cardAmount);
                } else {
                    queryAmount.put("Moneywave Portal Value", 0.0);
                }
                
            } else {

                inputStream = new ByteArrayInputStream(cardSource);

                Map<String, Object> card = null;
                
                
                try{
                    card = CsvUtil.processMoneywaveFileCard(inputStream, startDate, null, (Map<String, Double>)value.get("Reference"));
                }catch(Exception ex){
                    
                    inputStream = new ByteArrayInputStream(cardSource);
                    
                     workbook = StreamingReader.builder()
                        .rowCacheSize(100)
                        .bufferSize(4096)
                        .open(inputStream);

                //            // This assumes the sheet is first sheet
                    sheet = workbook.getSheetAt(0);

                    card = ExcelFileProcessor.processMoneywaveFileCard(sheet.rowIterator(), startDate, null, (Map<String, Double>)value.get("Reference"));
                }
                

                if (card != null && !card.isEmpty()) {

                    queryAmount.put("Moneywave Portal Value", (Double) card.getOrDefault("Total", 0.0));
                    disburseCard += (Double) card.getOrDefault("TotalAccount", 0.0);
                    moneywaneyTransactionCount += (Long)card.getOrDefault("Found", 0L);
                    allMoneywaveReference.putAll((HashMap)card.getOrDefault("Reference", new ArrayList<>()));
                }
            }

                    
            // This is called to pull card transaction from core
            List data = coreTransactionDao.getReportSummary(startDate, startDate, currency, "Thrive", MoneywaveUtil.ReportType.Card);
            
            if (!data.isEmpty()) {
                Double cardAmount = ((BigDecimal) ((Object[]) data.toArray(new Object[]{}))[0]).doubleValue();
                queryAmount.put("Settlement Portal Value", cardAmount);
            } else {
                queryAmount.put("Settlement Portal Value", 0.0);
            }

            queryAmount.put("Difference", (Double)queryAmount.get("Moneywave Portal Value") - (Double)queryAmount.get("Settlement Portal Value"));
            queryAmount.put("Exception", "");
            result.put("Moneywave Card Transactions", queryAmount);

            // end of processing money transactions from core
            
            queryAmount = new LinkedHashMap<>();

            if (Utility.ReportSource.valueOf(sourceType) == Utility.ReportSource.Database) {
                
                List disburse = moneywaveTransactionDao.getReportSummary(startDate, startDate, currency, null, MoneywaveUtil.ReportType.Disbursement);

                if (!disburse.isEmpty()) {
                    Object[] tempData = (Object[]) disburse.toArray(new Object[]{});

                    Object[] valueData = (Object[]) tempData[0];

                    Double disburseAmount = (Double) (valueData[0]);

                    queryAmount.put("Disbursements", disburseAmount);
                    moneywaneyTransactionCount += ((BigInteger) (valueData[1])) == null ? 0 : ((BigInteger) (valueData[1])).longValue();
                } else {
                    queryAmount.put("Disbursements", 0.0);
                }
                
            }
            else{
                inputStream = new ByteArrayInputStream(walletSource);

                Map<String, Object> wallet = null;
                
                try{
                    wallet = CsvUtil.processMoneywaveFile(inputStream, startDate, null, (Map<String, Double>)value.get("Reference"));
                }catch(Exception ex){
                    
                    inputStream = new ByteArrayInputStream(walletSource);
                     workbook = StreamingReader.builder()
                        .rowCacheSize(100)
                        .bufferSize(4096)
                        .open(inputStream);

                //            // This assumes the sheet is first sheet
                    sheet = workbook.getSheetAt(0);

                    wallet = ExcelFileProcessor.processMoneywaveFile(sheet.rowIterator(), startDate, null, (Map<String, Double>)value.get("Reference"));
                }
                
//                workbook = StreamingReader.builder()
//                        .rowCacheSize(100)
//                        .bufferSize(4096)
//                        .open(inputStream);
//
//                //            // This assumes the sheet is first sheet
//                sheet = workbook.getSheetAt(0);
//
//                Map<String, Object> wallet = ExcelFileProcessor.processMoneywaveFile(sheet.rowIterator(), startDate, null, (Map<String, Double>)value.get("Reference"));

                if (wallet != null && !wallet.isEmpty()) {

                    queryAmount.put("Disbursements", (Double) wallet.getOrDefault("Total", 0.0) + disburseCard);
                    moneywaneyTransactionCount += Long.parseLong(""+(Integer)wallet.getOrDefault("Found", 0));
                    
                    allMoneywaveReference.putAll((HashMap)wallet.getOrDefault("Reference", new HashMap<>()));
                    
                }
            }

            queryAmount.put("Debits on Bank Account", (Double) value.getOrDefault("Total", 0.0));

            queryAmount.put("Difference", (Double)queryAmount.get("Disbursements") - (Double)queryAmount.get("Debits on Bank Account"));
            
            Integer totalFound = ((HashMap<String, Double>)value.get("Reference")).size();
            
            long diff = totalFound - moneywaneyTransactionCount;
            
            if(diff > 0)
                queryAmount.put("Exception", "There are "+Math.abs(diff)+" more transactions in bank statement than money wave report");
            else
                queryAmount.put("Exception", "There are "+Math.abs(diff)+" more transactions in moneywave report than bank statement");

//            queryAmount.put("Exception", );
            result.put("Moneywave Disbursements", queryAmount);

            queryAmount = new LinkedHashMap<>();

//            Double disburseAmount = (Double)((Object[])card.toArray(new Object[]{}))[0];
            queryAmount.put("Earned", moneywaneyTransactionCount * 42.75);

            queryAmount.put("Collected/Verified", (Double) value.getOrDefault("Fee", 0.0));

            double difference = (Double)queryAmount.get("Earned") - (Double)queryAmount.get("Collected/Verified");
            queryAmount.put("Difference", difference);
            
            if(difference > 0)
                queryAmount.put("Exception", "There are "+ (int)(Math.abs(difference)/ 42.75)+" less transaction in bank statement at N42.75");
            else
                queryAmount.put("Exception", "There are "+ (int)(Math.abs(difference)/ 42.75)+" more transaction in bank statement at N42.75");

            result.put("Moneywave Disbursement Income", queryAmount);
            
            // Those found in moneywave but not in bank
            bankExceptions = allMoneywaveReference.entrySet().stream()
                    .filter(x -> !allBankReference.keySet().contains(x.getKey()))
                    .collect(Collectors.toMap(y -> y.getKey() , y-> y.getValue()));

            // Those found in bank but not in moneywave 
            moneywaveExceptions =  allBankReference.entrySet().stream()
                    .filter(x -> !allMoneywaveReference.keySet().contains(x.getKey()))
                    .collect(Collectors.toMap(y->y.getKey(), y-> y.getValue()));
            
            reportBean.setMoneywaveExceptions(moneywaveExceptions);
            reportBean.setBankExceptions(bankExceptions);
            
            setMoneywaveReportBean(reportBean);
            
            global.setMoneywaveReportBean(reportBean);

//            HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//            
//            httpSession.setAttribute("values", reportBean);
            
            return Response.status(Response.Status.OK).entity(result).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        } finally {

            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(TransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        // get the amount for transaction
//        List cardReport = moneywaveTransactionDao.getReportSummary(startDate, endDate, currency, null, MoneywaveUtil.ReportType.Card);
//        
//        List disburseReport = moneywaveTransactionDao.getReportSummary(startDate, endDate, currency, null, MoneywaveUtil.ReportType.Disbursement);

        return null;

    }
    
    @Path(value = "/redisburse")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response doDisbursementRetry(@QueryParam(value = "reference") String reference){
        
        Map<String, String> response = new HashMap<>();
        
        try {
            
            Transaction transaction = transactionDao.findByKey("flwTxnReference", reference);
            
            if(transaction == null){
                response.put("statusCode", "01");
                response.put("status", "transaction with reference is not found");
                
                return Response.status(Response.Status.OK).entity(response).build();
            }
            

            JsonObject responseObject = httpUtil.doRetryDisburse(transaction.getMerchantTxnReference());
            
            if(responseObject == null){
                
                response.put("statusCode", "02");
                response.put("status", "Unable to process transaction please try again later");
                
                return Response.status(Response.Status.OK).entity(response).build();
            }
            
//            {"responsemessage":"Successful","responsecode":"00","uniquereference":"TTMW000476040","internalreference":"04448199957"}
            String responseMessage = responseObject.getString("responsemessage", "");
            String responseCode = responseObject.getString("responsecode", "");
            String internalReference = responseObject.getString("internalreference", "");
            
            response.put("statusCode", responseCode);
            response.put("status", responseMessage + " with internal reference "+internalReference);
            
            transaction.setDisburseRetryCount(transaction.getDisburseRetryCount()+1);
            transaction.setLastRetried(new Date());

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");
            
            transaction.setLastRetriedBy(user);            
            
//            if("00".equalsIgnoreCase(responseCode)){
                
            transaction.setDisburseResponseCode(responseCode);
            transaction.setDisburseResponseMessage(responseMessage);
            
            String transactionData = transaction.getTransactionData();
            
            JSONObject jsonObject = new JSONObject()
                            .put("name", "internalreference")
                            .put("value", internalReference == null ? "" : internalReference);
            
            if(transactionData != null){
                
                try{
                    
//                    JsonValue jsonValue = (JsonValue) jsonObject;

                    JSONArray jSONArray = new JSONArray(transactionData);
                    jSONArray.put(jsonObject);
//                    
//                    JsonArray array = Json.createReader(new StringReader(transactionData)).readArray();
//                    JsonArrayBuilder arrayBuilder = Json.createArrayBuilder()
//                            .add(array)
//                            .add
                    
                    transaction.setTransactionData(jSONArray.toString());
                    
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                
            }else{
                
//                JsonArray array = Json.createArrayBuilder()
//                        .add(jsonObject)
//                        .build();
                
                    JSONArray jSONArray = new JSONArray();
                    jSONArray.put(jsonObject);
                    
                    transaction.setTransactionData(jSONArray.toString());
                    
//                transaction.setTransactionData(array.toString());
            }
                
            
            transactionDao.update(transaction);
            
            updateTransaction(transaction, internalReference);
            
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(TransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.put("statusCode", "RR");
        response.put("status", "Generic Error");
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @Path(value = "/disburse/status")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response doDisbursementStatus(@QueryParam(value = "reference") String reference){
        
        Map<String, Object> response = new HashMap<>();
        
        try {
            
            Transaction transaction = transactionDao.findByKey("flwTxnReference", reference);
            
            if(transaction == null){
                response.put("status", "failed");
                response.put("description", "transaction with reference is not found");
                
                return Response.status(Response.Status.OK).entity(response).build();
            }
            
            JsonObject responseObject = httpUtil.doDisburseStatus(transaction.getMerchantTxnReference());
            
            if(responseObject == null){
                
                response.put("status", "failed");
                response.put("description", "Unable to process transaction please try again later");
                
                return Response.status(Response.Status.OK).entity(response).build();
            }
            
//            {"responsemessage":"Successful","responsecode":"00","uniquereference":"TTMW000476040","internalreference":"04448199957"}
//            String responseMessage = responseObject.getString("responsemessage", "");
//            String responseCode = responseObject.getString("responsecode", "");
//            String presponseCode = responseObject.getString("postresponsecode", "");
            
//            String presponseMessage = responseObject.getString("postresponsecode", "");
//            String internalReference = responseObject.getString("internalreference", "");
            
            response.put("status", "success");
            response.put("description", "successful");
            
            Map<String, String> responseMap = new HashMap<>();
            responseMap.put("Response Code", responseObject.getString("postresponsecode", "RR"));
            responseMap.put("Response Message", responseObject.getString("postresponsemessage", "None"));
            responseMap.put("Internal Reference", responseObject.getString("internalreference", "None"));
            responseMap.put("Reference", responseObject.getString("uniquereference", "None"));
            
            response.put("data", responseMap);
            
//            transaction.setDisburseRetryCount(transaction.getDisburseRetryCount()+1);
//            transaction.setLastRetried(new Date());

//            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");
            
            
//            String transactionData = transaction.getTransactionData();
            
//            JSONObject jsonObject = new JSONObject()
//                            .put("name", "internalreference")
//                            .put("value", internalReference == null ? "" : internalReference);
            
//            if(transactionData != null){
//                
//                try{
//                    
////                    JsonValue jsonValue = (JsonValue) jsonObject;
//
//                    JSONArray jSONArray = new JSONArray(transactionData);
//                    jSONArray.put(jsonObject);
////                    
////                    JsonArray array = Json.createReader(new StringReader(transactionData)).readArray();
////                    JsonArrayBuilder arrayBuilder = Json.createArrayBuilder()
////                            .add(array)
////                            .add
//                    
//                    transaction.setTransactionData(jSONArray.toString());
//                    
//                }catch(Exception ex){
//                    ex.printStackTrace();
//                }
//                
//            }else{
//                
////                JsonArray array = Json.createArrayBuilder()
////                        .add(jsonObject)
////                        .build();
//                
//                    JSONArray jSONArray = new JSONArray();
//                    jSONArray.put(jsonObject);
//                    
//                    transaction.setTransactionData(jSONArray.toString());
//                    
////                transaction.setTransactionData(array.toString());
//            }
                
            
//            transactionDao.update(transaction);
            
//            updateTransaction(transaction, internalReference);
            
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(TransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.put("status", "RR");
        response.put("description", "Generic Error");
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    
    @Path(value = "/disburse/direct/status")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Response doDisbursementStatusDirect(@QueryParam(value = "reference") String reference){
        
        Map<String, Object> response = new HashMap<>();
        
        try {
            
            JsonObject responseObject = httpUtil.doDisburseStatus(reference);
            
            if(responseObject == null){
                
                response.put("status", "failed");
                response.put("description", "Unable to process transaction please try again later");
                
                return Response.status(Response.Status.OK).entity(response).build();
            }
            
            response.put("status", "success");
            response.put("description", "successful");
            
            Map<String, String> responseMap = new HashMap<>();
            responseMap.put("Response Code", responseObject.getString("postresponsecode", "RR"));
            responseMap.put("Response Message", responseObject.getString("postresponsemessage", "None"));
            responseMap.put("Internal Reference", responseObject.getString("internalreference", "None"));
            responseMap.put("Reference", responseObject.getString("uniquereference", "None"));
            
            response.put("data", responseMap);
            
            
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (Exception ex) {
            Logger.getLogger(TransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.put("status", "RR");
        response.put("description", "Generic Error");
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void updateTransaction(Transaction transaction, String internalRef){
        
        try {
            TransactionNew transactionNew = transactionNewDao.findByKey("flwTxnReference", transaction.getFlwTxnReference());
            
            if(transactionNew != null){
                transactionNew.setInternalReference(internalRef);
                transactionNew.setDisburseResponseCode(transaction.getDisburseResponseCode());
                transactionNew.setDisburseResponseMessage(transaction.getDisburseResponseMessage());
                transactionNew.setDisburseRetryCount(transaction.getDisburseRetryCount());
                transactionNew.setModifiedOn(new Date());
                
                transactionNewDao.update(transactionNew);
            }
            
            TransactionWarehouse transactionWarehouse = transactionWarehouseDao.findByKey("flwTxnReference", transaction.getFlwTxnReference());
            
            if(transactionWarehouse != null){
                transactionWarehouse.setInternalReference(internalRef);
                transactionWarehouse.setDisburseResponseCode(transaction.getDisburseResponseCode());
                transactionWarehouse.setDisburseResponseMessage(transaction.getDisburseResponseMessage());
                transactionWarehouse.setDisburseRetryCount(transaction.getDisburseRetryCount());
                transactionWarehouse.setModifiedOn(new Date());
                transactionNewDao.update(transactionNew);
                
            }
        } catch (DatabaseException ex) {
            Logger.getLogger(TransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void parseExcel(File file) throws IOException {

        OPCPackage container;
        try {
            container = OPCPackage.open(file.getAbsolutePath());
            ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(container);
            XSSFReader xssfReader = new XSSFReader(container);
            StylesTable styles = xssfReader.getStylesTable();
            XSSFReader.SheetIterator iter = (XSSFReader.SheetIterator) xssfReader.getSheetsData();

            XSSFWorkbook xssfWb = new XSSFWorkbook(container);
            SXSSFWorkbook wb = new SXSSFWorkbook(xssfWb, 100);

        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (OpenXML4JException e) {
            e.printStackTrace();
        }
    }

    protected void processSheet(StylesTable styles, ReadOnlySharedStringsTable strings, InputStream sheetInputStream) throws IOException, SAXException {

        org.xml.sax.InputSource sheetSource = new org.xml.sax.InputSource(sheetInputStream);
        SAXParserFactory saxFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxFactory.newSAXParser();
            XMLReader sheetParser = saxParser.getXMLReader();
            ContentHandler handler = new XSSFSheetXMLHandler(styles, strings, new SheetContentsHandler() {
                @Override
                public void startRow(int i) {

                    System.out.println(i);
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void endRow(int i) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void cell(String cellReference, String formattedValue, XSSFComment xssfc) {

                    System.out.println(formattedValue);

                }

                @Override
                public void headerFooter(String string, boolean bln, String string1) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

            },
                    false//means result instead of formula
            );
            sheetParser.setContentHandler(new SimpleHandler());
            sheetParser.parse(sheetSource);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
        }
    }

    class SimpleHandler extends DefaultHandler {

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            super.characters(ch, start, length); //To change body of generated methods, choose Tools | Templates.

            System.out.println(new String(ch));
        }

    }
    
    
    @Path(value = "/flutterwave")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getFlutterwaveTransaction(@QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "merchant") String merchantId,
            @QueryParam(value = "cardScheme") String cardScheme,
            @QueryParam(value = "type") String transactionType,
            @QueryParam(value = "status") String status,
            @QueryParam(value = "searchoption") String searchOption,
            @QueryParam(value = "category") String category,
            @QueryParam(value = "disbursecode") String disbursementCode,
            @QueryParam(value = "currency") String currency){
        
        PageResult<TransViewModel> pageResult = new PageResult<>();

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        String title = null;
        
        switch(searchOption){
            
            case "rrn": 
                title = "rrn";
                break;
            case "merchantRef" : 
                title = "merchantTxnReference";
                break;
            case "flwRef":
                title = "flwTxnReference";
                break;
            case "masked":
                title = "cardMask";
                break;
        }
        
        Page<TransactionListModel> page = transactionWarehouseDao.getTransactionsList(start, length, startDate, endDate, title,"".equalsIgnoreCase(search) ? null : search,
                "any".equalsIgnoreCase(currency) ? null : currency, "".equalsIgnoreCase(provider) ? null : provider , "".equalsIgnoreCase(merchantId) ? null : merchantId, 
                "any".equalsIgnoreCase(category) ? null : category,"any".equalsIgnoreCase(status) ? null : status, "any".equalsIgnoreCase(transactionType) ? null : transactionType,
                "".equals(cardScheme) ? null : cardScheme, "any".equalsIgnoreCase(disbursementCode) ? null : disbursementCode, false, null);
        
        
        List<TransactionListModel> content = page.getContent();
        
        List<TransViewModel> models = new ArrayList<>();
        
        if(content != null && !content.isEmpty()){
            
            models = content.stream().map((TransactionListModel x) -> {
                
                TransViewModel model = new TransViewModel();                
                
                String prov = x.getProvider();
               
                String mid = x.getMerchantId();
                
                if(prov != null && prov.toUpperCase().startsWith("MCASH")){
                    
                    String processReference =  x.getProcessorReference();
                    
                    if(processReference != null){
                        
                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();
                        
                        String merchantCode = Utility.getData(array, "merchantcode");
//                        String merchantName = jsonObject.getString("merchantname", status)
                        x.setMerchantId(x.getMerchantId() +" ("+merchantCode+") ");
                        
                    } 
                }
                
                model.setTransaction(x);
                
                try{
                    
                    if(mid != null){
                        String merchantName = coreMerchantDao.getMerchantName(mid);
                        model.setMerchantName(merchantName);
                    }
                }catch(Exception ex){
//                    ex.printStackTrace();
                }
                
                return model;
            }).collect(Collectors.<TransViewModel>toList());
            
        }
        
        pageResult.setData(models);
        pageResult.setDraw(draw);
        pageResult.setRecordsFiltered(page.getCount());
        pageResult.setRecordsTotal(page.getCount());
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    
    @Path(value = "/flutterwave/airtime")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getFlutterwaveAirtimeTransaction(@QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "status") String status,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "reverseResponseCode") String reverseResponseCode,
            @QueryParam(value = "transactResultResponseCode") String transactResultResponseCode){
        
        PageResult<AirtimeTransaction> pageResult = new PageResult<>();

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
            
        Page<AirtimeTransaction> page = airtimeTransactionDao.getAirtimeTransactions(start, length, startDate, endDate, "".equalsIgnoreCase(search) ? null : search,
                "any".equalsIgnoreCase(currency) ? null : currency, null , null, "any".equalsIgnoreCase(status) ? null : status,
                null,null,  "any".equalsIgnoreCase(transactResultResponseCode) ? null : transactResultResponseCode , 
                "any".equalsIgnoreCase(reverseResponseCode) ? null : reverseResponseCode,  null);
        
        
        List<AirtimeTransaction> content = page.getContent();
        
//        List<AirtimeTransaction> models = c
        
//        if(content != null && !content.isEmpty()){
//            
//            models = content.parallelStream().map((AirtimeTransaction x) -> {
//                
//                TransViewModel model = new TransViewModel();
//                model.setTransaction(x);
//                x.setLastRetriedBy(null);
//                try{
//                   String merchantName = coreMerchantDao.getMerchantName(x.getMerchantId());
//                   
//                   model.setMerchantName(merchantName);
//                }catch(Exception ex){
////                    ex.printStackTrace();
//                }
//                
//                return model;
//            }).collect(Collectors.<TransViewModel>toList());
//            
//        }
        
        pageResult.setData(content);
        pageResult.setDraw(draw);
        pageResult.setRecordsFiltered(page.getCount());
        pageResult.setRecordsTotal(page.getCount());
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path(value = "/refund")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getRefundTransaction(@QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "merchant") String merchantId,
            @QueryParam(value = "status") String status,
            @QueryParam(value = "currency") String currency){
        
        PageResult<Transaction> pageResult = new PageResult<>();

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
            
        Page<Transaction> page = transactionDao.getTransactions(start, length, startDate, endDate, "".equalsIgnoreCase(search) ? null : search,
                "any".equalsIgnoreCase(currency) ? null : currency, "".equalsIgnoreCase(provider) ? null : provider , 
                "".equalsIgnoreCase(merchantId) ? null : merchantId, null, 
                "any".equalsIgnoreCase(status) ? null : status, null, null, null, true);
        
        pageResult.setData(page.getContent());
        pageResult.setDraw(draw);
        pageResult.setRecordsFiltered(page.getCount());
        pageResult.setRecordsTotal(page.getCount());
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path(value = "/refund/summary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getRefundSummary(@QueryParam(value = "range") String range,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "product") String product,
            @QueryParam(value = "merchantid") String merchantId,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "currency") String currency){
        
        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        Map refunds = transactionDao.getTransactionSummary(startDate, endDate, "".equalsIgnoreCase(search) ? null : search,
                "any".equalsIgnoreCase(currency) ? null : currency, provider , merchantId, true);
        
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        startDate = calendar.getTime();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        endDate = calendar.getTime();
        
        Map monthRefunds = transactionDao.getTransactionSummary(startDate, endDate, "".equalsIgnoreCase(search) ? null : search,
                "any".equalsIgnoreCase(currency) ? null : currency, provider , merchantId,  true);
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM, yyyy");
        
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date sTime = calendar.getTime();
        
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date eTime = calendar.getTime();
        
        Map todayRefunds = transactionDao.getTransactionSummary(sTime, eTime, "".equalsIgnoreCase(search) ? null : search,
                "any".equalsIgnoreCase(currency) ? null : currency, provider , merchantId,  true);
        
        Map<String, Object> map = new HashMap<>();
        map.put("total", refunds);
        map.put("monthly", monthRefunds);
        map.put("today", todayRefunds);
        map.put("month", dateFormat.format(new Date()));
        
        return Response.status(Response.Status.OK).entity(map).build();
    }
    
    @Path(value = "/reference")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getTransaction(@QueryParam(value = "reference") long reference){
        
        
        try {
            TransactionWarehouse transaction = transactionWarehouseDao.find(reference);
            
            return Response.status(Response.Status.OK).entity(transaction).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(TransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.OK).entity(new Transaction()).build();
    }
    
    @Path(value = "/nip/reference")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getNIPTransaction(@QueryParam(value = "reference") long reference){
        
        
        try {
            NipTransaction transaction = nipTransactionDao.find(reference);
            
            return Response.status(Response.Status.OK).entity(transaction).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(TransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.OK).entity(new NipTransaction()).build();
    }
    
    @Path(value = "/airtime/reference")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAirtimeTransaction(@QueryParam(value = "reference") long reference){
        
        
        try {
            AirtimeTransaction transaction = airtimeTransactionDao.find(reference);
            
            return Response.status(Response.Status.OK).entity(transaction).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(TransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.OK).entity(new Transaction()).build();
    }
    
    @Path(value = "/airtime/summary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAirtimeSummary(@QueryParam(value = "daterange") String range){
        
        
        List successful = airtimeTransactionDao.getTotalSummary(null, null, true);
        
        Map<String, Object> response = new HashMap<>();
        
        Map<String, Object> data = new HashMap<>();
        
        Object[] successData = (Object[])successful.toArray();
         
        data.put("value",(Double) successData[0]);
        data.put("volume",((BigInteger) successData[0]).longValue());
        
        response.put("success", data);
        
        List failed = airtimeTransactionDao.getTotalSummary(null, null, false);
        
        Object[] failedData = (Object[])failed.toArray();
        
        data.put("value",(Double) failedData[0]);
        data.put("volume",((BigInteger) failedData[0]).longValue());
        
        response.put("failed", data);
        
        List all = airtimeTransactionDao.getTotalSummary(null, null, null);
        
        Object[] allData = (Object[])failed.toArray();
        
        data.put("value",(Double) allData[0]);
        data.put("volume",((BigInteger) allData[0]).longValue());
        
        response.put("total", data);
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @Path(value = "/wallet")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getWalletTransactions(@QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "product") String product,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "transactionType") String transactionType,
            @QueryParam(value = "merchantid") String merchantId){
    
        PageResult<WalletTransaction> pageResult = new PageResult<>();
        
        try {
            Date startDate = null, endDate = null;
            if (range != null && !range.equalsIgnoreCase("")) {
                
                try {
                    String[] splitDate = range.split("-");
                    
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    
                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }
            }   Product p = null;
            if(product != null)
                p = productDao.findByKey("name", product);
            Currency c = null;
            if(currency != null){
                c = currencyDao.findByKey("shortName", currency);
            }   Wallet wallet = walletDao.findByQuery(p, range, Utility.WalletCategory.SOURCE, c);
            Page<WalletTransaction> transaction = walletTransactionDao.findByQuery(start, length, startDate, endDate, wallet, WalletTransactionType.valueOf(transactionType));
            pageResult = new PageResult(transaction.getContent(), transaction.getCount(), transaction.getCount());
            return Response.status(Response.Status.OK).entity(pageResult).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(TransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path(value = "/provider/graph")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getTransactonSummary(@QueryParam(value = "provider") String provider,
            @QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency,
            @QueryParam(value = "merchant") String merchant) throws DatabaseException{
        
        
        Date startDate = null, endDate = null;
        
        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        Map<String, Object> result = new HashMap<>();
        
        Map refunds = transactionDao.getTransactionSummary(startDate, endDate, null,
                "any".equalsIgnoreCase(currency) ? null : currency, null , merchant, true);
        
        result.put("refund", 0.0);
        
        List summaryList = new ArrayList();
        
        summaryList =  transactionDao.getSummaryWithDate(startDate, endDate, currency, provider);
        
        Map<String, Double> map;
        Map<Date, Object> params = new TreeMap<>();
        Map<String, String> statusCodeMap = new HashMap<>();        

        double totalSum = 0, totalFee = 0, totalSuccessful = 0;
        long totalVolume = 0L;

        for (Object obj : summaryList) {

            Object[] list = (Object[]) obj;

            Double amount;
            try {
                amount = (Double) list[0];
            } catch (Exception ex) {
                amount = ((BigDecimal) list[0]) == null ? 0.0 : ((BigDecimal) list[0]).doubleValue();
            }
            Double fee;

            try {
                fee = (Double) list[1];
            } catch (Exception ex) {
                fee = ((BigInteger) list[1]) == null ? 0.0 : ((BigInteger) list[1]).doubleValue();
            }

            BigInteger count = (BigInteger) list[2];
            Date date = (Date) list[3];
            String response = (String) list[4];

            String status = "successful";

            if (response == null || !"00".equalsIgnoreCase(response)) {
                status = "failed";
            } else {
                totalSuccessful += amount == null ? 0 : amount;
            }

            map = (HashMap) params.getOrDefault(date, new HashMap<>());

            double value = map.getOrDefault(status, 0.0);

            value = value + (amount == null ? 0 : amount.longValue());

            totalSum += amount == null ? 0 : amount;
            totalFee += fee == null ? 0 : fee;
            totalVolume += (count == null ? 0 : count.longValue());

            map.put(status, value);

            params.put(date, map);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        Date tempDate = startDate;

        while (tempDate.before(endDate) || tempDate.equals(endDate)) {

            Map<String, Double> m = (HashMap<String, Double>) params.getOrDefault(tempDate, null);

            if (m == null) {
                m = new HashMap<>();
                m.put("successful", 0.0);
                m.put("failed", 0.0);
            } else {

                Double d = m.getOrDefault("successful", null);

                if (d == null) {
                    m.put("successful", 0.0);
                }

                d = m.getOrDefault("failed", null);

                if (d == null) {
                    m.put("failed", 0.0);
                }
            }

            params.put(tempDate, m);

            calendar.add(Calendar.DAY_OF_MONTH, 1);
            tempDate = calendar.getTime();
        }
        
        DashboardGraph dashboardGraph = new DashboardGraph();
        
        dashboardGraph.setDateData(params);
        dashboardGraph.setTotalFee(totalFee);
        dashboardGraph.setTotalSum(totalSum);
        dashboardGraph.setTotalSuccessful(totalSuccessful);
        dashboardGraph.setTotalVolume(totalVolume);
        dashboardGraph.setStatusCodeMapper(statusCodeMap);
        
        return Response.status(Response.Status.OK).entity(dashboardGraph).build();
    }
    
    @Path(value = "/info")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getTransactionById(@QueryParam(value = "id") long id,
            @QueryParam(value = "product") String product, @QueryParam(value = "reference") String reference) {

        try {
            if (id <= 0) {
                return null;
            }

            if ("rave".equalsIgnoreCase(product)) {

                RaveTransactionWH transaction = raveTransactionDao.find(id);

                return Response.status(Response.Status.OK).entity(transaction).build();

            } else if ("moneywave".equalsIgnoreCase(product)) {

                MoneywaveTransaction transaction = moneywaveTransactionDao.find(id);

                return Response.status(Response.Status.OK).entity(transaction).build();

            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                CoreTransaction transaction = coreTransactionDao.find(id);

                return Response.status(Response.Status.OK).entity(transaction).build();
            }else if ("core v2".equalsIgnoreCase(product) || "flutterwave corev2".equalsIgnoreCase(product)) {

                CoreV2TransactionViewModel model = flutterwaveApiService.getTransaction(reference);

                return Response.status(Response.Status.OK).entity(model).build();
            }

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).build();
    }
    
    @Path(value = "/merchant/summary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantReportSummary(@QueryParam("product") String product , @QueryParam("merchant") String merchant, 
            @QueryParam("currency") String currency, @QueryParam(value = "range") String range) throws DatabaseException {

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        List data = null;
        if ("rave".equalsIgnoreCase(product)) {
            
            data = raveTransactionDao.getTotalSummaryNoGrouping(startDate, endDate, currency, merchant);
                
        } else if ("moneywave".equalsIgnoreCase(product)) {
            
            data = moneywaveTransactionDao.getTotalSummaryNoGrouping(startDate, endDate, currency, merchant);
            
        } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product) ){
            
            data = coreTransactionDao.getTotalSummaryNoGrouping(startDate, endDate, currency, merchant);
            
        }
        
        Map<String, Object> result = new HashMap<>();
        
        result.put("totalvalue", data == null ? 0 : data.get(0));
        
        return Response.status(Response.Status.OK).entity(result).build();
    }
    
//    @Path(value = "/categories")
//    @GET
//    @Produces(value = MediaType.APPLICATION_JSON)
//    public Response getResponsesByCategory(@QueryParam(value = "category") String category){
//        
//        Response.status(Response.Status.OK).entity(array).build();
//        
//        return null;
//    }
//    
    @Path(value = "/upload/status")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getUploadStatus(){
        
        Map<String, String> response = new HashMap<>();
        
        if(global.getFuture() == null){
            response.put("responsecode", "01");
           response.put("responsemessage", "Uploading Pending");
           
           return Response.status(Response.Status.OK).entity(response).build();
        }
        
        boolean done = global.getFuture().isDone();
        
        if(done == false){
            
            if(global.getFuture().isCancelled()){
                
                response.put("responsecode", "99");
                response.put("responsemessage", "Upload was aborted");
            }else{
                
                response.put("responsecode", "01");
                response.put("responsemessage", "Uploading Pending");
            }
            
        }else{
            
            response.put("responsecode", "00");
            response.put("responsemessage", "successful");
        }
                 
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.util.Utility;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class LogPosTransactionRequest {

    /**
     * @return the paymentReference
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * @param paymentReference the paymentReference to set
     */
    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    /**
     * @return the fee
     */
    public String getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(String fee) {
        this.fee = fee;
    }

    /**
     * @return the chName
     */
    public String getChName() {
        return chName;
    }

    /**
     * @param chName the chName to set
     */
    public void setChName(String chName) {
        this.chName = chName;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * @param currencyCode the currencyCode to set
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * @return the refCode
     */
    public String getRefCode() {
        return refCode;
    }

    /**
     * @param refCode the refCode to set
     */
    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the requestDate
     */
    public String getRequestDate() {
        return requestDate;
    }

    /**
     * @param requestDate the requestDate to set
     */
    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    /**
     * @return the responseDate
     */
    public String getResponseDate() {
        return responseDate;
    }

    /**
     * @param responseDate the responseDate to set
     */
    public void setResponseDate(String responseDate) {
        this.responseDate = responseDate;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the scheme
     */
    public String getScheme() {
        return scheme;
    }

    /**
     * @param scheme the scheme to set
     */
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customerPhone
     */
    public String getCustomerPhone() {
        return customerPhone;
    }

    /**
     * @param customerPhone the customerPhone to set
     */
    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the posId
     */
    public String getPosId() {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(String posId) {
        this.posId = posId;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    

    @JsonProperty(value = "terminalid")
    private String terminalId;
    @NotBlank(message = "Pan must be provided")
    private String pan;
    @NotBlank(message = "Response code must be provided")
    @JsonProperty(value = "responsecode")
    private String responseCode;
    @JsonProperty(value = "responsemessage")
    @NotBlank(message = "Response Message must be provided")
    private String responseMessage;
    @JsonProperty(value = "currencycode")
    @NotBlank(message = "Currency Code must be provided")
    private String currencyCode;
    @JsonProperty(value = "refcode")
    @NotBlank(message = "Ref code must be provided")
    private String refCode;
    private double amount;
    private String type;
    @NotBlank(message = "Request date must be provided")
    @JsonProperty(value = "requestdate")
    private String requestDate;
    @NotBlank(message = "Response date must be provided")
    @JsonProperty(value = "responsedate")
    private String responseDate;
    @NotBlank(message = "Status must be provided")
    private String status;
    private String scheme;
    @JsonProperty(value = "customername")
    private String customerName;
    @JsonProperty(value = "customerphone")
    private String customerPhone;
    @NotBlank(message = "RRN must be provided")
    private String rrn;
//    @NotBlank(message = "POS ID must be provided")
    @JsonProperty(value = "posid")
    private String posId;
    @NotBlank(message = "Source must be provided")
    private String source;
    @JsonProperty(value = "id")
    private String fileId;
    private String hash;
    @JsonProperty(value = "cardholder_name")
    private String chName;
    private String fee;
    @JsonProperty(value = "paymentreference")
    private String paymentReference;
    
    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject(this);
//        jSONObject.put("terminalid", Utility.nullToEmpty(terminalId))
//                .put("pan", Utility.nullToEmpty(pan))
//                .put("responseCode", Utility.nullToEmpty(responseCode))
//                .put("responseMessage", Utility.nullToEmpty(responseMessage))
//                .put("currencyCode", Utility.nullToEmpty(currencyCode))
//                .put("amount", amount)
//                .put("refcode", Utility.nullToEmpty(refCode))
//                .put("type", Utility.nullToEmpty(type))
//                .put("requestDate", Utility.nullToEmpty(requestDate))
//                .put("responseDate", Utility.nullToEmpty(responseDate))
//                .put("status", Utility.nullToEmpty(status))
//                .put("scheme", Utility.nullToEmpty(scheme))
//                .put("customerName", Utility.nullToEmpty(customerName))
//                .put("customerPhone", Utility.nullToEmpty(customerPhone))
//                .put("rrn", Utility.nullToEmpty(rrn))
//                .put("posId", Utility.nullToEmpty(posId))
//                .put("source", Utility.nullToEmpty(source))
//                .put("fileId", Utility.nullToEmpty(fileId))
//                .put("cardholder_name", Utility.nullToEmpty(chName))
//                .put("hash", Utility.nullToEmpty(getHash()));
        
        return jSONObject.toString();
    }
    
    public String toHashableString(){
        
        StringBuilder builder = new StringBuilder();
        builder.append(posId)
                .append(terminalId);
        
        return builder.toString();
    }
    
}

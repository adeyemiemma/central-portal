/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class PaymentRefundRequest {

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the paymentReference
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * @param paymentReference the paymentReference to set
     */
    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    /**
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * @return the refundAmount
     */
    public String getRefundAmount() {
        return refundAmount;
    }

    /**
     * @param refundAmount the refundAmount to set
     */
    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    /**
     * @return the totalAmount
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
    
    @NotBlank(message = "Merchant name must be provided")
    private String merchant;
    @NotBlank(message = "Payment Reference must be provided")
    @JsonProperty(value = "payment_reference")
    private String paymentReference;
    @NotBlank(message = "Transaction date must be provided")
    @JsonProperty(value = "transaction_date")
    private String transactionDate;
    @JsonProperty(value = "refund_amount")
    @Pattern(regexp =  "(\\d+\\.\\d+)", message = "Amount can only be a decimal number")
    private String refundAmount;
    @Pattern(regexp =  "(\\d+\\.\\d+)", message = "Amount can only be a decimal number")
    @NotBlank(message = "Total amount must be provided")
    @JsonProperty(value = "total_amount")
    private String totalAmount;
    private String currency = "NGN";
    @NotBlank(message = "Hash value must be provided")
    private String hash;

    @Override
    public String toString() {
        
        return new JSONObject(this).toString();
    }
    
}
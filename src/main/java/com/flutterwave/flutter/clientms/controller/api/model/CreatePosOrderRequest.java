/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreatePosOrderRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the customerOrderId
     */
    public String getCustomerOrderId() {
        return customerOrderId;
    }

    /**
     * @param customerOrderId the customerOrderId to set
     */
    public void setCustomerOrderId(String customerOrderId) {
        this.customerOrderId = customerOrderId;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the posOrderId
     */
    public String getPosOrderId() {
        return posOrderId;
    }

    /**
     * @param posOrderId the posOrderId to set
     */
    public void setPosOrderId(String posOrderId) {
        this.posOrderId = posOrderId;
    }
    
    @NotBlank(message = "Customer order id must be provided")
    @JsonProperty(value = "customerorderid")
    private String customerOrderId;
    @NotBlank(message = "Merchant code must be provided")
    @JsonProperty(value = "merchantcode")
    private String merchantCode;
    @NotBlank(message = "Merchant code must be provided")
    @JsonProperty(value = "posorderid")
    private String posOrderId;
    @NotBlank(message = "Hash must be provided")
    private String hash;

    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject()
                .put("customerorderid", getCustomerOrderId())
                .put("merchantcode", getMerchantCode())
                .put("posorderid", getPosOrderId())
                .put("hash", getHash());
        
        return jSONObject.toString();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.dao.PosProviderDao;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import com.flutterwave.flutter.clientms.dao.PosTerminalDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosMerchantModel;
import com.flutterwave.flutter.clientms.model.PosProvider;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.service.ETopPosService;
import com.flutterwave.flutter.clientms.service.UtilityService;
import com.flutterwave.flutter.clientms.util.CsvUtil;
import com.flutterwave.flutter.clientms.util.FillManager;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.Layouter1;
import com.flutterwave.flutter.clientms.util.PdfUtilExport;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.MposTerminalViewModel;
import com.flutterwave.flutter.clientms.viewmodel.PosProviderViewModel;
import com.flutterwave.flutter.clientms.viewmodel.RegisterTerminalModel;
import com.flutterwave.flutter.clientms.viewmodel.UploadModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.stream.Collectors;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.util.IOUtils;
import org.apache.shiro.SecurityUtils;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "posController")
@SessionScoped
public class PosController implements Serializable {

    @EJB
    private PosTerminalDao posTerminalDao;
    @EJB
    private PosMerchantDao posMerchantDao;
    @EJB
    private PosProviderDao providerDao;
    @EJB
    private UserDao userDao;
    @EJB
    private UtilityService utilityService;
    @EJB
    private ETopPosService eTopPosService;
    @EJB
    private PosTransactionDao posTransactionDao;

    private RegisterTerminalModel registerTerminalModel;
    private PosProviderViewModel posProviderViewModel;
    private PosMerchantModel posMerchantModel;

    private static final Logger LOGGER = Logger.getLogger(PosController.class);

    /**
     * @return the posMerchantModel
     */
    public PosMerchantModel getPosMerchantModel() {

        if (posMerchantModel == null) {
            posMerchantModel = new PosMerchantModel();
        }

        return posMerchantModel;
    }

    /**
     * @param posMerchantModel the posMerchantModel to set
     */
    public void setPosMerchantModel(PosMerchantModel posMerchantModel) {
        this.posMerchantModel = posMerchantModel;
    }

    /**
     * @return the posProviderViewModel
     */
    public PosProviderViewModel getPosProviderViewModel() {

        if (posProviderViewModel == null) {
            posProviderViewModel = new PosProviderViewModel();
        }

        return posProviderViewModel;
    }

    /**
     * @param posProviderViewModel the posProviderViewModel to set
     */
    public void setPosProviderViewModel(PosProviderViewModel posProviderViewModel) {
        this.posProviderViewModel = posProviderViewModel;
    }

    /**
     * @return the registerTerminalModel
     */
    public RegisterTerminalModel getRegisterTerminalModel() {

        if (registerTerminalModel == null) {
            registerTerminalModel = new RegisterTerminalModel();
        }

        return registerTerminalModel;
    }

    /**
     * @param registerTerminalModel the registerTerminalModel to set
     */
    public void setRegisterTerminalModel(RegisterTerminalModel registerTerminalModel) {
        this.registerTerminalModel = registerTerminalModel;
    }

    public void prepareCreate() {

        registerTerminalModel = new RegisterTerminalModel();
    }

    public void prepareCreateMerchant() {

        posMerchantModel = new PosMerchantModel();
    }

    public String createMerchant() {

        try {

            String code = utilityService.generateMerchantCode();

            PosMerchant merchant = posMerchantDao.findByKey("merchantCode", code);

            while (merchant != null) {

                code = utilityService.generateMerchantCode();

                merchant = posMerchantDao.findByKey("merchantCode", code);
            }

            merchant = new PosMerchant();
            merchant.setAddress(posMerchantModel.getAddress());
            merchant.setCreatedOn(new Date());
            merchant.setEmail(posMerchantModel.getEmail());
            merchant.setMerchantCode(code);
            merchant.setMerchantId(posMerchantModel.getMerchantId());
            merchant.setName(posMerchantModel.getName());
            merchant.setPhone(posMerchantModel.getPhone());
            merchant.setCreatedBy(SecurityUtils.getSubject().getPrincipal().toString());
            merchant.setAccountName(posMerchantModel.getAccountName());
            merchant.setAccountNo(posMerchantModel.getAccountNo());
            merchant.setBankName(posMerchantModel.getBankName());
            merchant.setFeeCap(posMerchantModel.getFeeCap());
            merchant.setFlatFee(posMerchantModel.getFlatFee());
            merchant.setFeeType(posMerchantModel.getFeeType());
            merchant.setPercentageFee(posMerchantModel.getPercentageFee());
            merchant.setRelationshipMan(posMerchantModel.getRelationshipMan());

            if (posMerchantModel.getParentMerchantCode() != null && !"".equals(posMerchantModel.getParentMerchantCode())) {
                PosMerchant posMerchant = posMerchantDao.findByKey("merchantCode", posMerchantModel.getParentMerchantCode());
                merchant.setParent(posMerchant);
            }

            String posId = eTopPosService.createMerchant(code, merchant.getName());

            if (posId == null) {
                JsfUtil.addErrorMessage("Unable to add merchant, please try again later");
                return "";
            }

            merchant.setPosId(posId);
            merchant = posMerchantDao.create(merchant);

            if (merchant == null) {

                JsfUtil.addErrorMessage("Unable to add merchant, please try again later");
                return "";
            }

            JsfUtil.addSuccessMessage("Merchant has been created successfully with POS ID " + posId);

            posMerchantModel = new PosMerchantModel();

            return "/mpos/merchant/list?faces-redirect=true";
        } catch (DatabaseException ex) {
            Logger.getLogger(PosController.class.getName()).error(null, ex);
        }

        JsfUtil.addErrorMessage("Unable to add merchant, please try again later");

        return "";
    }

    public String prepareMerchantUpdate(long id) {

        PosMerchant merchant = null;

        try {
            merchant = posMerchantDao.find(id);
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (merchant == null) {

            JsfUtil.addErrorMessage("Merchant not found");
            return "/mpos/merchant/list?faces-redirect=true";
        }

        posMerchantModel = PosMerchantModel.from(merchant);

        return "/mpos/merchant/edit?faces-redirect=true";
    }

    public String updateMerchant() {

        try {

            //String code = utilityService.generateMerchantCode();
            PosMerchant merchant = posMerchantDao.find(posMerchantModel.getId());

            if (merchant == null) {
                JsfUtil.addErrorMessage("Merchant record not found");
                return "";
            }

            merchant.setAddress(posMerchantModel.getAddress());
            merchant.setModifiedOn(new Date());
            merchant.setEmail(posMerchantModel.getEmail());
            merchant.setMerchantId(posMerchantModel.getMerchantId());
            merchant.setName(posMerchantModel.getName());
            merchant.setPhone(posMerchantModel.getPhone());
            merchant.setCreatedBy(SecurityUtils.getSubject().getPrincipal().toString());
            merchant.setAccountName(posMerchantModel.getAccountName());
            merchant.setAccountNo(posMerchantModel.getAccountNo());
            merchant.setBankName(posMerchantModel.getBankName());
            merchant.setFeeCap(posMerchantModel.getFeeCap());
            merchant.setFlatFee(posMerchantModel.getFlatFee());
            merchant.setFeeType(posMerchantModel.getFeeType());
            merchant.setPercentageFee(posMerchantModel.getPercentageFee());
            merchant.setRelationshipMan(posMerchantModel.getRelationshipMan());

            if (posMerchantModel.getParentMerchantCode() != null && !"".equals(posMerchantModel.getParentMerchantCode())) {
                PosMerchant posMerchant = posMerchantDao.findByKey("merchantCode", posMerchantModel.getParentMerchantCode());
                merchant.setParent(posMerchant);
            }

            boolean state = eTopPosService.updateMerchant(merchant.getMerchantCode(), merchant.getName(), merchant.getPosId());

            if (state == false) {
                JsfUtil.addErrorMessage("Unable to update merchant, please try again later");
                return "";
            }

            boolean status = posMerchantDao.update(merchant);

            if (status == false) {

                JsfUtil.addErrorMessage("Unable to update merchant, please try again later");
                return "";
            }

            JsfUtil.addSuccessMessage("Merchant has been updated successfully");

            posMerchantModel = new PosMerchantModel();

            return "/mpos/merchant/list?faces-redirect=true";
        } catch (DatabaseException ex) {
            Logger.getLogger(PosController.class.getName()).error(null, ex);
        }

        JsfUtil.addErrorMessage("Unable to update merchant, please try again later");

        return "";

    }

    public String createTerminal() {

        try {
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", registerTerminalModel.getTerminalId());

            if (posTerminal != null) {

                JsfUtil.addErrorMessage("Terminal with terminal id " + registerTerminalModel.getTerminalId() + " exists");
                return "";
            }

            posTerminal = posTerminalDao.findByKey("serialNo", registerTerminalModel.getSerialNo());

            if (posTerminal != null) {

                JsfUtil.addErrorMessage("Terminal with serial no " + registerTerminalModel.getSerialNo() + " exists");
                return "";
            }

//            PosMerchant posMerchant = posMerchantDao.find(Long.parseLong(registerTerminalModel.getMerchant()));
//
//            if (posMerchant == null) {
//
//                JsfUtil.addErrorMessage("Pos merchant does not exists");
//                return "";
//            }
            PosProvider provider = providerDao.findByKey("shortName", registerTerminalModel.getProvider());

            posTerminal = new PosTerminal();
//            posTerminal.setMerchant(posMerchant);
            posTerminal.setCreatedOn(new Date());
            posTerminal.setDateIssued(registerTerminalModel.getDateIssued());
            posTerminal.setBatchNo(registerTerminalModel.getBatchNo());
            posTerminal.setTerminalId(registerTerminalModel.getTerminalId());
            posTerminal.setFccId(registerTerminalModel.getFccid());
            posTerminal.setModel(registerTerminalModel.getModel());
            posTerminal.setSerialNo(registerTerminalModel.getSerialNo());
            posTerminal.setReceivedBy(registerTerminalModel.getReceivedBy());
            posTerminal.setProvider(provider);
            posTerminal.setAssignedOn(new Date());
            posTerminal.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            
            Properties properties =  Utility.getConfigProperty();
            
            try{
                String bankPrefix = posTerminal.getTerminalId().substring(0,4);

                String bankName = properties.getProperty("T_"+bankPrefix);

                if(Utility.emptyToNull(bankName) != null){

                    posTerminal.setBankName(bankName);
                }
                else 
                    posTerminal.setBankName(registerTerminalModel.getBankName());

            }catch(Exception ex){
                ex.printStackTrace();

                posTerminal.setBankName(registerTerminalModel.getBankName());
            }
            
//            posTerminal.setBankName(registerTerminalModel.getBankName());
            
            posTerminalDao.create(posTerminal);

            JsfUtil.addSuccessMessage("POS Terminal has been added successfully");

            registerTerminalModel = new RegisterTerminalModel();

            return "/mpos/terminal?faces-redirect=true";
        } catch (DatabaseException ex) {
            LOGGER.error("Error creating pos", ex);
        }

        JsfUtil.addErrorMessage("Unable to register terminal, please try again later");

        return "";

    }

    public String prepareTerminalUpdate(long id) {

        PosTerminal terminal = null;

        try {
            terminal = posTerminalDao.find(id);
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (terminal == null) {

            JsfUtil.addErrorMessage("Terminal not found");
            return "/mpos/terminal?faces-redirect=true";
        }

        registerTerminalModel = RegisterTerminalModel.from(terminal);

        return "/mpos/edit?faces-redirect=true";

    }

    public String updateTerminal() {

        try {
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", registerTerminalModel.getTerminalId());

            if (posTerminal != null && posTerminal.getId() != registerTerminalModel.getId()) {

                JsfUtil.addErrorMessage("Terminal with terminal id " + registerTerminalModel.getTerminalId() + " exists");
                return "";
            }

            posTerminal = posTerminalDao.findByKey("serialNo", registerTerminalModel.getSerialNo());

            if (posTerminal != null && posTerminal.getId() != registerTerminalModel.getId()) {

                JsfUtil.addErrorMessage("Terminal with serial no " + registerTerminalModel.getSerialNo() + " exists");
                return "";
            }

            posTerminal = posTerminalDao.find(registerTerminalModel.getId());

//            PosMerchant posMerchant = posMerchantDao.find(Long.parseLong(registerTerminalModel.getMerchant()));
//
//            if (posMerchant == null) {
//
//                JsfUtil.addErrorMessage("Pos merchant does not exists");
//                return "";
//            }
            PosProvider provider = providerDao.findByKey("shortName", registerTerminalModel.getProvider());

//            posTerminal.setMerchant(posMerchant);
            posTerminal.setLastModified(new Date());
            posTerminal.setDateIssued(registerTerminalModel.getDateIssued());
            posTerminal.setBatchNo(registerTerminalModel.getBatchNo());
            posTerminal.setTerminalId(registerTerminalModel.getTerminalId());
            posTerminal.setFccId(registerTerminalModel.getFccid());
            posTerminal.setModel(registerTerminalModel.getModel());
            posTerminal.setSerialNo(registerTerminalModel.getSerialNo());
            posTerminal.setReceivedBy(registerTerminalModel.getReceivedBy());
            posTerminal.setProvider(provider);
            posTerminal.setAssignedOn(new Date());
            posTerminal.setBankName(registerTerminalModel.getBankName());
//            posTerminal.setuserDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));

            posTerminalDao.update(posTerminal);

            JsfUtil.addSuccessMessage("POS Terminal has been update successfully");

            registerTerminalModel = new RegisterTerminalModel();

            return "/mpos/terminal?faces-redirect=true";
        } catch (DatabaseException ex) {
            LOGGER.error("Error updating pos", ex);
        }

        JsfUtil.addErrorMessage("Unable to update terminal, please try again later");

        return "";

    }

    public List<PosProvider> getPosProviders() {

        try {
            return providerDao.findAll();
        } catch (DatabaseException ex) {
            LOGGER.error(null, ex);
        }

        return new ArrayList<>();
    }

    public List<PosMerchant> getPosMerchants() {

        try {
            return posMerchantDao.findAll();
        } catch (DatabaseException ex) {
            LOGGER.error(null, ex);
        }

        return new ArrayList<>();
    }

//    public String assignTerminal(){
//        
//    }
    // This is providers section
    public String createProvider() {

        try {
            PosProvider provider = providerDao.findByKey("name", posProviderViewModel.getName());

            if (provider != null) {

                JsfUtil.addErrorMessage("Pos Provider with name " + posProviderViewModel.getName() + " exists");
                return "";
            }

            provider = providerDao.findByKey("shortName", posProviderViewModel.getName());

            if (provider != null) {

                JsfUtil.addErrorMessage("Pos Provider with shortName " + posProviderViewModel.getShortName() + " exists");
                return "";
            }

            provider = new PosProvider();

            provider.setName(posProviderViewModel.getName());
            provider.setAddress(posProviderViewModel.getAddress());
            provider.setCreatedOn(new Date());
            provider.setPhone(posProviderViewModel.getPhone());
            provider.setEnabled(true);
            provider.setShortName(posProviderViewModel.getShortName());
            provider.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));

            providerDao.create(provider);

            JsfUtil.addSuccessMessage("Provider has been added successfully");

            posProviderViewModel = new PosProviderViewModel();

            return "/mpos/provider/list?faces-redirect=true";
        } catch (DatabaseException ex) {
            LOGGER.error(null, ex);
        }

        JsfUtil.addErrorMessage("Unable to create provider, please try again later");

        return "";
    }

    public void prepareCreateProvider() {

        posProviderViewModel = new PosProviderViewModel();
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void uploadTransaction() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            if (fileName.toLowerCase().endsWith("csv")) {

                User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString());

                utilityService.processCSVFile(uploadModel.getFile().getInputStream(), user, fileName);

                JsfUtil.addSuccessMessage("File is being processed, you will be notified when done");

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to upload records");
        }
    }
    
    public void uploadMerchants() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            if (fileName.toLowerCase().endsWith("xlsx")) {

                User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString());

                utilityService.processMerchantFile(uploadModel.getFile().getInputStream(), user, fileName);

                JsfUtil.addSuccessMessage("File is being processed, you will be notified when done");

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to upload records");
        }
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void uploadGATransaction() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            if (fileName.toLowerCase().endsWith("xlsx")) {

                User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString());

                utilityService.processITexFile(uploadModel.getFile().getInputStream(), user, fileName);

                JsfUtil.addSuccessMessage("File is being processed, you will be notified when done");

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to upload records");
        }
    }
    
    public void uploadMerchantCustomers() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            if(Utility.emptyToNull(uploadModel.getMerchantCode()) == null){
                JsfUtil.addErrorMessage("Merchant must be provided");    
                return;
            }
            
            if(Utility.emptyToNull(uploadModel.getParameterName()) == null){
                JsfUtil.addErrorMessage("Customer Tag / paramater name must be provided");    
                return;
            }
            
            PosMerchant merchant = posMerchantDao.findByKey("merchantCode", uploadModel.getMerchantCode());
            
            if(merchant == null){
                JsfUtil.addErrorMessage("Unknown pos merchant");    
                return;
            }
            
            if (fileName.toLowerCase().endsWith("xlsx")) {

                User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString());
                
                utilityService.processMerchantCustomerFile(uploadModel.getFile().getInputStream(), user,  uploadModel.getParameterName().trim(), merchant);

                JsfUtil.addSuccessMessage("File is being processed, you will be notified when done");

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to upload records");
        }
    }
    
    public void uploadTerminals() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            if (fileName.toLowerCase().endsWith("xlsx")) {

                User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString());

                utilityService.processTerminalFile(uploadModel.getFile().getInputStream(), user, fileName);

                JsfUtil.addSuccessMessage("File is being processed, you will be notified when done");

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to upload records");
        }
    }

    public void prepareUpload() {

        uploadModel = new UploadModel();
    }

    public UploadModel getUploadModel() {

        if (uploadModel == null) {
            uploadModel = new UploadModel();
        }

        return uploadModel;
    }

    public void setUploadModel(UploadModel uploadModel) {
        this.uploadModel = uploadModel;
    }

    private UploadModel uploadModel;

    public List<String> getBanks() {

        return utilityService.getBankNames();
    }

    public void exportPosMerchant(String parent, String search, String createdBetween, String format) throws DatabaseException {

        Date startDate = null, endDate = null;

        if (createdBetween != null && !"".equalsIgnoreCase(createdBetween)) {

            try {
                String[] splitDate = createdBetween.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                startDate = calendar.getTime();

                endDate = dateFormat.parse(splitDate[1]);

                calendar.setTime(endDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
//                calendar.set(Calendar.MILLISECOND, 59);

                endDate = calendar.getTime();

            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

        Page<PosMerchant> page = posMerchantDao.query(0, 0, startDate, endDate, Utility.emptyToNull(parent), Utility.emptyToNull(search));

        List<PosMerchantModel> merchants = page.getContent().stream().map((PosMerchant merchant) -> {

            return PosMerchantModel.from(merchant);
        }).collect(Collectors.<PosMerchantModel>toList());

        long totalCount = 0;

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        if ("pdf".equalsIgnoreCase(format)) {
            // export to pdf here

            List<String> headers = (List) Utility.getClassFields(PosMerchantModel.class);

            ServletOutputStream out = null;

            try {

                String fileName = PdfUtilExport.generatePdf("Flutterwave", "Merchants", headers, (List) merchants);

                response.setHeader("Content-Disposition", "attachment; filename=merchantrecord.pdf");
                response.setContentType("application/pdf");

                FileInputStream fileInputStream = new FileInputStream(fileName);
                out = response.getOutputStream();

                try {
                    IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                    out.flush();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }

//                out.flush();
                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } // end of pdf export
        else if ("csv".equalsIgnoreCase(format)) {

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
            List<String> headers = (List) Utility.getClassFields(PosMerchantModel.class);

            String output = CsvUtil.writeLine(headers, (List) merchants);

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=merchantrecord.csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        } else {

            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("PosMerchantRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

//        FillManager.fillTransactionReport(worksheet, startRowIndex, startColIndex, datasource);
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) TransactionUtility.getClassFields(PosMerchantModel.class), "Merchant Record", null);

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
//            List<String> headers = (List) TransactionUtility.getClassFields(PdfExportModel.class);
            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) merchants);

            //HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//       String fileName = "Card-Batch.xls";
            response.setHeader("Content-Disposition", "attachment; filename=merchantrecord.xls");
            response.setContentType("application/vnd.ms-excel");

            ServletOutputStream out = null;

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }
    }

    public void exportPosTerminal(String provider, String search, String format) throws DatabaseException {

        Date startDate = null, endDate = null;

//        if (createdBetween != null && !"".equalsIgnoreCase(createdBetween)) {
//
//            try {
//                String[] splitDate = createdBetween.split("-");
//
//                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//
//                startDate = dateFormat.parse(splitDate[0]);
//
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(startDate);
//                calendar.set(Calendar.HOUR_OF_DAY, 0);
//                calendar.set(Calendar.MINUTE, 0);
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//
//                startDate = calendar.getTime();
//
//                endDate = dateFormat.parse(splitDate[1]);
//
//                calendar.setTime(endDate);
//                calendar.set(Calendar.HOUR_OF_DAY, 23);
//                calendar.set(Calendar.MINUTE, 59);
//                calendar.set(Calendar.SECOND, 59);
////                calendar.set(Calendar.MILLISECOND, 59);
//
//                endDate = calendar.getTime();
//
//            } catch (ParseException pe) {
//                startDate = endDate = null;
//            }
//
//        }
        Page<PosTerminal> terminals = posTerminalDao.query(0, 0, startDate, endDate, 
                Utility.emptyToNull(provider),null, Utility.emptyToNull(search));

        List<MposTerminalViewModel> models = terminals.getContent().stream().map((PosTerminal terminal) -> {

            MposTerminalViewModel model = new MposTerminalViewModel();
            model.setActivated(terminal.isActivated());
            model.setEnabled(terminal.isEnabled());
            model.setCreatedOn(terminal.getCreatedOn());
            model.setId(terminal.getId());
            model.setActivatedOn(terminal.getActivatedOn());
            model.setTerminalId(terminal.getTerminalId());
            model.setMerchant(terminal.getMerchant() == null ? null : terminal.getMerchant().getName());
            model.setAssignedBy(terminal.getAssignedBy());
            model.setAssignedOn(terminal.getAssignedOn());
            model.setProvider(terminal.getProvider() == null ? null : terminal.getProvider().getName());
            model.setSerialNo(terminal.getSerialNo());
            model.setActivationCode(terminal.getActivationCode());
            model.setModel(terminal.getModel());
            model.setFccId(terminal.getFccId());

            return model;
        }).collect(Collectors.<MposTerminalViewModel>toList());

        long totalCount = 0;

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        if ("pdf".equalsIgnoreCase(format)) {
            // export to pdf here

            List<String> headers = (List) Utility.getClassFields(MposTerminalViewModel.class);

            ServletOutputStream out = null;

            try {

                String fileName = PdfUtilExport.generatePdf("Flutterwave", "Terminals", headers, (List) models);

                response.setHeader("Content-Disposition", "attachment; filename=terminalrecord.pdf");
                response.setContentType("application/pdf");

                FileInputStream fileInputStream = new FileInputStream(fileName);
                out = response.getOutputStream();

                try {
                    IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                    out.flush();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }

//                out.flush();
                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } // end of pdf export
        else if ("csv".equalsIgnoreCase(format)) {

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
            List<String> headers = (List) Utility.getClassFields(MposTerminalViewModel.class);

            String output = CsvUtil.writeLine(headers, (List) models);

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=terminalrecords.csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        } else {

            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("TerminalRecords");

            int startRowIndex = 0;
            int startColIndex = 0;

//        FillManager.fillTransactionReport(worksheet, startRowIndex, startColIndex, datasource);
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) TransactionUtility.getClassFields(MposTerminalViewModel.class), "Terminal Record", null);

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
//            List<String> headers = (List) TransactionUtility.getClassFields(PdfExportModel.class);
            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) models);

            //HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//       String fileName = "Card-Batch.xls";
            response.setHeader("Content-Disposition", "attachment; filename=terminalrecord.xls");
            response.setContentType("application/vnd.ms-excel");

            ServletOutputStream out = null;

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().getResponseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }
    }
}

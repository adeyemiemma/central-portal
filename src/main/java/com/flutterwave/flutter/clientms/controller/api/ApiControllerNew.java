package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.dao.GlobalRewardSettingDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.dao.PosProviderDao;
import com.flutterwave.flutter.clientms.dao.PosTerminalDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionNewDao;
import com.flutterwave.flutter.clientms.dao.RewardSettingDao;
import com.flutterwave.flutter.clientms.model.GlobalRewardSetting;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosMerchantModel;
import com.flutterwave.flutter.clientms.model.PosProvider;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.model.RewardSetting;
import com.flutterwave.flutter.clientms.service.TerminalService;
import com.flutterwave.flutter.clientms.service.UtilityService;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.MposTerminalViewModel;
import com.flutterwave.flutter.clientms.viewmodel.PosProviderViewModel;
import com.flutterwave.flutter.clientms.viewmodel.PosTransactionViewModelWeb;
import com.flutterwave.flutter.clientms.viewmodel.RewardSettingViewModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author emmanueladeyemi
 */
@Path("/")
@Produces(value = MediaType.APPLICATION_JSON)
@RequestScoped
public class ApiControllerNew {

    @EJB
    private PosTerminalDao posTerminalDao;
    @EJB
    private PosMerchantDao posMerchantDao;
    @EJB
    private TerminalService terminalService;
    @EJB
    private PosProviderDao posProviderDao;
    @EJB
    private RewardSettingDao rewardSettingDao;
    @EJB
    private GlobalRewardSettingDao globalRewardSettingLogDao;
    @EJB
    private PosTransactionDao posTransactionDao;
    @EJB
    private PosTransactionNewDao posTransactionNewDao;
    @EJB
    private UtilityService utilityService;

    @Path(value = "mpos/terminals")
    @GET
    public Response getMposTerminal(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "range") String dateRange,
            @QueryParam(value = "bank") String bank,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "searchValue") String searchValue) {

        if("any".equalsIgnoreCase("provider"))
            provider = null;
        
        if("any".equalsIgnoreCase(bank))
            bank = null;
        
        PageResult<MposTerminalViewModel> pageResult = terminalService.getTerminals(start, length,Utility.emptyToNull(provider),
                Utility.emptyToNull(bank), Utility.emptyToNull(searchValue));

        return Response.status(Response.Status.OK).entity(pageResult).build();
    }

    @Path(value = "pos/merchants")
    @GET
    public Response getPosmerchant(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") @DefaultValue(value = "20") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "parent")  String parent,
            @QueryParam(value = "range")  String createdBetween,
            @QueryParam(value = "searchValue")  String searchValue) {

        try {
            
            Date startDate = null, endDate = null;

        if (createdBetween != null && !"".equalsIgnoreCase(createdBetween)) {

            try {
                String[] splitDate = createdBetween.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                startDate = calendar.getTime();

                endDate = dateFormat.parse(splitDate[1]);

                calendar.setTime(endDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
//                calendar.set(Calendar.MILLISECOND, 59);

                endDate = calendar.getTime();

            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
            Page<PosMerchant> page = posMerchantDao.query(start, length,startDate, endDate, Utility.emptyToNull(parent), Utility.emptyToNull(searchValue) );

            if (page == null || page.getCount() <= 0) {

                return Response.status(Response.Status.OK).entity(new PageResult()).build();
            }

            List<PosMerchantModel> list = page.getContent().stream().map((PosMerchant merchant) -> {

                return PosMerchantModel.from(merchant);
            }).collect(Collectors.<PosMerchantModel>toList());

            PageResult<PosMerchantModel> pageResult = new PageResult<>(list, page.getCount(), page.getCount());

            return Response.status(Response.Status.OK).entity(pageResult).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiControllerNew.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).entity(new PageResult()).build();
    }

    @Path(value = "pos/providers")
    @GET
    public Response getPosProviders(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "range") String dateRange,
            @QueryParam(value = "searchKey") String searchKey,
            @QueryParam(value = "searchValue") String searchValue) {

        PageResult<PosProviderViewModel> pageResult = new PageResult<>();

        try {
            if (searchKey == null) {
                searchKey = "name";
            }

            Page<PosProvider> providers = posProviderDao.find(start, length);

            List<PosProviderViewModel> models = new ArrayList<>();

            if (providers != null && providers.getCount() > 0) {

                models = providers.getContent().stream().map((PosProvider x) -> {

                    PosProviderViewModel model = new PosProviderViewModel();
                    model.setAddress(x.getAddress());
                    model.setCreatedOn(x.getCreatedOn());
                    model.setName(x.getName());
                    model.setPhone(x.getPhone());
                    model.setShortName(x.getShortName());
                    model.setEnabled(x.isEnabled());

                    return model;
                }).collect(Collectors.<PosProviderViewModel>toList());

                pageResult.setData(models);
                pageResult.setDraw(draw);
                pageResult.setRecordsFiltered(providers.getCount());
                pageResult.setRecordsTotal(providers.getCount());
            }

            return Response.status(Response.Status.OK).entity(pageResult).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiControllerNew.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).entity(pageResult).build();

    }
    
    
    public Response ResponseDownloadReport(@QueryParam(value = "merchantid") String merchantId, 
            @QueryParam(value = "merchantcode") String merchantcode, 
            @QueryParam(value = "range") String range, 
            @QueryParam(value = "type") String type,
            @QueryParam(value = "provider") String provider, 
            @QueryParam(value = "currency") String currency, 
            @QueryParam(value = "responsecode") String responseCode, 
            @QueryParam(value = "bankname") String bankName, 
            @QueryParam(value = "search") String search){
        
        
        
        Date startDate = null, endDate = null;
        
        String startDateStr,  endDateStr;
        
        if (Utility.emptyToNull(range) != null) {

            try {
                String[] splitDate = range.split("-");
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                
                startDateStr = Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss");

                endDate = dateFormat.parse(splitDate[1]);
                
                endDateStr = Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        long age = Math.abs(Utility.age(endDate, startDate, true));
        
        if(age > 30){
            
            Map<String, String> response = new HashMap<>();
            response.put("status", "failed");
            response.put("description", "Maximum of 30 day(s) of transactions is permitted");
            
            return Response.status(Response.Status.OK).entity(response).build();
        }
        
        String email = SecurityUtils.getSubject().getPrincipal().toString();
        
        if("admin@flutterwavego.com".equalsIgnoreCase(email)){
            email = "emmanuel@flutterwavego.com";
        }
        
        
        return null;
    }

    @Path(value = "/mpos/terminal/fetch")
    @GET
    public Response fetchMPosTerminals() {

        int count = terminalService.fetchTerminals();

        Map<String, String> response = new HashMap<>();

        if (count <= -1) {
            response.put("status-code", "01");
            response.put("status", "Unable to fetch terminal at this point please try again later");
        } else if (count == 0) {

            response.put("status-code", "00");
            response.put("status", "No new terminal available");
        } else {
            response.put("status-code", "00");
            response.put("status", count + " terminals have been to the system");
        }

        return Response.status(Response.Status.OK).entity(response).build();

    }

    @Path(value = "/reward/merchant/setting")
    @GET
    public Response getRewardSetting(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") @DefaultValue(value = "20") int length,
            @QueryParam(value = "draw") int draw) {

        try {
            Page<RewardSetting> page = rewardSettingDao.find(start, length);

            if (page == null || page.getCount() <= 0) {
                return Response.status(Response.Status.OK).entity(new PageResult()).build();
            }

            List<RewardSettingViewModel> list = page.getContent().stream().map((RewardSetting rewardSetting) -> {

                return RewardSettingViewModel.from(rewardSetting);
            }).collect(Collectors.<RewardSettingViewModel>toList());

            PageResult<RewardSettingViewModel> pageResult = new PageResult<>(list, page.getCount(), page.getCount());

            return Response.status(Response.Status.OK).entity(pageResult).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiControllerNew.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).entity(new PageResult()).build();
    }

    @Path(value = "reward/global/setting")
    @GET
    public Response getGlobalRewardSetting(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") @DefaultValue(value = "20") int length,
            @QueryParam(value = "draw") int draw) {

        try {
            Page<GlobalRewardSetting> page = globalRewardSettingLogDao.find(start, length);

            if (page == null || page.getCount() <= 0) {
                return Response.status(Response.Status.OK).entity(new PageResult()).build();
            }

            List<RewardSettingViewModel> list = page.getContent().stream().map((GlobalRewardSetting rewardSetting) -> {

                return RewardSettingViewModel.from(rewardSetting);
            }).collect(Collectors.<RewardSettingViewModel>toList());

            PageResult<RewardSettingViewModel> pageResult = new PageResult<>(list, page.getCount(), page.getCount());

            return Response.status(Response.Status.OK).entity(pageResult).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiControllerNew.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).entity(new PageResult()).build();
    }

    @Path(value = "pos/merchant/all")
    @GET
    public Response getAllPOSMerchants() {

        try {
            Page<PosMerchant> coreMerchant = posMerchantDao.find(0, 0);

            List<Map> merchants = new ArrayList<>();

            if (coreMerchant.getContent() != null && !coreMerchant.getContent().isEmpty()) {

                merchants.addAll(coreMerchant.getContent().stream().map((PosMerchant rm) -> {
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", rm.getId());
                    map.put("text", rm.getName() + " ("+rm.getPosId()+")");

                    return map;
                }).collect(Collectors.<Map>toList()));
            }

//            pageResult = new PageResult(merchants, totalCount, totalCount);
            return Response.status(Response.Status.OK).entity(merchants).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiControllerNew.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).entity(new ArrayList()).build();
    }

    @Path(value = "pos/terminal/assign")
    @POST
    public Response assignTerminal(String data) {

        JSONObject jSONObject = new JSONObject(data);
        
        String merchant = jSONObject.optString("merchant", "0");
        String terminal = jSONObject.optString("terminal", "-1");
                
        
        Map<String, String> response = new HashMap<>();

        try {
            PosMerchant posMerchant = posMerchantDao.find(Long.parseLong(merchant));

            if (posMerchant == null) {

                response.put("status", "Unable to find merchant");
                response.put("status-code", "01");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", terminal);

            if (posTerminal == null) {

                response.put("status", "Unable to find terminal");
                response.put("status-code", "01");

                return Response.status(Response.Status.OK).entity(response).build();
            }

//            if (posTerminal.getMerchant() != null) {
//
//                response.put("status", "Pos is already assigned to another merchant");
//                response.put("status-code", "01");
//
//                return Response.status(Response.Status.OK).entity(response).build();
//            }

            posTerminal.setMerchant(posMerchant);
            posTerminal.setAssignedOn(new Date());
            posTerminal.setActivationCode(Utility.generateOTP(8));
            
            String user = SecurityUtils.getSubject().getPrincipal().toString();
            
            posTerminal.setAssignedBy(user);
            
            posTerminalDao.update(posTerminal);

            response.put("status", "Terminal has been assigned successfully");
            response.put("status-code", "00");

            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiControllerNew.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.put("status", "Unable to assign terminal, please try later");
        response.put("status-code", "01");

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "pos/transaction")
    @GET
    public Response getTransactions(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") @DefaultValue(value = "20") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "posid") String posId,
            @QueryParam(value = "merchantid") String merchantId,
            @QueryParam(value = "merchantcode") String merchantcode,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "type") String type,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "bankName") String bankName,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "responsecode") String responseCode,
            @QueryParam(value = "searchstring") String search){
        
        
        Date startDate = null, endDate = null;

        if (range != null && !"".equalsIgnoreCase(range)) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);

                endDate = dateFormat.parse(splitDate[1]);
                
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        String providerName = null;
        try{
            long providerId = Long.parseLong(provider);
            PosProvider posProvider =  posProviderDao.find(providerId);
            
            if(posProvider != null)
                providerName = posProvider.getName();
        }catch(Exception ex){
        }
        
        Page<PosTransactionViewModelWeb> page = posTransactionNewDao.findForWebNew(start, length, Utility.emptyToNull(merchantId), Utility.emptyToNull(merchantcode), 
                    Utility.emptyToNull(providerName), startDate, endDate, Utility.emptyToNull(responseCode), Utility.emptyToNull(currency), 
                    Utility.emptyToNull(type), null, Utility.emptyToNull(bankName), Utility.emptyToNull(search), null);
            
            
        PageResult<PosTransactionViewModelWeb> pageResult = new PageResult<>();

        if(page != null && page.getCount() > 0){


            pageResult.setRecordsFiltered(page.getCount());
            pageResult.setRecordsTotal(page.getCount());
            pageResult.setData(page.getContent());
        }

        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path(value = "pos/transaction/download")
    @GET
    public Response downloadPosTransactions(
            @QueryParam(value = "posid") String posId,
            @QueryParam(value = "merchantid") String merchantId,
            @QueryParam(value = "merchantcode") String merchantcode,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "type") String type,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "bankName") String bankName,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "responsecode") String responseCode,
            @QueryParam(value = "searchstring") String search){
        
        
        Date startDate = null, endDate = null;

        if (range != null && !"".equalsIgnoreCase(range)) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);


                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        String email = SecurityUtils.getSubject().getPrincipal() + "";
        
        
        CompletableFuture.runAsync(() -> utilityService.exportPOSTransaction(email,merchantId, merchantcode, range, 
                type, provider, currency, responseCode, bankName, search));        
            
//        PageResult<PosTransactionViewModelWeb> pageResult = new PageResult<>();

        Map<String, String> response = new HashMap<>();
        response.put("status_code", "00");
         response.put("status", "Successful");

        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @Path(value = "pos/transaction/response")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON) 
    public Response getCoreResponses(){
        
        Map<String, String> array = posTransactionDao.getResponse(null);
        
        return Response.status(Response.Status.OK).entity(array).build();
    }
    
    @Path(value = "pos/transaction/{id}")
    @GET
    public Response getTransaction(@PathParam(value = "id") long id){
        
        Map<String, Object> response = new HashMap<>();
        
        try {
            
            if(id <= 0){
                
                response.put("status", "failed");
                return Response.status(Response.Status.OK).entity(response).build();
            }
            
            PosTransaction posTransaction = posTransactionDao.findByKey("id", id);
            
            if(posTransaction == null ){
                response.put("status", "failed");
                return Response.status(Response.Status.OK).entity(response).build();
            }
            
            PosTransactionViewModelWeb modelWeb = PosTransactionViewModelWeb.from(posTransaction);
            
            PosTerminal terminal = terminalService.getTerminal(posTransaction.getTerminalId());
            
            if(terminal != null){
                if(terminal.getProvider() != null)
                    modelWeb.setProvider(terminal.getProvider().getName());
                
                
                PosMerchant posMerchant = terminalService.getMerchantName(posTransaction.getPosId(),
                        terminal.getMerchant() == null ? null : terminal.getMerchant().getMerchantCode());
                
                if(posMerchant != null){
                    modelWeb.setMerchantName(posMerchant.getName());
                    modelWeb.setMerchantId(posMerchant.getMerchantId());
                    modelWeb.setMerchantCode(posMerchant.getMerchantCode());
                }
            }
            
            response.put("status", "success");
            response.put("data", modelWeb );
            
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiControllerNew.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.put("status", "failed");
        return Response.status(Response.Status.OK).entity(response).build();
                
    }
    
    @Path("pos/merchant")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantCore(@QueryParam(value = "owner") String merchantOwner) {

        try {
            
            List<PosMerchant> coreMerchant = posMerchantDao.find("merchantId", merchantOwner);

            List<Map> merchants = new ArrayList<>();
                        
            if (coreMerchant != null && !coreMerchant.isEmpty()) {

                merchants.addAll(coreMerchant.stream().map((PosMerchant rm) -> {
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", rm.getMerchantCode());
                    map.put("text", rm.getName());

                    return map;
                }).collect(Collectors.<Map>toList()));
           }

//            pageResult = new PageResult(merchants, totalCount, totalCount);
            return Response.status(Response.Status.OK).entity(merchants).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
    }
    
    
}

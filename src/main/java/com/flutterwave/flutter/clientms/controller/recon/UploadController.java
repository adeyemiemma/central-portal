/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.recon;

import com.flutterwave.flutter.clientms.dao.recon.BatchDao;
import com.flutterwave.flutter.clientms.dao.recon.FetchTransactionStatusDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.recon.Batch;
import com.flutterwave.flutter.clientms.model.recon.BatchResolved;
import com.flutterwave.flutter.clientms.model.recon.FetchTransactionStatus;
import com.flutterwave.flutter.clientms.model.recon.FlutterTransaction;
import com.flutterwave.flutter.clientms.model.recon.ReconTransaction;
import com.flutterwave.flutter.clientms.model.recon.UploadAnalysisModel;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.recon.FileProcessor;
import com.flutterwave.flutter.clientms.service.recon.TransactionService;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.ReconHttpUtil;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.ReconUploadModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.flow.FlowScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

/**
 *
 * @author adeyemi
 */
@Named
@FlowScoped("upload")
public class UploadController implements Serializable{

    @EJB
    private FileProcessor fileProcessor;
    @EJB
    private TransactionService transactionService;
    @EJB
    private FetchTransactionStatusDao fetchTransactionStatusDao;
    @EJB
    private LogService logService;
    @EJB
    private BatchDao batchDao;
    
    @Inject
    private Global global;
    
    private static final long BATCH_SIZE = 500;
    
    private UploadAnalysisModel analysisModel;
    
    BatchResolved batchResolved;
    private Batch batch;
    private String recentUploadDateRange;
    
    public String getRecentUploadedDateRange(){
        
        
        return recentUploadDateRange;
    }
    
    private ListDataModel<ReconTransaction> transactions;

    public ListDataModel<ReconTransaction> getTransactions() {
        return transactions;
    }
    
    /**
     * @return the uploadModel
     */
    public ReconUploadModel getUploadModel() {
        
        if(uploadModel == null){
            uploadModel = new ReconUploadModel();
            uploadModel.setSettled(true);
        }
        return uploadModel;
    }

    /**
     * @param uploadModel the uploadModel to set
     */
    public void setUploadModel(ReconUploadModel uploadModel) {
        this.uploadModel = uploadModel;
    }
    
    private ReconUploadModel uploadModel;
    
    public String uploadPreview(){
        
       // System.out.println("This is the upload preview");
        
        if(getUploadModel() == null || getUploadModel().getFile() == null){
            JsfUtil.addErrorMessage("No file uploaded");
            
            return "upload";
        }
        
        try {
            
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();
            
            batch = batchDao.findByKey("fileName", fileName);
            
            if(batch != null){
                
                JsfUtil.addErrorMessage("File has already been uploaded");
                return "upload";
            }
            
            long size = uploadModel.getFile().getSize();            
            
            if(size > 10485760L){
                
                JsfUtil.addErrorMessage("Maximum file size must be 10MB");
                return "upload";
            }
            
            batch = new Batch();
            
            batch.setFileName(fileName);
            batch.setBatchId(TransactionUtility.generateBatchId());
            batch.setCreatedOn(new Date());
            batch.setEndDate(uploadModel.getToDate());
            batch.setStartDate(uploadModel.getFromDate());
            
            if (fileName.toLowerCase().endsWith("xls") || fileName.toLowerCase().endsWith("xlsx")) {
                
                if (fileName.toLowerCase().endsWith("xls")) {
                    analysisModel = fileProcessor.processXls(uploadModel, batch);
                } else {
                    analysisModel = fileProcessor.process(uploadModel, batch);
                }
                
                if(analysisModel != null)
                    analysisModel.getDates().sort((x, y) -> {
                        return x.compareTo(y);
                    });

                transactions = new ListDataModel<>(analysisModel.getTransactions());
                
            } else {
                throw new IOException("Invalid file type");
            }  

            if(uploadModel.getFile() != null)
                uploadModel.getFile().getInputStream().close();
            
            uploadModel.setFile(null);
            
        } catch (IOException ex) {
            Logger.getLogger(UploadController.class.getName()).log(Level.SEVERE, null, ex);
            
            if(ex != null)
                JsfUtil.addErrorMessage(ex.getMessage());
            else
                JsfUtil.addErrorMessage("Error while uploading file");
            
            return "upload";
        } catch (DatabaseException ex) {
            Logger.getLogger(UploadController.class.getName()).log(Level.SEVERE, null, ex);
            
            if(ex != null)
                JsfUtil.addErrorMessage(ex.getMessage());
            else
                JsfUtil.addErrorMessage("Error while uploading file");
            
            return "upload";
        }
        
        return "uploadpreview";
    }
    
    public String uploadSettlement(){
        
        return "/recon/upload/uploadsettlement";
    }
    
//    @Transactional
    public void uploadCompleted(){
        
        try {
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            
            Date startD = analysisModel.getDates().get(0);
            Date endD = analysisModel.getDates().get(analysisModel.getDates().size()-1);
            
            String startDate = dateFormat.format(startD);
            
            uploadModel.setFromDate(startD);
            uploadModel.setToDate(endD);
            
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(uploadModel.getToDate());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            
            String endDate = dateFormat.format(endD);
            
            if(endDate.equals(startDate)){
                calendar.setTime(startD);
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                endDate = dateFormat.format(calendar.getTime());
            }
            
            batch.setCount(analysisModel.getTransactionCount());
            batch.setSettled(uploadModel.isSettled());
            batch.setSettlementDate(uploadModel.getDateSettled());
            batch.setScheme(uploadModel.getScheme());
            batch.setStartDate(startD);
            batch.setEndDate(endD);
            
//            FetchTransactionStatus fts = fetchTransactionStatusDao.findByKey("startDate", startDate);
            
//            batchResolved = transactionService.uploadData(analysisModel.getTransactions(), batch);
            
            recentUploadDateRange = startDate+" to "+endDate;
            
            
            Future<BatchResolved> future = transactionService.uploadDataBackground(analysisModel.getTransactions(), batch);
            
            global.setFuture(future);
            
//            if(fts == null){
////                transactionFetcherService.queueFetchTransaction(startDate, endDate, 60*1000, batch.getBatchId() );
//
//                int count = 0;
//                
//                Log log = new Log();
//                log.setAction("FETCHING TRANSACTIONS FROM CORE");
//                log.setCreatedOn(new Date());
//                log.setLevel(LogLevel.Info);
//                log.setDescription("Fetching transactions from core");
//                log.setLogDomain(LogDomain.ADMIN);
//                log.setLogState(LogState.STARTED);
//                log.setStatus(LogStatus.SUCCESSFUL);
//                
//                logService.log(log, null);
//                
//                List<FlutterTransaction> mainList = new ArrayList<>();
////                String startDate = Utility.formatDate(uploadModel.getFromDate(), "yyyy-mm-dd");
//                
//                while(true){
//                    List<FlutterTransaction> flutterTransaction = ReconHttpUtil.getFlutterTransactions(startDate, endDate,count,BATCH_SIZE);
//                    count += BATCH_SIZE;
//                    mainList.addAll(flutterTransaction);
//                    if(flutterTransaction == null || flutterTransaction.size() < BATCH_SIZE)
//                      break;
//                }
//                
//                FetchTransactionStatus fetchTransactionStatus = new FetchTransactionStatus();
//                fetchTransactionStatus.setAttempt(1);
//                fetchTransactionStatus.setCreatedOn(new Date());
//                fetchTransactionStatus.setSize(mainList.size());
//                fetchTransactionStatus.setStartDate(startDate);
//                fetchTransactionStatus.setEndDate(endDate);
//                fetchTransactionStatus.setStatus(Utility.FetchStatus.SUCCESSFUL);
//                fetchTransactionStatusDao.create(fetchTransactionStatus);
//                
//                if(!mainList.isEmpty()){
//                    transactionService.uploadFlutterTransaction(mainList, startDate, endDate);
//                }
//            }

            
            
//            batchResolved = transactionService.resolveTransactions(analysisModel.getTransactions(), batch); 
                
            analysisModel.setBank(uploadModel.getBank());
            
//            return "/upload/uploadcompleted";
        } catch (Exception ex) {
            Logger.getLogger(UploadController.class.getName()).log(Level.SEVERE, null, ex);
            
            if(ex != null)
                JsfUtil.addErrorMessage(ex.getMessage());
            else
                JsfUtil.addErrorMessage("Unable to upload now please try again later");
            
//            return "uploadanalysis";
        }
        
//        finally{
//            analysisModel.setTransactions(null);
//        }
        
//        JsfUtil.addSuccessMessage("Transactions has been upload successfully and queue for settlement");
        
        //return "uploadanalysis";
    }
        
    public String showAnalysis(){
        
        try {
            batchResolved = global.getFuture().get();
            
            uploadModel = new ReconUploadModel();
            
            global.setFuture(null);
            
            return "uploadanalysis";
        } catch (InterruptedException ex) {
            Logger.getLogger(UploadController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(UploadController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "uploadanalysis";
    }
    
    public String getHomeAction() {
        
        uploadModel = new ReconUploadModel();
        analysisModel = new UploadAnalysisModel();
        
        return "/index";
    }
    
    public UploadAnalysisModel getAnalysisModel(){
        
        if(analysisModel == null)
            analysisModel = new UploadAnalysisModel();
        
        return analysisModel;
    }
    
    public BatchResolved getResolveBatch(){
    
        if(batchResolved == null)
            batchResolved = new BatchResolved();
        
        return batchResolved;
    }
    
    public Map getBanks(){
        
        Map<String,String> banks = new TreeMap<>();
        banks.put("Access Bank", "Access Bank Plc");
//        banks.put("Citi Bank", "Citi Bank Nigeria Limited");
//        banks.put("EcoBank", "Ecobank Nigeria Plc");
//        banks.put("Diamond Bank", "Diamond Bank Plc");
//        banks.put("Enterprise Bank", "Enterprise Bank");
//        banks.put("Fidelity Bank", "Fidelity Bank Plc");
//        banks.put("First Bank", "First Bank");
//        banks.put("GTB", "Guaranty Trust Bank Plc");
//        banks.put("Key Stone Bank", "Key Stone Bank Plc");
//        banks.put("MainStreet Bank", "MainStreet Bank Plc");
//        banks.put("Skye Bank", "Skye Bank Plc");
//        banks.put("Stanbic IBTC Bank", "Stanbic IBTC Bank Ltd.");
//        banks.put("Standard Chartered Bank", "Stanbic IBTC Bank Ltd.");
//        banks.put("Sterling Bank", "Sterling Bank Plc");
//        banks.put("United Bank", "United Bank For Africa Plc");
//        banks.put("Unity Bank", "Unity Bank Plc");
//        banks.put("Wema Bank", "Wema Bank");
//        banks.put("Zenith Bank", "Zenith Bank");
        
        return banks;
    }
    
}

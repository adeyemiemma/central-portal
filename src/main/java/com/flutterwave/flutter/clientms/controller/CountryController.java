/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.CountryDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.CountryMDao;
import com.flutterwave.flutter.clientms.model.Country;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.CountryM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.viewmodel.UploadModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import javax.transaction.Transactional;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author adeyemi
 */
@Named(value = "countryController")
@SessionScoped
public class CountryController implements Serializable {

    private Country country;

    private DataModel allCurrencies = null;
    private DataModel allUnauthCurrencies = null;
    
    private List<CountryM> countriesM;

    private static final Logger LOGGER = Logger.getLogger(CountryController.class.getName());

    @EJB
    private CountryDao countryDao;
    @EJB
    private CountryMDao countryMDao;
    @EJB
    private LogService logService;
    @EJB
    private UserDao userDao;
    
    private boolean sortByName = false;

    private PaginationHelper pagination, paginationHelper;

    private UploadModel uploadModel;

    public UploadModel getUploadModel() {

        if (uploadModel == null) {
            uploadModel = new UploadModel();
        }

        return uploadModel;
    }

    public void setUploadModel(UploadModel uploadModel) {
        this.uploadModel = uploadModel;
    }

    public CountryController() {
    }

    public Country getSelected() {
        if (country == null) {
            country = new Country();
        }
        return country;
    }

    public void setSelected(Country country) {

        this.country = country;
    }

    public String prepareCreate() {

        country = new Country();

        return "create";
    }

    public String prepateUpdate() {

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String param = params.get("editParam") + "";

        try {
            country = countryDao.find(Long.parseLong(param));

            Log log = new Log();
            log.setAction("Update Country " + country.getName());
            log.setCreatedOn(new Date());
            log.setDescription("about to update country");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

        } catch (DatabaseException de) {
            Logger.getLogger(CountryController.class.getName()).log(Level.SEVERE, null, de);

            try {
                Log log = new Log();
                log.setAction("update country " + country.getName());
                log.setCreatedOn(new Date());
                log.setDescription("update country");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (de != null) {
//                    log.setStatusMessage(de.getMessage());

                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception ex) {
            }
        }

        return "edit";
    }

    public String update() {

        try {

            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");

            Country curr = countryDao.findByKey("shortName", country.getShortName());

            if (curr != null && curr.getId() != country.getId()) {

                JsfUtil.addErrorMessage("Short name exists");

                return "edit";
            }
            
            curr = countryDao.findByCurrency(country.getCurrency());

            if (curr != null && curr.getId() != country.getId()) {


                JsfUtil.addErrorMessage("Country with base currency " + curr.getCurrency().getShortName()+"  exist");

                return "edit";
            }

            List<CountryM> list = countryMDao.find("modelId", country.getId());

            if (list != null && !list.isEmpty()) {

                CountryM cM = list.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {

                    JsfUtil.addErrorMessage("A pending update exists");
                    return "edit";
                }
            }

            CountryM countryM = new CountryM();
            countryM.setModelId(country.getId());
            countryM.setCreatedOn(new Date());
            countryM.setEnabled(country.isEnabled());
            countryM.setName(country.getName());
            countryM.setShortName(country.getShortName());
            countryM.setCreatedBy(user);
            countryM.setCurrency(country.getCurrency());

            countryM = countryMDao.create(countryM);

            Log log = new Log();
            log.setAction("Update Country " + country.getName());
            log.setCreatedOn(new Date());
            log.setDescription("Country updated has been created and successfully updated");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

            if (countryM == null) {
                throw new DatabaseException("Unable to update record");
            }

            JsfUtil.addSuccessMessage("Country has been updated succesfully waiting for authorization");

            paginationHelper = null;

            return "/country/list";

        } catch (DatabaseException ex) {
            Logger.getLogger(CountryController.class.getName()).log(Level.SEVERE, null, ex);

            if (ex != null) {
                JsfUtil.addErrorMessage("" + ex.getMessage());
            } else {
                JsfUtil.addErrorMessage("Unable to update record , please try again later");
            }

            try {
                Log log = new Log();
                log.setAction("update country " + country.getName());
                log.setCreatedOn(new Date());
                log.setDescription("update country");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {
//                    log.setStatusMessage(ex.getMessage());

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return "edit";
    }

    public String nextUnauth() {
        getPaginationHelperUnauth().nextPage();
        recreate();
        return "unauth";
    }
    
    public String previousUnauth() {
        getPaginationHelperUnauth().previousPage();
        recreate();
        return "unauth";
    }
    
    public String next() {
        getPaginationHelper().nextPage();
        recreate();
        return "list";
    }
    
    public String previous() {
        getPaginationHelper().previousPage();
        recreate();
        return "list";
    }
    
    @Transactional
    public String create() {

        if (country != null) {
            country.setCreatedOn(new Date());
        }

        try {
            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");

            Country c = countryDao.findByKey("name", country.getName());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create country: " + country.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate country with name " + c.getName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Country with name %s exists", country.getName()));

                return "create";
            }

            c = countryDao.findByKey("shortName", country.getShortName());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create country: " + country.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate country with short name " + c.getShortName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Country with short name %s exists", country.getShortName()));

                return "create";
            }
            
            c = countryDao.findByCurrency(country.getCurrency());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create country: " + country.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Country with base currency " + c.getCurrency().getShortName()+"  exist");
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage("Country with base currency " + c.getCurrency().getShortName()+"  exist");

                return "create";
            }

//            List<CountryM> countries = countryMDao.find("name", country.getName());
            boolean status = findUnauthorizedCountry(country.getName(), true);

            if (status == true) {

                Log log = new Log();
                log.setAction("Create country: " + country.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Pending country with name " + country.getShortName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Pending approval on Country with  name %s exists", country.getShortName()));

                return "create";
            }

            status = findUnauthorizedCountry(country.getShortName(), false);

            if (status == true) {

                Log log = new Log();
                log.setAction("Create country: " + country.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Pending country with name " + country.getShortName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Pending approval on Country with  name %s exists", country.getShortName()));

                return "create";
            }

            CountryM countryM = new CountryM();
            countryM.setCreatedOn(new Date());
            countryM.setCreatedBy(user);
            countryM.setEnabled(true);
            countryM.setName(country.getName());
            countryM.setShortName(country.getShortName());
            countryM.setStatus(Status.PENDING);

            countryMDao.create(countryM);

            Log log = new Log();
            log.setAction("Create country: " + country.getName());
            log.setCreatedOn(new Date());
            log.setDescription("country has been created and submitted for approval successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

            JsfUtil.addSuccessMessage(String.format("Country %s (%s) has been added successfully waiting for approval", country.getName(), country.getShortName()));
            
            country = new Country();
            
            recreate();

            prepareCreate();
            
            return "/country/list";
            
        } catch (DatabaseException de) {

            try {
                Log log = new Log();
                log.setAction("create country " + country.getName());
                log.setCreatedOn(new Date());
                log.setDescription("creation of country");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (de != null) {

                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception ex) {
            }

            if (de != null) {
                de.printStackTrace();
                JsfUtil.addErrorMessage(de.getMessage());
            }
        }
        
        return "create";
       
    }

    public Country getCountry(long id) {

        try {
            return countryDao.find(id);
        } catch (DatabaseException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public PaginationHelper getPaginationHelper() {

        if (pagination == null) {
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return countryDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(countryDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(CountryController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }

    public PaginationHelper getPaginationHelperUnauth() {

        if (paginationHelper == null) {
            paginationHelper = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return countryMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        countriesM = countryMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent();
                        
                        if(countriesM != null){
                            if(sortByName == false){
                                countriesM = countriesM.stream().sorted((x,y) -> {
                                        return x.getName().compareTo(y.getName());
                                    }).collect(Collectors.toList());
                            }else{
                                countriesM = countriesM.stream().sorted((x,y) -> {
                                        return y.getName().compareTo(x.getName());
                                    }).collect(Collectors.toList());
                            }
                        }
                        
                        return new ListDataModel(countriesM);
                    } catch (DatabaseException ex) {
                        Logger.getLogger(CountryController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationHelper;
    }

    public void recreate() {
        allCurrencies = null;
        allUnauthCurrencies = null;
    }

    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("countryupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            recreate();
            session.setAttribute("countryupdated", false);
        }

        if (allCurrencies == null) {
            allCurrencies = getPaginationHelper().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Currencies");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Currencies");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

        logService.log(log);

        return allCurrencies;
    }
    
    public void sortByName(){
        
        if(countriesM != null){
            if(sortByName == false){
                countriesM = countriesM.stream().sorted((x,y) -> {
                        return x.getName().compareTo(y.getName());
                    }).collect(Collectors.toList());
                
                sortByName = true;
            }else{
                countriesM = countriesM.stream().sorted((x,y) -> {
                        return y.getName().compareTo(x.getName());
                    }).collect(Collectors.toList());
                
                sortByName = false;
            }
        }
        
        allUnauthCurrencies = new ListDataModel(countriesM);
        
    }

    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("countryupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationHelper = null;
            recreate();
            session.setAttribute("countryupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch Unauthorized Currencies");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching Unauthorized Currencies");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

        logService.log(log);

        if (allUnauthCurrencies == null) {
            allUnauthCurrencies = getPaginationHelperUnauth().createPageDataModel();
        }

        return allUnauthCurrencies;
    }

    public List<Country> getAllCountry() throws DatabaseException {

        List<Country> countries = countryDao.findAll();
        return countries == null ? countries : countries.stream().filter(x -> x.isEnabled()).collect(Collectors.toList());
    }

    /**
     *
     * @param data
     * @param isName - used to control if it is name or shortname
     * @return
     */
    public boolean findUnauthorizedCountry(String data, boolean isName) {

        try {
            List<CountryM> countries = countryMDao.find(isName == true ? "name" : "shortName", isName == true ? country.getName() : country.getShortName());

            if (countries != null && !countries.isEmpty()) {

                CountryM cM = countries.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {
                    return true;
                }
            }

        } catch (Exception exception) {
            Logger.getLogger(CountryController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return false;
    }

//    @Transactional
    public String uploadCountry() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            List<CountryM> list = new ArrayList<>();

            if (fileName.toLowerCase().endsWith("csv")) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(uploadModel.getFile().getInputStream()));

                String line = null;

                Object userString = SecurityUtils.getSubject().getPrincipal();
                User user = userDao.findByKey("email", userString + "");

                int countUpload = 0, countDuplicate = 0;

                Map<String, String> map = new HashMap<>();

                int i = 0;
                while ((line = reader.readLine()) != null) {

                    if(i++ == 0)
                        continue;
                    
                    final String[] data = line.split(",");

                    if (data.length < 2) {
                        continue;
                    }
                    
                    String name = data[0].replaceAll("\"", "");
                    String shortName = data[1].replaceAll("\"", "");

                    if (countryDao.find("name", name) != null || countryDao.find("shortName", shortName) != null) {
                        countDuplicate++;
                        continue;
                    }
                    
                    if (countryMDao.find("name", name) != null || countryMDao.find("shortName", shortName) != null) {
                        countDuplicate++;
                        continue;
                    }

                    boolean result = map.entrySet().stream().anyMatch(x -> x.getKey().equalsIgnoreCase(data[0]) || x.getValue().equalsIgnoreCase(data[1]));

                    if (result == true) {
                        countDuplicate++;
                        continue;
                    }

                    map.put(data[0], data[1]);

                    CountryM cnty = new CountryM();
                    cnty.setEnabled(true);
                    cnty.setName(data[0].replaceAll("\"", ""));
                    cnty.setShortName(data[1].replaceAll("\"", ""));
                    cnty.setCreatedBy(user);
                    cnty.setCreatedOn(new Date());
                    cnty.setStatus(Status.PENDING);
                    
                    countUpload++;
                    list.add(cnty);
                }

                if(! list.isEmpty())
                    countryMDao.create(list);

                JsfUtil.addSuccessMessage(countUpload+" country record(s) has been upload and submitted for approval, ("+ countDuplicate +") Duplicate found");
                
            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
            
            JsfUtil.addErrorMessage("unable to upload record please try again later");
        }

        return "/country/upload";
    }

    @FacesConverter(value = "countryConverter")
    public static class CountryConverter implements javax.faces.convert.Converter {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {

            if (value == null || value.length() == 0) {
                return null;
            }

            CountryController countryController = (CountryController) context.getApplication()
                    .getELResolver().getValue(context.getELContext(), null, "countryController");

            return countryController.getCountry(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            if (value == null) {
                return null;
            }
            if (value instanceof Country) {
                Country o = (Country) value;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + value + " is of type " + value.getClass().getName() + "; expected type: " + Country.class.getName());
            }
        }
    }

}

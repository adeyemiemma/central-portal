/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.AirtimeFundingDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.AirtimeFundingMDao;
import com.flutterwave.flutter.clientms.model.AirtimeFunding;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.AirtimeFundingM;
import com.flutterwave.flutter.clientms.service.AirtimeHelper;
import com.flutterwave.flutter.clientms.service.model.VasMerchant;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "fundingController")
@SessionScoped
public class FundingController implements Serializable {
  
    @EJB
    private UserDao userDao;
    
    @EJB
    private AirtimeFundingDao airtimeFundingDao;
    
    @EJB
    private AirtimeFundingMDao airtimeFundingMDao;
    
    @Inject
    private AirtimeHelper airtimeHelper;
    
    private AirtimeFunding airtimeFunding;
    
    private PaginationHelper fundingPagination, fundingPaginationHelper;
    
    private DataModel allUnauthFunding = null;
    
    public AirtimeFunding getAirtimeFunding(){
        
        if(airtimeFunding == null)
            airtimeFunding = new AirtimeFunding();
        
        return airtimeFunding;
    }
    
    public void setAirtimeFunding(AirtimeFunding airtimeFunding){
        
        this.airtimeFunding = airtimeFunding;
    }
    
    public String initiateFunding(){
        
        Object userString = SecurityUtils.getSubject().getPrincipal();
        User user = null;
        
        try {
            user = userDao.findByKey("email", userString+"");
        } catch (DatabaseException ex) {
            Logger.getLogger(FundingController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        AirtimeFunding funding = null;
        
        try {
            funding = airtimeFundingDao.findByKey("reference", airtimeFunding.getReference() );
        } catch (DatabaseException ex) {
            Logger.getLogger(FundingController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        if(funding != null) {
            
            JsfUtil.addErrorMessage(String.format("Reference has been previously used"));
            
            allUnauthFunding = null;
            
            fundingPaginationHelper = null;
            
            return prepareCreate();
        }
        
        String merchantDetails = airtimeFunding.getMerchantId();
        
        String[] details =  merchantDetails.split("\\|");
        
        AirtimeFundingM airtimeFundingM = new AirtimeFundingM();
        airtimeFundingM.setAmount(airtimeFunding.getAmount());
        airtimeFundingM.setCreatedOn(new Date());
        airtimeFundingM.setCreatedBy(user);
        airtimeFundingM.setStatus(Status.PENDING);
        airtimeFundingM.setCurrency(airtimeFunding.getCurrency());
        airtimeFundingM.setMerchantId(details[1]);
        airtimeFundingM.setMerchantName(details[0]);
        airtimeFundingM.setReference(airtimeFunding.getReference());
        
        try {
            airtimeFundingMDao.create(airtimeFundingM);
        } catch (DatabaseException ex) {
            Logger.getLogger(FundingController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JsfUtil.addSuccessMessage(String.format("Funding has been logged for approval"));
        
//        allUnauthFunding = null;
        
        fundingPaginationHelper = null;
        
        return prepareCreate();
    }
    
    public String prepareCreate() {

        airtimeFunding = new AirtimeFunding();

        return "create";
    }
    
    
    public List<VasMerchant> getMerchants(){
        
        return airtimeHelper.getMerchantList();
    }
    
     public DataModel getUnauth(){
        
        Session session = SecurityUtils.getSubject().getSession();
        
        Object obj = session.getAttribute("fundingupdatedm");
        
        boolean status = obj == null ? false : Boolean.valueOf(obj+"");
        
        if(status == true){
            fundingPaginationHelper = null;
            session.setAttribute("fundingupdatedm", false);
        }
        
        Log log = new Log();
        log.setAction("Fetch Unauthorized Funding ");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching Unauthorized Funding");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");

//        logService.log(log);
        
        if(fundingPaginationHelper == null){
            allUnauthFunding = getPaginationHelperUnauthRate().createPageDataModel();
        }
        
        return allUnauthFunding;
    }
     
      public PaginationHelper getPaginationHelperUnauthRate() {

        if (fundingPaginationHelper == null) {
            fundingPaginationHelper = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return airtimeFundingMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(airtimeFundingMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(CurrencyController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return fundingPaginationHelper;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.RefundDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.maker.RefundMDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.viewmodel.ChargeBackFilter;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "refundController")
@SessionScoped
public class RefundController implements Serializable {
    
    @EJB
    private RefundMDao refundMDao;
    @EJB
    private RefundDao refundDao;
    
    @EJB
    private LogService logService;
    
    
    private Date startDate = null, endDate = null;
    
    private PaginationHelper paginationUnauth, pagination, paginationMerchant;
    
    private DataModel unauthdataModel, dataModel, dataModelMerchant;
    
    public PaginationHelper getPagination() {

        if (pagination == null) {
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return refundDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(refundDao.find(getPageFirstItem(), getPageSize(), startDate, endDate, null, null, null ).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }
    
    public PaginationHelper getPaginationMerchant() {

        if (paginationMerchant == null) {
            paginationMerchant = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return refundDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(refundDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationMerchant;
    }
    
    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("cchargeupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("cchargeupdated", false);
        }

        if (pagination == null) {
            dataModel = getPagination().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Charge Back ");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All charge back ");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log, null);

        return dataModel;
    }
    
    public DataModel getAllByMerchant() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("cchargeupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationMerchant = null;
            session.setAttribute("cchargeupdated", false);
        }

        if (paginationMerchant == null) {
            dataModelMerchant = getPagination().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Charge Back ");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All charge back ");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log, null);

        return dataModelMerchant;
    }
    
    /**
     * This called to reload 
     */
    public void recreate() {
        pagination = null;
        paginationUnauth = null;
    }
    
    
    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("crefundm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauth = null;
            session.setAttribute("crefundm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth ");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth ");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log, null);

        if (paginationUnauth == null) {
            unauthdataModel = getPaginationUnauth().createPageDataModel();
        }

        return unauthdataModel;
    }
    
    public PaginationHelper getPaginationUnauth() {

        if (paginationUnauth == null) {
            paginationUnauth = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return refundMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(refundMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauth;
    }
}

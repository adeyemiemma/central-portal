/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.service.SmsHelper;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.util.PageResult;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author emmanueladeyemi
 */
@RequestScoped
@Path(value = "/sms")
@Produces(value = MediaType.APPLICATION_JSON)
public class SmsApiController {
    
    @EJB
    private SmsHelper smsHelper;
    
    
    @Path(value = "/status")
    @GET
    public Response getStatus(){
        
        List<String> data = smsHelper.getStatus();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/balance")
    @GET
    public Response getBalance(){
        
        List data = smsHelper.getBalance();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/sender")
    @GET
    public Response getSender(){
        
        List data = smsHelper.getSender();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/provider")
    @GET
    public Response getProvider(){
        
        List data = smsHelper.getProvider();
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Path(value = "/transaction/all")
    @GET
    public Response getTransactions(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "sender") String sender,
            @QueryParam(value = "range") String date,
            @QueryParam(value = "searchValue") String search,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "status") String status){
        
        if("any".equalsIgnoreCase(sender)){
            sender = null;
        }
        
        if("any".equalsIgnoreCase(status)){
            status = null;
        }
        
        if("any".equalsIgnoreCase(provider)){
            provider = null;
        }
        
        
        String startDateStr = null, endDateStr = null;

        if (Utility.emptyToNull(date) != null) {

            Date startDate = null, endDate = null;
            try {
                String[] splitDate = date.split("-");
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                
                startDateStr = Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss");

                endDate = dateFormat.parse(splitDate[1]);
                
                endDateStr = Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException pe) {
//                startDate = endDate = null;
            }

        }
        
        PageResult data = smsHelper.getTransactions(start, length, sender , status, startDateStr, endDateStr, search, provider);
        
        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    
}

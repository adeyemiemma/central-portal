/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author emmanueladeyemi
 */
public class QueryTransactionResponse {

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(double rate) {
        this.rate = rate;
    }

    /**
     * @return the convertedAmount
     */
    public double getConvertedAmount() {
        return convertedAmount;
    }

    /**
     * @param convertedAmount the convertedAmount to set
     */
    public void setConvertedAmount(double convertedAmount) {
        this.convertedAmount = convertedAmount;
    }

    /**
     * @return the transactionResCode
     */
    public String getTransactionResCode() {
        return transactionResCode;
    }

    /**
     * @param transactionResCode the transactionResCode to set
     */
    public void setTransactionResCode(String transactionResCode) {
        this.transactionResCode = transactionResCode;
    }

    /**
     * @return the transactionResMessage
     */
    public String getTransactionResMessage() {
        return transactionResMessage;
    }

    /**
     * @param transactionResMessage the transactionResMessage to set
     */
    public void setTransactionResMessage(String transactionResMessage) {
        this.transactionResMessage = transactionResMessage;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
    
    private String reference;
    private String datetime;
    private double amount;
    private String currency;
    private double rate;
    @JsonProperty(value = "convertedamount")
    private double convertedAmount;
    @JsonProperty(value = "transactionrespcode")
    private String transactionResCode;
    @JsonProperty(value = "transactionrespmessage")
    private String transactionResMessage;
    @JsonProperty(value = "responsecode")
    private String responseCode;
    @JsonProperty(value = "responsemessage")
    private String responseMessage;
}

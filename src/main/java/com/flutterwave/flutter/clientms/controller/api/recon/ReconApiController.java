///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.flutterwave.flutter.clientms.controller.api.recon;
//
//import com.flutterwave.flutter.clientms.service.LogService;
//import com.flutterwave.flutter.core.exception.DatabaseException;
//import com.flutterwave.flutter.core.util.Page;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//import javax.ejb.EJB;
//import javax.enterprise.context.RequestScoped;
//import javax.inject.Inject;
//import javax.ws.rs.GET;
//import javax.ws.rs.POST;
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
//import javax.ws.rs.QueryParam;
//import javax.ws.rs.core.MediaType;
//import org.apache.shiro.SecurityUtils;
//import org.apache.shiro.session.Session;
//
///**
// *
// * @author adeyemi
// */
//@RequestScoped
//@Path("/recon")
//public class ReconApiController {
//
//    @EJB
//    private UserDao userDao;
//    @EJB
//    private PermissionDao permissionDao;
//    @EJB
//    private RoleDao roleDao;
//    @EJB
//    private LogService logService;
//    @EJB
//    private BankMDao bankMDao;
//    @EJB
//    private BankDao bankDao;
//    @Inject
//    private Global global;
//    
//
//    @Path("/users")
//    @GET
//    @Produces(value = MediaType.APPLICATION_JSON)
//    public PageResult getAllUsers(@QueryParam(value = "start") int start,
//            @QueryParam(value = "length") int length,
//            @QueryParam(value = "draw") int draw) {
//
//        if (length == 0) {
//            length = 10;
//        }
//
//        try {
//
//            PageResult<UserViewModel> pageResult = new PageResult<>();
//
//            
//            Page<User> users;
//            if (global.getDomainId() != null && !"".equals(global.getDomainId()))
//                users = userDao.findByDomainId(start, length, global.getDomainId());
//            else
//                users = userDao.find(start, length);
//
//            List<UserViewModel> viewModels = new ArrayList<>();
//
//            if (users.getCount() > 0) {
//
//                // This is called to transform merchants into merchant view models
//                viewModels = users.getContent().stream().map((User t) -> {
//                    UserViewModel model = new UserViewModel();
//                    model.setId(t.getId());
//                    model.setCreated(t.getCreated());
//                    model.setModified(t.getModified());
//                    model.setFirstName(t.getFirstName());
//                    model.setEmail(t.getEmail());
//                    model.setRole(t.getRole() != null ? t.getRole().getName().replaceAll("_", " ") : null);
//                    model.setLastName(t.getLastName());
//                    model.setEnabled(t.isEnabled());
//                    return model;
//                }).collect(Collectors.<UserViewModel>toList());
//
//            }
//
//            pageResult = new PageResult<>(viewModels, users.getCount(), users.getCount());
//
//            return pageResult;
//        } catch (DatabaseException ex) {
//            Logger.getLogger(ReconApiController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return new PageResult<>();
//    }
//    
//    @Path(value = "/bank/unauth")
//    @GET
//    @Produces(value = MediaType.APPLICATION_JSON)
//    public PageResult<BankViewModel> getUnauthBanks(@QueryParam(value = "start") int start,
//            @QueryParam(value = "length") int length,
//            @QueryParam(value = "draw") int draw){
//        
//        PageResult<BankViewModel> pageResult = new PageResult<>();
//        
//        try {
//            Page<BankM> page = bankMDao.findUnauth(start, length);
//            
//            if(page.getContent() != null && !page.getContent().isEmpty()){
//                
//                List<BankViewModel> bvms = page.getContent().stream().map((BankM m) -> {
//                    BankViewModel model = new BankViewModel();
//                    model.setName(m.getName());
//                    model.setShortName(m.getShortName());
//                    model.setId(m.getId());
//                    model.setCreatedOn(m.getCreatedOn());
//                    model.setCreatedBy(m.getCreatedBy() == null ? null : m.getCreatedBy().getFirstName() +" "+ m.getCreatedBy().getLastName());
//                    return model;
//                }).collect(Collectors.<BankViewModel>toList());
//                
//                pageResult = new PageResult<>(bvms, page.getCount(), page.getCount());
//            }
//            
//            return pageResult;
//            
//        } catch (DatabaseException ex) {
//            Logger.getLogger(ReconApiController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        return pageResult;
//    }
//    
//    @Path(value = "/bank")
//    @GET
//    @Produces(value = MediaType.APPLICATION_JSON)
//    public PageResult<BankViewModel> getBanks(@QueryParam(value = "start") int start,
//            @QueryParam(value = "length") int length,
//            @QueryParam(value = "draw") int draw){
//        
//        PageResult<BankViewModel> pageResult = new PageResult<>();
//        
//        try {
//            Page<Bank> page = bankDao.find(start, length);
//            
//            List<BankViewModel> bvms = page.getContent().stream().map((Bank m) -> {
//                    BankViewModel model = new BankViewModel();
//                    model.setName(m.getName());
//                    model.setShortName(m.getShortName());
//                    model.setId(m.getId());
//                    model.setCreatedOn(m.getCreatedOn());
//                    model.setCreatedBy(m.getCreatedBy() == null ? null : m.getCreatedBy().getFirstName() +" "+ m.getCreatedBy().getLastName());
//                    return model;
//                }).collect(Collectors.<BankViewModel>toList());
//                
//            pageResult = new PageResult<>(bvms, page.getCount(), page.getCount());
//            
////            pageResult = new PageResult<>(page.getContent(), page.getCount(), page.getCount());
//            
//            return pageResult;
//            
//        } catch (DatabaseException ex) {
//            Logger.getLogger(ReconApiController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        return pageResult;
//    }
//    
//    @Path("/bank/authorize/{id}")
//    @POST
//    @Produces(value = MediaType.APPLICATION_JSON)
//    public Map<String, String> authorizeBank(@PathParam(value = "id") String id) {
//        
//        Map<String, String> response = new HashMap<>();
//        
//        try {
//            
//            if (id == null) {
//
//                response.put("status-code", "01");
//                response.put("status", "No id provided");
//                return response;
//            }
//
//            Session session =  SecurityUtils.getSubject().getSession();
//
//            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal()+"");
//
//            BankM bankM = bankMDao.find(Long.parseLong(id));
//            
//            Log log = new Log();
//            log.setAction("Bank Authorization Started for "+ bankM.getName());
//            log.setCreatedOn(new Date());
//            log.setDescription("Bank Authorization started");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
//
//            logService.log(log);
//            
//            if(bankM.getBankId() <= 0){
//                Bank bank = new Bank();
//                bank.setName(bankM.getName());
//                bank.setCreatedOn(bankM.getCreatedOn());
//                bank.setShortName(bankM.getShortName());
//                bank.setEnabled(true);
//                bank.setCreatedBy(bankM.getCreatedBy());
//                bankDao.create(bank);
//            }else{
//                
//                Bank bank = bankDao.find(bankM.getBankId());
//                bank.setName(bankM.getName());
//                bank.setShortName(bankM.getShortName());
//                bank.setEnabled(bankM.isEnabled());
//                bank.setModified(new Date());
//                
//                bankDao.update(bank);
//            }
//            
//            bankM.setApproved(new Date());
//            bankM.setApprovedBy(user);
//            bankM.setStatus(Status.APPROVED);
//
//            response.put("status-code", "00");
//            response.put("status", "Success");
//            
//            bankMDao.update(bankM);
//            
//            log = new Log();
//            log.setAction("Bank Authorization Completed for "+ bankM.getName());
//            log.setCreatedOn(new Date());
//            log.setDescription("Bank Authorization Completed");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
//
//            logService.log(log);
//            
//            session.setAttribute("bankupdated", true);
//            session.setAttribute("bankupdatedm", true);
//
//            return response;
//            
//        } catch (DatabaseException ex) {
//            Logger.getLogger(ReconApiController.class.getName()).log(Level.SEVERE, null, ex);
//            response.put("status", "System error");
//            response.put("status-code", "96");
//            if(ex != null)
//                response.put("error", ""+ex.getMessage());
//            
//            try{
//                Log log = new Log();
//                log.setAction("Bank Authorization");
//                log.setCreatedOn(new Date());
//                log.setDescription("Bank Authorization error");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
//                
//                if(ex != null){
////                    log.setStatusMessage(ex.getMessage());
//                    
//                    String detailedString = Stream.of(ex.getStackTrace())
//                            .map(x -> x.toString()).collect(Collectors.joining(","));
//                    logService.log(log, detailedString);
//                }
//                else
//                    logService.log(log);
//            }catch(Exception e){
//            }
//        }
//        
//        return response;   
//    }
//   
//}

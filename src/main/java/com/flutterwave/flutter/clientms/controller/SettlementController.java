/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.controller.api.PosApiController;
import com.flutterwave.flutter.clientms.dao.AirtimeSettlementDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.RaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.SettlementCycleDao;
import com.flutterwave.flutter.clientms.dao.SettlementDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.SettlementMDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.SendSettlementModel;
import com.flutterwave.flutter.clientms.model.SettlementCycle;
import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.SettlementService;
import com.flutterwave.flutter.clientms.service.UtilityService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.UploadModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "settlementController")
@RequestScoped
public class SettlementController {

    /**
     * @return the sendSettlementModel
     */
    public SendSettlementModel getSendSettlementModel() {
        
        if(sendSettlementModel == null)
            sendSettlementModel = new SendSettlementModel();
        
        return sendSettlementModel;
    }

    /**
     * @param sendSettlementModel the sendSettlementModel to set
     */
    public void setSendSettlementModel(SendSettlementModel sendSettlementModel) {
      
        this.sendSettlementModel = sendSettlementModel;
    }

    @EJB
    private SettlementMDao settlementMDao;
    @EJB
    private SettlementDao settlementDao;
    @EJB
    private LogService logService;
    @EJB
    private SettlementCycleDao settlementCycleDao;
    @EJB
    private RaveMerchantDao raveMerchantDao;
    @EJB
    private UserDao userDao;
    @EJB
    private AirtimeSettlementDao airtimeSettlementDao;
    @EJB
    private SettlementService settlementService;
    @EJB
    private UtilityService utilityService;
    @EJB
    private PosMerchantDao posMerchantDao;

    private PaginationHelper paginationUnauth, pagination;

    private DataModel unauthdataModel, dataModel;

    private UploadModel uploadModel;
    
    private SendSettlementModel sendSettlementModel;

    public UploadModel getUploadModel() {

        if (uploadModel == null) {
            uploadModel = new UploadModel();
        }

        return uploadModel;
    }

    public void setUploadModel(UploadModel uploadModel) {

        this.uploadModel = uploadModel;
    }

    public PaginationHelper getPagination() {

        if (pagination == null) {
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return settlementDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(settlementDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }

    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("csettlementupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("csettlementupdated", false);
        }

        if (pagination == null) {
            dataModel = getPagination().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Settlement ");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All charge back ");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        return dataModel;
    }

    public String sendSettlementReport(){
           
        SendSettlementModel model = sendSettlementModel;
        
        if(model.getDate() == null || "".equalsIgnoreCase(model.getDate())){
            
            JsfUtil.addErrorMessage("Date must be provided in format yyyy-MM-dd");
            
            return "";
        }
        
        Date date = Utility.parseDate(model.getDate(), "yyyy-MM-dd");
        
        if(date == null){
            
            JsfUtil.addErrorMessage("Invalid date must be in format yyyy-MM-dd");
            
            return "";
        }
        
        if(model.getEmails()== null || "".equalsIgnoreCase(model.getEmails())){
            
            JsfUtil.addErrorMessage("Emails must be provided");
            
            return "";
        }
        
        if(model.getMerchantId()== null || "".equalsIgnoreCase(model.getMerchantId())){
            
            JsfUtil.addErrorMessage("Emails must be provided");
            
            return "";
        }
        
        
        settlementService.processMerchTransactions(date, model.getMerchantId(), model.getEmails(), model.getCategory());
        
        JsfUtil.addSuccessMessage("Notification is being processed, it will be sent once completed");
        
        return "";
    }
    
    public String sendPosSettlementReport(){
        
        SendSettlementModel model = sendSettlementModel;
        
        if(model.getDate() == null || "".equalsIgnoreCase(model.getDate())){
            
            JsfUtil.addErrorMessage("Date must be provided in format yyyy-MM-dd");
            
            return "";
        }
        
        Date date = Utility.parseDate(model.getDate(), "yyyy-MM-dd");
        
        if(date == null){
            
            JsfUtil.addErrorMessage("Invalid date must be in format yyyy-MM-dd");
            
            return "";
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        date = calendar.getTime();

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 59);

        Date endDate = calendar.getTime();
        
        if(model.getEmails()== null || "".equalsIgnoreCase(model.getEmails())){
            
            JsfUtil.addErrorMessage("Emails must be provided");
            
            return "";
        }
        
        if(model.getMerchantId()== null || "".equalsIgnoreCase(model.getMerchantId())){
            
            JsfUtil.addErrorMessage("Merchant Id must be provided");
            
            return "";
        }
        
        PosMerchant posMerchant = null;
        
        try {
            posMerchant = posMerchantDao.find(Long.parseLong(model.getMerchantId()));
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(PosApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(posMerchant == null){
            
            JsfUtil.addErrorMessage("Merchant not found");
            
            return "";
        }
        
        utilityService.sendPOSReport(posMerchant, date, endDate, model.getEmails());
        
//        settlementService.processMerchTransactions(date, model.getMerchantId(), model.getEmails(), model.getCategory());
        
        JsfUtil.addSuccessMessage("Notification is being processed, it will be sent once completed");
        
        return "";
    }
    
    /**
     * This called to reload
     */
    public void recreate() {
        pagination = null;
        paginationUnauth = null;
    }

    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("csettlementupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauth = null;
            session.setAttribute("csettlementupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth ");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth ");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        if (paginationUnauth == null) {
            unauthdataModel = getPaginationUnauth().createPageDataModel();
        }

        return unauthdataModel;
    }

    public PaginationHelper getPaginationUnauth() {

        if (paginationUnauth == null) {
            paginationUnauth = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return settlementMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(settlementMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauth;
    }

    // This section is for settlement cycle
    private SettlementCycle settlementCycle;

    public SettlementCycle getSettlementCycle() {

        if (settlementCycle == null) {
            settlementCycle = new SettlementCycle();
        }

        return settlementCycle;
    }

    public void setSettlementCycle(SettlementCycle settlementCycle) {

        this.settlementCycle = settlementCycle;
    }

    public String addSettlementCycle() {

        return "/settlement/cycle";
    }

    public void prepareSettlementCycle() {

        this.settlementCycle = new SettlementCycle();
    }

    public String createSettlementCycle() {

        try {
            if (settlementCycle.getMerchantId() == null) {
                JsfUtil.addErrorMessage("Merchant not selected");

                return "/settlement/cycle";
            }

            if (settlementCycle.getCycle() <= 0) {
                JsfUtil.addErrorMessage("Cycle must not be less than 1");
                return "/settlement/cycle";
            }

            Page<SettlementCycle> page = settlementCycleDao.find(0, 0, "rave", settlementCycle.getMerchantId());

            if (page != null) {

                if (page.getContent() != null && !page.getContent().isEmpty()) {
                    JsfUtil.addErrorMessage("Merchant cycle is already set");
                    return "/settlement/cycle";
                }
            }

            RaveMerchant raveMerchant = raveMerchantDao.find(Long.parseLong(settlementCycle.getMerchantId()));

            if (raveMerchant == null) {

                JsfUtil.addErrorMessage("Merchant not found");
                return "/settlement/cycle";
            }

            settlementCycle.setProduct("rave");
            settlementCycle.setCreatedOn(new Date());
            settlementCycle.setMerchantName(raveMerchant.getBusinessName());
            settlementCycle.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + ""));
            settlementCycleDao.create(settlementCycle);

            JsfUtil.addSuccessMessage("Settlement Cycle has been added for rave merchant " + settlementCycle.getMerchantName());

            settlementCycle = new SettlementCycle();

            return "/settlement/cycle";

        } catch (DatabaseException ex) {
            Logger.getLogger(SettlementController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SettlementController.class.getName()).log(Level.SEVERE, null, ex);
        }

        JsfUtil.addErrorMessage("Merchant cycle is already set");
        return "/settlement/cycle";

    }

    public String settlementAirtimeTransaction() {

        if (uploadModel.getFile() == null) {

            JsfUtil.addErrorMessage("File is empty, please upload file");

            return "/settlement/airtime";
        }

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();
            
            
            if (fileName.toLowerCase().endsWith("xlsx")) {

//                BufferedReader reader = new BufferedReader(new InputStreamReader(uploadModel.getFile().getInputStream()));

                String line = null;
                
                String username = SecurityUtils.getSubject().getPrincipal() + "";
                
                settlementService.processTransactionsXlsx(uploadModel.getFile().getInputStream(), fileName, username );
                
                JsfUtil.addSuccessMessage("File is has been queued for processing");

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only xlsx is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to get records");
        }

        return "/settlement/airtime";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import java.util.Date;
import java.util.Map;

/**
 *
 * @author emmanueladeyemi
 */
public class DashboardGraph {

    /**
     * @return the statusCodeMapper
     */
    public Map<String, String> getStatusCodeMapper() {
        return statusCodeMapper;
    }

    /**
     * @param statusCodeMapper the statusCodeMapper to set
     */
    public void setStatusCodeMapper(Map<String, String> statusCodeMapper) {
        this.statusCodeMapper = statusCodeMapper;
    }

    /**
     * @return the statusData
     */
    public Map<String, Double> getStatusData() {
        return statusData;
    }

    /**
     * @param statusData the statusData to set
     */
    public void setStatusData(Map<String, Double> statusData) {
        this.statusData = statusData;
    }

    /**
     * @return the totalVolume
     */
    public long getTotalVolume() {
        return totalVolume;
    }

    /**
     * @param totalVolume the totalVolume to set
     */
    public void setTotalVolume(long totalVolume) {
        this.totalVolume = totalVolume;
    }

    /**
     * @return the totalSum
     */
    public double getTotalSum() {
        return totalSum;
    }

    /**
     * @param totalSum the totalSum to set
     */
    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    /**
     * @return the totalSuccessful
     */
    public double getTotalSuccessful() {
        return totalSuccessful;
    }

    /**
     * @param totalSuccessful the totalSuccessful to set
     */
    public void setTotalSuccessful(double totalSuccessful) {
        this.totalSuccessful = totalSuccessful;
    }

    /**
     * @return the totalFee
     */
    public double getTotalFee() {
        return totalFee;
    }

    /**
     * @param totalFee the totalFee to set
     */
    public void setTotalFee(double totalFee) {
        this.totalFee = totalFee;
    }

    /**
     * @return the dateData
     */
    public Map<Date, Object> getDateData() {
        return dateData;
    }

    /**
     * @param dateData the dateData to set
     */
    public void setDateData(Map<Date, Object> dateData) {
        this.dateData = dateData;
    }
    
    private double totalSum;
    private double totalSuccessful;
    private double totalFee;
    private long totalVolume;
    private Map<Date, Object> dateData;
    private Map<String, Double> statusData;
    private Map<String, String> statusCodeMapper;
}

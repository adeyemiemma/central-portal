/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.json.Json;
import javax.json.JsonObject;
import javax.validation.constraints.NotNull;

/**
 *
 * @author emmanueladeyemi
 */
public class FetchComplianceRequest {

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }
    
    @JsonProperty(value = "merchantid")
    @NotNull(message = "Merchant Id must be provided")
    private String merchantId;
//    @NotNull(message = "Hash must be provided")
    private String hash = "";
    
    public String toHashableString(){
        
        StringBuilder builder = new StringBuilder();
        builder.append(merchantId);
        
        return builder.toString();
    }

    @Override
    public String toString() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("merchantId", merchantId)
                .add("Hash", hash).build();
        
        return jsonObject.toString();
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.api.model.RefundRequest;
import com.flutterwave.flutter.clientms.dao.RefundDao;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CoreTransactionDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.RaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.RefundMDao;
import com.flutterwave.flutter.clientms.model.Refund;
import com.flutterwave.flutter.clientms.model.CoreTransaction;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.RaveTransaction;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.RefundM;
import com.flutterwave.flutter.clientms.model.maker.UpdateModel;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 *
 * @author emmanueladeyemi
 */
@Path("/refund")
@RequestScoped
public class RefundApiController {

    @EJB
    private RaveMerchantDao raveMerchantDao;
    @EJB
    private MoneywaveMerchantDao moneywaveMerchantDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private RefundDao refundDao;
    @EJB
    private RefundMDao refundMDao;
    @EJB
    private UserDao userDao;
    @EJB
    private LogService logService;
    @EJB
    private RaveTransactionDao raveTransactionDao;
    @EJB
    private MoneywaveTransactionDao moneywaveTransactionDao;
    @EJB
    private CoreTransactionDao coreTransactionDao;
    @EJB
    private TransactionDao transactionDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private HttpUtil httpUtil;
    

    /**
     * This is called to authorize refund
     *
     * @param updateModel
     * @return
     */
    @Path("/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)    
    public Map<String, String> authorize(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            RefundM refundM = refundMDao.find(id);

            if (refundM == null) {
                response.put("status-code", "03");
                response.put("status", "refund charge record not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Merchant refund Authorization with id " + refundM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log, null);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (refundM.getModelId() <= 0 && status == true) {

                List<Refund> refunds = refundDao.find("flwReference", refundM.getFlwReference());
                
                if (refunds != null) {

                    Transaction t = transactionDao.findByKey("flwTxnReference", refundM.getFlwReference());
                
                    double totalRefund = refunds.stream().mapToDouble( x-> x.getAmount()).sum();
                    
                    if(t.getAmount() < totalRefund){
                        response.put("status-code", "02");
                        response.put("status", "Transaction has been fully refunded");
                        return response;
                    }
                }

                Refund refund = new Refund();

                refund.setCreatedBy(refundM.getCreatedBy());
                refund.setCreatedOn(refundM.getCreatedOn());
                refund.setCurrency(refundM.getCurrency());
                refund.setFlwReference(refundM.getFlwReference());
                refund.setMerchantId(refundM.getMerchantId());
                refund.setRrn(refundM.getRrn());
                refund.setApprovedOn(new Date());
                refund.setApprovedBy(user);
                refund.setAmount(refundM.getAmount());
                refund.setApprovedBy(user);
                refund.setApprovedOn(new Date());
                refund.setProvider(refundM.getProvider());
                refund.setTransactionAmount(refundM.getTransactionAmount());
                
                // end of setting intertional refund
                CoreTransaction coreTransaction = coreTransactionDao.findByKey("transactionReference", refund.getFlwReference());
                
                String merchantToken = null;
                
                if(coreTransaction == null){
                    CoreMerchant merchant = coreMerchantDao.findByKey("merchantID", refund.getMerchantId());
                    merchantToken = merchant.getTestToken(); // chnage to live
                }else
                    merchantToken = coreTransaction.getMerchant();
                
//                String refundResponse = httpUtil.logRefund("tk_kD9oqPzunj", refund.getAmount(), "FLWT00397022");
                String refundResponse = httpUtil.logRefund(merchantToken, refund.getAmount(), refundM.getFlwReference());
                
                if(refundResponse == null){
                    
                    refund = null;
                    response.put("status-code", "04");
                    response.put("status", "Unable to log refund at this time, please try again later ");
                    return response;
                }

//                if (status == true) {
                refundDao.create(refund);
//                }

            } else {

//                Refund refund = refundDao.findByKey("flwReference",refundM.getFlwReference());
//
//                if (refund != null && refund.getId() != refundM.getModelId()) {
//                    response.put("status-code", "02");
//                    response.put("status", "Duplicate refund with for refund with reference " + refund.getFlwReference());
//                    return response;
//                }
//
//                if (status == true) {
//
//                    refund = refundDao.find(refundM.getModelId());
//
////                    refund.setModifiedBy(refundM.getCreatedBy());
//                    refund.setModifiedOn(refundM.getCreatedOn());
//                    //                refund.setProduct(refundM.getProduct());
//
//                    // This section is for international refund
//                    refund.setProduct(refundM.getProduct());
//                    refund.setAccountNo(refundM.getAccountNo());
//                    refund.setCompanyName(refundM.getCompanyName());
//                    refund.setApprovedOn(new Date());
//                    refund.setApprovedBy(user);
//
//                    // This section is for international refund
//                    refund.setAddressLine1(refundM.getAddressLine1());
//                    refund.setAddressLine2(refundM.getAddressLine2());
//                    refund.setBank(refundM.getBank());
//                    refund.setContactPerson(refundM.getContactPerson());
//                    refund.setCountry(refundM.getCountry());
//                    refund.setCurrency(refundM.getCurrency());
//                    refund.setEmail(refundM.getEmail());
//                    refund.setFirstName(refundM.getFirstName());
//                    refund.setLastName(refundM.getLastName());
//                    refund.setMerchantType(refundM.getMerchantType());
////                    refund.setPassword("12345677");
//                    refund.setPhone(refundM.getPhone());
//                    refund.setProduct(refundM.getProduct());
//                    refund.setRcNumber(refundM.getRcNumber());
//                    refund.setEnabled(refundM.isEnabled());
//                    // end of setting pob refund
//
//                    refundDao.update(refund);
//
//                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            refundM.setApprovedOn(new Date());
            refundM.setApprovedBy(user);
            refundM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                refundM.setStatus(Status.APPROVED);
            } else {
                refundM.setStatus(Status.REJECTED);
            }

            updateRefundM(refundM);

            log = new Log();
            log.setAction("Merchant Authorization for " + refundM.getId() + " , " + refundM.getFlwReference());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("crefund", true);
            session.setAttribute("crefundm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Refund Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Refund Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log, null);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }
    
    @Transactional
    private void updateRefundM(RefundM refundM) throws DatabaseException{
        
        refundMDao.update(refundM);
    }

    @Path(value = "/transaction")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getTransaction(@QueryParam(value = "reference") String reference) {

        if (reference == null) {
            return null;
        }

        Map<String, Object> response = new HashMap<>();

        Transaction transaction;

        try {

            transaction = transactionDao.findByKey("flwTxnReference", reference);

            if (transaction == null) {
                String searchParam = "%" + reference + "%";

                List<Transaction> transactions = transactionDao.findByKeyLike("processorReference", searchParam);

                if (transactions == null || transactions.isEmpty()) {
                    response.put("responsecode", "05");
                    response.put("responsemessage", "transaction record not found");

                    return Response.status(Response.Status.OK).entity(response).build();
                }

                transaction = transactions.get(0);
            }

            response.put("responsecode", "00");
            response.put("responsemessage", "found");
            response.put("data", transaction);

            return Response.status(Response.Status.OK).entity(response).build();
                
        } catch (Exception ex) {
            Logger.getLogger(RefundApiController.class.getName()).log(Level.SEVERE, null, ex);

            response.put("responsecode", "RR");
            response.put("responsemessage", "Generic Error");
        }

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "/list")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getTransactions(@QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "merchantId") String merchantId,
            @QueryParam(value = "provider") String provider,
            @QueryParam(value = "searchString") String search) {

        try {

            Date startDate = null, endDate = null;
            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy ");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }
            Page<Refund> refunds = refundDao.find(start, length, startDate, endDate, merchantId, provider, search);
            PageResult pageResult = new PageResult(refunds.getContent(), refunds.getCount(), refunds.getCount());

            return Response.status(Response.Status.OK).entity(pageResult).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(RefundApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).entity(new PageResult()).build();
    }

    @Path(value = "/log")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response logRefund(@Valid RefundRequest request) {

        Map<String, Object> response = new HashMap<>();

        try {

            String reference = request.getReference();

            Transaction transaction;

            transaction = transactionDao.findByKey("flwTxnReference", reference);

            if (transaction == null) {
                String searchParam = "%" + reference + "%";

                Transaction transactions = transactionDao.findByKey("rrn", searchParam);

                if (transactions == null ) {
                    response.put("responsecode", "05");
                    response.put("responsemessage", "transaction record not found");

                    return Response.status(Response.Status.OK).entity(response).build();
                }

                transaction = transactions;
            }
            
            if(!"00".equals(transaction.getResponseCode())){
                
                response.put("responsecode", "06");
                response.put("responsemessage", "Refund Cannot be logged on failed transaction");
                return Response.status(Response.Status.OK).entity(response).build();
            }

//            long merchantId = 0;
//
//            CoreTransaction coreTransaction = coreTransactionDao.findByKey("transactionReference", reference);
//
//            if (coreTransaction != null) {
//                CoreMerchant coreMerchant = coreMerchantDao.findByKey("merchantID", coreTransaction.getMerchant());
//
//                if (coreMerchant == null) {
//                    coreMerchant = coreMerchantDao.findByKey("liveToken", coreTransaction.getMerchant());
//                }
//
//                if (coreMerchant != null) {
//                    merchantId = coreMerchant.getId();
//                }
//            }

            List<RefundM> refunds = refundMDao.find("flwReference", reference);
            
            if(refunds != null && !refunds.isEmpty()){
                RefundM refundM = refunds.stream().filter(x -> x.getStatus() == Status.PENDING).findFirst().orElse(null);
                
                if(refundM != null){
                    response.put("responsecode", "01");
                    response.put("responsemessage", "A pending refund has been logged on this transaction, waiting for approval");

                    return Response.status(Response.Status.OK).entity(response).build();
                }
            
            }
            
//            Refund cb = refundDao.findByKey("flwReference", reference);
//
//            if (cb != null) {
//
//                response.put("responsecode", "01");
//                response.put("responsemessage", "A refund has been logged on this transaction");
//
//                return Response.status(Response.Status.OK).entity(response).build();
//            }
            
            if(transaction.getAmount() < request.getAmount()){
                
                response.put("responsecode", "02");
                response.put("responsemessage", "Refund amount must not be greater than transaction amount ");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            RefundM refundM = new RefundM();
            refundM.setAmount(request.getAmount());
            refundM.setCurrency(transaction.getTransactionCurrency());
            refundM.setFlwReference(transaction.getFlwTxnReference());
            refundM.setCreatedBy(user);
            refundM.setCreatedOn(new Date());
            refundM.setStatus(Status.PENDING);
            refundM.setMerchantId(transaction.getMerchantId());
            refundM.setTransactionAmount(transaction.getAmount());
            refundM.setDatetime(transaction.getDatetime());
            refundM.setRrn(transaction.getRrn());
            refundM.setProvider(transaction.getProvider());

            refundMDao.create(refundM);

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("crefund", true);
            session.setAttribute("crefundm", true);

            response.put("responsecode", "00");
            response.put("responsemessage", "successful");

        } catch (DatabaseException ex) {
            Logger.getLogger(RefundApiController.class.getName()).log(Level.SEVERE, null, ex);

            response.put("responsecode", "RR");
            response.put("responsemessage", "Generic Error");
        }

        return Response.status(Response.Status.OK).entity(response).build();
    }

    private int getWeek(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int week = calendar.get(Calendar.WEEK_OF_YEAR);

        return week;
    }

    /**
     * year - start year of quarter
     *
     * @param year
     * @param date
     * @return
     */
    public int getQuarter(int year, Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int month = calendar.get(Calendar.MONTH);

        int yearInt = calendar.get(Calendar.YEAR);

        int value = 0;

        if (yearInt > year) {
            value += yearInt - year;
        }

        if (month <= 3) {
            return value += 1;
        }

        if (month <= 6) {
            return value += 2;
        }

        if (month <= 9) {
            return value += 3;
        } else {
            return value += 4;
        }
    }

    @Path(value = "/upload")
    @POST
    @Consumes("multipart/form-data")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response uploadRefunds(@Valid MultipartFormDataInput request) throws DatabaseException, IOException {

        Map<String, Object> response = new HashMap<>();

//        String range = request.getRange();
        Map<String, InputPart> map = request.getFormData();

        InputStream inputStream = map.get("file").getBody(InputStream.class, null);

        if (inputStream == null) {
            response.put("responsecode", "01");
            response.put("responsemessage", "No input file provided");
            return Response.status(Response.Status.OK).entity(response).build();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;

        Object userString = SecurityUtils.getSubject().getPrincipal();
        User user = userDao.findByKey("email", userString + "");

        int countUpload = 0, countDuplicate = 0;

        int i = 0;

        Map<String, String> dataMap = new HashMap<>();

        List<String> notFound = new ArrayList<>();
        List<Transaction> transactions = new ArrayList<>();

        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        while ((line = reader.readLine()) != null) {

            if (i++ == 0) {
                continue;
            }

            final String[] data = line.split(",");

            if (data.length < 2) {
                continue;
            }

            String productName = data[0].replaceAll("\"", "");
            String reference = data[1].replaceAll("\"", "");

            Transaction transaction = findTransaction(productName, reference);

            if (transaction == null) {
                notFound.add(productName + "  - " + reference);
                continue;
            }

            JsonObjectBuilder builder = Json.createObjectBuilder();
            builder.add("product", productName);
            builder.add("reference", reference);
            arrayBuilder.add(builder);

            transactions.add(transaction);
        }

        response.put("responsecode", "00");
        response.put("responsemessage", "Successful");
        response.put("found", arrayBuilder.build().toString());
        response.put("transactions", transactions);
        response.put("notfound", notFound);

        return Response.status(Response.Status.OK).entity(response).build();
    }

    private Transaction findTransaction(String product, String reference) throws DatabaseException {

        Transaction transaction;

        if ("rave".equalsIgnoreCase(product)) {

            RaveTransaction raveTransaction = raveTransactionDao.findByKey("flutterReference", reference);

            if (raveTransaction != null) {

                transaction = transactionDao.findByKey("flwReference", reference);

                return transaction;
            }

            String searchParam = "%" + reference + "%";

            List<Transaction> transactions = transactionDao.findByKeyLike("processorReference", searchParam);

            transaction = transactions.get(0);

        } else {

            transaction = transactionDao.findByKey("flwTxnReference", reference);

            if (transaction == null) {
                transaction = transactionDao.findByKey("rrn", reference);

                if (transaction == null) {
                    String searchParam = "%" + reference + "%";
                    List<Transaction> transactions = transactionDao.findByKeyLike("processorReference", searchParam);

                    if(transactions != null && !transactions.isEmpty())
                        transaction = transactions.get(0);
                }
            }
        }

        return transaction;
    }
    

}

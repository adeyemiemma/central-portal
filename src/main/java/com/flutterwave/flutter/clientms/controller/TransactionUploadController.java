/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.TransactionOther;
import com.flutterwave.flutter.clientms.model.TransactionUploadModel;
import com.flutterwave.flutter.clientms.service.recon.FileProcessor;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.flow.FlowScoped;
import javax.inject.Named;

/**
 *
 * @author emmanueladeyemi
 */
@Named
@FlowScoped(value = "transactionupload")
public class TransactionUploadController implements Serializable {
    
    @EJB
    private FileProcessor fileProcessor;
    @EJB
    private TransactionDao transactionDao;
    
    private TransactionUploadModel uploadModel;
    
    private List<Transaction> uploadedTransactions = new ArrayList<>();
    
    
    public List<Transaction> getUploadTransactions(){
        
        return uploadedTransactions;
    }
    
    public void prepareUpload(){
        
        uploadModel = new TransactionUploadModel();
        uploadedTransactions = new ArrayList<>();
    } 
    
    public TransactionUploadModel getTransactionUploadModel(){
        
        if(uploadModel == null)
            prepareUpload();
        
        return uploadModel;
    }
    
    public void setTransactionUploadModel(TransactionUploadModel uploadModel){
        
        this.uploadModel = uploadModel;
    }
    
    public String uploadTransactions(){
        
        
        if(getTransactionUploadModel() == null || getTransactionUploadModel().getFile() == null){
            JsfUtil.addErrorMessage("No file uploaded");
            
            return "transactionupload";
        }
        
        try{
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            List<TransactionOther> transactions = new ArrayList<>();

            if (fileName.toLowerCase().endsWith("xls") || fileName.toLowerCase().endsWith("xlsx")) {

                if (fileName.toLowerCase().endsWith("xls")) {
                    transactions = fileProcessor.processXls(uploadModel);
                } else {
                    transactions = fileProcessor.process(uploadModel);
                }
                
                List<Transaction> tranx = new ArrayList<>();
 
                if(transactions != null && !transactions.isEmpty()){
                    
                    tranx = transactions.stream().map((TransactionOther t) -> {
                        Transaction trnx = t.getTransaction(uploadModel.getProvider());
                        return trnx;
                    }).collect(Collectors.toList());
                    
                }
                
                uploadedTransactions = tranx;
                
                return "transactionpreview";
                
            } else {
                throw new IOException("Invalid file type");
            } 
        }catch(Exception ex){
            ex.printStackTrace();
            
            JsfUtil.addErrorMessage("Invalid file format");
            return "transactionupload";
        }
        
    }
    
    public String uploadCompleted(){
        
        try{
            List<Transaction> tranx = new ArrayList<>();

            if(uploadedTransactions != null){

                for(Transaction t : uploadedTransactions){

                    Transaction trans =  transactionDao.findByKey("rrn", t.getRrn());

                    if(trans != null)
                        continue;

                    tranx.add(t);
                }

                transactionDao.create(tranx);
            }
            
            JsfUtil.addSuccessMessage("Transaction has been uploaded successfully");
            
            return "transactionuploadcompleted";
            
        }catch(Exception ex){
            
            if(ex != null)
                ex.printStackTrace();
        }
        
        JsfUtil.addErrorMessage("Unable to upload transaction, pleasde try again later");
        
        return "transactionpreview";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.ConfigurationFeeDao;
import com.flutterwave.flutter.clientms.dao.GlobalRewardSettingDao;
import com.flutterwave.flutter.clientms.dao.GlobalRewardSettingLogDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.SettlementCycleDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.ConfigurationFeeMDao;
import com.flutterwave.flutter.clientms.model.ConfigurationFee;
import com.flutterwave.flutter.clientms.model.GlobalRewardSetting;
import com.flutterwave.flutter.clientms.model.GlobalRewardSettingLog;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.maker.ConfigurationFeeM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "configurationController")
@SessionScoped
public class ConfigurationController implements Serializable {

    @EJB
    private ConfigurationFeeDao configurationFeeDao;
    @EJB
    private ConfigurationFeeMDao configurationFeeMDao;
    @EJB
    private LogService logService;
    @EJB
    private UserDao userDao;
    @EJB
    private SettlementCycleDao settlementCycleDao;
    @EJB
    private GlobalRewardSettingDao rewardSettingDao;
    @EJB
    private GlobalRewardSettingLogDao rewardSettingLogDao;

    
    private ConfigurationFee configurationFee;
    
    
    private PaginationHelper paginationUnauth, pagination;
    
    private DataModel unauthdataModel, dataModel;
    
     /**
     * This creates fee for merchant
     * @return 
     */
    public String createFee() {

        try {
            ConfigurationFee p = configurationFeeDao.findByProduct(configurationFee.getProduct());

            if (p != null) {

                Log log = new Log();
                log.setAction("creating merchant fee");
                log.setCreatedOn(new Date());
                log.setDescription("created merchant fee");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Configuration Fee creation on product "+ configurationFee.getProduct().getName());

                JsfUtil.addErrorMessage("Fee exists for selected merchant ");
                return "/fee/create";
            }

            configurationFee.setCreated(new Date());
            configurationFee.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));

            //productDao.create(product);
            ConfigurationFeeM configurationFeeM=  new ConfigurationFeeM();
            configurationFeeM.setCreatedBy(configurationFee.getCreatedBy());
            configurationFeeM.setCreatedOn(configurationFee.getCreated());
            
            // This section is for international fee
            configurationFeeM.setInterAccountFee(configurationFee.getInterAccountFee());
            configurationFeeM.setInterAccountFeeExtra(configurationFee.getInterAccountFeeExtra());
            configurationFeeM.setInterAccountFeeCap(configurationFee.getInterAccountFeeCap());
            configurationFeeM.setInterAccountFeeType(configurationFee.getInterAccountFeeType());
            configurationFeeM.setInterCardFee(configurationFee.getInterCardFee());
            configurationFeeM.setInterCardFeeExtra(configurationFee.getInterCardFeeExtra());
            configurationFeeM.setInterCardFeeCap(configurationFee.getInterCardFeeCap());
            configurationFeeM.setInterCardFeeType(configurationFee.getInterCardFeeType());
            // end of setting intertional fee
            
            // This section is for pob fee
            configurationFeeM.setPobAccountFee(configurationFee.getPobAccountFee());
            configurationFeeM.setPobAccountFeeExtra(configurationFee.getPobAccountFeeExtra());
            configurationFeeM.setPobAccountFeeCap(configurationFee.getPobAccountFeeCap());
            configurationFeeM.setPobAccountFeeType(configurationFee.getPobAccountFeeType());
            configurationFeeM.setPobCardFee(configurationFee.getPobCardFee());
            configurationFeeM.setPobCardFeeExtra(configurationFee.getPobCardFeeExtra());
            configurationFeeM.setPobCardFeeCap(configurationFee.getPobCardFeeCap());
            configurationFeeM.setPobCardFeeType(configurationFee.getPobCardFeeType());
            // end of setting pob fee
            
            // This section is for micro transaction  fee
            configurationFeeM.setMicroTransactionAccountFee(configurationFee.getMicroTransactionAccountFee());
            configurationFeeM.setMicroTransactionAccountFeeType(configurationFee.getMicroTransactionAccountFeeType());
            configurationFeeM.setMicroTransactionCardFee(configurationFee.getMicroTransactionCardFee());
            configurationFeeM.setMicroTransactionCardFeeType(configurationFee.getMicroTransactionCardFeeType());
            configurationFeeM.setMicroTransactionAmount(configurationFee.getMicroTransactionAmount());
            // end of micro transaction  fee
           
            
            configurationFeeM.setStatus(Status.PENDING);
            configurationFeeM.setProduct(configurationFee.getProduct());
            
            configurationFeeM.setBvnFee(configurationFee.getBvnFee());
            
            configurationFeeMDao.create(configurationFeeM);

            Log log = new Log();
            log.setAction("creating configuration fee");
            log.setCreatedOn(new Date());
            log.setDescription("created configuration fee");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Configuration Fee has been created successfully and sent for approval");
            
            logService.log(log);

            configurationFee = new ConfigurationFee();
            
            recreate();
            
            return "/configuration/fee/list";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).error(null, ex);

            try {
                Log log = new Log();
                log.setAction("create configuration fee");
                log.setCreatedOn(new Date());
                log.setDescription("create configuration fee");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProductController.class.getName()).log( null, ex);
            }
        }

        return "/configuration/fee/create";
    }
    
    public PaginationHelper getPagination() {

        if (pagination == null) {
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return configurationFeeDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(configurationFeeDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).error(null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }
    
    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("cfeeupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("cfeeupdated", false);
        }

        if (pagination == null) {
            dataModel = getPagination().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Config fees");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All config fees");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        return dataModel;
    }
    
    /**
     * This called to reload 
     */
    public void recreate() {
        pagination = null;
        paginationUnauth = null;
    }
    
    
    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("cfeeupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauth = null;
            session.setAttribute("cfeeupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth fees");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth fees");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        if (paginationUnauth == null) {
            unauthdataModel = getPaginationUnauth().createPageDataModel();
        }

        return unauthdataModel;
    }
    
    public PaginationHelper getPaginationUnauth() {

        if (paginationUnauth == null) {
            paginationUnauth = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return configurationFeeMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(configurationFeeMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).error( null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauth;
    }
    
    public ConfigurationFee getConfigurationFee(){
        
        if(configurationFee == null){
            configurationFee = new ConfigurationFee();
        }
        
        return configurationFee;
    }
    
    public void setConfigurationFee(ConfigurationFee configurationFee){
        this.configurationFee = configurationFee;
    }
    
    public String prepareUpdate(long id) throws DatabaseException{
        
        configurationFee = configurationFeeDao.find(id);
        
        return "/configuration/fee/edit";
    }
    
    public String update(){
        
        try {
            ConfigurationFee p = configurationFeeDao.findByProduct(configurationFee.getProduct());

            if (p != null && p.getId() != configurationFee.getId()) {

                Log log = new Log();
                log.setAction("creating product fee");
                log.setCreatedOn(new Date());
                log.setDescription("created product fee");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Configuration Fee creation on product "+ configurationFee.getProduct().getName());

                JsfUtil.addErrorMessage("Fee exists for selected merchant ");
                return "/configuration/fee/create";
            }

//            configurationFee.setCreated(new Date());
//            configurationFee.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));

            //productDao.create(product);
            ConfigurationFeeM configurationFeeM=  new ConfigurationFeeM();
            configurationFeeM.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            configurationFeeM.setCreatedOn(new Date());
            
            // This section is for international fee
            configurationFeeM.setInterAccountFee(configurationFee.getInterAccountFee());
            configurationFeeM.setInterAccountFeeExtra(configurationFee.getInterAccountFeeExtra());
            configurationFeeM.setInterAccountFeeCap(configurationFee.getInterAccountFeeCap());
            configurationFeeM.setInterAccountFeeType(configurationFee.getInterAccountFeeType());
            configurationFeeM.setInterCardFee(configurationFee.getInterCardFee());
            configurationFeeM.setInterCardFeeExtra(configurationFee.getInterCardFeeExtra());
            configurationFeeM.setInterCardFeeCap(configurationFee.getInterCardFeeCap());
            configurationFeeM.setInterCardFeeType(configurationFee.getInterCardFeeType());
            // end of setting intertional fee
            
            // This section is for pob fee
            configurationFeeM.setPobAccountFee(configurationFee.getPobAccountFee());
            configurationFeeM.setPobAccountFeeExtra(configurationFee.getPobAccountFeeExtra());
            configurationFeeM.setPobAccountFeeCap(configurationFee.getPobAccountFeeCap());
            configurationFeeM.setPobAccountFeeType(configurationFee.getPobAccountFeeType());
            configurationFeeM.setPobCardFee(configurationFee.getPobCardFee());
            configurationFeeM.setPobCardFeeExtra(configurationFee.getPobCardFeeExtra());
            configurationFeeM.setPobCardFeeCap(configurationFee.getPobCardFeeCap());
            configurationFeeM.setPobCardFeeType(configurationFee.getPobCardFeeType());
            // end of setting pob fee
            
            // This section is for micro transaction  fee
            configurationFeeM.setMicroTransactionAccountFee(configurationFee.getMicroTransactionAccountFee());
            configurationFeeM.setMicroTransactionAccountFeeType(configurationFee.getMicroTransactionAccountFeeType());
            configurationFeeM.setMicroTransactionCardFee(configurationFee.getMicroTransactionCardFee());
            configurationFeeM.setMicroTransactionCardFeeType(configurationFee.getMicroTransactionCardFeeType());
            configurationFeeM.setMicroTransactionAmount(configurationFee.getMicroTransactionAmount());
            // end of micro transaction  fee
           
            
            configurationFeeM.setStatus(Status.PENDING);
            configurationFeeM.setProduct(configurationFee.getProduct());
            configurationFeeM.setModelId(configurationFee.getId());
            
            configurationFeeM.setBvnFee(configurationFee.getBvnFee());
            
            configurationFeeMDao.create(configurationFeeM);

            Log log = new Log();
            log.setAction("updating configuration fee");
            log.setCreatedOn(new Date());
            log.setDescription("updating configuration fee");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Configuration Fee has been updated successfully and sent for approval");
            
            logService.log(log);

            configurationFee = new ConfigurationFee();
            
            recreate();
            
            return "/configuration/fee/list";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).error( null, ex);

            try {
                Log log = new Log();
                log.setAction("updating configuration fee");
                log.setCreatedOn(new Date());
                log.setDescription("updating configuration fee");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProductController.class.getName()).error( null, ex);
            }
        }

        return "/configuration/fee/edit";
    }
    
    
    public void prepareCreate(){
        
        configurationFee = new ConfigurationFee();
    }
    
    
    /**
     * @return the rewardSetting
     */
    public GlobalRewardSetting getRewardSetting() {
        
        if(rewardSetting == null)
            rewardSetting = new GlobalRewardSetting();
        
        return rewardSetting;
    }

    /**
     * @param rewardSetting the rewardSetting to set
     */
    public void setRewardSetting(GlobalRewardSetting rewardSetting) {
        this.rewardSetting = rewardSetting;
    }
     
    private GlobalRewardSetting rewardSetting;
    
    public void setUpSetting(){
        
        rewardSetting = new GlobalRewardSetting();
    }
      
    public String createMerchantSetting(){
        
        
        try {
            List<GlobalRewardSetting> rs = rewardSettingDao.find(rewardSetting.getType(),
                    rewardSetting.getWeekDay(), rewardSetting.getStatus(), rewardSetting.getCurrency());
            
            if(rs != null && rs.size() > 0){
                
                JsfUtil.addErrorMessage("Reward with settings exist");
                return "";
            }
            
            rewardSetting.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            rewardSetting.setCreatedOn(new Date());
            rewardSetting.setUnit(1);
            
            GlobalRewardSettingLog rewardSettingLog = GlobalRewardSettingLog.from(rewardSetting);
            
            rewardSettingLogDao.create(rewardSettingLog);
            
            rewardSettingDao.create(rewardSetting);
            
            JsfUtil.addSuccessMessage("Setting has been created successfully");
            
            rewardSetting = new GlobalRewardSetting();
            
            return "";
        } catch (DatabaseException ex) {
            Logger.getLogger(RewardController.class.getName()).error(null, ex);
        }
        
        JsfUtil.addErrorMessage("Unable to add setting for merchant");
        return "";
    }
    
    
    public String prepareSetting(long id) throws DatabaseException{
        
        rewardSetting = rewardSettingDao.find(id);
        
        if(rewardSetting == null){
            
            JsfUtil.addErrorMessage("Reward not found");
            
            return "/reward/global/list";
        }   
        
        return "/reward/global/edit";
    }
    
    public String updateSetting(){
        
        try {
            List<GlobalRewardSetting> rs = rewardSettingDao.find(rewardSetting.getType(),
                    rewardSetting.getWeekDay(), rewardSetting.getStatus(), rewardSetting.getCurrency());
            
            if(rs != null && !rs.isEmpty()){
                
                GlobalRewardSetting setting = rs.get(0);
                
                if(setting.getId() != rewardSetting.getId()){
                    
                    JsfUtil.addErrorMessage("Reward with settings exist for merchant");
                    return "";
                }
            }
            
            rewardSetting.setModifiedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            rewardSetting.setModifiedOn(new Date());
            rewardSetting.setUnit(1);
            
            GlobalRewardSettingLog rewardSettingLog = GlobalRewardSettingLog.from(rewardSetting);
            rewardSettingLog.setCreatedOn(new Date());
            rewardSettingLog.setCreatedBy(rewardSetting.getModifiedBy());
            rewardSettingLogDao.create(rewardSettingLog);
            
            rewardSettingDao.update(rewardSetting);
            
            JsfUtil.addSuccessMessage("Setting has been updated successfully");
            
            rewardSetting = new GlobalRewardSetting();
            
            return "/reward/global/list";
        } catch (DatabaseException ex) {
            Logger.getLogger(RewardController.class.getName()).error(null, ex);
        }
        
        JsfUtil.addErrorMessage("Unable to update setting for merchant");
        return "";
        
    }
    
}

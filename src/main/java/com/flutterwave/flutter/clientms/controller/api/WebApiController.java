/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.api.model.AllExchangeRateRequest;
import com.flutterwave.flutter.clientms.controller.api.model.ChargeBackProofRequest;
import com.flutterwave.flutter.clientms.controller.api.model.CreateChargeBackRequest;
import com.flutterwave.flutter.clientms.controller.api.model.ExchangeRateRequest;
import com.flutterwave.flutter.clientms.controller.api.model.ExchangeRateResponse;
import com.flutterwave.flutter.clientms.controller.api.model.FetchComplianceRequest;
import com.flutterwave.flutter.clientms.controller.api.model.FetchComplianceResponse;
import com.flutterwave.flutter.clientms.controller.api.model.GetFeeResult;
import com.flutterwave.flutter.clientms.controller.api.model.GetMerchantFeeRequest;
import com.flutterwave.flutter.clientms.controller.api.model.GetMerchantFeeResponse;
import com.flutterwave.flutter.clientms.controller.api.model.GetSourceRequest;
import com.flutterwave.flutter.clientms.controller.api.model.GetSourceResponse;
import com.flutterwave.flutter.clientms.controller.api.model.MerchantComplianceRequest;
import com.flutterwave.flutter.clientms.controller.api.model.MerchantComplianceResponse;
import com.flutterwave.flutter.clientms.controller.api.model.MerchantRegistrationModel;
import com.flutterwave.flutter.clientms.controller.api.model.PullChargeBackRequest;
import com.flutterwave.flutter.clientms.controller.api.model.QueryTransactionRequest;
import com.flutterwave.flutter.clientms.controller.api.model.ResendComplianceInformation;
import com.flutterwave.flutter.clientms.controller.api.model.SendComplianceInformation;
import com.flutterwave.flutter.clientms.controller.api.model.SlaNotificationRequest;
import com.flutterwave.flutter.clientms.controller.api.model.SlaNotificationResponse;
import com.flutterwave.flutter.clientms.controller.api.model.ValidateMerchantRequest;
import com.flutterwave.flutter.clientms.controller.api.model.ValidateMerchantResponse;
import com.flutterwave.flutter.clientms.dao.ApiUserDao;
import com.flutterwave.flutter.clientms.dao.ChargeBackDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationFeeDao;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CoreTransactionDao;
import com.flutterwave.flutter.clientms.dao.CurrencyDao;
import com.flutterwave.flutter.clientms.dao.ExchangeRateDao;
import com.flutterwave.flutter.clientms.dao.MerchantCompliantDao;
import com.flutterwave.flutter.clientms.dao.MerchantDao;
import com.flutterwave.flutter.clientms.dao.MerchantFeeDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.ProviderDao;
import com.flutterwave.flutter.clientms.dao.RaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.dao.maker.ChargeBackDisputeMDao;
import com.flutterwave.flutter.clientms.dao.maker.ChargeBackMDao;
import com.flutterwave.flutter.clientms.model.ApiUser;
import com.flutterwave.flutter.clientms.model.ChargeBack;
import com.flutterwave.flutter.clientms.model.ConfigurationFee;
import com.flutterwave.flutter.clientms.model.CoreTransaction;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.MerchantCompliance;
import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.MoneywaveTransaction;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.RaveTransaction;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.maker.ChargeBackDisputeM;
import com.flutterwave.flutter.clientms.model.maker.ChargeBackM;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.model.products.MoneywaveMerchant;
import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.clientms.service.CardUtilService;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.NotificationManager;
import com.flutterwave.flutter.clientms.service.NotificationService;
import com.flutterwave.flutter.clientms.service.SmtpMailSender;
import com.flutterwave.flutter.clientms.service.UtilityService;
import com.flutterwave.flutter.clientms.service.model.SetupCardMerchantModel;
import com.flutterwave.flutter.clientms.util.ChargeBackState;
import com.flutterwave.flutter.clientms.util.ChargeBackStatus;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.util.WordUtil;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.validation.Valid;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.jboss.resteasy.spi.validation.ValidateRequest;

/**
 *
 * @author emmanueladeyemi
 */
@Path(value = "/web")
@RequestScoped
public class WebApiController {

    @EJB
    private MerchantCompliantDao merchantCompliantDao;
    @EJB
    private ApiUserDao apiUserDao;
    @EJB
    private LogService logService;
    @EJB
    private ProviderDao providerDao;
    @EJB
    private RaveMerchantDao raveMerchantDao;
    @EJB
    private MoneywaveMerchantDao moneywaveMerchantDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @Inject
    private WordUtil wordUtil;
    @EJB
    private MerchantDao merchantDao;
    @EJB
    private ChargeBackDao chargeBackDao;
    @EJB
    private ChargeBackMDao chargeBackMDao;
    @EJB
    private TransactionDao transactionDao;
    @EJB
    private RaveTransactionDao raveTransactionDao;
    @EJB
    private MoneywaveTransactionDao moneywaveTransactionDao;
    @EJB
    private CoreTransactionDao coreTransactionDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private ChargeBackDisputeMDao chargeBackDisputeMDao;
    @EJB
    private ExchangeRateDao exchangeRateDao;
    @EJB
    private CurrencyDao currencyDao;
    @EJB
    private MerchantFeeDao merchantFeeDao;
    @EJB
    private ConfigurationFeeDao configurationFeeDao;
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private NotificationManager notificationManager;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private UtilityService utilityService;
    @EJB
    private CardUtilService cardUtilService;
    
    private final Logger logger = Logger.getLogger(WebApiController.class);

    @Path(value = "/compliance/upload")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public MerchantComplianceResponse saveMerchantCompliantForm(@HeaderParam(value = "uniqueid") String uniqueId,
            @Valid MerchantComplianceRequest request) {

        MerchantComplianceResponse response = new MerchantComplianceResponse();

        try {

            Log log = new Log();
            log.setAction("Uploading Merchant Compliant Data");
            log.setCreatedOn(new Date());
            log.setDescription("Data: " + request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername("API Unique Id-" + uniqueId);

            logService.log(log);

            if (uniqueId == null) {

                log = new Log();
                log.setAction("Uploading Merchant Compliant Data");
                log.setCreatedOn(new Date());
                log.setDescription("");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);

                response.setResponseCode("03");
                response.setResponseMessage("unique reference not provided");

                logService.log(log, "unique reference not provided");

                return response;
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {
                response.setResponseCode("04");
                response.setResponseMessage("authentication failed");

                log = new Log();
                log.setAction("Uploading Merchant Compliant Data");
                log.setCreatedOn(new Date());
                log.setDescription("");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                logService.log(log, "authentication failed - user account not found");

                return response;
            }

//            String requestString = request.toHashableString();

//            requestString = apiUser.getPrivateKey() + "" + requestString;

//            String hashString = Utility.sha512(requestString);
//
//            if (!hashString.equals(request.getHash())) {
//
//                response.setResponseCode("05");
//                response.setResponseMessage("Invalid hash value");
//                
//                log = new Log();
//                log.setAction("Uploading Merchant Compliant Data");
//                log.setCreatedOn(new Date());
//                log.setDescription("");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                logService.log(log, "Invalid hash value");
//
//                return response;
//            }

            if("NG".equalsIgnoreCase(request.getCountry())){
                
                if(Utility.emptyToNull(request.getBvn()) == null){
                    
                    response.setResponseCode("06");
                    response.setResponseMessage("BVN must be provided ");
                    
                    log = new Log();
                    log.setAction("Uploading Merchant Compliant Data");
                    log.setCreatedOn(new Date());
                    log.setDescription("BVN must be provided");
                    log.setLevel(LogLevel.Severe);
                    log.setStatus(LogStatus.FAILED);
                    
                    logService.log(log, "BVN must be provided ");
                    
                    return response;
                }
            }
            
            MerchantCompliance compliance = merchantCompliantDao.findByKey("registeredNumber", request.getRegisteredNumber().trim());

            if (compliance != null) {
                
                if(compliance.getStatus() != Status.REJECTED){
                    
                    response.setResponseCode("06");
                    response.setResponseMessage("Registration Number " + compliance.getRegisteredNumber() + "  already exists ");

                    log = new Log();
                    log.setAction("Uploading Merchant Compliant Data");
                    log.setCreatedOn(new Date());
                    log.setDescription("Duplicate Registration for merchant with registered number");
                    log.setLevel(LogLevel.Severe);
                    log.setStatus(LogStatus.FAILED);
                    
                    logService.log(log, " Registration for merchant with registered number " + compliance.getRegisteredNumber());
                    
                    return response;
                }
            }
            
            String product = Stream.of(request.getProducts()).collect(Collectors.joining(","));
            
            MerchantCompliance complianceNew = merchantCompliantDao.findByMerchantId(request.getMerchantId(), product);
            
            if(compliance == null){
                
                if (complianceNew != null) {

//                    if(compliance.getStatus() != Status.REJECTED){
                        response.setResponseCode("06");
                        response.setResponseMessage("Compliance information exists for merchant " + complianceNew.getMerchantId());

                        log = new Log();
                        log.setAction("Uploading Merchant Compliant Data");
                        log.setCreatedOn(new Date());
                        log.setDescription("Duplicate Registration for merchant with id");
                        log.setLevel(LogLevel.Severe);
                        log.setStatus(LogStatus.FAILED);

                        logService.log(log, "Duplicate Registration for merchant with id  " + complianceNew.getMerchantId());

                        return response;
//                    }
                }
                
            }else {
                
                if(complianceNew != null){
                    
                    if(!complianceNew.getRegisteredNumber().equalsIgnoreCase(compliance.getRegisteredNumber())){
                        
                        response.setResponseCode("06");
                        response.setResponseMessage("Compliance information exists for merchant " + complianceNew.getRegisteredNumber());

                        log = new Log();
                        log.setAction("Uploading Merchant Compliant Data");
                        log.setCreatedOn(new Date());
                        log.setDescription("Duplicate Registration for merchant with id");
                        log.setLevel(LogLevel.Severe);
                        log.setStatus(LogStatus.FAILED);

                        logService.log(log, "Duplicate Registration for merchant with id  " + compliance.getRegisteredNumber());

                        return response;
                    }
                }
            }
            
            
            MerchantCompliance merchantCompliance = new MerchantCompliance();
            merchantCompliance.setAmlPolicyPath(request.getAmlPolicyPath());
            merchantCompliance.setCompanyRegDocumentPath(request.getCompanyRegDocumentPath());
            merchantCompliance.setCompanyType(request.getCompanyType());
            merchantCompliance.setContactAddress(request.getContactAddress());
            merchantCompliance.setContactEmail(request.getContactEmail());
            merchantCompliance.setContactName(request.getContactName());
            merchantCompliance.setContactPhone(request.getContactPhone());
            merchantCompliance.setCreatedOn(new Date());
            merchantCompliance.setDateofIncorporation(request.getDateofIncorporation());
            merchantCompliance.setDirectorIdPath(request.getDirectorIdPath());
            merchantCompliance.setIndustry(request.getIndustry());
            merchantCompliance.setOperatingLicencePath(request.getOperatingLicencePath());
            merchantCompliance.setOperationalAddress(request.getOperationalAddress());
            merchantCompliance.setProductWebUrl(request.getProductWebUrl());
            merchantCompliance.setProducts(Stream.of(request.getProducts()).collect(Collectors.joining(",")));
            merchantCompliance.setRegisteredAddress(request.getRegisteredAddress());
            merchantCompliance.setRegisteredNumber(request.getRegisteredNumber());
            merchantCompliance.setRegisteredName(request.getRegisteredName());
            merchantCompliance.setShareHolder1(request.getShareHolder1());
            merchantCompliance.setShareHolder2(request.getShareHolder2());
            merchantCompliance.setTradingName(request.getTradingName());
            merchantCompliance.setCountry(request.getCountry());
            merchantCompliance.setMerchantId(request.getMerchantId());
            merchantCompliance.setScumlCertificatePath(request.getScumlCertificatePath());
            merchantCompliance.setBvn(request.getBvn());
            merchantCompliance.setParentId(request.getParentId());
            
            if("Amadeus".equalsIgnoreCase(request.getCategory())){
                
                Map<String, String> dataSet = request.getCategoryDataSet();
                
                if(dataSet == null){
                    
                    response.setResponseCode("07");
                    response.setResponseMessage("Incomplete compliance data provided");

                    log = new Log();
                    log.setAction("Uploading Merchant Compliant Data");
                    log.setCreatedOn(new Date());
                    log.setDescription("Incomplete compliance data provided");
                    log.setLevel(LogLevel.Severe);
                    log.setStatus(LogStatus.FAILED);

                    logService.log(log, "Incomplete compliance data provided  " + merchantCompliance.getMerchantId());

                    return response;
                }
                
                String accountNo = dataSet.getOrDefault("accountno", null);
                
                if(Utility.emptyToNull(accountNo) == null) {
                
                    response.setResponseCode("07");
                    response.setResponseMessage("Account number must be provided");

                    log = new Log();
                    log.setAction("Uploading Merchant Compliant Data");
                    log.setCreatedOn(new Date());
                    log.setDescription("Account number must be provided");
                    log.setLevel(LogLevel.Severe);
                    log.setStatus(LogStatus.FAILED);

                    logService.log(log, "Account number must be provided  " + merchantCompliance.getMerchantId());

                    return response;
                }
                
                compliance.setNotifyCustomer(request.getSendEmail() == 1 ? true : false);
                
                String bankCode = dataSet.getOrDefault("bankcode", null);
                
                if(Utility.emptyToNull(bankCode) == null) {
                
                    response.setResponseCode("07");
                    response.setResponseMessage("Bank code must be provided");

                    log = new Log();
                    log.setAction("Uploading Merchant Compliant Data");
                    log.setCreatedOn(new Date());
                    log.setDescription("Account number must be provided");
                    log.setLevel(LogLevel.Severe);
                    log.setStatus(LogStatus.FAILED);

                    logService.log(log, "Bank code must be provided  " + merchantCompliance.getMerchantId());

                    return response;
                }
                
                String amadeusId = dataSet.getOrDefault("amadeusid", null);
                
                if(Utility.emptyToNull(amadeusId) == null){
                    
                    response.setResponseCode("07");
                    response.setResponseMessage("Amadeus Id must be provided in meta data");

                    log = new Log();
                    log.setAction("Uploading Merchant Compliant Data");
                    log.setCreatedOn(new Date());
                    log.setDescription("Amadeus id must be provided");
                    log.setLevel(LogLevel.Severe);
                    log.setStatus(LogStatus.FAILED);

                    logService.log(log, "Bank code must be provided  " + merchantCompliance.getMerchantId());

                    return response;
                }
                    
                String contactName = request.getContactName();
                
                String[] names = contactName.split(" ");
                
                SetupCardMerchantModel cardMerchantModel = new SetupCardMerchantModel();
                cardMerchantModel.setAccountNo(accountNo);
                cardMerchantModel.setBankCode(bankCode);
                cardMerchantModel.setEmail(request.getContactEmail());
                cardMerchantModel.setFirstName(names[0]);
                
                if(names.length > 1)
                    cardMerchantModel.setLastName(names[1]);
                else
                    cardMerchantModel.setLastName(names[0]);
                
                cardMerchantModel.setBusinessName(request.getTradingName());
                cardMerchantModel.setPhone(request.getContactPhone());
                cardMerchantModel.setCustomerId(amadeusId);
                
                String token = cardUtilService.setUpMerchantOnCoreAndProvisionCard(cardMerchantModel);
                
                if(token == null){
                    
                    response.setResponseCode("05");
                    response.setResponseMessage("Unable to verify account with bank");
                    
                    return response;
                }
                
                response.setToken(token);
            }
            
            if(request.getDescription() != null){
                try {
                    merchantCompliance.setDescription(new String(request.getDescription().getBytes(), "ISO-8859-1"));
                } catch (UnsupportedEncodingException ex) {
                    java.util.logging.Logger.getLogger(WebApiController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                
            merchantCompliance.setStatus(Status.PENDING);
            
            if(compliance != null){
                
                merchantCompliance.setId(compliance.getId());
                merchantCompliance.setResubmitted(compliance.getResubmitted()+1);
                merchantCompliance.setLastResubmitted(new Date());
                merchantCompliance.setLastProcessed(compliance.getLastProcessed());
                merchantCompliance.setProcessingCounter(compliance.getProcessingCounter());
                merchantCompliance.setRejectionReason(compliance.getRejectionReason());
                
                merchantCompliantDao.update(merchantCompliance);
            }
            else
                merchantCompliantDao.create(merchantCompliance);

            //sendNotification(request.getContactEmail(), request.getContactName());
            
            if(request.getSendEmail() == 1){
                notificationManager.sendComplianceSuccessEmail(request.getContactEmail(), 
                    request.getRegisteredName(), merchantCompliance.getProducts());
            }
            
            
            String emails =  configurationDao.getConfig("compliance_admin");
            
            String[] adminEmail = null;
            
            if(emails != null){
            
                adminEmail = emails.split(",");
                
                String username = Global.EMAIL_USERNAME;
                String password = Global.EMAIL_PASSWORD;

                SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT, true, Global.USE_STARTTLS, false);
                
                if(request.getSendEmail() == 1)
                    notificationManager.notifyComplianceAdmin(adminEmail, mailSender, merchantCompliance.getProducts(), request.getTradingName());
            }

//            String product = request.getProducts();

            if(Utility.emptyToNull(request.getParentId()) == null)
                wordUtil.getComplianceFile(request.getRegisteredName(), request.getProducts()[0],
                        request.getRegisteredAddress(), request.getCountry(), request.getContactEmail(), "", adminEmail);

            response.setResponseCode("00");
            response.setResponseMessage("Successful");
            
            utilityService.approveCompliance(merchantCompliance);

            return response;
        } catch (DatabaseException ex) {
            logger.error("error in web api", ex);
            response.setResponseCode("RR");
            response.setResponseMessage("Generic Error");
        }

        return response;
    }
    
    @Path(value = "/compliance/update")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public MerchantComplianceResponse updateMerchantCompliantForm(@HeaderParam(value = "uniqueid") String uniqueId,
            @Valid MerchantComplianceRequest request) {

        MerchantComplianceResponse response = new MerchantComplianceResponse();

        try {

            Log log = new Log();
            log.setAction("Uploading Merchant Compliant Data");
            log.setCreatedOn(new Date());
            log.setDescription("Data: " + request.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername("API Unique Id-" + uniqueId);

            logService.log(log);

            if (uniqueId == null) {

                log = new Log();
                log.setAction("Uploading Merchant Compliant Data");
                log.setCreatedOn(new Date());
                log.setDescription("");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);

                response.setResponseCode("03");
                response.setResponseMessage("unique reference not provided");

                logService.log(log, "unique reference not provided");

                return response;
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {
                response.setResponseCode("04");
                response.setResponseMessage("authentication failed");

                log = new Log();
                log.setAction("Uploading Merchant Compliant Data");
                log.setCreatedOn(new Date());
                log.setDescription("");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                logService.log(log, "authentication failed - user account not found");

                return response;
            }

//            String requestString = request.toHashableString();
//
//            requestString = apiUser.getPrivateKey() + "" + requestString;

//            String hashString = Utility.sha512(requestString);
//
//            if (!hashString.equals(request.getHash())) {
//
//                response.setResponseCode("05");
//                response.setResponseMessage("Invalid hash value");
//                
//                log = new Log();
//                log.setAction("Uploading Merchant Compliant Data");
//                log.setCreatedOn(new Date());
//                log.setDescription("");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                logService.log(log, "Invalid hash value");
//
//                return response;
//            }

            if("NG".equalsIgnoreCase(request.getCountry())){
                
                if(Utility.emptyToNull(request.getBvn()) == null){
                    
                    response.setResponseCode("06");
                    response.setResponseMessage("BVN must be provided ");
                    
                    log = new Log();
                    log.setAction("Uploading Merchant Compliant Data");
                    log.setCreatedOn(new Date());
                    log.setDescription("BVN must be provided");
                    log.setLevel(LogLevel.Severe);
                    log.setStatus(LogStatus.FAILED);
                    
                    logService.log(log, "BVN must be provided ");
                    
                    return response;
                }
            }
            
            MerchantCompliance compliance = merchantCompliantDao.findByKey("registeredNumber", request.getRegisteredNumber());

            if (compliance == null) {
                
                response.setResponseCode("06");
                response.setResponseMessage("Record not exist");

                log = new Log();
                log.setAction("Updating Merchant Compliant Data");
                log.setCreatedOn(new Date());
                log.setDescription("Record not found");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);

                logService.log(log, "Compliance record not found ");

                return response;
            }
            
            String product = Stream.of(request.getProducts()).collect(Collectors.joining(","));
            
            compliance = merchantCompliantDao.findByMerchantId(request.getMerchantId(), product);
            
            if (compliance == null) {
                                
//                if(compliance.getStatus() != Status.REJECTED){
                    response.setResponseCode("06");
                    response.setResponseMessage("Record not exist");

                    log = new Log();
                    log.setAction("Updating Merchant Compliant Data");
                    log.setCreatedOn(new Date());
                    log.setDescription("Record not found");
                    log.setLevel(LogLevel.Severe);
                    log.setStatus(LogStatus.FAILED);

                    logService.log(log, "Compliance record not found ");

                    return response;
//                }
            }

//            MerchantCompliance merchantCompliance = new MerchantCompliance();
            compliance.setAmlPolicyPath(request.getAmlPolicyPath());
            compliance.setCompanyRegDocumentPath(request.getCompanyRegDocumentPath());
            compliance.setCompanyType(request.getCompanyType());
            compliance.setContactAddress(request.getContactAddress());
            compliance.setContactEmail(request.getContactEmail());
            compliance.setContactName(request.getContactName());
            compliance.setContactPhone(request.getContactPhone());
            compliance.setModified(new Date());
            compliance.setDateofIncorporation(request.getDateofIncorporation());
            compliance.setDirectorIdPath(request.getDirectorIdPath());
            compliance.setIndustry(request.getIndustry());
            compliance.setOperatingLicencePath(request.getOperatingLicencePath());
            compliance.setOperationalAddress(request.getOperationalAddress());
            compliance.setProductWebUrl(request.getProductWebUrl());
            compliance.setProducts(Stream.of(request.getProducts()).collect(Collectors.joining(",")));
            compliance.setRegisteredAddress(request.getRegisteredAddress());
            compliance.setRegisteredNumber(request.getRegisteredNumber());
            compliance.setRegisteredName(request.getRegisteredName());
            compliance.setShareHolder1(request.getShareHolder1());
            compliance.setShareHolder2(request.getShareHolder2());
            compliance.setTradingName(request.getTradingName());
            compliance.setCountry(request.getCountry());
            compliance.setMerchantId(request.getMerchantId());
            compliance.setScumlCertificatePath(request.getScumlCertificatePath());
            compliance.setBvn(request.getBvn());
            compliance.setParentId(request.getParentId());
            
            if(request.getDescription() != null){
                try {
                    compliance.setDescription(new String(request.getDescription().getBytes(), "ISO-8859-1"));
                } catch (UnsupportedEncodingException ex) {
                    java.util.logging.Logger.getLogger(WebApiController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                
            compliance.setStatus(Status.PENDING);
            
//            if(compliance != null){
                
//                merchantCompliance.setId(compliance.getId());
                compliance.setResubmitted(compliance.getResubmitted()+1);
                compliance.setLastResubmitted(new Date());
                compliance.setLastProcessed(compliance.getLastProcessed());
                compliance.setProcessingCounter(compliance.getProcessingCounter());
                compliance.setRejectionReason(compliance.getRejectionReason());
                
                merchantCompliantDao.update(compliance);
//            }
//            else
//                merchantCompliantDao.create(merchantCompliance);

            //sendNotification(request.getContactEmail(), request.getContactName());
            
//            notificationManager.sendComplianceSuccessEmail(request.getContactEmail(), 
//                    request.getRegisteredName(), merchantCompliance.getProducts());
            
            
            String emails =  configurationDao.getConfig("compliance_admin");
            
            if(emails != null){
            
                String[] adminEmail = emails.split(",");
                
                String username = Global.EMAIL_USERNAME;
                String password = Global.EMAIL_PASSWORD;

                SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT, true, Global.USE_STARTTLS, false);
                notificationManager.notifyComplianceAdmin(adminEmail, mailSender, compliance.getProducts(), request.getTradingName());
            }

//            String product = request.getProducts();
//            wordUtil.getComplianceFile(request.getRegisteredName(), request.getProducts()[0],
//                    request.getRegisteredAddress(), request.getCountry(), request.getContactEmail());

            response.setResponseCode("00");
            response.setResponseMessage("Successful");
            
            utilityService.approveCompliance(compliance);

            return response;
        } catch (DatabaseException ex) {
            logger.error("error in web api", ex);
            response.setResponseCode("RR");
            response.setResponseMessage("Generic Error");
        }

        return response;
    }
    
    @Path(value = "/compliance/resend")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response resendCompliancedata(@Valid ResendComplianceInformation request, 
            @HeaderParam(value = "uniqueid") String uniqueId){
        
        Log log = new Log();
        log.setAction("Resend Compliance information");
        log.setCreatedOn(new Date());
        log.setDescription(request.toString());
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername("Api User: "+uniqueId);
        
        logService.log(log);
        
        
        MerchantComplianceResponse response = new MerchantComplianceResponse();
        
        
        MerchantCompliance compliance = null;
        
        try {
            compliance = merchantCompliantDao.findByKey("registeredNumber", request.getRcNumber());
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(WebApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(compliance == null){
            
            response.setResponseCode("04");
            response.setResponseMessage("Compliance record not found");
            
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }
        
        wordUtil.getComplianceFile(compliance.getRegisteredName(), compliance.getProducts(),
                    compliance.getOperationalAddress(), compliance.getCountry(), compliance.getContactEmail(), "", null);
        
        response.setResponseCode("00");
        response.setResponseMessage("Email has been sent to clients");

        return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            
//        return null;
    }
    
    @Path(value = "/compliance/info/send")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response sendCompliancedata(@Valid SendComplianceInformation request, 
            @HeaderParam(value = "uniqueid") String uniqueId){
        
        Log log = new Log();
        log.setAction("Send Compliance information");
        log.setCreatedOn(new Date());
        log.setDescription(request.toString());
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername("Api User: "+uniqueId);
        
        logService.log(log);
        
        
        MerchantComplianceResponse response = new MerchantComplianceResponse();
        
        
//        if(request.getCompanyName() == null || request.getContactAddress() == null ||
//                request.getCountry() == null || request.getEmail() == null || request.getProduct() == null){
//            
//            response.setResponseCode("00");
//            response.setResponseMessage("All neecessary ");
//        }
        
        wordUtil.getComplianceFile(request.getCompanyName(), request.getProduct(),
                    request.getContactAddress(), request.getCountry(), request.getEmail(), request.getCategory(), null);
        
        response.setResponseCode("00");
        response.setResponseMessage("Email has been sent to clients");

        return Response.status(Response.Status.OK).entity(response).build();
            
//        return null;
    }

    @Path(value = "/compliance/verify")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public ValidateMerchantResponse validMerchant(@HeaderParam(value = "uniqueid") String uniqueId,
            @Valid ValidateMerchantRequest request) {

        ValidateMerchantResponse response = new ValidateMerchantResponse();

        try {

            if (uniqueId == null) {

                response.setResponseCode("03");
                response.setResponseMessage("unique reference not provided");

                return response;
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {
                response.setResponseCode("04");
                response.setResponseMessage("authentication failed");

                return response;
            }

            String requestString = request.getRegistrationCode();

//            requestString = apiUser.getPrivateKey() + "" + requestString;
//
//            String hashString = Utility.sha512(requestString);
//
//            if (!hashString.equals(request.getHash())) {
//
//                response.setResponseCode("05");
//                response.setResponseMessage("Invalid hash value");
//
//                return response;
//            }

            MerchantCompliance compliance = merchantCompliantDao.findByKey("registeredNumber", request.getRegistrationCode());

            if (compliance == null) {

                response.setResponseCode("00");
                response.setResponseMessage("Successful");
                response.setStatusCode("R03");
                response.setStatusMessage("Compliance record not found for merchant with " + request.getRegistrationCode());

                return response;
            }

            response.setResponseCode("00");
            response.setResponseMessage("Successful");
            response.setEmail(compliance.getContactEmail());
            response.setStatusCode("R00");
            response.setStatusMessage("Compliance record found for merchant with " + request.getRegistrationCode());

            return response;

        } catch (Exception ex) {
            logger.error("error in web api", ex);
            response.setResponseMessage("Generic Error");
            response.setResponseCode("RR");
        }

        return response;
    }

    @Path(value = "/compliance/fetch")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response getMerchantComplianceInfo(@HeaderParam(value = "uniqueid") String uniqueId,
            @Valid FetchComplianceRequest request) {

        try {
            Map<String, String> response = new HashMap<>();

            FetchComplianceResponse complianceResponse = new FetchComplianceResponse();

            if (uniqueId == null) {

                response.put("responseCode", "03");
                response.put("responsemessage", "unique reference not provided");

                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {
                response.put("responseCode", "03");
                response.put("responsemessage", "Authorization failed");

                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }

//            String requestString = request.toHashableString();
//
//            requestString = apiUser.getPrivateKey() + "" + requestString;
//
//            String hashString = Utility.sha512(requestString);
//
//            if (!hashString.equals(request.getHash())) {
//
//                response.put("responseCode", "03");
//                response.put("responsemessage", "Invalid hash value");
//
//                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
//            }

            String product = apiUser.getUniqueId();

            MerchantCompliance compliance = merchantCompliantDao.findByMerchantId(request.getMerchantId() + "", product);
//        }

            if (("rave".equalsIgnoreCase(product) || "test".equalsIgnoreCase(product) ) && compliance == null) {
                compliance = merchantCompliantDao.findByMerchantId(request.getMerchantId() + "", "ravepay");
            }

            Map<String, Object> data = new HashMap<>();

            if (compliance == null) {

                data.put("responsecode", "01");
                data.put("responsemessage", "User compliance information not found");

                return Response.status(Response.Status.NOT_FOUND).entity(data).build();
            }

            complianceResponse.setAmlPolicyPath(compliance.getAmlPolicyPath());
            complianceResponse.setApprovedOn(compliance.getApprovedOn() != null ? Utility.formatDate(compliance.getApprovedOn(), "yyyy-MM-dd HH:mm") : null);
            complianceResponse.setCompanyRegDocumentPath(compliance.getCompanyRegDocumentPath());
            complianceResponse.setCompanyType(compliance.getCompanyType());
            complianceResponse.setContactAddress(compliance.getContactAddress());
            complianceResponse.setContactEmail(compliance.getContactEmail());
            complianceResponse.setContactName(compliance.getContactName());
            complianceResponse.setContactPhone(compliance.getContactPhone());
            complianceResponse.setCountry(compliance.getCountry());
            complianceResponse.setCreatedOn(compliance.getCreatedOn() != null ? Utility.formatDate(compliance.getCreatedOn(), "yyyy-MM-dd HH:mm") : null);
            complianceResponse.setDateofIncorporation(compliance.getDateofIncorporation());
            complianceResponse.setIndustry(compliance.getIndustry());
            complianceResponse.setMerchantId(compliance.getMerchantId());
            complianceResponse.setOperatingLicencePath(compliance.getOperatingLicencePath());
            complianceResponse.setOperationalAddress(compliance.getOperationalAddress());
            complianceResponse.setProductWebUrl(compliance.getProductWebUrl());
            complianceResponse.setProducts(compliance.getProducts());
            complianceResponse.setRegisteredAddress(compliance.getRegisteredAddress());
            complianceResponse.setRegisteredName(compliance.getRegisteredName());
            complianceResponse.setRegisteredNumber(compliance.getRegisteredNumber());
            complianceResponse.setScumlCertificatePath(compliance.getScumlCertificatePath());
            complianceResponse.setShareHolder1(compliance.getShareHolder1());
            complianceResponse.setShareHolder2(compliance.getShareHolder2());
            complianceResponse.setTradingName(compliance.getTradingName());
            complianceResponse.setStatus(compliance.getApprovedOn() != null ? "approved" : "pending");

            data.put("data", complianceResponse);
            data.put("responsecode", "00");
            data.put("responsemessage", "successful");

            return Response.status(Response.Status.OK).entity(data).build();
        } catch (DatabaseException ex) {
            logger.error("error in web api", ex);
        }
        
        Map<String, String> data = new HashMap<>();
        
        data.put("responsecode", "00");
        data.put("responsemessage", "successful");

        return Response.status(Response.Status.OK).entity(data).build();
    }

    @Path(value = "/compliance/approve")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response approvedCompliance(@HeaderParam(value = "uniqueid") String uniqueId,
            @Valid FetchComplianceRequest request) {

        try {
            
            Log log = new Log();
            log.setAction("Approve Compliance API");
            log.setCreatedOn(new Date());
            log.setDescription(request.toString());
            log.setLogDomain(LogDomain.API);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            
            logService.log(log);
            
            Map<String, String> response = new HashMap<>();

            FetchComplianceResponse complianceResponse = new FetchComplianceResponse();

            if (uniqueId == null) {

                response.put("responseCode", "03");
                response.put("responsemessage", "unique reference not provided");
                
                log = new Log();
                log.setAction("Approve Compliance API Response");
                log.setCreatedOn(new Date());
                log.setDescription(response.toString());
                log.setLogDomain(LogDomain.API);
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.FAILED);
                
                logService.log(log);

                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {
                response.put("responseCode", "03");
                response.put("responsemessage", "Authorization Failed");
                
                log = new Log();
                log.setAction("Approve Compliance API Response");
                log.setCreatedOn(new Date());
                log.setDescription(response.toString());
                log.setLogDomain(LogDomain.API);
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.FAILED);
                
                logService.log(log);

                
                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }

            String requestString = request.toHashableString();

            requestString = apiUser.getPrivateKey() + "" + requestString;

            String hashString = Utility.sha512(requestString);

            if (!hashString.equals(request.getHash())) {

                response.put("responseCode", "03");
                response.put("responsemessage", "Invalid hash value");
                
                log = new Log();
                log.setAction("Approve Compliance API Response");
                log.setCreatedOn(new Date());
                log.setDescription(response.toString());
                log.setLogDomain(LogDomain.API);
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.FAILED);
                
                logService.log(log);


                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

            String product = apiUser.getUniqueId();

            MerchantCompliance compliance = merchantCompliantDao.findByMerchantId(request.getMerchantId() + "", product);
//        }

            if (("rave".equalsIgnoreCase(product) || "test".equalsIgnoreCase(product) ) && compliance == null) {
                compliance = merchantCompliantDao.findByMerchantId(request.getMerchantId() + "", "ravepay");
            }

            Map<String, Object> data = new HashMap<>();

            if (compliance == null) {

                data.put("responsecode", "01");
                data.put("responsemessage", "Record not found");
                
                log = new Log();
                log.setAction("Approve Compliance API Response");
                log.setCreatedOn(new Date());
                log.setDescription(data.toString());
                log.setLogDomain(LogDomain.API);
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.FAILED);
                
                logService.log(log);


                return Response.status(Response.Status.OK).entity(data).build();
            }
            
            if(compliance.getApprovedOn() != null){
                
                data.put("responsecode", "02");
                data.put("responsemessage", "Record has been approved");
                
                log = new Log();
                log.setAction("Approve Compliance API Response");
                log.setCreatedOn(new Date());
                log.setDescription(data.toString());
                log.setLogDomain(LogDomain.API);
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.FAILED);
                
                logService.log(log);


                return Response.status(Response.Status.OK).entity(data).build();
            }
            

            if ("rave".equalsIgnoreCase(product) || "ravepay".equalsIgnoreCase(product)) {

                RaveMerchant merchant = raveMerchantDao.find(Long.parseLong(request.getMerchantId()));

                if (merchant != null) {

                    JsonObject jsonObject = httpUtil.goLiveRaveMerchant(merchant.getId() + "", true);

                    if (jsonObject == null) {
                        
                        data.put("responsecode", "01");
                        data.put("responsemessage", "Unable to approve merchant");
                        
                        log = new Log();
                        log.setAction("Approve Compliance API Response");
                        log.setCreatedOn(new Date());
                        log.setDescription(data.toString());
                        log.setLogDomain(LogDomain.API);
                        log.setLevel(LogLevel.Info);
                        log.setStatus(LogStatus.FAILED);

                        logService.log(log);


                        return Response.status(Response.Status.OK).entity(data).build();
                    }
                }

            }

            compliance.setApprovedOn(new Date());

            compliance.setApiUserId(uniqueId);

            compliance.setStatus(Status.APPROVED);
            merchantCompliantDao.update(compliance);
            
            data.put("responsecode", "00");
            data.put("responsemessage", "successful");
            
            log = new Log();
            log.setAction("Approve Compliance API Response");
            log.setCreatedOn(new Date());
            log.setDescription(data.toString());
            log.setLogDomain(LogDomain.API);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log);

            
            return Response.status(Response.Status.OK).entity(data).build();

        } catch (DatabaseException ex) {
            logger.error("error in web api", ex);
        }

        Map<String, String> data = new HashMap<>();

        data.put("responsecode", "01");
        data.put("responsemessage", "Unable to activate record");

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(data).build();
    }

    @Path(value = "/sources/fetch")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response getSources(@Valid GetSourceRequest request) {

        GetSourceResponse response = new GetSourceResponse();

        try {

            Log log = new Log();
            log.setAction("Fetching sources");
            log.setCreatedOn(new Date());
            log.setDescription("Fetching sources");
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername("API-" + request.getUniqueId());

            logService.log(log);

            if (request.getUniqueId() == null) {

                log.setDescription("");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);

                response.setResponseCode("03");
                response.setResponseMessage("unique reference not provided");

                logService.log(log, "unique reference not provided");

                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", request.getUniqueId());

            if (apiUser == null) {
                response.setResponseCode("04");
                response.setResponseMessage("authentication failed");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            String requestString = apiUser.getPrivateKey();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equals(request.getToken())) {

                response.setResponseCode("05");
                response.setResponseMessage("Invalid token value");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            List<Provider> providers = providerDao.findAll();

            if (providers != null) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                List<Map> list = providers.stream().map((Provider x) -> {
                    Map<String, Object> jsonObject = new HashMap<>();
                    jsonObject.put("name", x.getName());
                    jsonObject.put("shortname", x.getShortName());
                    jsonObject.put("id", x.getId());
                    jsonObject.put("createdon", dateFormat.format(x.getCreatedOn()));

                    return jsonObject;
                }).collect(Collectors.<Map>toList());

                response.setSources(list);
            }

            response.setResponseCode("00");
            response.setResponseMessage("successful");

            log.setDescription("");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log);

            return Response.status(Response.Status.OK).entity(response).build();

        } catch (Exception ex) {

            logger.error("error in web api", ex);

            response.setResponseCode("RR");
            response.setResponseMessage("Generic Error");
        }

        return Response.status(Response.Status.OK).entity(response).build();

    }

    @Path(value = "/sla")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response sendMerchantMail(@HeaderParam(value = "uniqueid") String uniqueId, @Valid SlaNotificationRequest request) {

        SlaNotificationResponse response = new SlaNotificationResponse();

        try {
            if (uniqueId == null) {

                response.setResponseCode("03");
                response.setResponseMessage("unique reference not provided");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            Log log = new Log();
            log.setAction("SLA Notification");
            log.setCreatedOn(new Date());
            log.setDescription(request.toHashableString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setUsername("API-" + uniqueId);

            logService.log(log);

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {
                response.setResponseCode("04");
                response.setResponseMessage("authentication failed");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashableString();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equals(request.getHash())) {

                log.setDescription("Invalid token value");
                log.setLogState(LogState.FINISH);
                log.setUsername("API-" + uniqueId);

                logService.log(log);

                response.setResponseCode("05");
                response.setResponseMessage("Invalid token value");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            String merchantName = "", email, country = "", address = "";

//            merchantName = request.getMerchantName();
            email = request.getEmail();

            switch (request.getProduct().toLowerCase()) {

                case "ravepay": 
                case "rave": {

                    Long id = Long.parseLong(request.getMerchantId());

                    RaveMerchant raveMerchant = raveMerchantDao.find(id);

                    if (raveMerchant == null) {

                        response.setResponseCode("06");
                        response.setResponseMessage("User account not found");

                        return Response.status(Response.Status.OK).entity(response).build();
                    }

                    country = raveMerchant.getCountry();
                    merchantName = raveMerchant.getBusinessName();
//                    address = raveMerchant.get

                    break;
                }

                case "moneywave": {

                    Long id = Long.parseLong(request.getMerchantId());

                    MoneywaveMerchant moneywaveMerchant = moneywaveMerchantDao.find(id);

                    if (moneywaveMerchant == null) {

                        response.setResponseCode("06");
                        response.setResponseMessage("User account not found");

                        return Response.status(Response.Status.OK).entity(response).build();
                    }

                    country = moneywaveMerchant.getCountry();
                    merchantName = moneywaveMerchant.getName();

                    break;
                }

                case "core":
                case "flutterwave core":
                case "flutterwavecore": {

//                    Long id = Long.parseLong(request.getMerchantId());
                    CoreMerchant coreMerchant = coreMerchantDao.findByKey("merchantID", request.getMerchantId());

                    if (coreMerchant == null) {

                        response.setResponseCode("06");
                        response.setResponseMessage("User account not found");

                        return Response.status(Response.Status.OK).entity(response).build();
                    }

                    country = "";
                    merchantName = coreMerchant.getCompanyname();

                }
            }

            wordUtil.getComplianceFile(merchantName, request.getProduct(), address, country, email, "", null);

            response.setResponseCode("00");
            response.setResponseMessage("successful");
            //        response.se            
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            response.setResponseCode("RR");
            response.setResponseMessage("Generic Error");
        }

        return Response.status(Response.Status.OK).entity(response).build();

    }

//    @Path(value = "/chargeback/log")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response createChargeBack(@HeaderParam(value = "uniqueid") String uniqueId, CreateChargeBackRequest request) {

        Map<String, String> response = new HashMap<>();

        try {
            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique reference not provided");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            String requestString = request.getReference();

            requestString = apiUser.getPrivateKey() + "" + requestString;

            String hashString = Utility.sha512(requestString);

            if (!hashString.equals(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            String reference = request.getReference();

            List<ChargeBackM> chargebacks = chargeBackMDao.find("flwReference", reference);

            if (chargebacks != null && !chargebacks.isEmpty()) {
                ChargeBackM chargeBackM = chargebacks.stream().filter(x -> x.getStatus() == Status.PENDING).findFirst().orElse(null);

                if (chargeBackM != null) {
                    response.put("responsecode", "06");
                    response.put("responsemessage", "A charge back has been logged on this transaction");

                    return Response.status(Response.Status.OK).entity(response).build();
                }

            }

            ChargeBack cb = chargeBackDao.findByKey("flwReference", reference);

            if (cb != null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Chargeback exist for reference");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            Transaction transaction = transactionDao.findByKey("flwTxnReference", reference);

            if (transaction == null) {

                response.put("responsecode", "07");
                response.put("responsemessage", "Transaction Not found");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            RaveTransaction raveTransaction = raveTransactionDao.findByKey("flutterReference", reference);

            long merchantId = 0;

            Product product = null;

            if (raveTransaction != null) {
                merchantId = raveTransaction.getMerchant().getId();

                product = productDao.findByKey("name", "rave");
            } else {

                MoneywaveTransaction moneywaveTransaction = moneywaveTransactionDao.findByKey("flutterReference", reference);

                if (moneywaveTransaction != null) {
                    merchantId = moneywaveTransaction.getMerchant().getId();

                    product = productDao.findByKey("name", "moneywave");
                } else {

                    CoreTransaction coreTransaction = coreTransactionDao.findByKey("transactionReference", reference);

                    product = productDao.findByKey("name", "core");

                    if (product == null) {
                        productDao.findByKey("name", "flutterwave core");
                    }

                    if (coreTransaction != null) {
                        CoreMerchant coreMerchant = coreMerchantDao.findByKey("testToken", coreTransaction.getMerchant());

                        if (coreMerchant == null) {
                            coreMerchant = coreMerchantDao.findByKey("liveToken", coreTransaction.getMerchant());
                        }

                        if (coreMerchant != null) {
                            merchantId = coreMerchant.getId();
                        }
                    }
                }
            }

            ChargeBackM chargeBackM = new ChargeBackM();
            chargeBackM.setAmount(transaction.getAmount());
            chargeBackM.setAuthenticationType(transaction.getAuthenticationModel());
            chargeBackM.setCurrency(transaction.getTransactionCountry());
            chargeBackM.setFlwReference(transaction.getFlwTxnReference());
            chargeBackM.setCreatedBy(null);
            chargeBackM.setCreatedOn(new Date());
            chargeBackM.setProduct(product);
            chargeBackM.setStatus(Status.PENDING);
            chargeBackM.setChargeBackStatus(ChargeBackStatus.NONE);
            chargeBackM.setMerchantId(transaction.getMerchantId());
            chargeBackM.setProductMid(merchantId);

            chargeBackMDao.create(chargeBackM);

            response.put("responsecode", "00");
            response.put("responsemessage", "Charge Back has been logged successfully");
            response.put("amount", transaction.getAmount() + "");
            response.put("authmodel", transaction.getAuthenticationModel());
            response.put("cardcountry", transaction.getCardCountry());
            response.put("transactiondate", transaction.getDatetime());
            response.put("reference", transaction.getFlwTxnReference());

            return Response.status(Response.Status.OK).entity(response).build();
        } catch (Exception ex) {

            if (ex != null) {
                ex.printStackTrace();
            }
        }

        response.put("responsecode", "RR");
        response.put("responsemessage", "Generic Error");

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "/chargeback/fetch")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response fetchChargeBack(@HeaderParam(value = "uniqueid") String uniqueId, PullChargeBackRequest request) {

        Map<String, Object> response = new HashMap<>();

        try {
            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique reference not provided");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashString();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equals(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            String startDateS = request.getStartDate();
            String endDateS = request.getEndDate();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            Date startDate = null, endDate = null;
            try {

                if (startDateS != null) {
                    startDate = dateFormat.parse(startDateS);
                }

                if (endDateS != null) {
                    endDate = dateFormat.parse(endDateS);
                }

            } catch (ParseException ex) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Invalid date range");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            int start = request.getStart();
            int length = request.getLength() == 0 ? 50 : request.getLength();

            List list = new ArrayList();
            List<ChargeBack> chargeList = null;

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            if ("rave".equalsIgnoreCase(uniqueId)) {
                chargeList = chargeBackDao.findByProduct(start, length, startDate, endDate, "rave", null);

            } else if ("moneywave".equalsIgnoreCase(uniqueId)) {
                chargeList = chargeBackDao.findByProduct(start, length, startDate, endDate, "moneywave", null);
            } else if ("core".equalsIgnoreCase(uniqueId)) {
                chargeList = chargeBackDao.findByProduct(start, length, startDate, endDate, "core", null);

                if (chargeList == null || chargeList.isEmpty()) {
                    chargeList = chargeBackDao.findByProduct(start, length, startDate, endDate, "flutterwave core", null);
                }
            }

            if (chargeList != null && !chargeList.isEmpty()) {
                list = chargeList.stream().map((ChargeBack back) -> {
                    Map<String, Object> map = new HashMap<>();
                    map.put("amount", back.getAmount());
                    map.put("flwreference", back.getFlwReference());
                    map.put("currency", back.getCurrency());
                    map.put("createdon", simpleDateFormat.format(back.getCreatedOn()));
                    map.put("status", back.getChargeBackState() == null ? ChargeBackState.LOST : back.getChargeBackState());
                    map.put("rrn", back.getRrn());

                    return map;
                }).collect(Collectors.<Map>toList());
            }

            response.put("responsecode", "00");
            response.put("responsemessage", "Successful");
            response.put("data", list);

            return Response.status(Response.Status.OK).entity(response).build();
        } catch (Exception ex) {

            if (ex != null) {
                ex.printStackTrace();
            }
        }

        response.put("responsecode", "RR");
        response.put("responsemessage", "Generic Error");

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "/chargeback/claim")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response logDisputedChargeBack(@HeaderParam(value = "uniqueid") String uniqueId, @Valid ChargeBackProofRequest request) {

        Map<String, Object> response = new HashMap<>();

        try {

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique reference not provided");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            String requestString = apiUser.getPrivateKey() + "" + request.toHashableString();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equals(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            ChargeBack chargeBack = chargeBackDao.findByKey("flwReference", request.getReference());

            if (chargeBack == null) {
                response.put("responsecode", "06");
                response.put("responsemessage", "ChargeBack not found for reference " + request.getReference());

                return Response.status(Response.Status.OK).entity(response).build();
            }

            if (chargeBack.getChargeBackState() == ChargeBackState.WON) {
                response.put("responsecode", "07");
                response.put("responsemessage", "Chargeback on transaction with ref " + chargeBack.getFlwReference() + " has been won");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            ChargeBackDisputeM chargeBackDisputeM = new ChargeBackDisputeM();
            chargeBackDisputeM.setApiUsername(uniqueId);
            chargeBackDisputeM.setChargeBack(chargeBack);
            chargeBackDisputeM.setCreatedOn(new Date());
            chargeBackDisputeM.setDescription(request.getMessage());
            chargeBackDisputeM.setImage(request.getImage());
            chargeBackDisputeM.setStatus(Status.PENDING);

            chargeBackDisputeMDao.create(chargeBackDisputeM);

            response.put("responsecode", "00");
            response.put("responsemessage", "Chargeback claim on trans. " + chargeBack.getFlwReference() + " has been logged");

            return Response.status(Response.Status.OK).entity(response).build();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        response.put("responsecode", "99");
        response.put("responsemessage", "Generic Error");

        return Response.status(Response.Status.OK).entity(response).build();
    }

    public Response registerMerchant(@Valid MerchantRegistrationModel model) {

        return null;
    }

    @Path(value = "/transaction/query")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response getFlutterwaveTransaction(@HeaderParam(value = "uniqueid") String uniqueId, @Valid QueryTransactionRequest request) {

        Map<String, Object> response = new HashMap<>();
        response.put("reference", request.getReference());

        try {

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique reference not provided");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            String requestString = apiUser.getPrivateKey() + "" + request.getReference();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equals(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            Transaction transaction = transactionDao.findByKey("flwTxnReference", request.getReference());

            if (transaction == null) {

                CoreTransaction coreTransaction = coreTransactionDao.findByKey("transactionReference", request.getReference());

                if (coreTransaction == null) {

                    response.put("responsecode", "06");
                    response.put("responsemessage", "Transaction reference not found");

                    return Response.status(Response.Status.OK).entity(response).build();
                }

                response.put("transactionrespcode", coreTransaction.getStatuscode());
                response.put("transactionrespmessage", coreTransaction.getStatus());
                response.put("currency", coreTransaction.getCurrency());
                response.put("amount", coreTransaction.getAmount());

                CoreMerchant coreMerchant = coreMerchantDao.findByKey("testToken", coreTransaction.getMerchant());

                if (coreMerchant != null) {
                    response.put("merchantid", coreMerchant.getMerchantID());
                }

                response.put("convertedamount", Utility.getExchangeRateInNGN(coreTransaction.getCurrency()) * coreTransaction.getAmount().doubleValue());
                response.put("rate", Utility.getExchangeRateInNGN(coreTransaction.getCurrency()));
                response.put("responsecode", "00");
                response.put("responsemessage", "Successful");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            response.put("transactionrespcode", transaction.getResponseCode());
            response.put("transactionrespmessage", transaction.getResponseMessage());
            response.put("currency", transaction.getTransactionCurrency());
            response.put("amount", transaction.getAmount());
            response.put("merchantid", transaction.getMerchantId());
            response.put("convertedamount", Utility.getExchangeRateInNGN(transaction.getTransactionCurrency()) * transaction.getAmount());
            response.put("rate", Utility.getExchangeRateInNGN(transaction.getTransactionCurrency() == null ? "NGN" : transaction.getTransactionCurrency()));
            response.put("responsecode", "00");
            response.put("responsemessage", "Successful");

            return Response.status(Response.Status.OK).entity(response).build();
        } catch (DatabaseException ex) {
            logger.error("error in web api", ex);
            response.put("responsecode", "RR");
            response.put("responsemessage", "Generic Error");
        }

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "/exchangerate")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response getExchangeRate(@HeaderParam(value = "uniqueid") String uniqueId, @Valid ExchangeRateRequest request) {

        Map<String, Object> response = new HashMap<>();

        try {
//        response.put("reference", request.getReference());

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique reference not provided");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }

            String requestString = apiUser.getPrivateKey() + "" + request.getOriginCurrency() + "" + request.getDestinationCurrency();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return Response.status(Response.Status.NOT_ACCEPTABLE).entity(response).build();
            }

            Currency originCurrency = currencyDao.findByKey("shortName", request.getOriginCurrency());

            if (originCurrency == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Origin  currency not found");

                return Response.status(Response.Status.NOT_FOUND).entity(response).build();
            }

            Currency destinationCurrency = currencyDao.findByKey("shortName", request.getDestinationCurrency());

            if (destinationCurrency == null) {

                response.put("responsecode", "06");
                response.put("responsemessage", "Destination currency not found");

                return Response.status(Response.Status.NOT_FOUND).entity(response).build();
            }

            ExchangeRate exchangeRate = exchangeRateDao.find(originCurrency, destinationCurrency);

            if (exchangeRate == null) {

                exchangeRate = exchangeRateDao.find(destinationCurrency, originCurrency);

                if (exchangeRate == null) {

                    response.put("responsecode", "07");
                    response.put("responsemessage", "Exchange rate currency not found");

                    return Response.status(Response.Status.NOT_FOUND).entity(response).build();
                }

                ExchangeRateResponse rateResponse = new ExchangeRateResponse();
                rateResponse.setDestinationCurrency(request.getDestinationCurrency());
                rateResponse.setOriginCurrency(request.getOriginCurrency());

//                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                double rate = 1 / exchangeRate.getAmount();

                BigDecimal decimal = new BigDecimal(rate);

                if (rate < 1) {

                    BigDecimal roundOff = decimal.setScale(4, BigDecimal.ROUND_HALF_EVEN);

                    rate = roundOff.doubleValue();

                } else {

                    BigDecimal roundOff = decimal.setScale(2, BigDecimal.ROUND_HALF_EVEN);

                    rate = roundOff.doubleValue();
                }

//                String value = decimalFormat.format(rate);
                rateResponse.setRate(rate);

//                rateResponse.setRate(rate);
                String date = Utility.formatDate(exchangeRate.getModified() == null ? exchangeRate.getCreatedOn() : exchangeRate.getModified(), "yyyy-MM-dd HH:mm:ss");
                rateResponse.setLastUpdated(date);

                return Response.status(Response.Status.OK).entity(rateResponse).build();
            }

            ExchangeRateResponse rateResponse = new ExchangeRateResponse();
            rateResponse.setDestinationCurrency(request.getDestinationCurrency());
            rateResponse.setOriginCurrency(request.getOriginCurrency());

//            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            double rate = exchangeRate.getAmount();

            BigDecimal decimal = new BigDecimal(rate);
            if (rate < 1) {

                BigDecimal roundOff = decimal.setScale(4, BigDecimal.ROUND_HALF_EVEN);

                rate = roundOff.doubleValue();

            } else {
//                BigDecimal decimal = new BigDecimal(rate);

                BigDecimal roundOff = decimal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                rate = roundOff.doubleValue();

            }

//            String value = decimalFormat.format(rate);
            rateResponse.setRate(rate);

            rateResponse.setRate(rate);
            String date = Utility.formatDate(exchangeRate.getCreatedOn(), "yyyy-MM-dd HH:mm:ss");
            rateResponse.setLastUpdated(date);

            return Response.status(Response.Status.OK).entity(rateResponse).build();
        } catch (Exception ex) {
            logger.error("error in web api", ex);
        }

        response.put("responsecode", "RR");
        response.put("responsemessage", "Generic error");

        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @Path(value = "/exchangerate/all")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response getAllExchangeRate(@HeaderParam(value = "uniqueid") String uniqueId, @Valid AllExchangeRateRequest request) {

        Map<String, Object> response = new HashMap<>();

        try {
//        response.put("reference", request.getReference());

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique reference not provided");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }

            String requestString = apiUser.getPrivateKey() + "" + uniqueId;

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

            List<ExchangeRate> exchangeRate = exchangeRateDao.findAll();

            response.put("responsecode", "00");
            response.put("responsemessage", "successful");
            
            if(exchangeRate == null || exchangeRate.isEmpty() ){
                

                response.put("rates", new ArrayList<>());
                
                return Response.status(Response.Status.OK).entity(response).build();
            }
            

            List<ExchangeRateResponse> exchangeRateResponses = exchangeRate.stream().map((ExchangeRate rate) -> {
                
                ExchangeRateResponse rateResponse = new ExchangeRateResponse();
                rateResponse.setDestinationCurrency(rate.getDestinationCurrency().getShortName());
                rateResponse.setOriginCurrency(rate.getOriginCurrency().getShortName());
                rateResponse.setLastUpdated(Utility.formatDate(rate.getModified() == null ? rate.getCreatedOn() : rate.getModified(), "yyyy-MM-dd HH:mm:ss"));
                
                double rateAmount =  rate.getAmount();

                BigDecimal decimal = new BigDecimal(rateAmount);

                if (rateAmount < 1) {

                    BigDecimal roundOff = decimal.setScale(4, BigDecimal.ROUND_HALF_EVEN);

                    rateAmount = roundOff.doubleValue();

                } else {

                    BigDecimal roundOff = decimal.setScale(2, BigDecimal.ROUND_HALF_EVEN);

                    rateAmount = roundOff.doubleValue();
                }
                
                rateResponse.setRate(rateAmount);
                
                return rateResponse;
            }).collect(Collectors.<ExchangeRateResponse>toList());
             
            response.put("rates", exchangeRateResponses);
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (Exception ex) {
            logger.error("error in web api", ex);
        }

        response.put("responsecode", "RR");
        response.put("responsemessage", "Generic error");

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "/merchant/fee")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @ValidateRequest
    public Response getMerchantRate(@HeaderParam(value = "uniqueid") String uniqueId, @Valid GetMerchantFeeRequest request) {

        Map<String, Object> response = new HashMap<>();

        try {

            if (uniqueId == null) {

                response.put("responsecode", "03");
                response.put("responsemessage", "unique reference not provided");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            ApiUser apiUser = apiUserDao.findByKey("uniqueId", uniqueId);

            if (apiUser == null) {

                response.put("responsecode", "04");
                response.put("responsemessage", "authentication failed");

                return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
            }

            String requestString = apiUser.getPrivateKey() + "" + request.getToken();

            String hashString = Utility.sha512(requestString);

            if (!hashString.equalsIgnoreCase(request.getHash())) {

                response.put("responsecode", "05");
                response.put("responsemessage", "Invalid hash value");

                return Response.status(Response.Status.NOT_ACCEPTABLE).entity(response).build();
            }

            CoreMerchant coreMerchant = coreMerchantDao.findByKey(Global.TOKEN_STRING_C, request.getToken());

            long id = 0;
            
//            boolean fromAPI = false;
            
            Product product = productDao.findByKey("name", "core");

            if (product == null) {
                product = productDao.findByKey("name", "flutterwave core");
            }
            
            MerchantFee merchantFee = null;
            
//            merchantFee = merchantFeeDao.findByProductAndMerchant(product, id);
            
            if (coreMerchant == null) {
                
                
                merchantFee = merchantFeeDao.findByKey("merchantToken", request.getToken());
                
                
//                String apiKey = configurationDao.getConfig("wallet_ms_api_key");
//                String url = null;
//                try {
//                    url = configurationDao.getConfig("wallet_ms_base")+"/merchant/"+URLEncoder.encode(request.getToken(), "UTF-8");
//                } catch (UnsupportedEncodingException ex) {
//                    logger.error("error in web api", ex);
//                }
//                
//                Map<String, String> header = new HashMap<>();
//                header.put("Content-Type", "application/json");
//                header.put("Authorization", "Bearer "+apiKey);
//                
////                if(url != nu)
//                String resString = httpUtil.doHttpGet(url, header);
//                
//                if(resString == null){
//                
//                response.put("responsecode", "05");
//                response.put("responsemessage", "Unknown merchant");
//
//                return Response.status(Response.Status.NOT_FOUND).entity(response).build();
//                }
//                
//                JsonObject jsonObject = Json.createReader(new ByteArrayInputStream(resString.getBytes())).readObject();
//                    
//                String status = jsonObject.getString("status", "failed");
//
//                if(!"success".equalsIgnoreCase(status)){
//                    response.put("responsecode", "05");
//                    response.put("responsemessage", "Unknown merchant");
//
//                    return Response.status(Response.Status.NOT_FOUND).entity(response).build();
//                }
//                
//                fromAPI = true;
                
            }else{
                
                id = coreMerchant.getId();
                
                merchantFee = merchantFeeDao.findByProductAndMerchant(product, id);
            }

            
            

//            if(fromAPI == false)
//                
//            else
//                merchantFee = merchantFeeDao.findByKey("merchantToken", request.getToken());

            GetMerchantFeeResponse feeResponse = new GetMerchantFeeResponse();
            feeResponse.setToken(request.getToken());

            if (merchantFee == null) {
                ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);

                if (configurationFee == null) {
                    response.put("responsecode", "06");
                    response.put("responsemessage", "Fee is not found");

                    return Response.status(Response.Status.OK).entity(response).build();
                }

                feeResponse.setCreatedOn(Utility.formatDate(configurationFee.getCreated(), "yyyy-MM-dd HH:mm:ss"));

                Map<String, Object> feeMap = new HashMap<>();

                GetFeeResult feeResult = new GetFeeResult();
                feeResult.setCap(configurationFee.getInterAccountFeeCap());
                feeResult.setExtra(configurationFee.getInterAccountFeeExtra());
                feeResult.setFee(configurationFee.getInterAccountFee());
                feeResult.setType(configurationFee.getInterAccountFeeType().name());

                feeMap.put("international", feeResult);

                feeResult.setCap(configurationFee.getPobAccountFeeCap());
                feeResult.setExtra(configurationFee.getPobAccountFeeExtra());
                feeResult.setFee(configurationFee.getPobAccountFee());
                feeResult.setType(configurationFee.getPobAccountFeeType().name());

                feeMap.put("local", feeResult);

                Map<String, Object> microMap = new HashMap<>();
                microMap.put("amount", configurationFee.getMicroTransactionAmount());
                microMap.put("fee", configurationFee.getMicroTransactionAccountFee());
                microMap.put("type", configurationFee.getMicroTransactionAccountFeeType().name());

                feeMap.put("micro_transaction", microMap);

                feeResponse.setAccount(feeMap);

                feeMap = new HashMap<>();

                feeResult = new GetFeeResult();
                feeResult.setCap(configurationFee.getInterCardFeeCap());
                feeResult.setExtra(configurationFee.getInterCardFeeExtra());
                feeResult.setFee(configurationFee.getInterCardFee());
                feeResult.setType(configurationFee.getInterCardFeeType().name());

                feeMap.put("international", feeResult);

                feeResult.setCap(configurationFee.getPobCardFeeCap());
                feeResult.setExtra(configurationFee.getPobCardFeeExtra());
                feeResult.setFee(configurationFee.getPobCardFee());
                feeResult.setType(configurationFee.getPobCardFeeType().name());

                feeMap.put("local", feeResult);

                microMap = new HashMap<>();
                microMap.put("amount", configurationFee.getMicroTransactionAmount());
                microMap.put("fee", configurationFee.getMicroTransactionCardFee());
                microMap.put("type", configurationFee.getMicroTransactionCardFeeType().name());

                feeMap.put("micro_transaction", microMap);

                feeResponse.setCard(feeMap);
                
                feeResponse.setBvn(configurationFee.getBvnFee());

                return Response.status(Response.Status.OK).entity(feeResponse).build();
            }

//            GetMerchantFeeResponse feeResponse = new GetMerchantFeeResponse();
//            feeResponse.setToken(request.getToken());
            if (merchantFee.getCreated() != null) {
                feeResponse.setCreatedOn(Utility.formatDate(merchantFee.getCreated(), "yyyy-MM-dd HH:mm:ss"));
            } else {
                feeResponse.setCreatedOn(Utility.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
            }

            Map<String, Object> feeMap = new HashMap<>();

            GetFeeResult feeResult = new GetFeeResult();
            feeResult.setCap(merchantFee.getInterAccountFeeCap());
            feeResult.setExtra(merchantFee.getInterAccountFeeExtra());
            feeResult.setFee(merchantFee.getInterAccountFee());
            feeResult.setType(merchantFee.getInterAccountFeeType().name());

            feeMap.put("international", feeResult);

            feeResult.setCap(merchantFee.getPobAccountFeeCap());
            feeResult.setExtra(merchantFee.getPobAccountFeeExtra());
            feeResult.setFee(merchantFee.getPobAccountFee());
            feeResult.setType(merchantFee.getPobAccountFeeType().name());

            feeMap.put("local", feeResult);

            Map<String, Double> microMap = new HashMap<>();
            microMap.put("amount", merchantFee.getMicroTransactionAmount());
            microMap.put("fee", merchantFee.getMicroTransactionAccountFee());

            feeMap.put("micro_transaction", microMap);

            feeResponse.setAccount(feeMap);

            feeMap = new HashMap<>();

            feeResult = new GetFeeResult();
            feeResult.setCap(merchantFee.getInterCardFeeCap());
            feeResult.setExtra(merchantFee.getInterCardFeeExtra());
            feeResult.setFee(merchantFee.getInterCardFee());
            feeResult.setType(merchantFee.getInterCardFeeType().name());

            feeMap.put("international", feeResult);

            feeResult.setCap(merchantFee.getPobCardFeeCap());
            feeResult.setExtra(merchantFee.getPobCardFeeExtra());
            feeResult.setFee(merchantFee.getPobCardFee());
            feeResult.setType(merchantFee.getPobCardFeeType().name());

            feeMap.put("local", feeResult);

            microMap = new HashMap<>();
            microMap.put("amount", merchantFee.getMicroTransactionAmount());
            microMap.put("fee", merchantFee.getMicroTransactionCardFee());

            feeMap.put("micro_transaction", microMap);

//                feeMap.setT
            feeResponse.setCard(feeMap);
            
            feeResponse.setBvn(merchantFee.getBvnFee());

            return Response.status(Response.Status.OK).entity(feeResponse).build();
        } catch (DatabaseException ex) {
            logger.error("error in web api", ex);
        }

        response.put("responsecode", "RR");
        response.put("responsemessage", "Generic error");

        return Response.status(Response.Status.OK).entity(response).build();

    }
    
    @Asynchronous
    public void sendNotification(String email, String name) {

        NotificationService.sendComplianceSuccessEmail(email, name);
    }

}

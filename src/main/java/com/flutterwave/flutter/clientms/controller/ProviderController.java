/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.ProviderDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.ProviderMDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.maker.ProviderM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "providerController")
@SessionScoped
public class ProviderController implements Serializable {
    
    @EJB
    private ProviderDao providerDao;
    @EJB
    private ProviderMDao providerMDao;
    @EJB
    private LogService logService;
    @EJB
    private UserDao userDao;
    
    private Provider provider;
    
    private PaginationHelper pagination, paginationUnauth;
    private DataModel dataModel, unauthdataModel;
   
    
    public Provider getProvider(){
        
        if(provider == null)
            provider = new Provider();
        
        return this.provider;
    }
    
    public void setProvider(Provider provider){
        this.provider = provider;
    }
        
    public String create() {

        try {
            Provider p = providerDao.findByKey("name", getProvider().getName());

            if (p != null) {

                Log log = new Log();
                log.setAction("creating provider");
                log.setCreatedOn(new Date());
                log.setDescription("created provider failed");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Provider with name " + p.getName() + " exists");

                JsfUtil.addErrorMessage("Provider with name  " + getProvider().getName() + " exists on the platform");
                return "/provider/create";
            }

            getProvider().setCreatedOn(new Date());
            getProvider().setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            
            //providerDao.create(provider);
            ProviderM providerM = new ProviderM();
            providerM.setCreatedBy(getProvider().getCreatedBy());
            providerM.setCreatedOn(getProvider().getCreatedOn());
            providerM.setName(getProvider().getName());
            providerM.setShortName(getProvider().getShortName());
            
            if(provider.isHasInterAccount()){
                providerM.setInterAccountFee(provider.getInterAccountFee());
                providerM.setInterAccountFeeExtra(provider.getInterAccountFeeExtra());
                providerM.setInterAccountFeeType(provider.getInterAccountFeeType());
                providerM.setInterAccountFeeCap(provider.getInterAccountFeeCap());
                providerM.setHasInterAccount(true);
            }
            
            if(provider.isHasInterCard()){
            providerM.setInterCardFee(provider.getInterCardFee());
            providerM.setInterCardFeeExtra(provider.getInterCardFeeExtra());
            providerM.setInterCardFeeType(provider.getInterCardFeeType());
            providerM.setInterCardFeeCap(provider.getInterCardFeeCap());
            providerM.setHasInterCard(true);
            }
            
            if(provider.isHasLocalAccount()){
            providerM.setLocalAccountFee(provider.getLocalAccountFee());
            providerM.setLocalAccountFeeExtra(provider.getLocalAccountFeeExtra());
            providerM.setLocalAccountFeeType(provider.getLocalAccountFeeType());
            providerM.setLocalAccountFeeCap(provider.getLocalAccountFeeCap());
            providerM.setHasLocalAccount(true);
            }
            
            if(provider.isHasLocalCard()){
            providerM.setLocalCardFee(provider.getLocalCardFee());
            providerM.setLocalCardFeeExtra(provider.getLocalCardFeeExtra());
            providerM.setLocalCardFeeType(provider.getLocalCardFeeType());
            providerM.setLocalCardFeeCap(provider.getLocalCardFeeCap());
            providerM.setHasLocalCard(true);
            }
            
            if(provider.isHasUssd()){
                providerM.setUSSDFee(provider.getUSSDFee());
                providerM.setHasUssd(true);
            }
            
            if(provider.isHasInternetBanking()){
                providerM.setInternetBankingFee(provider.getInternetBankingFee());
                providerM.setHasInternetBanking(true);
            }
            
            if(provider.isHasAmex()){
                providerM.setAmexFee(provider.getAmexFee());
                providerM.setAmexFeeExtra(provider.getAmexFeeExtra());
                providerM.setAmexFeeCap(provider.getAmexFeeCap());
                providerM.setAmexFeeType(provider.getAmexFeeType());
            }
            
            providerM.setCurrency(provider.getCurrency());
            
            providerM.setStatus(Status.PENDING);
            
            providerMDao.create(providerM);

            Log log = new Log();
            log.setAction("creating provider");
            log.setCreatedOn(new Date());
            log.setDescription("created provider successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Provider " + getProvider().getName() + " has been created successfully and sent for approval");
            
            logService.log(log);

            setProvider(new Provider());
            
            recreate();
            
            return "/provider/create";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProviderController.class.getName()).log(Level.SEVERE, null, ex);

            try {
                Log log = new Log();
                log.setAction("create provider");
                log.setCreatedOn(new Date());
                log.setDescription("create provider");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProviderController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "/provider/create";
    }
    
    public void prepareCreate(){
        
        provider = new Provider();
    }
    
    public String prepareUpdate(long id){
        
        try { 
            Provider pd = providerDao.find(id);
            
            if(pd == null){
                
                JsfUtil.addErrorMessage("Provide record not found");
                return "/provider/list";
            }
            
            setProvider(pd);
            
            return "edit";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProviderController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "/provider/list";
    }
    
    public String update() {

        try {
            Provider p = providerDao.findByKey("name", getProvider().getName());

            if (p != null && p.getId() != provider.getId()) {

                Log log = new Log();
                log.setAction("update provider");
                log.setCreatedOn(new Date());
                log.setDescription("update provider failed");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Provider with name " + p.getName() + " exists");

                JsfUtil.addErrorMessage("Provider with name  " + getProvider().getName() + " exists on the platform");
                return "/provider/edit";
            }
            
            p = providerDao.findByKey("shortName", getProvider().getShortName());
            
            if (p != null && p.getId() != provider.getId()) {

                Log log = new Log();
                log.setAction("update provider");
                log.setCreatedOn(new Date());
                log.setDescription("update provider failed");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Provider with short name " + p.getName() + " exists");

                JsfUtil.addErrorMessage("Provider with shortname  " + getProvider().getShortName()+ " exists on the platform");
                return "/provider/edit";
            }
            
            List<ProviderM> providers = providerMDao.find("modelId", provider.getId());
            
            if(providers != null && !providers.isEmpty()){
                
                JsfUtil.addErrorMessage("Pending Update exists");
                return "/provider/edit";
            }
//            getProvider().setCreatedOn(new Date());
//            getProvider().setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            
            //providerDao.create(provider);
            ProviderM providerM = new ProviderM();
            providerM.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            providerM.setCreatedOn(new Date());
            providerM.setName(getProvider().getName());
            providerM.setShortName(getProvider().getShortName());
            
            if(provider.isHasInterAccount()){
                providerM.setInterAccountFee(provider.getInterAccountFee());
                providerM.setInterAccountFeeExtra(provider.getInterAccountFeeExtra());
                providerM.setInterAccountFeeType(provider.getInterAccountFeeType());
                providerM.setInterAccountFeeCap(provider.getInterAccountFeeCap());
                providerM.setHasInterAccount(true);
            }
            
            if(provider.isHasInterCard()){
            providerM.setInterCardFee(provider.getInterCardFee());
            providerM.setInterCardFeeExtra(provider.getInterCardFeeExtra());
            providerM.setInterCardFeeType(provider.getInterCardFeeType());
            providerM.setInterCardFeeCap(provider.getInterCardFeeCap());
            providerM.setHasInterCard(true);
            }
            
            if(provider.isHasLocalAccount()){
            providerM.setLocalAccountFee(provider.getLocalAccountFee());
            providerM.setLocalAccountFeeExtra(provider.getLocalAccountFeeExtra());
            providerM.setLocalAccountFeeType(provider.getLocalAccountFeeType());
            providerM.setLocalAccountFeeCap(provider.getLocalAccountFeeCap());
            providerM.setHasLocalAccount(true);
            }
            
            if(provider.isHasLocalCard()){
            providerM.setLocalCardFee(provider.getLocalCardFee());
            providerM.setLocalCardFeeExtra(provider.getLocalCardFeeExtra());
            providerM.setLocalCardFeeType(provider.getLocalCardFeeType());
            providerM.setLocalCardFeeCap(provider.getLocalCardFeeCap());
            providerM.setHasLocalCard(true);
            }
            
            if(provider.isHasUssd()){
                providerM.setUSSDFee(provider.getUSSDFee());
                providerM.setHasUssd(true);
            }
            
            if(provider.isHasInternetBanking()){
                providerM.setInternetBankingFee(provider.getInternetBankingFee());
                providerM.setHasInternetBanking(true);
            }
            
            providerM.setCurrency(provider.getCurrency());
            
            providerM.setStatus(Status.PENDING);
            providerM.setModelId(provider.getId());
            
            providerMDao.create(providerM);
            
            paginationUnauth = null;

            Log log = new Log();
            log.setAction("update provider");
            log.setCreatedOn(new Date());
            log.setDescription("updated provider successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Provider " + getProvider().getName() + " has been updated successfully and sent for approval");
            
            logService.log(log);

            setProvider(new Provider());
            
//            recreate();
            
            return "/provider/list";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProviderController.class.getName()).log(Level.SEVERE, null, ex);

            try {
                Log log = new Log();
                log.setAction("create provider");
                log.setCreatedOn(new Date());
                log.setDescription("create provider");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProviderController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "/provider/edit";
    }
    
     public PaginationHelper getPaginationUnauth() {

        if (paginationUnauth == null) {
            paginationUnauth = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return providerMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(providerMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauth;
    }

    public PaginationHelper getPagination() {

        if (pagination == null) {
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return providerMDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(providerDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }
     
    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("providerupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("providerupdated", false);
        }

        if (pagination == null) {
            dataModel = getPagination().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Provider");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Provider");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        return dataModel;
    }
    

    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("providerupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauth = null;
            session.setAttribute("providerupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth Provider");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth Provider");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        if (paginationUnauth == null) {
            unauthdataModel = getPaginationUnauth().createPageDataModel();
        }

        return unauthdataModel;
    }

    public void recreate() {
        pagination = null;
        paginationUnauth = null;
    }
    
    public String viewAnalysis(String name){
        providerName = name;
        
        return "/provider/index";
    }
    
    public String getProviderName(){
        
        return providerName;
    }
    
    public String getWalletTransaction(String providerName){
        
        this.providerName = providerName;
        
        return "/provider/wallettransaction";
    }
    
    public List<Provider> getAllProviders(){
        
        try {
            return providerDao.findAll();
        } catch (DatabaseException ex) {
            Logger.getLogger(ProviderController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    private String providerName;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class CreateMposMerchantRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }
    
    @NotBlank(message = "Business name must be provided")
    @JsonProperty(value = "businessname")
    private String name;
    @NotBlank(message = "PWC merchant id must be provided")
    @JsonProperty(value = "pwcmerchantid")
    private String merchantId;
    @NotBlank(message = "Merchant code must be provided")
    @JsonProperty(value = "merchantcode")
    private String merchantCode;
    @NotBlank(message = "Phone must be provided")
    private String phone;
    @NotBlank(message = "Address must be provided")
    private String address;
    @NotBlank(message = "Hash value must be provided")
    private String hash;

    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject()
                .put("businessname", name)
                .put("pwcmerchantid", merchantId)
                .put("merchantcode", merchantCode)
                .put("phone", phone)
                .put("address", address == null ? "" : address)
                .put("hash", getHash());
                
        return jSONObject.toString();
    }
    
    public String toHashableString(){
        
        StringBuilder builder = new StringBuilder();
        builder.append(name)
                .append(merchantId)
                .append(merchantCode)
                .append(phone)
                .append(address == null ? "" : address);
        
        return builder.toString();
    }
    
}

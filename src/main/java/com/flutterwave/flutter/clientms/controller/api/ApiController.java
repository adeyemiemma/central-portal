/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.CountryController;
import com.flutterwave.flutter.clientms.controller.api.model.GetComplianceModel;
import com.flutterwave.flutter.clientms.dao.AccountQueryDao;
import com.flutterwave.flutter.clientms.dao.AirtimeFundingDao;
import com.flutterwave.flutter.clientms.dao.BankDao;
import com.flutterwave.flutter.clientms.dao.ControlDao;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CountryDao;
import com.flutterwave.flutter.clientms.dao.CurrencyDao;
import com.flutterwave.flutter.clientms.dao.MerchantCompliantDao;
import com.flutterwave.flutter.clientms.dao.MerchantCompliantLogDao;
import com.flutterwave.flutter.clientms.dao.MerchantDao;
import com.flutterwave.flutter.clientms.dao.MerchantFeeDao;
import com.flutterwave.flutter.clientms.dao.MerchantLimitDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.PermissionDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.RaveAddressBookDao;
import com.flutterwave.flutter.clientms.dao.RaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.RoleDao;
import com.flutterwave.flutter.clientms.dao.SectorDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.AccountQueryMDao;
import com.flutterwave.flutter.clientms.dao.maker.AirtimeFundingMDao;
import com.flutterwave.flutter.clientms.dao.maker.BankMDao;
import com.flutterwave.flutter.clientms.dao.maker.ControlModelMDao;
import com.flutterwave.flutter.clientms.dao.maker.CountryMDao;
import com.flutterwave.flutter.clientms.dao.maker.CurrencyMDao;
import com.flutterwave.flutter.clientms.dao.maker.MerchantFeeMDao;
import com.flutterwave.flutter.clientms.dao.maker.MerchantLimitMDao;
import com.flutterwave.flutter.clientms.dao.maker.MerchantMDao;
import com.flutterwave.flutter.clientms.dao.maker.ProductMDao;
import com.flutterwave.flutter.clientms.dao.maker.RoleMDao;
import com.flutterwave.flutter.clientms.dao.maker.SectorMDao;
import com.flutterwave.flutter.clientms.dao.maker.UserMDao;
import com.flutterwave.flutter.clientms.model.AccountQuery;
import com.flutterwave.flutter.clientms.model.AirtimeFunding;
import com.flutterwave.flutter.clientms.model.Bank;
import com.flutterwave.flutter.clientms.model.ControlModel;
import com.flutterwave.flutter.clientms.model.Country;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Merchant;
import com.flutterwave.flutter.clientms.model.MerchantCompliance;
import com.flutterwave.flutter.clientms.model.MerchantComplianceLog;
import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.MerchantLimit;
import com.flutterwave.flutter.clientms.model.Permission;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Role;
import com.flutterwave.flutter.clientms.model.Sector;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.AccountQueryM;
import com.flutterwave.flutter.clientms.model.maker.AirtimeFundingM;
import com.flutterwave.flutter.clientms.model.maker.BankM;
import com.flutterwave.flutter.clientms.model.maker.ControlModelM;
import com.flutterwave.flutter.clientms.model.maker.CountryM;
import com.flutterwave.flutter.clientms.model.maker.CurrencyM;
import com.flutterwave.flutter.clientms.model.maker.MerchantFeeM;
import com.flutterwave.flutter.clientms.model.maker.MerchantLimitM;
import com.flutterwave.flutter.clientms.model.maker.MerchantM;
import com.flutterwave.flutter.clientms.model.maker.ProductM;
import com.flutterwave.flutter.clientms.model.maker.RoleM;
import com.flutterwave.flutter.clientms.model.maker.SectorM;
import com.flutterwave.flutter.clientms.model.maker.UpdateModel;
import com.flutterwave.flutter.clientms.model.maker.UserM;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.model.products.GenericMerchant;
import com.flutterwave.flutter.clientms.model.products.MoneywaveMerchant;
import com.flutterwave.flutter.clientms.model.products.RaveAddressBook;
import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.clientms.service.AirtimeHelper;
import com.flutterwave.flutter.clientms.service.FlutterwaveApiService;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.NotificationManager;
import com.flutterwave.flutter.clientms.service.SendComplianceMessage;
import com.flutterwave.flutter.clientms.service.model.AirtimeFundingViewModel;
import com.flutterwave.flutter.clientms.service.model.VoucherFundingResponse;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.BvnResponseModel;
import com.flutterwave.flutter.clientms.viewmodel.RoleViewModel;
import com.flutterwave.flutter.clientms.viewmodel.UserViewModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@RequestScoped
@Path("/")
public class ApiController {

    @EJB
    private PermissionDao permissionDao;
    @EJB
    private RoleDao roleDao;
    @EJB
    private RoleMDao roleMDao;
    @EJB
    private UserDao userDao;
    @EJB
    private UserMDao userMDao;
    @EJB
    private LogService logService;
    @EJB
    private ProductMDao productMDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private MoneywaveMerchantDao moneywaveMerchantDao;
    @EJB
    private RaveMerchantDao raveMerchantDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private MerchantFeeDao merchantFeeDao;
    @EJB
    private MerchantFeeMDao merchantFeeMDao;
    @EJB
    private CurrencyMDao currencyMDao;
    @EJB
    private CurrencyDao currencyDao;
    @EJB
    private MerchantLimitMDao merchantLimitMDao;
    @EJB
    private MerchantLimitDao merchantLimitDao;
    @EJB
    private CountryMDao countryMDao;
    @EJB
    private CountryDao countryDao;
    @EJB
    private MerchantDao merchantDao;
    @EJB
    private MerchantMDao merchantMDao;
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private RaveAddressBookDao addressBookDao;
    @EJB
    private SectorMDao sectorMDao;
    @EJB
    private SectorDao sectorDao;
    @EJB
    private BankDao bankDao;
    @EJB
    private BankMDao bankMDao;
    @EJB
    private ControlModelMDao controlMDao;
    @EJB
    private ControlDao controlDao;
    @EJB
    private NotificationManager notificationManager;
    @EJB
    private AccountQueryMDao accountQueryMDao;
    @EJB
    private AccountQueryDao accountQueryDao;
    @EJB
    private MerchantCompliantDao merchantCompliantDao;
    @EJB
    private MerchantCompliantLogDao merchantCompliantLogDao;
    @EJB
    private FlutterwaveApiService flutterwaveApiService;
    
    @EJB
    private AirtimeFundingMDao airtimeFundingMDao;
    
    @EJB
    private AirtimeFundingDao airtimeFundingDao;

    @Inject
    private AirtimeHelper airtimeHelper;
    
    @GET
    @Path("/permissions")
    @Produces(value = MediaType.APPLICATION_JSON)
    public PageResult getAllPermissions(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw) {

        if (length == 0) {
            length = 20;
        }

        try {

            PageResult<Permission> pageResult = new PageResult<>();

            Page<Permission> permissions = permissionDao.find(start, length);

            pageResult = new PageResult<>(permissions.getContent(), permissions.getCount(), permissions.getCount());

            return pageResult;
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new PageResult<>();

    }

    @Path("/roles")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public PageResult getRoles(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw) {

        if (length == 0) {
            length = 10;
        }

        try {

            Page<Role> roles = roleDao.find(start, length, "permissions");

            List<RoleViewModel> roleViewModels = new ArrayList<>();

            if (roles != null && roles.getContent() != null) {
                roleViewModels = roles.getContent().stream().map((Role role) -> {
                    RoleViewModel viewModel = new RoleViewModel();
                    viewModel.setId(role.getId());
                    viewModel.setApprovedBy(role.getApprovedBy().getFirstName() + " " + role.getApprovedBy().getLastName());
                    viewModel.setCreatedBy(role.getCreatedBy().getFirstName() + " " + role.getCreatedBy().getLastName());
                    viewModel.setName(role.getName());
                    viewModel.setDescription(role.getDescription());
                    viewModel.setCreatedOn(role.getCreatedOn());
                    viewModel.setApprovedOn(role.getApprovedOn());
                    viewModel.setPermissions(role.getPermissions() != null ? role.getPermissions().size() : 0);
                    return viewModel;
                }).collect(Collectors.<RoleViewModel>toList());
            }

            PageResult<RoleViewModel> pageResult = new PageResult<>(roleViewModels, roles.getCount(), roles.getCount());

            return pageResult;
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new PageResult<>();
    }

    @Path("/role/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeRole(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            String id = updateModel.getId() + "";

            if (id == null) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            RoleM roleM = roleMDao.find(Long.parseLong(id), "permissions");

            Log log = new Log();
            log.setAction("Role Authorization for " + roleM.getName());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (status == true) {
                if (roleM.getModelId() <= 0) {

                    Role c = roleDao.findByKey("name", roleM.getName());

                    if (c != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate role with  name " + c.getName());
                        return response;
                    }

                    Role role = new Role();
                    role.setName(roleM.getName().toUpperCase());
                    role.setDescription(roleM.getDescription());
                    role.setCreatedBy(roleM.getCreatedBy());
                    role.setCreatedOn(roleM.getCreated());
                    role.setPermissions(roleM.getPermissions());
                    role.setApprovedOn(new Date());
                    role.setApprovedBy(user);

                    roleDao.create(role);

                } else {

                    Role role = roleDao.findByKey("name", roleM.getName(), "permissions");

                    if (role != null && role.getId() != roleM.getModelId()) {
                        response.put("status-code", "02");
                        response.put("status", "Duplicate role with  name " + role.getName());
                        return response;
                    }

                    role = roleDao.find(roleM.getModelId(), "permissions");

                    role.setName(roleM.getName());
                    role.setModified(roleM.getCreated());
                    role.setDescription(roleM.getDescription());
                    role.setPermissions(roleM.getPermissions());
                    role.setApprovedOn(new Date());
                    role.setApprovedBy(user);

                    roleDao.update(role);
                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            roleM.setApprovedOn(new Date());
            roleM.setApprovedBy(user);
            roleM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                roleM.setStatus(Status.APPROVED);
            } else {
                roleM.setStatus(Status.REJECTED);
            }

            roleMDao.update(roleM);

            log = new Log();
            log.setAction("Role Authorization for " + roleM.getName());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Completed Successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("roleupdated", true);
            session.setAttribute("roleupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Role Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Role Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path("/user/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeUser(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            UserM userM = userMDao.find(id);

            if (userM == null) {
                response.put("status-code", "03");
                response.put("status", "User not found");
                return response;
            }

            Log log = new Log();
            log.setAction("User Authorization with id " + userM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (userM.getModelId() <= 0) {

                if (status == true) {
                    User newUser = userDao.findByKey("username", userM.getEmail());

                    if (newUser != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate user with email " + newUser.getEmail());
                        return response;
                    }

                    newUser = new User();
                    newUser.setFirstName(userM.getFirstName());
                    newUser.setLastName(userM.getLastName());
                    newUser.setRole(userM.getRole());
                    newUser.setUsername(userM.getUsername());
                    newUser.setEmail(userM.getEmail());
                    newUser.setPassword(userM.getPassword());
                    newUser.setCreatedBy(userM.getCreatedBy());
                    newUser.setCreatedOn(userM.getCreated());
                    newUser.setEnabled(true);
                    newUser.setPhone(userM.getPhone());
                    newUser.setUsername(userM.getUsername());
                    newUser.setApprovedBy(user);
                    newUser.setApprovedOn(new Date());
                    newUser.setDomainId(userM.getDomainId());
                    newUser.setDomainName(userM.getDomainName());

                    userDao.create(newUser);

                    notificationManager.sendAccountCreationEmail((newUser.getFirstName().trim()).toLowerCase() + ""
                            + (newUser.getLastName().trim()).toLowerCase(), newUser.getFirstName(),
                            newUser.getLastName(), newUser.getEmail(), newUser.getDomainName(), newUser.getDomainId());
                }

            } else {

                User existingUser = userDao.findByKey("username", userM.getUsername());

                if (status == true) {
                    if (existingUser != null && existingUser.getId() != userM.getModelId()) {
                        response.put("status-code", "02");
                        response.put("status", "Duplicate user with email " + existingUser.getEmail());
                        return response;
                    }

                    existingUser = userDao.find(userM.getModelId());

                    existingUser.setFirstName(userM.getFirstName());
                    existingUser.setLastName(userM.getLastName());
                    existingUser.setModified(userM.getCreated());
                    existingUser.setRole(userM.getRole());
                    existingUser.setEnabled(userM.isEnabled());
                    existingUser.setApprovedBy(user);
                    existingUser.setApprovedOn(new Date());

                    userDao.update(existingUser);
                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            userM.setApprovedOn(new Date());
            userM.setApprovedBy(user);
            userM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                userM.setStatus(Status.APPROVED);
            } else {
                userM.setStatus(Status.REJECTED);
            }

            userMDao.update(userM);

            log = new Log();
            log.setAction("User Authorization for " + userM.getId() + " , " + userM.getEmail());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("userupdated", true);
            session.setAttribute("userupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("User Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("User Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    
    @Path("/users")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public PageResult getUserss(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw) {

        if (length == 0) {
            length = 10;
        }

        try {

            Page<User> users = userDao.find(start, length);

            List<UserViewModel> userViewModels = new ArrayList<>();

            if (users != null && users.getContent() != null) {
                userViewModels = users.getContent().stream().map((User user) -> {
                    UserViewModel viewModel = new UserViewModel();
                    viewModel.setId(user.getId());
                    viewModel.setApprovedBy(user.getApprovedBy().getFirstName() + " " + user.getApprovedBy().getLastName());
                    viewModel.setCreatedBy(user.getCreatedBy().getFirstName() + " " + user.getCreatedBy().getLastName());
                    viewModel.setName(user.getFirstName() + " "+ user.getLastName());
                    viewModel.setRole(user.getRole().getName());
                    viewModel.setCreatedOn(user.getCreatedOn());
                    viewModel.setApprovedOn(user.getApprovedOn());
                    return viewModel;
                }).collect(Collectors.<UserViewModel>toList());
            }
            
            long count = 0L;
            
            if(users != null )
                count = users.getCount();

            PageResult<UserViewModel> pageResult = new PageResult<>(userViewModels, count, count);

            return pageResult;
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new PageResult<>();
    }
    @Path("/product/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeProduct(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            ProductM productM = productMDao.find(id);

            if (productM == null) {
                response.put("status-code", "03");
                response.put("status", "product not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Product Authorization with id " + productM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (productM.getModelId() <= 0) {

                if (status == true) {
                    Product product = productDao.findByKey("name", productM.getName());

                    if (product != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate name with product " + product.getName());
                        return response;
                    }

                    product = new Product();
                    product.setCreatedBy(productM.getCreatedBy());
                    product.setCreatedOn(productM.getCreatedOn());
                    product.setLaunchDate(productM.getLaunchDate());
                    product.setName(productM.getName());
                    product.setStagingUrl(productM.getStagingUrl());
                    product.setDescription(productM.getDescription());
                    product.setInitiationDate(productM.getInitiationDate());
                    product.setLiveUrl(productM.getLiveUrl());
                    product.setApprovedBy(user);
                    product.setApprovedOn(new Date());

                    //                if (status == true) {
                    productDao.create(product);
                }
//                }

            } else {

                if (status == true) {
                    Product product = productDao.findByKey("name", productM.getName());

                    if (product != null && product.getId() != productM.getModelId()) {
                        response.put("status-code", "02");
                        response.put("status", "Duplicate name with product " + product.getName());
                        return response;
                    }

                    product = productDao.find(productM.getModelId());

                    product.setLaunchDate(productM.getLaunchDate());
                    product.setName(productM.getName());
                    product.setStagingUrl(productM.getStagingUrl());
                    product.setDescription(productM.getDescription());
                    product.setModified(productM.getCreatedOn());
                    product.setInitiationDate(productM.getInitiationDate());
                    product.setLiveUrl(productM.getLiveUrl());
                    product.setModified(productM.getCreatedOn());
                    product.setApprovedBy(user);
                    product.setEnabled(status);
                    product.setApprovedOn(new Date());
                    product.setEnabled(productM.isEnabled());
//                if (status == true) {
                    productDao.update(product);
//                }
                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            productM.setApprovedOn(new Date());
            productM.setApprovedBy(user);
            productM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                productM.setStatus(Status.APPROVED);
            } else {
                productM.setStatus(Status.REJECTED);
            }

            productMDao.update(productM);

            log = new Log();
            log.setAction("Product Authorization for " + productM.getId() + " , " + productM.getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("productupdated", true);
            session.setAttribute("productupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Product Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Product Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path("/product/changestatus")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> changeProductStatus(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            Product product = productDao.find(id);

            if (product == null) {
                response.put("status-code", "03");
                response.put("status", "product not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Changing product status " + product.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Changing product Status");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            List<ProductM> list = productMDao.find("modelId", product.getId());

            if (list != null && !list.isEmpty()) {

                ProductM cM = list.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {

//                    JsfUtil.addErrorMessage("A pending update exists");/country
                    response.put("status-code", "04");
                    response.put("status", "Pending update exist");
                    return response;
                }
            }

            ProductM productM = new ProductM();
            productM.setCreatedBy(user);
            productM.setCreatedOn(new Date());
            productM.setLaunchDate(product.getLaunchDate());
            productM.setStagingUrl(product.getStagingUrl());
            productM.setName(product.getName());
            productM.setDescription(product.getDescription());
            productM.setLiveUrl(product.getLiveUrl());
            productM.setStatus(Status.PENDING);
            productM.setEnabled(!"disable".equalsIgnoreCase(updateModel.getOperation()));
            productM.setInitiationDate(product.getInitiationDate());
            productM.setModelId(product.getId());

            productMDao.create(productM);

            log = new Log();
            log.setAction("Changing product status " + product.getId() + " , " + product.getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Changing product status");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }

            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("productupdated", true);
            session.setAttribute("productupdatedm", true);

            response.put("status-code", "00");
            response.put("status", "Status has been change modified successfully and submitted for approval");

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Changing Product Status");
                log.setCreatedOn(new Date());
                log.setDescription("Changing Product Status");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path(value = "/product/list")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List getAllProduct() {

        try {
            
            Session session =  SecurityUtils.getSubject().getSession();
            Object productIdObj = session.getAttribute("product");

            List<Product> products;
                    
            String productId = null;
            
            if(productIdObj != null){
                
                products = new ArrayList<>();
                
                Product product = productDao.findByKey("name", productIdObj.toString());

                products.add(product);
                
            }else{
                products = productDao.findAll();

                if (products == null || products.isEmpty()) {

                    return new ArrayList();
                }
            
            }

            return products.stream().map(x -> x.getName()).collect(Collectors.toList());
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return new ArrayList();
    }

    @Path("/merchant")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchants(@QueryParam(value = "product") String product,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "range") String createdBetween,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "live") String live,
            @QueryParam(value = "deleted") String deleted,
            @QueryParam(value = "country") String country) {

        try {

            Date startDate = null, endDate = null;

            if (createdBetween != null && !"".equalsIgnoreCase(createdBetween)) {

                try {
                    String[] splitDate = createdBetween.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(startDate);
                    calendar.set(Calendar.HOUR_OF_DAY, 0);
                    calendar.set(Calendar.MINUTE, 0);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);

                    startDate = calendar.getTime();

                    endDate = dateFormat.parse(splitDate[1]);

                    calendar.setTime(endDate);
                    calendar.set(Calendar.HOUR_OF_DAY, 23);
                    calendar.set(Calendar.MINUTE, 59);
                    calendar.set(Calendar.SECOND, 59);
//                calendar.set(Calendar.MILLISECOND, 59);

                    endDate = calendar.getTime();

                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }

            PageResult<GenericMerchant> pageResult = new PageResult<>();

            if (product == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<Product> products = productDao.findAll();

            if (products == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            if (products.isEmpty()) {

                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<GenericMerchant> merchants = new ArrayList<>();

            long totalCount = 0;

            if ("rave".equalsIgnoreCase(product)) {

                Page<RaveMerchant> raveMerchant = raveMerchantDao.search(start, length, search, deleted == null || "any".equalsIgnoreCase(deleted) ? null : "yes".equalsIgnoreCase(deleted),
                        live == null || "any".equalsIgnoreCase(live) ? null : !"yes".equalsIgnoreCase(live), country == null || "any".equalsIgnoreCase(country) ? null : country, startDate, endDate);

                totalCount += raveMerchant.getCount();

                if (raveMerchant.getContent() != null && !raveMerchant.getContent().isEmpty()) {

//                    if(totalCount > 0L)
                    merchants.addAll(raveMerchant.getContent().stream().map((RaveMerchant rm) -> {
                        GenericMerchant gm = rm.getGeneric();
                        try {
                            RaveAddressBook addressBook = addressBookDao.findByMerchant(rm);

                            gm.setActive((addressBook.getBlocked() == 0));
                            gm.setProduct(product);
                            gm.setEditing(isEditing(product, rm.getId()));

                        } catch (Exception ex) {
                            if (ex != null) {
                                ex.printStackTrace();
                            }
                        }
                        return gm;
                    }).collect(Collectors.<GenericMerchant>toList()));

//                    extraData.put("rave", "" + raveMerchant.getContent().size());
                }
            } else if ("moneywave".equalsIgnoreCase(product)) {

                Page<MoneywaveMerchant> waveMerchant = moneywaveMerchantDao.search(start, length, search, deleted == null || "any".equalsIgnoreCase(deleted) ? null : "yes".equalsIgnoreCase(deleted),
                        live == null || "any".equalsIgnoreCase(live) ? null : "yes".equalsIgnoreCase(live), country == null || "any".equalsIgnoreCase(country) ? null : country, startDate, endDate);

                totalCount += waveMerchant.getCount();

                if (waveMerchant.getContent() != null && !waveMerchant.getContent().isEmpty()) {

//                    if(totalCount > 0L)
                    merchants.addAll(waveMerchant.getContent().stream().map((MoneywaveMerchant rm) -> {
                        GenericMerchant gm = rm.getGeneric();
                        try {
                            gm.setProduct(product);
                            gm.setEditing(isEditing(product, rm.getId()));
                        } catch (Exception ex) {
                            if (ex != null) {
                                ex.printStackTrace();
                            }
                        }

                        return gm;
                    }).collect(Collectors.<GenericMerchant>toList()));
                }
            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                Page<CoreMerchant> coreMerchant = coreMerchantDao.search(start, length, search,
                        live == null || "any".equalsIgnoreCase(live) ? null : "yes".equalsIgnoreCase(live),
                        country == null || "any".equalsIgnoreCase(country) ? null : country, startDate, endDate);

                totalCount += coreMerchant.getCount();

                if (coreMerchant.getContent() != null && !coreMerchant.getContent().isEmpty()) {

                    merchants.addAll(coreMerchant.getContent().stream().map((CoreMerchant rm) -> {

                        GenericMerchant gm = rm.getGeneric();
                        try {
                            gm.setProduct(product);
                            gm.setEditing(isEditing(product, rm.getId()));
                        } catch (Exception ex) {
                            if (ex != null) {
                                ex.printStackTrace();
                            }
                        }

                        return gm;
                    }).collect(Collectors.<GenericMerchant>toList()));
                }
            }

//            if (!merchants.isEmpty()) {
            pageResult = new PageResult(merchants, totalCount, totalCount);
//            }

            return Response.status(Response.Status.OK).entity(pageResult).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
    }

    @Path("/merchant/sub")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getSubMerchants(@QueryParam(value = "parent") String parentId) {

        try {

            PageResult pageResult = new PageResult();

            JsonArray data = httpUtil.getSubmerchant(parentId);

            List<Object> list = new ArrayList<>();

            data.stream().map((value) -> (JsonObject) value).map((jsonObject) -> {
                Map<String, String> map = new HashMap<>();
                map.put("merchant_id", jsonObject.getString("merchant_id", ""));
                map.put("merchant_name", jsonObject.getString("merchant_name", ""));
                map.put("merchant_email", jsonObject.getString("merchant_email", ""));
                map.put("merchant_address", jsonObject.getString("merchant_address", ""));
                map.put("merchant_account_number", jsonObject.getString("merchant_account_number", ""));
                map.put("bank_code", jsonObject.getString("bank_code", ""));
                map.put("merchant_registration_number", jsonObject.getString("merchant_registration_number", ""));
                map.put("createdAt", jsonObject.getString("createdAt", ""));
                return map;
            }).forEachOrdered((map) -> {
                list.add(map);
            });

            long count = list.size();

            pageResult = new PageResult(list, count, count);
            return Response.status(Response.Status.OK).entity(pageResult).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
    }

    private boolean isEditing(String product, long id) throws DatabaseException {

        List<MerchantM> list = merchantMDao.find("modelId", id);

        if (list != null && !list.isEmpty()) {

            MerchantM cM = list.stream().filter(x -> x.getApprovedOn() == null && x.getProduct().getName().equalsIgnoreCase(product)).findFirst().orElse(null);

            if (cM != null) {

//                    JsfUtil.addErrorMessage("A pending update exists");
                return true;
            }
        }

        return false;
    }

    /**
     * This is called to get the list of merchants formatted to be usable by
     * auto-complete element
     *
     * @param term
     * @param product
     * @return
     */
    @Path("/merchant/all")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Object> getMerchantList(@QueryParam(value = "term") String term,
            @QueryParam(value = "product") String product) {

        try {

            List<GenericMerchant> merchants = new ArrayList<>();

            Product p = productDao.find(Long.parseLong(product));

            product = p.getName();

            if ("rave".equalsIgnoreCase(product)) {

                Page<RaveMerchant> raveMerchant = raveMerchantDao.search(0, 20, term, null, null, null, null, null);

                if (raveMerchant.getContent() != null && !raveMerchant.getContent().isEmpty()) {

                    merchants.addAll(raveMerchant.getContent().stream().map((RaveMerchant rm) -> {

                        GenericMerchant gm = rm.getGeneric();
                        return gm;
                    }).collect(Collectors.<GenericMerchant>toList()));

                }
            } else if ("moneywave".equalsIgnoreCase(product)) {

                Page<MoneywaveMerchant> waveMerchant = moneywaveMerchantDao.search(0, 20, term, null, null, null, null, null);

                if (waveMerchant.getContent() != null && !waveMerchant.getContent().isEmpty()) {

                    merchants.addAll(waveMerchant.getContent().stream().map((MoneywaveMerchant rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericMerchant>toList()));
                }
            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                Page<CoreMerchant> coreMerchant = coreMerchantDao.search(0, 20, term, null, null, null, null);

                if (coreMerchant.getContent() != null && !coreMerchant.getContent().isEmpty()) {

                    merchants.addAll(coreMerchant.getContent().stream().map((CoreMerchant rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericMerchant>toList()));
                }
            }

            List data = merchants.stream().map((GenericMerchant obj) -> {

                Map<String, String> map = new HashMap<>();
                map.put("id", obj.getName() + "");
                map.put("label", obj.getName() + "");
                map.put("value", obj.getName() + "-" + obj.getId());

                return map;
            }).collect(Collectors.<Map>toList());

            return data;

        } catch (Exception ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ArrayList<>();
    }
    
    /**
     * This is called to get the list of merchants formatted to be usable by
     * auto-complete element
     *
     * @param term
     * @param product
     * @return
     */
    @Path("/merchant/all/name")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Object> getMerchantListProduct(@QueryParam(value = "term") String term,
            @QueryParam(value = "product") String product) {

        try {

            List<GenericMerchant> merchants = new ArrayList<>();

            Product p;
            
            if("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)){
                p = productDao.findByKey("name", "core");
                
                if(p == null)
                    p = productDao.findByKey("name", "flutterwave core");
                
            }else
                p = productDao.findByKey("name", product);

            product = p.getName();

            if ("rave".equalsIgnoreCase(product)) {

                Page<RaveMerchant> raveMerchant = raveMerchantDao.search(0, 20, term, null, null, null, null, null);

                if (raveMerchant.getContent() != null && !raveMerchant.getContent().isEmpty()) {

                    merchants.addAll(raveMerchant.getContent().stream().map((RaveMerchant rm) -> {

                        GenericMerchant gm = rm.getGeneric();
                        return gm;
                    }).collect(Collectors.<GenericMerchant>toList()));

                }
            } else if ("moneywave".equalsIgnoreCase(product)) {

                Page<MoneywaveMerchant> waveMerchant = moneywaveMerchantDao.search(0, 20, term, null, null, null, null, null);

                if (waveMerchant.getContent() != null && !waveMerchant.getContent().isEmpty()) {

                    merchants.addAll(waveMerchant.getContent().stream().map((MoneywaveMerchant rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericMerchant>toList()));
                }
            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                Page<CoreMerchant> coreMerchant = coreMerchantDao.search(0, 20, term, null, null, null, null);

                if (coreMerchant.getContent() != null && !coreMerchant.getContent().isEmpty()) {

                    merchants.addAll(coreMerchant.getContent().stream().map((CoreMerchant rm) -> {
                        return rm.getGeneric();
                    }).collect(Collectors.<GenericMerchant>toList()));
                }
            }

            List data = merchants.stream().map((GenericMerchant obj) -> {

                Map<String, String> map = new HashMap<>();
                map.put("id", obj.getName() + "");
                map.put("label", obj.getName() + "");
                map.put("value", obj.getName() + "-" + obj.getId());

                return map;
            }).collect(Collectors.<Map>toList());

            return data;

        } catch (Exception ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ArrayList<>();
    }

    /**
     * This is called to authorize fee
     *
     * @param updateModel
     * @return
     */
    @Path("/fee/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeFee(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            MerchantFeeM feeM = merchantFeeMDao.find(id);

            if (feeM == null) {
                response.put("status-code", "03");
                response.put("status", "merchant fee record not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Merchant Fee Authorization with id " + feeM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (feeM.getModelId() <= 0 && status == true) {

                MerchantFee merchantFee = merchantFeeDao.findByKey("merchantId", feeM.getMerchantId());

                if (merchantFee != null) {

                    response.put("status-code", "02");
                    response.put("status", "Duplicate fee for merchant " + feeM.getMerchantName());
                    return response;
                }

                merchantFee = new MerchantFee();

                merchantFee.setCreatedBy(feeM.getCreatedBy());
                merchantFee.setCreated(feeM.getCreatedOn());
                merchantFee.setProduct(feeM.getProduct());
                merchantFee.setMerchantId(feeM.getMerchantId());
                merchantFee.setMerchantName(feeM.getMerchantName());

                // This section is for international fee
                merchantFee.setInterAccountFee(feeM.getInterAccountFee());
                merchantFee.setInterAccountFeeExtra(feeM.getInterAccountFeeExtra());
                merchantFee.setInterAccountFeeCap(feeM.getInterAccountFeeCap());
                merchantFee.setInterAccountFeeType(feeM.getInterAccountFeeType());
                merchantFee.setInterCardFee(feeM.getInterCardFee());
                merchantFee.setInterCardFeeExtra(feeM.getInterCardFeeExtra());
                merchantFee.setInterCardFeeCap(feeM.getInterCardFeeCap());
                merchantFee.setInterCardFeeType(feeM.getInterCardFeeType());
                // end of setting intertional fee

                // This section is for pob fee
                merchantFee.setPobAccountFee(feeM.getPobAccountFee());
                merchantFee.setPobAccountFeeExtra(feeM.getPobAccountFeeExtra());
                merchantFee.setPobAccountFeeCap(feeM.getPobAccountFeeCap());
                merchantFee.setPobAccountFeeType(feeM.getPobAccountFeeType());
                merchantFee.setPobCardFee(feeM.getPobCardFee());
                merchantFee.setPobCardFeeExtra(feeM.getPobCardFeeExtra());
                merchantFee.setPobCardFeeCap(feeM.getPobCardFeeCap());
                merchantFee.setPobCardFeeType(feeM.getPobCardFeeType());
                // end of setting pob fee

                // This section is for micro transaction  fee
                merchantFee.setMicroTransactionAccountFee(feeM.getMicroTransactionAccountFee());
                merchantFee.setMicroTransactionAccountFeeType(feeM.getMicroTransactionAccountFeeType());
                merchantFee.setMicroTransactionCardFee(feeM.getMicroTransactionCardFee());
                merchantFee.setMicroTransactionCardFeeType(feeM.getMicroTransactionCardFeeType());
                merchantFee.setMicroTransactionAmount(feeM.getMicroTransactionAmount());
                // end of micro transaction  fee
                
                merchantFee.setBvnFee(feeM.getBvnFee());

//                if (status == true) {
                merchantFeeDao.create(merchantFee);
//                }

            } else {

                if (status == true) {
                    
                    MerchantFee merchantFee = merchantFeeDao.findByKey("merchantId",feeM.getMerchantId());

                    if (merchantFee != null && merchantFee.getId() != feeM.getModelId()) {
                        response.put("status-code", "02");
                        response.put("status", "Duplicate fee for merchant with name " + merchantFee.getMerchantName());
                        return response;
                    }

                    merchantFee = merchantFeeDao.find(feeM.getModelId());

                    merchantFee.setModifiedBy(feeM.getCreatedBy());
                    merchantFee.setModified(feeM.getCreatedOn());
                    //                merchantFee.setProduct(feeM.getProduct());

                    // This section is for international fee
                    merchantFee.setInterAccountFee(feeM.getInterAccountFee());
                    merchantFee.setInterAccountFeeExtra(feeM.getInterAccountFeeExtra());
                    merchantFee.setInterAccountFeeCap(feeM.getInterAccountFeeCap());
                    merchantFee.setInterAccountFeeType(feeM.getInterAccountFeeType());
                    merchantFee.setInterCardFee(feeM.getInterCardFee());
                    merchantFee.setInterCardFeeExtra(feeM.getInterCardFeeExtra());
                    merchantFee.setInterCardFeeCap(feeM.getInterCardFeeCap());
                    merchantFee.setInterCardFeeType(feeM.getInterCardFeeType());
                    // end of setting intertional fee

                    // This section is for pob fee
                    merchantFee.setPobAccountFee(feeM.getPobAccountFee());
                    merchantFee.setPobAccountFeeExtra(feeM.getPobAccountFeeExtra());
                    merchantFee.setPobAccountFeeCap(feeM.getPobAccountFeeCap());
                    merchantFee.setPobAccountFeeType(feeM.getPobAccountFeeType());
                    merchantFee.setPobCardFee(feeM.getPobCardFee());
                    merchantFee.setPobCardFeeExtra(feeM.getPobCardFeeExtra());
                    merchantFee.setPobCardFeeCap(feeM.getPobCardFeeCap());
                    merchantFee.setPobCardFeeType(feeM.getPobCardFeeType());
                    // end of setting pob fee

                    // This section is for micro transaction  fee
                    merchantFee.setMicroTransactionAccountFee(feeM.getMicroTransactionAccountFee());
                    merchantFee.setMicroTransactionAccountFeeType(feeM.getMicroTransactionAccountFeeType());
                    merchantFee.setMicroTransactionCardFee(feeM.getMicroTransactionCardFee());
                    merchantFee.setMicroTransactionCardFeeType(feeM.getMicroTransactionCardFeeType());

                    merchantFee.setMerchantId(feeM.getMerchantId());
                    merchantFee.setMerchantName(feeM.getMerchantName());

                    merchantFee.setMicroTransactionAmount(feeM.getMicroTransactionAmount());
                    
                    merchantFee.setBvnFee(feeM.getBvnFee());

                    merchantFeeDao.update(merchantFee);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            feeM.setApprovedOn(new Date());
            feeM.setApprovedBy(user);
            feeM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                feeM.setStatus(Status.APPROVED);
            } else {
                feeM.setStatus(Status.REJECTED);
            }

            merchantFeeMDao.update(feeM);

            log = new Log();
            log.setAction("Merchant Fee Authorization for " + feeM.getId() + " , " + feeM.getMerchantName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("feeupdated", true);
            session.setAttribute("feeupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Fee Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Fee Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path(value = "/fee/unauth")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public MerchantFeeM getUnauthMerchantFeeById(@QueryParam(value = "id") long id) {

        try {
            if (id <= 0) {
                return null;
            }

            MerchantFeeM merchantFeeM = merchantFeeMDao.find(id);

            return merchantFeeM;
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Path(value = "/fee")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public MerchantFee getMerchantFeeById(@QueryParam(value = "id") long id) {

        try {
            if (id <= 0) {
                return null;
            }

            MerchantFee merchantFeeM = merchantFeeDao.find(id);

            return merchantFeeM;
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    // This section is for currency
    @Path("/currency/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeCurrency(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            CurrencyM sectorM = currencyMDao.find(id);

            if (sectorM == null) {
                response.put("status-code", "03");
                response.put("status", "currency not found");
                return response;
            }

            Log log = new Log();
            log.setAction("User Authorization with id " + sectorM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (status == true) {
                if (sectorM.getModelId() <= 0) {

                    Currency currency = currencyDao.findByKey("shortName", sectorM.getShortName());

                    if (currency != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate currency with short name " + currency.getShortName());
                        return response;
                    }

                    currency = currencyDao.findByKey("name", sectorM.getName());

                    if (currency != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate currency with name " + currency.getName());
                        return response;
                    }

                    currency = new Currency();

                    currency.setCreatedBy(sectorM.getCreatedBy());
                    currency.setCreatedOn(sectorM.getCreatedOn());
                    currency.setEnabled(true);
                    currency.setName(sectorM.getName());
                    currency.setShortName(sectorM.getShortName());
                    currency.setApprovedBy(user);
                    currency.setApprovedOn(new Date());

                    currencyDao.create(currency);

                } else {

                    Currency currency = currencyDao.findByKey("shortName", sectorM.getShortName());

                    if (currency != null && currency.getId() != sectorM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate currency with short name " + currency.getShortName());
                        return response;
                    }

                    currency = currencyDao.findByKey("name", sectorM.getName());

                    if (currency != null && currency.getId() != sectorM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate currency with name " + currency.getName());
                        return response;
                    }

                    currency = currencyDao.find(sectorM.getModelId());

                    currency.setEnabled(sectorM.isEnabled());
                    currency.setModified(sectorM.getCreatedOn());
                    currency.setName(sectorM.getName());
                    currency.setShortName(sectorM.getShortName());
                    currency.setApprovedBy(user);
                    currency.setApprovedOn(new Date());

                    currencyDao.update(currency);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            sectorM.setApprovedOn(new Date());
            sectorM.setApprovedBy(user);
            sectorM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                sectorM.setStatus(Status.APPROVED);
            } else {
                sectorM.setStatus(Status.REJECTED);
            }

            currencyMDao.update(sectorM);

            log = new Log();
            log.setAction("Currency Authorization for " + sectorM.getId() + " , " + sectorM.getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("currencyupdated", true);
            session.setAttribute("currencyupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Currency Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Currency Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path("/currency/changestatus")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> changeCurrencyStatus(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            Currency currency = currencyDao.find(id);

            if (currency == null) {
                response.put("status-code", "03");
                response.put("status", "currency not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Changing currency status " + currency.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Changing currency Status");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            List<CurrencyM> list = currencyMDao.find("modelId", currency.getId());

            if (list != null && !list.isEmpty()) {

                CurrencyM cM = list.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {

//                    JsfUtil.addErrorMessage("A pending update exists");
                    response.put("status-code", "04");
                    response.put("status", "Pending update exist");
                    return response;
                }
            }

            CurrencyM currencyM = new CurrencyM();
            currencyM.setModelId(currency.getId());
            currencyM.setCreatedOn(new Date());
            currencyM.setEnabled(!"disable".equalsIgnoreCase(updateModel.getOperation()));
            currencyM.setName(currency.getName());
            currencyM.setShortName(currency.getShortName());
            currencyM.setCreatedBy(user);

            currencyMDao.create(currencyM);

            log = new Log();
            log.setAction("Changing currency status " + currency.getId() + " , " + currency.getName());
            log.setCreatedOn(new Date());
//            if (status == true) {
            log.setDescription("Changing currency status");
//            } else {
//                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
//            }

            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("currencyupdated", true);
            session.setAttribute("currencyupdatedm", true);

            response.put("status-code", "00");
            response.put("status", "Status has been change modified successfully and submitted for approval");

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Changing Currency Status");
                log.setCreatedOn(new Date());
                log.setDescription("Changing Currency Status");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    // end of currency section
    // beginning of limit section
    @Path("/limit/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map<String, String> authorizeLimit(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            MerchantLimitM sectorM = merchantLimitMDao.find(id);

            if (sectorM == null) {
                response.put("status-code", "03");
                response.put("status", "merchant limit not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Limit Authorization with id " + sectorM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (status == true) {
                if (sectorM.getModelId() <= 0) {

                    //                merchant
                    Map<String, Object> searchParams = new HashMap<>();
                    searchParams.put("merchantId", sectorM.getMerchantId());
                    searchParams.put("limitCategory", sectorM.getLimitCategory());

                    List<MerchantLimit> limits = merchantLimitDao.findRaw(searchParams);

                    if (limits != null && !limits.isEmpty()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate limit for merchant with name " + sectorM.getMerchantName() + "");
                        return response;
                    }

                    MerchantLimit merchantLimit = new MerchantLimit();
                    merchantLimit.setLimitCategory(sectorM.getLimitCategory());

                    merchantLimit.setCreatedBy(sectorM.getCreatedBy());
                    merchantLimit.setCreatedOn(sectorM.getCreatedOn());

                    merchantLimit.setDailyLimit(sectorM.getDailyLimit());
                    merchantLimit.setMinLimit(sectorM.getMinLimit());
                    merchantLimit.setMaxLimit(sectorM.getMaxLimit());
                    merchantLimit.setLimitCategory(sectorM.getLimitCategory());
                    merchantLimit.setProduct(sectorM.getProduct());
                    merchantLimit.setValue(sectorM.getValue());
                    merchantLimit.setMerchantName(sectorM.getMerchantName());
                    merchantLimit.setMerchantId(sectorM.getMerchantId());

                    if ("rave".equalsIgnoreCase(merchantLimit.getProduct().getName())) {

                        JsonObject jsonObject = httpUtil.setRaveLimit(merchantLimit.getMerchantId(), merchantLimit);

                        if (jsonObject == null) {
                            throw new Exception("Unable to update limit on rave, please try again later");
                        }

//                        if ("success".equalsIgnoreCase(jsonObject.getString("status", "").toLowerCase())) {
//                            response.put("status-code", "00");
//                            response.put("status", "" + jsonObject.getString("message", ""));
                        merchantLimitDao.create(merchantLimit);
//                        } else {
//
//                            throw new Exception("" + jsonObject.getString("message", ""));
//                        }
//                        
                    } else if ("moneywave".equalsIgnoreCase(merchantLimit.getProduct().getName())) {

                        GenericMerchant gm = findMerchantById("moneywave", merchantLimit.getMerchantId());
                        JsonObject jsonObject = httpUtil.setMoneywaveLimit(gm.getSecret(), merchantLimit);

                        if (jsonObject == null) {
                            throw new Exception("Unable to update limit on moneywave, please try again later");
                        }

//                        if ("success".equalsIgnoreCase(jsonObject.getString("status", "").toLowerCase())) {
//                            response.put("status-code", "03");
//                            response.put("status", "" + jsonObject.getString("message", ""));
                        merchantLimitDao.create(merchantLimit);
//                        } else {
//
//                            throw new Exception("" + jsonObject.getString("message", ""));
//                        }
                    } else {
                        merchantLimitDao.create(merchantLimit);
                    }

                } else {

                    Map<String, Object> searchParams = new HashMap<>();
                    searchParams.put("merchantId", sectorM.getMerchantId());
                    searchParams.put("limitCategory", sectorM.getLimitCategory());

                    List<MerchantLimit> limits = merchantLimitDao.findRaw(searchParams);

                    MerchantLimit limit = limits.get(0);

                    if (limit != null && limit.getId() != sectorM.getModelId()) {
                        response.put("status-code", "02");
                        response.put("status", "Duplicate limit for merchant ");
                        return response;
                    }

                    MerchantLimit merchantLimit = merchantLimitDao.find(sectorM.getModelId());

                    merchantLimit.setLimitCategory(sectorM.getLimitCategory());

                    merchantLimit.setModifiedBy(sectorM.getCreatedBy());
                    merchantLimit.setModifiedOn(sectorM.getCreatedOn());

                    merchantLimit.setDailyLimit(sectorM.getDailyLimit());
                    merchantLimit.setMinLimit(sectorM.getMinLimit());
                    merchantLimit.setMaxLimit(sectorM.getMaxLimit());
                    merchantLimit.setLimitCategory(sectorM.getLimitCategory());
                    merchantLimit.setProduct(sectorM.getProduct());
                    merchantLimit.setValue(sectorM.getValue());
                    merchantLimit.setMerchantName(sectorM.getMerchantName());

                    if ("rave".equalsIgnoreCase(merchantLimit.getProduct().getName())) {

                        JsonObject jsonObject = httpUtil.setRaveLimit(merchantLimit.getMerchantId(), merchantLimit);

                        if (jsonObject == null) {
                            throw new Exception("Unable to update limit on rave, please try again later");
                        }

//                        if ("success".equalsIgnoreCase(jsonObject.getString("status", "").toLowerCase())) {
//                            response.put("status-code", "03");
//                            response.put("status", "" + jsonObject.getString("message", ""));
                        merchantLimitDao.update(merchantLimit);
//                        } else {
//
//                            throw new Exception("" + jsonObject.getString("message", ""));
//                        }

                    } else if ("moneywave".equalsIgnoreCase(merchantLimit.getProduct().getName())) {

                        GenericMerchant gm = findMerchantById("moneywave", merchantLimit.getMerchantId());
                        JsonObject jsonObject = httpUtil.setMoneywaveLimit(gm.getSecret(), merchantLimit);

                        if (jsonObject == null) {
                            throw new Exception("Unable to update limit on moneywave, please try again later");
                        }

//                        if ("success".equalsIgnoreCase(jsonObject.getString("status", "").toLowerCase())) {
//                            response.put("status-code", "03");
//                            response.put("status", "" + jsonObject.getString("message", ""));
                        merchantLimitDao.update(merchantLimit);
//                        } else {
//
//                            throw new Exception("" + jsonObject.getString("message", ""));
//                        }
                    } else {
                        merchantLimitDao.update(merchantLimit);
                    }

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            sectorM.setApprovedOn(new Date());
            sectorM.setApprovedBy(user);
            sectorM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                sectorM.setStatus(Status.APPROVED);
            } else {
                sectorM.setStatus(Status.REJECTED);
            }

            merchantLimitMDao.update(sectorM);

            log = new Log();
            log.setAction("Limit Authorization for " + sectorM.getId() + " , " + sectorM.getMerchantName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("limitupdated", true);
            session.setAttribute("limitupdatedm", true);

            return response;

        } catch (Exception ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Limit Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Limit Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }

            response.put("status-code", "RR");
            response.put("status", "" + ex.getMessage());
        }

        return response;
    }

    @Path(value = "/merchant/info")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantById(@QueryParam(value = "id") long id,
            @QueryParam(value = "product") String product) {

        try {
            if (id <= 0) {
                return null;
            }

            if ("rave".equalsIgnoreCase(product)) {

                RaveMerchant raveMerchant = raveMerchantDao.find(id);

                return Response.status(Response.Status.OK).entity(raveMerchant).build();

            } else if ("moneywave".equalsIgnoreCase(product)) {

                MoneywaveMerchant waveMerchant = moneywaveMerchantDao.find(id);
                waveMerchant.setSecret("");
                waveMerchant.setApiKey("");

                return Response.status(Response.Status.OK).entity(waveMerchant).build();

            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                CoreMerchant coreMerchant = coreMerchantDao.find(id);

                coreMerchant.setPassword("");
                coreMerchant.setTestApikey("");
                coreMerchant.setTestToken("");
                coreMerchant.setLiveApikey("");
                coreMerchant.setLiveToken("");

                return Response.status(Response.Status.OK).entity(coreMerchant).build();
            }

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).build();
    }
    
    @Path(value = "/merchant/kyc/all")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantKycs(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "id") long id,
            @QueryParam(value = "country") String country,
            @QueryParam(value = "range") String createdBetween,
            @QueryParam(value = "status") String status,
            @QueryParam(value = "searchState") boolean searchState,
            @QueryParam(value = "filterState") boolean filterState,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "product") String product){
        
        if("".equals(product))
            product = null;
        
        if("".equals(search))
            search = null;
        
        if("".equals(status) || "any".equalsIgnoreCase(status))
            status = null;
        
        if("".equals(country) || "any".equalsIgnoreCase(country))
            country = null;
        
        Date startDate = null, endDate = null;        
        
        if (Utility.emptyToNull(createdBetween) != null) {

            try {
                String[] splitDate = createdBetween.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                startDate = calendar.getTime();

                endDate = dateFormat.parse(splitDate[1]);

                calendar.setTime(endDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
//                calendar.set(Calendar.MILLISECOND, 59);

                endDate = calendar.getTime();

            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        PageResult<GetComplianceModel> pageResult;
        
        Page<MerchantCompliance> page = new Page<>();
        
        if(filterState == true)
            page =  merchantCompliantDao.findMerchant(start, length, startDate, endDate, product, null, search,
                    status == null ? null : ("YES".equalsIgnoreCase(status) || "REJECTED".equalsIgnoreCase(status)) , 
                    country, "REJECTED".equalsIgnoreCase(status));
        else if(searchState == true){
            List<MerchantCompliance> compliances =  merchantCompliantDao.search(search);
            
            if(compliances != null && !compliances.isEmpty())
                page = new Page<>(compliances.size(), compliances);
        }
        
        List<MerchantCompliance> compliances = page.getContent();
        
        if(compliances == null)
            pageResult = new PageResult<>(new ArrayList<>(),0L,0L);
        else{
            
            List<GetComplianceModel> cModel = compliances.stream().map((MerchantCompliance x) -> { return GetComplianceModel.getComplianceModel(x);})
                    .collect(Collectors.<GetComplianceModel> toList());
            
            pageResult = new PageResult<>(cModel, page.getCount(), page.getCount());
        }
        
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path(value = "/merchant/kyc/alllog")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantKycLog(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "id") long id,
            @QueryParam(value = "country") String country,
            @QueryParam(value = "range") String createdBetween,
            @QueryParam(value = "status") String status,
            @QueryParam(value = "searchString") String search,
            @QueryParam(value = "product") String product){
        
        if("".equals(product))
            product = null;
        
        if("".equals(search))
            search = null;
        
        if("".equals(status) || "any".equalsIgnoreCase(status))
            status = null;
        
        if("".equals(country) || "any".equalsIgnoreCase(country))
            country = null;
        
        Date startDate = null, endDate = null;        
        
        if (Utility.emptyToNull(createdBetween) != null) {

            try {
                String[] splitDate = createdBetween.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                startDate = calendar.getTime();

                endDate = dateFormat.parse(splitDate[1]);

                calendar.setTime(endDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
//                calendar.set(Calendar.MILLISECOND, 59);

                endDate = calendar.getTime();

            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        PageResult<GetComplianceModel> pageResult;
        
        Page<MerchantComplianceLog> page =  merchantCompliantLogDao.findMerchant(start, length, startDate, endDate, product, null, search,
                status == null ? null : ("YES".equalsIgnoreCase(status) || "REJECTED".equalsIgnoreCase(status)) , 
                country, "REJECTED".equalsIgnoreCase(status));
        
        List<MerchantComplianceLog> compliances = page.getContent();
        
        if(compliances == null)
            pageResult = new PageResult<>(new ArrayList<>(),0L,0L);
        else{
            
            List<GetComplianceModel> cModel = compliances.stream().map((MerchantComplianceLog x) -> { return GetComplianceModel.getComplianceModel(x);})
                    .collect(Collectors.<GetComplianceModel> toList());
            
            pageResult = new PageResult<>(cModel, page.getCount(), page.getCount());
        }
        
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }

    @Path(value = "/merchant/kyc")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantKYC(@QueryParam(value = "id") long id,
            @QueryParam(value = "product") String product) {

        try {
            if (id <= 0 || product == null) {
                return null;
            }
//        if("rave".equalsIgnoreCase(product)){

            MerchantCompliance compliance = merchantCompliantDao.findByMerchantId(id+"", product);
//        }
            
            if("rave".equalsIgnoreCase(product) && compliance == null){
                compliance = merchantCompliantDao.findByMerchantId(id+"", "ravepay");
            }
            
            Map<String, Object> data = new HashMap<>();
            
            if(compliance == null){
                
                data.put("responsecode", "01");
                data.put("responsemessage", "Record not found");
                
                return Response.status(Response.Status.OK).entity(data).build();
            }
            
            if("moneywave".equalsIgnoreCase(product)){
                
                compliance.setAmlPolicyPath(Utility.fixMoneywavePath(compliance.getAmlPolicyPath()));
                compliance.setCompanyRegDocumentPath(Utility.fixMoneywavePath(compliance.getCompanyRegDocumentPath()));
                compliance.setScumlCertificatePath(Utility.fixMoneywavePath(compliance.getScumlCertificatePath()));
                compliance.setDirectorIdPath(Utility.fixMoneywavePath(compliance.getDirectorIdPath()));
                compliance.setOperatingLicencePath(Utility.fixMoneywavePath(compliance.getOperatingLicencePath()));
            }
            
            data.put("responsecode", "00");
            data.put("responsemessage", "Successful");
            data.put("data", GetComplianceModel.getComplianceModel(compliance));
                
            compliance = null;
            
            return Response.status(Response.Status.OK).entity(data).build();
                
//            return Response.status(Response.Status.OK).entity(compliance).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    @Path(value = "/merchant/kyc/verify/bvn")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantVerifyBVNKYC(@QueryParam(value = "id") long id,
            @QueryParam(value = "product") String product) {

        try {
            if (id <= 0 || product == null) {
                return null;
            }
//        if("rave".equalsIgnoreCase(product)){

            MerchantCompliance compliance = merchantCompliantDao.findByMerchantId(id+"", product);
//        }
            
//            Map<String, String> response 
            
            Map<String, Object> data = new HashMap<>();
            
            if("rave".equalsIgnoreCase(product) && compliance == null){
                compliance = merchantCompliantDao.findByMerchantId(id+"", "ravepay");
            }
            
            if(compliance == null){
                
                data.put("responsecode", "01");
                data.put("responsemessage", "Record not found");
                
                return Response.status(Response.Status.OK).entity(data).build();
            }
            
            if(!"NG".equalsIgnoreCase(compliance.getCountry())){
                data.put("responsecode", "00");
                data.put("responsemessage", "BVN Verification not neccesary for non-Nigeria companies");
                
                compliance.setBvnChecked(true);
                compliance.setBvnCheckedOn(new Date());
                
                merchantCompliantDao.update(compliance);
                
                return Response.status(Response.Status.OK).entity(data).build();
            }
            
            if("moneywave".equalsIgnoreCase(product)){
                
                compliance.setAmlPolicyPath(Utility.fixMoneywavePath(compliance.getAmlPolicyPath()));
                compliance.setCompanyRegDocumentPath(Utility.fixMoneywavePath(compliance.getCompanyRegDocumentPath()));
                compliance.setScumlCertificatePath(Utility.fixMoneywavePath(compliance.getScumlCertificatePath()));
                compliance.setDirectorIdPath(Utility.fixMoneywavePath(compliance.getDirectorIdPath()));
                compliance.setOperatingLicencePath(Utility.fixMoneywavePath(compliance.getOperatingLicencePath()));
            }
            
            
            BvnResponseModel responseModel = flutterwaveApiService.validateBVN(compliance.getBvn());
            
            if(responseModel == null){
                
                data.put("responsecode", "01");
                data.put("responsemessage", "Bvn verification failed");
                
                return Response.status(Response.Status.OK).entity(data).build();
            }
            
            compliance.setBvnChecked(true);
            compliance.setBvnCheckedOn(new Date());
            
            merchantCompliantDao.update(compliance);
            
            data.put("responsecode", "00");
            data.put("responsemessage", "Successful");
            data.put("data", responseModel);
                
            compliance = null;
            
            return Response.status(Response.Status.OK).entity(data).build();
                
//            return Response.status(Response.Status.OK).entity(compliance).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    
    @Path(value = "/merchant/kyc/log")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantKYCLog(@QueryParam(value = "id") long id,
            @QueryParam(value = "product") String product) {

        try {
            if (id <= 0 || product == null) {
                return null;
            }
//        if("rave".equalsIgnoreCase(product)){

            MerchantComplianceLog compliance = merchantCompliantLogDao.find(id);
//        }
            
//            if("rave".equalsIgnoreCase(product) && compliance == null){
//                compliance = merchantCompliantLogDao.findByMerchantId(id+"", "ravepay");
//            }
            
            Map<String, Object> data = new HashMap<>();
            
            if(compliance == null){
                
                data.put("responsecode", "01");
                data.put("responsemessage", "Record not found");
                
                return Response.status(Response.Status.OK).entity(data).build();
            }
            
            if("moneywave".equalsIgnoreCase(product)){
                
                compliance.setAmlPolicyPath(Utility.fixMoneywavePath(compliance.getAmlPolicyPath()));
                compliance.setCompanyRegDocumentPath(Utility.fixMoneywavePath(compliance.getCompanyRegDocumentPath()));
                compliance.setScumlCertificatePath(Utility.fixMoneywavePath(compliance.getScumlCertificatePath()));
                compliance.setDirectorIdPath(Utility.fixMoneywavePath(compliance.getDirectorIdPath()));
                compliance.setOperatingLicencePath(Utility.fixMoneywavePath(compliance.getOperatingLicencePath()));
            }
            
            data.put("responsecode", "00");
            data.put("responsemessage", "Successful");
            data.put("data", GetComplianceModel.getComplianceModel(compliance));
                
            compliance = null;
            
            return Response.status(Response.Status.OK).entity(data).build();
                
//            return Response.status(Response.Status.OK).entity(compliance).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    @Path(value = "/merchant/kyc/approve")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantKYCApprove(@QueryParam(value = "id") long id,
            @QueryParam(value = "product") String product, @QueryParam(value = "status") String status) {

        try {
            if (id <= 0 || product == null) {
                return null;
            }
//        if("rave".equalsIgnoreCase(product)){

            MerchantCompliance compliance = merchantCompliantDao.findByMerchantId(id+"", product);
//        }
            
            if("rave".equalsIgnoreCase(product) && compliance == null){
                            compliance = merchantCompliantDao.findByMerchantId(id+"", "ravepay");
            }
            
            Map<String, String> data = new HashMap<>();
            
            if(compliance == null){
                
                data.put("responsecode", "01");
                data.put("responsemessage", "Unable to approve compliance");
                
                return Response.status(Response.Status.OK).entity(data).build();
            }
            
//            if("NG".equalsIgnoreCase(compliance.getCountry())){
//            
////                if(compliance.isBvnChecked() == false){
////                    
////                    data.put("responsecode", "01");
////                    data.put("responsemessage", "Bvn verification has not been performed");
////
////                    return Response.status(Response.Status.OK).entity(data).build();
////                }
//                    
//            }
                
            
            if("rave".equalsIgnoreCase(product) || "ravepay".equalsIgnoreCase(product)){
                
                RaveMerchant merchant =  raveMerchantDao.find(id);
                
                if(merchant != null){
                    
                    JsonObject jsonObject;
                    
                    if("".equalsIgnoreCase(status) || status == null){
                        jsonObject = httpUtil.goLiveRaveMerchant(merchant.getId()+"", true);
                    }else
                        jsonObject = httpUtil.goLiveRaveMerchant(merchant.getId()+"", false);
                    
                    if(jsonObject == null){
                        
                        data.put("responsecode", "01");
                        data.put("responsemessage", "Unable to approve merchant");

                        return Response.status(Response.Status.OK).entity(data).build();
                    }
                }
                
            }else if("moneywave".equalsIgnoreCase(product)){
                
                MoneywaveMerchant merchant =  moneywaveMerchantDao.find(id);
                
                if(merchant != null){
                    
                    JsonObject jsonObject;
                    
                    if("".equalsIgnoreCase(status) || status == null)
                        jsonObject = httpUtil.complianceMoneywaveMerchant(merchant.getSecret(), true);
                    else
                        jsonObject = httpUtil.complianceMoneywaveMerchant(merchant.getSecret(), false);
                        
                    
                    if(jsonObject == null){
                        
                        data.put("responsecode", "01");
                        data.put("responsemessage", "Unable to approve merchant");

                        return Response.status(Response.Status.OK).entity(data).build();
                    }
                }
            }
            
            compliance.setApprovedOn(new Date());
            
            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");
            
            compliance.setApprovedBy(user);
            
            if("".equalsIgnoreCase(status) || status == null){
                compliance.setStatus(Status.APPROVED);
            }
            else
                compliance.setStatus(Status.PENDING);
            
            compliance.setLastProcessed(new Date());
            compliance.setProcessingCounter(compliance.getProcessingCounter()+1);
            
            MerchantComplianceLog merchantComplianceLog = MerchantComplianceLog.fromCompliance(compliance);
            
            merchantCompliantLogDao.create(merchantComplianceLog);
            
            merchantCompliantDao.update(compliance);
            
            data.put("responsecode", "00");
            
            if("".equalsIgnoreCase(status) || status == null){
                data.put("responsemessage", "Record has been approved successfully");
                notificationManager.sendComplianceApprovedEmail(compliance.getContactEmail(),
                    compliance.getRegisteredName(), compliance.getProducts());
            }
            else
                data.put("responsemessage", "Approved Record has been reversed successfully");
            
            return Response.status(Response.Status.OK).entity(data).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    @Path(value = "/merchant/kyc/reject")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantKYCReject(@QueryParam(value = "id") long id,
            @QueryParam(value = "product") String product, 
            @QueryParam(value = "reason") String reason,
            @QueryParam(value = "status") String status) {

        Map<String, String> data = new HashMap<>();
        
        try {
            
            if (id <= 0 || product == null || (reason == null || "".equalsIgnoreCase(reason))) {
                data.put("responsecode", "01");
                data.put("responsemessage", "please provide all required information");
                
                return Response.status(Response.Status.OK).entity(data).build();
            }
            
            
//        if("rave".equalsIgnoreCase(product)){

            MerchantCompliance compliance = merchantCompliantDao.findByMerchantId(id+"", product);
//        }
            
            if("rave".equalsIgnoreCase(product) && compliance == null){
                compliance = merchantCompliantDao.findByMerchantId(id+"", "ravepay");
            }
            
            
            String url = null;
            
            Product productObj ;
            
            if(compliance == null){
                
                data.put("responsecode", "01");
                data.put("responsemessage", "Unable to reject compliance");
                
                return Response.status(Response.Status.OK).entity(data).build();
            }
            
            
            if("rave".equalsIgnoreCase(product) || "ravepay".equalsIgnoreCase(product)){
                
                RaveMerchant merchant =  raveMerchantDao.find(id);
                
                if(merchant != null){
                    
                    JsonObject jsonObject = httpUtil.goLiveRaveMerchant(merchant.getId()+"", false);
                    
                    if(jsonObject == null){
                        
                        data.put("responsecode", "01");
                        data.put("responsemessage", "Unable to reject compliance");

                        return Response.status(Response.Status.OK).entity(data).build();
                    }
                }
                
                productObj = productDao.findByKey("name", "rave");
                
                if(productObj != null){
                    url = productObj.getLiveUrl();
                }else
                    url = "https://ravepay.co";
                
            }else if("moneywave".equalsIgnoreCase(product)){
                
                MoneywaveMerchant merchant =  moneywaveMerchantDao.find(id);
                
                if(merchant != null){
                    
                    JsonObject jsonObject = httpUtil.complianceMoneywaveMerchant(merchant.getSecret(), false);
                    
                    if(jsonObject == null){
                        
                        data.put("responsecode", "01");
                        data.put("responsemessage", "Unable to reject merchant");

                        return Response.status(Response.Status.OK).entity(data).build();
                    }
                }
                
                productObj = productDao.findByKey("name", "moneywave");
                
                if(productObj != null){
                    url = productObj.getLiveUrl();
                }else
                    url = "https://moneywave.flutterwave.com";
            }
            
            compliance.setApprovedOn(new Date());
            
            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");
            
            
            compliance.setApprovedBy(user);
            if("".equalsIgnoreCase(status) || status == null)
                compliance.setStatus(Status.REJECTED);
            else
                compliance.setStatus(Status.PENDING);
            
            compliance.setLastProcessed(new Date());
            compliance.setProcessingCounter(compliance.getProcessingCounter()+1);
            
            MerchantComplianceLog merchantComplianceLog = MerchantComplianceLog.fromCompliance(compliance);
            
            createComplianceLog(merchantComplianceLog);
            
            try {
                reason = new String(reason.getBytes(), "ISO-8859-1");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if(compliance.getRejectionReason() != null)
                compliance.setRejectionReason(reason+","+compliance.getRejectionReason());
            else
                compliance.setRejectionReason(reason);
            
            merchantCompliantDao.update(compliance);
            
            if(("".equalsIgnoreCase(status) || status == null) && compliance.isNotifyCustomer() == true )
                notificationManager.sendComplianceRejectedEmail(compliance.getContactEmail(),
                        compliance.getRegisteredName(), compliance.getProducts(), url, reason);
            
            data.put("responsecode", "00");
            data.put("responsemessage", "Record has been rejected successfully");
            
            
            return Response.status(Response.Status.OK).entity(data).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
       data.put("responsecode", "01");
       data.put("responsemessage", "Unable to reject record");
        
       return Response.status(Response.Status.OK).entity(data).build();
    }

    @Path(value = "/merchant/kyc/message")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response sendKYCMessage(SendComplianceMessage message){
        
        
        Map<String, String> data = new HashMap<>();
        
        
        MerchantCompliance compliance = null;
        
        try {
            compliance = merchantCompliantDao.findByMerchantId(message.getId(), message.getProduct());
            
            if("rave".equalsIgnoreCase(message.getProduct()) && compliance == null){
                compliance = merchantCompliantDao.findByMerchantId(message.getId(), "ravepay");
            }
        } catch (Exception ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if(compliance == null){

            data.put("responsecode", "01");
            data.put("responsemessage", "Unable to send compliance message");

            return Response.status(Response.Status.OK).entity(data).build();
        }
        
        if(compliance.isNotifyCustomer() == true)
            notificationManager.sendComplianceEmail(compliance.getContactEmail(), 
                compliance.getRegisteredName(), compliance.getProducts(), null, message.getMessage());
        
        data.put("responsecode", "00");
        data.put("responsemessage", "Message sent successfully");

        return Response.status(Response.Status.OK).entity(data).build();
    }
    
    @Transactional
    public void createComplianceLog(MerchantComplianceLog complianceLog) throws DatabaseException{
        
        merchantCompliantLogDao.create(complianceLog);
    }
    
    // This section is for currency
    @Path("/country/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeCountry(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            CountryM sectorM = countryMDao.find(id);

            if (sectorM == null) {
                response.put("status-code", "03");
                response.put("status", "country not found");
                return response;
            }

            Log log = new Log();
            log.setAction("User Authorization with id " + sectorM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (status == true) {
                if (sectorM.getModelId() <= 0) {

                    Country country = countryDao.findByKey("shortName", sectorM.getShortName());

                    if (country != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate country with short name " + country.getShortName());
                        return response;
                    }

                    country = countryDao.findByKey("name", sectorM.getName());

                    if (country != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate country with name " + country.getName());
                        return response;
                    }

                    country = new Country();

                    country.setCreatedBy(sectorM.getCreatedBy());
                    country.setCreatedOn(sectorM.getCreatedOn());
                    country.setEnabled(true);
                    country.setName(sectorM.getName());
                    country.setShortName(sectorM.getShortName());
                    country.setApprovedBy(user);
                    country.setApprovedOn(new Date());
                    country.setCurrency(sectorM.getCurrency());

                    countryDao.create(country);

                } else {

                    Country country = countryDao.findByKey("shortName", sectorM.getShortName());

                    if (country != null && country.getId() != sectorM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate country with short name " + country.getShortName());
                        return response;
                    }

                    country = countryDao.findByKey("name", sectorM.getName());

                    if (country != null && country.getId() != sectorM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate country with name " + country.getName());
                        return response;
                    }

                    country = countryDao.find(sectorM.getModelId());

                    country.setEnabled(sectorM.isEnabled());
                    country.setModifiedOn(country.getCreatedOn());
                    country.setName(sectorM.getName());
                    country.setShortName(sectorM.getShortName());
                    country.setApprovedBy(user);
                    country.setApprovedOn(new Date());
                    country.setCurrency(sectorM.getCurrency());

                    countryDao.update(country);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            sectorM.setApprovedOn(new Date());
            sectorM.setApprovedBy(user);
            sectorM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                sectorM.setStatus(Status.APPROVED);
            } else {
                sectorM.setStatus(Status.REJECTED);
            }

            countryMDao.update(sectorM);

            log = new Log();
            log.setAction("Country Authorization for " + sectorM.getId() + " , " + sectorM.getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("countryupdated", true);
            session.setAttribute("countryupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Country Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Country Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    // This section is for currency
    @Path("/sector/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeSector(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            SectorM sectorM = sectorMDao.find(id);

            if (sectorM == null) {
                response.put("status-code", "03");
                response.put("status", "sector not found");
                return response;
            }

            Log log = new Log();
            log.setAction("User Authorization with id " + sectorM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (status == true) {
                if (sectorM.getModelId() <= 0) {

                    Sector sector = sectorDao.findByKey("name", sectorM.getName());

//                    if (sector != null) {
//
//                        response.put("status-code", "02");
//                        response.put("status", "Duplicate sector with short name " + sector.getName());
//                        return response;
//                    }
//
//                    sector = sectorDao.findByKey("name", queryM.getName());
                    if (sector != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate sector with name " + sector.getName());
                        return response;
                    }

                    sector = new Sector();

                    sector.setCreatedBy(sectorM.getCreatedBy());
                    sector.setCreatedOn(sectorM.getCreatedOn());
                    sector.setName(sectorM.getName());
                    sector.setApprovedBy(user);
                    sector.setApprovedOn(new Date());

                    sectorDao.create(sector);

                } else {

                    Sector sector = sectorDao.findByKey("name", sectorM.getName());

                    if (sector != null && sector.getId() != sectorM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate sector with name " + sector.getName());
                        return response;
                    }

                    sector = sectorDao.find(sectorM.getModelId());

                    sector.setModifiedOn(sector.getCreatedOn());
                    sector.setName(sectorM.getName());
                    sector.setApprovedBy(user);
                    sector.setApprovedOn(new Date());

                    sectorDao.update(sector);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            sectorM.setApprovedOn(new Date());
            sectorM.setApprovedBy(user);
            sectorM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                sectorM.setStatus(Status.APPROVED);
            } else {
                sectorM.setStatus(Status.REJECTED);
            }

            sectorMDao.update(sectorM);

            log = new Log();
            log.setAction("Sector Authorization for " + sectorM.getId() + " , " + sectorM.getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("sectorupdated", true);
            session.setAttribute("sectorupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Sector Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Sector Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path("/country/changestatus")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> changeCountryStatus(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            Country country = countryDao.find(id);

            if (country == null) {
                response.put("status-code", "03");
                response.put("status", "country not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Changing country status " + country.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Changing country Status");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            List<CountryM> list = countryMDao.find("modelId", country.getId());

            if (list != null && !list.isEmpty()) {

                CountryM cM = list.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {

//                    JsfUtil.addErrorMessage("A pending update exists");
                    response.put("status-code", "04");
                    response.put("status", "Pending update exist");
                    return response;
                }
            }

            CountryM countryM = new CountryM();
            countryM.setModelId(country.getId());
            countryM.setCreatedOn(new Date());
            countryM.setEnabled(!"disable".equalsIgnoreCase(updateModel.getOperation()));
            countryM.setName(country.getName());
            countryM.setShortName(country.getShortName());
            countryM.setCreatedBy(user);

            countryMDao.create(countryM);

            log = new Log();
            log.setAction("Changing country status " + country.getId() + " , " + country.getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Changing country status");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }

            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("countryupdated", true);
            session.setAttribute("countryupdatedm", true);

            response.put("status-code", "00");
            response.put("status", "Status has been change modified successfully and submitted for approval");

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Changing Country Status");
                log.setCreatedOn(new Date());
                log.setDescription("Changing Country Status");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    /**
     * This is called to authorize merchant
     *
     * @param updateModel
     * @return
     */
    @Path("/merchant/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
//    @Transactional
    public Map<String, String> authorizeMerchant(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            MerchantM merchantM = merchantMDao.find(id);

            if (merchantM == null) {
                response.put("status-code", "03");
                response.put("status", "merchant merchant record not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Merchant Authorization with id " + merchantM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (merchantM.getModelId() <= 0 && status == true) {

//                Merchant merchant = merchantDao.findByKey("rcNumber", merchantM.getRcNumber());
//
//                if (merchant != null) {
//
//                    if(!(merchant.getProduct() != null && merchantM.getProduct().getName().equalsIgnoreCase(merchant.getProduct().getName()))){
//                        
//                            response.put("status-code", "02");
//                            response.put("status", "Duplicate merchant with for merchant with name " + merchant.getCompanyName());
//                            return response;
//                     }
////                    response.put("status-code", "02");
////                    response.put("status", "Duplicate merchant for merchant " + merchantM.getRcNumber());
////                    return response;
//                }
                if ("rave".equalsIgnoreCase(merchantM.getProduct().getName())) {

                    GenericMerchant raveMerchant = findMerchantByName("rave", merchantM.getCompanyName());

                    if (raveMerchant != null) {
                        response.put("status-code", "02");
                        response.put("status", "merchant with name " + merchantM.getCompanyName() + " exists");
                        return response;
                    }
                } else if ("moneywave".equalsIgnoreCase(merchantM.getProduct().getName())) {

                    GenericMerchant moneywaveMerchant = findMerchantByName("moneywave", merchantM.getCompanyName());

                    if (moneywaveMerchant != null) {
                        response.put("status-code", "02");
                        response.put("status", "merchant with name " + merchantM.getCompanyName() + " exists");
                        return response;
                    }
                }

                Merchant merchant = new Merchant();

                merchant.setCreatedBy(merchantM.getCreatedBy());
                merchant.setCreatedOn(merchantM.getCreatedOn());
                merchant.setProduct(merchantM.getProduct());
                merchant.setAccountNo(merchantM.getAccountNo());
                merchant.setCompanyName(merchantM.getCompanyName());
                merchant.setApprovedOn(new Date());
                merchant.setApprovedBy(user);

                // This section is for international merchant
                merchant.setAddressLine1(merchantM.getAddressLine1());
                merchant.setAddressLine2(merchantM.getAddressLine2());
                merchant.setBank(merchantM.getBank());
                merchant.setContactPerson(merchantM.getContactPerson());
                merchant.setCountry(merchantM.getCountry());
                merchant.setCurrency(merchantM.getCurrency());
                merchant.setEmail(merchantM.getEmail());
                merchant.setFirstName(merchantM.getFirstName());
                merchant.setLastName(merchantM.getLastName());
                merchant.setMerchantType(merchantM.getMerchantType());
//                merchant.setPassword("12345677");
                merchant.setPhone(merchantM.getPhone());
                merchant.setProduct(merchantM.getProduct());
                merchant.setRcNumber(merchantM.getRcNumber());
                merchant.setEnabled(true);
                merchant.setSector(merchantM.getSector());
                // end of setting intertional merchant

                if ("rave".equalsIgnoreCase(merchantM.getProduct().getName())) {

                    JsonObject jsonObject = httpUtil.registerRaveMerchant(merchant);

                    if (jsonObject == null) {
                        throw new RuntimeException("Unable to create rave merchant");
                    }

                } else if ("moneywave".equalsIgnoreCase(merchantM.getProduct().getName())) {

                    JsonObject jsonObject = httpUtil.registerMoneywaveMerchant(merchant);

                    if (jsonObject == null) {
                        throw new RuntimeException("Unable to create moneywave merchant");
                    }
                }

//                if (status == true) {
                merchantDao.create(merchant);
//                }

            } else {

                if (status == true) {

                    Merchant merchant = merchantDao.findByKey("rcNumber", merchantM.getRcNumber());

//                    if (merchant != null && merchant.getId() != merchantM.getModelId()) {
//                        
//                        if(!(merchant.getProduct() != null && merchantM.getProduct().getName().equalsIgnoreCase(merchant.getProduct().getName()))){
//                        
//                            response.put("status-code", "02");
//                            response.put("status", "Duplicate merchant with for merchant with name " + merchant.getCompanyName());
//                            return response;
//                        }
//                    }
//                    merchant = merchantDao.find(merchantM.getModelId());
//
////                    merchant.setbanBy(merchantM.getCreatedBy());
//                    merchant.setModifiedOn(merchantM.getCreatedOn());
//                    //                merchant.setProduct(merchantM.getProduct());
//
//                    // This section is for international merchant
//                    merchant.setProduct(merchantM.getProduct());
//                    merchant.setAccountNo(merchantM.getAccountNo());
//                    merchant.setCompanyName(merchantM.getCompanyName());
//                    merchant.setApprovedOn(new Date());
//                    merchant.setApprovedBy(user);
//
//                    // This section is for international merchant
//                    merchant.setAddressLine1(merchantM.getAddressLine1());
//                    merchant.setAddressLine2(merchantM.getAddressLine2());
//                    merchant.setBank(merchantM.getBank());
//                    merchant.setContactPerson(merchantM.getContactPerson());
//                    merchant.setCountry(merchantM.getCountry());
//                    merchant.setCurrency(merchantM.getCurrency());
//                    merchant.setEmail(merchantM.getEmail());
//                    merchant.setFirstName(merchantM.getFirstName());
//                    merchant.setLastName(merchantM.getLastName());
//                    merchant.setMerchantType(merchantM.getMerchantType());
////                    merchant.setPassword("12345677");
//                    merchant.setPhone(merchantM.getPhone());
//                    merchant.setProduct(merchantM.getProduct());
//                    merchant.setRcNumber(merchantM.getRcNumber());
//                    merchant.setEnabled(merchantM.isEnabled());
                    // end of setting pob merchant
                    if ("rave".equalsIgnoreCase(merchantM.getProduct().getName())) {

                        GenericMerchant gm = findMerchantById(merchantM.getProduct().getName(), merchantM.getModelId());

                        JsonObject jsonObject = httpUtil.enableRaveMerchant(gm.getEmail(), merchantM.isEnabled());

                        if (jsonObject == null) {
                            throw new RuntimeException("Unable to create rave merchant");
                        }

                    } else if ("moneywave".equalsIgnoreCase(merchantM.getProduct().getName())) {

                        GenericMerchant gm = findMerchantById(merchantM.getProduct().getName(), merchantM.getModelId());

                        JsonObject jsonObject = httpUtil.updateMoneywaveMerchant(gm.getSecret(), merchantM.isEnabled());

                        if (jsonObject == null) {
                            throw new RuntimeException("Unable to create moneywave merchant");
                        }
                    } else if ("core".equalsIgnoreCase(merchantM.getProduct().getName()) || "flutterwave core".equalsIgnoreCase(merchantM.getProduct().getName())) {

                        GenericMerchant gm = findMerchantById(merchantM.getProduct().getName(), merchantM.getModelId());

                        JsonObject jsonObject = httpUtil.changeMerchantStatus(gm.getSecret(), merchantM.isEnabled());

                        if (jsonObject == null) {
                            throw new RuntimeException("Unable to create moneywave merchant");
                        }
                    }

//                    merchantDao.update(merchant);
                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            merchantM.setApprovedOn(new Date());
            merchantM.setApprovedBy(user);
            merchantM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                merchantM.setStatus(Status.APPROVED);
            } else {
                merchantM.setStatus(Status.REJECTED);
            }

            updateMerchantM(merchantM);
//            merchantMDao.update(merchantM);

            log = new Log();
            log.setAction("Merchant Authorization for " + merchantM.getId() + " , " + merchantM.getCompanyName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("merchantupdated", true);
            session.setAttribute("merchantupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Fee Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Fee Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        } catch (Exception ex) {
            response.put("status", "" + ex.getMessage());
            response.put("status-code", "96");
        }

        return response;
    }

    @Transactional
    public void updateMerchantM(MerchantM merchantM) throws DatabaseException {

        merchantMDao.update(merchantM);
    }

//    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public GenericMerchant findMerchantByName(String productName, String businessName) throws DatabaseException {

        if ("rave".equalsIgnoreCase(productName)) {

            RaveMerchant raveMerchant = raveMerchantDao.findByKey("businessName", businessName);

            if (raveMerchant == null) {
                return null;
            }

            GenericMerchant gm = raveMerchant.getGeneric();
            RaveAddressBook addressBook = addressBookDao.findByMerchant(raveMerchant);
            if (addressBook != null) {
                gm.setEmail(addressBook.getEmail());
            }

            return raveMerchant.getGeneric();

        } else if ("moneywave".equalsIgnoreCase(productName)) {

            MoneywaveMerchant moneywaveMerchant = moneywaveMerchantDao.findByKey("name", businessName);

            if (moneywaveMerchant != null) {

                return moneywaveMerchant.getGeneric();
            }
        }

        return null;
    }

//    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public GenericMerchant findMerchantById(String productName, long id) throws DatabaseException {

        if ("rave".equalsIgnoreCase(productName)) {

            RaveMerchant raveMerchant = raveMerchantDao.find(id);

            if (raveMerchant != null) {
                RaveAddressBook addressBook = addressBookDao.findByMerchant(raveMerchant);
                GenericMerchant gm = raveMerchant.getGeneric();

                gm.setEmail(addressBook.getEmail());
                gm.setActive((addressBook.getBlocked() == 0));

                return gm;
            }

            return null;

        } else if ("moneywave".equalsIgnoreCase(productName)) {

            MoneywaveMerchant moneywaveMerchant = moneywaveMerchantDao.find(id);

            if (moneywaveMerchant != null) {

                return moneywaveMerchant.getGeneric();
            }
        } else if ("core".equalsIgnoreCase(productName) || "flutterwave core".equalsIgnoreCase(productName)) {

            CoreMerchant coreMerchant = coreMerchantDao.find(id);

            if (coreMerchant != null) {

                GenericMerchant gm = coreMerchant.getGeneric();

                //gm.setSecret(coreMerchant.getTestToken());
                gm.setSecret(coreMerchant.getTestToken());

                return gm;
            }
        }

        return null;
    }

    @Path("/merchant/changestatus")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map<String, String> changeStatusMerchant(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            List<MerchantM> list = merchantMDao.find("modelId", id);

            if (list != null && !list.isEmpty()) {

                MerchantM cM = list.stream().filter(x -> x.getApprovedOn() == null && x.getProduct().getName().equalsIgnoreCase(updateModel.getProduct())).findFirst().orElse(null);

                if (cM != null) {

//                    JsfUtil.addErrorMessage("A pending update exists");
                    response.put("status-code", "04");
                    response.put("status", "Pending update exist");
                    return response;
                }
            }

            GenericMerchant merchant = new GenericMerchant();

//            Product product = productDao.findByKey("name", updateModel.getProduct());;
            if ("rave".equalsIgnoreCase(updateModel.getProduct())) {
                RaveMerchant raveMerchant = raveMerchantDao.find(id);
                merchant = raveMerchant.getGeneric();

            } else if ("moneywave".equalsIgnoreCase(updateModel.getProduct())) {
                MoneywaveMerchant moneywaveMerchant = moneywaveMerchantDao.find(id);
                merchant = moneywaveMerchant.getGeneric();
            } else if ("core".equalsIgnoreCase(updateModel.getProduct()) || "flutterwave core".equalsIgnoreCase(updateModel.getProduct())) {
                CoreMerchant coreMerchant = coreMerchantDao.find(id);
                merchant = coreMerchant.getGeneric();
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            Product product = productDao.findByKey("name", "" + updateModel.getProduct());
            MerchantM merchantM = new MerchantM();
            merchantM.setCreatedOn(new Date());
            merchantM.setCreatedBy(user);
            merchantM.setEnabled(true);

            merchantM.setStatus(Status.PENDING);
            merchantM.setProduct(product);

//            if("Rave".equalsIgnoreCase(merchant.getProduct().getName())){
            merchantM.setCompanyName(merchant.getName());
            merchantM.setEnabled(!"disable".equalsIgnoreCase(updateModel.getOperation()));
            merchantM.setProduct(product);
            merchantM.setModelId(merchant.getId());
//                merchantM.set
//            }

            createMerchant(merchantM, product);

            response.put("status-code", "00");
            response.put("status", "Merchant updated and submitted for approval");

            Log log = new Log();
            log.setAction("Merchant Status Update for " + merchantM.getId() + " , " + merchantM.getCompanyName());
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("merchantupdated", true);
            session.setAttribute("merchantupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Fee Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Fee Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    // This section is for banks
    @Path("/bank/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeBank(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            BankM sectorM = bankMDao.find(id);

            if (sectorM == null) {
                response.put("status-code", "03");
                response.put("status", "bank not found");
                return response;
            }

            Log log = new Log();
            log.setAction("User Authorization with id " + sectorM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (status == true) {
                if (sectorM.getModelId() <= 0) {

                    Bank bank = bankDao.findByKey("code", sectorM.getCode());

                    if (bank != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate bank with code name " + bank.getCode());
                        return response;
                    }

                    bank = bankDao.findByKey("name", sectorM.getName());

                    if (bank != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate bank with name " + bank.getName());
                        return response;
                    }

                    bank = new Bank();

                    bank.setCreatedBy(sectorM.getCreatedBy());
                    bank.setCreatedOn(sectorM.getCreatedOn());
                    bank.setName(sectorM.getName());
                    bank.setCode(sectorM.getCode());
                    bank.setApprovedBy(user);
                    bank.setApprovedOn(new Date());
                    bank.setProduct(sectorM.getProduct());
                    bank.setProductId(sectorM.getProductId());

                    bankDao.create(bank);

                } else {

                    Bank bank = bankDao.findByKey("code", sectorM.getCode());

                    if (bank != null && bank.getId() != sectorM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate bank with short name " + bank.getCode());
                        return response;
                    }

                    bank = bankDao.findByKey("name", sectorM.getName());

                    if (bank != null && bank.getId() != sectorM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate bank with name " + bank.getName());
                        return response;
                    }

                    bank = bankDao.find(sectorM.getModelId());

//                    query.setModified(queryM.getCreatedOn());
                    bank.setName(sectorM.getName());
                    bank.setCode(sectorM.getCode());
                    bank.setApprovedBy(user);
                    bank.setApprovedOn(new Date());
                    bank.setProduct(sectorM.getProduct());
                    bank.setProductId(sectorM.getProductId());

                    bankDao.update(bank);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            sectorM.setApprovedOn(new Date());
            sectorM.setApprovedBy(user);
            sectorM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                sectorM.setStatus(Status.APPROVED);
            } else {
                sectorM.setStatus(Status.REJECTED);
            }

            bankMDao.update(sectorM);

            log = new Log();
            log.setAction("Bank Authorization for " + sectorM.getId() + " , " + sectorM.getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("bankupdated", true);
            session.setAttribute("bankupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Bank Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Bank Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path("/account/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeAccount(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            AccountQueryM queryM = accountQueryMDao.find(id);

            if (queryM == null) {
                response.put("status-code", "03");
                response.put("status", "bank not found");
                return response;
            }

            Log log = new Log();
            log.setAction("User Authorization with id " + queryM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (status == true) {
                if (queryM.getModelId() <= 0) {

                    AccountQuery query = accountQueryDao.findByKey("accountNo", queryM.getAccountNo());

                    if (query != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate bank with name " + query.getAccountNo());
                        return response;
                    }

                    query = new AccountQuery();

                    query.setCreatedBy(queryM.getCreatedBy());
                    query.setCreatedOn(queryM.getCreatedOn());
                    query.setAccountNo(queryM.getAccountNo());
                    query.setBank(queryM.getBank());
                    query.setApprovedBy(user);
                    query.setApprovedOn(new Date());

                    accountQueryDao.create(query);

                } else {

                    AccountQuery query = accountQueryDao.findByKey("name", queryM.getAccountNo());

                    if (query != null && query.getId() != queryM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate bank with name " + query.getAccountNo());
                        return response;
                    }

                    query = accountQueryDao.find(queryM.getModelId());

                    query.setAccountNo(queryM.getAccountNo());
                    query.setBank(queryM.getBank());
                    query.setApprovedBy(user);
                    query.setApprovedOn(new Date());
                    query.setAccountNo(queryM.getAccountNo());
                    query.setApprovedBy(user);
                    query.setApprovedOn(new Date());

                    accountQueryDao.update(query);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            queryM.setApprovedOn(new Date());
            queryM.setApprovedBy(user);
            queryM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                queryM.setStatus(Status.APPROVED);
            } else {
                queryM.setStatus(Status.REJECTED);
            }

            accountQueryMDao.update(queryM);

            log = new Log();
            log.setAction("AccountQuery Authorization for " + queryM.getId() + " , " + queryM.getAccountNo());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("accountupdated", true);
            session.setAttribute("accountupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("AccountQuery Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("AccountQuery Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Transactional
    public MerchantM createMerchant(MerchantM merchant, Product product) throws DatabaseException {

        merchant.setProduct(product);
        return merchantMDao.create(merchant);
    }
    // This section is the utility section

    public boolean findUnauthorizedCountry(long modelId) {

        try {
            List<CountryM> countries = countryMDao.find("modelId", modelId + "");

            if (countries != null && !countries.isEmpty()) {

                CountryM cM = countries.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {
                    return true;
                }
            }

        } catch (Exception exception) {
            Logger.getLogger(CountryController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return false;
    }

    @Path("/control/changestatus")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> changeControlStatus(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            ControlModel control = controlDao.find(id);

            if (control == null) {
                response.put("status-code", "03");
                response.put("status", "control not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Changing control status " + control.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Changing control Status");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            List<ControlModelM> list = controlMDao.find("modelId", control.getId());

            if (list != null && !list.isEmpty()) {

                ControlModelM cM = list.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {

//                    JsfUtil.addErrorMessage("A pending update exists");
                    response.put("status-code", "04");
                    response.put("status", "Pending update exist");
                    return response;
                }
            }

            ControlModelM controlM = new ControlModelM();
            controlM.setModelId(control.getId());
            controlM.setCreatedOn(new Date());
            controlM.setState(!"disable".equalsIgnoreCase(updateModel.getOperation()));
            controlM.setName(control.getName());
            controlM.setCreatedBy(user);

            controlMDao.create(controlM);

            log = new Log();
            log.setAction("Changing control status " + control.getId() + " , " + control.getName());
            log.setCreatedOn(new Date());
//            if (status == true) {
            log.setDescription("Changing control status");
//            } else {
//                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
//            }

            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("controlupdated", true);
            session.setAttribute("controlupdatedm", true);

            response.put("status-code", "00");
            response.put("status", "Status has been change modified successfully and submitted for approval");

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Changing Control Status");
                log.setCreatedOn(new Date());
                log.setDescription("Changing Control Status");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path("/control/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeControl(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            ControlModelM sectorM = controlMDao.find(id);

            if (sectorM == null) {
                response.put("status-code", "03");
                response.put("status", "control not found");
                return response;
            }

            Log log = new Log();
            log.setAction("User Authorization with id " + sectorM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (status == true) {
                if (sectorM.getModelId() <= 0) {

                    ControlModel control = controlDao.findByKey("name", sectorM.getName());

                    if (control != null) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate control with name " + control.getName());
                        return response;
                    }

                    control = new ControlModel();

                    control.setCreatedBy(sectorM.getCreatedBy());
                    control.setCreatedOn(sectorM.getCreatedOn());
                    control.setName(sectorM.getName());
                    control.setApprovedBy(user);
                    control.setApprovedOn(new Date());
                    control.setState(true);

                    controlDao.create(control);

                } else {

                    ControlModel control = controlDao.findByKey("name", sectorM.getName());

                    if (control != null && control.getId() != sectorM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate control with name " + control.getName());
                        return response;
                    }

                    control = controlDao.find(sectorM.getModelId());

//                    control.setModified(queryM.getCreatedOn());
                    control.setName(sectorM.getName());
                    control.setApprovedBy(user);
                    control.setApprovedOn(new Date());

                    controlDao.update(control);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            sectorM.setApprovedOn(new Date());
            sectorM.setApprovedBy(user);
            sectorM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                sectorM.setStatus(Status.APPROVED);
            } else {
                sectorM.setStatus(Status.REJECTED);
            }

            controlMDao.update(sectorM);

            log = new Log();
            log.setAction("ControlModel Authorization for " + sectorM.getId() + " , " + sectorM.getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("controlupdated", true);
            session.setAttribute("controlupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("ControlModel Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("ControlModel Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }
    
    // This section is for currency
    @Path("/funding/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeFunding(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            AirtimeFundingM airtimeFundingM = airtimeFundingMDao.find(id);

            if (airtimeFundingM == null) {
                response.put("status-code", "03");
                response.put("status", "currency not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Airtime Funding with id " + airtimeFundingM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Airtime Funding Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (status == true) {

                 
                String otp = airtimeHelper.generateVoucher(airtimeFundingM.getAmount().doubleValue(), 
                        airtimeFundingM.getMerchantId(), airtimeFundingM.getCurrency(), airtimeFundingM.getReference());
                
               
                if (otp == null) {

                    response.put("status-code", "02");
                    response.put("status", "Unable to complete funding (t)");
                    return response;
                }

                VoucherFundingResponse fundingResponse = airtimeHelper.applyVoucher(otp, airtimeFundingM.getMerchantId());
                
                
                if(fundingResponse == null){
                       
                    response.put("status-code", "03");
                    response.put("status", "Unable to complete funding (vl)");
                    return response;
                }
                

                AirtimeFunding airtimeFunding = new AirtimeFunding();
                airtimeFunding.setAmount(airtimeFundingM.getAmount());
                airtimeFunding.setAppliedBy(user);
                airtimeFunding.setAppliedOn(new Date());
                airtimeFunding.setBalanceAfter(new BigDecimal(fundingResponse.getBalanceAfter()));  
                airtimeFunding.setBalanceBefore(new BigDecimal(fundingResponse.getBalanceBefore()));
                airtimeFunding.setCurrency(airtimeFundingM.getCurrency());
                airtimeFunding.setMerchantId(airtimeFundingM.getMerchantId());
                airtimeFunding.setMerchantName(airtimeFundingM.getMerchantName());
                airtimeFunding.setReference(airtimeFundingM.getReference());
                airtimeFunding.setCreatedOn(airtimeFundingM.getCreatedOn());
                airtimeFunding.setInitiatedBy(airtimeFundingM.getCreatedBy());
                airtimeFunding.setVoucher(otp);
                
                airtimeFundingDao.create(airtimeFunding);
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            airtimeFundingM.setApprovedOn(new Date());
            airtimeFundingM.setApprovedBy(user);
            airtimeFundingM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                airtimeFundingM.setStatus(Status.APPROVED);
            } else {
                airtimeFundingM.setStatus(Status.REJECTED);
            }

            airtimeFundingMDao.update(airtimeFundingM);

            log = new Log();
            log.setAction("Funding For " + airtimeFundingM.getId() + " , " + airtimeFundingM.getMerchantName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Funding approved Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("fundingupdated", true);
            session.setAttribute("fundingupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Funding Approval");
                log.setCreatedOn(new Date());
                log.setDescription("Funding approval error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path("/fundings")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getFundings(
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw) {

        try {

            Date startDate = null, endDate = null;

            PageResult<AirtimeFundingViewModel> pageResult = new PageResult<>();

//            if (product == null) {
//                return Response.status(Response.Status.OK).entity(pageResult).build();
//            }

            List<Product> products = productDao.findAll();

            if (products == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            if (products.isEmpty()) {

                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<AirtimeFunding> merchants = new ArrayList<>();

            long totalCount = 0;

            
            Page<AirtimeFunding> page =  airtimeFundingDao.find(start, length);
            
            
            List<AirtimeFundingViewModel> fundingViewModels = new ArrayList<>();
            
            for(AirtimeFunding airtimeFunding : page.getContent()){
                
                AirtimeFundingViewModel fundingViewModel = new AirtimeFundingViewModel();
                fundingViewModel.setAmount(airtimeFunding.getAmount());
                fundingViewModel.setAppliedBy(airtimeFunding.getAppliedBy().getFirstName());
                fundingViewModel.setAppliedOn(airtimeFunding.getAppliedOn());
                fundingViewModel.setBalanceAfter(airtimeFunding.getBalanceAfter());
                fundingViewModel.setBalanceBefore(airtimeFunding.getBalanceBefore());
                fundingViewModel.setCreatedOn(airtimeFunding.getCreatedOn());
                fundingViewModel.setCurrency(airtimeFunding.getCurrency());
                fundingViewModel.setInitiatedBy(airtimeFunding.getInitiatedBy().getFirstName());
                fundingViewModel.setId(airtimeFunding.getId());
                fundingViewModel.setMerchantId(airtimeFunding.getMerchantId());
                fundingViewModel.setMerchantName(airtimeFunding.getMerchantName());
                fundingViewModel.setReference(airtimeFunding.getReference());
                fundingViewModel.setVoucher(airtimeFunding.getVoucher());
                
                fundingViewModels.add(fundingViewModel);
            }
            

//            if (!merchants.isEmpty()) {
            pageResult = new PageResult(fundingViewModels, page.getCount(), page.getCount());
//            }

            return Response.status(Response.Status.OK).entity(pageResult).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CoreTransactionDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.RaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionDao;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.model.products.GenericMerchant;
import com.flutterwave.flutter.clientms.model.products.MoneywaveMerchant;
import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author emmanueladeyemi
 */
@Path(value = "/product/report")
@RequestScoped
public class ReportApiController {

    @EJB
    private CoreTransactionDao coreTransactionDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private MoneywaveMerchantDao moneywaveMerchantDao;
    @EJB
    private RaveMerchantDao raveMerchantDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private RaveTransactionDao raveTransactionDao;
    @EJB
    private MoneywaveTransactionDao moneywaveTransactionDao;

    @GET
    @Path(value = "/core")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getCoreReport(@QueryParam(value = "product") String product, @QueryParam(value = "merchant") String merchant,
            @QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency, @QueryParam(value = "grouping") String grouping) {

        Date startDate = null, endDate = null;

//        String range = reportModel.getRange();
        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        List report = coreTransactionDao.getReportSummary(startDate, endDate, null, null, merchant, currency);

        switch (grouping.toLowerCase()) {
            case "daily": {

                Map<Date, Object> data = new TreeMap<>();

                for (Object obj : report) {

                    Map<String, Double> map;

                    Object[] reportElement = (Object[]) obj;

                    Double amount = ((BigDecimal) reportElement[0]).doubleValue();
                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);

                    map = (HashMap<String, Double>) data.getOrDefault(date, new HashMap<>());

                    if ("success".equalsIgnoreCase(status)) {
                        amount += map.getOrDefault("successful", 0.0);
                        map.put("successful", amount);
                    } else {
                        amount += map.getOrDefault("failed", 0.0);
                        map.put("failed", amount);
                    }

                    data.put(date, map);
                }

                return Response.status(Response.Status.OK).entity(data).build();
            }

            case "weekly": {

                Map<String, Object> data = new TreeMap<>();

                int firstWeek = getWeek(startDate);

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount = ((BigDecimal) reportElement[0]).doubleValue();
                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);

                    Map<String, Double> map;

                    int currentWeek = (getWeek(date) / firstWeek) + (getWeek(date) % firstWeek);
                    map = (Map<String, Double>) data.getOrDefault("week " + currentWeek, new TreeMap<>());

                    if ("success".equalsIgnoreCase(status)) {
                        amount += map.getOrDefault("successful", 0.0);
                        map.put("successful", amount);
                    } else {
                        amount += map.getOrDefault("failed", 0.0);
                        map.put("failed", amount);
                    }

                    data.put("week " + currentWeek, map);
                }

                return Response.status(Response.Status.OK).entity(data).build();

            }

            case "monthly": {

                Map<String, Object> data = new TreeMap<>();

                SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount = ((BigDecimal) reportElement[0]).doubleValue();
                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);

                    String d = dateFormat.format(date);
                    Map<String, Double> map;

                    map = (TreeMap<String, Double>) data.getOrDefault(d, new TreeMap<>());

                    if ("success".equalsIgnoreCase(status)) {
                        amount += map.getOrDefault("successful", 0.0);
                        map.put("successful", amount);
                    } else {
                        amount += map.getOrDefault("failed", 0.0);
                        map.put("failed", amount);
                    }

                    data.put(d, map);
                }

                return Response.status(Response.Status.OK).entity(data).build();
            }

            case "quarterly": {

                Map<String, Object> data = new TreeMap<>();

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount = ((BigDecimal) reportElement[0]).doubleValue();
                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(startDate);

                    String d = "Quarter " + getQuarter(calendar.get(Calendar.YEAR), date);
                    Map<String, Double> map;

                    map = (TreeMap<String, Double>) data.getOrDefault(d, new TreeMap<>());

                    if ("success".equalsIgnoreCase(status)) {
                        amount += map.getOrDefault("successful", 0.0);
                        map.put("successful", amount);
                    } else {
                        amount += map.getOrDefault("failed", 0.0);
                        map.put("failed", amount);
                    }

                    data.put(d, map);
                }

                return Response.status(Response.Status.OK).entity(data).build();
            }

            case "yearly": {

                Map<String, Object> data = new TreeMap<>();

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount = ((BigDecimal) reportElement[0]).doubleValue();
                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);

                    String d = dateFormat.format(date);
                    Map<String, Double> map;

                    map = (TreeMap<String, Double>) data.getOrDefault(d, new TreeMap<>());

                    if ("success".equalsIgnoreCase(status)) {
                        amount += map.getOrDefault("successful", 0.0);
                        map.put("successful", amount);
                    } else {
                        amount += map.getOrDefault("failed", 0.0);
                        map.put("failed", amount);
                    }

                    data.put(d, map);
                }

                return Response.status(Response.Status.OK).entity(data).build();
            }
        }

        return null;
    }

    @GET
    @Path(value = "/main")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getReport(@QueryParam(value = "product") String product, @QueryParam(value = "merchant") String merchant,
            @QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency, @QueryParam(value = "grouping") String grouping) {

        Date startDate = null, endDate = null;

//        String range = reportModel.getRange();
        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        List report = new ArrayList();

        if ("rave".equalsIgnoreCase(product)) {
            report = raveTransactionDao.getReportSummary(startDate, endDate, null, null, merchant, currency);
        } else if ("moneywave".equalsIgnoreCase(product)) {
            report = moneywaveTransactionDao.getReportSummary(startDate, endDate, null, null, merchant, currency);
        } else if ("core".equalsIgnoreCase(product)) {
            report = coreTransactionDao.getReportSummary(startDate, endDate, null, null, merchant, currency);
        }

        double total = 0, successful = 0, failed = 0;
        long totalCount = 0, totalSuccessful = 0, totalFailed = 0;

        switch (grouping.toLowerCase()) {
            case "daily": {

                Map<Date, Object> data = new TreeMap<>();
                Map<Date, Object> volume = new TreeMap<>();

                for (Object obj : report) {

                    Map<String, Double> map;

                    Object[] reportElement = (Object[]) obj;
//                    
                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]);
                    }

                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);
                    Long count = (Long) ((BigInteger) reportElement[3] == null ? 0 : ((BigInteger) reportElement[3]).longValue());
                    Long c = count;
//                    String d = dateFormat.format(date);
                    Map<String, Long> mapVolume;

                    map = (TreeMap<String, Double>) data.getOrDefault(date, new TreeMap<>());
                    mapVolume = (TreeMap<String, Long>) volume.getOrDefault(date, new TreeMap<>());

                    double amnt = amount;

                    if ("success".equalsIgnoreCase(status)) {
                        amnt += map.getOrDefault("successful", 0.0);
                        map.put("successful", amnt);

                        count += mapVolume.getOrDefault("successful", 0L);
                        mapVolume.put("successful", count);

                        successful += amount;
                        totalSuccessful += c;

                    } else {
                        amnt += map.getOrDefault("failed", 0.0);
                        map.put("failed", amnt);

                        count += mapVolume.getOrDefault("failed", 0L);
                        mapVolume.put("failed", count);

                        totalFailed += c;
                        failed += amount;
                    }

                    total += amount;
                    totalCount += c;

                    volume.put(date, mapVolume);
                    data.put(date, map);
                }

                Map<String, Object> values = new HashMap<>();
                values.put("value", data);
                values.put("volume", volume);
                values.put("total", total);
                values.put("successful", successful);
                values.put("failed", failed);
                values.put("totalV", totalCount);
                values.put("successfulV", totalSuccessful);
                values.put("failedV", totalFailed);

                return Response.status(Response.Status.OK).entity(values).build();
            }

            case "weekly": {

                Map<String, Object> data = new TreeMap<>();
                Map<String, Object> volume = new TreeMap<>();

                int firstWeek = getWeek(startDate);

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]) == null ? 0 : ((Double) reportElement[0]);
                    }

                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);
                    Long count = (Long) ((BigInteger) reportElement[3] == null ? 0 : ((BigInteger) reportElement[3]).longValue());
                    Long c = count;
                    
                    Map<String, Double> map;

                    int currentWeek = (getWeek(date) / firstWeek) + (getWeek(date) % firstWeek);
                    map = (Map<String, Double>) data.getOrDefault("week " + currentWeek, new TreeMap<>());

                    Map<String, Long> mapVolume;

                    mapVolume = (TreeMap<String, Long>) volume.getOrDefault("week " + currentWeek, new TreeMap<>());
                    double amnt = amount;

                    if ("success".equalsIgnoreCase(status)) {
                        amnt += map.getOrDefault("successful", 0.0);
                        map.put("successful", amnt);

                        count += mapVolume.getOrDefault("successful", 0L);
                        mapVolume.put("successful", count);

                        successful += amount;
                        
                        totalSuccessful += c;
                    } else {
                        amnt += map.getOrDefault("failed", 0.0);
                        map.put("failed", amnt);

                        count += mapVolume.getOrDefault("failed", 0L);
                        mapVolume.put("failed", count);

                        failed += amount;
                        totalFailed += c;
                    }

                    total += amount;
                    totalCount += c;
                    
                    volume.put("week " + currentWeek, mapVolume);

                    data.put("week " + currentWeek, map);
                }

                Map<String, Object> values = new HashMap<>();
                values.put("value", data);
                values.put("volume", volume);
                values.put("total", total);
                values.put("successful", successful);
                values.put("failed", failed);
                values.put("totalV", totalCount);
                values.put("successfulV", totalSuccessful);
                values.put("failedV", totalFailed);
                
                return Response.status(Response.Status.OK).entity(values).build();

            }

            case "monthly": {

                Map<String, Object> data = new TreeMap<>();
                Map<String, Object> volume = new TreeMap<>();

                SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]) == null ? 0 : ((Double) reportElement[0]);
                    }
                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);
                    Long count = (Long) ((BigInteger) reportElement[3] == null ? 0 : ((BigInteger) reportElement[3]).longValue());

                    Long c = count;
                    
                    String d = dateFormat.format(date);
                    Map<String, Double> map;
                    Map<String, Long> mapVolume;

                    map = (TreeMap<String, Double>) data.getOrDefault(d, new TreeMap<>());
                    mapVolume = (TreeMap<String, Long>) volume.getOrDefault(d, new TreeMap<>());

                    double amnt = amount;

                    if ("success".equalsIgnoreCase(status)) {
                        amnt += map.getOrDefault("successful", 0.0);
                        map.put("successful", amnt);

                        count += mapVolume.getOrDefault("successful", 0L);
                        mapVolume.put("successful", count);

                        successful += amount;

                        totalSuccessful += c;
                        
                    } else {
                        amnt += map.getOrDefault("failed", 0.0);
                        map.put("failed", amnt);

                        count += mapVolume.getOrDefault("failed", 0L);
                        mapVolume.put("failed", count);

                        failed += amount;
                        totalFailed += c;
                    }

                    total += amount;
                    totalCount += c;
                    
                    volume.put(d, mapVolume);
                    data.put(d, map);
                }

                Map<String, Object> values = new HashMap<>();
                values.put("value", data);
                values.put("volume", volume);
                values.put("total", total);
                values.put("successful", successful);
                values.put("failed", failed);
                values.put("totalV", totalCount);
                values.put("successfulV", totalSuccessful);
                values.put("failedV", totalFailed);

                return Response.status(Response.Status.OK).entity(values).build();
            }

            case "quarterly": {

                Map<String, Object> data = new TreeMap<>();
                Map<String, Object> volume = new TreeMap<>();

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]) == null ? 0 : ((Double) reportElement[0]);
                    }

                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);
                    Long count = (Long) ((BigInteger) reportElement[3] == null ? 0 : ((BigInteger) reportElement[3]).longValue());

                    Long c = count;
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(startDate);

                    String d = "Quarter " + getQuarter(calendar.get(Calendar.YEAR), date);
                    Map<String, Double> map;

                    Map<String, Long> mapVolume;

                    map = (TreeMap<String, Double>) data.getOrDefault(d, new TreeMap<>());
                    mapVolume = (TreeMap<String, Long>) volume.getOrDefault(d, new TreeMap<>());
                    double amnt = amount;
                    

                    if ("success".equalsIgnoreCase(status)) {
                        amnt += map.getOrDefault("successful", 0.0);
                        map.put("successful", amnt);

                        count += mapVolume.getOrDefault("successful", 0L);
                        mapVolume.put("successful", count);

                        successful += amount;
                        totalSuccessful += c;
                        
                    } else {
                        amnt += map.getOrDefault("failed", 0.0);
                        map.put("failed", amnt);

                        count += mapVolume.getOrDefault("failed", 0L);
                        mapVolume.put("failed", count);

                        failed += amount;
                        
                        totalFailed += c;
                    }

                    total += amount;
                    totalCount += c;
                    
                    volume.put(d, mapVolume);
                    data.put(d, map);
                }

                Map<String, Object> values = new HashMap<>();
                values.put("value", data);
                values.put("volume", volume);
                values.put("total", total);
                values.put("successful", successful);
                values.put("failed", failed);
                values.put("totalV", totalCount);
                values.put("successfulV", totalSuccessful);
                values.put("failedV", totalFailed);

                return Response.status(Response.Status.OK).entity(values).build();
            }

            case "yearly": {

                Map<String, Object> data = new TreeMap<>();
                Map<String, Object> volume = new TreeMap<>();

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]) == null ? 0 : ((Double) reportElement[0]);
                    }
                    Date date = (Date) (reportElement[1]);
                    String status = (String) (reportElement[2]);
                    Long count = (Long) ((BigInteger) reportElement[3] == null ? 0 : ((BigInteger) reportElement[3]).longValue());
                    
                    Long c = count;
                    String d = dateFormat.format(date);
                    Map<String, Double> map;

//                    map = (TreeMap<String, Double>)data.getOrDefault(d, new TreeMap<>());
                    Map<String, Long> mapVolume;

                    map = (TreeMap<String, Double>) data.getOrDefault(d, new TreeMap<>());
                    mapVolume = (TreeMap<String, Long>) volume.getOrDefault(d, new TreeMap<>());

                    double amnt = amount;

                    if ("success".equalsIgnoreCase(status)) {
                        amnt += map.getOrDefault("successful", 0.0);
                        map.put("successful", amnt);

                        count += mapVolume.getOrDefault("successful", 0L);
                        mapVolume.put("successful", count);

                        successful += amount;
                        totalSuccessful += c;

                    } else {
                        amnt += map.getOrDefault("failed", 0.0);
                        map.put("failed", amnt);

                        count += mapVolume.getOrDefault("failed", 0L);
                        mapVolume.put("failed", count);

                        failed += amount;
                        totalFailed += c;
                    }

                    total += amount;
                    totalCount += c;

                    volume.put(d, mapVolume);
                    data.put(d, map);
                }

                Map<String, Object> values = new HashMap<>();
                values.put("value", data);
                values.put("volume", volume);
                values.put("total", total);
                values.put("successful", successful);
                values.put("failed", failed);
                values.put("totalV", totalCount);
                values.put("successfulV", totalSuccessful);
                values.put("failedV", totalFailed);
            
                return Response.status(Response.Status.OK).entity(values).build();
            }


        }

        return null;
    }

    List divideToWeek(Date startDate, Date endDate) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(startDate);

        calendar.getActualMaximum(Calendar.WEEK_OF_MONTH);

        return null;
    }

    /**
     * This is called to get the number of weeks between date
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private int noOfWeeks(Date startDate, Date endDate) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.setTime(endDate);

        int weeks = calendarEnd.getActualMaximum(Calendar.WEEK_OF_YEAR) - calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);

        return weeks;
    }

    private int getWeek(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int week = calendar.get(Calendar.WEEK_OF_YEAR);

        return week;
    }

    /**
     * year - start year of quarter
     *
     * @param year
     * @param date
     * @return
     */
    public int getQuarter(int year, Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int month = calendar.get(Calendar.MONTH);

        int yearInt = calendar.get(Calendar.YEAR);

        int value = 0;

        if (yearInt > year) {
            value += yearInt - year;
        }

        if (month <= 3) {
            return value += 1;
        }

        if (month <= 6) {
            return value += 2;
        }

        if (month <= 9) {
            return value += 3;
        } else {
            return value += 4;
        }
    }

    // helper utils
    @Path("/merchant")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchants(@QueryParam(value = "product") String product) {

        try {

            PageResult<GenericMerchant> pageResult = new PageResult<>();

            if (product == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            List<Product> products = productDao.findAll();

            if (products == null) {
                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

            if (products.isEmpty()) {

                return Response.status(Response.Status.OK).entity(pageResult).build();
            }

//            List<GenericMerchant> merchants = new ArrayList<>();
            List<Object> merchants = new ArrayList<>();

            long totalCount = 0;

            if ("rave".equalsIgnoreCase(product)) {

                Page<RaveMerchant> raveMerchant = raveMerchantDao.search(0, 0, null, null, null, null, null, null);

                totalCount += raveMerchant.getCount();

                if (raveMerchant.getContent() != null && !raveMerchant.getContent().isEmpty()) {

                    merchants.addAll(raveMerchant.getContent().stream().map((RaveMerchant rm) -> {
                        Map<String, Object> map = new HashMap<>();
                        map.put("id", rm.getId());
                        map.put("text", rm.getBusinessName()+" ("+rm.getId()+")");

                        return map;
                    }).collect(Collectors.<Map>toList()));

//                    extraData.put("rave", "" + raveMerchant.getContent().size());
                }
            } else if ("moneywave".equalsIgnoreCase(product)) {

                Page<MoneywaveMerchant> waveMerchant = moneywaveMerchantDao.search(0, 0, null, null, null, null, null, null);
                totalCount += waveMerchant.getCount();

                if (waveMerchant.getContent() != null && !waveMerchant.getContent().isEmpty()) {

                    merchants.addAll(waveMerchant.getContent().stream().map((MoneywaveMerchant rm) -> {

                        Map<String, Object> map = new HashMap<>();
                        map.put("id", rm.getId());
                        map.put("text", rm.getName()+" ("+rm.getId()+")");

                        return map;
                    }).collect(Collectors.<Map>toList()));
                }
            } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

                Page<CoreMerchant> coreMerchant = coreMerchantDao.search(0, 0, null, null, null, null, null);

                totalCount += coreMerchant.getCount();

                if (coreMerchant.getContent() != null && !coreMerchant.getContent().isEmpty()) {

                    merchants.addAll(coreMerchant.getContent().stream().map((CoreMerchant rm) -> {
                        Map<String, Object> map = new HashMap<>();
                        map.put("id", rm.getId());
                        map.put("text", rm.getCompanyname()+" ("+rm.getMerchantID()+")");

                        return map;
                    }).collect(Collectors.<Map>toList()));
                }
            }

//            pageResult = new PageResult(merchants, totalCount, totalCount);
            return Response.status(Response.Status.OK).entity(merchants).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
    }
    
    @Path("/coremerchant")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getMerchantCore() {

        try {
            
            Page<CoreMerchant> coreMerchant = coreMerchantDao.search(0, 0, null, null, null, null, null);

            List<Map> merchants = new ArrayList<>();
                        

            if (coreMerchant.getContent() != null && !coreMerchant.getContent().isEmpty()) {

                merchants.addAll(coreMerchant.getContent().stream().map((CoreMerchant rm) -> {
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", rm.getMerchantID());
                    map.put("text", rm.getCompanyname());

                    return map;
                }).collect(Collectors.<Map>toList()));
           }

//            pageResult = new PageResult(merchants, totalCount, totalCount);
            return Response.status(Response.Status.OK).entity(merchants).build();

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
    }

//    // helper utils
//    @Path("/corereport")
//    @GET
//    @Produces(value = MediaType.APPLICATION_JSON)

}

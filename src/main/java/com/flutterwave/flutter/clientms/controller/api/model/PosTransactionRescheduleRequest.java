/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author emmanueladeyemi
 */
public class PosTransactionRescheduleRequest {

    /**
     * @return the posId
     */
    public String[] getPosId() {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(String[] posId) {
        this.posId = posId;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the all
     */
    public Boolean getAll() {
        return all;
    }

    /**
     * @param all the all to set
     */
    public void setAll(Boolean all) {
        this.all = all;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

//    /**
//     * @return the posId
//     */
//    public String getPosId() {
//        return posId;
//    }
//
//    /**
//     * @param posId the posId to set
//     */
//    public void setPosId(String posId) {
//        this.posId = posId;
//    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }
    
    @NotBlank(message = "Date cannot be null")
    private String date;
    @JsonProperty(value = "merchant_id")
    private String merchantId;
    @JsonProperty(value = "merchant_code")
    private String merchantCode;
    @JsonProperty(value = "pos_id")
    private String[] posId;
    @JsonProperty(value = "reference")
    private String reference;
    private Boolean all;
    @JsonProperty(value = "end_date")
    private String endDate;
}

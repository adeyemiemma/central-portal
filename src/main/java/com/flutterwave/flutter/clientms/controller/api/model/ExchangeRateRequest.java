/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author emmanueladeyemi
 */
public class ExchangeRateRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the originCurrency
     */
    public String getOriginCurrency() {
        return originCurrency;
    }

    /**
     * @param originCurrency the originCurrency to set
     */
    public void setOriginCurrency(String originCurrency) {
        this.originCurrency = originCurrency;
    }

    /**
     * @return the destinationCurrency
     */
    public String getDestinationCurrency() {
        return destinationCurrency;
    }

    /**
     * @param destinationCurrency the destinationCurrency to set
     */
    public void setDestinationCurrency(String destinationCurrency) {
        this.destinationCurrency = destinationCurrency;
    }
    
    @NotNull(message = "origin currency must  be provided")
    @JsonProperty(value = "origin_currency")
    private String originCurrency;
    @NotNull(message = "destination currency must  be provided")
    @JsonProperty(value = "destination_currency")
    private String destinationCurrency;
    private String hash;
}

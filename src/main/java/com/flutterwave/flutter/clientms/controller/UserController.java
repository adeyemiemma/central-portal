/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.ApiUserDao;
import com.flutterwave.flutter.clientms.dao.BankDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.RoleDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.UserMDao;
import com.flutterwave.flutter.clientms.model.ApiUser;
import com.flutterwave.flutter.clientms.model.Bank;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.ResetPasswordModel;
import com.flutterwave.flutter.clientms.model.Role;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.UserM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.NotificationManager;
import com.flutterwave.flutter.clientms.service.UserService;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.ChangePasswordModel;
import com.flutterwave.flutter.clientms.viewmodel.LoginModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "userController")
@SessionScoped
public class UserController implements Serializable {

    /**
     * @return the resetPasswordModel
     */
    public ResetPasswordModel getResetPasswordModel() {
        
        if(resetPasswordModel == null)
            resetPasswordModel = new ResetPasswordModel();
        
        return resetPasswordModel;
    }

    /**
     * @param resetPasswordModel the resetPasswordModel to set
     */
    public void setResetPasswordModel(ResetPasswordModel resetPasswordModel) {
        this.resetPasswordModel = resetPasswordModel;
    }

    /**
     * @return the passwordModel
     */
    public ChangePasswordModel getPasswordModel() {
        
        if(passwordModel == null)
            passwordModel = new ChangePasswordModel();
        
        return passwordModel;
    }

    /**
     * @param passwordModel the passwordModel to set
     */
    public void setPasswordModel(ChangePasswordModel passwordModel) {
        this.passwordModel = passwordModel;
    }

    /**
     * @return the apiUser
     */
    public ApiUser getApiUser() {
        
        if(apiUser == null)
            apiUser = new ApiUser();
        
        return apiUser;
    }

    /**
     * @param apiUser the apiUser to set
     */
    public void setApiUser(ApiUser apiUser) {
        this.apiUser = apiUser;
    }

    
    /**
     * @return the selectedUser
     */
    public User getSelectedUser() {
        
        if(selectedUser == null)
            selectedUser = new User();
            
        return selectedUser;
    }

    /**
     * @param selectedUser the selectedUser to set
     */
    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    /**
     * @return the loginModel
     */
    public LoginModel getLoginModel() {
        
        if(loginModel == null)
            loginModel = new LoginModel();
        
        return loginModel;
    }

    /**
     * @param loginModel the loginModel to set
     */
    public void setLoginModel(LoginModel loginModel) {
        this.loginModel = loginModel;
    }
    
    public String loginOTP(){
        
        
        try{
            boolean status = userService.validatePassword(loginModel);

            if(status == false){
                JsfUtil.addErrorMessage("Invalid username or password");
                return "/login";
            }

            String otp = userService.generateOTP(loginModel.getUsername(), loginModel.getPassword());
            
            User user = userDao.findByKey("username", loginModel.getUsername());
            
            if(user.isEnabled() == false){
                JsfUtil.addErrorMessage("User is not enable please contact admin");
                return "/login";
            }
            
            notificationManager.sendLoginTokenSMTP(user.getEmail(), user.getDomainName(), user.getDomainId(),
                    user.getFirstName() + " "+user.getLastName(), otp, ""+expiry);
            
            loginModel.setMaskedEmail(userService.maskEmail(user.getEmail()));
            
            loginModel.setToken(otp);
            
            return "/loginotp?faces-redirect=true"; 
            
        }catch(Exception ex){
            if(ex != null)
                ex.printStackTrace();
        }
        
        return "/login";
    }
    
    public String validateOTP(){
        
        boolean status = userService.validate(loginModel.getUsername(), loginModel.getPassword(),loginModel.getToken(), expiry);
        
        if(status == false){
            
            JsfUtil.addErrorMessage("Invalid / Expired OTP");
            return "/loginotp"; 
        }
        
        login();
        
        return null;
    }
    
    public void login(){
        
        if(loginModel != null){

            boolean status = userService.loginUser(loginModel);
            
            if(status == true){
                try {
                    
                    Session session =  SecurityUtils.getSubject().getSession();
                    User user = userDao.findByKey("username", loginModel.getUsername());
                    
                    session.setAttribute("fullName", user.getFirstName()+" "+user.getLastName());
                    
                    if(user.isPasswordChanged() == false){
                        
                        FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/changepassword");
                        return;
                    }
                    
                    if(user.isEnabled() == false){
                        JsfUtil.addErrorMessage("User is not enable please contact admin");
                        return;
                    }
                    
                    if("bank_chargeback".equalsIgnoreCase(user.getDomainName())){
                        global.setDomainId(""+user.getDomainId());
                        
                        Bank bank = bankDao.findByKey("name",user.getDomainId());

                        session.setAttribute("domainName", user.getDomainName()+"");
                        session.setAttribute("domainId", user.getDomainId()+"");
                        
                        if(bank != null){
                            session.setAttribute("productId", bank.getProductId()+"");
                            session.setAttribute("product", bank.getProduct()+"");
                        }
                        
                        FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "home");
                        FacesContext.getCurrentInstance().getExternalContext().redirect("homepage.xhtml?faces-redirect=true");
                    }
                    else if("bank_airtime".equalsIgnoreCase(user.getDomainName())){
                        global.setDomainId(""+user.getDomainId());

                        session.setAttribute("domainName", user.getDomainName()+"");
                        session.setAttribute("domainId", user.getDomainId()+"");
                        
                        FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "home");
                        FacesContext.getCurrentInstance().getExternalContext().redirect("home.xhtml?faces-redirect=true");
                    }
                    else if("bank".equalsIgnoreCase(user.getDomainName())){
                        global.setDomainId(""+user.getDomainId());

                        session.setAttribute("domainName", user.getDomainName()+"");
                        session.setAttribute("domainId", user.getDomainId()+"");
                        
                        FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/recon/index");
                        FacesContext.getCurrentInstance().getExternalContext().redirect("recon/index.xhtml");
                    }
                    else{
//                        FacesContext.getCurrentInstance().getExternalContext().redirect("/index");
                        //FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/index");
                        
                        session.setAttribute("domainName", null);
                        FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "index?faces-redirect=true");
                    }
                    session.setAttribute("fullName", user.getFirstName()+" "+user.getLastName());
                    
                    loginModel = new LoginModel();
                    
//                    if(savedRequest != null)
////                        FacesContext.getCurrentInstance().getExternalContext().redirect(savedRequest.getRequestUrl());
////                    else
//                        FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
//                    else
//                        FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                    
                } catch (Exception ex) {
                    Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                }   
            }else{
                
                JsfUtil.addErrorMessage("Invalid username or password");
            }
        }   
    }
    
    public String create(){
        
        try{
            
            User user = userDao.findByKey("email",selectedUser.getEmail());

            if(user != null){
                JsfUtil.addErrorMessage("User with email exists on the platform");
                return "/user/create";
            }
            
            if(selectedUser.getRole() == null){
                
                JsfUtil.addErrorMessage("Please select user role");
                return "/user/create";
            }   
            
            selectedUser.setUsername(selectedUser.getEmail());
            selectedUser.setCreatedOn(new Date());
            String password = new Sha256Hash((selectedUser.getFirstName().trim()).toLowerCase()+""+
                    (selectedUser.getLastName().trim()).toLowerCase())
                    .toHex();
            selectedUser.setPassword(password);
            selectedUser.setEnabled(true);
//            userDao.create(selectedUser);

            UserM userM = new UserM();
            userM.setCreated(selectedUser.getCreatedOn());
            userM.setEmail(selectedUser.getEmail());
            userM.setEnabled(true);
            userM.setPhone(selectedUser.getPhone());
            userM.setFirstName(selectedUser.getFirstName());
            userM.setLastName(selectedUser.getLastName());
            userM.setRole(selectedUser.getRole());
            userM.setStatus(Status.PENDING);
            userM.setUsername(selectedUser.getEmail());
            userM.setPassword(password);
            
            if(!"admin".equalsIgnoreCase(selectedUser.getDomainName()))
                userM.setDomainId(selectedUser.getDomainId());
            
            userM.setDomainName(selectedUser.getDomainName());
            userM.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            
            userMDao.create(userM);
            
            JsfUtil.addSuccessMessage("User has been created successfully and submitted for approval");
            
            selectedUser = new User();
            
            Session session = SecurityUtils.getSubject().getSession();
            
            session.setAttribute("userupdated", true);
            session.setAttribute("userupdatedm", true);
            
            return "/user/create";
            
        }catch(Exception ex){
            
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        
        return "/user/create";
    }
    
    public String createSystemUser(){
        
        try{
            User user = userDao.findByKey("email",selectedUser.getEmail());

            if(user != null){
                JsfUtil.addErrorMessage("User with email exists on the platform");
                return "/user/create_system";
            }
            
            selectedUser.setUsername(selectedUser.getEmail());
            selectedUser.setCreatedOn(new Date());
            String password = new Sha256Hash((selectedUser.getFirstName().trim()).toLowerCase()+""+
                    (selectedUser.getLastName().trim()).toLowerCase())
                    .toHex();
            selectedUser.setPassword(password);
            selectedUser.setEnabled(true);
            userDao.create(selectedUser);
            
            JsfUtil.addSuccessMessage("User has been created successfully");
            
            selectedUser = new User();
            
            return "/user/create_system";
            
        }catch(Exception ex){
            
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        
        return "/user/create_system";
    }
    
    public String view(String id){
        
        try {
            if(id == null){
                
                JsfUtil.addErrorMessage("No user has been selected");
                return "/user/list";
            }
            
            selectedUser = userDao.find(Long.parseLong(id));
            
            return "/user/view";
        } catch (DatabaseException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "/user/list";
    }
    
    public String prepareUpdate(String id){
        
        try {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            
            if(id == null){
                
                JsfUtil.addErrorMessage("No user has been selected");
                return "/user/edit";
            }
            
            selectedUser = userDao.find(Long.parseLong(id));
            
            return "/user/edit";
        } catch (DatabaseException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "/user/list";
    }
    
    public String update(){
        
        try{
            User user = userDao.findByKey("email",selectedUser.getEmail());

            if(user != null && user.getId() != selectedUser.getId()){
                JsfUtil.addErrorMessage("User with email exists on the platform");
                return "/user/edit";
            }
            
            user = userDao.find(selectedUser.getId());
            
            if(selectedUser.getRole() == null){
                
                JsfUtil.addErrorMessage("Please select user role");
                return "/user/edit";
            }   
            
            user.setUsername(selectedUser.getEmail());
            user.setPhone(selectedUser.getPassword());
            user.setUsername(selectedUser.getEmail());
            user.setEmail(selectedUser.getEmail());
            user.setFirstName(selectedUser.getFirstName());
            user.setLastName(selectedUser.getLastName());
            user.setModified(new Date());
            
            UserM userM = new UserM();
            userM.setCreated(new Date());
            userM.setEmail(selectedUser.getEmail());
            userM.setEnabled(selectedUser.isEnabled());
            userM.setFirstName(selectedUser.getFirstName());
            userM.setLastName(selectedUser.getLastName());
            userM.setRole(selectedUser.getRole());
            userM.setStatus(Status.PENDING);
            userM.setPhone(selectedUser.getPhone());
            userM.setUsername(selectedUser.getEmail());
            userM.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            userM.setModelId(user.getId());
            
            // This is done to ensure user record is updated
            user = userDao.find(selectedUser.getId());
            
            userMDao.create(userM);
            
            JsfUtil.addSuccessMessage("User record has been updated successfully and submitted for approval");
            
            return "/user/edit";
            
        }catch(Exception ex){
            
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        
        return "/user/edit";
    }
    
    @FacesConverter(value = "roleConverter")
    public static class RoleConverter implements Converter{

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            
            if(value == null)
                return null;
            
            UserController controller = (UserController)  context.getApplication()
                        .getELResolver().getValue(context.getELContext(), null, "userController");
            
            return controller.getRoleFromId(Long.parseLong(value));
            
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            
            if(value == null)
                return null;
            
            if(!(value instanceof Role))
                return null;
                
            Role p = (Role) value;
            
            return p.getId()+"";
        }   
    }
    
    public Role getRoleFromId(long id){
        try {
            return roleDao.find(id);
        } catch (DatabaseException ex) {
            Logger.getLogger(PermissionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    // This is used to get the permission status currently logged in user
    public boolean hasRole(String permissionName){
        
        if(permissionName == null)
            return false;
        
        permissionName = permissionName.replaceAll(" ", "_");
        
        String username = SecurityUtils.getSubject().getPrincipal()+"";
        
        if("admin@flutterwavego.com".equalsIgnoreCase(username)){
            return true;
        }
        
        boolean status = SecurityUtils.getSubject().isPermitted(permissionName);
        
        return status;
    }
    
    public boolean hasRoles(String permissionName){
        
        if(permissionName == null)
            return false;
        
        String[] sArray = permissionName.split(",");
        
        String username = SecurityUtils.getSubject().getPrincipal()+"";
        
        if("admin@flutterwavego.com".equalsIgnoreCase(username)){
            return true;
        }
        
        for(String s : sArray){
            s = s.trim().replaceAll(" ", "_");
            
            boolean status = SecurityUtils.getSubject().isPermitted(s);
            
            if(status == true)
                return true;
        }
        
        return false;
    }
    
    
    public PaginationHelper getPagination() {
        
        if (pagination == null) {
            pagination = new PaginationHelper(50) {
                @Override
                public long getItemsCount() {
                    return userDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(userDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }
    
    
    public PaginationHelper getPaginationUnauth() {
        
        if (paginationUnauth == null) {
            paginationUnauth = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return userMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(userMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauth;
    }
    
    public String resendOTP() throws DatabaseException{
        
        
        if(loginModel == null || Utility.nullToEmpty(loginModel.getUsername()) == null){
            
            return "/login?faces-redirect=true"; 
        }
        
        String otp = userService.generateOTP(loginModel.getUsername(), loginModel.getPassword());
            
        User user = userDao.findByKey("username", loginModel.getUsername());

        notificationManager.sendLoginToken(user.getEmail(), user.getDomainName(), user.getDomainId(),
                user.getFirstName() + " "+user.getLastName(), otp, ""+expiry);
        
        JsfUtil.addSuccessMessage("OTP has been resent, please check your mail");
        
        return "";
    }
    
     public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("userupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("userupdated", false);
        }

        Log log = new Log();
        log.setAction("Fetch All user");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All user");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);
 
        logService.log(log, null);

        if (pagination == null) {
            dataModel = getPagination().createPageDataModel();
        }

        return dataModel;
    }
     
    public String getStyle(boolean status){
        
        return status == true ? "tag-success" : "tag-danger";
    }
    
    public String getStatus(boolean status){
        
        return status == true ? "Active" : "Inactive";
    }
    
    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("userupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauth = null;
            session.setAttribute("userupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth user");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth user");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);
 
        logService.log(log, null);

        if (paginationUnauth == null) {
            unauthdataModel = getPaginationUnauth().createPageDataModel();
        }

        return unauthdataModel;
    }
    
    public void relogin(){
        
        loginModel = new LoginModel();
        
        FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/login");
    }
    
    public String changePassword(){
        
        try {
            String username = String.valueOf(SecurityUtils.getSubject().getPrincipal());
            
            User user = userDao.findByKey("username", username);
            
            boolean status = userService.validatePassword(passwordModel.getCurrentPassword(), user.getPassword());
            
            if(status == false){
                JsfUtil.addErrorMessage("Incorrect current password");
                
                return "/changepassword";
            }
            
            if(!passwordModel.getConfirmPassword().equals(passwordModel.getNewPassword())){
                JsfUtil.addErrorMessage("Password and confirm password does not match");
                
                return "/changepassword";
            }
            
            if(passwordModel.getNewPassword().equals(passwordModel.getCurrentPassword())){
                JsfUtil.addErrorMessage("New Password and Current password are the same");
                return "/changepassword";
            }
            
            String password = passwordModel.getNewPassword();
            
            if(!userService.validatePassword(password)){
                JsfUtil.addErrorMessage("Invalid Password, (password must contain at least 1 uppercase, 1 lowercase, a number, 1 special character and must be greater than 5)");
                return "/changepassword";
            }
            
            String newPassword = userService.hashPassword(passwordModel.getNewPassword());
            
            user.setPassword(newPassword);
            user.setPasswordChanged(true);
            
            userDao.update(user);
            
            notificationManager.sendPasswordChangeEmail(user.getFirstName(), 
                            user.getLastName(), user.getEmail(), user.getDomainName(), user.getDomainId());
            
            JsfUtil.addSuccessMessage("Password changed successfully. Please login.");
            
            Session session = SecurityUtils.getSubject().getSession();
            session.setAttribute("passwordChanged", "true");
            
            SecurityUtils.getSubject().logout();
            
            passwordModel = new ChangePasswordModel();
            
            FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/login");
            
            return null;
        } catch (DatabaseException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "/changepassword";
    }

    public void recreate() {
        pagination = null;
        paginationUnauth = null;
    }
    
    public String createApiUser(){
        
        try {
            
            if(apiUser.getUniqueId() == null){
                JsfUtil.addErrorMessage("User with unique id must be provided");
                return "/apiuser/create";
            }
            
            ApiUser au = apiUserDao.findByKey("uniqueId", apiUser.getUniqueId());
            
            if(au != null){
                JsfUtil.addErrorMessage("User with unique id exists on the platform");
                return "/apiuser/create";
            }
            
            
            apiUser.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            apiUser.setCreatedOn(new Date());
            apiUser.setPrivateKey(Utility.generateAccessToken(apiUser.getUniqueId()));
            
            apiUserDao.create(apiUser);
            
            JsfUtil.addSuccessMessage("User has been created successfully key - "+apiUser.getPrivateKey());
            
            apiUser = new ApiUser();
            
            return "/apiuser/create";
        } catch (DatabaseException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "/apiuser/create";
    }
    
     public String resetPassword(){
        
        try {
            String username = getResetPasswordModel().getEmail();
            
            User user = userDao.findByKey("username", username);
            
            
            if(user == null){
                JsfUtil.addErrorMessage("User not found");
//                
                return "/user/resetpassword";
            }
//            boolean status = userService.validatePassword(passwordModel.getCurrentPassword(), user.getPassword());
//            
//            if(status == false){
//                JsfUtil.addErrorMessage("Incorrect current password");
//                
//                return "/user/resetpassword";
//            }
            
            if(!resetPasswordModel.getConfirmPassword().equals(resetPasswordModel.getNewPassword())){
                JsfUtil.addErrorMessage("Password and confirm password does not match");
                
                return "/user/resetpassword";
            }
            
//            if(resetPasswordModel.getNewPassword().equals(resetPasswordModel.getCurrentPassword())){
//                JsfUtil.addErrorMessage("New Password and Current password are the same");
//                return "/changepassword";
//            }
            
            String password = getResetPasswordModel().getNewPassword();
            
            if(!userService.validatePassword(password)){
                JsfUtil.addErrorMessage("Invalid Password, (password must contain at least 1 uppercase, 1 lowercase, a number, 1 special character and must be greater than 5)");
                return "/user/resetpassword";
            }
            
            String newPassword = userService.hashPassword(getResetPasswordModel().getNewPassword());
            
            user.setPassword(newPassword);
            user.setPasswordChanged(false);
            
            userDao.update(user);
            
            notificationManager.sendPasswordResetEmail(user.getFirstName(), 
                            user.getLastName(), user.getEmail(), user.getDomainName(), user.getDomainId(), password);
            
            JsfUtil.addSuccessMessage("User password reset successful.");
//            
//            Session session = SecurityUtils.getSubject().getSession();
//            session.setAttribute("passwordChanged", "true");
            
//            SecurityUtils.getSubject().logout();
            
            setResetPasswordModel(new ResetPasswordModel());
            
//            FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "/login");
            
            return "/user/resetpassword";
        } catch (DatabaseException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "/changepassword";
    }
     
    public String getLoggedInName(){
        
        return SecurityUtils.getSubject().getSession().getAttribute("fullName")+"";
    }

    public boolean getState(boolean status){
        
        return status;
    }
    
    @EJB
    private UserService userService;
    @EJB
    private RoleDao roleDao;
    @EJB
    private UserDao userDao;
    @EJB
    private UserMDao userMDao;
    @EJB
    private LogService logService;
    @EJB
    private ApiUserDao apiUserDao;
    @EJB
    private NotificationManager notificationManager;
    @EJB
    private BankDao bankDao;
    
    @Inject
    private Global global;
    private LoginModel loginModel ;
    private User selectedUser;
    private PaginationHelper pagination, paginationUnauth;
    private DataModel dataModel, unauthdataModel;
    
    private final int expiry = 120;
    private ApiUser apiUser;
    
    private ChangePasswordModel passwordModel;
    private ResetPasswordModel resetPasswordModel;
}

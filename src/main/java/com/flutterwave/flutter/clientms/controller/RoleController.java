/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.PermissionDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.RoleDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.RoleMDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Permission;
import com.flutterwave.flutter.clientms.model.Role;
import com.flutterwave.flutter.clientms.model.maker.RoleM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import javax.transaction.Transactional;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "roleController")
@RequestScoped
public class RoleController implements Serializable {

    /**
     * @return the currentRole
     */
    public Role getCurrentRole() {
        
        if(currentRole == null)
            currentRole = new Role();
        
        return currentRole;
    }

    /**
     * @param currentRole the currentRole to set
     */
    public void setCurrentRole(Role currentRole) {
        this.currentRole = currentRole;
    }
    
    public String prepareCreate(){
        
        currentRole = new Role();
        
        return "/role/create";
    }
    
    @Transactional
    public String create(){
        
        try {
            Role r = roleDao.findByKey("name", (currentRole.getName().trim()).replaceAll(" ", "_"));
            
            
            Log log = new Log();
            log.setAction("create role");
            log.setCreatedOn(new Date());
            log.setDescription("role creation started");
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.STARTED);
            
            logService.log(log);
            
            if(r != null){
                JsfUtil.addErrorMessage("Role with name "+currentRole.getName()+" exists");
                
                log = new Log();
                log.setAction("create role");
                log.setCreatedOn(new Date());
                log.setDescription("duplicate role name");
                log.setLevel(LogLevel.Warning);
                log.setLogDomain(LogDomain.ADMIN);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                
                logService.log(log, "role with name "+currentRole.getName()+" exists on the platform");
                
                return "/role/create";
            }
            
            if(currentRole.getPermissions() != null && currentRole.getPermissions().isEmpty()){
                
                JsfUtil.addErrorMessage("At least one permission must be selected");
                return "/role/create";
            }
            
            currentRole.setName(currentRole.getName().trim().replaceAll(" ", "_"));
            currentRole.setCreatedOn(new Date());
            
            RoleM roleM = new RoleM();
            roleM.setCreated(currentRole.getCreatedOn());
            roleM.setDescription(currentRole.getDescription());
            roleM.setPermissions(currentRole.getPermissions());
            roleM.setName(currentRole.getName());
            roleM.setStatus(Status.PENDING);
            roleM.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            
            roleMDao.create(roleM);
            
            log = new Log();
            log.setAction("create role");
            log.setCreatedOn(new Date());
            log.setDescription("role created successfully");
            log.setLevel(LogLevel.Warning);
            log.setLogDomain(LogDomain.ADMIN);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            
            logService.log(log);
            
            JsfUtil.addSuccessMessage("Role has been created successfully and submitted for approval");
            
            currentRole = new Role();
            
            return "/role/create" ;
            
        } catch (Exception ex) {
            Logger.getLogger(RoleController.class.getName()).log(Level.SEVERE, null, ex);
            
            try{
                Log log = new Log();
                log.setAction("create role");
                log.setCreatedOn(new Date());
                log.setDescription("create role failed");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                
                if(ex != null){
                    
                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                }
                else
                    logService.log(log);
                
            }catch(Exception e){
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
//            JsfUtil.addErrorMessage("Unable to update product at the moment, please again later");
                
            if( ex != null)
                JsfUtil.addErrorMessage(ex.getMessage());
            else
                JsfUtil.addErrorMessage("Unable to create role at the moment, please try again later");
        }
        
        return "/role/create";
        
    }
    
    public String view(long id){
        
        try {
            if(id <= 0){
                JsfUtil.addErrorMessage("Role details not found");
                return "/role/list";
            }
            
            Log log = new Log();
            log.setAction("view role ");
            log.setCreatedOn(new Date());
            log.setDescription("viewing role with id "+id);
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.STARTED);
            
            logService.log(log);
            
            currentRole = roleDao.find(id, "permissions");
            
            return "/role/view";
        } catch (DatabaseException ex) {
            Logger.getLogger(RoleController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        
        return "/role/list";
    }
    
    public String prepareUpdate(){
        
        try {
            
            Map<String, String> map = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            
            String idString = map.getOrDefault("editParam", null);

            if(idString == null){
                JsfUtil.addErrorMessage("No role is selected");
                return "/role/list";
            }
            
            long id = Long.parseLong(idString);

            
            if(id <= 0 ){
                
                JsfUtil.addErrorMessage("No role is selected");
                return "/role/list";
            }
            
            Log log = new Log();
            log.setAction("update role");
            log.setCreatedOn(new Date());
            log.setDescription("update role started");
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.STARTED);
            
            logService.log(log);
            
            if(currentRole == null){
                
                JsfUtil.addErrorMessage("Role not selected");
                return "/role/list";
            }
            
            currentRole = roleDao.find(id, "permissions");
            
            return "/role/edit";
        } catch (DatabaseException ex) {
            Logger.getLogger(RoleController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public String update(){
        
        try {
            Role role = roleDao.findByKey("name", currentRole.getName());
            
            if(role != null && role.getId() != currentRole.getId()){
                
                Log log = new Log();
                log.setAction("update role");
                log.setCreatedOn(new Date());
                log.setDescription("update role started");
                log.setLevel(LogLevel.Warning);
                log.setLogDomain(LogDomain.ADMIN);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);

                logService.log(log, "Duplicate role with name "+role.getName());
                
                JsfUtil.addErrorMessage("Duplicate role with name "+role.getName());
                return "/role/edit";
            }
            
//            currentRole = roleDao.find(currentRole.getId());
            
            RoleM roleM = new RoleM();
            roleM.setCreated(new Date());
            roleM.setDescription(currentRole.getDescription());
            roleM.setPermissions(currentRole.getPermissions());
            roleM.setName(currentRole.getName().toUpperCase());
            roleM.setStatus(Status.PENDING);
            roleM.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            roleM.setModelId(currentRole.getId());

            roleMDao.create(roleM);
            
            Log log = new Log();
            log.setAction("update role");
            log.setCreatedOn(new Date());
            log.setDescription("update role started");
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            
            logService.log(log);
            
            JsfUtil.addSuccessMessage("Role has been updated successfully and submitted for approval");
            return "/role/edit";
            
        } catch (DatabaseException ex) {
            Logger.getLogger(RoleController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        
        return "/role/edit";
    }
    
    @FacesConverter(value = "permissionConverter")
    public static class PermissionConverter implements Converter{

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            
            if(value == null)
                return null;
            
            RoleController controller = (RoleController)  context.getApplication()
                        .getELResolver().getValue(context.getELContext(), null, "roleController");
            
            return controller.getPermissionFromString(Long.parseLong(value));
            
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            
            if(value == null)
                return null;
            
            if(!(value instanceof Permission))
                return null;
                
            Permission p = (Permission) value;
            
            return p.getId()+"";
        }   
    }
    
    public Permission getPermissionFromString(long id){
        try {
            return permissionDao.find(id);
        } catch (DatabaseException ex) {
            Logger.getLogger(PermissionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<Role> getAllRoles(){
        
        try {
            List<Role> roles = roleDao.findAll();
            
            return roles;
        } catch (DatabaseException ex) {
            Logger.getLogger(RoleController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return new ArrayList<>();
    }
    
    public PaginationHelper getPagination() {
        
        if (pagination == null) {
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return roleDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(roleDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }
    
    public PaginationHelper getPaginationUnauth() {
        
        if (paginationUnauth == null) {
            paginationUnauth = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return roleMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(roleMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauth;
    }
    
    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("roleupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauth = null;
            session.setAttribute("roleupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth roles");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth roles");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        if (paginationUnauth == null) {
            unauthdataModel = getPaginationUnauth().createPageDataModel();
        }

        return unauthdataModel;
    }

    public void recreate() {
        pagination = null;
        paginationUnauth = null;
    }
    
    private Role currentRole;
    @EJB
    private RoleDao roleDao;
    @EJB
    private PermissionDao permissionDao;
    @EJB
    private RoleMDao roleMDao;
    @EJB
    private UserDao userDao;
    @EJB
    private LogService logService;
    private PaginationHelper pagination, paginationUnauth;
    private DataModel dataModel, unauthdataModel;
}

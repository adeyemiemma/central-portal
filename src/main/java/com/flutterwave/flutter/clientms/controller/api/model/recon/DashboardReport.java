/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model.recon;

/**
 *
 * @author emmanueladeyemi
 */
public class DashboardReport {

    /**
     * @return the transactionSettledValue
     */
    public double getTransactionSettledValue() {
        return transactionSettledValue;
    }

    /**
     * @param transactionSettledValue the transactionSettledValue to set
     */
    public void setTransactionSettledValue(double transactionSettledValue) {
        this.transactionSettledValue = transactionSettledValue;
    }

    /**
     * @return the transactionValue
     */
    public double getTransactionValue() {
        return transactionValue;
    }

    /**
     * @param transactionValue the transactionValue to set
     */
    public void setTransactionValue(double transactionValue) {
        this.transactionValue = transactionValue;
    }

    /**
     * @return the transactionVolume
     */
    public long getTransactionVolume() {
        return transactionVolume;
    }

    /**
     * @param transactionVolume the transactionVolume to set
     */
    public void setTransactionVolume(long transactionVolume) {
        this.transactionVolume = transactionVolume;
    }

    /**
     * @return the cards
     */
    public long getCards() {
        return cards;
    }

    /**
     * @param cards the cards to set
     */
    public void setCards(long cards) {
        this.cards = cards;
    }

    /**
     * @return the merchant
     */
    public long getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(long merchant) {
        this.merchant = merchant;
    }
    
    private double transactionValue;
    private long transactionVolume;
    private long cards;
    private long merchant;
    private double transactionSettledValue;
    
}

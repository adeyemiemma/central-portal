/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.AccountQueryDao;
import com.flutterwave.flutter.clientms.dao.AccountQueryTransactionDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.AccountQueryMDao;
import com.flutterwave.flutter.clientms.model.AccountQuery;
import com.flutterwave.flutter.clientms.model.AccountQuery;
import com.flutterwave.flutter.clientms.model.AccountQueryTransaction;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.AccountQueryM;
import com.flutterwave.flutter.clientms.model.maker.BankM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import javax.transaction.Transactional;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "accountQueryController")
@SessionScoped
public class AccountQueryController implements Serializable {
    
    @EJB
    private AccountQueryDao accountQueryDao;
    @EJB
    private AccountQueryMDao accountQueryMDao;
    @EJB
    private LogService logService;
    @EJB
    private UserDao userDao;
    @EJB
    private AccountQueryTransactionDao accountQueryTransactionDao;
    
    private PaginationHelper pagination, paginationHelper;
    
    private AccountQuery accountQuery;
    
    private DataModel allAccounts = null;
    private DataModel allUnauthAccounts = null;
    
    public AccountQuery getAccountQuery(){
        
        if(accountQuery == null)
            accountQuery = new AccountQuery();
        
        return accountQuery;
    }
    
    public void setAccountQuery(AccountQuery accountQuery){
        this.accountQuery = accountQuery;
    }
    
    public List<AccountQuery> getAllAccount() throws DatabaseException {

        List<AccountQuery> account = accountQueryDao.findAll();
        return account;
    }
    
    @Transactional
    public String create() {

        if (accountQuery != null) {
            accountQuery.setCreatedOn(new Date());
        }

        try {
            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");

            AccountQuery c = accountQueryDao.findByKey("accountNo", accountQuery.getAccountNo());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create accountQuery: " + accountQuery.getAccountNo());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate accountQuery with accountNo " + c.getAccountNo());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("AccountQuery with accountNo %s exists", accountQuery.getAccountNo()));

                return "create";
            }
            
            List<AccountQuery> accountQuerys = accountQueryDao.findByBank(accountQuery.getBank());
            
            if(accountQuerys != null && !accountQuerys.isEmpty() ){
                JsfUtil.addErrorMessage(String.format("An account has been configured for this bank %s", accountQuery.getBank().getName()));
                return "create";
            }
            
            List<AccountQueryM> queries = accountQueryMDao.findByBank(accountQuery.getBank());
            
            if(queries != null && !queries.isEmpty() ){
                
                AccountQueryM queryM = queries.stream().filter(x-> x.getStatus() == Status.PENDING).findFirst().orElse(null);
                
                if(queryM != null){
                    JsfUtil.addErrorMessage(String.format("An pending approval has been configured for this bank", accountQuery.getBank().getName()));
                    return "create";
                }
            }

            
            queries = accountQueryMDao.find("accountNo",  accountQuery.getAccountNo());
            
            if(queries != null && !queries.isEmpty()){
                
                AccountQueryM queryM = queries.stream().filter(x-> x.getStatus() == Status.PENDING).findFirst().orElse(null);
                
                if(queryM != null){
                    JsfUtil.addErrorMessage(String.format("An pending approval has been configured for this bank", accountQuery.getBank().getName()));
                    return "create";
                }
            }
            
            AccountQueryM accountM = new AccountQueryM();
            accountM.setCreatedOn(new Date());
            accountM.setCreatedBy(user);
            accountM.setAccountNo(accountQuery.getAccountNo());
            accountM.setBank(accountQuery.getBank());
            accountM.setStatus(Status.PENDING);

            accountQueryMDao.create(accountM);

            Log log = new Log();
            log.setAction("Create accountQuery: " + accountQuery.getAccountNo());
            log.setCreatedOn(new Date());
            log.setDescription("accountQuery has been created and submitted for approval successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

            JsfUtil.addSuccessMessage(String.format("AccountQuery %s has been added successfully waiting for approval", accountQuery.getAccountNo()));
            
            accountQuery = new AccountQuery();
            
            recreate();
            
            return "/cardstatus/list";
            
        } catch (DatabaseException de) {

            try {
                Log log = new Log();
                log.setAction("create accountQuery " + accountQuery.getAccountNo());
                log.setCreatedOn(new Date());
                log.setDescription("creation of accountQuery");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (de != null) {

                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception ex) {
            }

            if (de != null) {
                de.printStackTrace();
                JsfUtil.addErrorMessage(de.getMessage());
            }
        }
        
        return "create";
    }
    
    public PaginationHelper getPaginationHelper() {

        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public long getItemsCount() {
                    return accountQueryDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(accountQueryDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(AccountQueryController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }

    public PaginationHelper getPaginationHelperUnauth() {

        if (paginationHelper == null) {
            paginationHelper = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return accountQueryMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(accountQueryMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(AccountQueryController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationHelper;
    }
    
    public void recreate() {
        pagination = null;
        paginationHelper = null;
    }
    
    public String prepareCreate() {

        accountQuery = new AccountQuery();

        return "create";
    }
    
    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("accountupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("accountupdated", false);
        }

        if (pagination == null || pagination.getItemsCount() <= 0) {
            allAccounts = getPaginationHelper().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Accounts");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Accounts");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

        logService.log(log);

        return allAccounts;
    }

    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("accountupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationHelper = null;
            session.setAttribute("accountupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch Unauthorized Accounts");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching Unauthorized Accounts");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

        logService.log(log);

        if (paginationHelper == null) {
            allUnauthAccounts = getPaginationHelperUnauth().createPageDataModel();
        }

        return allUnauthAccounts;
    }
    
    /**
     *
     * @param data
     * @return
     */
    public boolean findUnauthorizedAccount(String data) {

        try {
            List<AccountQueryM> accounts = accountQueryMDao.find("accountNo",data);

            if (accounts != null && !accounts.isEmpty()) {

                AccountQueryM cM = accounts.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {
                    return true;
                }
            }

        } catch (Exception exception) {
            Logger.getLogger(AccountQueryController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return false;
    }
    
    public DataModel getAccountStatus(){
        
        List<AccountQueryTransaction> queryTransactions = accountQueryTransactionDao.findRecent();
        
        DataModel dataModel = new ListDataModel(queryTransactions);
                
        return dataModel;
    }
}

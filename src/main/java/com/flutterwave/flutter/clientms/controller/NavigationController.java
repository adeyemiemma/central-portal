/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "navigationController")
@RequestScoped
public class NavigationController {
    
//    private Global global;
    
    public String getHomePage(){
                
        Session session = SecurityUtils.getSubject().getSession();
        
        String domain = session.getAttribute("domainName")+"";
        
//        System.out.println("Domain is "+domain);
        
        if("bank".equalsIgnoreCase(domain))
            return "recon/index";
        
        if("bank_airtime".equalsIgnoreCase(domain))
            return "home";
        
        if("bank_chargeback".equalsIgnoreCase(domain))
            return "homepage";
        
        return "index";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model.recon;

import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public class SummaryReportModel {

    /**
     * @return the amountAt
     */
    public double getAmountAt() {
        return amountAt;
    }

    /**
     * @param amountAt the amountAt to set
     */
    public void setAmountAt(double amountAt) {
        this.amountAt = amountAt;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the monthTotal
     */
    public double getMonthTotal() {
        return monthTotal;
    }

    /**
     * @param monthTotal the monthTotal to set
     */
    public void setMonthTotal(double monthTotal) {
        this.monthTotal = monthTotal;
    }

    /**
     * @return the settledAt
     */
    public double getSettledAt() {
        return settledAt;
    }

    /**
     * @param settledAt the settledAt to set
     */
    public void setSettledAt(double settledAt) {
        this.settledAt = settledAt;
    }

    /**
     * @return the monthSettled
     */
    public double getMonthSettled() {
        return monthSettled;
    }

    /**
     * @param monthSettled the monthSettled to set
     */
    public void setMonthSettled(double monthSettled) {
        this.monthSettled = monthSettled;
    }
    
    private double amountAt;
    private Date date;
    private double total;
    private String currency;
    private double monthTotal;
    private double settledAt;
    private double monthSettled;
}

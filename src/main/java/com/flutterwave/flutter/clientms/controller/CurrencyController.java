/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;


import com.flutterwave.flutter.clientms.dao.CurrencyDao;
import com.flutterwave.flutter.clientms.dao.ExchangeRateDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.CurrencyMDao;
import com.flutterwave.flutter.clientms.dao.maker.ExchangeRateMDao;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.CurrencyM;
import com.flutterwave.flutter.clientms.model.maker.CurrencyM;
import com.flutterwave.flutter.clientms.model.maker.CurrencyM;
import com.flutterwave.flutter.clientms.model.maker.ExchangeRateM;
import com.flutterwave.flutter.clientms.model.maker.ProductM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.viewmodel.UploadModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import javax.transaction.Transactional;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author adeyemi
 */
@Named(value = "currencyController")
@SessionScoped
public class CurrencyController implements Serializable {

    private Currency currency;

    private DataModel allCurrencies = null;
    private DataModel allUnauthCurrencies = null;
    
    private DataModel allUnauthRates = null;
    private DataModel allRates = null;
    
    private static final Logger LOGGER = Logger.getLogger(CurrencyController.class.getName());

    @EJB
    private CurrencyDao currencyDao;
    @EJB
    private CurrencyMDao currencyMDao;
    @EJB
    private LogService logService;
    @EJB
    private UserDao userDao;
    @EJB
    private ExchangeRateDao exchangeRateDao;
    @EJB
    private ExchangeRateMDao exchangeRateMDao;
    
    
    private UploadModel uploadModel;
    
    private ExchangeRate exchangeRate;
    
    
    public ExchangeRate getExchangeRate(){
        
        if(exchangeRate == null)
            exchangeRate = new ExchangeRate();
        
        return exchangeRate;
    }
    
    public void setExchangeRate(ExchangeRate exchangeRate){
        
        this.exchangeRate = exchangeRate;
    }
    
    public UploadModel getUploadModel(){
        
        if(uploadModel == null)
            uploadModel = new UploadModel();
        
        return uploadModel;
    }
    
    public void setUploadModel(UploadModel uploadModel){
        this.uploadModel = uploadModel;
    }
    
    private PaginationHelper pagination, paginationHelper;
    
    //for rates
    private PaginationHelper ratePagination, ratePaginationHelper;

    public CurrencyController() {
    }

    public Currency getSelected() {
        if (currency == null) {
            currency = new Currency();
        }
        return currency;
    }

    public void setSelected(Currency currency) {

        this.currency = currency;
    }

    public String prepareCreate() {

        currency = new Currency();

        return "create";
    }
    
    public String prepateUpdate(){
        
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String param = params.get("editParam")+"";
        
        try {
            currency = currencyDao.find(Long.parseLong(param));
            
            Log log = new Log();
            log.setAction("Update Currency "+currency.getName());
            log.setCreatedOn(new Date());
            log.setDescription("about to update currency");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");

            logService.log(log);
            
        } catch (DatabaseException de) {
            Logger.getLogger(CurrencyController.class.getName()).log(Level.SEVERE, null, de);
            
            try{
                Log log = new Log();
                log.setAction("update currency "+currency.getName());
                log.setCreatedOn(new Date());
                log.setDescription("update currency");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
                
                if(de != null){
//                    log.setStatusMessage(de.getMessage());
                    
                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                }
                else
                    logService.log(log);
            }catch(Exception ex){
            }
        }
        
        return "edit";
    }
    
    public String uploadCurrency() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            List<CurrencyM> list = new ArrayList<>();

            if (fileName.toLowerCase().endsWith("csv")) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(uploadModel.getFile().getInputStream()));

                String line = null;

                Object userString = SecurityUtils.getSubject().getPrincipal();
                User user = userDao.findByKey("email", userString + "");

                int countUpload = 0, countDuplicate = 0;

                Map<String, String> map = new HashMap<>();

                int i = 0;
                while ((line = reader.readLine()) != null) {

                    if(i++ == 0)
                        continue;
                    
                    final String[] data = line.split(",");

                    if (data.length < 2) {
                        continue;
                    }
                    
                    String name = data[0].replaceAll("\"", "");
                    String shortName = data[1].replaceAll("\"", "");
                    
                    if (currencyDao.find("name", name) != null || currencyDao.find("shortName", shortName) != null) {
                        countDuplicate++;
                        continue;
                    }
                    
                    if (currencyMDao.find("name", name) != null || currencyMDao.find("shortName", shortName) != null) {
                        countDuplicate++;
                        continue;
                    }

                    boolean result = map.entrySet().stream().anyMatch(x -> x.getKey().equalsIgnoreCase(data[0]) || x.getValue().equalsIgnoreCase(data[1]));

                    if (result == true) {
                        countDuplicate++;
                        continue;
                    }

                    map.put(data[0], data[1]);

                    CurrencyM cnty = new CurrencyM();
                    cnty.setEnabled(true);
                    cnty.setName(data[0].replaceAll("\"", ""));
                    cnty.setShortName(data[1].replaceAll("\"", ""));
                    cnty.setCreatedBy(user);
                    cnty.setCreatedOn(new Date());
                    cnty.setStatus(Status.PENDING);
                    
                    countUpload++;
                    list.add(cnty);
                }

                if(! list.isEmpty())
                    currencyMDao.create(list);

                JsfUtil.addSuccessMessage(countUpload+" currency record(s) has been upload and submitted for approval, ("+ countDuplicate +") Duplicates found");
                
            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
            
            JsfUtil.addErrorMessage("unable to upload record please try again later");
        }

        return "/currency/upload";
    }
    
    public String update(){
        
        
        try {
            
            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString+"");
            
            Currency curr = currencyDao.findByKey("shortName", currency.getShortName());
            
            if(curr != null && curr.getId() != currency.getId()){
                
                JsfUtil.addErrorMessage("Short name exists");
                
                return "edit";
            }
            
            List<CurrencyM> list = currencyMDao.find("modelId", currency.getId());
            
            if(list != null && !list.isEmpty()){
                
                CurrencyM cm = list.stream().filter(x->x.getApprovedOn() == null).findFirst().orElse(null);
                
                if(cm != null){
                    JsfUtil.addErrorMessage("Pending update exist on currency " + cm.getName() + "");
                    
                    return "edit";
                }
            }
            
            CurrencyM currencyM = new CurrencyM();
            currencyM.setModelId(currency.getId());
            currencyM.setCreatedOn(new Date());
            currencyM.setEnabled(currency.isEnabled());
            currencyM.setName(currency.getName());
            currencyM.setShortName(currency.getShortName());
            currencyM.setCreatedBy(user);
            
            currencyM = currencyMDao.create(currencyM);
            
                        
            Log log = new Log();
            log.setAction("Update Currency "+currency.getName());
            log.setCreatedOn(new Date());
            log.setDescription("Currency updated has been created and successfully updated");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");

            logService.log(log);
            
            if(currencyM == null){
                throw new DatabaseException("Unable to update record");
            }
            
            JsfUtil.addSuccessMessage("Current has been updated succesfully waiting for authorization");
            
            paginationHelper = null;
            
            return "edit";
            
        } catch (DatabaseException ex) {
            Logger.getLogger(CurrencyController.class.getName()).log(Level.SEVERE, null, ex);
            
            if(ex != null)
                JsfUtil.addErrorMessage(""+ ex.getMessage());
            else
                JsfUtil.addErrorMessage("Unable to update record , please try again later");
            
            try{
                Log log = new Log();
                log.setAction("update currency "+currency.getName());
                log.setCreatedOn(new Date());
                log.setDescription("update currency");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
                
                if(ex != null){
//                    log.setStatusMessage(ex.getMessage());
                    
                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                }
                else
                    logService.log(log);
            }catch(Exception e){
            }
        }
        
        return "edit";
    }

    @Transactional
    public String create() {

        if (currency != null) {
            currency.setCreatedOn(new Date());
        }

        try {
            
            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString+"");
            
            Currency c = currencyDao.findByKey("name", currency.getName());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create currency: " + currency.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate currency with name " + c.getName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Currency with name %s exists", currency.getName()));

                return "create";
            }

            c = currencyDao.findByKey("shortName", currency.getShortName());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create currency: " + currency.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate currency with short name " + c.getShortName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Currency with short name %s exists", currency.getShortName()));

                return "create";
            }

//            List<CurrencyM> countries = currencyMDao.find("name", currency.getName());
            boolean status = findUnauthorizedCurrency(currency.getName(), true);

            if (status == true) {

                Log log = new Log();
                log.setAction("Create currency: " + currency.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Pending currency with name " + currency.getShortName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Pending approval on Currency with  name %s exists", currency.getShortName()));

                return "create";
            }

            status = findUnauthorizedCurrency(currency.getShortName(), false);

            if (status == true) {

                Log log = new Log();
                log.setAction("Create currency: " + currency.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Pending currency with name " + currency.getShortName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Pending approval on Currency with  name %s exists", currency.getShortName()));

                return "create";
            }
            
            CurrencyM currencyM  = new CurrencyM();
            currencyM.setCreatedOn(new Date());
            currencyM.setCreatedBy(user);
            currencyM.setEnabled(true);
            currencyM.setName(currency.getName());
            currencyM.setShortName(currency.getShortName());
            currencyM.setStatus(Status.PENDING);
            
            currencyMDao.create(currencyM);
            
            Log log = new Log();
            log.setAction("Create currency: "+currency.getName());
            log.setCreatedOn(new Date());
            log.setDescription("currency has been created and submitted for approval successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
            
            logService.log(log);
            
            JsfUtil.addSuccessMessage(String.format("Currency %s (%s) has been added successfully waiting for approval", currency.getName(), currency.getShortName()));
            
            currency = new Currency();
        } catch (DatabaseException de) {

            try{
                Log log = new Log();
                log.setAction("create currency "+currency.getName());
                log.setCreatedOn(new Date());
                log.setDescription("creation of currency");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
                
                if(de != null){
                    
                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                }
                else
                    logService.log(log);
            }catch(Exception ex){
            }
            
            if (de != null) {
                de.printStackTrace();
                JsfUtil.addErrorMessage(de.getMessage());
            }
        }

        recreate();
        
        return prepareCreate();
    }

    /**
     *
     * @param data
     * @param isName - used to control if it is name or shortname
     * @return
     */
    public boolean findUnauthorizedCurrency(String data, boolean isName) {

        try {
            List<CurrencyM> countries = currencyMDao.find(isName == true ? "name" : "shortName", isName == true ? currency.getName() : currency.getShortName());

            if (countries != null && !countries.isEmpty()) {

                CurrencyM cM = countries.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {
                    return true;
                }
            }

        } catch (Exception exception) {
            Logger.getLogger(CurrencyController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return false;
    }
    public Currency getCurrency(long id) {

        try {
            return currencyDao.find(id);
        } catch (DatabaseException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public PaginationHelper getPaginationHelper() {
        
        if (pagination == null) {
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return currencyDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(currencyDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(CurrencyController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }
    
    public PaginationHelper getPaginationHelperUnauth() {

        if (paginationHelper == null) {
            paginationHelper = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return currencyMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(currencyMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(CurrencyController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationHelper;
    }
    
    public void recreate(){
        pagination = null;
        paginationHelper = null;
    }
    
    public List<Currency> getAllCurrencies() throws DatabaseException{
        
        return currencyDao.findAll().stream().filter(x -> x.isEnabled()).collect(Collectors.toList());
    }
    
    public DataModel getAll(){
        
        Session session = SecurityUtils.getSubject().getSession();
        
        Object obj = session.getAttribute("currencyupdated");
        
        boolean status = obj == null ? false : Boolean.valueOf(obj+"");
        
        if(status == true){
            pagination = null;
            session.setAttribute("currencyupdated", false);
        }
        
        if(pagination == null || pagination.getItemsCount() <= 0){
            allCurrencies = getPaginationHelper().createPageDataModel();
        }
        
        Log log = new Log();
        log.setAction("Fetch All Currencies");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Currencies");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");

        logService.log(log);
        
        
        return allCurrencies;
    }
    
    public DataModel getUnauth(){
        
        Session session = SecurityUtils.getSubject().getSession();
        
        Object obj = session.getAttribute("currencyupdatedm");
        
        boolean status = obj == null ? false : Boolean.valueOf(obj+"");
        
        if(status == true){
            paginationHelper = null;
            session.setAttribute("currencyupdatedm", false);
        }
        
        Log log = new Log();
        log.setAction("Fetch Unauthorized Currencies");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching Unauthorized Currencies");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");

        logService.log(log);
        
        if(paginationHelper == null){
            allUnauthCurrencies = getPaginationHelperUnauth().createPageDataModel();
        }
        
        return allUnauthCurrencies;
    }
       
    public void prepareCreateExchangeRate(){
        
        this.exchangeRate = new ExchangeRate();
    }
    
    public PaginationHelper getPaginationHelperRate() {
        
        if (ratePagination == null) {
            ratePagination = new PaginationHelper(30) {
                @Override
                public long getItemsCount() {
                    return currencyDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(exchangeRateDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(CurrencyController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return ratePagination;
    }
    
    public PaginationHelper getPaginationHelperUnauthRate() {

        if (ratePaginationHelper == null) {
            ratePaginationHelper = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return exchangeRateMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(exchangeRateMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(CurrencyController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return ratePaginationHelper;
    }
    
    public String createExchangeRate(){
        
        if (exchangeRate != null) {
            exchangeRate.setCreatedOn(new Date());
        }
        
        try{
            
            ExchangeRate rate = exchangeRateDao.find(exchangeRate.getOriginCurrency(), exchangeRate.getDestinationCurrency());
            
            if(rate != null){
                
                Log log = new Log();
                log.setAction("Create exchange rate: ");
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate exhange rate with name ");
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Exchange rate from %s to %s exists", exchangeRate.getOriginCurrency().getName(), 
                        exchangeRate.getDestinationCurrency().getName()));

                return "/currency/rate/create";
            }
            
            List<ExchangeRateM> exchangeRateM = exchangeRateMDao.find(exchangeRate.getOriginCurrency(), exchangeRate.getDestinationCurrency());
            
            if(exchangeRateM != null ){
                
                ExchangeRateM rateM  = exchangeRateM.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);
                
                if(rateM != null){
                Log log = new Log();
                log.setAction("Create Exchange Rate: " + exchangeRate.getId());
                log.setCreatedOn(new Date());
                log.setDescription("Pending Authorization exchange rate with exists ");
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Pending Authorization Exchange rate from %s to %s exists", exchangeRate.getOriginCurrency().getName(), 
                        exchangeRate.getDestinationCurrency().getName()));

                return "/currency/rate/create";
                }
                     
            }
            
            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("username", userString + "");
            ExchangeRateM rateM = new ExchangeRateM();
            rateM.setCreatedBy(user);
            rateM.setCreatedOn(new Date());
            rateM.setDestinationCurrency(exchangeRate.getDestinationCurrency());
            rateM.setOriginCurrency(exchangeRate.getOriginCurrency());
            rateM.setStatus(Status.PENDING);
            rateM.setAmount(exchangeRate.getAmount());
            
            exchangeRateMDao.create(rateM);
            
//            Log log = new Log();
//            log.setAction("Create currency rate: ");
//            log.setCreatedOn(new Date());
//            log.setDescription("currency has been created and submitted for approval successfully");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
//            
//            logService.log(log);
            
            JsfUtil.addSuccessMessage(String.format("Exchange rate from %s to %s has been created successfully", exchangeRate.getOriginCurrency().getName(),
                    exchangeRate.getDestinationCurrency().getName()));
            
            prepareCreateExchangeRate();
            
            allUnauthRates = null;
            allRates = null;
            
            return "/currency/rate/list";
            
        }catch(Exception de){
        
            try{
                Log log = new Log();
                log.setAction("create exchange rate ");
                log.setCreatedOn(new Date());
                log.setDescription("create exchange rate ");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
                
                if(de != null){
                    
                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                }
                else
                    logService.log(log);
            }catch(Exception ex){
            }
            
            if (de != null) {
                de.printStackTrace();
                JsfUtil.addErrorMessage(de.getMessage());
            }
        }
        
        return "/currency/rate/create";
    }
    
    public DataModel getAllExchangeRate(){
        
        Session session = SecurityUtils.getSubject().getSession();
        
        Object obj = session.getAttribute("exchangeupdated");
        
        boolean status = obj == null ? false : Boolean.valueOf(obj+"");
        
        if(status == true){
            ratePagination = null;
            session.setAttribute("exchangeupdated", false);
        }
        
        if(ratePagination == null || ratePagination.getItemsCount() <= 0){
            allRates = getPaginationHelperRate().createPageDataModel();
        }
        
//        Log log = new Log();
//        log.setAction("Fetch All Rates");
//        log.setCreatedOn(new Date());
//        log.setDescription("Fetching All Rates");
//        log.setLevel(LogLevel.Info);
//        log.setStatus(LogStatus.SUCCESSFUL);
//        log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
//
//        logService.log(log);
        
        
        return allRates;
    }
    
    public DataModel getUnauthExchangeRate(){
        
        Session session = SecurityUtils.getSubject().getSession();
        
        Object obj = session.getAttribute("exchangeupdatedm");
        
        boolean status = obj == null ? false : Boolean.valueOf(obj+"");
        
        if(status == true){
            ratePaginationHelper = null;
            session.setAttribute("exchangeupdatedm", false);
        }
        
        Log log = new Log();
        log.setAction("Fetch Unauthorized Exchange rates");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching Unauthorized Exchange rates");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");

        logService.log(log);
        
        if(ratePaginationHelper == null){
            allUnauthRates = getPaginationHelperUnauthRate().createPageDataModel();
        }
        
        return allUnauthRates;
    }
    
    public String prepareExchangeRateUpdate(long id){
        
        try {
            exchangeRate = exchangeRateDao.find(id);
            
            Log log = new Log();
            log.setAction("Update Exchange Rate "+exchangeRate.getId());
            log.setCreatedOn(new Date());
            log.setDescription("about to update rate");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");

            logService.log(log);
            
        } catch (DatabaseException de) {
            Logger.getLogger(CurrencyController.class.getName()).log(Level.SEVERE, null, de);
            
            try{
                Log log = new Log();
                log.setAction("update rate "+exchangeRate.getId());
                log.setCreatedOn(new Date());
                log.setDescription("update rate");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
                
                if(de != null){
//                    log.setStatusMessage(de.getMessage());
                    
                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                }
                else
                    logService.log(log);
            }catch(Exception ex){
            }
        }
        
        return "/currency/rate/edit";
    }
    
    public String updateExchangeRate(){
        
        try{
            
            ExchangeRate rate = exchangeRateDao.find(exchangeRate.getOriginCurrency(), exchangeRate.getDestinationCurrency());
            
            if(rate != null && rate.getId() != exchangeRate.getId()){
                
                Log log = new Log();
                log.setAction("Update exchange rate id: " + exchangeRate.getId());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate exhange rate with name ");
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Exchange rate from %s to %s exists", exchangeRate.getOriginCurrency().getName(), 
                        exchangeRate.getDestinationCurrency().getName()));

                return "/currency/rate/edit";
            }
            
            List<ExchangeRateM> exchangeRateM = exchangeRateMDao.find(exchangeRate.getOriginCurrency(), exchangeRate.getDestinationCurrency());
            
            if(exchangeRateM != null ){
                
                ExchangeRateM rateM  = exchangeRateM.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);
                
                if(rateM != null){
                    Log log = new Log();
                    log.setAction("Update Exchange Rate ID: " + exchangeRate.getId());
                    log.setCreatedOn(new Date());
                    log.setDescription("Pending Authorization exchange rate with exists ");
                    log.setLevel(LogLevel.Info);
                    log.setStatus(LogStatus.SUCCESSFUL);
                    log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                    JsfUtil.addErrorMessage(String.format("Pending Authorization Exchange rate from %s to %s exists", exchangeRate.getOriginCurrency().getName(), 
                            exchangeRate.getDestinationCurrency().getName()));

                    return "/currency/rate/edit";
                }
                     
            }
            
            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");
            ExchangeRateM rateM = new ExchangeRateM();
            rateM.setCreatedBy(user);
            rateM.setCreatedOn(new Date());
            rateM.setDestinationCurrency(exchangeRate.getDestinationCurrency());
            rateM.setOriginCurrency(exchangeRate.getOriginCurrency());
            rateM.setStatus(Status.PENDING);
            rateM.setModelId(exchangeRate.getId());
            rateM.setAmount(exchangeRate.getAmount());
            
            exchangeRateMDao.create(rateM);
            
            Log log = new Log();
            log.setAction("update currency exchange rate: "+exchangeRate.getId());
            log.setCreatedOn(new Date());
            log.setDescription("currency exchange rate has been updated and submitted for approval successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
            
            logService.log(log);
            
            JsfUtil.addSuccessMessage(String.format("Exchange rate from %s to %s has been updated successfully and sent for approval", exchangeRate.getOriginCurrency().getName(),
                    exchangeRate.getDestinationCurrency().getName()));
            
            ratePaginationHelper = null;
            
            prepareCreateExchangeRate();
            
            return "/currency/rate/list";
            
        }catch(Exception de){
        
            try{
                Log log = new Log();
                log.setAction("create exchange rate ");
                log.setCreatedOn(new Date());
                log.setDescription("create exchange rate ");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal()+"");
                
                if(de != null){
                    
                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                }
                else
                    logService.log(log);
            }catch(Exception ex){
            }
            
            if (de != null) {
                de.printStackTrace();
                JsfUtil.addErrorMessage(de.getMessage());
            }
        }
        
        return "/currency/rate/edit";
    }
    
    @FacesConverter(value = "currencyConverter")
    public static class CurrencyConverter implements javax.faces.convert.Converter {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {

            if (value == null || value.length() == 0) {
                return null;
            }

            CurrencyController currencyController = (CurrencyController) context.getApplication()
                    .getELResolver().getValue(context.getELContext(), null, "currencyController");

            return currencyController.getCurrency(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            if (value == null) {
                return null;
            }
            if (value instanceof Currency) {
                Currency o = (Currency) value;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + value + " is of type " + value.getClass().getName() + "; expected type: " + Currency.class.getName());
            }
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.RewardSettingDao;
import com.flutterwave.flutter.clientms.dao.RewardSettingLogDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.model.RewardSetting;
import com.flutterwave.flutter.clientms.model.RewardSettingLog;
import com.flutterwave.flutter.clientms.util.EnumUtil;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "rewardController")
@SessionScoped
public class RewardController implements Serializable{
    
    @EJB
    private RewardSettingDao rewardSettingDao;
    @EJB
    private RewardSettingLogDao rewardSettingLogDao;
    @EJB
    private UserDao userDao;
    
    /**
     * @return the rewardSetting
     */
    public RewardSetting getRewardSetting() {
        
        if(rewardSetting == null)
            rewardSetting = new RewardSetting();
        
        return rewardSetting;
    }

    /**
     * @param rewardSetting the rewardSetting to set
     */
    public void setRewardSetting(RewardSetting rewardSetting) {
        this.rewardSetting = rewardSetting;
    }
     
    private RewardSetting rewardSetting;
    
    public void setUpSetting(){
        
        rewardSetting = new RewardSetting();
    }
      
    public String createMerchantSetting(){
        
        
        try {
            List<RewardSetting> rs = rewardSettingDao.find(rewardSetting.getMerchantId(), rewardSetting.getType(),
                    rewardSetting.getWeekDay(), rewardSetting.getStatus(), rewardSetting.getCurrency());
            
            if(rs != null && rs.size() > 0){
                
                JsfUtil.addErrorMessage("Reward with settings exist for merchant");
                return "";
            }
            
            rewardSetting.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            rewardSetting.setCreatedOn(new Date());
            rewardSetting.setUnit(1);
            
            RewardSettingLog rewardSettingLog = RewardSettingLog.from(rewardSetting);
            
            rewardSettingLogDao.create(rewardSettingLog);
            
            rewardSettingDao.create(rewardSetting);
            
            JsfUtil.addSuccessMessage("Setting has been created successfully");
            
            rewardSetting = new RewardSetting();
            
            return "";
        } catch (DatabaseException ex) {
            Logger.getLogger(RewardController.class.getName()).error(null, ex);
        }
        
        JsfUtil.addErrorMessage("Unable to add setting for merchant");
        return "";
    }
    
    public String prepareSetting(long id) throws DatabaseException{
        
        rewardSetting = rewardSettingDao.find(id);
        
        if(rewardSetting == null){
            
            JsfUtil.addErrorMessage("Reward not found");
            
            return "/reward/merchant";
        }   
        
        return "/reward/edit";
    }
    
    public String updateSetting(){
        
        try {
            List<RewardSetting> rs = rewardSettingDao.find(rewardSetting.getMerchantId(), rewardSetting.getType(),
                    rewardSetting.getWeekDay(), rewardSetting.getStatus(), rewardSetting.getCurrency());
            
            if(rs != null && !rs.isEmpty()){
                
                RewardSetting setting = rs.get(0);
                
                if(setting.getId() != rewardSetting.getId()){
                    
                    JsfUtil.addErrorMessage("Reward with settings exist for merchant");
                    return "";
                }
            }
            
            rewardSetting.setModifiedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            rewardSetting.setModifiedOn(new Date());
            rewardSetting.setUnit(1);
            
            RewardSettingLog rewardSettingLog = RewardSettingLog.from(rewardSetting);
            rewardSettingLog.setCreatedOn(new Date());
            rewardSettingLog.setCreatedBy(rewardSetting.getModifiedBy());
            rewardSettingLogDao.create(rewardSettingLog);
            
            rewardSettingDao.update(rewardSetting);
            
            JsfUtil.addSuccessMessage("Setting has been updated successfully");
            
            rewardSetting = new RewardSetting();
            
            return "/reward/merchant";
        } catch (DatabaseException ex) {
            Logger.getLogger(RewardController.class.getName()).error(null, ex);
        }
        
        JsfUtil.addErrorMessage("Unable to update setting for merchant");
        return "";
        
    }
     
    public EnumUtil.Status[] getAllStatus(){
        
        return EnumUtil.Status.values();
    }
    
    public EnumUtil.TransactionType[] getAllTypes(){
        
        return EnumUtil.TransactionType.values();
    }
    
    public EnumUtil.WeekDay[] getWeekDays(){
        
        return EnumUtil.WeekDay.values();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.util.Utility;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.stream.Stream;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author emmanueladeyemi
 */
public class MerchantComplianceRequest {

    /**
     * @return the sendEmail
     */
    public int getSendEmail() {
        return sendEmail;
    }

    /**
     * @param sendEmail the sendEmail to set
     */
    public void setSendEmail(int sendEmail) {
        this.sendEmail = sendEmail;
    }

    /**
     * @return the useOfProduct
     */
    public String getUseOfProduct() {
        return useOfProduct;
    }

    /**
     * @param useOfProduct the useOfProduct to set
     */
    public void setUseOfProduct(String useOfProduct) {
        this.useOfProduct = useOfProduct;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the categoryDataSet
     */
    public Map<String, String> getCategoryDataSet() {
        return categoryDataSet;
    }

    /**
     * @param categoryDataSet the categoryDataSet to set
     */
    public void setCategoryDataSet(Map<String, String> categoryDataSet) {
        this.categoryDataSet = categoryDataSet;
    }

    /**
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the bvn
     */
    public String getBvn() {
        return bvn;
    }

    /**
     * @param bvn the bvn to set
     */
    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    /**
     * @return the scumlCertificatePath
     */
    public String getScumlCertificatePath() {
        return scumlCertificatePath;
    }

    /**
     * @param scumlCertificatePath the scumlCertificatePath to set
     */
    public void setScumlCertificatePath(String scumlCertificatePath) {
        this.scumlCertificatePath = scumlCertificatePath;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the registeredName
     */
    public String getRegisteredName() {
        return registeredName;
    }

    /**
     * @param registeredName the registeredName to set
     */
    public void setRegisteredName(String registeredName) {
        this.registeredName = registeredName;
    }

    /**
     * @return the registeredNumber
     */
    public String getRegisteredNumber() {
        return registeredNumber;
    }

    /**
     * @param registeredNumber the registeredNumber to set
     */
    public void setRegisteredNumber(String registeredNumber) {
        this.registeredNumber = registeredNumber;
    }

    /**
     * @return the dateofIncorporation
     */
    public String getDateofIncorporation() {
        return dateofIncorporation;
    }

    /**
     * @param dateofIncorporation the dateofIncorporation to set
     */
    public void setDateofIncorporation(String dateofIncorporation) {
        this.dateofIncorporation = dateofIncorporation;
    }

    /**
     * @return the tradingName
     */
    public String getTradingName() {
        return tradingName;
    }

    /**
     * @param tradingName the tradingName to set
     */
    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    /**
     * @return the registeredAddress
     */
    public String getRegisteredAddress() {
        return registeredAddress;
    }

    /**
     * @param registeredAddress the registeredAddress to set
     */
    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    /**
     * @return the operationalAddress
     */
    public String getOperationalAddress() {
        return operationalAddress;
    }

    /**
     * @param operationalAddress the operationalAddress to set
     */
    public void setOperationalAddress(String operationalAddress) {
        this.operationalAddress = operationalAddress;
    }

    /**
     * @return the companyType
     */
    public String getCompanyType() {
        return companyType;
    }

    /**
     * @param companyType the companyType to set
     */
    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    /**
     * @return the companyRegDocumentPath
     */
    public String getCompanyRegDocumentPath() {
        return companyRegDocumentPath;
    }

    /**
     * @param companyRegDocumentPath the companyRegDocumentPath to set
     */
    public void setCompanyRegDocumentPath(String companyRegDocumentPath) {
        this.companyRegDocumentPath = companyRegDocumentPath;
    }

    /**
     * @return the directorIdPath
     */
    public String getDirectorIdPath() {
        return directorIdPath;
    }

    /**
     * @param directorIdPath the directorIdPath to set
     */
    public void setDirectorIdPath(String directorIdPath) {
        this.directorIdPath = directorIdPath;
    }

    /**
     * @return the contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * @param contactName the contactName to set
     */
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    /**
     * @return the contactPhone
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * @param contactPhone the contactPhone to set
     */
    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    /**
     * @return the contactAddress
     */
    public String getContactAddress() {
        return contactAddress;
    }

    /**
     * @param contactAddress the contactAddress to set
     */
    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    /**
     * @return the contactEmail
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * @param contactEmail the contactEmail to set
     */
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     * @return the industry
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * @param industry the industry to set
     */
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    /**
     * @return the products
     */
    public String[] getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(String[] products) {
        this.products = products;
    }

    /**
     * @return the productWebUrl
     */
    public String getProductWebUrl() {
        return productWebUrl;
    }

    /**
     * @param productWebUrl the productWebUrl to set
     */
    public void setProductWebUrl(String productWebUrl) {
        this.productWebUrl = productWebUrl;
    }

    /**
     * @return the operatingLicencePath
     */
    public String getOperatingLicencePath() {
        return operatingLicencePath;
    }

    /**
     * @param operatingLicencePath the operatingLicencePath to set
     */
    public void setOperatingLicencePath(String operatingLicencePath) {
        this.operatingLicencePath = operatingLicencePath;
    }

    /**
     * @return the amlPolicyPath
     */
    public String getAmlPolicyPath() {
        return amlPolicyPath;
    }

    /**
     * @param amlPolicyPath the amlPolicyPath to set
     */
    public void setAmlPolicyPath(String amlPolicyPath) {
        this.amlPolicyPath = amlPolicyPath;
    }

    /**
     * @return the shareHolder1
     */
    public String getShareHolder1() {
        return shareHolder1;
    }

    /**
     * @param shareHolder1 the shareHolder1 to set
     */
    public void setShareHolder1(String shareHolder1) {
        this.shareHolder1 = shareHolder1;
    }

    /**
     * @return the shareHolder2
     */
    public String getShareHolder2() {
        return shareHolder2;
    }

    /**
     * @param shareHolder2 the shareHolder2 to set
     */
    public void setShareHolder2(String shareHolder2) {
        this.shareHolder2 = shareHolder2;
    }
    
    @NotBlank(message = "Registration name must be provided")
    @JsonProperty(value = "registeredname")
    private String registeredName;
    @NotBlank(message = "Registration number must be provided")
    @JsonProperty(value = "registerednumber")
    private String registeredNumber;
    @NotBlank(message = "Date of incorporation must be provided")
    @JsonProperty(value = "dateofincorporation")
    private String dateofIncorporation;  
    @NotBlank(message = "Trading name must be provided")
    @JsonProperty(value = "tradingname")
    private String tradingName;
    @NotBlank(message = "Registered address must be provided")
    @JsonProperty(value = "registeredaddress")
    private String registeredAddress;
    @NotBlank(message = "Operational address must be provided")
    @JsonProperty(value = "operationaladdress")
    private String operationalAddress;
    @NotBlank(message = "Company type must be provided")
    @JsonProperty(value = "companytype")
    private String companyType;
//    @NotBlank(message = "Path to company's registration document must be provided")
    @JsonProperty(value = "companyregdocumentpath")
    private String companyRegDocumentPath;
//    @NotBlank(message = "Path to director's registration document must be provided")
    @JsonProperty(value = "directoridpath")
    private String directorIdPath;
    @NotBlank(message = "Contact name must be provided")
    @JsonProperty(value = "contactname", required = true)
    private String contactName;
    @JsonProperty(value = "contactphone")
    private String contactPhone;
    @NotBlank(message = "Contact Address must be provided")
    @JsonProperty(value = "contactaddress", required = true)
    private String contactAddress;
    @NotBlank(message = "Contact Email must be provided")
    @JsonProperty(value = "contactemail", required = true)
    private String contactEmail;
    @NotBlank(message = "Industry must be provided")
    private String industry;
    @Size(min = 1, message = "At least one product must be selected")
    private String[] products;
    @NotBlank(message = "Product web url must be provided")
    @JsonProperty(value = "productweburl", required = true)
    private String productWebUrl;
//    @NotBlank(message = "Path to operating licence document must be provided")
    @JsonProperty(value = "operatinglicencepath")
    private String operatingLicencePath;   
//    @NotBlank(message = "Path to aml policy path document must be provided")   
    @JsonProperty(value = "amlpolicypath")
    private String amlPolicyPath;
    @JsonProperty(value = "shareholder1")
    private String shareHolder1;
    @JsonProperty(value = "shareholder2")
    private String shareHolder2; 
//    @NotBlank(message = "hash must be provided")
    private String hash = "";
    @NotBlank(message = "merchant id must not be null")
    @JsonProperty(value = "merchantid")
    private String merchantId;
    @NotBlank(message = "Country must be provided")
    private String country;
    @JsonProperty(value = "scumlcertificatepath" )
    private String scumlCertificatePath;
//    @NotBlank(message = "BVN must be provided")
    private String bvn;
    private String description;
    @JsonProperty(value = "parentid" )
    private String parentId;
     @JsonProperty(value = "useofproduct" )
    private String useOfProduct;
    private String category;
    @JsonProperty(value = "categorydataset" )
    private Map<String, String> categoryDataSet;
    @JsonProperty(value = "sendemail" )
    private int sendEmail = 1;
    
    @Override
    public String toString() {
        
        StringBuilder builder = new StringBuilder();
        builder.append(registeredName).append(" ")
                .append(registeredNumber).append(" ")
                .append(dateofIncorporation).append(" ")
                .append(tradingName).append(" ")
                .append(registeredAddress).append(" ")
                .append(operationalAddress).append(" ")
                .append(companyType).append(" ")
                .append(companyRegDocumentPath).append(" ")
                .append(directorIdPath).append(" ")
                .append(contactName).append(" ")
                .append(contactPhone).append(" ")
                .append(contactAddress).append(" ")
                .append(contactEmail).append(" ")
                .append(getCountry()).append(" ")
                .append(industry).append(" ")
                .append(Stream.of(products).reduce("", (preview, current ) -> preview+=" "+current))
                .append(productWebUrl).append(" ")
                .append(operatingLicencePath).append(" ")
                .append(amlPolicyPath).append(" ")
                .append(shareHolder1).append(" ")
                .append(shareHolder2)
                .append(merchantId)
                .append(scumlCertificatePath)
                .append(getBvn())
                .append(Utility.nullToEmpty(getParentId()))
                .append( getDescription() == null ? null : Charset.forName("UTF-8").encode(getDescription()));
        
        return builder.toString();
    }
    
    public String toHashableString(){
        
        StringBuilder builder = new StringBuilder();
        builder.append(registeredName)
                .append(registeredNumber)
                .append(dateofIncorporation == null ? "" : dateofIncorporation)
                .append(tradingName == null ? "" : tradingName)
                .append(registeredAddress)
                .append(operationalAddress)
                .append(companyType)
                .append(companyRegDocumentPath == null ? "" : companyRegDocumentPath)
                .append(directorIdPath == null ? "" : directorIdPath )
                .append(contactName  == null ? "" : contactName)
                .append(contactPhone == null ? "" : contactPhone)
                .append(contactAddress == null ? "" : contactAddress)
                .append(contactEmail == null ? "" : contactEmail)
                .append(getCountry())
                .append(industry == null ? "" : industry)
                .append(Stream.of(products).reduce("", (preview, current ) -> preview+=""+current))
                .append(productWebUrl)
                .append(operatingLicencePath == null ? "" : operatingLicencePath)
                .append(amlPolicyPath == null ? "" : amlPolicyPath)
                .append(shareHolder1 == null ? "" : shareHolder1)
                .append(shareHolder2 == null ? "" : shareHolder2)
                .append(merchantId)
                .append(scumlCertificatePath != null ? scumlCertificatePath : "")
                .append(getBvn() == null ? "" : getBvn())
                .append(Utility.nullToEmpty(getParentId()));
        
        
        return builder.toString();
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.dao.ConfigurationFeeDao;
import com.flutterwave.flutter.clientms.dao.ExchangeRateDao;
import com.flutterwave.flutter.clientms.dao.ProductLimitDao;
import com.flutterwave.flutter.clientms.dao.ProviderDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.ConfigurationFeeMDao;
import com.flutterwave.flutter.clientms.dao.maker.ExchangeRateMDao;
import com.flutterwave.flutter.clientms.dao.maker.ProviderMDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.ConfigurationFee;
import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.model.ProductLimit;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.ConfigurationFeeM;
import com.flutterwave.flutter.clientms.model.maker.ExchangeRateM;
import com.flutterwave.flutter.clientms.model.maker.ProviderM;
import com.flutterwave.flutter.clientms.model.maker.UpdateModel;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Path("/config")
@RequestScoped
public class ConfigurationApiController {

    @EJB
    private ConfigurationFeeDao configurationFeeDao;
    @EJB
    private ConfigurationFeeMDao configurationFeeMDao;
    @EJB
    private ProviderMDao providerMDao;
    @EJB
    private ProviderDao providerDao;
    @EJB
    private LogService logService;
    @EJB
    private UserDao userDao;
    @EJB
    private ProductLimitDao productLimitDao;
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private ExchangeRateDao exchangeRateDao;
    @EJB
    private ExchangeRateMDao exchangeRateMDao;

    /**
     * This is called to authorize fee
     *
     * @param updateModel
     * @return
     */
    @Path("/fee/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeFee(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            ConfigurationFeeM feeM = configurationFeeMDao.find(id);

            if (feeM == null) {
                response.put("status-code", "03");
                response.put("status", "fee record not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Fee Authorization with id " + feeM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log, null);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (feeM.getModelId() <= 0 && status == true) {

                ConfigurationFee configurationFee = configurationFeeDao.findByProduct(feeM.getProduct());

                if (configurationFee != null) {

                    response.put("status-code", "02");
                    response.put("status", "Duplicate fee for product " + feeM.getProduct().getName());
                    return response;
                }

                configurationFee = new ConfigurationFee();

                configurationFee.setCreatedBy(feeM.getCreatedBy());
                configurationFee.setCreated(feeM.getCreatedOn());
                configurationFee.setProduct(feeM.getProduct());

                // This section is for international fee
                configurationFee.setInterAccountFee(feeM.getInterAccountFee());
                configurationFee.setInterAccountFeeExtra(feeM.getInterAccountFeeExtra());
                configurationFee.setInterAccountFeeCap(feeM.getInterAccountFeeCap());
                configurationFee.setInterAccountFeeType(feeM.getInterAccountFeeType());
                configurationFee.setInterCardFee(feeM.getInterCardFee());
                configurationFee.setInterCardFeeExtra(feeM.getInterCardFeeExtra());
                configurationFee.setInterCardFeeCap(feeM.getInterCardFeeCap());
                configurationFee.setInterCardFeeType(feeM.getInterCardFeeType());
                // end of setting intertional fee

                // This section is for pob fee
                configurationFee.setPobAccountFee(feeM.getPobAccountFee());
                configurationFee.setPobAccountFeeExtra(feeM.getPobAccountFeeExtra());
                configurationFee.setPobAccountFeeCap(feeM.getPobAccountFeeCap());
                configurationFee.setPobAccountFeeType(feeM.getPobAccountFeeType());
                configurationFee.setPobCardFee(feeM.getPobCardFee());
                configurationFee.setPobCardFeeExtra(feeM.getPobCardFeeExtra());
                configurationFee.setPobCardFeeCap(feeM.getPobCardFeeCap());
                configurationFee.setPobCardFeeType(feeM.getPobCardFeeType());
                // end of setting pob fee

                // This section is for micro transaction  fee
                configurationFee.setMicroTransactionAccountFee(feeM.getMicroTransactionAccountFee());
                configurationFee.setMicroTransactionAccountFeeType(feeM.getMicroTransactionAccountFeeType());
                configurationFee.setMicroTransactionCardFee(feeM.getMicroTransactionCardFee());
                configurationFee.setMicroTransactionCardFeeType(feeM.getMicroTransactionCardFeeType());
                configurationFee.setMicroTransactionAmount(feeM.getMicroTransactionAmount());
                // end of micro transaction  fee

                configurationFee.setBvnFee(feeM.getBvnFee());
//                if (status == true) {
                configurationFeeDao.create(configurationFee);
//                }

            } else {

                if (status == true) {
                    
                    ConfigurationFee configurationFee = configurationFeeDao.findByProduct(feeM.getProduct());

                    if (configurationFee != null && configurationFee.getId() != feeM.getModelId()) {
                        response.put("status-code", "02");
                        response.put("status", "Duplicate fee with for product with name " + configurationFee.getProduct().getName());
                        return response;
                    }

                    configurationFee = configurationFeeDao.find(feeM.getModelId());

                    configurationFee.setModifiedBy(feeM.getCreatedBy());
                    configurationFee.setModified(feeM.getCreatedOn());
                    //                configurationFee.setProduct(feeM.getProduct());

                    // This section is for international fee
                    configurationFee.setInterAccountFee(feeM.getInterAccountFee());
                    configurationFee.setInterAccountFeeExtra(feeM.getInterAccountFeeExtra());
                    configurationFee.setInterAccountFeeCap(feeM.getInterAccountFeeCap());
                    configurationFee.setInterAccountFeeType(feeM.getInterAccountFeeType());
                    configurationFee.setInterCardFee(feeM.getInterCardFee());
                    configurationFee.setInterCardFeeExtra(feeM.getInterCardFeeExtra());
                    configurationFee.setInterCardFeeCap(feeM.getInterCardFeeCap());
                    configurationFee.setInterCardFeeType(feeM.getInterCardFeeType());
                    // end of setting intertional fee

                    // This section is for pob fee
                    configurationFee.setPobAccountFee(feeM.getPobAccountFee());
                    configurationFee.setPobAccountFeeExtra(feeM.getPobAccountFeeExtra());
                    configurationFee.setPobAccountFeeCap(feeM.getPobAccountFeeCap());
                    configurationFee.setPobAccountFeeType(feeM.getPobAccountFeeType());
                    configurationFee.setPobCardFee(feeM.getPobCardFee());
                    configurationFee.setPobCardFeeExtra(feeM.getPobCardFeeExtra());
                    configurationFee.setPobCardFeeCap(feeM.getPobCardFeeCap());
                    configurationFee.setPobCardFeeType(feeM.getPobCardFeeType());
                    // end of setting pob fee

                    // This section is for micro transaction  fee
                    configurationFee.setMicroTransactionAccountFee(feeM.getMicroTransactionAccountFee());
                    configurationFee.setMicroTransactionAccountFeeType(feeM.getMicroTransactionAccountFeeType());
                    configurationFee.setMicroTransactionCardFee(feeM.getMicroTransactionCardFee());
                    configurationFee.setMicroTransactionCardFeeType(feeM.getMicroTransactionCardFeeType());

                    configurationFee.setBvnFee(feeM.getBvnFee());
                    
                    configurationFee.setMicroTransactionAmount(feeM.getMicroTransactionAmount());

                    configurationFeeDao.update(configurationFee);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            feeM.setApprovedOn(new Date());
            feeM.setApprovedBy(user);
            feeM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                feeM.setStatus(Status.APPROVED);
            } else {
                feeM.setStatus(Status.REJECTED);
            }

            configurationFeeMDao.update(feeM);

            log = new Log();
            log.setAction("Fee Authorization for " + feeM.getId() + " , " + feeM.getProduct().getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("cfeeupdated", true);
            session.setAttribute("cfeeupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Fee Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Fee Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log, null);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path(value = "/fee/unauth")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public ConfigurationFeeM getUnauthMerchantFeeById(@QueryParam(value = "id") long id) {

        try {
            if (id <= 0) {
                return null;
            }

            ConfigurationFeeM merchantFeeM = configurationFeeMDao.find(id);

            return merchantFeeM;
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Path(value = "/fee")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public ConfigurationFee getMerchantFeeById(@QueryParam(value = "id") long id) {

        try {
            if (id <= 0) {
                return null;
            }

            ConfigurationFee merchantFeeM = configurationFeeDao.find(id);

            return merchantFeeM;
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Path("/provider/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeProvider(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            ProviderM feeM = providerMDao.find(id);

            if (feeM == null) {
                response.put("status-code", "03");
                response.put("status", "fee record not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Provider Authorization with id " + feeM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log, null);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (feeM.getModelId() <= 0 && status == true) {

                Provider provider = providerDao.findByKey("name", feeM.getName());

                if (provider != null) {

                    response.put("status-code", "02");
                    response.put("status", "Duplicate record for provider " + feeM.getName());
                    return response;
                }

                provider = providerDao.findByKey("shortName", feeM.getShortName());

                if (provider != null) {

                    response.put("status-code", "02");
                    response.put("status", "Duplicate record for provider " + feeM.getName());
                    return response;
                }

                provider = new Provider();

                provider.setCreatedBy(feeM.getCreatedBy());
                provider.setCreatedOn(feeM.getCreatedOn());
                provider.setShortName(feeM.getShortName());

                // This section is for international fee
                if (feeM.isHasInterAccount()) {
                    provider.setInterAccountFee(feeM.getInterAccountFee());
                    provider.setInterAccountFeeExtra(feeM.getInterAccountFeeExtra());
                    provider.setInterAccountFeeCap(feeM.getInterAccountFeeCap());
                    provider.setInterAccountFeeType(feeM.getInterAccountFeeType());
                    provider.setHasInterAccount(true);
                }

                if (feeM.isHasInterCard()) {
                    provider.setInterCardFee(feeM.getInterCardFee());
                    provider.setInterCardFeeExtra(feeM.getInterCardFeeExtra());
                    provider.setInterCardFeeCap(feeM.getInterCardFeeCap());
                    provider.setInterCardFeeType(feeM.getInterCardFeeType());
                    provider.setHasInterCard(true);
                }
                // end of setting intertional fee

                // This section is for pob fee
                if (feeM.isHasLocalAccount()) {
                    provider.setLocalAccountFee(feeM.getLocalAccountFee());
                    provider.setLocalAccountFeeExtra(feeM.getLocalAccountFeeExtra());
                    provider.setLocalAccountFeeCap(feeM.getLocalAccountFeeCap());
                    provider.setLocalAccountFeeType(feeM.getLocalAccountFeeType());
                    provider.setHasLocalAccount(true);
                }

                if (feeM.isHasLocalCard()) {
                    provider.setLocalCardFee(feeM.getLocalCardFee());
                    provider.setLocalCardFeeExtra(feeM.getLocalCardFeeExtra());
                    provider.setLocalCardFeeCap(feeM.getLocalCardFeeCap());
                    provider.setLocalCardFeeType(feeM.getLocalCardFeeType());
                    provider.setHasLocalCard(true);
                }
                
                if(feeM.isHasUssd()){
                    provider.setUSSDFee(feeM.getUSSDFee());
                    provider.setUssdFeeCap(feeM.getUssdFeeCap());
                    provider.setUssdFeeExtra(feeM.getUssdFeeExtra());
                    provider.setUssdFeeType(feeM.getUssdFeeType());
                    provider.setHasUssd(true);
                }
                
                if(feeM.isHasInternetBanking()){
                    provider.setInternetBankingFee(feeM.getInternetBankingFee());
                    provider.setInternetBankingFeeCap(feeM.getInternetBankingFeeCap());
                    provider.setInternetBankingFeeType(feeM.getInternetBankingFeeType());
                    provider.setInternetBankingFeeType(feeM.getUssdFeeType());
                    provider.setHasInternetBanking(true);
                }
                
                if(feeM.isHasAmex()){
                    provider.setAmexFee(feeM.getAmexFee());
                    provider.setAmexFeeExtra(feeM.getAmexFeeExtra());
                    provider.setAmexFeeCap(feeM.getAmexFeeCap());
                    provider.setAmexFeeType(feeM.getAmexFeeType());
                }
                
                provider.setCurrency(feeM.getCurrency());

                provider.setName(feeM.getName());
                // end of micro transaction  fee

//                if (status == true) {
                providerDao.create(provider);
//                }

            } else {

                if (status == true) {
                    
                    
                    Provider provider = providerDao.findByKey("name", feeM.getName());

                    if (provider != null && provider.getId() != feeM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate record for provider " + feeM.getName());
                        return response;
                    }

                    provider = providerDao.findByKey("shortName", feeM.getShortName());

                    if (provider != null && provider.getId() != feeM.getModelId()) {
                        response.put("status-code", "02");
                        response.put("status", "Duplicate record with for provider with name " + provider.getName());
                        return response;
                    }

                    provider = providerDao.find(feeM.getModelId());

//                    provider.setModified(feeM.getCreatedBy());
                    provider.setModified(feeM.getCreatedOn());
                    provider.setName(feeM.getName());
                    provider.setShortName(feeM.getShortName());
                    //                provider.setProduct(feeM.getProduct());

                    // This section is for international fee
                    provider.setInterAccountFee(feeM.getInterAccountFee());
                    provider.setInterAccountFeeExtra(feeM.getInterAccountFeeExtra());
                    provider.setInterAccountFeeCap(feeM.getInterAccountFeeCap());
                    provider.setInterAccountFeeType(feeM.getInterAccountFeeType());
                    provider.setInterCardFee(feeM.getInterCardFee());
                    provider.setInterCardFeeExtra(feeM.getInterCardFeeExtra());
                    provider.setInterCardFeeCap(feeM.getInterCardFeeCap());
                    provider.setInterCardFeeType(feeM.getInterCardFeeType());
                    // end of setting intertional fee

                    // This section is for pob fee
                    provider.setLocalAccountFee(feeM.getLocalAccountFee());
                    provider.setLocalAccountFeeExtra(feeM.getLocalAccountFeeExtra());
                    provider.setLocalAccountFeeCap(feeM.getLocalAccountFeeCap());
                    provider.setLocalAccountFeeType(feeM.getLocalAccountFeeType());
                    provider.setLocalCardFee(feeM.getLocalCardFee());
                    provider.setLocalCardFeeExtra(feeM.getLocalCardFeeExtra());
                    provider.setLocalCardFeeCap(feeM.getLocalCardFeeCap());
                    provider.setLocalCardFeeType(feeM.getLocalCardFeeType());
                    provider.setCurrency(feeM.getCurrency());
                    
                    providerDao.update(provider);

                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            feeM.setApprovedOn(new Date());
            feeM.setApprovedBy(user);
            feeM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                feeM.setStatus(Status.APPROVED);
            } else {
                feeM.setStatus(Status.REJECTED);
            }

            providerMDao.update(feeM);

            log = new Log();
            log.setAction("Provider Authorization for " + feeM.getId() + " , " + feeM.getName());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("providerupdated", true);
            session.setAttribute("providerupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("Provider Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("Provider Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log, null);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path(value = "/provider/unauth")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public ProviderM getProviderByIdUnauth(@QueryParam(value = "id") long id) {

        try {
            if (id <= 0) {
                return null;
            }

            ProviderM provider = providerMDao.find(id);

            return provider;
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Path(value = "/provider")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Provider getProviderById(@QueryParam(value = "id") long id) {

        try {
            if (id <= 0) {
                return null;
            }

            Provider provider = providerDao.find(id);

            return provider;
        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Path(value = "/fetchcorelimits")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response fetchLimitsFromCore() throws DatabaseException {

        JsonArray array = httpUtil.getLimitFromCore();
        int counter = 0;

        if (array != null) {

//            List<ProductLimit> limits = new ArrayList<>();
            for (JsonValue object : array) {
                JsonObject jsonObject = (JsonObject) object;

                ProductLimit productLimit = new ProductLimit();
                productLimit.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + ""));
                productLimit.setCreatedOn(new Date());
                productLimit.setDailyTransactionLimit(jsonObject.getJsonNumber("dailytransactionlimit").doubleValue());
                productLimit.setSingleTransactionLimit(jsonObject.getJsonNumber("singletransactionlimit").doubleValue());
                productLimit.setLimitCreatedOn(jsonObject.getString("createdon", null));
                productLimit.setLastUpdated(jsonObject.getString("lastupdated", null));
                productLimit.setMerchant(jsonObject.getString("merchant", null));
                productLimit.setLimitType(jsonObject.getString("limittype", null));
                productLimit.setProductName("core");

                ProductLimit limit = productLimitDao.findByKey("limitType", productLimit.getLimitType());

                if (limit == null) {
                    counter++;
                    productLimitDao.create(productLimit);
                }
            }

            String description = "";

            if (counter == 0) {
                description = "No new limit is available from core";
            } else {
                description = counter + " Limit(s) has been added to the system";
            }

            JsonObject jsonObject = Json.createObjectBuilder().add("responsecode", "00").add("responsemessage", "successful").add("description", description).build();

            return Response.status(Response.Status.OK).entity(jsonObject).build();
        } else {

            JsonObject jsonObject = Json.createObjectBuilder().add("responsecode", "04").add("responsemessage", "failed").add("description", "Unable to pull limits. Please try again later").build();
            return Response.status(Response.Status.OK).entity(jsonObject).build();
        }

    }

    @Path(value = "/limits")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getLimits(@QueryParam(value = "product") String product) throws DatabaseException {

        PageResult<ProductLimit> pageResult = new PageResult<>(new ArrayList<>(), 0L, 0L);

        if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

            Page<ProductLimit> limits = productLimitDao.find(0, 0);

            pageResult.setData(limits.getContent());
            pageResult.setRecordsFiltered(limits.getCount());
            pageResult.setRecordsTotal(limits.getCount());

//            return Response.status(Response.Status.OK).entity(pageResult).build();
        }

        return Response.status(Response.Status.OK).entity(pageResult).build();
    }

    @Path(value = "/providers")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAllProviders() throws DatabaseException {

        List<Provider> providers = providerDao.findAll();

        List<Map> jsonObjects = providers.stream().map((Provider x) -> {
            Map<String, String> jsonObject = new HashMap<>();
            jsonObject.put("name", x.getName());
            jsonObject.put("shortName", x.getShortName());

            return jsonObject;
        }).collect(Collectors.<Map>toList());

        return Response.status(Response.Status.OK).entity(jsonObjects).build();
    }
    
    @Path("/currency/rate/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorizeExchangeRate(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            ExchangeRateM feeM = exchangeRateMDao.find(id);

            if (feeM == null) {
                response.put("status-code", "03");
                response.put("status", "exchangeRate record not found");
                return response;
            }

            Log log = new Log();
            log.setAction("ExchangeRate Authorization with id " + feeM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log, null);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (feeM.getModelId() <= 0 && status == true) {

                ExchangeRate exchangeRate = new ExchangeRate();
                exchangeRate.setApprovedBy(user);
                exchangeRate.setCreatedBy(feeM.getCreatedBy());
                exchangeRate.setCreatedOn(feeM.getCreatedOn());
                exchangeRate.setDestinationCurrency(feeM.getDestinationCurrency());
                exchangeRate.setOriginCurrency(feeM.getOriginCurrency());
                exchangeRate.setAmount(feeM.getAmount());
                exchangeRate.setApprovedOn(new Date());
                
                exchangeRateDao.create(exchangeRate);

            } else {

                if (status == true) {
                    
                    ExchangeRate exchangeRate = exchangeRateDao.find(feeM.getOriginCurrency(), feeM.getDestinationCurrency());

                    if (exchangeRate != null && exchangeRate.getId() != feeM.getModelId()) {

                        response.put("status-code", "02");
                        response.put("status", "Duplicate record for exchange Rate ");
                        return response;
                    }

                    exchangeRate = exchangeRateDao.find(feeM.getModelId());
                    
                    exchangeRate.setOriginCurrency(feeM.getOriginCurrency());
                    exchangeRate.setDestinationCurrency(feeM.getDestinationCurrency());
                    exchangeRate.setModified(new Date());
                    exchangeRate.setApprovedBy(user);
                    exchangeRate.setApprovedOn(new Date());
                    exchangeRate.setAmount(feeM.getAmount());

                    exchangeRateDao.update(exchangeRate);
                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            feeM.setApprovedOn(new Date());
            feeM.setApprovedBy(user);
            feeM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                feeM.setStatus(Status.APPROVED);
            } else {
                feeM.setStatus(Status.REJECTED);
            }

            exchangeRateMDao.update(feeM);

            log = new Log();
            log.setAction("ExchangeRate Authorization for " + feeM.getId());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("exchangeupdated", true);
            session.setAttribute("exchangeupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("ExchangeRate Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("ExchangeRate Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log, null);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }
}

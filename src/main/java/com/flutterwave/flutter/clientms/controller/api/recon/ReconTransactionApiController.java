/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.recon;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.flutterwave.flutter.clientms.controller.api.model.recon.DashboardReport;
import com.flutterwave.flutter.clientms.dao.recon.BatchDao;
import com.flutterwave.flutter.clientms.dao.recon.FlutterTransactionDao;
import com.flutterwave.flutter.clientms.dao.recon.ReconTransactionDao;
import com.flutterwave.flutter.clientms.model.recon.Batch;
import com.flutterwave.flutter.clientms.model.recon.FlutterTransaction;
import com.flutterwave.flutter.clientms.model.recon.ReconTransaction;
import com.flutterwave.flutter.clientms.service.recon.TransactionService;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.AnalysisModel;
import com.flutterwave.flutter.clientms.viewmodel.TransactionViewModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author adeyemi
 */
@Path("/recon/transaction")
@RequestScoped
public class ReconTransactionApiController {

    @EJB
    private TransactionService transactionService;
    @EJB
    private ReconTransactionDao transactionDao;
    @EJB
    private FlutterTransactionDao flutterTransactionDao;
    @EJB
    private BatchDao batchDao;
    @Inject
    private Global global;
    
    @Path("/summary")
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    @JsonAnyGetter
    public Response getDashBoardSummary() {

        return Response.status(Response.Status.OK).entity(transactionService.getDashboardSummary()).build();
    }
    
    @Path("/dsummary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @JsonAnyGetter
    public Response getDashboardSummary(@QueryParam(value = "currency") String currency,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "range") String range){
        
        Date startDate = null , endDate = null;
        
        if (!range.isEmpty() && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        DashboardReport report = transactionService.getDashboardReport(startDate, endDate, currency, global.getDomainId());
        
        return Response.status(Response.Status.OK).entity(report). build();
    }

    @Path("/csummary/{dateRange}")
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    @JsonAnyGetter
    public Response getCurrencyAnalysis(@PathParam(value = "dateRange") String range) {

        Date startDate = null , endDate = null;
        
        if (!range.isEmpty() && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        return Response.status(Response.Status.OK).entity(transactionService.getCurrencyAnalysis(startDate, endDate)).build();
    }
    
                    
    @Path("/list")
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response getTransactions(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "search") String search,
            @QueryParam(value = "searchColumn") String searchColumn,
            @QueryParam(value = "status") String status,
            @QueryParam(value = "doSearch") boolean doSearch,
            @QueryParam(value = "doFilter") boolean doFilter){
        
        if(length == 0)
            length = 10;
        
        PageResult<TransactionViewModel> pageResult = new PageResult<>();
        
        try {
            
            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }
            
            Page<ReconTransaction> transactions = new Page<>();
            
            if(search.equals(""))
                search = null;
            
            if(doSearch){
                startDate = endDate = null;
                status = "any";
            }
            
            if(doFilter){
                searchColumn = null;
                search =  null;
            }
            
            if(searchColumn != null && !searchColumn.equalsIgnoreCase("") && search != null){
                
                searchColumn = TransactionUtility.populateIndex().getOrDefault(searchColumn.toUpperCase(), null)+"";
                
                Class type = TransactionUtility.getFieldType(ReconTransaction.class, searchColumn);
                
                type = TransactionUtility.map.getOrDefault(type, type);
                
                if(type == Date.class){
                    Date d = TransactionUtility.formatDate(search);
                    transactions =transactionDao.getTransaction(startDate, endDate, searchColumn, d, length, start, 
                            "any".equalsIgnoreCase(status) == true ? null : "settled".equalsIgnoreCase(status), global.getDomainId());                    
                }else if(type == Double.class){
                    
                    Object value = TransactionUtility.convertToType(search, type);
                    double dValue = Double.parseDouble(value+"");
                    transactions = transactionDao.getTransaction(startDate, endDate, searchColumn, dValue, length, start, 
                            "any".equalsIgnoreCase(status) == true ? null : "settled".equalsIgnoreCase(status), global.getDomainId());
                    
                }else if ( type == Integer.class || type == Long.class){
                    
                    Object value = TransactionUtility.convertToType(search, type);
                    long dValue = Long.parseLong(value+"");
                    transactions = transactionDao.getTransaction(startDate, endDate, searchColumn, dValue, length, start, 
                            "any".equalsIgnoreCase(status) == true ? null : "settled".equalsIgnoreCase(status), global.getDomainId());
                    
                }else{
                    
                    Object value = TransactionUtility.convertToType(search, type);
                    transactions = transactionDao.getTransaction(startDate, endDate, searchColumn, value.toString(), length, start, 
                            "any".equalsIgnoreCase(status) == true ? null : "settled".equalsIgnoreCase(status), global.getDomainId());
                }
            }else
                transactions = transactionDao.getTransactions(startDate, endDate, search, searchColumn, length, start,
                        "any".equalsIgnoreCase(status) == true ? null : "settled".equalsIgnoreCase(status), global.getDomainId());
            
            
            List<TransactionViewModel> transactionViewModels = new ArrayList();
            
            if(transactions != null && transactions.getContent() != null){
                transactionViewModels = transactions.getContent().stream().map((ReconTransaction t) -> {
                    TransactionViewModel model = new TransactionViewModel();
                    model.setTransaction(t);
                    model.setAge(Utility.getAge(t));
                    
                    return model;
                }).collect(Collectors.<TransactionViewModel>toList());
            
                pageResult = new PageResult<>(transactionViewModels, transactions.getCount(), transactions.getCount());
            }
            
        } catch (Exception ex) {
            Logger.getLogger(ReconTransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    
    @Path("/recentupload")
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response getRecentUploaded(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "range") String batchId,
            @QueryParam(value = "settled") String settled){
        
        if(length == 0)
            length = 10;
        
        PageResult<ReconTransaction> pageResult = new PageResult<>();
        
        try {
            
            Date startDate = null, endDate = null;

            Batch b = batchDao.findByKey("batchId",batchId);
            
            Page<ReconTransaction> transactions = new Page<>();
            
            if("all".equalsIgnoreCase(settled)) 
                transactions = transactionDao.findByBatch(b, null, start, length);
            else
                transactions = transactionDao.findByBatch(b, "settled".equalsIgnoreCase(settled), start, length);
            
            pageResult = new PageResult<>(transactions.getContent(), transactions.getCount(), transactions.getCount());
            
        } catch (Exception ex) {
            Logger.getLogger(ReconTransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    } 
    
    @Path("/flutterwave/list")
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response getFlutterwaveTransactions(@QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "batch") long batchId){
        
        if(length == 0)
            length = 20;
        
        PageResult<FlutterTransaction> pageResult = new PageResult<>();
        
        try {
            
            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }
            
            com.flutterwave.flutter.core.util.Page<FlutterTransaction> transactions;
            
            if(startDate == null)
                transactions = flutterTransactionDao.find(start, length);
            else
                transactions = flutterTransactionDao.getTransactions(startDate, endDate, null, null, length, start);
            
            pageResult = new PageResult<>(transactions.getContent(), transactions.getCount(), transactions.getCount());
            
        } catch (Exception ex) {
            Logger.getLogger(ReconTransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path("/allfield")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<String> getAllFieldNames(){
        
        List<String> ids = new ArrayList<>();
        ids.addAll(TransactionUtility.populateIndex().keySet());
        
        Collections.sort(ids);
        
        return ids;
    }
    
    @Path("/allmerchant")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<String> getMerchants(){
        
        return transactionDao.getMerchants(global.getDomainId());
    }
    
    @Path("/fallmerchant")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<String> getFlutterMerchants(){
        
        return flutterTransactionDao.getMerchants();
    }
    
    @Path(value = "/find/{fileid}")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map<String, Object> getTransaction(@PathParam(value = "fileid") String fileId){
        
        Map<String, Object> obj = new HashMap<>();
        try {
            if(fileId == null){
                obj.put("status", "file id not provided");
                obj.put("status-code", "90");
                return obj;
            }
            
            ReconTransaction transaction = transactionDao.findByKey("fileId", Long.parseLong(fileId));
            
            if(transaction == null){
                obj.put("status", "transaction with id not found");
                obj.put("status-code", "01");
                return obj;
            }
            
            obj.put("status", "Successful");
            obj.put("status-code", "00");
            obj.put("data", transaction);
            obj.put("settled", transaction.isSettled());
            return obj;
        } catch (DatabaseException ex) {
            Logger.getLogger(ReconTransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
            
            obj.put("status", "System error");
            obj.put("status-code", "96");
        }
         
        return obj;
    }
    
    @Path(value = "/settle/{fileid}")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map<String, Object> settledTransaction(@PathParam(value = "fileid") String fileId){
        
        Map<String, Object> obj = new HashMap<>();
        try {
            if(fileId == null){
                obj.put("status", "file id not provided");
                obj.put("status-code", "90");
                return obj;
            }
            
            ReconTransaction transaction = transactionDao.findByKey("fileId", Long.parseLong(fileId));
            
            if(transaction == null){
                obj.put("status", "transaction with id not found");
                obj.put("status-code", "01");
                return obj;
            }
            
            if(transaction.isSettled() == true){
                
                obj.put("status", "Transaction already settled");
                obj.put("status-code", "92");
                
                return obj;
            }
            
            transaction.setSettlementDate(new Date());
            transaction.setSettled(true);
            
            boolean status = transactionDao.update(transaction);
            
            if(status == true){
                obj.put("status", "Successful");
                obj.put("status-code", "00");
                obj.put("fileId", fileId);
            }else{
               throw new DatabaseException("Unable to settle transaction at the moment, please try again later");
            }
            
            return obj;
        } catch (DatabaseException ex) {
            Logger.getLogger(ReconTransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
            
            obj.put("status", "System error");
            obj.put("status-code", "96");
        }
         
        return obj;
    }
    
    @Path(value = "/graph")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map<String, Long> getTransactionsForGraph(@QueryParam(value = "daterange") String range){
        
        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
       List transactions = transactionDao.getTransactionCount(startDate, endDate);
       
       Map<String, Long> map = new HashMap<>();
       
       SimpleDateFormat dateFormat = new SimpleDateFormat("DD/MM/YYY");
       
       for(Object obj : transactions){
           
           Object[] list = (Object[]) obj;
           
           BigInteger count = (BigInteger) list[0];
           Date date = (Date) list[1];
           
           if(date != null)
               map.put(dateFormat.format(date), count.longValue());
       }
       
       return map;
    }
    
    @Path(value = "/currencies")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map getCurrencies(){
        
        List currencies = transactionDao.getCurrencies();
        
        Map<String, String> currencyMap = new HashMap<>();
        
        if(currencies != null){
            
            currencies.forEach((s) -> {
                String name = TransactionUtility.getCurrencyName(s.toString());
                currencyMap.put(name, s.toString());
            });            
        }
        
        return currencyMap;
    }
    
    @Path(value = "/worth")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map<String, Object> getWorth(@QueryParam("currency") String currency,
            @QueryParam("daterange") String range, @QueryParam("merchant") String merchant){
        
        try{
            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }
            }
            
            List<Object[]> list = transactionDao.getWorthAndVolume(startDate, endDate, global.getDomainId(), TransactionUtility.getCurrencyCode(currency), 
                    merchant == null ? null : "all".equalsIgnoreCase(merchant) ? null : merchant );
            
            
            Map<String, Object> values = new HashMap<>();
            
//            long currencyC = Integer.parseInt(currency);
            NumberFormat formatter = new DecimalFormat("#0.00");
            
            list.forEach((obj) -> {
                Double sum = (Double) obj[0];
                BigInteger count = (BigInteger) obj[1];
                int currencyCode = (Integer) obj[2];
                String scheme = (String)obj[3];
                String status = (String)obj[4];
                if (currency.equalsIgnoreCase(TransactionUtility.getCurrencyName(currencyCode+""))) {
                    
                    Map<String, String> temp = (Map<String, String>) values.getOrDefault(scheme, new HashMap<>());
                    temp.put("00".equals(status) ? "success" : "failure", formatter.format(sum));
                    values.put(scheme, temp);
                }
            });
            
            List<String> schemes = TransactionUtility.getScheme();
            
            schemes.forEach((s) -> {
                Map<String, String> k = (Map<String, String>)values.getOrDefault(s, null);
                
                if(k == null){
                    k = new HashMap<>();
                    k.put("success", "0");
                    k.put("failure", "0");
                    values.put(s, k);
                }else{
                    String value = k.getOrDefault("success", null);
                    
                    if(value == null){
                        k.put("success", "0");                        
                    }
                    
                    value = k.getOrDefault("failure", null);
                    
                    if(value == null){
                        k.put("failure", "0");                        
                    }
                    
                    values.put(s, k);
                }
                
            });
            
            return values;
            
        }catch(Exception ex){
            if(ex != null)
                ex.printStackTrace();
        }
        
        return new HashMap();
    }
    
    @Path(value = "/volume")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map<String, Object> getVolume(@QueryParam("currency") String currency,
            @QueryParam("daterange") String range, @QueryParam("merchant") String merchant){
        
        try{
            Date startDate = null, endDate = null;

            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }
            }
            
            List<Object[]> list;
            
//            if(global.getDomainId() != null){
//                list = transactionDao.getSettlement(startDate, endDate, global.getDomainId(), TransactionUtility.getCurrencyCode(currency), 
//                        merchant == null ? null : "all".equalsIgnoreCase(merchant) ? null : merchant);
//            }else
//                list = transactionDao.getSettlement(startDate, endDate,null, TransactionUtility.getCurrencyCode(currency), merchant == null ? null : "all".equalsIgnoreCase(merchant) ? null : merchant);
//            

            list = new ArrayList<>();
            
            Map<String, Object> values = new HashMap<>();
            
            
            list.forEach((obj) -> {
                Double sum = (Double) obj[0];
                BigInteger count = (BigInteger) obj[1];
                int currencyCode = (Integer) obj[2];
                String scheme = (String)obj[3];
                boolean status = (Boolean)obj[4];
                if (currency.equalsIgnoreCase(TransactionUtility.getCurrencyName(currencyCode+"")) || "ALL".equalsIgnoreCase(currency)) {
                    
                    Map<String, String> temp = (Map<String, String>) values.getOrDefault(scheme, new HashMap<>());
                    
                    long value = 0;
                    
                    if("ALL".equalsIgnoreCase(currency)){
                       value = Long.parseLong(temp.getOrDefault(true == status ? "success" : "failure", "0"));
                    }
                    
                    value += count.longValue();
                    
                    temp.put(true == status ? "success" : "failure", value+"");
                    values.put(scheme, temp);
                }
            });
            
            List<String> schemes = TransactionUtility.getScheme();
            
            schemes.forEach((s) -> {
                Map<String, String> k = (Map<String, String>)values.getOrDefault(s, null);
                
                if(k == null){
                    k = new HashMap<>();
                    k.put("success", "0");
                    k.put("failure", "0");
                    values.put(s, k);
                }else{
                    String value = k.getOrDefault("success", null);
                    
                    if(value == null){
                        k.put("success", "0");                        
                    }
                    
                    value = k.getOrDefault("failure", null);
                    
                    if(value == null){
                        k.put("failure", "0");                        
                    }
                    
                    values.put(s, k);
                }
            });
            
            return values;
            
        }catch(Exception ex){
            if(ex != null)
                ex.printStackTrace();
        }
        
        return new HashMap();
    }
    
    @Path(value = "/fsummary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public AnalysisModel getFlutterTransactionAnalysis( @QueryParam(value = "range") String range,
            @QueryParam(value = "currency") String currency){
            
        Date startDate = null, endDate = null;
        
        String currencyC;
        
        if(currency == null || "".equalsIgnoreCase(currency))
            currencyC = "NGN";
        else
            currencyC = currency;
            

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        List<Object[]> list = flutterTransactionDao.getAnalysedData(startDate, endDate, currencyC);
        
        AnalysisModel analysisModel = new AnalysisModel();
        
        list.forEach((Object[] obj) -> {
            Double sum = (Double) obj[0];
            String response = (String) obj[1]+"";
            BigInteger count = (BigInteger) obj[2];
            String currencyS = (String)obj[3];
            String scheme = (String)obj[4];
            
            if (currencyC.equalsIgnoreCase(currencyS)) {
                    
                if(scheme != null && !"".equals(scheme) && !"0".equals(scheme)){
                    
                    if("vc".equalsIgnoreCase(scheme)){
                        
                        if( !"0".equalsIgnoreCase(response)){
                            analysisModel.setVcFailedAmount( analysisModel.getVcFailedAmount()+ (sum == null ? 0 : sum));
                            analysisModel.setVcFailedVolume(analysisModel.getVcFailedVolume() + (count == null ? 0 : count.longValue()));
                            
                        }else{
                            analysisModel.setVcSuccessAmount(analysisModel.getVcSuccessAmount()+ (sum == null ? 0 : sum));
                            analysisModel.setVcSuccessVolume(analysisModel.getVcSuccessVolume() + (count == null ? 0 : count.longValue()));
                            
                        }
                    }else if("mc".equalsIgnoreCase(scheme)){
                        
                        if( !"0".equalsIgnoreCase(response)){
                            analysisModel.setMcFailedAmount( analysisModel.getMcFailedAmount()+ (sum == null ? 0 : sum));
                            analysisModel.setMcFailedVolume(analysisModel.getMcFailedVolume() + (count == null ? 0 : count.longValue()));
                            
                        }else{
                            analysisModel.setMcSuccessAmount(analysisModel.getMcSuccessAmount()+ (sum == null ? 0 : sum));
                            analysisModel.setMcSuccessVolume(analysisModel.getMcSuccessVolume() + (count == null ? 0 : count.longValue()));
                        }
                    }if("verve".equalsIgnoreCase(scheme)){
                        
                        if( !"0".equalsIgnoreCase(response)){
                            analysisModel.setVerveFailedAmount( analysisModel.getVerveFailedAmount()+ (sum == null ? 0 : sum));
                            analysisModel.setVerveFailedVolume(analysisModel.getVerveFailedVolume() + (count == null ? 0 : count.longValue()));
                        }else{
                            analysisModel.setVerveSuccessAmount(analysisModel.getVerveSuccessAmount()+ (sum == null ? 0 : sum));
                            analysisModel.setVerveSuccessVolume(analysisModel.getVerveSuccessVolume() + (count == null ? 0 : count.longValue()));
                            
                        }
                    }
                    
                    if( !"0".equalsIgnoreCase(response)){
                        analysisModel.setFailedAmount( analysisModel.getFailedAmount()+ (sum == null ? 0 : sum));
                        analysisModel.setFailedVolume(analysisModel.getFailedVolume() + (count == null ? 0 : count.longValue()));
                    }else{
                        analysisModel.setSuccessAmount(analysisModel.getSuccessAmount()+ (sum == null ? 0 : sum));
                        analysisModel.setSuccessVolume(analysisModel.getSuccessVolume() + (count == null ? 0 : count.longValue()));
                    }                    
                    
                }else{
                    
                    if( !"0".equalsIgnoreCase(response)){
                        analysisModel.setFailedAmount( analysisModel.getFailedAmount()+ (sum == null ? 0 : sum));
                        analysisModel.setFailedVolume(analysisModel.getFailedVolume() + (count == null ? 0 : count.longValue()));
                    }else{
                        analysisModel.setSuccessAmount(analysisModel.getSuccessAmount()+ (sum == null ? 0 : sum));
                        analysisModel.setSuccessVolume(analysisModel.getSuccessVolume() + (count == null ? 0 : count.longValue()));
                    }    
                }
                
            }
        });
        
        return analysisModel;
    }
    
    @Path(value = "/recent")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<FlutterTransaction> getRecent(){
        
        try {
            return flutterTransactionDao.find(0, 8).getContent();
        } catch (DatabaseException ex) {
            Logger.getLogger(ReconTransactionApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return new ArrayList<>();
    }
    
    @Path(value = "/fgraph")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map getFlutterTransactionGraph(@QueryParam(value = "range") String range,
            @QueryParam(value = "currency") String currency){
        
        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
       List transactions = flutterTransactionDao.getTransactionCount(startDate, endDate, currency);
       
       Map<String, Long> map = new HashMap<>();
       
       Map<String, Object> params = new TreeMap<>();
       
       SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYY");
       
       for(Object obj : transactions){
           
           Object[] list = (Object[]) obj;
           
           String response = (String)list[0];
           BigInteger count = (BigInteger) list[1];
           Date date = (Date) list[2];
           
           String dateS = dateFormat.format(date);
           
           String status = "successful";
           
           if(!"0".equalsIgnoreCase(response)){
               status = "failed";
           }
           
           map = (HashMap)params.getOrDefault(dateS, new HashMap<>());
           
           long value = map.getOrDefault(status, 0L);
           
           value = value + (count == null ? 0 : count.longValue());
           
           map.put(status, value);
           
           params.put(dateS, map);           
       }
       
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
       
        Date tempDate = startDate;
        
        while( tempDate.before(endDate) || tempDate.equals(endDate)){
            
            String tempS = dateFormat.format(tempDate);
            
            Map<String, Long> m = (HashMap<String, Long>)params.getOrDefault(tempS, null);
            
            if(m == null){
                m = new HashMap<>();
                m.put("successful", 0L);
                m.put("failed", 0L);
                params.put(tempS, m);
            }
            
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            tempDate = calendar.getTime();
        }
       
       
       return params;
    }
    
    @Path("/trend")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Map getDashboardTrend(@QueryParam(value = "currency") String currency,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "merchant") String merchant){
        
        Date startDate = null , endDate = null;
        
        if (!range.isEmpty() && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
         
        Map data = transactionService.getTransactionTrend(startDate, endDate, currency, global.getDomainId(), 
                merchant == null ? null : "all".equalsIgnoreCase(merchant) ? null : merchant);
        
        return data; 
    }
}

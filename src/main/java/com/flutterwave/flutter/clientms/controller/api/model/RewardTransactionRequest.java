/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class RewardTransactionRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the transactionReference
     */
    public String getTransactionReference() {
        return transactionReference;
    }

    /**
     * @param transactionReference the transactionReference to set
     */
    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    /**
     * @return the point
     */
    public int getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(int point) {
        this.point = point;
    }
    
    @JsonProperty(value = "transactionreference")
    @NotBlank(message = "Transaction reference must be provided")
    private String transactionReference;
    private int point;
    private String hash;

    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("transactionReference", transactionReference)
                .put("point", point)
                .put("hash", hash);
        
        return jSONObject.toString();
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author emmanueladeyemi
 */
public class SlaNotificationRequest {

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

//    /**
//     * @return the merchantName
//     */
//    public String getMerchantName() {
//        return merchantName;
//    }
//
//    /**
//     * @param merchantName the merchantName to set
//     */
//    public void setMerchantName(String merchantName) {
//        this.merchantName = merchantName;
//    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }
    
//    @JsonProperty(value = "merchantname")
//    private String merchantName;
    @NotBlank(message = "Email address must be provided")
    private String email;
    @NotBlank(message = "product must be provided")
    private String product;
    @NotBlank(message = "hash value must be provided")
    private String hash;
    @JsonProperty(value = "merchantid")
    @NotBlank(message = "merchant id must be provided")
    private String merchantId;
    
    public String toHashableString(){
    
        StringBuilder builder = new StringBuilder();
        builder.append(merchantId)
                .append(email)
                .append(product);
        
        return builder.toString();
    }
}

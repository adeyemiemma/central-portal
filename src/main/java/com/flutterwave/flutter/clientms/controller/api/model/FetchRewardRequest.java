/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.util.Utility;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class FetchRewardRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the sourceId
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }
    
//    private String merchantToken;
    @JsonProperty(value = "startdate")
    private String startDate;
    @JsonProperty(value = "enddate")
    private String endDate;
    @JsonProperty(value = "sourceid")
    @NotBlank(message = "Source id must be provided")
    private String sourceId;
    @NotBlank(message = "Hash must be provided")
    private String hash;

    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("startDate", Utility.nullToEmpty(startDate))
                .put("endDate", Utility.nullToEmpty(endDate))
                .put("sourceid", Utility.nullToEmpty(sourceId))
                .put("hash", Utility.nullToEmpty(hash));
        
        return jSONObject.toString();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.api.model.ChargeBackRequest;
import com.flutterwave.flutter.clientms.controller.api.model.ChargebackTransactionRequest;
import com.flutterwave.flutter.clientms.dao.ChargeBackDao;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CoreTransactionDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.RaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionWHDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.ChargeBackDisputeMDao;
import com.flutterwave.flutter.clientms.dao.maker.ChargeBackMDao;
import com.flutterwave.flutter.clientms.dao.maker.MerchantConfigMDao;
import com.flutterwave.flutter.clientms.model.ChargeBack;
import com.flutterwave.flutter.clientms.model.CoreTransaction;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.MoneywaveTransaction;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.RaveTransaction;
import com.flutterwave.flutter.clientms.model.RaveTransactionWH;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.TransactionViewModel;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.ChargeBackDisputeM;
import com.flutterwave.flutter.clientms.model.maker.ChargeBackM;
import com.flutterwave.flutter.clientms.model.maker.UpdateModel;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.ChargeBackStatus;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.ChargeBackViewModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 *
 * @author emmanueladeyemi
 */
@Path("/chargeback")
@RequestScoped
public class ChargeApiController {

    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private ChargeBackDao chargeBackDao;
    @EJB
    private ChargeBackMDao chargeBackMDao;
    @EJB
    private UserDao userDao;
    @EJB
    private LogService logService;
    @EJB
    private RaveTransactionWHDao raveTransactionWHDao;
    @EJB
    private CoreTransactionDao coreTransactionDao;
    @EJB
    private TransactionDao transactionDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private ChargeBackDisputeMDao chargeBackDisputeMDao;

    /**
     * This is called to authorize chargeBack
     *
     * @param updateModel
     * @return
     */
    @Path("/authorize")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, String> authorize(@Valid UpdateModel updateModel) {

        Map<String, String> response = new HashMap<>();

        try {

            long id = updateModel.getId();

            if (id <= 0) {

                response.put("status-code", "01");
                response.put("status", "No id provided");
                return response;
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            ChargeBackM chargeBackM = chargeBackMDao.find(id);

            if (chargeBackM == null) {
                response.put("status-code", "03");
                response.put("status", "chargeBack charge record not found");
                return response;
            }

            Log log = new Log();
            log.setAction("Merchant Charge back Authorization with id " + chargeBackM.getId());
            log.setCreatedOn(new Date());
            log.setDescription("Authorization Started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
            log.setLogState(LogState.STARTED);

            logService.log(log, null);

            boolean status = true;

            if (!"approve".equalsIgnoreCase(updateModel.getOperation())) {

                if (updateModel.getRejectionMessage() == null || "".equalsIgnoreCase(updateModel.getRejectionMessage())) {

                    response.put("status-code", "03");
                    response.put("status", "please provide reason for rejection");
                    return response;
                }

                status = false;
            }

            if (chargeBackM.getModelId() <= 0 && status == true) {

                ChargeBack chargeBack = chargeBackDao.findByKey("flwReference", chargeBackM.getFlwReference());

                if (chargeBack != null) {

                    response.put("status-code", "02");
                    response.put("status", "Duplicate charge back for transaction " + chargeBackM.getFlwReference());
                    return response;
                }

                chargeBack = new ChargeBack();

                chargeBack.setCreatedBy(chargeBackM.getCreatedBy());
                chargeBack.setCreatedOn(chargeBackM.getCreatedOn());
                
                if("US".equalsIgnoreCase(chargeBackM.getCurrency())){
                    chargeBack.setCurrency("USD");
                }else
                    chargeBack.setCurrency(chargeBackM.getCurrency());
                
                chargeBack.setProduct(chargeBackM.getProduct());
                chargeBack.setFlwReference(chargeBackM.getFlwReference());
                chargeBack.setMerchantId(chargeBackM.getMerchantId());
                chargeBack.setRrn(chargeBackM.getRrn());
                chargeBack.setChargeBackStatus(ChargeBackStatus.NONE);
                chargeBack.setApprovedOn(new Date());
                chargeBack.setApprovedBy(user);
                chargeBack.setAmount(chargeBackM.getAmount());
                chargeBack.setAuthenticationType(chargeBackM.getAuthenticationType());
                chargeBack.setProduct(chargeBackM.getProduct());
                chargeBack.setProductMid(chargeBackM.getProductMid());
                chargeBack.setProductParentMid(chargeBackM.getProductParentMid());
                // end of setting intertional chargeBack

//                if (status == true) {
                chargeBackDao.create(chargeBack);
//                }

            } else {

//                ChargeBack chargeBack = chargeBackDao.findByKey("flwReference",chargeBackM.getFlwReference());
//
//                if (chargeBack != null && chargeBack.getId() != chargeBackM.getModelId()) {
//                    response.put("status-code", "02");
//                    response.put("status", "Duplicate chargeBack with for chargeBack with reference " + chargeBack.getFlwReference());
//                    return response;
//                }
//
//                if (status == true) {
//
//                    chargeBack = chargeBackDao.find(chargeBackM.getModelId());
//
////                    chargeBack.setModifiedBy(chargeBackM.getCreatedBy());
//                    chargeBack.setModifiedOn(chargeBackM.getCreatedOn());
//                    //                chargeBack.setProduct(chargeBackM.getProduct());
//
//                    // This section is for international chargeBack
//                    chargeBack.setProduct(chargeBackM.getProduct());
//                    chargeBack.setAccountNo(chargeBackM.getAccountNo());
//                    chargeBack.setCompanyName(chargeBackM.getCompanyName());
//                    chargeBack.setApprovedOn(new Date());
//                    chargeBack.setApprovedBy(user);
//
//                    // This section is for international chargeBack
//                    chargeBack.setAddressLine1(chargeBackM.getAddressLine1());
//                    chargeBack.setAddressLine2(chargeBackM.getAddressLine2());
//                    chargeBack.setBank(chargeBackM.getBank());
//                    chargeBack.setContactPerson(chargeBackM.getContactPerson());
//                    chargeBack.setCountry(chargeBackM.getCountry());
//                    chargeBack.setCurrency(chargeBackM.getCurrency());
//                    chargeBack.setEmail(chargeBackM.getEmail());
//                    chargeBack.setFirstName(chargeBackM.getFirstName());
//                    chargeBack.setLastName(chargeBackM.getLastName());
//                    chargeBack.setMerchantType(chargeBackM.getMerchantType());
////                    chargeBack.setPassword("12345677");
//                    chargeBack.setPhone(chargeBackM.getPhone());
//                    chargeBack.setProduct(chargeBackM.getProduct());
//                    chargeBack.setRcNumber(chargeBackM.getRcNumber());
//                    chargeBack.setEnabled(chargeBackM.isEnabled());
//                    // end of setting pob chargeBack
//
//                    chargeBackDao.update(chargeBack);
//
//                }
            }

            response.put("status-code", "00");
            response.put("status", "Success");

            chargeBackM.setApprovedOn(new Date());
            chargeBackM.setApprovedBy(user);
            chargeBackM.setRejectionReason(updateModel.getRejectionMessage());

            if (status) {
                chargeBackM.setStatus(Status.APPROVED);
            } else {
                chargeBackM.setStatus(Status.REJECTED);
            }

            chargeBackMDao.updateE(chargeBackM);

            log = new Log();
            log.setAction("Merchant Authorization for " + chargeBackM.getId() + " , " + chargeBackM.getFlwReference());
            log.setCreatedOn(new Date());
            if (status == true) {
                log.setDescription("Authorization Completed Successfully");
            } else {
                log.setDescription("Details Rejected because " + updateModel.getRejectionMessage());
            }
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.FINISH);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("cchargeupdated", true);
            session.setAttribute("cchargeupdatedm", true);

            return response;

        } catch (DatabaseException ex) {
            Logger.getLogger(ApiController.class.getName()).log(Level.SEVERE, null, ex);
            response.put("status", "System error");
            response.put("status-code", "96");
            if (ex != null) {
                response.put("error", "" + ex.getMessage());
            }

            try {
                Log log = new Log();
                log.setAction("ChargeBack Authorization");
                log.setCreatedOn(new Date());
                log.setDescription("ChargeBack Authorization error");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log, null);
                }
            } catch (Exception e) {
            }
        }

        return response;
    }

    @Path(value = "/transaction")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getTransaction(@Valid ChargebackTransactionRequest request) {

//        if (reference == null) {
//            return null;
//        }

        Map<String, Object> response = new HashMap<>();

        TransactionViewModel transaction = null;

        String reference = request.getReference();
        
        String product = request.getProduct();
        
        try {
            String searchParam = "%" + request.getReference() + "%";

            if ("rave".equalsIgnoreCase(product)) {

                RaveTransactionWH raveTransaction = raveTransactionWHDao.findByKey("flutterReference", reference);
                
                if (raveTransaction != null) {

                     List<TransactionViewModel> transactions = transactionDao.findMiniTransaction("flwTxnReference", searchParam);

                    if (transactions == null || transactions.isEmpty()) {
                        response.put("responsecode", "05");
                        response.put("responsemessage", "transaction record not found");

                        return Response.status(Response.Status.OK).entity(response).build();
                    }

                    transaction = transactions.get(0);
                    
                    response.put("responsecode", "00");
                    response.put("responsemessage", "found");
                    response.put("data", transaction);

                    return Response.status(Response.Status.OK).entity(response).build();
                }


                List<TransactionViewModel> transactions = transactionDao.findMiniTransaction("rrn", reference);

                if (transactions == null || transactions.isEmpty()) {
                    response.put("responsecode", "05");
                    response.put("responsemessage", "transaction record not found");

                    return Response.status(Response.Status.OK).entity(response).build();
                }

                transaction = transactions.get(0);

                response.put("responsecode", "00");
                response.put("responsemessage", "found");
                response.put("data", transaction);

                return Response.status(Response.Status.OK).entity(response).build();

            } else {

                List<TransactionViewModel> transactions = transactionDao.findMiniTransaction("flwTxnReference", searchParam);

                if (transactions == null || transactions.isEmpty()) {
//                    String searchParam = "%" + reference + "%";

                    transactions = transactionDao.findMiniTransaction("flwTxnReference", searchParam);

                    if (transactions == null || transactions.isEmpty()) {
                        response.put("responsecode", "05");
                        response.put("responsemessage", "transaction record not found");

                        return Response.status(Response.Status.OK).entity(response).build();
                    }

                    transaction = transactions.get(0);
                }else{
                    
                    transaction = transactions.get(0);
                }

                
                String name = coreMerchantDao.getMerchantName(transaction.getMerchantId());
                
                response.put("responsecode", "00");
                response.put("responsemessage", "found");
                response.put("data", transaction);
                response.put("merchantName", name);

                return Response.status(Response.Status.OK).entity(response).build();

            }
        } catch (Exception ex) {
            Logger.getLogger(ChargeApiController.class.getName()).log(Level.SEVERE, null, ex);

            response.put("responsecode", "RR");
            response.put("responsemessage", "Generic Error");
        }

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "/list")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getTransactions(@QueryParam(value = "range") String range,
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "merchantId") String merchantId,
            @QueryParam(value = "productmid") long productmid,
            @QueryParam(value = "searchString") String search) {

        try {

            Date startDate = null, endDate = null;
            if (range != null && !range.equalsIgnoreCase("")) {

                try {
                    String[] splitDate = range.split("-");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    startDate = dateFormat.parse(splitDate[0]);
                    endDate = dateFormat.parse(splitDate[1]);
                } catch (ParseException pe) {
                    startDate = endDate = null;
                }

            }
            Page<ChargeBack> chargebacks = chargeBackDao.find(start, length, startDate, endDate, null, 
                    merchantId, search, productmid, null);
            PageResult pageResult = new PageResult(chargebacks.getContent(), chargebacks.getCount(), chargebacks.getCount());

            return Response.status(Response.Status.OK).entity(pageResult).build();
        } catch (DatabaseException ex) {
            Logger.getLogger(ChargeApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.status(Response.Status.OK).entity(new PageResult()).build();
    }

    @Path(value = "/log")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response logChargeBack(@Valid ChargeBackRequest request) {

        Map<String, Object> response = new HashMap<>();

        try {

            String reference = request.getReference();

            Product product = productDao.findByKey("name", request.getProduct());

            String searchParam = "%" + reference + "%";
            
            TransactionViewModel transaction;

            List<TransactionViewModel> transactions = transactionDao.findMiniTransaction("flwTxnReference", searchParam);

            if (transactions == null || transactions.isEmpty()) {

                transactions = transactionDao.findMiniTransaction("rrn", searchParam);

                if (transactions == null || transactions.isEmpty()) {
                    response.put("responsecode", "05");
                    response.put("responsemessage", "transaction record not found");

                    return Response.status(Response.Status.OK).entity(response).build();
                }

                transaction = transactions.get(0);
            }else{

                transaction = transactions.get(0);
            }

            
//            transaction = transactions.get(0);
            
            if(!"00".equals(transaction.getResponseCode())){
                
                response.put("responsecode", "06");
                response.put("responsemessage", "Chargeback Cannot be logged on failed transaction");
                return Response.status(Response.Status.OK).entity(response).build();
            }

            long merchantId = 0;

            ChargeBackM chargeBackM = new ChargeBackM();
            
            if ("rave".equalsIgnoreCase(product.getName())) {

                RaveTransactionWH raveTransactionWH = raveTransactionWHDao.findByKey("flutterReference", reference);

                if(raveTransactionWH.getAccountid() != null)
                        merchantId = Long.parseLong(raveTransactionWH.getAccountid());
                    
                    Long parentId = raveTransactionWH.getParent();
                    
                    if(raveTransactionWH.getAccountid() != null && !"".equalsIgnoreCase(raveTransactionWH.getAccountid()))
                        chargeBackM.setProductMid(Long.parseLong(raveTransactionWH.getAccountid()));
                        
                    if(parentId == null || parentId == 0L)
                        chargeBackM.setProductParentMid(merchantId);
                    else
                        chargeBackM.setProductParentMid(parentId);
                    
            } if ("flutterwave core".equalsIgnoreCase(product.getName())) {

                CoreTransaction coreTransaction = coreTransactionDao.findByKey("transactionReference", reference);

                if (coreTransaction != null) {
                    CoreMerchant coreMerchant = coreMerchantDao.findByKey(Global.TOKEN_STRING_C, coreTransaction.getMerchant());

//                    if (coreMerchant == null) {
//                        coreMerchant = coreMerchantDao.findByKey("liveToken", coreTransaction.getMerchant());
//                    }

                    if (coreMerchant != null) {
                        merchantId = coreMerchant.getId();
                    }
                }
            }

            List<ChargeBackM> chargebacks = chargeBackMDao.find("flwReference", reference);
            
            if(chargebacks != null && !chargebacks.isEmpty()){
                ChargeBackM cbM = chargebacks.stream().filter(x -> x.getStatus() == Status.PENDING).findFirst().orElse(null);
                
                if(cbM != null){
                    response.put("responsecode", "01");
                    response.put("responsemessage", "A charge back has been logged on this transaction");

                    return Response.status(Response.Status.OK).entity(response).build();
                }
            
            }
            
            ChargeBack cb = chargeBackDao.findByKey("flwReference", transaction.getFlwTxnReference());

            if (cb != null) {

                response.put("responsecode", "01");
                response.put("responsemessage", "A charge back has been logged on this transaction");

                return Response.status(Response.Status.OK).entity(response).build();
            }

            User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

            chargeBackM.setAmount(transaction.getAmount());
            chargeBackM.setAuthenticationType(transaction.getAuthenticationModel());
            chargeBackM.setCurrency(transaction.getTransactionCurrency());
            chargeBackM.setFlwReference(transaction.getFlwTxnReference());
            chargeBackM.setCreatedBy(user);
            chargeBackM.setCreatedOn(new Date());
            chargeBackM.setProduct(product);
            chargeBackM.setStatus(Status.PENDING);
            chargeBackM.setChargeBackStatus(ChargeBackStatus.NONE);
            chargeBackM.setMerchantId(transaction.getMerchantId());
            chargeBackM.setProductMid(merchantId);
//            chargeBackM.setProductParentMid(parentId);

            chargeBackMDao.create(chargeBackM);

             Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("cchargeupdated", true);
            session.setAttribute("cchargeupdatedm", true);

            response.put("responsecode", "00");
            response.put("responsemessage", "successful");

        } catch (DatabaseException ex) {
            Logger.getLogger(ChargeApiController.class.getName()).log(Level.SEVERE, null, ex);

            response.put("responsecode", "RR");
            response.put("responsemessage", "Generic Error");
        }

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "/report")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getChargebackReport(@QueryParam(value = "product") String product, @QueryParam(value = "merchant") long merchantId,
            @QueryParam(value = "range") String range, @QueryParam(value = "currency") String currency, @QueryParam(value = "grouping") String grouping) throws DatabaseException {

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        List report = chargeBackDao.getReportSummary(startDate, endDate, product, 0, currency, merchantId);

        double total = 0;
        Long totalVolume = 0L;

        switch (grouping.toLowerCase()) {
            case "daily": {
                Map<Date, Double> data = new TreeMap<>();
                Map<Date, Long> volume = new TreeMap<>();

                Map<String, Double> productValue = new HashMap<>();
                Map<String, Long> productVolume = new HashMap<>();

                for (Object obj : report) {

                    Double value = 0.0;

                    Object[] reportElement = (Object[]) obj;
//                    
                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]);
                    }

                    amount = Utility.convertTo2DP(amount);
                    
                    Date date = (Date) (reportElement[1]);
                    Long count = (Long) ((BigInteger) reportElement[2] == null ? 0 : ((BigInteger) reportElement[2]).longValue());
                    String productS = (String) (reportElement[4]);
//                    String d = dateFormat.format(date);
                    Long volumeD = 0L;

                    value = data.getOrDefault(date, 0.0);
                    volumeD = volume.getOrDefault(date, 0L);

                    value += Utility.convertTo2DP(amount);
                    volumeD += count;

                    long productCount = productVolume.getOrDefault(productS, 0L);
                    productCount += count;
                    productVolume.put(productS, productCount);

                    double productV = productValue.getOrDefault(productS, 0.0);
                    productV += amount;
                    productValue.put(productS, productV);

                    total += amount;
                    totalVolume += count;

                    volume.put(date, volumeD);
                    data.put(date, value);
                }

                List<Product> products = productDao.findAll().stream().filter(x -> x.isEnabled()).collect(Collectors.toList());

                products.stream().filter(x -> !productVolume.keySet().stream().anyMatch(x.getName()::equalsIgnoreCase)).forEach(x -> {

                    productVolume.put(x.getName(), 0L);
                    productValue.put(x.getName(), 0.0);
                });

                Map<String, Object> values = new HashMap<>();
                values.put("value", data);
                values.put("volume", volume);
                values.put("total", total);
                values.put("totalVolume", totalVolume);
                values.put("productVolume", productVolume);
                values.put("productValue", productValue);

                return Response.status(Response.Status.OK).entity(values).build();
            }

            case "weekly": {
                Map<String, Double> data = new TreeMap<>();
                Map<String, Long> volume = new TreeMap<>();

                Map<String, Double> productValue = new HashMap<>();
                Map<String, Long> productVolume = new HashMap<>();

                int firstWeek = getWeek(startDate);

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

//                    Long volumnD;
                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]) == null ? 0 : ((Double) reportElement[0]);
                    }

                    Date date = (Date) (reportElement[1]);
                    Long count = (Long) ((BigInteger) reportElement[2] == null ? 0 : ((BigInteger) reportElement[2]).longValue());
                    String productS = (String) (reportElement[4]);

                    amount = Utility.convertTo2DP(amount);
                    Double map;

                    int currentWeek = (getWeek(date) / firstWeek) + (getWeek(date) % firstWeek);
                    map = data.getOrDefault("week " + currentWeek, 0.0);

                    Long mapVolume;

                    mapVolume = volume.getOrDefault("week " + currentWeek, 0L);
                    map += amount;

                    long productCount = productVolume.getOrDefault(productS, 0L);
                    productCount += count;
                    productVolume.put(productS, productCount);

                    double productV = productValue.getOrDefault(productS, 0.0);
                    productV += amount;
                    productValue.put(productS, productV);

                    mapVolume += count;

                    totalVolume += count;

                    total += amount;
                    volume.put("week " + currentWeek, mapVolume);

                    data.put("week " + currentWeek, map);
                }

                List<Product> products = productDao.findAll().stream().filter(x -> x.isEnabled()).collect(Collectors.toList());

                products.stream().filter(x -> !productVolume.keySet().stream().anyMatch(x.getName()::equalsIgnoreCase)).forEach(x -> {

                    productVolume.put(x.getName(), 0L);
                    productValue.put(x.getName(), 0.0);
                });

                Map<String, Object> values = new HashMap<>();
                values.put("value", data);
                values.put("volume", volume);
                values.put("total", total);
                values.put("totalVolume", totalVolume);
                values.put("productVolume", productVolume);
                values.put("productValue", productValue);

                return Response.status(Response.Status.OK).entity(values).build();

            }

            case "monthly": {

                Map<String, Double> data = new TreeMap<>();
                Map<String, Long> volume = new TreeMap<>();

                Map<String, Double> productValue = new HashMap<>();
                Map<String, Long> productVolume = new HashMap<>();

                SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]) == null ? 0 : ((Double) reportElement[0]);
                    }
                    
                    amount = Utility.convertTo2DP(amount);
                    
                    Date date = (Date) (reportElement[1]);
                    Long count = (Long) ((BigInteger) reportElement[2] == null ? 0 : ((BigInteger) reportElement[2]).longValue());
                    String productS = (String) (reportElement[4]);

                    String d = dateFormat.format(date);
                    Double map;
                    Long mapVolume;

                    map = data.getOrDefault(d, 0.0);
                    mapVolume = volume.getOrDefault(d, 0L);

                    mapVolume += count;

                    map += Utility.convertTo2DP(amount);

                    long productCount = productVolume.getOrDefault(productS, 0L);
                    productCount += count;
                    productVolume.put(productS, productCount);

                    double productV = productValue.getOrDefault(productS, 0.0);
                    productV += amount;
                    productValue.put(productS, productV);

                    totalVolume += count;
                    total += amount;

                    volume.put(d, mapVolume);
                    data.put(d, map);
                }

                List<Product> products = productDao.findAll().stream().filter(x -> x.isEnabled()).collect(Collectors.toList());

                products.stream().filter(x -> !productVolume.keySet().stream().anyMatch(x.getName()::equalsIgnoreCase)).forEach(x -> {

                    productVolume.put(x.getName(), 0L);
                    productValue.put(x.getName(), 0.0);
                });

                Map<String, Object> values = new HashMap<>();
                values.put("value",data);
                values.put("volume", volume);
                values.put("total", Utility.convertTo2DP(total));
                values.put("totalVolume", totalVolume);
                values.put("productVolume", productVolume);
                values.put("productValue", productValue);

                return Response.status(Response.Status.OK).entity(values).build();
            }

            case "quarterly": {
                Map<String, Double> data = new TreeMap<>();
                Map<String, Long> volume = new TreeMap<>();

                Map<String, Double> productValue = new HashMap<>();
                Map<String, Long> productVolume = new HashMap<>();

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]) == null ? 0 : ((Double) reportElement[0]);
                    }

                    amount = Utility.convertTo2DP(amount);
                    
                    Date date = (Date) (reportElement[1]);
                    Long count = (Long) ((BigInteger) reportElement[2] == null ? 0 : ((BigInteger) reportElement[2]).longValue());
                    String productS = (String) (reportElement[4]);

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(startDate);

                    String d = "Quarter " + getQuarter(calendar.get(Calendar.YEAR), date);
                    Double map;

                    Long mapVolume;

                    map = data.getOrDefault(d, 0.0);
                    mapVolume = volume.getOrDefault(d, 0L);

                    long productCount = productVolume.getOrDefault(productS, 0L);
                    productCount += count;
                    productVolume.put(productS, productCount);

                    double productV = productValue.getOrDefault(productS, 0.0);
                    productV += Utility.convertTo2DP(amount);
                    productValue.put(productS, productV);

                    mapVolume += count;
                    map += Utility.convertTo2DP(amount);

                    totalVolume += count;
                    total += Utility.convertTo2DP(amount);

                    volume.put(d, mapVolume);
                    data.put(d, map);
                }

                List<Product> products = productDao.findAll().stream().filter(x -> x.isEnabled()).collect(Collectors.toList());

                products.stream().filter(x -> !productVolume.keySet().stream().anyMatch(x.getName()::equalsIgnoreCase)).forEach(x -> {

                    productVolume.put(x.getName(), 0L);
                    productValue.put(x.getName(), 0.0);
                });

                Map<String, Object> values = new HashMap<>();
                values.put("value", data);
                values.put("volume", volume);
                values.put("total", total);
                values.put("totalVolume", totalVolume);
                values.put("productVolume", productVolume);
                values.put("productValue", productValue);

                return Response.status(Response.Status.OK).entity(values).build();
            }

            case "yearly": {

                Map<String, Double> data = new TreeMap<>();
                Map<String, Long> volume = new TreeMap<>();

                Map<String, Double> productValue = new HashMap<>();
                Map<String, Long> productVolume = new HashMap<>();

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");

                for (Object obj : report) {

                    Object[] reportElement = (Object[]) obj;

                    Double amount;
                    try {
                        amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
                    } catch (Exception ex) {
                        amount = ((Double) reportElement[0]) == null ? 0 : ((Double) reportElement[0]);
                    }
                    amount = Utility.convertTo2DP(amount);
                    
                    Date date = (Date) (reportElement[1]);
                    Long count = (Long) ((BigInteger) reportElement[2] == null ? 0 : ((BigInteger) reportElement[2]).longValue());
                    String productS = (String) (reportElement[4]);

                    String d = dateFormat.format(date);
                    Double map;

//                    map = (TreeMap<String, Double>)data.getOrDefault(d, new TreeMap<>());
                    Long mapVolume;

                    map = data.getOrDefault(d, 0.0);
                    mapVolume = volume.getOrDefault(d, 0L);

                    long productCount = productVolume.getOrDefault(productS, 0L);
                    productCount += count;
                    productVolume.put(productS, productCount);

                    double productV = productValue.getOrDefault(productS, 0.0);
                    productV += amount;
                    productValue.put(productS, productV);

                    mapVolume += count;
                    map += amount;

                    totalVolume += count;
                    total += amount;

                    volume.put(d, mapVolume);
                    data.put(d, map);
                }

                List<Product> products = productDao.findAll().stream().filter(x -> x.isEnabled()).collect(Collectors.toList());

                products.stream().filter(x -> !productVolume.keySet().stream().anyMatch(x.getName()::equalsIgnoreCase)).forEach(x -> {

                    productVolume.put(x.getName(), 0L);
                    productValue.put(x.getName(), 0.0);
                });

                Map<String, Object> values = new HashMap<>();
                values.put("value", data);
                values.put("volume", volume);
                values.put("total", total);
                values.put("totalVolume", totalVolume);
                values.put("productVolume", productVolume);
                values.put("productValue", productValue);

                return Response.status(Response.Status.OK).entity(values).build();
            }
        }

        return null;
    }

    private int getWeek(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int week = calendar.get(Calendar.WEEK_OF_YEAR);

        return week;
    }

    /**
     * year - start year of quarter
     *
     * @param year
     * @param date
     * @return
     */
    public int getQuarter(int year, Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int month = calendar.get(Calendar.MONTH);

        int yearInt = calendar.get(Calendar.YEAR);

        int value = 0;

        if (yearInt > year) {
            value += yearInt - year;
        }

        if (month <= 3) {
            return value += 1;
        }

        if (month <= 6) {
            return value += 2;
        }

        if (month <= 9) {
            return value += 3;
        } else {
            return value += 4;
        }
    }

    @Path(value = "/upload")
    @POST
    @Consumes("multipart/form-data")
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response uploadChargeBacks(@Valid MultipartFormDataInput request) throws DatabaseException, IOException {

        Map<String, Object> response = new HashMap<>();

//        String range = request.getRange();
        Map<String, InputPart> map = request.getFormData();

        InputStream inputStream = map.get("file").getBody(InputStream.class, null);

        if (inputStream == null) {
            response.put("responsecode", "01");
            response.put("responsemessage", "No input file provided");
            return Response.status(Response.Status.OK).entity(response).build();
        }
        
//        InputStream inputStream = map.get("file").getBody(String.class, null);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;

        Object userString = SecurityUtils.getSubject().getPrincipal();
        User user = userDao.findByKey("email", userString + "");

        int countUpload = 0, countDuplicate = 0;

        int i = 0;

        Map<String, String> dataMap = new HashMap<>();

        List<String> notFound = new ArrayList<>();
        List<TransactionViewModel> transactions = new ArrayList<>();

        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        while ((line = reader.readLine()) != null) {

            if (i++ == 0) {
                continue;
            }

            final String[] data = line.split(",");

            if (data.length < 2) {
                continue;
            }

            String productName = data[0].replaceAll("\"", "");
            String reference = data[1].replaceAll("\"", "");

            TransactionViewModel transaction = findTransactionModel(reference);

            if (transaction == null) {
                notFound.add(" " + reference);
                continue;
            }

            JsonObjectBuilder builder = Json.createObjectBuilder();
            builder.add("product", productName);
            builder.add("reference", reference);
            arrayBuilder.add(builder);

            transactions.add(transaction);
        }

        response.put("responsecode", "00");
        response.put("responsemessage", "Successful");
        response.put("found", arrayBuilder.build().toString());
        response.put("transactions", transactions);
        response.put("notfound", notFound);

        return Response.status(Response.Status.OK).entity(response).build();
    }

    private Transaction findTransaction(String reference) throws DatabaseException {

        Transaction transaction;


            transaction = transactionDao.findByKey("flwTxnReference", reference);

            if (transaction == null) {
                transaction = transactionDao.findByKey("rrn", reference);

                if (transaction == null) {
                    String searchParam = "%" + reference + "%";
                    List<Transaction> transactions = transactionDao.findByKeyLike("rrn", searchParam);

                    if(transactions != null && !transactions.isEmpty())
                        transaction = transactions.get(0);
                }
            }

        return transaction;
    }
    
    private TransactionViewModel findTransactionModel(String reference) throws DatabaseException {

        TransactionViewModel transaction = null;


        List<TransactionViewModel> transactions = transactionDao.findMiniTransaction("flwTxnReference", "%"+reference+"%");

        if (transactions == null || transactions.isEmpty()) {
            
            transactions = transactionDao.findMiniTransaction("rrn", "%"+reference+"%");

            if(transactions != null && !transactions.isEmpty())
                transaction = transactions.get(0);
            
        }

        return transaction;
    }

    @Path(value = "/logmulti")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response logMutipleChargeBack(@Valid ChargeBackRequest request) {

        Map<String, Object> response = new HashMap<>();

        try {

            String ref = request.getReference();

            JsonReader jsonReader = Json.createReader(new StringReader(ref));
            JsonArray jsonArray = jsonReader.readArray();
            
            List<ChargeBackM> chargeBacks = new ArrayList<>();
            
            int duplicate = 0, total = 0;

            for (JsonValue value : jsonArray) {

                JsonObject jsonObject = (JsonObject) value;
                
                String reference = jsonObject.getString("reference", "");
                
                String product = jsonObject.getString("product", "").replaceAll("\"", "");
                
                TransactionViewModel transaction;

                List<TransactionViewModel> transactions = transactionDao.findMiniTransaction("flwTxnReference", reference);

                
                if(transactions == null || transactions.isEmpty()){
                    
                    transactions = transactionDao.findMiniTransaction("rrn", reference);
                    
                    if(transactions == null || transactions.isEmpty()){
                        response.put("responsecode", "05");
                        response.put("responsemessage", "transaction record not found");

                        return Response.status(Response.Status.OK).entity(response).build();
                    }
                }
                
                transaction = transactions.get(0);
                
//                if (transaction == null) {
//                    String searchParam = "" + reference + "%";
//
//                    transactions = transactionDao.findMiniTransaction("rrn", searchParam);
//
//                    if (transactions == null || transactions.isEmpty()) {
//                        response.put("responsecode", "05");
//                        response.put("responsemessage", "transaction record not found");
//
//                        return Response.status(Response.Status.OK).entity(response).build();
//                    }
//
//                    transaction = transactions.get(0);
//                }

                long merchantId = 0;

//                merchantId = transaction.getMerchantId();
                
//                if ("rave".equalsIgnoreCase(product)) {
//
//                    RaveTransaction raveTransaction = raveTransactionDao.findByKey("flutterReference", reference);
//
//                    if (raveTransaction != null) {
//                        merchantId = raveTransaction.getMerchant().getId();
//                    }
//                } else if ("moneywave".equalsIgnoreCase(product)) {
//
//                    MoneywaveTransaction moneywaveTransaction = moneywaveTransactionDao.findByKey("flutterReference", reference);
//
//                    if (moneywaveTransaction != null) {
//                        merchantId = moneywaveTransaction.getMerchant().getId();
//                    }
//                } 

//                else if ("flutterwave core".equalsIgnoreCase(product) || "core".equalsIgnoreCase(product)) {


                        CoreMerchant coreMerchant = coreMerchantDao.findByKey("merchantID", transaction.getMerchantId());

                        if (coreMerchant != null) {
                            merchantId = coreMerchant.getId();
                        }
//                    }
//                }
                
                ChargeBack cb = chargeBackDao.findByKey("flwReference", transaction.getFlwTxnReference());

                if (cb != null) {
                    
                    duplicate++;
                    continue;
                }
                
                List<ChargeBackM> chargeBackMs = chargeBackMDao.find("flwReference", reference);
                
                if(chargeBackMs != null && !chargeBackMs.isEmpty()){
                    
                    ChargeBackM cbm = chargeBackMs.stream().filter(x -> x.getApprovedOn() != null).findFirst().orElse(null);
                    
                    if(cbm != null){
                        duplicate++;
                        continue;
                    }
                }

                User user = userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal() + "");

                Product product1 = null;
                
                ChargeBackM chargeBackM = new ChargeBackM();
                
                if("144163".equalsIgnoreCase(transaction.getMerchantId())){
                    
                    product1 = productDao.findByKey("name", "rave");
                    
                    if(product1 == null){
                        product1 = productDao.findByKey("name", "ravepay");
                    }
                    
                    RaveTransactionWH raveTransactionWH = raveTransactionWHDao.findByKey("flutterReference", transaction.getFlwTxnReference());
                    
                    if(raveTransactionWH.getAccountid() != null || !"".equalsIgnoreCase(product))
                        merchantId = Long.parseLong(raveTransactionWH.getAccountid());
                    
                    Long parentId = raveTransactionWH.getParent();
                    
                    if(raveTransactionWH.getAccountid() != null && !"".equalsIgnoreCase(raveTransactionWH.getAccountid()))
                        chargeBackM.setProductMid(Long.parseLong(raveTransactionWH.getAccountid()));
                        
                    if(parentId == null)
                        chargeBackM.setProductParentMid(merchantId);
                    else
                        chargeBackM.setProductParentMid(parentId);
                }else if("144091".equalsIgnoreCase(transaction.getMerchantId())){
                    
                    product1 = productDao.findByKey("name", "moneywave");
                } 
                
                if(product1 == null){
                    
                    product1 = productDao.findByKey("name", "core");
                    
                    if(product1 == null)
                        product1 = productDao.findByKey("name", "flutterwave core");
                }
                
                chargeBackM.setAmount(transaction.getAmount());
                chargeBackM.setAuthenticationType(transaction.getAuthenticationModel());
                chargeBackM.setCurrency(transaction.getTransactionCurrency());
                chargeBackM.setFlwReference(transaction.getFlwTxnReference());
                chargeBackM.setCreatedBy(user);
                chargeBackM.setCreatedOn(new Date());
                chargeBackM.setProduct(product1);
                chargeBackM.setStatus(Status.PENDING);
                chargeBackM.setChargeBackStatus(ChargeBackStatus.NONE);
                chargeBackM.setMerchantId(transaction.getMerchantId());
                chargeBackM.setProductMid(merchantId);

                chargeBacks.add(chargeBackM);
                
                total++;
            }

            if(!chargeBacks.isEmpty())
                chargeBackMDao.create(chargeBacks);

            Session session = SecurityUtils.getSubject().getSession();

            session.setAttribute("cchargeupdated", true);
            session.setAttribute("cchargeupdatedm", true);
            
            response.put("responsecode", "00");
            response.put("responsemessage", "successful");
            response.put("total", total);
            response.put("duplicate", duplicate);

        } catch (DatabaseException ex) {
            Logger.getLogger(ChargeApiController.class.getName()).log(Level.SEVERE, null, ex);

            response.put("responsecode", "RR");
            response.put("responsemessage", "Generic Error");
        }

        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @Path(value = "/summary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getSummary(@QueryParam(value = "range") String range,
            @QueryParam(value = "merchantId") String merchantId,
            @QueryParam(value = "productmid") long productmid,
            @QueryParam(value = "product") String product,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "search") String search) throws DatabaseException{
        
        Date startDate = null, endDate = null;
        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        Map checkbacks = chargeBackDao.getSummary(startDate, endDate, product, merchantId, currency, productmid, null);
        
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        startDate = calendar.getTime();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        endDate = calendar.getTime();
        
        Map monthChargebacks = chargeBackDao.getSummary(startDate, endDate, product, merchantId, currency, productmid, null);
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM, yyyy");
        
        Map<String, Object> map = new HashMap<>();
        map.put("total", checkbacks);
        map.put("monthly", monthChargebacks);
        map.put("month", dateFormat.format(new Date()));
        
        return Response.status(Response.Status.OK).entity(map).build();
        
    }

    @Path(value = "/clist")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getTransaction(
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "merchantId") String merchantId,
            @QueryParam(value = "productmid") long productmid,
            @QueryParam(value = "product") String product,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "search") String search){
        
        Date startDate = null, endDate = null;
        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        Page<ChargeBack> page = null;
        
        try {
            page = chargeBackDao.find(start, length, startDate, endDate, null, merchantId, Utility.emptyToNull(currency), 0, null);
        } catch (DatabaseException ex) {
            Logger.getLogger(ChargeApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(page == null || page.getContent() == null || page.getCount() <= 0L ){
            
            return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
        }
        
        PageResult<ChargeBackViewModel> pageResult = new PageResult<>();
        
        pageResult.setData(page.getContent().stream().map((ChargeBack cb) -> {
            
            return ChargeBackViewModel.from(cb);
        }).collect(Collectors.<ChargeBackViewModel>toList()));
        
        pageResult.setRecordsFiltered(page.getCount());
        pageResult.setRecordsTotal(page.getCount());
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    
    @Path(value = "/bank/all")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getBankChargeBacks(
            @QueryParam(value = "start") int start,
            @QueryParam(value = "length") int length,
            @QueryParam(value = "draw") int draw,
            @QueryParam(value = "range") String range,
            @QueryParam(value = "merchantId") String merchantId,
            @QueryParam(value = "productmid") long productmid,
            @QueryParam(value = "product") String product,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "search") String search){
        
        Date startDate = null, endDate = null;
        
        
        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }
        
        Session session =  SecurityUtils.getSubject().getSession();
        Object productIdObj = session.getAttribute("productId");
        
        if(productIdObj == null)
            return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
        
        String productId = productIdObj.toString();
        
        Object productNameObj = session.getAttribute("product");
        
        if(productNameObj == null)
            return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
        
        String productName = productIdObj.toString();
        
        Page<ChargeBack> page = null;
        
        try {
            
//            long productIdLong = Long.parseLong(productId);
            
            page = chargeBackDao.find(start, length, startDate, endDate, null,
                    Utility.emptyToNull(merchantId), Utility.emptyToNull(currency), productmid, productId);
            
        } catch (DatabaseException ex) {
            Logger.getLogger(ChargeApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(page == null || page.getContent() == null || page.getCount() <= 0L ){
            
            return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
        }
        
        PageResult<ChargeBackViewModel> pageResult = new PageResult<>();
        
        pageResult.setData(page.getContent().stream().map((ChargeBack cb) -> {
            
            return ChargeBackViewModel.from(cb);
        }).collect(Collectors.<ChargeBackViewModel>toList()));
        
        pageResult.setRecordsFiltered(page.getCount());
        pageResult.setRecordsTotal(page.getCount());
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    
    @Path(value = "/bank/summary")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getBankSummary(@QueryParam(value = "range") String range,
            @QueryParam(value = "merchantId") String merchantId,
            @QueryParam(value = "productmid") long productmid,
            @QueryParam(value = "product") String product,
            @QueryParam(value = "currency") String currency,
            @QueryParam(value = "search") String search) throws DatabaseException{
        
        Date startDate = null, endDate = null;
        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
        
        Session session =  SecurityUtils.getSubject().getSession();
        Object productIdObj = session.getAttribute("productId");
        
        if(productIdObj == null)
            return Response.status(Response.Status.OK).entity(new PageResult<>()).build();
        
        String productId = productIdObj.toString();
        
        Object productNameObj = session.getAttribute("product");
        
        if(productNameObj == null)
            return Response.status(Response.Status.OK).entity(new HashMap<>()).build();
        
        String productName = productIdObj.toString();
        
        Map checkbacks = chargeBackDao.getSummary(startDate, endDate, product, merchantId, currency, productmid, productId );
        
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        startDate = calendar.getTime();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        endDate = calendar.getTime();
        
        Map monthChargebacks = chargeBackDao.getSummary(startDate, endDate, product, merchantId, currency, productmid, productId);
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM, yyyy");
        
        Map<String, Object> map = new HashMap<>();
        map.put("total", checkbacks);
        map.put("monthly", monthChargebacks);
        map.put("month", dateFormat.format(new Date()));
        
        return Response.status(Response.Status.OK).entity(map).build();
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.flutterwave.flutter.clientms.util.Utility;
import javax.servlet.http.Part;
import javax.ws.rs.FormParam;
import org.hibernate.validator.constraints.NotBlank;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

/**
 *
 * @author emmanueladeyemi
 */
public class ReportModel {

    /**
     * @return the wallet
     */
    public byte[] getWallet() {
        return wallet;
    }

    /**
     * @param wallet the wallet to set
     */
    public void setWallet(byte[] wallet) {
        this.wallet = wallet;
    }

    /**
     * @return the card
     */
    public byte[] getCard() {
        return card;
    }

    /**
     * @param card the card to set
     */
    public void setCard(byte[] card) {
        this.card = card;
    }
    /**
     * @return the sourceType
     */
    public Utility.ReportSource getSourceType() {
        return sourceType;
    }

    /**
     * @param sourceType the sourceType to set
     */
    public void setSourceType(Utility.ReportSource sourceType) {
        this.sourceType = sourceType;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the range
     */
    public String getRange() {
        return range;
    }

    /**
     * @param range the range to set
     */
    public void setRange(String range) {
        this.range = range;
    }

    /**
     * @return the file
     */
    public byte[] getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(byte[] file) {
        this.file = file;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
    @NotBlank
    @FormParam(value = "type")
    private String type;
    @NotBlank
    @FormParam(value = "range")
    private String range;
    @FormParam(value = "file")
    @PartType("application/octet-stream")
    private byte[] file;
    @PartType("application/octet-stream")
    private byte[] wallet;
    @PartType("application/octet-stream")
    private byte[] card;
    @FormParam(value = "currency")
    private String currency;
    @FormParam(value = "product")
    private String product;
    private Utility.ReportSource sourceType;
}

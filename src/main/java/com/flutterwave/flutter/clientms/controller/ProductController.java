/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.ProductMDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.maker.ProductM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "productController")
@RequestScoped
public class ProductController {

    @EJB
    private ProductDao productDao;
    @EJB
    private UserDao userDao;
    @EJB
    private LogService logService;
    @EJB
    private ProductMDao productMDao;
    
    private Product product;
    private PaginationHelper pagination, paginationUnauth;
    private DataModel dataModel, unauthdataModel;
    
    public Product getProduct() {

        if (product == null) {
            product = new Product();
        }

        return product;
    }

    public void setProduct(Product product) {

        this.product = product;
    }

    public String prepareCreate() {

        Log log = new Log();
        log.setAction("creating product");
        log.setCreatedOn(new Date());
        log.setDescription("creating product started");
        log.setLevel(LogLevel.Info);
        log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.STARTED);
        log.setStatus(LogStatus.SUCCESSFUL);

        product = null;

        return "/product/create";
    }

    public String create() {

        try {
            Product p = productDao.findByKey("name", product.getName());

            if (p != null) {

                Log log = new Log();
                log.setAction("creating product");
                log.setCreatedOn(new Date());
                log.setDescription("created product failed");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "Product with name " + p.getName() + " exists");

                JsfUtil.addErrorMessage("Product with name  " + product.getName() + " exists on the platform");
                return "/product/create";
            }

            product.setCreatedOn(new Date());
            product.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));

            //productDao.create(product);
            ProductM productM = new ProductM();
            productM.setCreatedBy(product.getCreatedBy());
            productM.setCreatedOn(product.getCreatedOn());
            productM.setLaunchDate(product.getLaunchDate());
            productM.setStagingUrl(product.getStagingUrl());
            productM.setName(product.getName());
            productM.setDescription(product.getDescription());
            productM.setLiveUrl(product.getLiveUrl());
            productM.setStatus(Status.PENDING);
            productM.setEnabled(true);
            productM.setInitiationDate(product.getInitiationDate());
            
            productMDao.create(productM);

            Log log = new Log();
            log.setAction("creating product");
            log.setCreatedOn(new Date());
            log.setDescription("created product successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            JsfUtil.addSuccessMessage("Product " + product.getName() + " has been created successfully and sent for approval");
            
            logService.log(log);

            product = new Product();
            
            return "/product/create";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);

            try {
                Log log = new Log();
                log.setAction("create product");
                log.setCreatedOn(new Date());
                log.setDescription("create product");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return "/product/create";
    }
    
    public String view(long id){
        
        try {
            if (id <= 0) {
                JsfUtil.addErrorMessage("product with id not found");
                
                Log log = new Log();
                log.setAction("update product with id " + id);
                log.setCreatedOn(new Date());
                log.setDescription("product with id not found");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);
                
                logService.log(log, "product with id " + id + " not found ");
                
                return "/product/list";
            }
            
            product = productDao.find(id);
            
            if (product == null) {
                JsfUtil.addErrorMessage("product with id not found");
                return "/product/list";
            }
            
            Log log = new Log();
            log.setAction("previewing product with name " + product.getName());
            log.setCreatedOn(new Date());
            log.setDescription(" product previewing");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);
            
//            Page<RaveMerchant> raveMerchants = raveMerchantDao.find(0, 20);
//            
//            moneywaveMerchantDao.find(0, 10);
            
            logService.log(log);
            
            return "/product/view";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            
            JsfUtil.addErrorMessage("Please try again later");
        }
            
        return "/product/list";
    }

    public String prepareUpdate(String id) {

        try {
            
            //Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

            //id = params.get("editParam");
            
             if (id == null || "0".equalsIgnoreCase(id)) {
                JsfUtil.addErrorMessage("product with id not found");

                Log log = new Log();
                log.setAction("update product with id " + id);
                log.setCreatedOn(new Date());
                log.setDescription("product with id not found");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "product with id " + id + " not found ");

                return "/product/list";
            }
            
            long val = Long.parseLong(id);

            product = productDao.find(val);

            if (product == null) {
                JsfUtil.addErrorMessage("product with id not found");
                return "/product/list";
            }

            Log log = new Log();
            log.setAction("update product with name " + product.getName());
            log.setCreatedOn(new Date());
            log.setDescription("updated started");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            logService.log(log);

            return "/product/edit";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);

            try {
                Log log = new Log();
                log.setAction("update product");
                log.setCreatedOn(new Date());
                log.setDescription("update product");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }

//            JsfUtil.addErrorMessage("Unable to update product at the moment, please again later");
            return "/product/edit";
        }
    }

    public String update() {

        try {
            Product p = productDao.findByKey("name", product.getName());

            if (p != null && p.getId() != product.getId()) {

                Log log = new Log();
                log.setAction("update product with id " + product.getId());
                log.setCreatedOn(new Date());
                log.setDescription("product with id not found");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
                log.setLogDomain(LogDomain.ADMIN);
                log.setLogState(LogState.FINISH);

                logService.log(log, "product with id " + product.getId() + " not found ");

                JsfUtil.addErrorMessage("product with name " + product.getName() + " exists");
                return "/product/all";
            }
            
            List<ProductM>  list = productMDao.find("modelId", product.getId());
            
            if(list != null && !list.isEmpty()){
                
                ProductM pm = list.stream().filter(x->x.getApprovedOn() == null).findFirst().orElse(null);
                
                if(pm != null){
                    JsfUtil.addErrorMessage("Pending update exist on product " + product.getName() + "");
                    
                    return "/product/edit";
                }
            }
            
            p = productDao.find(product.getId());

            String before = p.toString();

            p.setName(product.getName());
            p.setStagingUrl(product.getStagingUrl());
            p.setModified(new Date());

            product.setCreatedOn(p.getCreatedOn());
//            productDao.update(p);
            ProductM productM = new ProductM();
            productM.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
            productM.setCreatedOn(new Date());
            productM.setLaunchDate(product.getLaunchDate());
            productM.setStagingUrl(product.getStagingUrl());
            productM.setName(product.getName());
            productM.setModelId(product.getId());
            productM.setLiveUrl(product.getLiveUrl());
            productM.setDescription(product.getDescription());
            productM.setInitiationDate(product.getInitiationDate());

            product = productDao.find(product.getId());
            
            productMDao.create(productM);

            Log log = new Log();
            log.setAction("update product with id " + p.getId());
            log.setCreatedOn(new Date());
            log.setDescription("former " + before + " , later: " + p.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);

            logService.log(log, "product with id " + p.getId() + " updated successfully ");

            JsfUtil.addSuccessMessage("Product has been updated and submitted for approval successfully");

            productM = new ProductM();
            product = new Product();
            
            return "/product/list";
        } catch (DatabaseException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);

            try {
                Log log = new Log();
                log.setAction("update product");
                log.setCreatedOn(new Date());
                log.setDescription("update product");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal().toString());

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }

            } catch (Exception e) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }

            JsfUtil.addErrorMessage("Unable to update product at the moment, please again later");
            return "/product/edit";
        }
    }

    public PaginationHelper getPagination() {

        if (pagination == null) {
            pagination = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return productDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(productDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }
    
    public List<Product> getAllProducts() throws DatabaseException{
    
        return productDao.findAll();
    }

    public PaginationHelper getPaginationUnauth() {

        if (paginationUnauth == null) {
            paginationUnauth = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return productMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(productMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauth;
    }

    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("productupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("productupdated", false);
        }

        if (pagination == null) {
            dataModel = getPagination().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Products");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Products");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        return dataModel;
    }
    

    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("productupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauth = null;
            session.setAttribute("productupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch All Unauth Products");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Unauth Products");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log);

        if (paginationUnauth == null) {
            unauthdataModel = getPaginationUnauth().createPageDataModel();
        }

        return unauthdataModel;
    }

    public void recreate() {
        pagination = null;
        paginationUnauth = null;
    }

    public Utility.FeeType[] getFeeType(){
        
        return Utility.FeeType.values();
    }
    
    public Utility.LimitCategory[] getLimitCategory(){
        
        return Utility.LimitCategory.values();
    }
    
    public Product getProductById(long value) throws DatabaseException{
        
        return productDao.find(value);
    }
    
    @FacesConverter("productConverter")
    public static class productConverter implements Converter{
        
        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            
            if(value == null || value.isEmpty())
               return null;
           
            try {

                ProductController productController = (ProductController) context.getApplication()
                    .getELResolver().getValue(context.getELContext(), null, "productController");

                Product p = productController.getProductById(Long.parseLong(value));
                return p;
                
            } catch (Exception ex) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return null;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            
            if(value == null)
                return null;
            
           if( ! (value instanceof Product))
               return null;
               
            Product product = (Product) value;
            
            return product.getId()+"";
        }
    }
    
    @FacesConverter("dateConverter")
    public static class DateConverter implements Converter{

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        
        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            
            if(value == null)
               return null;
           
            try {
                Date date =  dateFormat.parse(value);
                
                return date;
            } catch (ParseException ex) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return null;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            
            if(value == null)
                return null;
            
            return dateFormat.format((Date) value);
        }
    }
    
    @FacesConverter("dateConverter1")
    public static class DateConverter1 implements Converter{

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        
        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            
            if(value == null)
               return null;
           
            try {
                Date date =  dateFormat.parse(value);
                
                return date;
            } catch (ParseException ex) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return null;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            
            if(value == null)
                return null;
            
            return dateFormat.format((Date) value);
        }
    }
    
}

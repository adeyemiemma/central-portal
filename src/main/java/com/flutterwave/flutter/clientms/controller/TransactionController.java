/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.AirtimeTransactionDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationFeeDao;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CoreReportDao;
import com.flutterwave.flutter.clientms.dao.CoreTransactionDao;
import com.flutterwave.flutter.clientms.dao.CountryDao;
import com.flutterwave.flutter.clientms.dao.CurrencyDao;
import com.flutterwave.flutter.clientms.dao.ExchangeRateDao;
import com.flutterwave.flutter.clientms.dao.MerchantFeeDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionNewDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.ProviderDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionWHDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.dao.TransactionWarehouseDao;
import com.flutterwave.flutter.clientms.dao.recon.ReconTransactionDao;
import com.flutterwave.flutter.clientms.model.AirtimeTransaction;
import com.flutterwave.flutter.clientms.model.ConfigurationFee;
import com.flutterwave.flutter.clientms.model.CoreReport;
import com.flutterwave.flutter.clientms.model.CoreTransaction;
import com.flutterwave.flutter.clientms.model.Country;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.model.GenericTransaction;
import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.MoneywaveTransaction;
import com.flutterwave.flutter.clientms.model.PosSettlementModel;
import com.flutterwave.flutter.clientms.model.PosTransactionNew;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.RaveTransactionWH;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.TransactionListModel;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.model.recon.PdfExportModel;
import com.flutterwave.flutter.clientms.model.recon.ReconTransaction;
import com.flutterwave.flutter.clientms.service.ETopPosService;
import com.flutterwave.flutter.clientms.service.NotificationManager;
import com.flutterwave.flutter.clientms.service.SimpleCache;
import com.flutterwave.flutter.clientms.service.SmtpMailSender;
import com.flutterwave.flutter.clientms.util.CsvUtil;
import com.flutterwave.flutter.clientms.util.FillManager;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.Layouter1;
import com.flutterwave.flutter.clientms.util.MoneywaveReportBean;
import com.flutterwave.flutter.clientms.util.PdfUtilExport;
import com.flutterwave.flutter.clientms.util.ReconPdfUtil;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.CoreReportViewModel;
import com.flutterwave.flutter.clientms.viewmodel.FailureAnalysisModel;
import com.flutterwave.flutter.clientms.viewmodel.FetchRRNModel;
import com.flutterwave.flutter.clientms.viewmodel.PosTransactionViewModelWeb;
import com.flutterwave.flutter.clientms.viewmodel.RaveSettlementModel;
import com.flutterwave.flutter.clientms.viewmodel.RaveSettlementViewModel;
import com.flutterwave.flutter.clientms.viewmodel.SettlementViewModel;
import com.flutterwave.flutter.clientms.viewmodel.UploadModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "transactionController")
@SessionScoped
public class TransactionController implements Serializable {

    /**
     * @return the dateRange
     */
    public String getDateRange() {
        return dateRange;
    }

    /**
     * @param dateRange the dateRange to set
     */
    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    @EJB
    private RaveTransactionWHDao raveTransactionDao;
    @EJB
    private CoreTransactionDao coreTransactionDao;
    @EJB
    private MoneywaveTransactionDao moneywaveTransactionDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private TransactionDao transactionDao;
    @EJB
    private TransactionWarehouseDao transactionWarehouseDao;
    @EJB
    private ReconTransactionDao reconTransactionDao;
    @EJB
    private ProviderDao providerDao;
    @EJB
    private CoreReportDao coreReportDao;
    @EJB
    private MerchantFeeDao merchantFeeDao;
    @EJB
    private ConfigurationFeeDao configurationFeeDao;
    @EJB
    private CountryDao countryDao;
    @EJB
    private ProductDao productDao;
    @Inject
    private Global global;
    @EJB
    private CurrencyDao currencyDao;
    @EJB
    private ExchangeRateDao exchangeRateDao;
    @EJB
    private AirtimeTransactionDao airtimeTransactionDao;
    @EJB
    private NotificationManager notificationManager;
    @EJB
    private PosTransactionNewDao posTransactionDao;
    @EJB
    private ETopPosService eTopPosService;   
    
    @Inject
    SimpleCache simpleCache;

    String token_string = Global.TOKEN_STRING; // test_token for test and live_token for live

//    @ManagedProperty(value="#{moneywaveReport}")
    private MoneywaveReportBean moneywaveReportBean;

    public UploadModel getUploadModel() {

        if (uploadModel == null) {
            uploadModel = new UploadModel();
        }

        return uploadModel;
    }

    public void setUploadModel(UploadModel uploadModel) {
        this.uploadModel = uploadModel;
    }

    private UploadModel uploadModel;

    public void setMoneywaveReportBean(MoneywaveReportBean moneywaveReportBean) {
        this.moneywaveReportBean = moneywaveReportBean;
    }

    public void downloadReceipt(long id){
        
        if(id <= 0)
            return;
        
        List<PosTransactionViewModelWeb> transactions = posTransactionDao.findForDownload(id);
        
        if(transactions == null || transactions.isEmpty())
            return;
        
        PosTransactionViewModelWeb model = transactions.get(0);
        
        InputStream inputStream = null;
        
        File fileName = null;
        
//        if(!"ETOP".equalsIgnoreCase(model.getProvider())){
        
            PosTransactionNew posTransactionNew = null;
            
            try {
                posTransactionNew = posTransactionDao.find(model.getId());
            } catch (DatabaseException ex) {
                Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            String chName = "";
            
            if(posTransactionNew != null)
                chName = posTransactionNew.getCardholder();
                
            fileName = PdfUtilExport.generatePosReceipt("Flutterwave", model, chName);

            try {
                inputStream = new FileInputStream(fileName);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
            }
                
//        }
//        else{
//            
//            inputStream = eTopPosService.downloadReceipt(model.getRefCode());
//        
//            if(inputStream == null)
//                return;
//        }   
        
       
        
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        
//        FileInputStream fileInputStream = new FileInputStream(fileName);
        ServletOutputStream out = null;
        try {
            
            response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.pdf");
            response.setContentType("application/pdf");
            out = response.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            IOUtils.copy(inputStream, out);
            
            inputStream.close();

            try{
                if(fileName != null)
                    fileName.delete();
            }catch(Exception ex){
                ex.printStackTrace();
            }
            
            out.flush();
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }
//                out.flush();
        FacesContext.getCurrentInstance().responseComplete();
        
    }
    
    public void export(String product, String range, String search, String transactionType, String transactionStatus,
            String cycle, String currency, String fraudStatus, String chargeType, String country, String merchant, Boolean isMerchantId, String paymentEntity,
            String destination, String flutterResponse, String format) {

        PageResult<GenericTransaction> pageResult = new PageResult<>();

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        List<GenericTransaction> transactions = new ArrayList<>();

        long totalCount = 0;

        if ("rave".equalsIgnoreCase(product)) {

            Page<RaveTransactionWH> raveTransaction = raveTransactionDao.search(0, 0, startDate, endDate, "any".equalsIgnoreCase(transactionType) ? null : transactionType,
                    "any".equalsIgnoreCase(fraudStatus) ? null : fraudStatus, "any".equalsIgnoreCase(chargeType) ? null : chargeType,
                    "any".equalsIgnoreCase(cycle) ? null : cycle, "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity, "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus,
                    search, country, "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency);

            totalCount += raveTransaction.getCount();

            if (raveTransaction.getContent() != null && !raveTransaction.getContent().isEmpty()) {

                transactions.addAll(raveTransaction.getContent().stream().map((RaveTransactionWH rm) -> {
                    return rm.getGeneric();
                }).collect(Collectors.<GenericTransaction>toList()));

            }
        } else if ("moneywave".equalsIgnoreCase(product)) {

            Page<MoneywaveTransaction> waveTransaction = moneywaveTransactionDao.getTransactions(0, 0, startDate, endDate,
                    "any".equalsIgnoreCase(transactionType) ? null : transactionType, "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus,
                    "any".equalsIgnoreCase(destination) ? null : destination,
                    "any".equalsIgnoreCase(paymentEntity) ? null : paymentEntity, search, "any".equalsIgnoreCase(flutterResponse) ? null : flutterResponse,
                    "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency);

            totalCount += waveTransaction.getCount();

            if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                transactions.addAll(waveTransaction.getContent().stream().map((MoneywaveTransaction rm) -> {
                    return rm.getGeneric();
                }).collect(Collectors.<GenericTransaction>toList()));
            }
        } else if ("core".equalsIgnoreCase(product) || "flutterwave core".equalsIgnoreCase(product)) {

            Page<CoreTransaction> waveTransaction = coreTransactionDao.getTransactions(0, 0, startDate, endDate,
                    "any".equalsIgnoreCase(transactionStatus) ? null : transactionStatus, search, "".equalsIgnoreCase(merchant) ? null : merchant, isMerchantId, "any".equalsIgnoreCase(currency) ? null : currency);

            totalCount += waveTransaction.getCount();

            if (waveTransaction.getContent() != null && !waveTransaction.getContent().isEmpty()) {

                transactions.addAll(waveTransaction.getContent().stream().map((CoreTransaction rm) -> {

                    GenericTransaction gt = rm.getGeneric();

                    try {
                        List<CoreMerchant> merchants = coreMerchantDao.find("" + Global.TOKEN_STRING_C, gt.getMerchant());

                        gt.setMerchant(merchants == null || merchants.isEmpty() ? null : merchants.get(0).getCompanyname());
                    } catch (Exception ex) {
                        if (ex != null) {
                            ex.printStackTrace();
                        }

                        gt.setMerchant(null);
                    }

                    return gt;
                }).collect(Collectors.<GenericTransaction>toList()));
            }
        }

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        if ("pdf".equalsIgnoreCase(format)) {
            // export to pdf here

            List<String> headers = (List) Utility.getClassFields(GenericTransaction.class);

            ServletOutputStream out = null;

            try {

                String fileName = PdfUtilExport.generatePdf(product, "Transaction Report", headers, (List) transactions);

                response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.pdf");
                response.setContentType("application/pdf");

                FileInputStream fileInputStream = new FileInputStream(fileName);
                out = response.getOutputStream();

                try {
                    IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                    out.flush();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }

//                out.flush();
                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } // end of pdf export
        else if ("csv".equalsIgnoreCase(format)) {

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
            List<String> headers = (List) Utility.getClassFields(GenericTransaction.class);

            String output = CsvUtil.writeLine(headers, (List) transactions);

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } else {

            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("TransactionRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) Utility.getClassFields(GenericTransaction.class), "Transaction Export", null);

            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) transactions);

            response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.xls");
            response.setContentType("application/vnd.ms-excel");

            ServletOutputStream out = null;

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }

    }

    public void exportPOSTransaction(String merchantId, String merchantcode, String range, String type,
            String provider, String currency, String responseCode, String bankName, String search, String format) {

        PageResult<GenericTransaction> pageResult = new PageResult<>();

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                startDate = calendar.getTime();

                endDate = dateFormat.parse(splitDate[1]);

                calendar.setTime(endDate);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
//                calendar.set(Calendar.MILLISECOND, 59);

                endDate = calendar.getTime();

            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        long totalCount = 0;

        Page<PosTransactionViewModelWeb> page = posTransactionDao.findForWebNew(0, 0, Utility.emptyToNull(merchantId), Utility.emptyToNull(merchantcode),
                Utility.emptyToNull(provider), startDate, endDate, Utility.emptyToNull(responseCode), Utility.emptyToNull(currency),
                Utility.emptyToNull(type), null, Utility.emptyToNull(bankName), Utility.emptyToNull(search), true);

        List<PosTransactionViewModelWeb> list = page.getContent(); // page.getContent().stream().map((PosTransaction posTransaction) -> {
//
//            PosTransactionViewModelWeb modelWeb = PosTransactionViewModelWeb.from(posTransaction);
//
//            PosTerminal terminal = terminalService.getTerminal(posTransaction.getTerminalId());
//
//            if (terminal == null) {
//                return modelWeb;
//            }
//
//            modelWeb.setBankName(terminal.getBankName());
//            
//            if (terminal.getProvider() != null) {
//                modelWeb.setProvider(terminal.getProvider().getName());
//            }
//
//            PosMerchant posMerchant = terminalService.getMerchantName(posTransaction.getPosId(),
//                    terminal.getMerchant() == null ? null : terminal.getMerchant().getMerchantCode());
//
//            if (posMerchant != null) {
//                modelWeb.setMerchantName(posMerchant.getName());
//                modelWeb.setMerchantId(posMerchant.getMerchantId());
//                modelWeb.setMerchantCode(posMerchant.getMerchantCode());
//            }
//
//            return modelWeb;
//        }).collect(Collectors.<PosTransactionViewModelWeb>toList());

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        String date = Utility.formatDate(new Date(), "yyyyMMddHHmmss");
        
        if ("pdf".equalsIgnoreCase(format)) {
            // export to pdf here

            List<String> headers = (List) Utility.getClassFields(PosTransactionViewModelWeb.class);

            ServletOutputStream out = null;

            try {

                String fileName = PdfUtilExport.generatePdf(product, "Transaction Report", headers, (List) list);

                response.setHeader("Content-Disposition", "attachment; filename=postransactionrecord"+date+".pdf");
                response.setContentType("application/pdf");

                FileInputStream fileInputStream = new FileInputStream(fileName);
                out = response.getOutputStream();

                try {
                    IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                    out.flush();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }

//                out.flush();
                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } // end of pdf export
        else if ("csv".equalsIgnoreCase(format)) {

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
            List<String> headers = (List) Utility.getClassFields(PosTransactionViewModelWeb.class);

            String output = CsvUtil.writeLine(headers, (List) list);

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=postransactionrecord"+date+".csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } else {

            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet worksheet = workbook.createSheet("TransactionRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

            
            
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) Utility.getClassFields(PosTransactionViewModelWeb.class), "Transaction Export", null);

            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) list);

            response.setHeader("Content-Disposition", "attachment; filename=postransactionrecord"+date+".xls");
            response.setContentType("application/vnd.ms-excel");

            ServletOutputStream out = null;

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }

    }

    public void exportAirtimeTransactions(String network, String date, String search,
            String provider, String status) {

        if ("any".equalsIgnoreCase(network)) {
            network = null;
        }

        if ("any".equalsIgnoreCase(status)) {
            status = null;
        }

        if ("any".equalsIgnoreCase(provider)) {
            provider = null;
        }

        Date startDate = null, endDate = null;

        String startDateStr = null, endDateStr = null;

        if (Utility.emptyToNull(date) != null) {

            try {
                String[] splitDate = date.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);

                startDateStr = Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss");

                endDate = dateFormat.parse(splitDate[1]);

                endDateStr = Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

//        InputStream inputStream = airtimeHelper.downloadTransaction(network, status, startDateStr, endDateStr, search, provider, "");
//
//        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//
//        response.setHeader("Content-Disposition", "attachment; filename=transactionrecord" + Utility.formatDate(new Date(), "yyyyMMddHHmmss") + ".csv");
//        response.setContentType("application/csv");
//
//        ServletOutputStream out = null;
//
//        try {
//
//            if (inputStream != null) {
//
//                out = response.getOutputStream();
//                byte[] buffer = new byte[inputStream.available()];
//
//                inputStream.read(buffer);
//
//                out.write(buffer);
//            }
//
//            FacesContext.getCurrentInstance().responseComplete();
//
//        } catch (IOException err) {
//            err.printStackTrace();
//        } finally {
//            try {
//                if (out != null) {
//                    out.close();
//                }
//            } catch (IOException err) {
//                err.printStackTrace();
//            }
//        }
    }

    /**
     * This is called to export flutterwave transaction (from core)
     *
     * @param range
     * @param provider
     * @param search
     * @param currency
     * @param format
     */
    public void exportFlutterwaveTransaction(String range, String provider, String search, String currency, String format) {

        PageResult<Transaction> pageResult = new PageResult<>();

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

        Page<Transaction> page = transactionDao.getTransactions(0, 0, startDate, endDate, "".equalsIgnoreCase(search) ? null : search,
                "any".equalsIgnoreCase(currency) ? null : currency, "".equals(provider) ? null : provider, null, null,
                null, null, null, null, false);

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        List<Transaction> transactions = page.getContent();

        if ("pdf".equalsIgnoreCase(format)) {
            // export to pdf here

            List<String> headers = (List) Utility.getClassFields(GenericTransaction.class);

            ServletOutputStream out = null;

            try {

                String fileName = PdfUtilExport.generatePdf("Flutterwave", "Transaction Report", headers, (List) transactions);

                response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.pdf");
                response.setContentType("application/pdf");

                FileInputStream fileInputStream = new FileInputStream(fileName);
                out = response.getOutputStream();

                try {
                    IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                    out.flush();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }

//                out.flush();
                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } // end of pdf export
        else if ("csv".equalsIgnoreCase(format)) {

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
            List<String> headers = (List) Utility.getClassFields(GenericTransaction.class);

            String output = CsvUtil.writeLine(headers, (List) transactions);

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        }
    }

    public void exportData(String range, String search, String searchColumn, String status, String format, String doSearch, String doFilter) {

        if (search.equals("")) {
            search = null;
        }

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        Page<ReconTransaction> trxns;

        if ("".equals(search)) {
            search = null;
        }

        if ("true".equalsIgnoreCase(doSearch)) {
            startDate = endDate = null;
            status = "any";
        }

        if ("true".equalsIgnoreCase(doFilter)) {
            searchColumn = null;
            search = null;
        }

        Session session = SecurityUtils.getSubject().getSession();

        String domainId = String.valueOf(session.getAttribute("domainId"));

        if (searchColumn != null && !searchColumn.equalsIgnoreCase("") && search != null) {

            searchColumn = TransactionUtility.populateIndex().getOrDefault(searchColumn.toUpperCase(), null) + "";

            Class type = TransactionUtility.getFieldType(Transaction.class, searchColumn);

            type = TransactionUtility.map.getOrDefault(type, type);

            if (type == Date.class) {
                Date d = TransactionUtility.formatDate(search);
                trxns = reconTransactionDao.getTransaction(startDate, endDate, searchColumn, d, 0, 0, null, domainId);

            } else if (type == Double.class) {

                Object value = TransactionUtility.convertToType(search, type);
                double dValue = Double.parseDouble(value + "");
                trxns = reconTransactionDao.getTransaction(startDate, endDate, searchColumn, dValue, 0, 0, null, domainId);

            } else if (type == Integer.class || type == Long.class) {

                Object value = TransactionUtility.convertToType(search, type);
                long dValue = Long.parseLong(value + "");
                trxns = reconTransactionDao.getTransaction(startDate, endDate, searchColumn, dValue, 0, 0, "any".equalsIgnoreCase(status) == true ? null : "settled".equalsIgnoreCase(status), domainId);

            } else {

                Object value = TransactionUtility.convertToType(search, type);
                trxns = reconTransactionDao.getTransaction(startDate, endDate, searchColumn, value.toString(), 0, 0, "any".equalsIgnoreCase(status) == true ? null : "settled".equalsIgnoreCase(status), domainId);
            }
        } else {
            trxns = reconTransactionDao.getTransactions(null, null, search, searchColumn, 0, 0, "any".equalsIgnoreCase(status) == true ? null : "settled".equalsIgnoreCase(status), domainId);
        }

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        if ("pdf".equalsIgnoreCase(format)) {
            // export to pdf here

            List<String> headers = (List) TransactionUtility.getClassFields(PdfExportModel.class);

            ServletOutputStream out = null;

            try {

                String domainName = session.getAttribute("domainName") == null ? null : session.getAttribute("domainName") + "";
                domainId = session.getAttribute("domainId") == null ? null : session.getAttribute("domainId") + "";

                List<PdfExportModel> exportModels = new ArrayList<>();

                if (trxns.getCount() > 0) {
                    exportModels = trxns.getContent().stream().map((ReconTransaction x) -> {
                        PdfExportModel exportModel = new PdfExportModel();
                        exportModel.setAmount(x.getAccountAmount());
                        exportModel.setAcquiredId(x.getAcquiredId());
                        exportModel.setAid(x.getAid());
                        exportModel.setApprovalCode(x.getApprovalCode());
                        exportModel.setBank(x.getBank());
                        exportModel.setBatchId(x.getBatchId());
                        exportModel.setCardAcceptorId(x.getCardAcceptorId());
                        exportModel.setCardScheme(x.getCardScheme());
                        exportModel.setExternalResponseCode(x.getExternalResponseCode());
                        exportModel.setResponseCode(x.getResponseCode());
                        exportModel.setMaskedPan(x.getMaskedPan());
                        exportModel.setMerchantTitle(x.getMerchantTitle());
                        exportModel.setRrn(x.getRrn());
                        exportModel.setCurrency(x.getAccountCurrency());
                        exportModel.setTerminalName(x.getTerminalName());
                        exportModel.setOperationDate(x.getOperationDate());
                        exportModel.setSettled(x.isSettled());
                        exportModel.setSettlementDate(x.getSettlementDate());
                        exportModel.setAge(Utility.getAge(x.getOriginTime(), x.getSettlementDate()));
                        return exportModel;
                    }).collect(Collectors.<PdfExportModel>toList());
                }

                String fileName = ReconPdfUtil.generatePdf(domainName == null ? "Flutterwave" : domainId, "Transaction Report", headers, exportModels);

                response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.pdf");
                response.setContentType("application/pdf");

                FileInputStream fileInputStream = new FileInputStream(fileName);
                out = response.getOutputStream();

                try {
                    IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                    out.flush();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }

//                out.flush();
                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } else if ("csv".equalsIgnoreCase(format)) {

            List<PdfExportModel> exportModels = new ArrayList<>();

            if (trxns.getCount() > 0) {
                exportModels = trxns.getContent().stream().map((ReconTransaction x) -> {
                    PdfExportModel exportModel = new PdfExportModel();
                    exportModel.setAmount(x.getAccountAmount());
                    exportModel.setAcquiredId(x.getAcquiredId());
                    exportModel.setAid(x.getAid());
                    exportModel.setApprovalCode(x.getApprovalCode());
                    exportModel.setBank(x.getBank());
                    exportModel.setBatchId(x.getBatchId());
                    exportModel.setCardAcceptorId(x.getCardAcceptorId());
                    exportModel.setCardScheme(x.getCardScheme());
                    exportModel.setExternalResponseCode(x.getExternalResponseCode());
                    exportModel.setResponseCode(x.getResponseCode());
                    exportModel.setMaskedPan(x.getMaskedPan());
                    exportModel.setMerchantTitle(x.getMerchantTitle());
                    exportModel.setRrn(x.getRrn());
                    exportModel.setCurrency(x.getAccountCurrency());
                    exportModel.setTerminalName(x.getTerminalName());
                    exportModel.setOperationDate(x.getOperationDate());
                    exportModel.setSettled(x.isSettled());
                    exportModel.setSettlementDate(x.getSettlementDate());
                    exportModel.setAge(Utility.getAge(x.getOriginTime(), x.getSettlementDate()));
                    return exportModel;
                }).collect(Collectors.<PdfExportModel>toList());
            }

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
            List<String> headers = (List) TransactionUtility.getClassFields(PdfExportModel.class);

            List<String> additionalContent = new ArrayList();

            trxns.getContent().stream().forEach(x -> additionalContent.add(Utility.getAge(x) + ""));

            String output = CsvUtil.writeLine(headers, (List) exportModels, "flutterTransaction", "batch");

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } else {

            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("TransactionRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

//        FillManager.fillTransactionReport(worksheet, startRowIndex, startColIndex, datasource);
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) TransactionUtility.getClassFields(PdfExportModel.class), "Transaction Export", null);

            List<PdfExportModel> exportModels = new ArrayList<>();

            if (trxns.getCount() > 0) {
                exportModels = trxns.getContent().stream().map((ReconTransaction x) -> {
                    PdfExportModel exportModel = new PdfExportModel();
                    exportModel.setAmount(x.getAccountAmount());
                    exportModel.setAcquiredId(x.getAcquiredId());
                    exportModel.setAid(x.getAid());
                    exportModel.setApprovalCode(x.getApprovalCode());
                    exportModel.setBank(x.getBank());
                    exportModel.setBatchId(x.getBatchId());
                    exportModel.setCardAcceptorId(x.getCardAcceptorId());
                    exportModel.setCardScheme(x.getCardScheme());
                    exportModel.setExternalResponseCode(x.getExternalResponseCode());
                    exportModel.setResponseCode(x.getResponseCode());
                    exportModel.setMaskedPan(x.getMaskedPan());
                    exportModel.setMerchantTitle(x.getMerchantTitle());
                    exportModel.setRrn(x.getRrn());
                    exportModel.setCurrency(x.getAccountCurrency());
                    exportModel.setTerminalName(x.getTerminalName());
                    exportModel.setOperationDate(x.getOperationDate());
                    exportModel.setSettled(x.isSettled());
                    exportModel.setSettlementDate(x.getSettlementDate());
                    exportModel.setAge(Utility.getAge(x.getOriginTime(), x.getSettlementDate()));
                    return exportModel;
                }).collect(Collectors.<PdfExportModel>toList());
            }

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
//            List<String> headers = (List) TransactionUtility.getClassFields(PdfExportModel.class);
            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) exportModels, "flutterTransaction", "batch");

            //HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//       String fileName = "Card-Batch.xls";
            response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.xls");
            response.setContentType("application/vnd.ms-excel");

            ServletOutputStream out = null;

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }

    }

    public void exportFlutterwaveData(String range, String searchKey, String search, String provider, String merchantId,
            String currency, String transactionType, String status, String cardScheme, String category, String disbursementCode, Boolean refund, String format) {

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

        String title = null;

        switch (searchKey) {

            case "rrn":
                title = "rrn";
                break;
            case "merchantRef":
                title = "merchantTxnReference";
                break;
            case "flwRef":
                title = "flwTxnReference";
                break;
            case "masked":
                title = "cardMask";
                break;
        }

        Page<TransactionListModel> page = transactionDao.getTransactionsList(0, 0, startDate, endDate, title, "".equalsIgnoreCase(search) ? null : search,
                "any".equalsIgnoreCase(currency) ? null : currency, "".equals(provider) ? null : provider, "".equals(merchantId) ? null : merchantId,
                "any".equalsIgnoreCase(category) ? null : category, "any".equalsIgnoreCase(status) ? null : status,
                "any".equalsIgnoreCase(transactionType) ? null : transactionType, "".equals(cardScheme) ? null : cardScheme, ("any".equalsIgnoreCase(disbursementCode) || "".equalsIgnoreCase(disbursementCode)) ? null : disbursementCode,
                true,refund);

        List<TransactionListModel> transactions = page.getContent();
        
//        List<TransactionListModel> transactions = new ArrayList<>();
        
//        if(tlist != null && !tlist.isEmpty()){
//            
//            transactions = tlist.stream().map((TransactionListModel x) -> {
//                
////                TransactionListModel model = new TransactionListModel();                
//                
//                String prov = x.getProvider();
//               
//                String mid = x.getMerchantId();
//                
//                if(prov != null && prov.toUpperCase().startsWith("MCASH")){
//                    
//                    String processReference =  x.getProcessorReference();
//                    
//                    if(processReference != null){
//                        
//                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();
//                        
//                        String merchantCode = Utility.getData(array, "merchantcode");
////                        String merchantName = jsonObject.getString("merchantname", status)
//                        x.setMerchantId(x.getMerchantId() +" ("+merchantCode+") ");
//                        
//                    } 
//                }
//                
//                return x;
//            }).collect(Collectors.<TransactionListModel>toList());
//            
//        }
        

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        if ("pdf".equalsIgnoreCase(format)) {
            // export to pdf here

            List<String> extraHeaders = new ArrayList<>();
            extraHeaders.add("ReprocessedReference");

            List<String> headers = (List) Utility.getClassFields(TransactionListModel.class, extraHeaders);

            ServletOutputStream out = null;

            try {

                String fileName = PdfUtilExport.generatePdf("Flutterwave", "Transaction Report", headers, (List) transactions);

                response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.pdf");
                response.setContentType("application/pdf");

                FileInputStream fileInputStream = new FileInputStream(fileName);
                out = response.getOutputStream();

                try {
                    IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                    out.flush();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }

//                out.flush();
                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } // end of pdf export
        else if ("csv".equalsIgnoreCase(format)) {

            String[] exceptions = new String[]{"settledOn", "settlementState", "settledOn", "settledBy", "settlement", "transactionData", "processorReference", "id"};
            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);

            List<String> extraHeaders = new ArrayList<>();
            extraHeaders.add("ReprocessedReference");

            List<String> headers = (List) Utility.getClassFields(TransactionListModel.class, extraHeaders, exceptions);

            String output = CsvUtil.writeLine(headers, (List) transactions, exceptions);

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        } else {

            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet worksheet = workbook.createSheet("TransactionRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

            List<String> extraHeaders = new ArrayList<>();
            extraHeaders.add("ReprocessedReference");

            List<String> headers = TransactionUtility.getClassFields(TransactionListModel.class);

            headers.addAll(extraHeaders);

            String[] exceptions = new String[]{"settledOn", "settlementState", "settledOn", "settledBy", "settlement", "processorReference"};

            List<String> list = new ArrayList<>();
            list.addAll(Stream.of(exceptions).collect(Collectors.toList()));

            List<String> list1 = list.stream().map(x -> x.toLowerCase()).collect(Collectors.toList());

            list = headers.stream().filter(x -> !list1.contains(x.toLowerCase())).collect(Collectors.toList());

//        FillManager.fillTransactionReport(worksheet, startRowIndex, startColIndex, datasource);
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) list, "Transaction Export", null);

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
//            List<String> headers = (List) TransactionUtility.getClassFields(PdfExportModel.class);
            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) transactions, "settledOn", "settlementState", "settledOn", "settledBy", "settlement", "processorReference");

            //HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//       String fileName = "Card-Batch.xls";
            response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.xls");
            response.setContentType("application/vnd.ms-excel");

            ServletOutputStream out = null;

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }

    }

    public void exportFlutterwaveAirtimeData(String range, String search, String provider, String merchantId,
            String currency, String transactionType, String status, String cardScheme,
            String transResCode, String reverseCode, Boolean refund, String format) {

        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }

        Page<AirtimeTransaction> page = airtimeTransactionDao.getAirtimeTransactions(0, 0, startDate, endDate, "".equalsIgnoreCase(search) ? null : search,
                "any".equalsIgnoreCase(currency) ? null : currency, "".equals(provider) ? null : provider, "".equals(merchantId) ? null : merchantId,
                "any".equalsIgnoreCase(status) ? null : status, "any".equalsIgnoreCase(transactionType) ? null : transactionType, "".equals(cardScheme) ? null : cardScheme,
                "any".equalsIgnoreCase(reverseCode) ? null : reverseCode, "any".equalsIgnoreCase(reverseCode) ? null : reverseCode, refund);

        List<AirtimeTransaction> transactions = page.getContent();

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        Map<String, String> replacement = new HashMap<>();
        replacement.put("sourceIdentifier", "Sender");
        replacement.put("targetIdentifier", "Beneficiary");
        replacement.put("cardMask", "Account Number");

        if ("pdf".equalsIgnoreCase(format)) {
            // export to pdf here

            List<String> headers = (List) Utility.getClassFields(AirtimeTransaction.class);

            ServletOutputStream out = null;

            try {

                String fileName = PdfUtilExport.generatePdf("Flutterwave", "Airtime Transaction Report", headers, (List) transactions);

                response.setHeader("Content-Disposition", "attachment; filename=airtimetransactionrecord.pdf");
                response.setContentType("application/pdf");

                FileInputStream fileInputStream = new FileInputStream(fileName);
                out = response.getOutputStream();

                try {
                    IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                    out.flush();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }

//                out.flush();
                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }

        } // end of pdf export
        else if ("csv".equalsIgnoreCase(format)) {

            String[] exceptions = new String[]{"transactionData", "processorReference", "provider", "id", "cardScheme", "flwTxnReference"};
            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);

            List<String> headers = (List) Utility.getClassFields(AirtimeTransaction.class, replacement, exceptions);

            String output = CsvUtil.writeLine(headers, (List) transactions, exceptions);

            ServletOutputStream out = null;

            try {

                response.setHeader("Content-Disposition", "attachment; filename=airtimetransactionrecord.csv");
                response.setContentType("application/csv");

                out = response.getOutputStream();
                out.write(output.getBytes("UTF-8"));

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        } else {

            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("AirtimeTransactionRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

            String[] exceptions = new String[]{"transactionData", "processorReference", "provider", "cardScheme", "flwTxnReference"};

            List<String> headers = Utility.getClassFields(AirtimeTransaction.class, replacement, exceptions);

            List<String> list = new ArrayList<>();
            list.addAll(Stream.of(exceptions).collect(Collectors.toList()));

            List<String> list1 = list.stream().map(x -> x.toLowerCase()).collect(Collectors.toList());

            list = headers.stream().filter(x -> !list1.contains(x.toLowerCase())).collect(Collectors.toList());

//        FillManager.fillTransactionReport(worksheet, startRowIndex, startColIndex, datasource);
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) list, "Transaction Export", null);

            //List<String> headers = (List) TransactionUtility.getClassFields(Transaction.class);
//            List<String> headers = (List) TransactionUtility.getClassFields(PdfExportModel.class);
            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) transactions, "transactionData", "processorReference", "provider", "cardScheme", "flwTxnReference");

            //HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//       String fileName = "Card-Batch.xls";
            response.setHeader("Content-Disposition", "attachment; filename=airtimetransactionrecord.xls");
            response.setContentType("application/vnd.ms-excel");

            ServletOutputStream out = null;

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }

    }

    public String getTransactions(String product, String status) {

        this.product = product;
        this.status = status;

        return "/transaction/filtered";
    }

    public String getSelectedProduct() {
        return product;
    }

    public String getSelectedStatus() {
        return status;
    }

    private String product;
    private String status;

    private String dateRange;

    public void getCoreReport(String range) {

        try {

            if (range == null && !"".equalsIgnoreCase(range)) {
                JsfUtil.addErrorMessage("Please select date range");
                return;
            }

            Date startDate = null, endDate = null;

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            startDate = dateFormat.parse(range);
            Date tempStartDate = startDate;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            startDate = calendar.getTime();
            calendar = Calendar.getInstance();
            calendar.setTime(tempStartDate);
            //                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 59);
            endDate = calendar.getTime();
            // This get successful card account
            List cardSuccessSummary = transactionDao.getReport(startDate, endDate, true, true);
            // card failure
            List cardFailureSummary = transactionDao.getReport(startDate, endDate, false, true);
            // account success
            List accountSuccessSummary = transactionDao.getReport(startDate, endDate, true, false);
            // account failure
            List accountFailureSummary = transactionDao.getReport(startDate, endDate, false, false);
            // card failure reasons
            List cardFailureReasons = transactionDao.getFailureReason(startDate, endDate, true);

            List cardSuccessful = transactionDao.getSuccessAnalysis(startDate, endDate, true);

            Map<String, Map<Date, CoreReportViewModel>> map = new TreeMap<>();

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            if (cardSuccessSummary != null && !cardSuccessSummary.isEmpty()) {

                for (Object data : cardSuccessSummary) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());

                    Map<Date, CoreReportViewModel> result = map.getOrDefault(currency, new HashMap<>());

                    CoreReportViewModel model = result.getOrDefault(date, new CoreReportViewModel());

                    model.setCurrency(currency);
                    if (sum != null) {
                        model.setCardSuccessValue(model.getCardSuccessValue() + (sum));
                    }
                    if (count != null) {
                        model.setCardSuccessVolume(model.getCardSuccessVolume().add(count));
                    }

                    result.put(date, model);

                    map.put(currency, result);
                }
            }

            if (cardFailureSummary != null && !cardFailureSummary.isEmpty()) {

                for (Object data : cardFailureSummary) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());

                    Map<Date, CoreReportViewModel> result = map.getOrDefault(currency, new HashMap<>());

                    CoreReportViewModel model = result.getOrDefault(date, new CoreReportViewModel());

                    model.setCurrency(currency);
                    if (sum != null) {
                        model.setCardFailureValue(model.getCardFailureValue() + sum);
                    }
                    if (count != null) {
                        model.setCardFailureVolume(model.getCardFailureVolume().add(count));
                    }

                    result.put(date, model);

                    map.put(currency, result);
                }
            }

            if (accountSuccessSummary != null && !accountSuccessSummary.isEmpty()) {

                for (Object data : accountSuccessSummary) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());

                    Map<Date, CoreReportViewModel> result = map.getOrDefault(currency, new HashMap<>());

                    CoreReportViewModel model = result.getOrDefault(date, new CoreReportViewModel());

                    model.setCurrency(currency);

                    if (sum != null) {
                        model.setAccountSuccessValue(model.getAccountSuccessValue() + sum);
                    }
                    if (count != null) {
                        model.setAccountSuccessVolume(model.getAccountSuccessVolume().add(count));
                    }

                    result.put(date, model);

                    map.put(currency, result);
                }
            }

            if (accountFailureSummary != null && !accountFailureSummary.isEmpty()) {

                for (Object data : accountFailureSummary) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());

                    Map<Date, CoreReportViewModel> result = map.getOrDefault(currency, new HashMap<>());

                    CoreReportViewModel model = result.getOrDefault(date, new CoreReportViewModel());

                    model.setCurrency(currency);
                    if (sum != null) {
                        model.setAccountFailureValue(model.getAccountFailureValue() + sum);
                    }
                    if (count != null) {
                        model.setAccountFailureVolume(model.getAccountFailureVolume().add(count));
                    }

                    result.put(date, model);

                    map.put(currency, result);
                }
            }

            Map<Date, Map<String, Map<String, List<FailureAnalysisModel>>>> failureAnalysis = new TreeMap<>(Collections.reverseOrder());

            if (cardFailureReasons != null && !cardFailureReasons.isEmpty()) {

                for (Object data : cardFailureReasons) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(String.valueOf(objects[3]));
                    String reason = String.valueOf(objects[4]);
                    String cardScheme = String.valueOf(objects[5]);

                    Map<String, Map<String, List<FailureAnalysisModel>>> result = failureAnalysis.getOrDefault(date, new HashMap<>());

                    Map<String, List<FailureAnalysisModel>> models = result.getOrDefault(currency.toUpperCase(), new HashMap<>());

                    List<FailureAnalysisModel> failureAnalysisModels = models.getOrDefault(cardScheme + "", new ArrayList<>());

                    FailureAnalysisModel analysisModel = new FailureAnalysisModel();
                    analysisModel.setCurrency(currency);
                    analysisModel.setCardScheme(cardScheme);
                    analysisModel.setReason(reason);
                    analysisModel.setValue(sum);
                    analysisModel.setVolume(count);

                    failureAnalysisModels.add(analysisModel);

                    models.put(cardScheme + "", failureAnalysisModels);

                    result.put(currency, models);

                    failureAnalysis.put(date, result);
                }
            }

            Map<Date, Map<String, Map<String, FailureAnalysisModel>>> successAnalysis = new TreeMap<>(Collections.reverseOrder());

            if (cardSuccessful != null && !cardSuccessful.isEmpty()) {

                for (Object data : cardSuccessful) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());
                    String cardScheme = String.valueOf(objects[4]);

                    Map<String, Map<String, FailureAnalysisModel>> result = successAnalysis.getOrDefault(date, new HashMap<>());

                    Map<String, FailureAnalysisModel> models = result.getOrDefault(currency.toUpperCase(), new HashMap<>());

                    FailureAnalysisModel analysisModel = models.getOrDefault(cardScheme + "", new FailureAnalysisModel());

                    analysisModel.setCurrency(currency);
                    analysisModel.setCardScheme(cardScheme);
                    analysisModel.setValue(sum);
                    analysisModel.setVolume(count);

                    models.put(cardScheme + "", analysisModel);

                    result.put(currency, models);

                    successAnalysis.put(date, result);
                }
            }

            XSSFWorkbook workbook = new XSSFWorkbook();
//            
            XSSFSheet worksheet = workbook.createSheet("Card And Account Transactions Analysis");

            List<String> headers = new ArrayList<>();
            headers.add("Date/Currency");
            headers.add("Card Successful Value");
            headers.add("Card Successful Volume");
            headers.add("Card Failed Value");
            headers.add("Card Failed Volume");

            headers.add("Account Successful Value");
            headers.add("Account Successful Volume");
            headers.add("Account Failed Value");
            headers.add("Account Failed Volume");

            Layouter1.buildHeaders(worksheet, 0, 0, headers);

            Long val = ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));

            int diff = val.intValue();

            FillManager.buildCoreReportTrans(worksheet, 0, 0, map, diff);

            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

            worksheet = workbook.createSheet("Card Failure Analysis");

            headers = new ArrayList<>();
            headers.add("Date/Currency");
            headers.add("Card Scheme");
            headers.add("Value");
            headers.add("Volume");
            headers.add("Reason");

            Layouter1.buildHeaders(worksheet, 0, 0, headers);

            FillManager.buildCoreReportFailure(worksheet, 0, 0, failureAnalysis);

            worksheet = workbook.createSheet("Card Success Analysis");

            headers = new ArrayList<>();
            headers.add("Date/Currency");
            headers.add("Card Scheme");
            headers.add("Value");
            headers.add("Volume");

            Layouter1.buildHeaders(worksheet, 0, 0, headers);

            FillManager.buildCoreReportSuccess(worksheet, 0, 0, successAnalysis);

            response.setHeader("Content-Disposition", "attachment; filename=Core_Transaction_Report_" + Utility.formatDate(startDate, "dd-MM-yyyy") + "_to_" + Utility.formatDate(endDate, "dd-MM-yyyy") + ".xlsx");
            response.setContentType("application/vnd.ms-excel");

            OutputStream out = response.getOutputStream();
            workbook.write(out);

            out.flush();
            out.close();

            FacesContext.getCurrentInstance().responseComplete();

        } catch (Exception ex) {
            Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getMainCoreReport(String range) throws DatabaseException {

        Date startDate = null, endDate = null, startMonthYear = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
//                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                startDate = dateFormat.parse(range);

                Date tempStartDate = startDate;

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                startDate = calendar.getTime();

                calendar = Calendar.getInstance();
                calendar.setTime(tempStartDate);
//                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                calendar.set(Calendar.MILLISECOND, 59);

                endDate = calendar.getTime();

//                calendar.set(Calendar.MONTH, 0);
                calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());

                startMonthYear = calendar.getTime();

            } catch (ParseException pe) {
                startDate = null;
            }
        }

        List currentMonthTransaction = processTransactions(startDate, endDate);

//        List monthSelection = transactionDao.getTransactionByMonth(endDate);
        Map<String, List> data = new HashMap<>();
//        data.put("months", list);
        data.put("currentmonth", currentMonthTransaction);

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet worksheet = workbook.createSheet("Transaction Report Summary " + range);

        int startRowIndex = 0;
        int startColIndex = 0;

        List<String> headers = new LinkedList<>();
        headers.add("Daily");
        headers.add("Volume");
        headers.add("Value");
        headers.add("Revenue");
        headers.add("% Growth In Volume");
        headers.add("% Growth In Value");
        headers.add("% Growth In Revenue");

        int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, headers, "Core Transaction Report", null);

        FillManager.coreReport(worksheet, usedRows - 1, startColIndex, currentMonthTransaction);

        worksheet = workbook.createSheet("Flutterwave Core Breakdown ");

        Map<String, Map> map;

        headers = new LinkedList<>();
        headers.add("");
        headers.add("Volume");
        headers.add("Value");
        headers.add("Revenue");

        try {

            Calendar calendar = Calendar.getInstance();

            map = getMonthToDateSummary(calendar.getTime());

            Layouter1.buildReport(worksheet, startRowIndex, startColIndex, headers, "Core Transaction Report", null);

            FillManager.coreReportToDate(worksheet, usedRows - 1, startColIndex, map);

        } catch (ParseException ex) {
            Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }

        worksheet = workbook.createSheet("Summary");

        map = getByMonthTillLastMonth();

        headers = new LinkedList<>();
        headers.add("M-o-M");
        headers.add("Volume");
        headers.add("Value");
        headers.add("Revenue");
        headers.add("% Growth in Volume");
        headers.add("% Growth in Value");
        headers.add("% Growth in Revenue");

        Layouter1.buildReport(worksheet, startRowIndex, startColIndex, headers, "Summary", null);

        Map nineDayMap = getByMonthFirstNineDays();

        FillManager.coreReportSummary(worksheet, usedRows - 1, startColIndex, map, nineDayMap);

        //HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//       String fileName = "Card-Batch.xls";
        response.setHeader("Content-Disposition", "attachment; filename=report.xls");
        response.setContentType("application/vnd.ms-excel");

        ServletOutputStream out = null;

        try {

            String rootPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();

            response.setHeader("Refresh", "3; url = " + rootPath + "/report/maincore.xhtml");

            out = response.getOutputStream();
            workbook.write(out);

//            FacesContext.getCurrentInstance().getExternalContext().redirect("/report/maincore");
            FacesContext.getCurrentInstance().responseComplete();

        } catch (IOException err) {
            err.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException err) {
                err.printStackTrace();
            }
        }

//        return "/report/maincore";
    }

    private List processTransactions(Date startDate, Date endDate) throws DatabaseException {

        Date tempDate = startDate;

        Product prod = productDao.findByKey("name", "core");

        if (prod == null) {
            prod = productDao.findByKey("name", "flutterwave core");
        }

        List responseList = new LinkedList();

        Map<Date, List> reports = new TreeMap<>();

//        List<CoreReport> coreReports = new ArrayList();
        int start = 0;

        double previousTotalValue = 0.0, previousTotalRevenue = 0.0;
        long previousTotalVolume = 0L;

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        while (tempDate.compareTo(endDate) <= 0) {

            List<CoreReport> filteredReport = coreReportDao.findByReportDate(tempDate);

            if (filteredReport == null || filteredReport.isEmpty()) {

                List<CoreReport> updatedReport = new ArrayList<>();
                Map<String, Object> map = new LinkedHashMap<>();

                List<Provider> providers = providerDao.findAll();

                List<Product> list = productDao.findAll();

                if (list == null) {
                    return null;
                }

//                    Product product = list.stream().filter(x -> "core".equalsIgnoreCase(x.getName()) || "flutterwave core".equalsIgnoreCase(x.getName())).findFirst().orElse(null);
                Map<String, Map> currencyData = new HashMap<>();

                Map<String, Object> currMap;

                double totalValue = 0.0, totalRevenue = 0.0;

                long totalVolume = 0L;

                for (Provider p : providers) {

                    List<Transaction> transaction = transactionDao.getSettleableTransanctions(startDate, endDate,
                            p, null, null);

                    Map<String, Map<String, List<Transaction>>> dataMapping = transaction.stream().collect(Collectors
                            .groupingBy(Transaction::getMerchantId, Collectors.groupingBy(Transaction::getCardCountry)));

                    ConfigurationFee configurationFee = configurationFeeDao.findByProduct(prod);

                    for (Map.Entry<String, Map<String, List<Transaction>>> entry : dataMapping.entrySet()) {

                        String mid = entry.getKey();
                        Map<String, List<Transaction>> values = entry.getValue();

                        List<CoreMerchant> coreMer = coreMerchantDao.find("merchantID", mid);

                        CoreMerchant coreMerchant = null;

                        if (coreMer != null) {
                            coreMerchant = coreMer.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
                        }

                        MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(prod, coreMerchant == null ? 0 : coreMerchant.getId());

                        Currency providerCurrency = p.getCurrency();

                        String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                        Country currencyCountry = null;

                        if (providerCurrency != null) {
                            currencyCountry = countryDao.findByCurrency(providerCurrency);
                        }

                        String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                        String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                        for (Map.Entry<String, List<Transaction>> val : values.entrySet()) {

                            String cardCountry = val.getKey();

                            Map<String, List<Transaction>> currencyValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency));

//                            Map<String, Long> countValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency, Collectors.counting()));
//                            boolean local = false;
                            for (String currencyS : currencyValues.keySet()) {

                                currMap = currencyData.getOrDefault(currencyS, new LinkedHashMap<>());

                                double fee = 0.0, cost = 0.0, value = 0.0;
                                long volume = 0;
//                                long count = countValues.getOrDefault(currencyS, 1L);
                                boolean local = currencyS.equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));

                                List<Transaction> txns = currencyValues.get(currencyS);

                                for (Transaction t : txns) {

                                    cost += getCost(p, t.getAmount(), local, 1);

                                    double configFee = getFee(configurationFee, t.getAmount(), local, currencyS, 1);

                                    double merchantFe = getFee(merchantFee, t.getAmount(), local, currencyS, 1);

                                    value += t.getAmount();

                                    fee += merchantFe == 0 ? configFee : merchantFe;
                                }

                                volume = txns.size();

                                double cValue = getExchangeRateInNGN(currencyS) * value;

                                double cFee = getExchangeRateInNGN(currencyS) * fee;

                                totalRevenue += cFee;
                                totalValue += cValue;
                                totalVolume += volume;

                                cValue = Double.parseDouble(decimalFormat.format(cValue));

                                currMap.put("amount", Double.parseDouble(currMap.getOrDefault("amount", 0.0) + "") + cValue);
                                currMap.put("volume", Long.parseLong(currMap.getOrDefault("volume", 0) + "") + volume);
                                currMap.put("revenue", Double.parseDouble(currMap.getOrDefault("revenue", 0.0) + "") + fee);
                                currMap.put("currency", currencyS);
                                currMap.put("rate", getExchangeRateInNGN(currencyS));

                                currencyData.put(currencyS, currMap);

                            }
                        }

                    }

                }

                for (String curr : currencyData.keySet()) {

                    Map<String, Object> m = currencyData.getOrDefault(curr, new HashMap());

                    CoreReport coreReport = new CoreReport();
                    coreReport.setOriginCurrency(curr);
                    coreReport.setDestinationCurrency("NGN");
                    coreReport.setRate(Double.parseDouble(m.getOrDefault("rate", "0.0") + ""));
                    coreReport.setAmount(Double.parseDouble(m.getOrDefault("amount", "0.0") + ""));
                    coreReport.setRevenue(Double.parseDouble(m.getOrDefault("revenue", "0.0") + ""));
                    coreReport.setVolume(Long.parseLong(m.getOrDefault("volume", "0") + ""));
                    coreReport.setCreatedOn(new Date());
                    coreReport.setReportDate(tempDate);

                    updatedReport.add(coreReport);
                }

                map.put("volume", totalVolume);
                map.put("value", totalValue);
                map.put("revenue", totalRevenue);

                if (start == 0) {
                    map.put("volumegrowth", 0);
                    map.put("valuegrowth", 0);
                    map.put("revenuegrowth", 0);

                    start++;
                } else {

                    map.put("volumegrowth", decimalFormat.format(previousTotalVolume == 0 ? (totalVolume - previousTotalVolume) * 100 : ((totalVolume - previousTotalVolume) / previousTotalVolume) * 100));
                    map.put("valuegrowth", decimalFormat.format(previousTotalValue == 0 ? (totalValue - previousTotalValue) * 100 : ((totalValue - previousTotalValue) / previousTotalValue) * 100));
                    map.put("revenuegrowth", decimalFormat.format(previousTotalRevenue == 0 ? (totalRevenue - previousTotalRevenue) * 100 : ((totalRevenue - previousTotalRevenue) / previousTotalRevenue) * 100));
                }

                previousTotalVolume = totalVolume;
                previousTotalValue = totalValue;
                previousTotalRevenue = totalRevenue;

                responseList.add(map);

                if (!updatedReport.isEmpty()) {
                    coreReportDao.create(updatedReport);
                }

            } else {

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

                Map<String, Object> map = new HashMap<>();
                map.put("Date", dateFormat.format(tempDate));

                double totalValue = 0.0, totalRevenue = 0.0;
                long totalVolume = 0L;

                for (CoreReport coreReport : filteredReport) {

                    totalValue += coreReport.getAmount();
                    totalRevenue += coreReport.getRevenue();
                    totalVolume += coreReport.getVolume();
                }

                map.put("value", totalValue);
                map.put("volume", totalVolume);
                map.put("revenue", totalRevenue);

                if (start == 0) {

                    map.put("volumegrowth", 0);
                    map.put("valuegrowth", 0);
                    map.put("revenuegrowth", 0);

                    start++;
                } else {

                    map.put("volumegrowth", decimalFormat.format(previousTotalVolume == 0 ? (totalVolume - previousTotalVolume) * 100 : ((totalVolume - previousTotalVolume) / previousTotalVolume) * 100));
                    map.put("valuegrowth", decimalFormat.format(previousTotalValue == 0 ? (totalValue - previousTotalValue) * 100 : ((totalValue - previousTotalValue) / previousTotalValue) * 100));
                    map.put("revenuegrowth", decimalFormat.format(previousTotalRevenue == 0 ? (totalRevenue - previousTotalRevenue) * 100 : ((totalRevenue - previousTotalRevenue) / previousTotalRevenue) * 100));
                }

                previousTotalVolume = totalVolume;
                previousTotalValue = totalValue;
                previousTotalRevenue = totalRevenue;

                responseList.add(map);
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(tempDate);
            calendar.add(Calendar.DAY_OF_MONTH, 1);

            tempDate = calendar.getTime();
        }

        return responseList;
    }

    private Map<Date, Object> processBreakDown(Date startDate, Date endDate) throws DatabaseException {

        Date tempDate = startDate;

        Product prod = productDao.findByKey("name", "core");

        if (prod == null) {
            prod = productDao.findByKey("name", "flutterwave core");
        }

        Map<Date, Object> responseList = new HashMap();

        Map<Date, List> reports = new TreeMap<>();

//        List<CoreReport> coreReports = new ArrayList();
        int start = 0;

        double previousTotalValue = 0.0, previousTotalRevenue = 0.0;
        long previousTotalVolume = 0L;

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        while (tempDate.compareTo(endDate) <= 0) {

            List<CoreReport> filteredReport = coreReportDao.findByReportDate(tempDate);

            if (filteredReport == null || filteredReport.isEmpty()) {

                List<CoreReport> updatedReport = new ArrayList<>();
//                    Map<String, Object> map = new LinkedHashMap<>();

                List<Provider> providers = providerDao.findAll();

                List<Product> list = productDao.findAll();

                if (list == null) {
                    return null;
                }

//                    Product product = list.stream().filter(x -> "core".equalsIgnoreCase(x.getName()) || "flutterwave core".equalsIgnoreCase(x.getName())).findFirst().orElse(null);
                Map<String, Map> currencyData = new HashMap<>();

                Map<String, Object> currMap;

                double totalValue = 0.0, totalRevenue = 0.0;

                long totalVolume = 0L;

                for (Provider p : providers) {

                    List<Transaction> transaction = transactionDao.getSettleableTransanctions(startDate, endDate,
                            p, null, null);

                    Map<String, Map<String, List<Transaction>>> dataMapping = transaction.stream().collect(Collectors
                            .groupingBy(Transaction::getMerchantId, Collectors.groupingBy(Transaction::getCardCountry)));

                    ConfigurationFee configurationFee = configurationFeeDao.findByProduct(prod);

                    for (Map.Entry<String, Map<String, List<Transaction>>> entry : dataMapping.entrySet()) {

                        String mid = entry.getKey();
                        Map<String, List<Transaction>> values = entry.getValue();

                        List<CoreMerchant> coreMer = coreMerchantDao.find("merchantID", mid);

                        CoreMerchant coreMerchant = null;

                        if (coreMer != null) {
                            coreMerchant = coreMer.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
                        }

                        MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(prod, coreMerchant == null ? 0 : coreMerchant.getId());

                        Currency providerCurrency = p.getCurrency();

                        String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                        Country currencyCountry = null;

                        if (providerCurrency != null) {
                            currencyCountry = countryDao.findByCurrency(providerCurrency);
                        }

                        String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                        String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                        for (Map.Entry<String, List<Transaction>> val : values.entrySet()) {

                            String cardCountry = val.getKey();

                            Map<String, List<Transaction>> currencyValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency));

//                            Map<String, Long> countValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency, Collectors.counting()));
//                            boolean local = false;
                            for (String currencyS : currencyValues.keySet()) {

                                currMap = currencyData.getOrDefault(currencyS, new LinkedHashMap<>());

                                double fee = 0.0, cost = 0.0, value = 0.0;
                                long volume = 0;
//                                long count = countValues.getOrDefault(currencyS, 1L);
                                boolean local = currencyS.equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));

                                List<Transaction> txns = currencyValues.get(currencyS);

                                for (Transaction t : txns) {

                                    cost += getCost(p, t.getAmount(), local, 1);

                                    double configFee = getFee(configurationFee, t.getAmount(), local, currencyS, 1);

                                    double merchantFe = getFee(merchantFee, t.getAmount(), local, currencyS, 1);

                                    value += t.getAmount();

                                    fee += merchantFe == 0 ? configFee : merchantFe;
                                }

                                volume = txns.size();

                                double cValue = getExchangeRateInNGN(currencyS) * value;

                                double cFee = getExchangeRateInNGN(currencyS) * fee;

                                totalRevenue += cFee;
                                totalValue += cValue;
                                totalVolume += volume;

                                cValue = Double.parseDouble(decimalFormat.format(cValue));

                                currMap.put("amount", Double.parseDouble(currMap.getOrDefault("amount", 0.0) + "") + cValue);
                                currMap.put("volume", Long.parseLong(currMap.getOrDefault("volume", 0) + "") + volume);
                                currMap.put("revenue", Double.parseDouble(currMap.getOrDefault("revenue", 0.0) + "") + fee);
                                currMap.put("currency", currencyS);
                                currMap.put("rate", getExchangeRateInNGN(currencyS));

                                currencyData.put(currencyS, currMap);

                            }
                        }

                    }

                }

                for (String curr : currencyData.keySet()) {

                    Map<String, Object> m = currencyData.getOrDefault(curr, new HashMap());

                    CoreReport coreReport = new CoreReport();
                    coreReport.setOriginCurrency(curr);
                    coreReport.setDestinationCurrency("NGN");
                    coreReport.setRate(Double.parseDouble(m.getOrDefault("rate", "0.0") + ""));
                    coreReport.setAmount(Double.parseDouble(m.getOrDefault("amount", "0.0") + ""));
                    coreReport.setRevenue(Double.parseDouble(m.getOrDefault("revenue", "0.0") + ""));
                    coreReport.setVolume(Long.parseLong(m.getOrDefault("volume", "0") + ""));
                    coreReport.setCreatedOn(new Date());
                    coreReport.setReportDate(tempDate);

                    updatedReport.add(coreReport);

                    Map<String, Object> map = new LinkedHashMap<>();
                    map.put("currency", coreReport.getOriginCurrency());
                    map.put("volume", coreReport.getVolume());
                    map.put("value", coreReport.getAmount());
                    map.put("revenue", coreReport.getRevenue());

                    responseList.put(tempDate, map);
                }

                if (!updatedReport.isEmpty()) {
                    coreReportDao.create(updatedReport);
                }

            } else {

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

//                Map<String, Object> map = new HashMap<>();
//                map.put("Date", tempDate);
                double totalValue = 0.0, totalRevenue = 0.0;
                long totalVolume = 0L;

                for (CoreReport coreReport : filteredReport) {

                    totalValue += coreReport.getAmount();
                    totalRevenue += coreReport.getRevenue();
                    totalVolume += coreReport.getVolume();

                    Map<String, Object> map = new LinkedHashMap<>();
                    map.put("currency", coreReport.getOriginCurrency());
                    map.put("volume", coreReport.getVolume());
                    map.put("value", coreReport.getAmount());
                    map.put("revenue", coreReport.getRevenue());

                    responseList.put(tempDate, map);
                }

//                map.put(status, map)
//                map.put("value", totalValue);
//                map.put("volume", totalVolume);
//                map.put("revenue", totalRevenue);
//
//                if (start == 0) {
//
//                    map.put("volumegrowth", 0);
//                    map.put("valuegrowth", 0);
//                    map.put("revenuegrowth", 0);
//
//                    start++;
//                } else {
//
//                    map.put("volumegrowth", decimalFormat.format(previousTotalVolume == 0 ? (totalVolume - previousTotalVolume) * 100 : ((totalVolume - previousTotalVolume) / previousTotalVolume) * 100));
//                    map.put("valuegrowth", decimalFormat.format(previousTotalValue == 0 ? (totalValue - previousTotalValue) * 100 : ((totalValue - previousTotalValue) / previousTotalValue) * 100));
//                    map.put("revenuegrowth", decimalFormat.format(previousTotalRevenue == 0 ? (totalRevenue - previousTotalRevenue) * 100 : ((totalRevenue - previousTotalRevenue) / previousTotalRevenue) * 100));
//                }
//
//                previousTotalVolume = totalVolume;
//                previousTotalValue = totalValue;
//                previousTotalRevenue = totalRevenue;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(tempDate);
            calendar.add(Calendar.DAY_OF_MONTH, 1);

            tempDate = calendar.getTime();
        }

        return responseList;

    }

    private Map getByMonthTillLastMonth() throws DatabaseException {

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");

        String tempDateStr = dateFormat.format(new Date());
//        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date startDate = calendar.getTime();

        Map<String, Map> result = new LinkedHashMap<>();

        double previousMonthValue = 0.0, previousMonthRevenue = 0.0;
        long previousMonthVolume = 0;

        int counter = 0;
        while (!tempDateStr.equalsIgnoreCase(dateFormat.format(startDate))) {

            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 59);

            Date endDate = calendar.getTime();

            Object[] response = (Object[]) coreReportDao.getReport(startDate, endDate).get(0);

            if (response == null || response.length <= 0) {

                processTransactions(startDate, endDate);
            }

            response = (Object[]) coreReportDao.getReport(startDate, endDate).get(0);

            Map<String, Object> map = new LinkedHashMap<>();

            if (counter == 0) {
                map.put("volume", response[1] == null ? 0 : response[1]);
                map.put("value", response[0] == null ? 0 : response[1]);
                map.put("revenue", response[2] == null ? 0 : response[2]);
                map.put("volumegrowth", 0.0);
                map.put("valuegrowth", 0.0);
                map.put("revenuegrowth", 0.0);
                counter++;
            } else {
                long volume = 0;
                double value = 0.0;
                double revenue = 0.0;

                if (response != null && response.length > 0) {

                    volume = response[1] == null ? 0 : ((BigDecimal) response[1]).longValue();
                    value = response[0] == null ? 0 : (Double) response[0];
                    revenue = response[1] == null ? 0 : (Double) response[2];
                }

                map.put("volume", volume);
                map.put("value", value);
                map.put("revenue", revenue);

                map.put("volumegrowth", previousMonthVolume == 0 ? volume * 100 : ((volume - previousMonthVolume) / previousMonthVolume) * 100);
                map.put("valuegrowth", previousMonthValue == 0 ? value * 100 : ((volume - previousMonthValue) / previousMonthValue) * 100);
                map.put("revenuegrowth", previousMonthRevenue == 0 ? revenue * 100 : ((volume - previousMonthRevenue) / previousMonthRevenue) * 100);

                previousMonthVolume = volume;
                previousMonthValue = value;
                previousMonthRevenue = revenue;
            }

            result.put(dateFormat.format(startDate), map);

            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            startDate = calendar.getTime();
        }

        return result;

    }

    private Map getByMonthFirstNineDays() throws DatabaseException {

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");

        String tempDateStr = dateFormat.format(new Date());
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        Date startDate = calendar.getTime();

        Map<String, Map> result = new TreeMap<>();

        double previousMonthValue = 0.0, previousMonthRevenue = 0.0;
        long previousMonthVolume = 0;

        int counter = 0;
        while (!tempDateStr.equalsIgnoreCase(dateFormat.format(startDate))) {

            calendar.set(Calendar.DAY_OF_MONTH, 9);

            Date endDate = calendar.getTime();

            Object[] response = (Object[]) coreReportDao.getReport(startDate, endDate).get(0);

            if (response == null || response.length <= 0) {

                processBreakDown(startDate, endDate);
            }

            response = (Object[]) coreReportDao.getReport(startDate, endDate).get(0);

            Map<String, Object> map = new LinkedHashMap<>();

            if (counter == 0) {
                map.put("volume", response[1] == null ? 0 : response[1]);
                map.put("value", response[0] == null ? 0 : response[0]);
                map.put("revenue", response[2] == null ? 0 : response[2]);
                map.put("volumegrowth", 0.0);
                map.put("valuegrowth", 0.0);
                map.put("revenuegrowth", 0.0);
                counter++;
            } else {
                long volume = 0;
                double value = 0.0;
                double revenue = 0.0;

                if (response != null && response.length > 0) {

                    volume = response[1] == null ? 0 : ((BigDecimal) response[1]).longValue();
                    value = response[0] == null ? 0 : (Double) response[0];
                    revenue = response[2] == null ? 0 : (Double) response[2];
                }

                map.put("volume", volume);
                map.put("value", value);
                map.put("revenue", revenue);

                map.put("volumegrowth", previousMonthVolume == 0 ? volume * 100 : ((volume - previousMonthVolume) / previousMonthVolume) * 100);
                map.put("valuegrowth", previousMonthValue == 0 ? value * 100 : ((volume - previousMonthValue) / previousMonthValue) * 100);
                map.put("revenuegrowth", previousMonthRevenue == 0 ? revenue * 100 : ((volume - previousMonthRevenue) / previousMonthRevenue) * 100);

                previousMonthVolume = volume;
                previousMonthValue = value;
                previousMonthRevenue = revenue;
            }

            result.put(dateFormat.format(startDate), map);

            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            startDate = calendar.getTime();
        }

        return result;

    }

    private Map<String, Map> getMonthToDateSummary(Date startDate) throws DatabaseException, ParseException {

        Map<String, Map> result = new LinkedHashMap<>();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        startDate = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yyyy");

        calendar.setTime(new Date());

        Date endDate = calendar.getTime();

        String tempStartString = dateFormat.format(startDate);

        String endString = dateFormat.format(endDate);

        while (!tempStartString.equalsIgnoreCase(endString)) {

            Date currentSelection = dateFormat.parse(tempStartString);

            calendar.setTime(currentSelection);
            calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());

            Date strDate = calendar.getTime();

            calendar.setTime(currentSelection);
//            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

            Date eDate = calendar.getTime();

            Map<Date, Object> list = processBreakDown(strDate, eDate);

            result.put(tempStartString, list);

//            calendar.add(Calendar.MONTH, 1);
            tempStartString = dateFormat.format(calendar.getTime());
        }

        if (tempStartString.equalsIgnoreCase(endString)) {

            calendar.setTime(endDate);
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            Date strDate = calendar.getTime();

            calendar.setTime(new Date());
            Date eDate = calendar.getTime();
            Map<Date, Object> list = processBreakDown(strDate, eDate);

            result.put(endString, list);
        }

        return result;
    }

    public double getExchangeRateInNGN(String orig) {

        try {
            Currency curr = currencyDao.findByKey("shortName", orig);
            Currency destcurr = currencyDao.findByKey("shortName", "NGN");

            ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

            if (exchangeRate == null) {
                return 1.0;
            }

            return exchangeRate.getAmount();
//        switch (orig.toUpperCase()) {
//
//            case "NGN":
//                return 1.0;
//            case "GHS":
//                return 108;
//            case "EUR":
//                return 480;
//            case "USD":
//                return 455;
//            case "CAD":
//                return 270;
//            case "GBP":
//                return 540;
//            case "KES":
//                return 3.06;
//            default:
//                return 1;
//        }
        } catch (DatabaseException ex) {
            Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 1.0;
    }

    private double getCost(Provider p, double value, boolean local, long count) {

        double cost = 0.0;
        if (local == true) {

            if (null == p.getLocalCardFeeType()) {
                cost = p.getLocalCardFee() * count;
            } else {
                switch (p.getLocalCardFeeType()) {
                    case BOTH:
                        cost = p.getLocalCardFee() * count + (p.getLocalCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        cost = (p.getLocalCardFee() * value) / 100;
                        break;
                    default:
                        cost = p.getLocalCardFee();
                        break;
                }
            }

            double configFee = p.getLocalCardFeeCap() * count;

            if (cost > configFee && p.getLocalCardFeeCap() != 0.0) {
                cost = configFee;
            }

        } else {

            if (null == p.getInterCardFeeType()) {
                cost = p.getInterCardFee() * count;
            } else {
                switch (p.getInterCardFeeType()) {
                    case BOTH:
                        cost = p.getInterCardFee() * count + (p.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        cost = (p.getInterCardFee() * value) / 100;
                        break;
                    default:
                        cost = p.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = p.getInterCardFeeCap() * count;

            if (cost > configFee && p.getInterCardFeeCap() != 0.0) {
                cost = configFee;
            }
        }

        return cost;
    }

    private double getFee(ConfigurationFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    private double getFee(MerchantFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    public void downloadMoneywaveException() {

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet worksheet = workbook.createSheet("Bank Exception report");

        int startRowIndex = 0;
        int startColIndex = 0;

        moneywaveReportBean = global.getMoneywaveReportBean();

        List<String> headers = new LinkedList<>();
        headers.add("Transaction Reference");
        headers.add("Amount");

        int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, headers, "Bank Exception Report", null);

        FillManager.build(worksheet, usedRows - 1, startColIndex,
                moneywaveReportBean != null ? moneywaveReportBean.getMoneywaveExceptions() : new HashMap<>());

        worksheet = workbook.createSheet("Monweywave Exception Report");

        headers = new LinkedList<>();
        headers.add("Transaction Reference");
        headers.add("Amount");

        usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, headers, "Monweywave Exception Report", null);

        FillManager.build(worksheet, usedRows - 1, startColIndex,
                moneywaveReportBean != null ? moneywaveReportBean.getBankExceptions() : new HashMap<>());

        response.setHeader("Content-Disposition", "attachment; filename=exception.xls");
        response.setContentType("application/vnd.ms-excel");

        ServletOutputStream out = null;

        try {

            out = response.getOutputStream();
            workbook.write(out);

            FacesContext.getCurrentInstance().responseComplete();

        } catch (IOException err) {
            err.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException err) {
                err.printStackTrace();
            }
        }
    }

    public void exportSettlementReport(String format) throws FileNotFoundException, IOException {

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet worksheet = workbook.createSheet("Settlement Report");

        int startRowIndex = 0;
        int startColIndex = 0;

        List<SettlementViewModel> viewModels = (List<SettlementViewModel>) simpleCache.get("settlementData");

        List<String> headers = new LinkedList<>();
        headers.add("Provider");
        headers.add("Merchant Id");
        headers.add("Merchant Name");
        headers.add("Amount");
        headers.add("Volume");
        headers.add("Amount To Settle");
        headers.add("Fee");
        headers.add("Cost");

        ServletOutputStream out = null;

        if ("pdf".equalsIgnoreCase(format)) {

            String fileName = PdfUtilExport.generatePdf("FLUTTERWAVE", "Settlement Report", headers, (List) viewModels);

            response.setHeader("Content-Disposition", "attachment; filename=settlementreport.pdf");
            response.setContentType("application/pdf");

            FileInputStream fileInputStream = new FileInputStream(fileName);
            out = response.getOutputStream();

            try {
                IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                out.flush();
            } catch (Exception ex) {
                if (ex != null) {
                    ex.printStackTrace();
                }
            }

        } else {

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, headers, "Settlement Report", null);

            FillManager.build(worksheet, usedRows - 1, startColIndex, viewModels);

            response.setHeader("Content-Disposition", "attachment; filename=settlementreport.xls");
            response.setContentType("application/vnd.ms-excel");

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }

    }

    public void exportRaveSettlementReport(String format) throws FileNotFoundException, IOException {

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet worksheet = workbook.createSheet("Settlement Report");

        int startRowIndex = 0;
        int startColIndex = 0;

        List<RaveSettlementViewModel> viewModels = (List<RaveSettlementViewModel>) global.getRaveSettlementReport();

        List<String> headers = new LinkedList<>();
        headers.add("Provider");
        headers.add("Merchant Id");
        headers.add("Merchant Name");
        headers.add("Amount");
        headers.add("Amount To Settle");
        headers.add("Volume");
        headers.add("Fee");
        headers.add("Account Number");
        headers.add("Bank");

        ServletOutputStream out = null;

        if ("pdf".equalsIgnoreCase(format)) {

            String fileName = PdfUtilExport.generatePdf("FLUTTERWAVE", "Rave Settlement Report", headers, (List) viewModels);

            response.setHeader("Content-Disposition", "attachment; filename=ravesettlementreport.pdf");
            response.setContentType("application/pdf");

            FileInputStream fileInputStream = new FileInputStream(fileName);
            out = response.getOutputStream();

            try {
                IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                out.flush();
            } catch (Exception ex) {
                if (ex != null) {
                    ex.printStackTrace();
                }
            }

        } else {

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, headers, "Settlement Report", null);

            FillManager.buildRave(worksheet, usedRows - 1, startColIndex, viewModels);

            response.setHeader("Content-Disposition", "attachment; filename=ravesettlementreport.xls");
            response.setContentType("application/vnd.ms-excel");

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }

    }

    public void exportRaveTransactionSettlementReport(String format) throws FileNotFoundException, IOException {

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet worksheet = workbook.createSheet("Rave Transaction Settlement Report");

        int startRowIndex = 0;
        int startColIndex = 0;

        List<RaveSettlementModel> viewModels = (List<RaveSettlementModel>) global.getRaveTransactionSettlement();

//        private String businessName;
//    private double amount;
//    private double appFee;
//    private double merchantFee;
//    private String type;
//    private long volume;
//    private String currency;
//    private String parent;
//    private String accountNumber;
//    private String bankCode;
//    private String headers.add("Bank");;
        List<String> headers = new LinkedList<>();
        headers.add("Business Name");
        headers.add("Parent");
        headers.add("Charged Amount");
        headers.add("App Fee");
        headers.add("Merchant Fee");
        headers.add("Volume");
        headers.add("Currency");
        headers.add("Account Number");
        headers.add("Bank");
        headers.add("Settlement Class");

        ServletOutputStream out = null;

        if ("pdf".equalsIgnoreCase(format)) {

            String fileName = PdfUtilExport.generatePdf("FLUTTERWAVE", "Rave Transaction Settlement Report", headers, (List) viewModels);

            response.setHeader("Content-Disposition", "attachment; filename=ravesettlementreport.pdf");
            response.setContentType("application/pdf");

            FileInputStream fileInputStream = new FileInputStream(fileName);
            out = response.getOutputStream();

            try {
                IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                out.flush();
            } catch (Exception ex) {
                if (ex != null) {
                    ex.printStackTrace();
                }
            }

        } else {

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, headers, "Settlement Report", null);

            FillManager.buildRaveSettlement(worksheet, usedRows - 1, startColIndex, viewModels);

            response.setHeader("Content-Disposition", "attachment; filename=ravesettlementreport.xls");
            response.setContentType("application/vnd.ms-excel");

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }

    }
    
    public void exportPosTransactionSettlementReport(String format) throws FileNotFoundException, IOException {

        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet worksheet = workbook.createSheet("POS Transaction Settlement Report");

        int startRowIndex = 0;
        int startColIndex = 0;

        List<PosSettlementModel> viewModels = (List<PosSettlementModel>) global.getPosSettlementReport();

//        private String businessName;
//    private double amount;
//    private double appFee;
//    private double merchantFee;
//    private String type;
//    private long volume;
//    private String currency;
//    private String parent;
//    private String accountNumber;
//    private String bankCode;
//    private String headers.add("Bank");;
        List<String> headers = new LinkedList<>();
        
//        private String name;
//    private String terminalId;
//    private double amount;
//    private double fee;
//    private String currency = "NGN";
//    private String type;
//    private Date transactionDate;
        headers.add("Name");
        headers.add("Terminal Id");
        headers.add("Amount");
        headers.add("Fee");
        headers.add("Currency");
        headers.add("Type");
        headers.add("Transaction Date");

        ServletOutputStream out = null;

        String dateformat = Utility.formatDate(new Date(), "yyyyMMddHHmmss");
        
        if ("pdf".equalsIgnoreCase(format)) {

            String fileName = PdfUtilExport.generatePdf("FLUTTERWAVE", "Transaction Settlement Report", headers, (List) viewModels);

            response.setHeader("Content-Disposition", "attachment; filename=possettlementreport"+dateformat+".pdf");
            response.setContentType("application/pdf");

            FileInputStream fileInputStream = new FileInputStream(fileName);
            out = response.getOutputStream();

            try {
                IOUtils.copy(fileInputStream, out);

//                    if(baos != null)
                out.flush();
            } catch (Exception ex) {
                if (ex != null) {
                    ex.printStackTrace();
                }
            }

        } else {

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, headers, "Settlement Report", null);

            FillManager.buildPosSettlement(worksheet, usedRows - 1, startColIndex, viewModels);

            response.setHeader("Content-Disposition", "attachment; filename=possettlementreport"+dateformat+".xls");
            response.setContentType("application/vnd.ms-excel");

            try {

                out = response.getOutputStream();
                workbook.write(out);

                FacesContext.getCurrentInstance().responseComplete();

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        }

    }

    public Map<String, String> getCardTypes() {

        Map<String, String> data = new HashMap<>();
        data.put("MasterCard", "MC");
        data.put("Visa", "VC");
        data.put("Verve", "VERVE");
        data.put("American_Express", "AMEX");
        data.put("Discover", "DISC");

        return data;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void fetchRRN() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            if (fileName.toLowerCase().endsWith("csv")) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(uploadModel.getFile().getInputStream()));

                String line = null;

                List<FetchRRNModel> fetchRRNModels = new ArrayList<>();
                
                
//                StringBuilder builder = new StringBuilder();
                
                String rrn = reader.lines().filter(x -> x != null).map(x -> "'"+x+"'").collect(Collectors.joining(","));
                               
                
                fetchRRNModels = transactionWarehouseDao.getRRNModels(uploadModel.getParameterName(), rrn);
                
                if(fetchRRNModels == null)
                    fetchRRNModels = new ArrayList<>();
                
                List<FetchRRNModel> fetchRRNModelNew = new ArrayList<>();
                
                if(!fetchRRNModels.isEmpty()){
                    
                    Map<String, String> merchantIdTempList = new HashMap<>();
                    
                    for(FetchRRNModel fetchRRNModel : fetchRRNModels){
                        
                        FetchRRNModel model = fetchRRNModel;
                        
                        String merchantName = merchantIdTempList.getOrDefault(fetchRRNModel.getMerchantId(), null);

                        if(merchantName == null){
                             merchantName = coreMerchantDao.getMerchantName(fetchRRNModel.getMerchantId());
                             
                             if(merchantName != null)
                                 merchantIdTempList.put(fetchRRNModel.getMerchantId(), merchantName);
                        }
                        
                        if(merchantName != null)
                            model.setMerchantName(merchantName);
                        
                        fetchRRNModelNew.add(model);
                    }
                }
                
                
//                while ((line = reader.readLine()) != null) {
//
////                    if(i++ == 0)
////                        continue;
//                    FetchRRNModel model = transactionDao.getRRNModel(line.trim());
//
////                     = new FetchRRNModel();
////                    model.setRrn(line);
//
////                    if (transaction == null) {
////
////                        fetchRRNModels.add(model);
////                        continue;
////                    }
////
////                    model.setAmount(transaction.getAmount());
////                    model.setFlutterReference(transaction.getFlwTxnReference());
////                    model.setTransactionDate(transaction.getDatetime());
////                    model.setCurrency(transaction.getTransactionCurrency());
//
//                    fetchRRNModels.add(model);
//                }

                List<String> headers = (List) Utility.getClassFields(FetchRRNModel.class);

                HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

                String output = CsvUtil.writeLine(headers, (List) fetchRRNModelNew);

                ServletOutputStream out = null;

                try {

                    response.setHeader("Content-Disposition", "attachment; filename=transactionrecord.csv");
                    response.setContentType("application/csv");

                    out = response.getOutputStream();
                    out.write(output.getBytes("UTF-8"));

                    FacesContext.getCurrentInstance().responseComplete();

                } catch (IOException err) {
                    err.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException err) {
                        err.printStackTrace();
                    }
                }

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to get records");
        }
    }

    public void pullAirtimeTransaction() {

        try {
            String fileName = uploadModel.getFile().getSubmittedFileName().toLowerCase();

            if (fileName.toLowerCase().endsWith("csv")) {

                String username = Global.EMAIL_USERNAME;
                String password = Global.EMAIL_PASSWORD;

                SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                        true, Global.USE_STARTTLS, false);

                String email = SecurityUtils.getSubject().getPrincipal() + "";

                notificationManager.processAirtimeTransaction(uploadModel.getFile().getInputStream(), new String[]{email}, mailSender);

                JsfUtil.addSuccessMessage("File is being processed and sent to your email when done.");

            } else {
                JsfUtil.addErrorMessage("File format not supported. Only CSV is allowed");
            }
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            JsfUtil.addErrorMessage("Unable to get records");
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author emmanueladeyemi
 */
public class FetchComplianceResponse {

    /**
     * @return the registeredName
     */
    public String getRegisteredName() {
        return registeredName;
    }

    /**
     * @param registeredName the registeredName to set
     */
    public void setRegisteredName(String registeredName) {
        this.registeredName = registeredName;
    }

    /**
     * @return the registeredNumber
     */
    public String getRegisteredNumber() {
        return registeredNumber;
    }

    /**
     * @param registeredNumber the registeredNumber to set
     */
    public void setRegisteredNumber(String registeredNumber) {
        this.registeredNumber = registeredNumber;
    }

    /**
     * @return the dateofIncorporation
     */
    public String getDateofIncorporation() {
        return dateofIncorporation;
    }

    /**
     * @param dateofIncorporation the dateofIncorporation to set
     */
    public void setDateofIncorporation(String dateofIncorporation) {
        this.dateofIncorporation = dateofIncorporation;
    }

    /**
     * @return the tradingName
     */
    public String getTradingName() {
        return tradingName;
    }

    /**
     * @param tradingName the tradingName to set
     */
    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    /**
     * @return the registeredAddress
     */
    public String getRegisteredAddress() {
        return registeredAddress;
    }

    /**
     * @param registeredAddress the registeredAddress to set
     */
    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    /**
     * @return the operationalAddress
     */
    public String getOperationalAddress() {
        return operationalAddress;
    }

    /**
     * @param operationalAddress the operationalAddress to set
     */
    public void setOperationalAddress(String operationalAddress) {
        this.operationalAddress = operationalAddress;
    }

    /**
     * @return the companyType
     */
    public String getCompanyType() {
        return companyType;
    }

    /**
     * @param companyType the companyType to set
     */
    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    /**
     * @return the companyRegDocumentPath
     */
    public String getCompanyRegDocumentPath() {
        return companyRegDocumentPath;
    }

    /**
     * @param companyRegDocumentPath the companyRegDocumentPath to set
     */
    public void setCompanyRegDocumentPath(String companyRegDocumentPath) {
        this.companyRegDocumentPath = companyRegDocumentPath;
    }

    /**
     * @return the directorIdPath
     */
    public String getDirectorIdPath() {
        return directorIdPath;
    }

    /**
     * @param directorIdPath the directorIdPath to set
     */
    public void setDirectorIdPath(String directorIdPath) {
        this.directorIdPath = directorIdPath;
    }

    /**
     * @return the contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * @param contactName the contactName to set
     */
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    /**
     * @return the contactPhone
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * @param contactPhone the contactPhone to set
     */
    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    /**
     * @return the contactAddress
     */
    public String getContactAddress() {
        return contactAddress;
    }

    /**
     * @param contactAddress the contactAddress to set
     */
    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    /**
     * @return the contactEmail
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * @param contactEmail the contactEmail to set
     */
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     * @return the industry
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * @param industry the industry to set
     */
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    /**
     * @return the products
     */
    public String getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(String products) {
        this.products = products;
    }

    /**
     * @return the productWebUrl
     */
    public String getProductWebUrl() {
        return productWebUrl;
    }

    /**
     * @param productWebUrl the productWebUrl to set
     */
    public void setProductWebUrl(String productWebUrl) {
        this.productWebUrl = productWebUrl;
    }

    /**
     * @return the operatingLicencePath
     */
    public String getOperatingLicencePath() {
        return operatingLicencePath;
    }

    /**
     * @param operatingLicencePath the operatingLicencePath to set
     */
    public void setOperatingLicencePath(String operatingLicencePath) {
        this.operatingLicencePath = operatingLicencePath;
    }

    /**
     * @return the amlPolicyPath
     */
    public String getAmlPolicyPath() {
        return amlPolicyPath;
    }

    /**
     * @param amlPolicyPath the amlPolicyPath to set
     */
    public void setAmlPolicyPath(String amlPolicyPath) {
        this.amlPolicyPath = amlPolicyPath;
    }

    /**
     * @return the shareHolder1
     */
    public String getShareHolder1() {
        return shareHolder1;
    }

    /**
     * @param shareHolder1 the shareHolder1 to set
     */
    public void setShareHolder1(String shareHolder1) {
        this.shareHolder1 = shareHolder1;
    }

    /**
     * @return the shareHolder2
     */
    public String getShareHolder2() {
        return shareHolder2;
    }

    /**
     * @param shareHolder2 the shareHolder2 to set
     */
    public void setShareHolder2(String shareHolder2) {
        this.shareHolder2 = shareHolder2;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the scumlCertificatePath
     */
    public String getScumlCertificatePath() {
        return scumlCertificatePath;
    }

    /**
     * @param scumlCertificatePath the scumlCertificatePath to set
     */
    public void setScumlCertificatePath(String scumlCertificatePath) {
        this.scumlCertificatePath = scumlCertificatePath;
    }

    /**
     * @return the createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the approvedOn
     */
    public String getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(String approvedOn) {
        this.approvedOn = approvedOn;
    }
    
    @JsonProperty(value = "registeredname")
    private String registeredName;
    @JsonProperty(value = "registerednumber")
    private String registeredNumber;
    @JsonProperty(value = "dateofincorporation")
    private String dateofIncorporation;  
    @JsonProperty(value = "tradingname")
    private String tradingName;
    @JsonProperty(value = "registeredaddress")
    private String registeredAddress;
    @JsonProperty(value = "operationaladdress")
    private String operationalAddress;
    @JsonProperty(value = "companytype")
    private String companyType;
    @JsonProperty(value = "companyregdocumentpath")
    private String companyRegDocumentPath;
    @JsonProperty(value = "directoridpath")
    private String directorIdPath;
    @JsonProperty(value = "contactname")
    private String contactName;
    @JsonProperty(value = "contactphone")
    private String contactPhone;  
    @JsonProperty(value = "contactaddress")
    private String contactAddress;
    @JsonProperty(value = "contactemail")
    private String contactEmail;    
    private String industry;
    private String products;
    @JsonProperty(value = "productweburl")
    private String productWebUrl;
    @JsonProperty(value = "operatinglicencepath")
    private String operatingLicencePath;    
    @JsonProperty(value = "amlpolicypath")
    private String amlPolicyPath;
    @JsonProperty(value = "shareholder1")
    private String shareHolder1;
    @JsonProperty(value = "shareholder2")
    private String shareHolder2;
    @JsonProperty(value = "country")
    private String country;
    @JsonProperty(value = "merchantid")
    private String merchantId;
    @JsonProperty(value = "scumlcertificatepath")
    private String scumlCertificatePath;    
    @JsonProperty(value = "createdon")
    private String createdOn;    
    private String status;   
    @JsonProperty(value = "approvedon")
    private String approvedOn;
}

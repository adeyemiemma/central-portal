/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class GetMposTerminalRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the pageNumber
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * @param pageNumber the pageNumber to set
     */
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    
    @NotBlank(message = "Pwc Merchant id must be provided")
    @JsonProperty(value = "pwcmerchantid")
    private String merchantId;
    @NotBlank(message = "Merchant code must be provided")
    @JsonProperty(value = "merchantcode")
    private String merchantCode;
    @JsonProperty(value = "pagenumber")
    private int pageNumber = 0;
    @JsonProperty(value = "pagesize")
    private int pageSize = 20; 
    @NotBlank(message = "Hash value must be provided")
    private String hash;

    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("merchantid", merchantId)
                .put("merchantcode", merchantCode)
                .put("pagenumber", pageNumber)
                .put("pagesize", pageSize)
                .put("hash", hash);
        
        return jSONObject.toString();
    }
    
    public String toHashableString(){
        
        StringBuilder builder = new StringBuilder();
        builder.append(merchantId)
                .append(merchantCode);
        
        return builder.toString();
    }
    
    
}

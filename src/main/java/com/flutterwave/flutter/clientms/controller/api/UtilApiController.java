/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.dao.AccountQueryDao;
import com.flutterwave.flutter.clientms.dao.AccountQueryTransactionDao;
import com.flutterwave.flutter.clientms.dao.CurrencyDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.ProviderDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.WalletDao;
import com.flutterwave.flutter.clientms.model.AccountQuery;
import com.flutterwave.flutter.clientms.model.AccountQueryTransaction;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.Wallet;
import com.flutterwave.flutter.clientms.service.FlutterwaveApiService;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.PageResult;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;

/**
 *
 * @author emmanueladeyemi
 */
@RequestScoped
@Path(value = "/util/")
public class UtilApiController {

    @EJB
    private FlutterwaveApiService flutterwaveApiService;
    @EJB
    private WalletDao walletDao;
    @EJB
    private ProviderDao providerDao;
    @EJB
    private AccountQueryDao accountQueryDao;
    @EJB
    private AccountQueryTransactionDao accountQueryTransactionDao;
    @EJB
    private CurrencyDao currencyDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private UserDao userDao;
    
    @Path(value = "/bank/namecheck")
    @Produces(value = MediaType.APPLICATION_JSON)
    @GET
    public Response doNameCheck(@QueryParam(value = "bank") String bank) {

        if (bank == null) {
            return Response.status(Response.Status.OK).entity(false).build();
        }

        if ("access".equalsIgnoreCase(bank)) {
            boolean status = flutterwaveApiService.accountEnquiry("0690000031", "044");
            return Response.status(Response.Status.OK).entity(status).build();
        } else if ("gtb".equalsIgnoreCase(bank)) {
            boolean status = flutterwaveApiService.accountEnquiry("0921318712", "058");
            return Response.status(Response.Status.OK).entity(status).build();
        } else {
            return Response.status(Response.Status.OK).entity(false).build();
        }
    }

    @Path(value = "/wallet/balance")
    @Produces(value = MediaType.APPLICATION_JSON)
    @GET
    public Response getWalletBalance(@QueryParam(value = "category") String category, @QueryParam(value = "product") String product,
            @QueryParam(value = "id") String id, @QueryParam(value = "currency") String currencyCode) {

        Map<String, Object> response = new HashMap<>();

        try {

            if (Utility.WalletCategory.valueOf(category) == Utility.WalletCategory.MERCHANT) {

                if (product == null) {
                    response.put("responsecode", "01");
                    response.put("responsemessage", "product not found");
                    response.put("amount", 0.0);

                    return Response.status(Response.Status.OK).entity(response).build();
                }

                Currency currency = currencyDao.findByKey("shortName", currencyCode);

                Product p = productDao.findByKey("name", product);
                
                Wallet wallet = walletDao.findByQuery(p, id, Utility.WalletCategory.valueOf(category), currency);

                if (wallet == null) {
                    
                    Wallet merchantWallet = new Wallet();
                    merchantWallet.setAmount(0.0);
                    merchantWallet.setApprovedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
                    merchantWallet.setApprovedOn(new Date());
                    merchantWallet.setCategory(Utility.WalletCategory.MERCHANT);
                    merchantWallet.setProduct(p);
                    merchantWallet.setCategoryId(id);
                    merchantWallet.setCurrency(currency);
                    merchantWallet.setCreatedBy(userDao.findByKey("username", SecurityUtils.getSubject().getPrincipal().toString()));
                    merchantWallet.setCreatedOn(new Date());
                    walletDao.create(merchantWallet);
                    
                    response.put("responsecode", "02");
                    response.put("responsemessage", "wallet not found");
                    response.put("amount", 0.0);
                } else {

                    response.put("responsecode", "00");
                    response.put("responsemessage", "successful");
                    response.put("amount", wallet.getAmount());
                }

            } else if (Utility.WalletCategory.valueOf(category) == Utility.WalletCategory.MERCHANT) {

                Provider provider = providerDao.findByKey("shortName", id);

                if (provider == null) {
                    response.put("responsecode", "01");
                    response.put("responsemessage", "provider not found");
                    response.put("amount", 0.0);
                } else {
                    Currency currency = currencyDao.findByKey("shortName", currencyCode);

                    Wallet wallet = walletDao.findByQuery(null, provider.getShortName(), Utility.WalletCategory.valueOf(category), currency);

                    if (wallet == null) {
                        response.put("responsecode", "02");
                        response.put("responsemessage", "wallet not found");
                        response.put("amount", 0.0);
                    } else {

                        response.put("responsecode", "00");
                        response.put("responsemessage", "successful");
                        response.put("amount", wallet.getAmount());
                    }
                }

            }

        } catch (Exception ex) {

            if (ex != null) {
                ex.printStackTrace();
            }

            response.put("responsecode", "RR");
            response.put("responsemessage", "Generic Error");
            response.put("amount", 0.0);
        }

        return Response.status(Response.Status.OK).entity(response).build();

    }

    @Path(value = "/accounts")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAccountForQuery() throws DatabaseException {

        List<AccountQuery> list = accountQueryDao.findAll();

        return Response.status(Response.Status.OK).entity(list).build();
    }
    
    @Path(value = "/accounts/query")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAccountQueryStatus() throws DatabaseException{
        
        List<AccountQuery> list = accountQueryDao.findAll();
        
        PageResult pageResult = new PageResult();
        
        
        if(list == null){
            
            pageResult.setData(new ArrayList());
            pageResult.setRecordsFiltered(0L);
            pageResult.setRecordsTotal(0L);
            
            return Response.status(Response.Status.OK).entity(pageResult).build();
        }

        
        List<AccountQueryTransaction> queryTransactions = accountQueryTransactionDao.findRecent();
        
        if(queryTransactions == null || queryTransactions.isEmpty()){
            pageResult.setData(new ArrayList());
            pageResult.setRecordsFiltered(0L);
            pageResult.setRecordsTotal(0L);
            
            return Response.status(Response.Status.OK).entity(pageResult).build();
        }
        
        List data = queryTransactions.stream().map((AccountQueryTransaction aqt) ->{
            
            Map<String, Object> map = new HashMap<>();
            map.put("accountNo", aqt.getAccountNo());
            map.put("bankName", aqt.getAccountQuery().getBank().getName());
            map.put("bankCode", aqt.getAccountQuery().getBank().getCode());
            map.put("createdOn", aqt.getAccountQuery().getCreatedOn());
            map.put("status", aqt.getStatus().toString());
            
            return map;
        }).collect(Collectors.<Map>toList());
            
        pageResult.setData(data);
        pageResult.setRecordsFiltered(Long.valueOf(queryTransactions.size()+""));
        pageResult.setRecordsTotal(Long.valueOf(queryTransactions.size()+""));
        
        return Response.status(Response.Status.OK).entity(pageResult).build();
    }
    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.SectorDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.dao.maker.SectorMDao;
import com.flutterwave.flutter.clientms.model.Sector;
import com.flutterwave.flutter.clientms.model.Sector;
import com.flutterwave.flutter.clientms.model.Sector;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Sector;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.maker.SectorM;
import com.flutterwave.flutter.clientms.model.maker.SectorM;
import com.flutterwave.flutter.clientms.model.maker.SectorM;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.JsfUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import javax.transaction.Transactional;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "sectorController")
@SessionScoped
public class SectorController implements Serializable {

    private Sector sector;

    private DataModel allSector = null;
    private DataModel allUnauthSector = null;

    @EJB
    private SectorDao sectorDao;
    @EJB
    private SectorMDao sectorMDao;
    @EJB
    private LogService logService;
    @EJB
    private UserDao userDao;

    private PaginationHelper pagination, paginationHelper;

    public String prepareCreate() {

        sector = new Sector();

        return "create";
    }

    @Transactional
    public String create() {

        if (sector != null) {
            sector.setCreatedOn(new Date());
        }

        try {
            Object userString = SecurityUtils.getSubject().getPrincipal();
            User user = userDao.findByKey("email", userString + "");

            Sector c = sectorDao.findByKey("name", sector.getName());

            if (c != null) {

                Log log = new Log();
                log.setAction("Create sector: " + sector.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Duplicate sector with name " + c.getName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Sector with name %s exists", sector.getName()));

                return "create";
            }

            boolean status = findUnauthorizedSector(sector.getName(), true);

            if (status == true) {

                Log log = new Log();
                log.setAction("Create sector: " + sector.getName());
                log.setCreatedOn(new Date());
                log.setDescription("Pending sector with name " + sector.getName());
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.SUCCESSFUL);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                JsfUtil.addErrorMessage(String.format("Pending approval on Sector with  name %s exists", sector.getName()));

                return "create";
            }

            SectorM sectorM = new SectorM();
            sectorM.setCreatedOn(new Date());
            sectorM.setCreatedBy(user);
            sectorM.setName(sector.getName());
            sectorM.setStatus(Status.PENDING);

            sectorMDao.create(sectorM);

            Log log = new Log();
            log.setAction("Create sector: " + sector.getName());
            log.setCreatedOn(new Date());
            log.setDescription("sector has been created and submitted for approval successfully");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            JsfUtil.addSuccessMessage(String.format("Sector %s has been added successfully waiting for approval", sector.getName()));

            sector = new Sector();

            logService.log(log);

        } catch (DatabaseException de) {

            try {
                Log log = new Log();
                log.setAction("create sector " + sector.getName());
                log.setCreatedOn(new Date());
                log.setDescription("creation of sector");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (de != null) {

                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception ex) {
            }

            if (de != null) {
                de.printStackTrace();
                JsfUtil.addErrorMessage(de.getMessage());
            }
        }

        recreate();

        return prepareCreate();
    }

    /**
     *
     * @param data
     * @return
     */
    public boolean findUnauthorizedSector(String data, boolean creation) {

        try {
            List<SectorM> countries = sectorMDao.find("name", sector.getName());

            if (countries != null && !countries.isEmpty()) {

                SectorM cM = countries.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {
                    return true;
                }
            }

        } catch (Exception exception) {
            Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return false;
    }

    public String prepateUpdate() {

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String param = params.get("editParam") + "";

        try {
            sector = sectorDao.find(Long.parseLong(param));

            Log log = new Log();
            log.setAction("Update Sector " + sector.getName());
            log.setCreatedOn(new Date());
            log.setDescription("about to update sector");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

        } catch (DatabaseException de) {
            Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, de);

            try {
                Log log = new Log();
                log.setAction("update sector " + sector.getName());
                log.setCreatedOn(new Date());
                log.setDescription("update sector");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (de != null) {
//                    log.setStatusMessage(de.getMessage());

                    String detailedString = Stream.of(de.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception ex) {
            }
        }

        return "edit";
    }
    
    public String update() {

        try {

            Object userString = SecurityUtils.getSubject().getPrincipal();
            
            User user = userDao.findByKey("email", userString + "");

            Sector curr = sectorDao.findByKey("name", sector.getName());

            if (curr != null && curr.getId() != sector.getId()) {

                JsfUtil.addErrorMessage("Short name exists");

                return "edit";
            }

            List<SectorM> list = sectorMDao.find("modelId", sector.getId());

            if (list != null && !list.isEmpty()) {

                SectorM cM = list.stream().filter(x -> x.getApprovedOn() == null).findFirst().orElse(null);

                if (cM != null) {

                    JsfUtil.addErrorMessage("A pending update exists");
                    return "edit";
                }
            }

            SectorM sectorM = new SectorM();
            sectorM.setModelId(sector.getId());
            sectorM.setCreatedOn(new Date());
            sectorM.setName(sector.getName());
            sectorM.setCreatedBy(user);

            sectorM = sectorMDao.create(sectorM);

            Log log = new Log();
            log.setAction("Update Sector " + sector.getName());
            log.setCreatedOn(new Date());
            log.setDescription("Sector updated has been created and successfully updated");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

            logService.log(log);

            if (sectorM == null) {
                throw new DatabaseException("Unable to update record");
            }

            JsfUtil.addSuccessMessage("Sector has been updated succesfully waiting for authorization");

            paginationHelper = null;

            return "edit";

        } catch (DatabaseException ex) {
            Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);

            if (ex != null) {
                JsfUtil.addErrorMessage("" + ex.getMessage());
            } else {
                JsfUtil.addErrorMessage("Unable to update record , please try again later");
            }

            try {
                Log log = new Log();
                log.setAction("update sector " + sector.getName());
                log.setCreatedOn(new Date());
                log.setDescription("update sector");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
                log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

                if (ex != null) {
//                    log.setStatusMessage(ex.getMessage());

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }

        return "edit";
    }

    public void recreate() {
        pagination = null;
        paginationHelper = null;
    }

    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("sectorupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("sectorupdated", false);
        }

        if (pagination == null || pagination.getItemsCount() <= 0) {
            allSector = getPaginationHelper().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Sector");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All Sector");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

        logService.log(log);

        return allSector;
    }

    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("sectorupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationHelper = null;
            session.setAttribute("sectorupdatedm", false);
        }

        Log log = new Log();
        log.setAction("Fetch Unauthorized Sector");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching Unauthorized Sector");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");

        logService.log(log);

        if (paginationHelper == null) {
            allUnauthSector = getPaginationHelperUnauth().createPageDataModel();
        }

        return allUnauthSector;
    }

    public PaginationHelper getPaginationHelper() {

        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public long getItemsCount() {
                    return sectorDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(sectorDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }

    public PaginationHelper getPaginationHelperUnauth() {

        if (paginationHelper == null) {
            paginationHelper = new PaginationHelper(20) {
                @Override
                public long getItemsCount() {
                    return sectorMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(sectorMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(SectorController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationHelper;
    }

    public Sector getSector() {

        if (sector == null) {
            sector = new Sector();
        }

        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }
    
    public List<Sector> getAllSectors() throws DatabaseException{
        
        return sectorDao.findAll();
    }
    
    public Sector getSector(long id){
        
        try{
            return sectorDao.find(id);
        }catch(Exception ex){
            if(ex != null)
                ex.printStackTrace();
        }
        
        return null;
    }
    
    @FacesConverter(value = "sectorConverter")
    public static class SectorConverter implements javax.faces.convert.Converter {

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {

            if (value == null || value.length() == 0) {
                return null;
            }

            SectorController sectorController = (SectorController) context.getApplication()
                    .getELResolver().getValue(context.getELContext(), null, "sectorController");

            return sectorController.getSector(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            if (value == null) {
                return null;
            }
            if (value instanceof Sector) {
                Sector o = (Sector) value;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + value + " is of type " + value.getClass().getName() + "; expected type: " + Sector.class.getName());
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller;

import com.flutterwave.flutter.clientms.dao.ChargeBackDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.maker.ChargeBackMDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.PaginationHelper;
import com.flutterwave.flutter.clientms.viewmodel.ChargeBackFilter;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 *
 * @author emmanueladeyemi
 */
@Named(value = "chargebackController")
@SessionScoped
public class ChargeBackController implements Serializable {
    
    @EJB
    private ChargeBackMDao chargeBackMDao;
    @EJB
    private ChargeBackDao chargeBackDao;
    
    @EJB
    private LogService logService;
    
    private ChargeBackFilter chargeBackFilter;
    
    private Date startDate = null, endDate = null;
    
    private PaginationHelper paginationUnauth, pagination, paginationMerchant;
    
    private DataModel unauthdataModel, dataModel, dataModelMerchant;
    
    public PaginationHelper getPagination() {

        if (pagination == null) {
            pagination = new PaginationHelper(50) {
                @Override
                public long getItemsCount() {
                    return chargeBackDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(chargeBackDao.find(getPageFirstItem(), getPageSize(), startDate, endDate, null, null, getChargeBackFilter().getCurrency(), 0, null).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return pagination;
    }
    
    public PaginationHelper getPaginationMerchant() {

        if (paginationMerchant == null) {
            paginationMerchant = new PaginationHelper(50) {
                @Override
                public long getItemsCount() {
                    return chargeBackDao.count();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(chargeBackDao.find(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationMerchant;
    }
    
    public DataModel getAll() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("cchargeupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            pagination = null;
            session.setAttribute("cchargeupdated", false);
        }

        if (pagination == null) {
            dataModel = getPagination().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Charge Back ");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All charge back ");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log, null);

        return dataModel;
    }
    
    public DataModel getAllByMerchant() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("cchargeupdated");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationMerchant = null;
            session.setAttribute("cchargeupdated", false);
        }

        if (paginationMerchant == null) {
            dataModelMerchant = getPagination().createPageDataModel();
        }

        Log log = new Log();
        log.setAction("Fetch All Charge Back ");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching All charge back ");
        log.setLevel(LogLevel.Info);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(LogState.FINISH);

        logService.log(log, null);

        return dataModelMerchant;
    }
    
    /**
     * This called to reload 
     */
    public void recreate() {
        pagination = null;
        paginationUnauth = null;
    }
    
    
    public DataModel getUnauth() {

        Session session = SecurityUtils.getSubject().getSession();

        Object obj = session.getAttribute("cchargeupdatedm");

        boolean status = obj == null ? false : Boolean.valueOf(obj + "");

        if (status == true) {
            paginationUnauth = null;
            unauthdataModel = null;
            session.setAttribute("cchargeupdatedm", false);
        }

//        Log log = new Log();
//        log.setAction("Fetch All Unauth ");
//        log.setCreatedOn(new Date());
//        log.setDescription("Fetching All Unauth ");
//        log.setLevel(LogLevel.Info);
//        log.setStatus(LogStatus.SUCCESSFUL);
//        log.setUsername(SecurityUtils.getSubject().getPrincipal() + "");
//        log.setLogDomain(LogDomain.ADMIN);
//        log.setLogState(LogState.FINISH);
//
//        logService.log(log, null);

        if (paginationUnauth == null || unauthdataModel == null) {
            unauthdataModel = getPaginationUnauth().createPageDataModel();
        }

        return unauthdataModel;
    }
    
    public PaginationHelper getPaginationUnauth() {

        if (paginationUnauth == null) {
            paginationUnauth = new PaginationHelper(50) {
                @Override
                public long getItemsCount() {
                    return chargeBackMDao.countUnauth();
                }

                @Override
                public DataModel createPageDataModel() {
                    try {
                        return new ListDataModel(chargeBackMDao.findUnauth(getPageFirstItem(), getPageSize()).getContent());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    return new ListDataModel();
                }
            };
        }

        return paginationUnauth;
    }
    
    /**
     * @return the chargeBackFilter
     */
    public ChargeBackFilter getChargeBackFilter() {
        
        if(chargeBackFilter == null)
            chargeBackFilter = new ChargeBackFilter();
        
        return chargeBackFilter;
    }

    /**
     * @param chargeBackFilter the chargeBackFilter to set
     */
    public void setChargeBackFilter(ChargeBackFilter chargeBackFilter) {
        this.chargeBackFilter = chargeBackFilter;
    }
    
    public String filter(){
        
        String range = chargeBackFilter.getRange();
        
        startDate = null;
        endDate = null;
        
        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                startDate = dateFormat.parse(splitDate[0]);
                endDate = dateFormat.parse(splitDate[1]);
            } catch (ParseException pe) {
                startDate = endDate = null;
            }

        }
       
        pagination = null;
        
        return null;
    }
    
    public String clearFilter(){
        
        startDate = endDate = null;
        pagination = null;
        
        chargeBackFilter = new ChargeBackFilter();
        
        return "list";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.util.Utility;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class RegisterTerminalRequest {

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the batchNo
     */
    public String getBatchNo() {
        return batchNo;
    }

    /**
     * @param batchNo the batchNo to set
     */
    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    /**
     * @return the serialNo
     */
    public String getSerialNo() {
        return serialNo;
    }

    /**
     * @param serialNo the serialNo to set
     */
    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    /**
     * @return the dateIssued
     */
    public String getDateIssued() {
        return dateIssued;
    }

    /**
     * @param dateIssued the dateIssued to set
     */
    public void setDateIssued(String dateIssued) {
        this.dateIssued = dateIssued;
    }

    /**
     * @return the receivedBy
     */
    public String getReceivedBy() {
        return receivedBy;
    }

    /**
     * @param receivedBy the receivedBy to set
     */
    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }

    /**
     * @return the issuedBy
     */
    public String getIssuedBy() {
        return issuedBy;
    }

    /**
     * @param issuedBy the issuedBy to set
     */
    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the fccId
     */
    public String getFccId() {
        return fccId;
    }

    /**
     * @param fccId the fccId to set
     */
    public void setFccId(String fccId) {
        this.fccId = fccId;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }
    
    @NotBlank(message = "Terminal Id must be provided")
    @JsonProperty(value = "terminalid")
    private String terminalId;
    @NotBlank(message = "Batch no must be provided")
    @JsonProperty(value = "batchno")
    private String batchNo;
    @JsonProperty(value = "serialno")
    @NotBlank(message = "Serial No must be provided")
    private String serialNo;
    @JsonProperty(value = "dateissued")
    @NotBlank(message = "Date issued must be provided")
    private String dateIssued;
    @JsonProperty(value = "receivedby")
    @NotBlank(message = "Received by must be provided")
    private String receivedBy;
    @JsonProperty(value = "issuedby")
    @NotBlank(message = "Issued by must be provided")
    private String issuedBy;
    @JsonProperty(value = "pwcmerchantid")
    @NotBlank(message = "Merchant id must be provided")
    private String merchantId;
    @NotBlank(message = "Merchant code must be provided")
    @JsonProperty(value = "merchantcode")
    private String merchantCode;
    @JsonProperty(value = "fccid")
    private String fccId;
    private String provider;
    @NotBlank(message = "Hash must be provided")
    private String hash;
    @NotBlank(message = "Model must be provided")
    private String model; 

    
    public String toHashable(){
        
        StringBuilder builder = new StringBuilder();
        builder.append(Utility.nullToEmpty(merchantId))
                .append(Utility.nullToEmpty(getMerchantCode()))
                .append(Utility.nullToEmpty(getTerminalId()))
                .append(Utility.nullToEmpty(getModel()))
                .append(Utility.nullToEmpty(serialNo))
                .append(Utility.nullToEmpty(batchNo))
                .append(Utility.nullToEmpty(dateIssued))
                .append(Utility.nullToEmpty(receivedBy))
                .append(Utility.nullToEmpty(issuedBy))
                .append(Utility.nullToEmpty(fccId))
                .append(Utility.nullToEmpty(provider));
                
        return builder.toString();
    }
    
    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("terminalid", getTerminalId())
                .put("batchno", Utility.nullToEmpty(getBatchNo()))
                .put("serialno", Utility.nullToEmpty(getSerialNo()))
                .put("dateissued", Utility.nullToEmpty(getDateIssued()))
                .put("receivedBy", Utility.nullToEmpty(getReceivedBy()))
                .put("issuedby", Utility.nullToEmpty(getIssuedBy()))
                .put("merchantid", Utility.nullToEmpty(getMerchantId()))
                .put("merchantcode", Utility.nullToEmpty(getMerchantCode()))
                .put("fccid", Utility.nullToEmpty(getFccId()))
                .put("provider", Utility.nullToEmpty(getProvider()))
                .put("model", Utility.nullToEmpty(getModel()))
                .put("hash", Utility.nullToEmpty(getHash()));        
        
        return jSONObject.toString();
    }
    
}

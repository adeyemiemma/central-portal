/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

/**
 *
 * @author emmanueladeyemi
 */
public class DashboardSummaryReport {

    /**
     * @return the totalCore
     */
    public double getTotalCore() {
        return totalCore;
    }

    /**
     * @param totalCore the totalCore to set
     */
    public void setTotalCore(double totalCore) {
        this.totalCore = totalCore;
    }

    /**
     * @return the coreSuccess
     */
    public double getCoreSuccess() {
        return coreSuccess;
    }

    /**
     * @param coreSuccess the coreSuccess to set
     */
    public void setCoreSuccess(double coreSuccess) {
        this.coreSuccess = coreSuccess;
    }

    /**
     * @return the coreFailed
     */
    public double getCoreFailed() {
        return coreFailed;
    }

    /**
     * @param coreFailed the coreFailed to set
     */
    public void setCoreFailed(double coreFailed) {
        this.coreFailed = coreFailed;
    }

    /**
     * @return the totalMoneywave
     */
    public double getTotalMoneywave() {
        return totalMoneywave;
    }

    /**
     * @param totalMoneywave the totalMoneywave to set
     */
    public void setTotalMoneywave(double totalMoneywave) {
        this.totalMoneywave = totalMoneywave;
    }

    /**
     * @return the moneywaveSuccess
     */
    public double getMoneywaveSuccess() {
        return moneywaveSuccess;
    }

    /**
     * @param moneywaveSuccess the moneywaveSuccess to set
     */
    public void setMoneywaveSuccess(double moneywaveSuccess) {
        this.moneywaveSuccess = moneywaveSuccess;
    }

    /**
     * @return the moneywaveFailed
     */
    public double getMoneywaveFailed() {
        return moneywaveFailed;
    }

    /**
     * @param moneywaveFailed the moneywaveFailed to set
     */
    public void setMoneywaveFailed(double moneywaveFailed) {
        this.moneywaveFailed = moneywaveFailed;
    }

    /**
     * @return the totalRave
     */
    public double getTotalRave() {
        return totalRave;
    }

    /**
     * @param totalRave the totalRave to set
     */
    public void setTotalRave(double totalRave) {
        this.totalRave = totalRave;
    }

    /**
     * @return the raveSuccess
     */
    public double getRaveSuccess() {
        return raveSuccess;
    }

    /**
     * @param raveSuccess the raveSuccess to set
     */
    public void setRaveSuccess(double raveSuccess) {
        this.raveSuccess = raveSuccess;
    }

    /**
     * @return the raveFailed
     */
    public double getRaveFailed() {
        return raveFailed;
    }

    /**
     * @param raveFailed the raveFailed to set
     */
    public void setRaveFailed(double raveFailed) {
        this.raveFailed = raveFailed;
    }
    
    private double totalCore;
    private double coreSuccess;
    private double coreFailed;
    private double totalMoneywave;
    private double moneywaveSuccess;
    private double moneywaveFailed;
    private double totalRave;
    private double raveSuccess;
    private double raveFailed;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api;

import com.flutterwave.flutter.clientms.controller.api.model.GetCoreTransactionRequest;
import com.flutterwave.flutter.clientms.controller.api.model.GetProductTransactionRequest;
import com.flutterwave.flutter.clientms.controller.api.model.GetProductTransactionResponse;
import com.flutterwave.flutter.clientms.controller.api.model.PaymentRefundRequest;
import com.flutterwave.flutter.clientms.controller.api.model.QueryNIPTransactionResponse;
import com.flutterwave.flutter.clientms.dao.ApiUserDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.NipTransactionDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionWHDao;
import com.flutterwave.flutter.clientms.dao.RefundTransactionDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.model.ApiUser;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.MoneywaveTransaction;
import com.flutterwave.flutter.clientms.model.NipTransaction;
import com.flutterwave.flutter.clientms.model.RaveTransactionWH;
import com.flutterwave.flutter.clientms.model.RefundTransaction;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.TransactionViewModel;
import com.flutterwave.flutter.clientms.service.AWSDeadSuccessTransactionQueueListenerManager;
import com.flutterwave.flutter.clientms.service.AWSSuccessTransactionQueueListenerManager;
import com.flutterwave.flutter.clientms.service.LogService;
import com.flutterwave.flutter.clientms.service.SettlementService;
import com.flutterwave.flutter.clientms.service.UtilityService;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author emmanueladeyemi
 */
@Produces(value = MediaType.APPLICATION_JSON)
@Path(value = "/v1")
@RequestScoped
public class ExternalApiController {

    @EJB
    private TransactionDao transactionDao;
    @EJB
    private ApiUserDao apiUserDao;
    @EJB
    private RaveTransactionWHDao raveTransactionWHDao;
    @EJB
    private MoneywaveTransactionDao moneywaveTransactionDao;
    @EJB
    private UtilityService utilityService;
    @EJB
    private SettlementService settlementService;
    @EJB
    private AWSSuccessTransactionQueueListenerManager aWSSuccessTransactionQueueListenerManager;
    @EJB
    private AWSDeadSuccessTransactionQueueListenerManager wSDeadSuccessTransactionQueueListenerManager;
    @EJB
    private NipTransactionDao nipTransactionDao;
    @EJB
    private LogService logService;
    @EJB
    private RefundTransactionDao refundTransactionDao;

    @Path(value = "/transaction/core")
    @POST
    public Response getTransaction(@Valid GetCoreTransactionRequest request,
            @HeaderParam(value = "authorization") String authorization) {

        String[] data = authorization.split(" ");

        String key = null;

        data = Arrays.asList(data).stream().filter(x -> !x.isEmpty()).collect(Collectors.toList()).toArray(new String[]{});

        if (data.length > 1) {
            key = data[1];
        } else {
            key = data[0];
        }

        ApiUser apiUser = null;

        try {
            apiUser = apiUserDao.findByKey("privateKey", key);
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("data", null);

        if (apiUser == null) {
            response.put("status", "failed");
            response.put("description", "Invalid api key");

            return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
        }

        String date = request.getTransactionDate();

        try {
            Date transactionDate = Utility.parseDate(date, "yyyy-MM-dd");

            if (transactionDate == null) {
                response.put("status", "failed");
                response.put("description", "invalid date format");

                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

        } catch (Exception ex) {
            ex.printStackTrace();

            response.put("status", "failed");
            response.put("description", "invalid date format");

            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }

        TransactionViewModel transaction = null;
        try {
            transaction = transactionDao.findTransaction(request.getReference(), request.getTransactionDate());
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Map<String, String> body = new HashMap<>();
        body.put("transaction_date", date);
        body.put("reference", request.getReference());

        if (transaction == null) {

            response.put("status", "failed");
            response.put("description", "transaction not found");
            response.put("data", body);

            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }

        body.put("logged_on", Utility.formatDate(transaction.getCreatedOn(), "yyyy-MM-dd HH:mm:ss"));

        response.put("status", "success");
        response.put("description", "successful");
        response.put("data", body);

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "/transaction/core/query")
    @POST
    public Response getTransactionByQuery(@Valid GetCoreTransactionRequest request,
            @HeaderParam(value = "authorization") String authorization, @QueryParam(value = "all") @DefaultValue(value = "false") boolean all) {

        String[] data = authorization.split(" ");

        String key = null;

        data = Arrays.asList(data).stream().filter(x -> !x.isEmpty()).collect(Collectors.toList()).toArray(new String[]{});

        if (data.length > 1) {
            key = data[1];
        } else {
            key = data[0];
        }

        ApiUser apiUser = null;

        try {
            apiUser = apiUserDao.findByKey("privateKey", key);
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("data", null);

        if (apiUser == null) {
            response.put("status", "failed");
            response.put("description", "Invalid api key");

            return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
        }

        String date = request.getTransactionDate();

        try {
            Date transactionDate = Utility.parseDate(date, "yyyy-MM-dd");

            if (transactionDate == null) {
                response.put("status", "failed");
                response.put("description", "invalid date format");

                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }

        } catch (Exception ex) {
            ex.printStackTrace();

            response.put("status", "failed");
            response.put("description", "invalid date format (yyyy-MM-dd)");

            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }

        TransactionViewModel transaction = null;
        try {
            transaction = transactionDao.findTransactionLike(request.getReference(), request.getTransactionDate());
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Map<String, String> body = new HashMap<>();
        body.put("transaction_date", date);
        body.put("reference", request.getReference());

        if (transaction == null) {

            response.put("status", "failed");
            response.put("description", "transaction not found");
            response.put("data", body);

            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }

        body.put("response_code", transaction.getResponseCode());
        body.put("response_message", transaction.getResponseMessage());
        body.put("amount", transaction.getAmount() + "");

        if (transaction.getTransactionType() != null && transaction.getTransactionType().toUpperCase().contains("ACCOUNT")) {
            body.put("bank_code", transaction.getCardScheme());
        } 

        body.put("pan_account", transaction.getCardMask());
        body.put("logged_on", Utility.formatDate(transaction.getCreatedOn(), "yyyy-MM-dd HH:mm:ss"));

        response.put("status", "success");
        response.put("description", "successful");
        
        if(all == false )
            response.put("data", body);
        else 
            response.put("data", transaction);
        
        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Path(value = "/transactions/products/query")
    @POST
    public Response getProductTransaction(@Valid GetProductTransactionRequest request, @HeaderParam(value = "authorization") String authorization) {

        String[] data = authorization.split(" ");

        String key = null;

        data = Arrays.asList(data).stream().filter(x -> !x.isEmpty()).collect(Collectors.toList()).toArray(new String[]{});

        if (data.length > 1) {
            key = data[1];
        } else {
            key = data[0];
        }

        ApiUser apiUser = null;

        try {
            apiUser = apiUserDao.findByKey("privateKey", key);
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        GetProductTransactionResponse response = new GetProductTransactionResponse();
        response.setProduct(request.getProduct());
        response.setReference(request.getReference());
//        Map<String, Object> response = new HashMap<>();
//        response.put("data", null);

        if (apiUser == null) {
//            response.put("status", "failed");
//            response.put("description", "Invalid api key");
//            
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        switch (request.getProduct().toLowerCase()) {

            case "rave": {
                RaveTransactionWH raveTransactionWH = null;
                try {
                    raveTransactionWH = raveTransactionWHDao.findByKey("transactionReference", request.getReference());
                } catch (DatabaseException ex) {
                    Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (raveTransactionWH == null) {
                    try {
                        raveTransactionWH = raveTransactionWHDao.findByKey("flutterReference", request.getReference());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                if (raveTransactionWH == null) {

                    return buildFailedResponse("transaction not found", response);
                }

                response.setStatus(raveTransactionWH.getStatus());
                response.setCustomerReference(raveTransactionWH.getTransactionReference());
                response.setResponseCode(raveTransactionWH.getChargeCode());
                response.setResponseMessage(raveTransactionWH.getChargeMessage());
                response.setAmount(raveTransactionWH.getChargedAmount());
                response.setCurrency(raveTransactionWH.getCurrency());
                response.setMerchantName(raveTransactionWH.getMerchant());
                response.setMerchantId(raveTransactionWH.getAccountid());

                return buildSuccessResponse(response);

            }
            case "moneywave": {

                MoneywaveTransaction transaction = null;
                try {
                    transaction = moneywaveTransactionDao.findByKey("flutterReference", request.getReference());
                } catch (DatabaseException ex) {
                    Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (transaction == null) {
                    try {
                        transaction = moneywaveTransactionDao.findByKey("linkingReference", request.getReference());
                    } catch (DatabaseException ex) {
                        Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                if (transaction == null) {

                    return buildFailedResponse("transaction not found", response);
                }

                response.setStatus(transaction.getStatus());
                response.setCustomerReference(transaction.getFlutterResponseCode());
                response.setResponseCode(transaction.getFlutterResponseCode());
                response.setResponseMessage(transaction.getFlutterResponseMessage());
                response.setAmount(transaction.getAmount());
                response.setCurrency(transaction.getCurrency() == null ? null : transaction.getCurrency().getShortCode());
                response.setMerchantId(transaction.getMerchant().getId() + "");
                response.setMerchantName(transaction.getMerchant().getName());
                response.setTransactionType(transaction.getSystemType());
//                response.setStatus(transaction.getStatus());

                return buildSuccessResponse(response);

            }
            case "core": {

                Transaction transaction = null;
                try {
                    transaction = transactionDao.findByKey("flwTxnReference", request.getReference());
                } catch (DatabaseException ex) {
                    Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (transaction == null) {

                    return buildFailedResponse("transaction not found", response);
                }

//                response.setStatus(transaction.get());
                response.setCustomerReference(transaction.getMerchantTxnReference());
                response.setResponseCode(transaction.getResponseCode());
                response.setResponseMessage(transaction.getResponseMessage());
                response.setAmount(transaction.getAmount());
                response.setCurrency(transaction.getTransactionCurrency());
                response.setMerchantId(transaction.getMerchantId());
                response.setTransactionType(transaction.getTransactionType());
                response.setNarration(transaction.getTransactionNarration());
                
                return buildSuccessResponse(response);

            }

        }
        
        return buildFailedResponse("Unknown product", response);
        
    }

    @Path(value = "/transactions/core/report")
    @GET
    public Response dropCoreReport(@QueryParam(value = "date") String dateString ){
        
        Date date = null;
        
        if(Utility.emptyToNull(dateString) == null){
            dateString = null;
        }else{
            date = Utility.parseDate(dateString, "yyyy-MM-dd");
        }
        
        if(date == null){
            
            Calendar calendar = Calendar.getInstance();

            calendar.add(Calendar.DATE, -1);

            date = calendar.getTime();
        }
        
        utilityService.buildAndDropCoreReportToSlack(date);
        
        Map<String, String> response = new HashMap<>();
        
        response.put("status", "successful");
        response.put("description", "successful");
        
        return Response.status(Response.Status.OK).entity(response).build();
    } 
    
    @Path(value = "/transactions/settlement/report")
    @GET
    public Response sendSettlementReport(@QueryParam(value = "date") String dateString ){
        
        Date date = null;
        
        if(Utility.emptyToNull(dateString) != null){
            date = Utility.parseDate(dateString, "yyyy-MM-dd");
        }
        
        if(date == null){
            
            Calendar calendar = Calendar.getInstance();

            calendar.add(Calendar.DATE, -1);

            date = calendar.getTime();
        }
        
        settlementService.processMerchTransactions(date);
        
        Map<String, String> response = new HashMap<>();
        
        response.put("status", "successful");
        response.put("description", "successful");
        
        return Response.status(Response.Status.OK).entity(response).build();
    } 
    
    @Path(value = "/transactions/queues")
    @POST
    public Response startNewSuccessQueue(@QueryParam(value = "dead") @DefaultValue(value = "false") boolean deadQueue){
        
        if(deadQueue == false)
            aWSSuccessTransactionQueueListenerManager.startNewService();
        else
            wSDeadSuccessTransactionQueueListenerManager.startNewService();
        
        Map<String, String> response = new HashMap<>();
        
        response.put("status", "successful");
        response.put("description", "successful");
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @Path(value = "/transactions/queues")
    @DELETE
    public Response deleteSuccessQueue(@QueryParam(value = "dead") @DefaultValue(value = "false") boolean deadQueue){
        
        if(deadQueue == false)
            aWSSuccessTransactionQueueListenerManager.stopAll();
        else
            wSDeadSuccessTransactionQueueListenerManager.stopAll();
        
        Map<String, String> response = new HashMap<>();
        
        response.put("status", "successful");
        response.put("description", "successful");
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @Path(value = "/transactions/queues/dead")
    @POST
    public Response startDeadSuccessQueue(){
        
        wSDeadSuccessTransactionQueueListenerManager.startNewService();
        
        Map<String, String> response = new HashMap<>();
        
        response.put("status", "successful");
        response.put("description", "successful");
        
        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @Path(value = "/transactions/nip/{reference}")
    @GET
    public Response getNIPTransaction(@PathParam(value = "reference") String reference){
        
        
        NipTransaction nipTransaction = null;
        try {
            nipTransaction = nipTransactionDao.findByKey("paymentReference", reference);
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Map<String, Object> response = new HashMap<>();
        
        if(nipTransaction == null){
        
            response.put("status", "failed");
            response.put("description", "Transaction not found");
            
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }
                
        QueryNIPTransactionResponse transactionResponse = new QueryNIPTransactionResponse();
        transactionResponse.setAccountName(nipTransaction.getAccountName());
        transactionResponse.setAccountNumber(nipTransaction.getAccountNo());
        transactionResponse.setAmount(nipTransaction.getAmount().doubleValue());
        transactionResponse.setCurrency(nipTransaction.getCurrency());
        transactionResponse.setPaymentReference(nipTransaction.getPaymentReference());
        transactionResponse.setSesssionId(nipTransaction.getSessionId());
        transactionResponse.setStatus(nipTransaction.getStatus());
        transactionResponse.setTransactionDate(Utility.formatDate(nipTransaction.getTransactionTime(),"yyyy-MM-dd HH:mm:ss" ));
        transactionResponse.setType(nipTransaction.getTransactionType());
        
        response.put("status", "successful");
        response.put("description", "Transaction found");
        response.put("data", transactionResponse);
            
        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @Path(value = "/transactions/refunds")
    @POST
    public Response logRefund( @Valid PaymentRefundRequest request, @HeaderParam(value = "uniqueid") String uniqueId ){
        
        Log log = new Log();
        log.setAction("Logging Refund");
        log.setCreatedOn(new Date());
        log.setDescription(request.toString());
        log.setLevel(LogLevel.Info);
        log.setLogDomain(LogDomain.API);
        log.setLogState(LogState.STARTED);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setUsername(uniqueId);
        
        logService.log(log);
        
        Map<String, String> response = new HashMap<>();
        
        ApiUser apiUser = null;
        try {
            apiUser = apiUserDao.findByKey("uniqueId", uniqueId);
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (apiUser == null) {

            response.put("responsecode", "04");
            response.put("responsemessage", "authentication failed");

            return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
        }
            
        String requestString = apiUser.getPrivateKey() + "" + request.getPaymentReference()
                +""+request.getMerchant()+""+request.getTotalAmount();

        String hashString = Utility.sha512(requestString);

        if (!hashString.equalsIgnoreCase(request.getHash())) {

            response.put("responsecode", "05");
            response.put("responsemessage", "Invalid hash value");

            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(response).build();
        }
        
        RefundTransaction refundTransaction = null;
        try {
            refundTransaction = refundTransactionDao.findByKey("paymentReference", request.getPaymentReference());
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        if(refundTransaction != null){
            
            response.put("status", "failed");
            response.put("message", "Refund already logged");
            
            return Response.status(Response.Status.CONFLICT).entity(response).build();
        }
        
        refundTransaction = new RefundTransaction();
        
        refundTransaction.setCreationTime(new Date());
        refundTransaction.setMerchant(request.getMerchant());
        
        if(Utility.emptyToNull(request.getRefundAmount()) != null)
            refundTransaction.setRefundAmount(new BigDecimal(request.getRefundAmount()));
        
        refundTransaction.setTotalAmount(new BigDecimal(request.getTotalAmount()));
        refundTransaction.setTransactionDate(request.getTransactionDate());
        refundTransaction.setPaymentReference(request.getPaymentReference());
        refundTransaction.setCurrency(request.getCurrency());
        
        try {
            refundTransactionDao.create(refundTransaction);
        } catch (DatabaseException ex) {
            Logger.getLogger(ExternalApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.put("status", "success");
        response.put("message", "Refund logged successfully");

        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    public Response buildSuccessResponse(GetProductTransactionResponse response) {

        Map<String, Object> data = new HashMap<>();
        data.put("status", "success");
        data.put("description", "Transaction found");
        data.put("data", response);

        return Response.status(Response.Status.OK).entity(data).build();
    }

    public Response buildFailedResponse(String reason, GetProductTransactionResponse response) {

        Map<String, Object> data = new HashMap<>();
        data.put("status", "failed");
        data.put("description", "" + reason);
        data.put("data", "" + reason);
//        data.put("data", response);

        return Response.status(Response.Status.BAD_REQUEST).entity(data).build();
    }
}

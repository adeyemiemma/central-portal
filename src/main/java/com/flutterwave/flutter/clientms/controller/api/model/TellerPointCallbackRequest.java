/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.util.Utility;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TellerPointCallbackRequest {

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the transactionCharge
     */
    public int getTransactionCharge() {
        return transactionCharge;
    }

    /**
     * @param transactionCharge the transactionCharge to set
     */
    public void setTransactionCharge(int transactionCharge) {
        this.transactionCharge = transactionCharge;
    }

    /**
     * @return the approvalCode
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * @param approvalCode the approvalCode to set
     */
    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    /**
     * @return the acquiringBank
     */
    public String getAcquiringBank() {
        return acquiringBank;
    }

    /**
     * @param acquiringBank the acquiringBank to set
     */
    public void setAcquiringBank(String acquiringBank) {
        this.acquiringBank = acquiringBank;
    }

    /**
     * @return the cardHolderIssuingBank
     */
    public String getCardHolderIssuingBank() {
        return cardHolderIssuingBank;
    }

    /**
     * @param cardHolderIssuingBank the cardHolderIssuingBank to set
     */
    public void setCardHolderIssuingBank(String cardHolderIssuingBank) {
        this.cardHolderIssuingBank = cardHolderIssuingBank;
    }

    /**
     * @return the maskedCardPan
     */
    public String getMaskedCardPan() {
        return maskedCardPan;
    }

    /**
     * @param maskedCardPan the maskedCardPan to set
     */
    public void setMaskedCardPan(String maskedCardPan) {
        this.maskedCardPan = maskedCardPan;
    }

    /**
     * @return the cardholderName
     */
    public String getCardholderName() {
        return cardholderName;
    }

    /**
     * @param cardholderName the cardholderName to set
     */
    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    /**
     * @return the traceRef
     */
    public String getTraceRef() {
        return traceRef;
    }

    /**
     * @param traceRef the traceRef to set
     */
    public void setTraceRef(String traceRef) {
        this.traceRef = traceRef;
    }

    /**
     * @return the providerRef
     */
    public String getProviderRef() {
        return providerRef;
    }

    /**
     * @param providerRef the providerRef to set
     */
    public void setProviderRef(String providerRef) {
        this.providerRef = providerRef;
    }

    /**
     * @return the tellerpointRef
     */
    public String getTellerpointRef() {
        return tellerpointRef;
    }

    /**
     * @param tellerpointRef the tellerpointRef to set
     */
    public void setTellerpointRef(String tellerpointRef) {
        this.tellerpointRef = tellerpointRef;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the creationTime
     */
    public String getCreationTime() {
        return creationTime;
    }

    /**
     * @param creationTime the creationTime to set
     */
    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }
    
    @NotBlank(message = "Terminal Id must be provided")
    @JsonProperty(value = "terminalId")
    private String terminalId;
    @NotBlank(message = "Merchant Id must be provided")
    @JsonProperty(value = "merchantId")
    private String merchantId;
    @JsonProperty(value = "merchantName")
    private String merchantName;
    private double amount;
    @JsonProperty(value = "transactionCharge")
    private int transactionCharge;
    @JsonProperty(value = "approvalCode")
    private String approvalCode;
    @JsonProperty(value = "acquiringBank")
    private String acquiringBank;
    @JsonProperty(value = "cardHolderIssuingBank")
    private String cardHolderIssuingBank;
    @JsonProperty(value = "maskedCardPan")
    private String maskedCardPan;
    @JsonProperty(value = "cardholderName")
    private String cardholderName;
    @JsonProperty(value = "traceRef")
    private String traceRef;
    @JsonProperty(value = "providerRef")
    private String providerRef;
    @NotBlank(message = "Teller Point Ref must be provided")
    @JsonProperty(value = "tellerpointRef")
    private String tellerpointRef;
    private String status;
    @JsonProperty(value = "creationTime")
    private String creationTime;
    @NotBlank(message = "Hash must be provided")
    private String hash;
    
    
    @Override
    public String toString(){
        
        JSONObject jSONObject = new JSONObject()
                .put("terminalid", Utility.nullToEmpty(getTerminalId()))
                .put("merchantid", Utility.nullToEmpty(getMerchantId()))
                .put("merchantname", Utility.nullToEmpty(getMerchantName()))
                .put("amount", amount)
                .put("transactionCharge", getTransactionCharge())
                .put("approvalCode", Utility.nullToEmpty(getApprovalCode()))
                .put("acquiringbank", Utility.nullToEmpty(getAcquiringBank()))
                .put("cardHolderIssuingBank", Utility.nullToEmpty(getCardHolderIssuingBank()))
                .put("maskedCardPan", Utility.nullToEmpty(getMaskedCardPan()))
                .put("cardholderName", Utility.nullToEmpty(getCardholderName()))
                .put("traceRef", Utility.nullToEmpty(traceRef))
                .put("providerRef", Utility.nullToEmpty(providerRef))
                .put("tellerpointRef", Utility.nullToEmpty(tellerpointRef))
                .put("creationTime", Utility.nullToEmpty(creationTime))
                .put("status", Utility.nullToEmpty(status))
                .put("hash", Utility.nullToEmpty(getHash()));
        
        return jSONObject.toString();
    }
    
    
    public String toHashable(){
        
        StringBuilder builder = new StringBuilder();
        builder.append(merchantId)
                .append(terminalId)
                .append(tellerpointRef);
        
        // hash = privatekey+merchantId+terminalId+tellerpointRef
        
        return builder.toString();
    }
}

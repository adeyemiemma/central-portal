/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.controller.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class ReversalRequest {

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the maskedPan
     */
    public String getMaskedPan() {
        return maskedPan;
    }

    /**
     * @param maskedPan the maskedPan to set
     */
    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }
    
    @NotBlank(message = "RRN must be provided")
    @JsonProperty(value = "rrn")
    private String rrn;
    @NotBlank(message = "Hash must be provided")
    private String hash;
    @JsonProperty(value = "maskedpan")
    @NotBlank(message = "Masked Pan must be provided")
    private String maskedPan;
    @NotBlank(message = "Amount must be provided")
    private String amount;
    @JsonProperty(value = "terminalid")
    @NotBlank(message = "Terminal Id must be provided")
    private String terminalId;
    private String date;
    
    @Override
    public String toString() {
        
        JSONObject jSONObject = new JSONObject()
                .put("rrn", getRrn())
                .put("maskedpan", getMaskedPan())
                .put("terminalid", getTerminalId())
                .put("amount", getAmount())
                .put("data", getDate())
                .put("hash", getHash());
        
        return jSONObject.toString();
    }
    
    
    public String toHashable(){
        
        StringBuilder builder = new StringBuilder();
                builder.append(getRrn())
                .append(getTerminalId())
                .append(getAmount());
        
        return builder.toString();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.util.Utility;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author emmanueladeyemi
 */
@Entity
public class PosTransactionViewModelWeb implements Serializable {

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the meta
     */
    public String getMeta() {
        return meta;
    }

    /**
     * @param meta the meta to set
     */
    public void setMeta(String meta) {
        this.meta = meta;
    }

    /**
     * @return the providerRef
     */
    public String getProviderRef() {
        return providerRef;
    }

    /**
     * @param providerRef the providerRef to set
     */
    public void setProviderRef(String providerRef) {
        this.providerRef = providerRef;
    }

    /**
     * @return the reversed
     */
    public boolean isReversed() {
        return reversed;
    }

    /**
     * @param reversed the reversed to set
     */
    public void setReversed(boolean reversed) {
        this.reversed = reversed;
    }

    /**
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * @param fileId the fileId to set
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * @return the callback
     */
    public boolean isCallback() {
        return callback;
    }

    /**
     * @param callback the callback to set
     */
    public void setCallback(boolean callback) {
        this.callback = callback;
    }

    /**
     * @return the callbackOn
     */
    public String getCallbackOn() {
        return callbackOn;
    }

    /**
     * @param callbackOn the callbackOn to set
     */
    public void setCallbackOn(String callbackOn) {
        this.callbackOn = callbackOn;
    }
//
//    /**
//     * @return the merchantParent
//     */
//    public String getMerchantParent() {
//        return merchantParent;
//    }
//
//    /**
//     * @param merchantParent the merchantParent to set
//     */
//    public void setMerchantParent(String merchantParent) {
//        this.merchantParent = merchantParent;
//    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the posId
     */
    public String getPosId() {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(String posId) {
        this.posId = posId;
    }

    /**
     * @return the createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the refCode
     */
    public String getRefCode() {
        return refCode;
    }

    /**
     * @param refCode the refCode to set
     */
    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
    
    @Id
    private long id;
    private String rrn;
    @JsonProperty(value = "terminalid")
    private String terminalId;
    @JsonProperty(value = "mask")
    private String pan;
    private double amount;
    private String currency;
    @JsonProperty(value = "refcode")
    private String refCode;
    private String type;
    @JsonProperty(value = "responsecode")
    private String responseCode;
    @JsonProperty(value = "responsemessage")
    private String responseMessage;
    private String status;
    private String datetime;
    @JsonProperty(value = "merchantcode")
    private String merchantCode;
    @JsonProperty(value = "merchantid")
    private String merchantId;
    private String provider;
    @JsonProperty(value = "merchantname")
    private String merchantName;
    private String posId;
    private String createdOn;
    private String bankName;
    private String fileId;
    private boolean callback;
    private String callbackOn;
    private boolean reversed;
    private String providerRef;
    private String meta;
    private String source;
//    private String merchantParent;
    
    
    public static PosTransactionViewModelWeb from(PosTransaction posTransaction){
        
        PosTransactionViewModelWeb transactionViewModel = new PosTransactionViewModelWeb();
        transactionViewModel.setAmount(posTransaction.getAmount());
        transactionViewModel.setCurrency(posTransaction.getCurrency());
        transactionViewModel.setDatetime(Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
        transactionViewModel.setPan(posTransaction.getPan());
        transactionViewModel.setRefCode(posTransaction.getRefCode());
        transactionViewModel.setResponseCode(posTransaction.getResponseCode());
        transactionViewModel.setResponseMessage(posTransaction.getResponseMessage());
        transactionViewModel.setRrn(posTransaction.getRrn());
        transactionViewModel.setStatus(posTransaction.getStatus());
        transactionViewModel.setTerminalId(posTransaction.getTerminalId());
        transactionViewModel.setType(posTransaction.getType());
        transactionViewModel.setId(posTransaction.getId());
//        transactionViewModel.setMerchantParent(posTransaction.get());
        transactionViewModel.setPosId(posTransaction.getPosId());
//        transactionViewModel.setBankName(posTransaction.g());
        transactionViewModel.setCreatedOn(Utility.formatDate(posTransaction.getCreatedOn(), "yyyy-MM-dd HH:mm:ss"));
        transactionViewModel.setReversed(posTransaction.isReversed());
        transactionViewModel.setSource(posTransaction.getSource());
        
        
        return transactionViewModel;
    }
    
}

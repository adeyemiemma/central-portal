/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import com.flutterwave.flutter.clientms.model.GlobalRewardSetting;
import com.flutterwave.flutter.clientms.model.RewardSetting;
import com.flutterwave.flutter.clientms.util.EnumUtil;
import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public class RewardSettingViewModel {

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the type
     */
    public EnumUtil.TransactionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EnumUtil.TransactionType type) {
        this.type = type;
    }

    /**
     * @return the weekDay
     */
    public EnumUtil.WeekDay getWeekDay() {
        return weekDay;
    }

    /**
     * @param weekDay the weekDay to set
     */
    public void setWeekDay(EnumUtil.WeekDay weekDay) {
        this.weekDay = weekDay;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the status
     */
    public EnumUtil.Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(EnumUtil.Status status) {
        this.status = status;
    }

    /**
     * @return the point
     */
    public double getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(double point) {
        this.point = point;
    }

    /**
     * @return the unit
     */
    public int getUnit() {
        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(int unit) {
        this.unit = unit;
    }

    /**
     * @return the modifiedOn
     */
    public Date getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn the modifiedOn to set
     */
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    private String merchantId;
    private Date createdOn;
    private EnumUtil.TransactionType type; 
    private EnumUtil.WeekDay weekDay;
    private String currency;
    private EnumUtil.Status status;
    private double point;
    private int unit = 1;
    private Date modifiedOn;
    private String createdBy;
    private String modifiedBy;
    
    public static RewardSettingViewModel from(RewardSetting setting){
        RewardSettingViewModel model = new RewardSettingViewModel();
        model.setCreatedBy(setting.getCreatedBy() == null ?  null : (setting.getCreatedBy().getFirstName() + " "+setting.getCreatedBy().getLastName()));
        model.setCreatedOn(setting.getCreatedOn());
        model.setMerchantId(setting.getMerchantId());
        model.setCurrency(setting.getCurrency());
        model.setPoint(setting.getPoint());
        model.setModifiedOn(setting.getModifiedOn());
        model.setStatus(setting.getStatus());
        model.setType(setting.getType());
        model.setUnit(setting.getUnit());
        model.setWeekDay(setting.getWeekDay());
        
        return model;
    }
    
     public static RewardSettingViewModel from(GlobalRewardSetting setting){
        RewardSettingViewModel model = new RewardSettingViewModel();
        model.setCreatedBy(setting.getCreatedBy() == null ?  null : (setting.getCreatedBy().getFirstName() + " "+setting.getCreatedBy().getLastName()));
        model.setCreatedOn(setting.getCreatedOn());
        model.setCurrency(setting.getCurrency());
        model.setPoint(setting.getPoint());
        model.setModifiedOn(setting.getModifiedOn());
        model.setStatus(setting.getStatus());
        model.setType(setting.getType());
        model.setUnit(setting.getUnit());
        model.setWeekDay(setting.getWeekDay());
        
        return model;
    }
}

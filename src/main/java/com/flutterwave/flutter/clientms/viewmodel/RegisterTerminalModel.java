/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import com.flutterwave.flutter.clientms.model.PosTerminal;
import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public class RegisterTerminalModel {

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the merchant
     */
    public String getMerchant() {
        return merchant;
    }

    /**
     * @param merchant the merchant to set
     */
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the fccid
     */
    public String getFccid() {
        return fccid;
    }

    /**
     * @param fccid the fccid to set
     */
    public void setFccid(String fccid) {
        this.fccid = fccid;
    }

    /**
     * @return the serialNo
     */
    public String getSerialNo() {
        return serialNo;
    }

    /**
     * @param serialNo the serialNo to set
     */
    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    /**
     * @return the batchNo
     */
    public String getBatchNo() {
        return batchNo;
    }

    /**
     * @param batchNo the batchNo to set
     */
    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    /**
     * @return the dateIssued
     */
    public String getDateIssued() {
        return dateIssued;
    }

    /**
     * @param dateIssued the dateIssued to set
     */
    public void setDateIssued(String dateIssued) {
        this.dateIssued = dateIssued;
    }

    /**
     * @return the receivedBy
     */
    public String getReceivedBy() {
        return receivedBy;
    }

    /**
     * @param receivedBy the receivedBy to set
     */
    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }
    
    private String terminalId;
    private String merchant;
    private String model;
    private String fccid;
    private String serialNo;
    private String batchNo;
    private String dateIssued;
    private String receivedBy;
    private String provider;
    private long id;
    private Date createdOn;
    private String bankName;
    
    public static RegisterTerminalModel from(PosTerminal posTerminal){
        
        if(posTerminal == null)
            return null;
        
        RegisterTerminalModel model = new RegisterTerminalModel();
        model.setBatchNo(posTerminal.getBatchNo());
        model.setCreatedOn(posTerminal.getCreatedOn());
        model.setDateIssued(posTerminal.getDateIssued());
        model.setFccid(posTerminal.getFccId());
        model.setId(posTerminal.getId());
        
        if(posTerminal.getMerchant() != null)
            model.setMerchant(posTerminal.getMerchant().getName());
        
        model.setModel(posTerminal.getModel());
        model.setProvider(posTerminal.getProvider().getName());
        model.setReceivedBy(posTerminal.getReceivedBy());
        model.setSerialNo(posTerminal.getSerialNo());
        model.setTerminalId(posTerminal.getTerminalId());
        model.setBankName(posTerminal.getBankName());
        
        return model;
    }
}

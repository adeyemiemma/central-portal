/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

/**
 *
 * @author emmanueladeyemi
 */
public class ChargeBackFilter {

    /**
     * @return the currency
     */
    public String getCurrency() {
        if(currency == null)
            return "NGN";
        
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the range
     */
    public String getRange() {
        return range;
    }

    /**
     * @param range the range to set
     */
    public void setRange(String range) {
        this.range = range;
    }
    
    private String range;
    private String currency;
}

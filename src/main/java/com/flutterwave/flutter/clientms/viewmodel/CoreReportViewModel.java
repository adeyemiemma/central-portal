/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import java.math.BigInteger;

/**
 *
 * @author emmanueladeyemi
 */
public class CoreReportViewModel {

    /**
     * @return the accountSuccessValue
     */
    public Double getAccountSuccessValue() {
        return accountSuccessValue;
    }

    /**
     * @param accountSuccessValue the accountSuccessValue to set
     */
    public void setAccountSuccessValue(Double accountSuccessValue) {
        this.accountSuccessValue = accountSuccessValue;
    }

    /**
     * @return the accountSuccessVolume
     */
    public BigInteger getAccountSuccessVolume() {
        return accountSuccessVolume;
    }

    /**
     * @param accountSuccessVolume the accountSuccessVolume to set
     */
    public void setAccountSuccessVolume(BigInteger accountSuccessVolume) {
        this.accountSuccessVolume = accountSuccessVolume;
    }

    /**
     * @return the accountFailureValue
     */
    public Double getAccountFailureValue() {
        return accountFailureValue;
    }

    /**
     * @param accountFailureValue the accountFailureValue to set
     */
    public void setAccountFailureValue(Double accountFailureValue) {
        this.accountFailureValue = accountFailureValue;
    }

    /**
     * @return the accountFailureVolume
     */
    public BigInteger getAccountFailureVolume() {
        return accountFailureVolume;
    }

    /**
     * @param accountFailureVolume the accountFailureVolume to set
     */
    public void setAccountFailureVolume(BigInteger accountFailureVolume) {
        this.accountFailureVolume = accountFailureVolume;
    }

    /**
     * @return the cardSuccessValue
     */
    public Double getCardSuccessValue() {
        return cardSuccessValue;
    }

    /**
     * @param cardSuccessValue the cardSuccessValue to set
     */
    public void setCardSuccessValue(Double cardSuccessValue) {
        this.cardSuccessValue = cardSuccessValue;
    }

    /**
     * @return the cardSuccessVolume
     */
    public BigInteger getCardSuccessVolume() {
        return cardSuccessVolume;
    }

    /**
     * @param cardSuccessVolume the cardSuccessVolume to set
     */
    public void setCardSuccessVolume(BigInteger cardSuccessVolume) {
        this.cardSuccessVolume = cardSuccessVolume;
    }

    /**
     * @return the cardFailureValue
     */
    public Double getCardFailureValue() {
        return cardFailureValue;
    }

    /**
     * @param cardFailureValue the cardFailureValue to set
     */
    public void setCardFailureValue(Double cardFailureValue) {
        this.cardFailureValue = cardFailureValue;
    }

    /**
     * @return the cardFailureVolume
     */
    public BigInteger getCardFailureVolume() {
        return cardFailureVolume;
    }

    /**
     * @param cardFailureVolume the cardFailureVolume to set
     */
    public void setCardFailureVolume(BigInteger cardFailureVolume) {
        this.cardFailureVolume = cardFailureVolume;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
    private Double cardSuccessValue = 0.0;
    private BigInteger cardSuccessVolume = BigInteger.ZERO;
    
    private Double cardFailureValue = 0.0;
    private BigInteger cardFailureVolume = BigInteger.ZERO;;
    
    private Double accountSuccessValue = 0.0;
    private BigInteger accountSuccessVolume = BigInteger.ZERO;
    
    private Double accountFailureValue = 0.0;
    private BigInteger accountFailureVolume = BigInteger.ZERO;;
    
    private String currency;
}

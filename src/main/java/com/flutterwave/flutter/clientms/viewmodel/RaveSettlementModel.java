/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

/**
 *
 * @author emmanueladeyemi
 */
public class RaveSettlementModel {

    /**
     * @return the businessName
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     * @param businessName the businessName to set
     */
    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the appFee
     */
    public double getAppFee() {
        return appFee;
    }

    /**
     * @param appFee the appFee to set
     */
    public void setAppFee(double appFee) {
        this.appFee = appFee;
    }

    /**
     * @return the merchantFee
     */
    public double getMerchantFee() {
        return merchantFee;
    }

    /**
     * @param merchantFee the merchantFee to set
     */
    public void setMerchantFee(double merchantFee) {
        this.merchantFee = merchantFee;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the volume
     */
    public long getVolume() {
        return volume;
    }

    /**
     * @param volume the volume to set
     */
    public void setVolume(long volume) {
        this.volume = volume;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the parent
     */
    public String getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(String parent) {
        this.parent = parent;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the parentSettlementType
     */
    public String getParentSettlementType() {
        return parentSettlementType;
    }

    /**
     * @param parentSettlementType the parentSettlementType to set
     */
    public void setParentSettlementType(String parentSettlementType) {
        this.parentSettlementType = parentSettlementType;
    }
    
    private String businessName;
    private double amount;
    private double appFee;
    private double merchantFee;
    private String type;
    private long volume;
    private String currency;
    private String parent;
    private String accountNumber;
    private String bankCode;
    private String parentSettlementType;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author emmanueladeyemi
 */
@Entity
public class FetchRRNModel implements Serializable {

    /**
     * @return the vpcmerchant
     */
    public String getVpcmerchant() {
        return vpcmerchant;
    }

    /**
     * @param vpcmerchant the vpcmerchant to set
     */
    public void setVpcmerchant(String vpcmerchant) {
        this.vpcmerchant = vpcmerchant;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantTxnReference
     */
    public String getMerchantTxnReference() {
        return merchantTxnReference;
    }

    /**
     * @param merchantTxnReference the merchantTxnReference to set
     */
    public void setMerchantTxnReference(String merchantTxnReference) {
        this.merchantTxnReference = merchantTxnReference;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the flutterReference
     */
    public String getFlutterReference() {
        return flutterReference;
    }

    /**
     * @param flutterReference the flutterReference to set
     */
    public void setFlutterReference(String flutterReference) {
        this.flutterReference = flutterReference;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private double amount;
    private String flutterReference;
    private String rrn;
    private String transactionDate;
    private String currency;
    private String merchantTxnReference;
    private String merchantId;
    private String merchantName;
    private String responseCode;
    private String responseMessage;
    private String vpcmerchant;
            
}

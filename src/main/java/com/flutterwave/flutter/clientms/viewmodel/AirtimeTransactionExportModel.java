/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

/**
 *
 * @author emmanueladeyemi
 */
public class AirtimeTransactionExportModel {

    /**
     * @return the reversalCode
     */
    public String getReversalCode() {
        return reversalCode;
    }

    /**
     * @param reversalCode the reversalCode to set
     */
    public void setReversalCode(String reversalCode) {
        this.reversalCode = reversalCode;
    }

    /**
     * @return the reversalMessage
     */
    public String getReversalMessage() {
        return reversalMessage;
    }

    /**
     * @param reversalMessage the reversalMessage to set
     */
    public void setReversalMessage(String reversalMessage) {
        this.reversalMessage = reversalMessage;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return the sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * @return the beneficiary
     */
    public String getBeneficiary() {
        return beneficiary;
    }

    /**
     * @param beneficiary the beneficiary to set
     */
    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the clientTxnRef
     */
    public String getClientTxnRef() {
        return clientTxnRef;
    }

    /**
     * @param clientTxnRef the clientTxnRef to set
     */
    public void setClientTxnRef(String clientTxnRef) {
        this.clientTxnRef = clientTxnRef;
    }

    /**
     * @return the transactResultResponseCode
     */
    public String getTransactResultResponseCode() {
        return transactResultResponseCode;
    }

    /**
     * @param transactResultResponseCode the transactResultResponseCode to set
     */
    public void setTransactResultResponseCode(String transactResultResponseCode) {
        this.transactResultResponseCode = transactResultResponseCode;
    }
    
    private String merchantId;
    private String currency;
    private double amount;
    private String datetime;
    private String sender;
    private String beneficiary;
    private String accountNo;
    private String responseCode;
    private String responseMessage;
    private String clientTxnRef;
    private String transactResultResponseCode;
    private String reversalCode;
    private String reversalMessage;
}

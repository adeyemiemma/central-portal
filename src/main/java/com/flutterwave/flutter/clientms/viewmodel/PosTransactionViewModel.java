/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.util.Utility;
import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public class PosTransactionViewModel {

    /**
     * @return the createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the reversed
     */
    public boolean isReversed() {
        return reversed;
    }

    /**
     * @param reversed the reversed to set
     */
    public void setReversed(boolean reversed) {
        this.reversed = reversed;
    }

    /**
     * @return the posId
     */
    public String getPosId() {
        return posId;
    }

    /**
     * @param posId the posId to set
     */
    public void setPosId(String posId) {
        this.posId = posId;
    }

    /**
     * @return the merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode the merchantCode to set
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the refCode
     */
    public String getRefCode() {
        return refCode;
    }

    /**
     * @param refCode the refCode to set
     */
    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
    
    private String rrn;
    @JsonProperty(value = "terminalid")
    private String terminalId;
    @JsonProperty(value = "mask")
    private String pan;
    private double amount;
    private String currency;
    @JsonProperty(value = "refcode")
    private String refCode;
    private String type;
    @JsonProperty(value = "responsecode")
    private String responseCode;
    @JsonProperty(value = "responsemessage")
    private String responseMessage;
    private String status;
    private String datetime;
    @JsonProperty(value = "merchantcode")
    private String merchantCode;
    @JsonProperty(value = "merchantid")
    private String merchantId;
    @JsonProperty(value = "posid")
    private String posId;
    private boolean reversed;
    private String createdOn;
    
    public static PosTransactionViewModel from(PosTransaction posTransaction){
        
        PosTransactionViewModel transactionViewModel = new PosTransactionViewModel();
        transactionViewModel.setAmount(posTransaction.getAmount());
        transactionViewModel.setCurrency(posTransaction.getCurrency());
        transactionViewModel.setDatetime(Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
        transactionViewModel.setPan(posTransaction.getPan());
        transactionViewModel.setRefCode(posTransaction.getRefCode());
        transactionViewModel.setResponseCode(posTransaction.getResponseCode());
        transactionViewModel.setResponseMessage(posTransaction.getResponseMessage());
        transactionViewModel.setRrn(posTransaction.getRrn());
        transactionViewModel.setStatus(posTransaction.getStatus());
        transactionViewModel.setTerminalId(posTransaction.getTerminalId());
        transactionViewModel.setType(posTransaction.getType());
        transactionViewModel.setPosId(posTransaction.getPosId());
        transactionViewModel.setReversed(posTransaction.isReversed());
        transactionViewModel.setCreatedOn(Utility.formatDate(posTransaction.getCreatedOn(), "yyyy-MM-dd HH:mm:ss"));
        
        return transactionViewModel;
    }
    
}

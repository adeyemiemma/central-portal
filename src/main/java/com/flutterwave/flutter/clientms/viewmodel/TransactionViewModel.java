/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import com.flutterwave.flutter.clientms.model.recon.ReconTransaction;

/**
 *
 * @author emmanueladeyemi
 */
public class TransactionViewModel {

    /**
     * @return the transaction
     */
    public ReconTransaction getTransaction() {
        return transaction;
    }

    /**
     * @param transaction the transaction to set
     */
    public void setTransaction(ReconTransaction transaction) {
        this.transaction = transaction;
    }

    /**
     * @return the age
     */
    public long getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(long age) {
        this.age = age;
    }
    
    private ReconTransaction transaction;
    private long age;
}

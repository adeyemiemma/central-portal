/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

/**
 *
 * @author emmanueladeyemi
 */
public class TransactionExportModel {

    /**
     * @return the merchantReference
     */
    public String getMerchantReference() {
        return merchantReference;
    }

    /**
     * @param merchantReference the merchantReference to set
     */
    public void setMerchantReference(String merchantReference) {
        this.merchantReference = merchantReference;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the transactionAmount
     */
    public double getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * @param transactionAmount the transactionAmount to set
     */
    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    /**
     * @return the transactionFee
     */
    public double getTransactionFee() {
        return transactionFee;
    }

    /**
     * @param transactionFee the transactionFee to set
     */
    public void setTransactionFee(double transactionFee) {
        this.transactionFee = transactionFee;
    }

    /**
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * @return the last4
     */
    public String getLast4() {
        return last4;
    }

    /**
     * @param last4 the last4 to set
     */
    public void setLast4(String last4) {
        this.last4 = last4;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the international
     */
    public int getInternational() {
        return international;
    }

    /**
     * @param international the international to set
     */
    public void setInternational(int international) {
        this.international = international;
    }
    
    private String merchantId;
    private String merchantName;
    private String reference;
    private double transactionAmount;
    private double transactionFee;
    private String transactionDate;
    private String last4;
    private String currency;
    private int international;
    private String transactionType;
    private String merchantReference;
}

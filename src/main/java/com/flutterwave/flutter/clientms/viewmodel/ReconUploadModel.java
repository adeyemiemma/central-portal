/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import java.util.Date;
import javax.servlet.http.Part;
import javax.validation.constraints.NotNull;

/**
 *
 * @author adeyemi
 */
public class ReconUploadModel {

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the settled
     */
    public boolean isSettled() {
        return settled;
    }

    /**
     * @param settled the settled to set
     */
    public void setSettled(boolean settled) {
        this.settled = settled;
    }

    /**
     * @return the dateSettled
     */
    public Date getDateSettled() {
        return dateSettled;
    }

    /**
     * @param dateSettled the dateSettled to set
     */
    public void setDateSettled(Date dateSettled) {
        this.dateSettled = dateSettled;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return the scheme
     */
    public String getScheme() {
        return scheme;
    }

    /**
     * @param scheme the scheme to set
     */
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }
    
    @NotNull
    private Part file;
    
    @NotNull
    // This is the card scheme that will be accepted
    private String scheme;
    
    @NotNull
    private String bank;

    /**
     * @return the file
     */
    public Part getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(Part file) {
        this.file = file;
    }
    
    private boolean settled;
    private Date dateSettled;
    private Date toDate;
    private Date fromDate;
    
    
}

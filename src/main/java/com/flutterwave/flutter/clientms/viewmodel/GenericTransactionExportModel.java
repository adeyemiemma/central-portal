/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
public class GenericTransactionExportModel {

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the merchantTransactionReference
     */
    public String getMerchantTransactionReference() {
        return merchantTransactionReference;
    }

    /**
     * @param merchantTransactionReference the merchantTransactionReference to set
     */
    public void setMerchantTransactionReference(String merchantTransactionReference) {
        this.merchantTransactionReference = merchantTransactionReference;
    }

    /**
     * @return the pnr
     */
    public String getPnr() {
        return pnr;
    }

    /**
     * @param pnr the pnr to set
     */
    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the transactionAmount
     */
    public double getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * @param transactionAmount the transactionAmount to set
     */
    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    /**
     * @return the transactionFee
     */
    public double getTransactionFee() {
        return transactionFee;
    }

    /**
     * @param transactionFee the transactionFee to set
     */
    public void setTransactionFee(double transactionFee) {
        this.transactionFee = transactionFee;
    }

    /**
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    
    private String merchantId;
    private String merchantName;
    private String reference;
    private double transactionAmount;
    private double transactionFee;
    private String transactionDate;
    private String currency;
    private String merchantTransactionReference;
    private String narration;
    private String pnr;

    @Override
    public String toString() {
       return new JSONObject(this).toString();
    }
    
    
}

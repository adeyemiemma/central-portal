/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import com.flutterwave.flutter.clientms.model.ChargeBack;
import com.flutterwave.flutter.clientms.util.ChargeBackState;
import com.flutterwave.flutter.clientms.util.ChargeBackStatus;
import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public class ChargeBackViewModel {

    /**
     * @return the parentId
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the flwReference
     */
    public String getFlwReference() {
        return flwReference;
    }

    /**
     * @param flwReference the flwReference to set
     */
    public void setFlwReference(String flwReference) {
        this.flwReference = flwReference;
    }

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn the rrn to set
     */
    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the authenticationType
     */
    public String getAuthenticationType() {
        return authenticationType;
    }

    /**
     * @param authenticationType the authenticationType to set
     */
    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the approvedOn
     */
    public Date getApprovedOn() {
        return approvedOn;
    }

    /**
     * @param approvedOn the approvedOn to set
     */
    public void setApprovedOn(Date approvedOn) {
        this.approvedOn = approvedOn;
    }

    /**
     * @return the approvedBy
     */
    public String getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the chargeBackStatus
     */
    public ChargeBackStatus getChargeBackStatus() {
        return chargeBackStatus;
    }

    /**
     * @param chargeBackStatus the chargeBackStatus to set
     */
    public void setChargeBackStatus(ChargeBackStatus chargeBackStatus) {
        this.chargeBackStatus = chargeBackStatus;
    }

    /**
     * @return the amountSettled
     */
    public double getAmountSettled() {
        return amountSettled;
    }

    /**
     * @param amountSettled the amountSettled to set
     */
    public void setAmountSettled(double amountSettled) {
        this.amountSettled = amountSettled;
    }

    /**
     * @return the productMid
     */
    public long getProductMid() {
        return productMid;
    }

    /**
     * @param productMid the productMid to set
     */
    public void setProductMid(long productMid) {
        this.productMid = productMid;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the applied
     */
    public boolean isApplied() {
        return applied;
    }

    /**
     * @param applied the applied to set
     */
    public void setApplied(boolean applied) {
        this.applied = applied;
    }

    /**
     * @return the appliedBy
     */
    public String getAppliedBy() {
        return appliedBy;
    }

    /**
     * @param appliedBy the appliedBy to set
     */
    public void setAppliedBy(String appliedBy) {
        this.appliedBy = appliedBy;
    }

    /**
     * @return the appliedOn
     */
    public Date getAppliedOn() {
        return appliedOn;
    }

    /**
     * @param appliedOn the appliedOn to set
     */
    public void setAppliedOn(Date appliedOn) {
        this.appliedOn = appliedOn;
    }

    /**
     * @return the chargeBackState
     */
    public ChargeBackState getChargeBackState() {
        return chargeBackState;
    }

    /**
     * @param chargeBackState the chargeBackState to set
     */
    public void setChargeBackState(ChargeBackState chargeBackState) {
        this.chargeBackState = chargeBackState;
    }
    
    private long id;
    private String product;
    private String merchantId;
    private double amount;
    private String flwReference;
    private String rrn;
    private String authenticationType;
    private String createdBy;
    private Date createdOn;
    private Date approvedOn;
    private String approvedBy;
    private ChargeBackStatus chargeBackStatus;
    private double amountSettled;
    private long productMid;
    private String currency;
    private boolean applied;
    private String appliedBy;
    private Date appliedOn;
    private ChargeBackState chargeBackState;
    private Long parentId;
    
    public static ChargeBackViewModel from(ChargeBack chargeBack){
        
        ChargeBackViewModel viewModel = new ChargeBackViewModel();
        viewModel.setAmount(chargeBack.getAmount());
        viewModel.setAmountSettled(chargeBack.getAmountSettled());
        viewModel.setApplied(chargeBack.isApplied());
        
        if(chargeBack.getProduct() != null)
            viewModel.setProduct(chargeBack.getProduct().getName());
        
        if(chargeBack.getAppliedBy() != null)
            viewModel.setAppliedBy(chargeBack.getAppliedBy().getFirstName()+" "+chargeBack.getAppliedBy().getLastName());
        
        viewModel.setAppliedOn(chargeBack.getAppliedOn());
        viewModel.setApprovedOn(chargeBack.getApprovedOn());
        
        if(chargeBack.getApprovedBy()!= null)
            viewModel.setApprovedBy(chargeBack.getApprovedBy().getFirstName()+" "+chargeBack.getApprovedBy().getLastName());
        
        viewModel.setAuthenticationType(chargeBack.getAuthenticationType());
        viewModel.setChargeBackState(chargeBack.getChargeBackState());
        viewModel.setChargeBackStatus(chargeBack.getChargeBackStatus());
        
        if(chargeBack.getCreatedBy()!= null)
            viewModel.setCreatedBy(chargeBack.getCreatedBy().getFirstName()+" "+chargeBack.getCreatedBy().getLastName());
        
        viewModel.setCreatedOn(chargeBack.getCreatedOn());
        viewModel.setCurrency(chargeBack.getCurrency());
        viewModel.setFlwReference(chargeBack.getFlwReference());
        viewModel.setId(chargeBack.getId());
        viewModel.setMerchantId(chargeBack.getMerchantId());
        
        if(chargeBack.getProduct() != null)
            viewModel.setProduct(chargeBack.getProduct().getName());
        
        viewModel.setProductMid(chargeBack.getProductMid());
        viewModel.setRrn(chargeBack.getRrn());
        viewModel.setParentId(chargeBack.getProductParentMid());
        
        return viewModel;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

/**
 *
 * @author emmanueladeyemi
 */
public class AnalysisModel {

    /**
     * @return the mcSuccessVolume
     */
    public long getMcSuccessVolume() {
        return mcSuccessVolume;
    }

    /**
     * @param mcSuccessVolume the mcSuccessVolume to set
     */
    public void setMcSuccessVolume(long mcSuccessVolume) {
        this.mcSuccessVolume = mcSuccessVolume;
    }

    /**
     * @return the mcSuccessAmount
     */
    public double getMcSuccessAmount() {
        return mcSuccessAmount;
    }

    /**
     * @param mcSuccessAmount the mcSuccessAmount to set
     */
    public void setMcSuccessAmount(double mcSuccessAmount) {
        this.mcSuccessAmount = mcSuccessAmount;
    }

    /**
     * @return the mcFailedVolume
     */
    public long getMcFailedVolume() {
        return mcFailedVolume;
    }

    /**
     * @param mcFailedVolume the mcFailedVolume to set
     */
    public void setMcFailedVolume(long mcFailedVolume) {
        this.mcFailedVolume = mcFailedVolume;
    }

    /**
     * @return the mcFailedAmount
     */
    public double getMcFailedAmount() {
        return mcFailedAmount;
    }

    /**
     * @param mcFailedAmount the mcFailedAmount to set
     */
    public void setMcFailedAmount(double mcFailedAmount) {
        this.mcFailedAmount = mcFailedAmount;
    }

    /**
     * @return the vcSuccessVolume
     */
    public long getVcSuccessVolume() {
        return vcSuccessVolume;
    }

    /**
     * @param vcSuccessVolume the vcSuccessVolume to set
     */
    public void setVcSuccessVolume(long vcSuccessVolume) {
        this.vcSuccessVolume = vcSuccessVolume;
    }

    /**
     * @return the vcSuccessAmount
     */
    public double getVcSuccessAmount() {
        return vcSuccessAmount;
    }

    /**
     * @param vcSuccessAmount the vcSuccessAmount to set
     */
    public void setVcSuccessAmount(double vcSuccessAmount) {
        this.vcSuccessAmount = vcSuccessAmount;
    }

    /**
     * @return the vcFailedVolume
     */
    public long getVcFailedVolume() {
        return vcFailedVolume;
    }

    /**
     * @param vcFailedVolume the vcFailedVolume to set
     */
    public void setVcFailedVolume(long vcFailedVolume) {
        this.vcFailedVolume = vcFailedVolume;
    }

    /**
     * @return the vcFailedAmount
     */
    public double getVcFailedAmount() {
        return vcFailedAmount;
    }

    /**
     * @param vcFailedAmount the vcFailedAmount to set
     */
    public void setVcFailedAmount(double vcFailedAmount) {
        this.vcFailedAmount = vcFailedAmount;
    }

    /**
     * @return the verveSuccessVolume
     */
    public long getVerveSuccessVolume() {
        return verveSuccessVolume;
    }

    /**
     * @param verveSuccessVolume the verveSuccessVolume to set
     */
    public void setVerveSuccessVolume(long verveSuccessVolume) {
        this.verveSuccessVolume = verveSuccessVolume;
    }

    /**
     * @return the verveSuccessAmount
     */
    public double getVerveSuccessAmount() {
        return verveSuccessAmount;
    }

    /**
     * @param verveSuccessAmount the verveSuccessAmount to set
     */
    public void setVerveSuccessAmount(double verveSuccessAmount) {
        this.verveSuccessAmount = verveSuccessAmount;
    }

    /**
     * @return the verveFailedVolume
     */
    public long getVerveFailedVolume() {
        return verveFailedVolume;
    }

    /**
     * @param verveFailedVolume the verveFailedVolume to set
     */
    public void setVerveFailedVolume(long verveFailedVolume) {
        this.verveFailedVolume = verveFailedVolume;
    }

    /**
     * @return the verveFailedAmount
     */
    public double getVerveFailedAmount() {
        return verveFailedAmount;
    }

    /**
     * @param verveFailedAmount the verveFailedAmount to set
     */
    public void setVerveFailedAmount(double verveFailedAmount) {
        this.verveFailedAmount = verveFailedAmount;
    }

    /**
     * @return the successVolume
     */
    public long getSuccessVolume() {
        return successVolume;
    }

    /**
     * @param successVolume the successVolume to set
     */
    public void setSuccessVolume(long successVolume) {
        this.successVolume = successVolume;
    }

    /**
     * @return the successAmount
     */
    public double getSuccessAmount() {
        return successAmount;
    }

    /**
     * @param successAmount the successAmount to set
     */
    public void setSuccessAmount(double successAmount) {
        this.successAmount = successAmount;
    }

    /**
     * @return the failedVolume
     */
    public long getFailedVolume() {
        return failedVolume;
    }

    /**
     * @param failedVolume the failedVolume to set
     */
    public void setFailedVolume(long failedVolume) {
        this.failedVolume = failedVolume;
    }

    /**
     * @return the failedAmount
     */
    public double getFailedAmount() {
        return failedAmount;
    }

    /**
     * @param failedAmount the failedAmount to set
     */
    public void setFailedAmount(double failedAmount) {
        this.failedAmount = failedAmount;
    }
    
    private long mcSuccessVolume;
    private double mcSuccessAmount;
    private long mcFailedVolume;
    private double mcFailedAmount;
    
    private long vcSuccessVolume;
    private double vcSuccessAmount;
    private long vcFailedVolume;
    private double vcFailedAmount;
    
    private long verveSuccessVolume;
    private double verveSuccessAmount;
    private long verveFailedVolume;
    private double verveFailedAmount;
    
    private long successVolume;
    private double successAmount;
    private long failedVolume;
    private double failedAmount;
    
}

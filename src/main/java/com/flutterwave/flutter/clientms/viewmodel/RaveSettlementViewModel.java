/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

/**
 *
 * @author emmanueladeyemi
 */
public class RaveSettlementViewModel {

    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the volume
     */
    public Integer getVolume() {
        return volume;
    }

    /**
     * @param volume the volume to set
     */
    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    /**
     * @return the refund
     */
    public double getRefund() {
        return refund;
    }

    /**
     * @param refund the refund to set
     */
    public void setRefund(double refund) {
        this.refund = refund;
    }

    /**
     * @return the chargeBack
     */
    public double getChargeBack() {
        return chargeBack;
    }

    /**
     * @param chargeBack the chargeBack to set
     */
    public void setChargeBack(double chargeBack) {
        this.chargeBack = chargeBack;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the amountToSettled
     */
    public double getAmountToSettled() {
        return amountToSettled;
    }

    /**
     * @param amountToSettled the amountToSettled to set
     */
    public void setAmountToSettled(double amountToSettled) {
        this.amountToSettled = amountToSettled;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the cost
     */
    public double getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * @return the fee
     */
    public double getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(double fee) {
        this.fee = fee;
    }
                                
    private String provider;
    private String merchantId;
    private String merchantName;
    private double amount;
    private double amountToSettled;
    private Integer volume;
    private long id;
    private double cost;
    private double fee;
    private double refund;
    private double chargeBack;
    private String accountNo;
    private String bankCode;
    
    
}

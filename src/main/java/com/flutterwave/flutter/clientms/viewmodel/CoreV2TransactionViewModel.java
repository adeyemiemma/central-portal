/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.viewmodel;

import com.flutterwave.flutter.clientms.model.GenericTransaction;

/**
 *
 * @author emmanueladeyemi
 */
public class CoreV2TransactionViewModel {

    /**
     * @return the customerReference
     */
    public String getCustomerReference() {
        return customerReference;
    }

    /**
     * @param customerReference the customerReference to set
     */
    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    /**
     * @return the recipientName
     */
    public String getRecipientName() {
        return recipientName;
    }

    /**
     * @param recipientName the recipientName to set
     */
    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    /**
     * @return the channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * @param channel the channel to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * @return the beneficiaryBank
     */
    public String getBeneficiaryBank() {
        return beneficiaryBank;
    }

    /**
     * @param beneficiaryBank the beneficiaryBank to set
     */
    public void setBeneficiaryBank(String beneficiaryBank) {
        this.beneficiaryBank = beneficiaryBank;
    }

    /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the fee
     */
    public double getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(double fee) {
        this.fee = fee;
    }

    /**
     * @return the feeValue
     */
    public double getFeeValue() {
        return feeValue;
    }

    /**
     * @param feeValue the feeValue to set
     */
    public void setFeeValue(double feeValue) {
        this.feeValue = feeValue;
    }

    /**
     * @return the sourceBank
     */
    public String getSourceBank() {
        return sourceBank;
    }

    /**
     * @param sourceBank the sourceBank to set
     */
    public void setSourceBank(String sourceBank) {
        this.sourceBank = sourceBank;
    }


    /**
     * @return the transactionIdentifier
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * @param transactionIdentifier the transactionIdentifier to set
     */
    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    /**
     * @return the responseDate
     */
    public String getResponseDate() {
        return responseDate;
    }

    /**
     * @param responseDate the responseDate to set
     */
    public void setResponseDate(String responseDate) {
        this.responseDate = responseDate;
    }

    /**
     * @return the statuscode
     */
    public String getStatuscode() {
        return statuscode;
    }

    /**
     * @param statuscode the statuscode to set
     */
    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    /**
     * @return the pwcmerchantId
     */
    public String getPwcmerchantId() {
        return pwcmerchantId;
    }

    /**
     * @param pwcmerchantId the pwcmerchantId to set
     */
    public void setPwcmerchantId(String pwcmerchantId) {
        this.pwcmerchantId = pwcmerchantId;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * @return the cardNo
     */
    public String getCardNo() {
        return cardNo;
    }

    /**
     * @param cardNo the cardNo to set
     */
    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the customerPhoneno
     */
    public String getCustomerPhoneno() {
        return customerPhoneno;
    }

    /**
     * @param customerPhoneno the customerPhoneno to set
     */
    public void setCustomerPhoneno(String customerPhoneno) {
        this.customerPhoneno = customerPhoneno;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the callbackUrl
     */
    public String getCallbackUrl() {
        return callbackUrl;
    }

    /**
     * @param callbackUrl the callbackUrl to set
     */
    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    /**
     * @return the bvn
     */
    public String getBvn() {
        return bvn;
    }

    /**
     * @param bvn the bvn to set
     */
    public void setBvn(String bvn) {
        this.bvn = bvn;
    }

    /**
     * @return the validateOption
     */
    public String getValidateOption() {
        return validateOption;
    }

    /**
     * @param validateOption the validateOption to set
     */
    public void setValidateOption(String validateOption) {
        this.validateOption = validateOption;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @param method the method to set
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * @return the transactionReference
     */
    public String getTransactionReference() {
        return transactionReference;
    }

    /**
     * @param transactionReference the transactionReference to set
     */
    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    /**
     * @return the transactiontoken
     */
    public String getTransactiontoken() {
        return transactiontoken;
    }

    /**
     * @param transactiontoken the transactiontoken to set
     */
    public void setTransactiontoken(String transactiontoken) {
        this.transactiontoken = transactiontoken;
    }

    /**
     * @return the usertoken
     */
    public String getUsertoken() {
        return usertoken;
    }

    /**
     * @param usertoken the usertoken to set
     */
    public void setUsertoken(String usertoken) {
        this.usertoken = usertoken;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the dateOfTransaction
     */
    public String getDateOfTransaction() {
        return dateOfTransaction;
    }

    /**
     * @param dateOfTransaction the dateOfTransaction to set
     */
    public void setDateOfTransaction(String dateOfTransaction) {
        this.dateOfTransaction = dateOfTransaction;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the requestStatus
     */
    public String getRequestStatus() {
        return requestStatus;
    }

    /**
     * @param requestStatus the requestStatus to set
     */
    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    /**
     * @return the apiType
     */
    public String getApiType() {
        return apiType;
    }

    /**
     * @param apiType the apiType to set
     */
    public void setApiType(String apiType) {
        this.apiType = apiType;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    private String transactionIdentifier;
    private String responseDate;
    private String statuscode;
    private String pwcmerchantId;
    private String customerName;
    private String provider;
    private String merchantId;
    private String currency;
    private String pan;
    private String cardNo;
    private String email;
    private String customerPhoneno;
    private String merchantName;
    private String callbackUrl;
    private String bvn;
    private String validateOption;
    private double amount;
    private String method;
    private String transactionReference;
    private String transactiontoken;
    private String usertoken;
    private String accountNumber;
    private String dateOfTransaction;
    private String narration;
    private String requestStatus;
    private String apiType;
    private String status;
    private String bankCode;
    private double fee;
    private double feeValue;
    private String sourceBank;
    private String beneficiaryBank;
    private String recipientName;
    private String channel;
    private String customerReference;
    
    public GenericTransaction getGeneric(){
        
        GenericTransaction genericTransaction = new GenericTransaction();
        genericTransaction.setAmount(amount);
        genericTransaction.setCreatedOn(dateOfTransaction); 
        
        genericTransaction.setFlutterReference(transactionReference);
        genericTransaction.setId(0L);
        genericTransaction.setLinkingReference(transactionReference);
        genericTransaction.setMerchant(getMerchantName());
//        genericTransaction.setSource(source);
        genericTransaction.setStatus(status);
        genericTransaction.setTransactionReference(transactionReference);
//        genericTransaction.setUpdatedAt(updatedAt);
        genericTransaction.setCurrency(currency == null ? "NGN" : currency);
        genericTransaction.setMerchantReference(customerReference);
        genericTransaction.setTransactionType(apiType);
        genericTransaction.setFee(fee);
        genericTransaction.setStatusCode(statuscode);
        
        return genericTransaction;
    }

}

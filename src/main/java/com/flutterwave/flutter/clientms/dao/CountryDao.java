/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Country;
import com.flutterwave.flutter.clientms.model.Country;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class CountryDao extends HibernateDao<Country> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public CountryDao() {
        super(Country.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Country findByCurrency(Currency product) throws DatabaseException{
        
        if(product == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<Country> query = builder.createQuery(Country.class);

            // This used to map the entities to a class and extract the fields involved
            Root<Country> rootQuery = query.from(Country.class);

            ParameterExpression keyParam = builder.parameter(Currency.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("currency"), keyParam));

            List<Country> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, product)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list.get(0);
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
        
    }
    
}

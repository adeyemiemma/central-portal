/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.ConfigurationFee;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author adeyemi
 */
@Stateless
public class ConfigurationFeeDao extends com.flutterwave.flutter.core.dao.HibernateDao<ConfigurationFee> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public ConfigurationFeeDao() {
        super(ConfigurationFee.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }  
    
    public ConfigurationFee findByProduct(Product product) throws DatabaseException{
        
        if(product == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<ConfigurationFee> query = builder.createQuery(ConfigurationFee.class);

            // This used to map the entities to a class and extract the fields involved
            Root<ConfigurationFee> rootQuery = query.from(ConfigurationFee.class);

            ParameterExpression keyParam = builder.parameter(Product.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("product"), keyParam));

            List<ConfigurationFee> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, product)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list.get(0);
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.MerchantConfig;
import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.Settlement;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class MerchantConfigDao extends HibernateDao<MerchantConfig> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public MerchantConfigDao() {
        super(MerchantConfig.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<MerchantConfig> findByMerchant(Long merchant, String currency) throws DatabaseException{
        
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<MerchantConfig> query = builder.createQuery(MerchantConfig.class);
            
            ParameterExpression keyParamId = builder.parameter(Long.class);
            ParameterExpression keyParamMerchant = builder.parameter(Long.class);
            ParameterExpression keyParamCurrency = builder.parameter(String.class);
        
            // This used to map the entities to a class and extract the fields involved
            Root<MerchantConfig> rootQuery = query.from(MerchantConfig.class);
            
            Predicate predicate = builder.gt(rootQuery.get("id"), keyParamId);

            if(merchant != null)
                predicate = builder.and(predicate, builder.equal(rootQuery.get("coreMerchantId"), keyParamMerchant));
            
            if(currency != null)
                predicate = builder.and(predicate, builder.equal(rootQuery.get("currency"), keyParamCurrency));
            
            query.select(rootQuery).where(predicate).orderBy(builder.desc(rootQuery.get("id")));
        
            javax.persistence.Query queryP =  getEntityManager().createQuery(query);

            queryP.setParameter(keyParamId, 0L);

            if(merchant != null){
                queryP.setParameter(keyParamMerchant, merchant);
            }

            if(currency != null){
                queryP.setParameter(keyParamCurrency, currency);
            }


            List<MerchantConfig> list = queryP.getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
}

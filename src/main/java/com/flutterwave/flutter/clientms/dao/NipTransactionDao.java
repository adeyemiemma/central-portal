/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.NipTransaction;
import com.flutterwave.flutter.clientms.model.TransactionListModel;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.util.Page;
import com.sun.webkit.Utilities;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class NipTransactionDao extends HibernateDao<NipTransaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME_NEW)
    private EntityManager entityManager;

    public NipTransactionDao() {
        super(NipTransaction.class);
    }

    @Override
    public EntityManager getEntityManager() {

        return entityManager;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<NipTransaction> getTransactions(int pageNo, int pageSize, Date startDate, Date endDate, 
            String status, String searchParam, String searchValue, boolean noCount) {

        String query = " where transactionTime between '" + Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss") + "'";

        query += " and '" + Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss") + "'";

        if (Utility.emptyToNull(searchParam) != null && Utility.emptyToNull(searchValue) != null) {

            if("paymentReference".equalsIgnoreCase(searchParam) || "sessionId".equalsIgnoreCase(searchParam)){
                
                // This is used to convert a comma separated string to a string that can be used as in search
                String newParam = Stream.of(searchValue.split(",")).map(x -> "\""+x.trim()+"\"").collect(Collectors.joining(","));
                
                query += " and " + searchParam + " In (" + newParam + ") ";
                
            }else
                query += " and " + searchParam + " like '%" + searchValue + "%'";
        }
        
        if(Utility.emptyToNull(status) != null)
            query += " and status= '" + status + "'";

        List<NipTransaction> nipTransactions;

        if (pageSize == 0) {
            nipTransactions = entityManager.createNativeQuery("select * from nip_transaction " + query+" order by transactionTime desc", entityClass)
                    .setFirstResult(pageNo)
                    .getResultList();
        } else {

            nipTransactions = entityManager.createNativeQuery("select * from nip_transaction " + query+" order by transactionTime desc", entityClass)
                    .setFirstResult(pageNo)
                    .setMaxResults(pageSize)
                    .getResultList();
        }

        long count = 0;

        if (noCount == false) {
            Object countResult = entityManager.createNativeQuery("select count(*) from nip_transaction " + query).getSingleResult();

            count = Long.parseLong(countResult + "");
        }

        Page<NipTransaction> page = new Page(count, nipTransactions);

        return page;
    }
}

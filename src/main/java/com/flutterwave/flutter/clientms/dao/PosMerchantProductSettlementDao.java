/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.PosMerchantProductSettlement;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class PosMerchantProductSettlementDao extends HibernateDao<PosMerchantProductSettlement> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME_NEW)
    private EntityManager entityManager;

    public PosMerchantProductSettlementDao() {
        super(PosMerchantProductSettlement.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public PosMerchantProductSettlement findByMerchantAndProductPrefix(String merchantCode, String productCode){
        
//        if(Utility.emptyToNull(posOrderId) != null)
//            query += " and pos = '"+posOrderId+"'";
        
        List<PosMerchantProductSettlement> products = entityManager.createNamedQuery("PosMerchantProductSettlement.findByMerchantCodeAndProductPrefix",PosMerchantProductSettlement.class)
                .setParameter("productPrefix", productCode)
                .setParameter("merchantCode", merchantCode)
                .getResultList();
        
        if(products == null || products.isEmpty())
            return null;
        
        return products.get(0);
    }
    
}


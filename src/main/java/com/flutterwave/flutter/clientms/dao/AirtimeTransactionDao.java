/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.Settlement;
import com.flutterwave.flutter.clientms.model.SettlementReportModel;
import com.flutterwave.flutter.clientms.model.AirtimeTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class AirtimeTransactionDao extends HibernateDao<AirtimeTransaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public AirtimeTransactionDao() {
        super(AirtimeTransaction.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<AirtimeTransaction> findByKeyLike(String key, String value) throws DatabaseException{
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<AirtimeTransaction> query = builder.createQuery(entityClass);

            // This used to map the entities to a class and extract the fields involved
            Root<AirtimeTransaction> rootQuery = query.from(entityClass);

            ParameterExpression keyParam = builder.parameter(Long.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.like(rootQuery.get(key), keyParam)).orderBy(builder.desc(rootQuery.get("id")));

            List<AirtimeTransaction> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, value)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
//    public AirtimeTransaction FindRefundAirtimeTransaction(String transactionReference){
//        String query = " where airtime_transaction.id <> 0 and responsecode='00' and transactionType = 'CARD_REFUND'";
//        
//        
//        List<AirtimeTransaction> list = entityManager.createNativeQuery("select * from airtime_transaction "+ query +"", AirtimeTransaction.class)
//                .getResultList();
//        
//        return list == null ? null : list.get(0);
//    }
    
//    public List<AirtimeTransaction> getUnsettledPendingTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId){
//        
//        String query = " where airtime_transaction.id <> 0 and settlementState <> 2 and (settled=0 or settled is NULL) and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND') ";
////        String joinQuery = "";
//        
//        if(startDate != null){
////            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            
//            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            
////             query +=" and date(STR_TO_DATE(airtime_transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(airtime_transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
//            query +=" and TIMESTAMP(airtime_transaction.datetime) >= TIMESTAMP('"+dateTimeFormat.format(startDate)+"') and TIMESTAMP(airtime_transaction.datetime) <= TIMESTAMP('"+dateTimeFormat.format(endDate)+"')";
//        }
//        
//        if(currency != null){
//            query += " and airtime_transaction.transactionCurrency='"+currency+"'";
//        }
//        
//        if(provider != null){
////            joinQuery += " join pro on merchants.id=airtime_transaction.merchantId";
//            
//            query += " and airtime_transaction.provider ='"+provider.getShortName()+"'";
//        }
//        
//        if(merchantId != null){
//            
//            query += " and airtime_transaction.merchantId ='"+merchantId+"'";
//        }
//        
//        List<AirtimeTransaction> list = entityManager.createNativeQuery("select * from airtime_transaction "+ query +"", AirtimeTransaction.class)
//                .getResultList();
//        
//        return list;
//    }
//    
//    public List<AirtimeTransaction> getSettleableTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId){
//        
//        String query = " where airtime_transaction.id <> 0 and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND') ";
////        String joinQuery = "";
//        
//        if(startDate != null){
////            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            
//            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            
////             query +=" and date(STR_TO_DATE(airtime_transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(airtime_transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
//            query +=" and TIMESTAMP(airtime_transaction.datetime) >= TIMESTAMP('"+dateTimeFormat.format(startDate)+"') and TIMESTAMP(airtime_transaction.datetime) <= TIMESTAMP('"+dateTimeFormat.format(endDate)+"')";
//        }
//        
//        if(currency != null){
//            query += " and airtime_transaction.transactionCurrency='"+currency+"'";
//        }
//        
//        if(provider != null){
////            joinQuery += " join pro on merchants.id=airtime_transaction.merchantId";
//            
//            query += " and airtime_transaction.provider ='"+provider.getShortName()+"'";
//        }
//        
//        if(merchantId != null){
//            
//            query += " and airtime_transaction.merchantId ='"+merchantId+"'";
//        }
//        
//        List<AirtimeTransaction> list = entityManager.createNativeQuery("select * from airtime_transaction "+ query +"", AirtimeTransaction.class)
//                .getResultList();
//        
//        return list;
//    }
//    
//    public List<AirtimeTransaction> getUnsettledTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId){
//        
//        String query = " where airtime_transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and (settled=0 or settled is NULL) and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND') ";
////        String joinQuery = "";
//        
//        if(startDate != null){
////            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            
//            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            
////             query +=" and date(STR_TO_DATE(airtime_transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(airtime_transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
//            query +=" and TIMESTAMP(airtime_transaction.datetime) >= TIMESTAMP('"+dateTimeFormat.format(startDate)+"') and TIMESTAMP(airtime_transaction.datetime) <= TIMESTAMP('"+dateTimeFormat.format(endDate)+"')";
//        }
//        
//        if(currency != null){
//            query += " and airtime_transaction.transactionCurrency='"+currency+"'";
//        }
//        
//        if(provider != null){
////            joinQuery += " join pro on merchants.id=airtime_transaction.merchantId";
//            
//            query += " and airtime_transaction.provider ='"+provider.getShortName()+"'";
//        }
//        
//        if(merchantId != null){
//            
//            query += " and airtime_transaction.merchantId ='"+merchantId+"'";
//        }
//        
//        List<AirtimeTransaction> list = entityManager.createNativeQuery("select * from airtime_transaction "+ query +"", AirtimeTransaction.class)
//                .getResultList();
//        
//        return list;
//    }
//    
//    public List<AirtimeTransaction> getUnsettledTransanctionAccount(Date startDate, Date endDate, Provider provider, String currency, String merchantId){
//        
//        String query = " where airtime_transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and "
//                + "(settled=0 or settled is NULL) and responsecode='00' and (transactionType='ACCOUNT_PURCHASE') ";
////        String joinQuery = "";
//        
//        if(startDate != null){
////            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            
//            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            
////             query +=" and date(STR_TO_DATE(airtime_transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(airtime_transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
//            query +=" and TIMESTAMP(airtime_transaction.datetime) >= TIMESTAMP('"+dateTimeFormat.format(startDate)+"') and TIMESTAMP(airtime_transaction.datetime) <= TIMESTAMP('"+dateTimeFormat.format(endDate)+"')";
//        }
//        
//        if(currency != null){
//            query += " and airtime_transaction.transactionCurrency='"+currency+"'";
//        }
//        
//        if(provider != null){
////            joinQuery += " join pro on merchants.id=airtime_transaction.merchantId";
//            
//            query += " and airtime_transaction.provider ='"+provider.getShortName()+"'";
//        }
//        
//        if(merchantId != null){
//            
//            query += " and airtime_transaction.merchantId ='"+merchantId+"'";
//        }
//        
//        List<AirtimeTransaction> list = entityManager.createNativeQuery("select * from airtime_transaction "+ query +"", AirtimeTransaction.class)
//                .getResultList();
//        
//        return list;
//    }
//    
//    public List<SettlementReportModel> getUnsettledSettlement(Date startDate, Date endDate, Provider provider, String currency){
//        
//        String query = " where airtime_transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and (settled=0 or settled is NULL) and responsecode='00' "
//                + "and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE') ";
////        String joinQuery = "";
//        
//        if(startDate != null){
////            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            query +=" and TIMESTAMP(airtime_transaction.datetime) >= TIMESTAMP('"+dateFormat.format(startDate)+"') and TIMESTAMP(airtime_transaction.datetime) <= TIMESTAMP('"+dateFormat.format(endDate)+"')";
//        }
//        
//        if(currency != null){
//            query += " and airtime_transaction.transactionCurrency='"+currency+"'";
//        }
//        
//        if(provider != null){
////            joinQuery += " join pro on merchants.id=airtime_transaction.merchantId";
//            
//            query += " and airtime_transaction.provider_id ="+provider.getId()+"";
//        }
//        
//        List<SettlementReportModel> list = entityManager.createNativeQuery("select * from airtime_transaction "+ query +" group by merchantId", SettlementReportModel.class)
//                .getResultList();
//        
//        return list;
//    }
    
    
    public Page<AirtimeTransaction> getAirtimeTransactions(int pageNum, int pageSize, Date startDate, Date endDate, String reference, 
            String currency, String provider, String merchantId,String statusCode, String type, String cardScheme,
            String transactionResponseCode, String reverseResponseCode, Boolean isRefund){
        
        String query = " where airtime_transaction.id <> 0 ";
        
//        if(isRefund != null)
//            if(!isRefund){
//                query += " and transactionType <> 'CARD_REFUND'";
//            }else
//                query += " and transactionType = 'CARD_REFUND'";
//        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query +=" and TIMESTAMP(airtime_transaction.datetime) >= TIMESTAMP('"+dateFormat.format(startDate)+"') and TIMESTAMP(airtime_transaction.datetime) <= TIMESTAMP('"+dateFormat.format(endDate)+"')";
        }
        
        if(cardScheme != null){
            query += " and cardscheme = '"+cardScheme+"'";
        }
        
        if(currency != null){
            query += " and airtime_transaction.transactionCurrency='"+currency+"'";
        }
        
        if(merchantId != null){
            query += " and airtime_transaction.merchantId='"+merchantId+"'";
        }
        
        if(provider != null){
            query += " and airtime_transaction.provider ='"+provider+"'";
        }
        
        if(statusCode != null){
            query += " and airtime_transaction.responseCode='"+statusCode+"'";
        }
        
        if(transactionResponseCode != null){
            query += " and airtime_transaction.transactResultResponseCode='"+transactionResponseCode+"'";
        }
        
        if(reverseResponseCode != null){
            query += " and airtime_transaction.reverseResponseCode='"+reverseResponseCode+"'";
        }
 
        
//        if(category != null || type != null ){
//            query += " (";
//        }
//        
        
        if(type != null){
            query += " and airtime_transaction.transactionType='"+type+"'";
        }
        
//        if(category != null || type != null ){
//            query += " )";
//        }
//        
        if(provider != null){
            query += " and airtime_transaction.provider ='"+provider+"'";
        }
        
        if(reference != null){
            query += " and ( airtime_transaction.flwTxnReference like '%"+reference+"%' or airtime_transaction.merchantTxnReference like '%"+reference+"%' or airtime_transaction.targetIdentifier like '%"+reference+"%' or airtime_transaction.sourceIdentifier like '%"+reference+"%' or airtime_transaction.cardMask like '%"+reference+"%'  )";
        }
        
        
         List<AirtimeTransaction> list = entityManager.createNativeQuery("select * from airtime_transaction "+ query +" order by TIMESTAMP(datetime) desc ", AirtimeTransaction.class)
                 .setFirstResult(pageNum)
                 .setMaxResults(pageSize)
                .getResultList();
         
        Object countResult = entityManager.createNativeQuery("select count(*) as count from airtime_transaction "+ query +"").getSingleResult();
         
        long count = Long.parseLong(countResult+"");
        
        Page<AirtimeTransaction> transactions = new Page(count, list);
        
        return transactions;
    }
    
    public Map<String, Object> getAirtimeTransactionSummary(Date startDate, Date endDate, String reference, 
            String currency, String provider, String merchantId, Boolean isRefund){
        
        String query = " where airtime_transaction.id <> 0 ";
        
//        if(isRefund != null)
//            if(!isRefund){
//                query += " and transactionType <> 'CARD_REFUND'";
//            }else
//                query += " and transactionType = 'CARD_REFUND'";
//        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query +=" and TIMESTAMP(airtime_transaction.datetime) >= TIMESTAMP('"+dateFormat.format(startDate)+"') and TIMESTAMP(airtime_transaction.datetime) <= TIMESTAMP('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and airtime_transaction.transactionCurrency='"+currency+"'";
        }
        
         if(merchantId != null){
            query += " and airtime_transaction.merchantId='"+merchantId+"'";
        }
        
        if(provider != null){
            query += " and airtime_transaction.provider ='"+provider+"'";
        }
        
        if(reference != null){
            query += " and (airtime_transaction.flwTxnReference like '%"+reference+"%' or airtime_transaction.merchantTxnReference like '%"+reference+"%' )";
        }
        
        Object result = entityManager.createNativeQuery("select sum(amount) from airtime_transaction "+ query +"").getSingleResult();
         
        Object countResult = entityManager.createNativeQuery("select count(distinct(merchantId)) as count from airtime_transaction "+ query +" ").getSingleResult();
         
        Map<String, Object> map = new HashMap<>();
        
        map.put("amount", result);
        map.put("merchant", countResult);
        
        
        return map;
    }
    
        
//    public List<AirtimeTransaction> findBySettlement(Settlement settlement)
//        throws DatabaseException{
//        
//        if(settlement == null)
//            return null;
//        
//        try{
//        // This is used to build the criteria
//            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
//
//            // This is used to build a cq instance from the entity
//            CriteriaQuery<AirtimeTransaction> query = builder.createQuery(AirtimeTransaction.class);
//
//            // This used to map the entities to a class and extract the fields involved
//            Root<AirtimeTransaction> rootQuery = query.from(AirtimeTransaction.class);
//
//            ParameterExpression keyParam = builder.parameter(Settlement.class);
//
//            // This is used to build the cq that will allow the keyParam
//            query.select(rootQuery).where(builder.equal(rootQuery.get("settlement"), keyParam));
//
//            List<AirtimeTransaction> list = getEntityManager().createQuery(query)
//                    .setParameter(keyParam, settlement)
//                    .getResultList();
//
//            if(list.isEmpty())
//                return null;
//
//            return list;
//        }catch(Exception ex){
//            throw new DatabaseException(ex);
//        }
//    }
    
    public List getSummaryWithDate(Date startDate, Date endDate, String currency, String provider){
        
        String query = " where airtime_transaction.id <> 0 and responsecode='00' ";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(STR_TO_DATE(airtime_transaction.datetime, '%Y-%m-%d')) >= date('"+dateFormat.format(startDate)+"') and date(airtime_transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and airtime_transaction.transactionCurrency='"+currency+"'";
        }
        
        if(provider != null){
//            joinQuery += " join pro on merchants.id=airtime_transaction.merchantId";
            
            query += " and airtime_transaction.provider ='"+provider+"'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), 0 ,count(*), date(STR_TO_DATE(airtime_transaction.datetime, '%Y-%m-%d')), responseCode, responseMessage from airtime_transaction "+joinQuery+" "+query + " group by date(STR_TO_DATE(airtime_transaction.datetime, '%Y-%m-%d')), responseCode")
                .getResultList();
        
        return list;
    }
    
    
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED) 
    public boolean exists(String param, String value){
        
        
        List list = getEntityManager().createNativeQuery("select * from airtime_transaction where "+param+"='"+value+"' limit 1").getResultList();
        
        if(list == null)
            return false;
        
        return list.size() > 0 ;
    }
    
    public List getSummaryWithDate(Date startDate, Date endDate){
        
        String query = " where airtime_transaction.id <> 0 and responsecode='00' ";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(airtime_transaction.datetime) >= date('"+dateFormat.format(startDate)+"') and date(airtime_transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), count(*), transactionCurrency,  merchantId, cardCountry from airtime_transaction "+joinQuery+" "+query + " group by date(STR_TO_DATE(airtime_transaction.datetime, '%Y-%m-%d')), transactionCurrency, merchantId")
                .getResultList();
        
        return list;
    }
    
    public List getAirtimeTransactionSummarybyMonth(Date startDate, Date endDate){
        
        String query = " where airtime_transaction.id <> 0 and responsecode='00' ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(airtime_transaction.datetime) >= date('"+dateFormat.format(startDate)+"') and date(airtime_transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d') from airtime_transaction "+query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency ")
                .getResultList();
        
        return list;
    }
    
    
    public Map<String, String> getResponse(String category){
        
        String whereClause = "";
        
        if(category != null && !"".equals(category)){
            whereClause += " where transactionType like '"+category+"%'";
        }
        
        List list = entityManager.createNativeQuery("select distinct(responseCode), responseMessage from airtime_transaction "+whereClause+" group by responseCode").getResultList();
        
        final Map<String, String> responses = new TreeMap<>();
        
        list.stream().forEachOrdered((data) -> {
            
           Object[] x = (Object[]) data;
            
           responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });
        
        return responses;
    }
    
    public Map<String, String> getTransactionResponse(){
        
        String whereClause = "";
        
//        if(category != null && !"".equals(category)){
//            whereClause += " where transactionType like '"+category+"%'";
//        }
        
        List list = entityManager.createNativeQuery("select distinct(transactResultResponseCode) from airtime_transaction").getResultList();
        
        final Map<String, String> responses = new TreeMap<>();
        
        list.stream().forEachOrdered((data) -> {
            
//           Object[] x = (Object[]) data;
            
           responses.put(String.valueOf(data), String.valueOf(data));
        });
        
        return responses;
    }
    
    public Map<String, String> getReverseTransactionResponse(){
        
        String whereClause = "";
        
//        if(category != null && !"".equals(category)){
//            whereClause += " where transactionType like '"+category+"%'";
//        }
        
        List list = entityManager.createNativeQuery("select reverseResponseCode, reverseResponseMessage from airtime_transaction "+whereClause+" group by reverseResponseCode").getResultList();
        
        final Map<String, String> responses = new TreeMap<>();
        
        list.stream().forEachOrdered((data) -> {
            
           Object[] x = (Object[]) data;
            
           responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });
        
        return responses;
    }
    
    public Map<String, String> getDisburseResponse(){
        
        
        List list = entityManager.createNativeQuery("select disburseResponseCode, disburseResponseMessage from airtime_transaction group by disburseResponseCode").getResultList();
        
        final Map<String, String> responses = new TreeMap<>();
        
        list.stream().forEachOrdered((data) -> {
            
           Object[] x = (Object[]) data;
            
           responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });
        
        return responses;
    }
    
    public List<String> getAirtimeTransactionType(String category){
        
        List list = entityManager.createNativeQuery("select transactionType from airtime_transaction group by transactionType").getResultList();
        
        final List<String> responses = new ArrayList<>();
        
        list.stream().filter(x -> category == null ? x != null : x.toString().toLowerCase().startsWith(category))
                .forEachOrdered((data) -> {
            
           responses.add(String.valueOf(data));
        });
        
        return responses;
    }
    
    public List getTotalSummary(Date startDate, Date endDate, Boolean successful){
        
        String format = "yyyy-MM-dd";
        
//        String startDateString = Utility.formatDate(startDate, format );
//        String endDateString = Utility.formatDate(endDate, format);
        
        String query = " where airtime_transaction.id <> 0";
        
        if(successful != null)
            query += " and "+(successful == true ? " responsecode = '00'" : "responsecode <> '00' " );
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            query +=" and date(airtime_transaction.datetime) >= date('"+dateFormat.format(startDate)+"') and date(airtime_transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
//        query +=" and date(airtime_transaction.datetime) >= date('"+startDateString+"') and date(airtime_transaction.datetime) <= date('"+endDateString+"')";
        
        List result = entityManager.createNativeQuery("select sum(amount), count(*) from airtime_transaction "+query + " ")
                .getResultList();
        
        return result;
    }
    
    public List getAirtimeTransactionByMonth(Date date){
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date startDate = calendar.getTime();
        
        
        calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date endDate = calendar.getTime();        
        
        String query = " where airtime_transaction.id <> 0 and responsecode='00' ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(airtime_transaction.datetime) >= date('"+dateFormat.format(startDate)+"') and date(airtime_transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d') from airtime_transaction "+query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency; ")
                .getResultList();
        
        return list;
    }
    
    public List getReport(Date startDate, Date endDate, boolean successful, boolean card){
        
        String format = "yyyy-MM-dd";
        
        String startDateString = Utility.formatDate(startDate, format );
        String endDateString = Utility.formatDate(endDate, format);
        
        String query = " where airtime_transaction.id <> 0 and  "+(successful == true ? " responsecode = '00'" : "responsecode <> '00' " );
        
//        if(card == true){
//            query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE')";
//        }else
//            query += " and transactionType = 'ACCOUNT_PURCHASE'";
        
        query +=" and date(airtime_transaction.datetime) >= date('"+startDateString+"') and date(airtime_transaction.datetime) <= date('"+endDateString+"')";
        
        List result = entityManager.createNativeQuery("select sum(amount), count(*), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d') from airtime_transaction "+query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency ")
                .getResultList();
        
        return result;
    }
    
    public List getFailureReason(Date startDate, Date endDate, boolean card){
        
        String format = "yyyy-MM-dd";
        
        String startDateString = Utility.formatDate(startDate, format );
        String endDateString = Utility.formatDate(endDate, format);
        
        String query = " where airtime_transaction.id <> 0 and responsecode <> '00'";
        
//        if(card == true){
//            query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE')";
//        }else
//            query += " and transactionType = 'ACCOUNT_PURCHASE'";
        
        query +=" and date(airtime_transaction.datetime) >= date('"+startDateString+"') and date(airtime_transaction.datetime) <= date('"+endDateString+"')";
        
        List result = entityManager.createNativeQuery("select sum(amount), count(*), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d'), responseMessage, cardScheme from airtime_transaction "+query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency,responsecode, cardScheme ")
                .getResultList();
        
        return result;
    }
    
    public List getSuccessAnalysis(Date startDate, Date endDate, boolean card){
        
        String format = "yyyy-MM-dd";
        
        String startDateString = Utility.formatDate(startDate, format );
        String endDateString = Utility.formatDate(endDate, format);
        
        String query = " where airtime_transaction.id <> 0 and responsecode = '00'";
        
//        if(card == true){
//            query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE')";
//        }else
//            query += " and transactionType = 'ACCOUNT_PURCHASE'";
        
        query +=" and date(airtime_transaction.datetime) >= date('"+startDateString+"') and date(airtime_transaction.datetime) <= date('"+endDateString+"')";
        
        List result = entityManager.createNativeQuery("select sum(amount), count(*), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d'), cardScheme from airtime_transaction "+query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency, cardScheme ")
                .getResultList();
        
        return result;
    }
    
    public List<String> getDistinctMerchant(Date startDate, Date endDate){
        
        String format = "yyyy-MM-dd";
        
        String startDateString = Utility.formatDate(startDate, format );
        String endDateString = Utility.formatDate(endDate, format);
        
        String query = " where airtime_transaction.id <> 0 and responsecode = '00'";
        
//        query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE' or transactionType = 'ACCOUNT_PURCHASE')";
        
        query +=" and date(airtime_transaction.datetime) >= date('"+startDateString+"') and date(airtime_transaction.datetime) <= date('"+endDateString+"')";
        
        List result = entityManager.createNativeQuery("select distinct(merchantId) from airtime_transaction "+query + "")
                .getResultList();
        
        return result;
    }
    
    public List getSettlementSummary(Date startDate, Date endDate){
        
        String format = "yyyy-MM-dd HH:mm:ss";
        
        String startDateString = Utility.formatDate(startDate, format );
        String endDateString = Utility.formatDate(endDate, format);
        
        String query = " where ( airtime_transaction.id <> 0 and responsecode = '00' and airtime_transaction.transactResultResponseCode = '0000' )";
        
//        query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE' or transactionType = 'ACCOUNT_PURCHASE')";
        
        query +=" and airtime_transaction.datetime between '"+startDateString+"' and '"+endDateString+"'";
        
        List result = entityManager.createNativeQuery("select sum(amount), count(*) from airtime_transaction "+query + "")
                .getResultList();
        
        return result;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.recon;

import com.flutterwave.flutter.clientms.model.recon.Batch;
import com.flutterwave.flutter.clientms.model.recon.FlutterTransaction;
import com.flutterwave.flutter.clientms.model.recon.ReconTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.math.BigInteger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.jboss.ejb3.annotation.TransactionTimeout;

/**
 *
 * @author adeyemi
 */
@Stateless
public class ReconTransactionDao extends com.flutterwave.flutter.core.dao.HibernateDao<ReconTransaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;
    
//    @Resource
//    private SessionContext context;
    
//    @EJB 
//    private FlutterTransactionDao flutterTransactionDao;

    public ReconTransactionDao() {
        super(ReconTransaction.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        
        return entityManager;
    }
    
    
    public Page<ReconTransaction> getTransactions(Date startDate, Date endDate, String searchValue, Object searchColumn, 
            int pageSize, int pageNum, Boolean status, String bank){
        
        String query;
        
        if(startDate == null){
            query = " where id <> 0 ";
        }else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query =" where date(operationDate) >= date('"+dateFormat.format(startDate)+"') and date(operationDate) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(searchValue != null && !searchValue.isEmpty()){
            query += " and concat_ws(' '"+TransactionUtility.populateIndex().get(searchColumn)+") like '%"+searchValue+"%'";
        }
        
        if(bank != null && !"null".equalsIgnoreCase(bank))
            query += " and bank='"+bank+"'";
        
        if(status != null)
            query += " and settled="+status; 
                
        Page<ReconTransaction> page = new Page<>();
        
        List<ReconTransaction> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select * from recon_transaction "+query, ReconTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<ReconTransaction>)entityManager.createNativeQuery("select * from recon_transaction "+query +"", ReconTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from recon_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    public Page<ReconTransaction> getTransaction(Date startDate, Date endDate, String column, long data, int pageSize, int pageNum, Boolean status, String bank){
        
        String query = " where "+column+"="+data;
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        if(startDate != null){
            
            query += " and date(operationDate) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(operationDate) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(status != null){
            query += " and settled="+status;
        }
        
        if(status != null)
            query += " and settled="+status;
        
        if(bank != null)
            query += " and bank='"+bank+"'";
        
        Page<ReconTransaction> page = new Page<>();
        
        List<ReconTransaction> transactions ;
        
        if(pageSize > 0 )
            transactions = (List<ReconTransaction>)entityManager.createNativeQuery("select * from recon_transaction "+query, ReconTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<ReconTransaction>)entityManager.createNativeQuery("select * from recon_transaction "+query +"", ReconTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from recon_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    public Page<ReconTransaction> getTransaction(Date startDate, Date endDate, String column, String data, int pageSize, int pageNum, Boolean status, String bank){
        
        String query = " where "+column+" like '%"+data+"%'";
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        if(startDate != null){
            
            query += " and date(operationDate) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(operationDate) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(status != null){
            query += " and settled="+status;
        }
        
        if(bank != null)
            query += " and bank='"+bank+"'";
        
        Page<ReconTransaction> page = new Page<>();
        
        List<ReconTransaction> transactions ;
        
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select * from recon_transaction "+query, ReconTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<ReconTransaction>)entityManager.createNativeQuery("select * from recon_transaction "+query +"", ReconTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from recon_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    public Page<ReconTransaction> getTransaction(Date startDate, Date endDate, String column, double data, int pageSize, int pageNum, Boolean status, String bank){
        
        String query = " where "+column+"="+data;
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        if(startDate != null){
            
            query += " and date(operationDate) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(operationDate) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(status != null){
            query += " and settled="+status;
        }
        
        if(status != null){
            query += " and settled="+status;
        }
        
        if(bank != null)
            query += " and bank='"+bank+"'";
        
        Page<ReconTransaction> page = new Page<>();
        
        List<ReconTransaction> transactions ;
        
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select * from recon_transaction "+query, ReconTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<ReconTransaction>)entityManager.createNativeQuery("select * from recon_transaction "+query +"", ReconTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from recon_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    public Page<ReconTransaction> getTransaction(Date startDate, Date endDate, String column, Date data, int pageSize, int pageNum, Boolean status, String bank){
        
        String query = " where date("+column+")="+TransactionUtility.formatDateToString(data);
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        if(startDate != null){
            
            query += " and date(operationDate) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(operationDate) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(status != null){
            query += " and settled="+status;
        }
        
        if(status != null){
            query += " and settled="+status;
        }
        
        if(bank != null)
            query += " and bank='"+bank+"'";
        
        Page<ReconTransaction> page = new Page<>();
        
        List<ReconTransaction> transactions ;
        
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select * from recon_transaction "+query, ReconTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<ReconTransaction>)entityManager.createNativeQuery("select * from recon_transaction "+query +"", ReconTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from recon_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    public double[] getSum(Date startDate, Date endDate, String bank, String cardScheme, Boolean status){
        
        String query = "where id <> 0 ";
        
//        long skip = pageNum * pageSize;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(startDate != null){
            
            query += " and date(operationDate) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(operationDate) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(bank != null && !bank.isEmpty()){
            query += " and bank='"+bank+"'";
        }
        
        if(cardScheme != null && !cardScheme.isEmpty()){
            query += " and cardScheme='"+cardScheme+"'";
        }
        
        if(status != null){
            query += " and settled="+status;
        }
        
        // This returns both the source and destination amount
        Object[] decimals =  (Object[])entityManager.createNativeQuery("select sum(originclearanceAmount), sum(destinationclearanceamount) from recon_transaction "+query).getSingleResult();
        
        
        double[] dValues = new double[]{Double.parseDouble(decimals[0] == null ? "0" : decimals[0].toString()), 
                Double.parseDouble(decimals[1] == null ? "0" : decimals[1].toString())};
        
        return dValues;
    }
    
    public double[] getSum(Date startDate, Date endDate, String bank, String cardScheme, String currency, Boolean status){
        
        String query = "where id <> 0 ";
        
//        long skip = pageNum * pageSize;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(startDate != null){
            
            query += " and date(originTime) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(originTime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(bank != null && !bank.isEmpty()){
            query += " and bank='"+bank+"'";
        }
        
        if(cardScheme != null && !cardScheme.isEmpty()){
            query += " and cardScheme='"+cardScheme+"'";
        }
        
        if(currency != null){
            query += " and accountCurrency='"+currency+"'";
        }
        
        if(status != null){
            query += " and settled="+status;
        }
        
        // This returns both the source and destination amount
        Object[] decimals =  (Object[])entityManager.createNativeQuery("select sum(originclearanceAmount), sum(destinationclearanceamount) from recon_transaction "+query).getSingleResult();
        
        
        double[] dValues = new double[]{Double.parseDouble(decimals[0] == null ? "0" : decimals[0].toString()), 
                Double.parseDouble(decimals[1] == null ? "0" : decimals[1].toString())};
        
        return dValues;
    }
    
    public List getSumSettlement(Date startDate, Date endDate, String bank, String cardScheme, String currency, String merchant, Boolean status){
        
        String query = "where id <> 0 ";
        
//        long skip = pageNum * pageSize;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(startDate != null){
            
            query += " and date(originTime) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(originTime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(bank != null && !bank.isEmpty()){
            query += " and bank='"+bank+"'";
        }
        
        if(cardScheme != null && !cardScheme.isEmpty()){
            query += " and cardScheme='"+cardScheme+"'";
        }
        
        if(currency != null){
            query += " and accountCurrency='"+currency+"'";
        }
        
        if(currency != null){
            query += " and accountCurrency='"+currency+"'";
        }
        
        if(merchant != null){
            query += " and merchantTitle='"+merchant+"'";
        }
        
        
        List obj = entityManager.createNativeQuery("select sum(accountAmount) as amount, count(*) as volume, accountCurrency, cardscheme, settled  from recon_transaction "+query+" group by accountCurrency, cardscheme, settled  ").getResultList();
        
        return obj;
        // This returns both the source and destination amount
        //Object[] decimals =  (Object[])entityManager.createNativeQuery("select sum(originclearanceAmount), sum(destinationclearanceamount) from recon_transaction "+query).getSingleResult();
        
        
        
        
        //return dValues;
    }
    
    public long getTransactionCount(Date startDate, Date endDate, String bank, String cardScheme,  Boolean status){
        
        String query = "where id <> 0 ";
        
//        long skip = pageNum * pageSize;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(startDate != null){
            
            query += " date(operationDate) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(operationDate) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(bank != null && !bank.isEmpty()){
            query += " and bank='"+bank+"'";
        }
        
        if(cardScheme != null && !cardScheme.isEmpty()){
            query += " and cardScheme='"+cardScheme+"'";
        }
        
        if(status != null){
            query += " and settled="+status;
        }
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from recon_transaction "+query).getSingleResult().toString());
        
        return count;
    }
    
    public List getTransactionCount(Date startDate, Date endDate){
        
        String query = "where id <> 0 ";
        
//        long skip = pageNum * pageSize;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(startDate != null){
            
            query += " and date(operationDate) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(operationDate) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        List list = entityManager.createNativeQuery("select count(*) as count, operationDate from recon_transaction "+query).getResultList();
        
        return list;
    }
    
    public List getCurrencies(){
        
        List list = entityManager.createNativeQuery("select distinct(accountCurrency) from recon_transaction").getResultList();
        
        return list;
    }
    
    
    
    public List<String> getBatchs(){
        
        List merchant =  entityManager.createNativeQuery("select distinct(merchantTitle) from recon_transaction").getResultList();
        
        return merchant;
    }
    
    public Map<String, Object> getCountQuery(String query){
        
        try{
            List list = entityManager.createNativeQuery(query).getResultList();

            Map<String, Object> object = new HashMap<>();
            
            object.put("result", list);

            return object;
        }catch(Exception ex){
            if(ex != null)
                ex.printStackTrace();
        }
        
        return null;
    }
    
    
    private boolean exists(String param, String value){
        
        
        long count = ((BigInteger)(getEntityManager().createNativeQuery("select count(*) from recon_transaction where "+param+"='"+value+"'")
                .getSingleResult())).longValue();
        
        return count > 0L;
    }
    
    @TransactionTimeout(unit = TimeUnit.MINUTES, value = 60)
    public boolean create(List<ReconTransaction> data, Batch batch) throws DatabaseException{
        
        try{
            System.out.println(getEntityManager());
            
            
            List<ReconTransaction> txs = new ArrayList<>();
            
            data.forEach((rt) -> {
                boolean status = exists("rrn", rt.getRrn());
                if (status != true) {
                    txs.add(rt);
                }
            });
            
            int index = 0;
            for(ReconTransaction t : txs){
                t.setBatch(batch);
                
                getEntityManager().persist(t);
                index++;
                
                if(index % 50 == 0){
                    getEntityManager().flush();
                    getEntityManager().clear();
                }
            }
            
            getEntityManager().flush();
            getEntityManager().clear();
            
             // This ensure the id generated
            return true;
        }catch(Exception ex){
            
            if(ex != null)
                ex.printStackTrace();
            
            throw new DatabaseException(ex);
        }
    }
    
    public List<ReconTransaction> findByBatch(Batch batch) throws DatabaseException{
        
        if(batch == null)
            return new ArrayList<>();
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<ReconTransaction> query = builder.createQuery(ReconTransaction.class);

            // This used to map the entities to a class and extract the fields involved
            Root<ReconTransaction> rootQuery = query.from(ReconTransaction.class);

            ParameterExpression keyParam = builder.parameter(Batch.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("batch"), keyParam));

            List<ReconTransaction> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, batch)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
        
    }
    
    public Page<ReconTransaction> findByBatch(Batch batch, Boolean status, int pageNum, int pageSize) throws DatabaseException{
        
        Page<ReconTransaction> page = new Page<>();
        
        if(batch == null)
            return page;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<ReconTransaction> query = builder.createQuery(ReconTransaction.class);

            // This used to map the entities to a class and extract the fields involved
            Root<ReconTransaction> rootQuery = query.from(ReconTransaction.class);

            ParameterExpression keyParam = builder.parameter(Batch.class);
            ParameterExpression keyParam2 = builder.parameter(Boolean.class);
            
            Predicate predicate = builder.equal(rootQuery.get("batch"), keyParam);
            
            if(status != null)
                predicate = builder.and(predicate, builder.equal(rootQuery.get("settled"), keyParam2));
            // This is used to build the cq that will allow the keyParam
            
            query.select(rootQuery).where(predicate);

            javax.persistence.Query q = getEntityManager().createQuery(query)
                    .setParameter(keyParam, batch);
            
            if(status != null)
                q.setParameter(keyParam2, status);
            
            q.setFirstResult(pageNum);
            
            // This is nd
            if(pageSize > 0)
                q.setMaxResults(pageNum+pageSize);
                    
                   
            List<ReconTransaction> list =  q.getResultList();

            if(list.isEmpty())
                return page;

            if(status != null)
                page.setCount(countBatch(batch, status));
            else
                page.setCount(countBatch(batch));
            
            page.setContent(list);
            
            return page;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    public List getWorthAndVolume(Date startDate, Date endDate){
       
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String startD = dateFormat.format(startDate);
        String endD = dateFormat.format(endDate);
        
        String queryString = " where date(operationDate) >= date('"+startD+"') and date(operationDate) <= date('"+endD+"') ";
        
        List obj = entityManager.createNativeQuery("select sum(accountAmount) as amount, count(*) as volume, accountCurrency, cardscheme, externalResponseCode  from recon_transaction "+queryString+" group by accountCurrency, cardscheme  ").getResultList();
        
        return obj;
    }
    
    public List getWorthAndVolume(Date startDate, Date endDate, String bank, String currency, String merchant){
       
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String startD = dateFormat.format(startDate);
        String endD = dateFormat.format(endDate);
        
        String queryString = " where date(operationDate) >= date('"+startD+"') and date(operationDate) <= date('"+endD+"')  ";
        
        if(currency != null)
            queryString += " and accountCurrency='"+currency+"'";
        
        if(bank != null)
            queryString += " and bank='"+bank+"'";
        
        if(merchant != null)
            queryString += " and merchantTitle='"+merchant+"'";
        
        List obj = entityManager.createNativeQuery("select sum(accountAmount) as amount, count(*) as volume, accountCurrency, cardscheme, externalResponseCode  from recon_transaction "+queryString+" group by accountCurrency, cardscheme  ").getResultList();
        
        return obj;
    }
    
    public List getWorthSettlement(Date startDate, Date endDate, String bank, String currency, String merchant){
       
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String startD = dateFormat.format(startDate);
        String endD = dateFormat.format(endDate);
        
        String queryString = " where settled=1 and date(operationDate) >= date('"+startD+"') and date(operationDate) <= date('"+endD+"')  ";
        
        if(currency != null)
            queryString += " and accountCurrency='"+currency+"'";
        
        if(bank != null)
            queryString += " and bank='"+bank+"'";
        
        if(merchant != null)
            queryString += " and merchantTitle='"+merchant+"'";
        
        List obj = entityManager.createNativeQuery("select sum(accountAmount) as amount, count(*) as volume from recon_transaction "+queryString+" ").getResultList();
        
        return obj;
    }
    
    public List getSettlement(Date startDate, Date endDate, String bank, String currency, String merchant){
       
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String startD = dateFormat.format(startDate);
        String endD = dateFormat.format(endDate);
        
        String queryString = " where date(operationDate) >= date('"+startD+"') and date(operationDate) <= date('"+endD+"') ";
        
        if(currency != null)
            queryString += " and accountCurrency='"+currency+"'";
        
        if(bank != null)
            queryString += " and bank='"+bank+"'";
        
        if(merchant != null)
            queryString += " and merchantTitle='"+merchant+"'";
        
        List obj = entityManager.createNativeQuery("select sum(accountAmount) as amount, count(*) as volume, accountCurrency, cardscheme, settled  from recon_transaction "+queryString+" group by cardscheme,settled,accountCurrency ").getResultList();
        
        return obj;
    }
    
    public List getSettlement(Date startDate, Date endDate){
       
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String startD = dateFormat.format(startDate);
        String endD = dateFormat.format(endDate);
        
        String queryString = " where date(operationDate) >= date('"+startD+"') and date(operationDate) <= date('"+endD+"') ";
        
        List obj = entityManager.createNativeQuery("select sum(accountAmount) as amount, count(*) as volume, accountCurrency, cardscheme, settled  from recon_transaction "+queryString+" group by cardscheme,settled,accountCurrency ").getResultList();
        
        return obj;
    }
    
    public List get(Date startDate, Date endDate){
       
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String startD = dateFormat.format(startDate);
        String endD = dateFormat.format(endDate);
        
        String queryString = " where date(operationDate) >= date("+startD+") and date(operationDate) <= date("+endD+") ";
        
        List obj = entityManager.createNativeQuery("select sum(accountAmount) as amount, count(*) as volume, accountCurrency from recon_transaction "+queryString+" group by accountCurrency").getResultList();
        
        return obj;
    }
    
    public List<String> getMerchants(String bank){
        
        String query =  " where id <> 0 ";
        
        if(bank != null)
            query += " and bank = '"+bank+"'";
        
        List merchant =  entityManager.createNativeQuery("select distinct(merchantTitle) from recon_transaction "+query).getResultList();
        
        return merchant;
    }
    
    public List getSummary(Date startDate, Date endDate, String currency, String bank, String merchant){
        
        
        String query = "where id <> 0 and accountCurrency='"+currency+"' ";
        
//        long skip = pageNum * pageSize;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(startDate != null){
            
            query += " and date(originTime) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(originTime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(merchant != null){
            query += " and merchantTitle='"+merchant+"'";
        }
        
        if(bank != null)
            query += " and bank='"+bank+"'";
        
        List list = entityManager.createNativeQuery("select sum(accountAmount) as amount, count(*) as volume, accountCurrency, count(distinct(merchantTitle)) as merchant from recon_transaction "+query).getResultList();
        
        return list;
    }
    
    
     public List getTrend(Date startDate, Date endDate, String currency, String bank, String merchant){
        
        
        String query = "where id <> 0 and accountCurrency='"+currency+"' ";
        
//        long skip = pageNum * pageSize;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(startDate != null){
            
            query += " and date(originTime) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(originTime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(merchant != null)
            query += " and merchantTitle='"+merchant+"'";
        
        if(bank != null)
            query += " and bank='"+bank+"'";
        
        List list = entityManager.createNativeQuery("select sum(accountAmount) as amount, date(originTime) as date, count(*) as volume from recon_transaction "+query+" group by date(originTime)").getResultList();
        
        return list;
    }
    
    public long getCardCount(Date startDate, Date endDate, String currency, String bank){
        
        String query = "where id <> 0 and accountCurrency='"+currency+"' ";
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(startDate != null){
            
            query += " and date(originTime) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(originTime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(bank != null)
            query += " and bank='"+bank+"'";
        
        long value = Long.parseLong( entityManager.createNativeQuery("select count(distinct(maskedpan)) from recon_transaction "+query).getSingleResult().toString());
        
        return value;
    }
    
    public long countBatch(Batch batch){
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        javax.persistence.criteria.Root<ReconTransaction> rt = cq.from(ReconTransaction.class);
        ParameterExpression keyParam = cb.parameter(Batch.class);
        
        Predicate predicate = cb.equal(rt.get("batch"), keyParam);
        predicate = cb.or(predicate, cb.isNull(rt.get("batch")));
        
        cq.select(getEntityManager().getCriteriaBuilder().count(rt))
                .where(predicate);
        
        javax.persistence.Query q = getEntityManager().createQuery(cq)
                .setParameter(keyParam, batch);
        
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public long countBatch(Batch batch, boolean status){
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        javax.persistence.criteria.Root<ReconTransaction> rt = cq.from(ReconTransaction.class);
        ParameterExpression keyParam = cb.parameter(Batch.class);
        ParameterExpression keyParam2 = cb.parameter(Boolean.class);
        
        Predicate predicate = cb.equal(rt.get("batch"), keyParam);
        predicate = cb.and(predicate, cb.equal(rt.get("settled"), keyParam2));
        
        predicate = cb.or(predicate, cb.isNull(rt.get("batch")));
        
        cq.select(getEntityManager().getCriteriaBuilder().count(rt))
                .where(predicate);
        
        javax.persistence.Query q = getEntityManager().createQuery(cq)
                .setParameter(keyParam, batch)
                .setParameter(keyParam2, status);
        
        return ((Long) q.getSingleResult()).intValue();
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean createNew(List<ReconTransaction> data) throws DatabaseException{
        
        try{
//            System.out.println(getEntityManager());
            
//            getEntityManager().setProperty("hibernate.jdbc.batch_size", BATCH_SIZE);
            
            int index = 0;
            for(ReconTransaction t : data){
                
                try{
                    getEntityManager().persist(t);
                }catch(Exception ex){
                }
                
                index++;
                
                if(index % 50 == 0){
                    getEntityManager().flush();
                    getEntityManager().clear();
                }
            }
            
            getEntityManager().flush();
            getEntityManager().clear();
            
             // This ensure the id generated
            return true;
        }catch(Exception ex){
            //context.setRollbackOnly();
            throw new DatabaseException(ex);
        }
    } 
    
    
    
//    public ReconTransaction createNew(ReconTransaction transaction, int count){
//        
//        
//    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public ReconTransaction create(ReconTransaction data) throws DatabaseException {
        return super.create(data); //To change body of generated methods, choose Tools | Templates.
    }
    
}

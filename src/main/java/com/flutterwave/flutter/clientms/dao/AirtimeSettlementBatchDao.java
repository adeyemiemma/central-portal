/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.AirtimeSettlement;
import com.flutterwave.flutter.clientms.model.AirtimeSettlementBatch;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class AirtimeSettlementBatchDao extends HibernateDao<AirtimeSettlementBatch> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public AirtimeSettlementBatchDao() {
        super(AirtimeSettlementBatch.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
}

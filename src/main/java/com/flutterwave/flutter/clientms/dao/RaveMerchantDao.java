/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.util.Page;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class RaveMerchantDao extends HibernateDao<RaveMerchant> {

    @PersistenceContext(unitName = Global.RAVE_PERSISTENT_NAME)
    private EntityManager entityManager;

    public RaveMerchantDao() {
        super(RaveMerchant.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<RaveMerchant> search(int pageNum, int pageSize, 
            String search, Boolean deleted, Boolean live, String country, Date startDate, Date endDate){
    
        getEntityManager().getEntityManagerFactory().getCache().evictAll();
            
        Page<RaveMerchant> page = new Page<>();

        CriteriaBuilder cb =getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<RaveMerchant> root = cq.from(entityClass);
        
        ParameterExpression keyParamId = cb.parameter(Long.class);
        ParameterExpression keyParamName = cb.parameter(String.class);
        ParameterExpression keyParamLive = cb.parameter(Boolean.class);
        ParameterExpression keyParamDeleted = cb.parameter(Boolean.class);
        ParameterExpression keyParamCountry = cb.parameter(String.class);
        
        ParameterExpression keyParamStartDate = cb.parameter(Date.class);
        ParameterExpression keyParamEndDate = cb.parameter(Date.class);
        
        Predicate predicate = cb.gt(root.get("id"), keyParamId);
        
//        RaveMerchant.class.newInstance().isLive()
        if(search != null && !"".equals(search))
            predicate = cb.and(predicate, cb.like(root.get("businessName"), keyParamName));
        
        if(live != null)
            predicate = cb.and(predicate, cb.equal(root.get("live"), keyParamLive));
        
        if(deleted != null)
            predicate = cb.and(predicate, cb.isNotNull(root.get("deletedAt")));
        
        if(country != null && !"".equals(country))
            predicate = cb.and(predicate, cb.equal(root.get("country"), keyParamCountry));
        
        if(startDate != null){
            
            Predicate p = cb.between(root.get("createdAt"), keyParamStartDate, keyParamEndDate);
            
            predicate = cb.and(predicate,p);
        }
        
        cq.select(root).where(predicate).orderBy(cb.desc(root.get("id")));

        javax.persistence.Query query =  getEntityManager().createQuery(cq);
        
        query.setParameter(keyParamId, 0L);
        
        cq.select(getEntityManager().getCriteriaBuilder().count(root)).where(predicate);
        javax.persistence.Query countQuery = getEntityManager().createQuery(cq);
        
        countQuery.setParameter(keyParamId, 0L);
        
        
        if(search != null && !"".equals(search)){
            query.setParameter(keyParamName, "%"+search+"%");
            countQuery.setParameter(keyParamName, "%"+search+"%");
        }
        
        if(live != null){
            query.setParameter(keyParamLive, live);
            countQuery.setParameter(keyParamLive, live);
        }
        
        if(deleted != null){
            query.setParameter(keyParamDeleted, deleted);
            countQuery.setParameter(keyParamDeleted, deleted);
        }
        
        if(country != null && !"".equals(country)){
            query.setParameter(keyParamCountry, country);
            countQuery.setParameter(keyParamCountry, country);
        }
        
        if(startDate != null){
            query.setParameter(keyParamStartDate, startDate);
            query.setParameter(keyParamEndDate, endDate);
            
            countQuery.setParameter(keyParamStartDate, startDate);
            countQuery.setParameter(keyParamEndDate, endDate);
        }
        
        
        query.setFirstResult(pageNum);

        // This is nd
        if(pageSize > 0)
            query.setMaxResults(pageNum+pageSize);

        List<RaveMerchant> content = query.getResultList();

        long count = ((Long) countQuery.getSingleResult()).intValue();

        page.setContent(content);
        page.setCount(count);

        query.setHint("org.hibernate.cacheMode", "IGNORE");
        
        page.setContent(content);
        return page;
    }
}

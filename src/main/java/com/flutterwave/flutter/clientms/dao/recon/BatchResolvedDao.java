/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.recon;


import com.flutterwave.flutter.clientms.model.recon.BatchResolved;
import com.flutterwave.flutter.clientms.util.Global;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author adeyemi
 */
@Stateless
public class BatchResolvedDao extends com.flutterwave.flutter.core.dao.HibernateDao<BatchResolved> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public BatchResolvedDao() {
        super(BatchResolved.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        
        return entityManager;
    }
    
}

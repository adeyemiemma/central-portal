/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.MerchantComplianceLog;
import com.flutterwave.flutter.clientms.model.MerchantComplianceLog;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.util.Page;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class MerchantCompliantLogDao extends BaseHibernateDao<MerchantComplianceLog> {
    
    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;
    
    public MerchantCompliantLogDao() {
        super(MerchantComplianceLog.class);
    }

    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<MerchantComplianceLog> findMerchant(int pageNum, int pageSize, String product, String merchantId, String searchString, Boolean approved){
        
        Page<MerchantComplianceLog> page = new Page<>();
        
        String query = " where id <> 0";
        String joinQuery = "";
        
        if(merchantId != null)
            query += " and merchantId='"+merchantId+"'";
        
        if(product != null){
            query += " and ( products= '"+product+"'";
        
            if(product.equalsIgnoreCase("rave")){
                query += " or products= 'ravepay'";
            }

            query += ")";
        }
        
        if(searchString != null)
            query += " and ( registeredName like '%"+searchString+"%' or registeredNumber like '%"+searchString+"%' )";
        
        if(approved != null)
            if(approved == true)
                query += " and approvedOn is NOT NULL";
            else
                query += " and approvedOn is NULL";
        
        List<MerchantComplianceLog> complianceList;
        
        if(pageSize > 0 )
            complianceList = entityManager.createNativeQuery("select merchant_compliance_log.* from merchant_compliance_log "+joinQuery+" "+query, MerchantComplianceLog.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            complianceList = entityManager.createNativeQuery("select merchant_compliance_log.* from merchant_compliance_log "+joinQuery+" "+query +"", MerchantComplianceLog.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from merchant_compliance_log "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(complianceList);
        page.setCount(count);
        
        return page;
        
    }
    
    public Page<MerchantComplianceLog> findMerchant(int pageNum, int pageSize, Date startDate, Date endDate, String product, String merchantId, 
            String searchString, Boolean approved, String country, boolean rejected){
        
        Page<MerchantComplianceLog> page = new Page<>();
        
        String query = " where id <> 0";
        String joinQuery = "";
        
        if(merchantId != null)
            query += " and merchantId='"+merchantId+"'";
        
        if(product != null){
            query += " and ( products= '"+product+"'";
        
            if(product.equalsIgnoreCase("rave")){
                query += " or products= 'ravepay'";
            }

            query += ")";
        }
        
        if(startDate != null){
            query += " and createdOn >= '"+Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss")+"'";
        }
        
        if(endDate != null){
               query += " and createdOn <= '"+Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss")+"'";
        }
        
        if(searchString != null)
            query += " and ( merchantId like '%"+searchString+"%' or registeredName like '%"+searchString+"%' or registeredNumber like '%"+searchString+"%' )";
        
        if(approved != null)
            if(approved == true){
                query += " and (approvedOn is NOT NULL";
                
                if(rejected == true)
                   query += " and `status`='"+Status.REJECTED+"' )"; 
                else
                    query += " and (`status`='"+Status.APPROVED+"' or `status`='"+Status.UNKNOWN+"'))"; 
            }
            else
                query += " and approvedOn is NULL";
        
        if(country != null && !country.equalsIgnoreCase("")){
            query += " and country='"+country+"'";
        }
        
        List<MerchantComplianceLog> complianceList;
        
        if(pageSize > 0 )
            complianceList = entityManager.createNativeQuery("select merchant_compliance_log.* from merchant_compliance_log "+joinQuery+" "+query +"  order by id desc", entityClass)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            complianceList = entityManager.createNativeQuery("select merchant_compliance_log.* from merchant_compliance_log "+joinQuery+" "+query +" order by id desc", MerchantComplianceLog.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from merchant_compliance_log "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(complianceList);
        page.setCount(count);
        
        return page;
        
    }
    

    public MerchantComplianceLog findByMerchantId(String merchantId, String product){
     
        String query = " where id <> 0";
        
        if(merchantId != null)
            query += " and merchantId='"+merchantId+"'";
        
        if(product != null)
            query += " and products= '"+product+"'";
        
        
        List<MerchantComplianceLog> list = getEntityManager().createNativeQuery("select * from merchant_compliance_log "+query, MerchantComplianceLog.class).getResultList();
        
        if(list != null && !list.isEmpty())
            return list.get(0);
        
        return null;
    }
}

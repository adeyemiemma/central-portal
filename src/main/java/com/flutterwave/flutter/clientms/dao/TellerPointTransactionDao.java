/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.TellerPointTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class TellerPointTransactionDao extends HibernateDao<TellerPointTransaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public TellerPointTransactionDao() {
        super(TellerPointTransaction.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<TellerPointTransaction> findByTerminalAndReference(String terminalId, String merchantId, String reference){
        
        String query = " where id <> 0 ";
        
        if(terminalId == null || merchantId == null || reference == null)
            return null;
        
        query += " and terminalId = '"+terminalId+"'";
        
        query += " and posId = '"+merchantId+"'";
        
        query += " and tellerpointRef='"+reference+"'";
        
        List<TellerPointTransaction> pointTransactions = entityManager.createNativeQuery("select * from teller_point_transaction "+query, entityClass)
                .getResultList();
        
        return pointTransactions;        
    }
}

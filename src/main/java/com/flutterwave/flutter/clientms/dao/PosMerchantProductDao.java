/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.PosMerchantProduct;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class PosMerchantProductDao extends HibernateDao<PosMerchantProduct> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME_NEW)
    private EntityManager entityManager;

    public PosMerchantProductDao() {
        super(PosMerchantProduct.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public PosMerchantProduct findByMerchantAndProduct(String merchantCode, String productCode){
        
//        if(Utility.emptyToNull(posOrderId) != null)
//            query += " and pos = '"+posOrderId+"'";
        
        List<PosMerchantProduct> products = entityManager.createNamedQuery("PosMerchantProduct.findByMerchantCodeAndProductCode",PosMerchantProduct.class)
                .setParameter("productCode", productCode)
                .setParameter("merchantCode", merchantCode)
                .getResultList();
        
        if(products == null || products.isEmpty())
            return null;
        
        return products.get(0);
    }
    
}


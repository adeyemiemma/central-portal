///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.flutterwave.flutter.clientms.dao.maker;
//
//import com.flutterwave.flutter.clientms.dao.*;
//import com.flutterwave.flutter.clientms.model.ConfigurationModel;
//import com.flutterwave.flutter.clientms.model.maker.ConfigurationModelM;
//import com.flutterwave.flutter.clientms.util.Global;
//import javax.ejb.Stateless;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//
///**
// *
// * @author adeyemi
// */
//@Stateless
//public class ConfigurationMDao extends com.flutterwave.flutter.core.dao.HibernateDao<ConfigurationModelM> {
//
//    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
//    private EntityManager entityManager;
//
//    public ConfigurationMDao() {
//        super(ConfigurationModelM.class);
//    }
//    
//    @Override
//    public EntityManager getEntityManager() {
//        return entityManager;
//    }  
//    
//}

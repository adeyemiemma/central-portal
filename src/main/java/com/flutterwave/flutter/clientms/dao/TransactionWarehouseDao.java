/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.Settlement;
import com.flutterwave.flutter.clientms.model.SettlementReportModel;
import com.flutterwave.flutter.clientms.model.TransactionWarehouse;
import com.flutterwave.flutter.clientms.model.TransactionListModel;
import com.flutterwave.flutter.clientms.model.TransactionWarehouse;
import com.flutterwave.flutter.clientms.model.TransactionViewModel;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.FetchRRNModel;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class TransactionWarehouseDao extends HibernateDao<TransactionWarehouse> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME_NEW)
    private EntityManager entityManager;

    public TransactionWarehouseDao() {
        super(TransactionWarehouse.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    public List<TransactionWarehouse> findByKeyLike(String key, String value) throws DatabaseException {

        try {
            // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<TransactionWarehouse> query = builder.createQuery(entityClass);

            // This used to map the entities to a class and extract the fields involved
            Root<TransactionWarehouse> rootQuery = query.from(entityClass);

            ParameterExpression keyParam = builder.parameter(Long.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.like(rootQuery.get(key), keyParam)).orderBy(builder.desc(rootQuery.get("id")));

            List<TransactionWarehouse> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, value)
                    .getResultList();

            if (list.isEmpty()) {
                return null;
            }

            return list;
        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }
    
    public List<TransactionViewModel> findMiniTransactionNew(String key, String value) throws DatabaseException {

        try {
            
            String query = " where "+key+" like '"+value+"'";
            
            List<TransactionViewModel> list = entityManager.createNativeQuery("select id, rrn, provider, merchantId,transactionCurrency, transactionCountry, amount, merchantTxnReference, flwTxnReference, transactionType, cardCountry, cardMask, datetime, responseCode,responseMessage, createdOn, "
                + "cardScheme,authenticationModel,transactionNarration from transaction_warehouse as transaction  " + query + " order by datetime desc ", TransactionViewModel.class)
                .getResultList();
            
            if(list == null || list.isEmpty())
                return null;
            
            return list;
            
        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }

    public TransactionWarehouse FindRefundTransactionNew(String transactionReference) {
        String query = " where transaction.id <> 0 and responsecode='00' and transactionType = 'CARD_REFUND'";

        List<TransactionWarehouse> list = entityManager.createNativeQuery("select * from transaction_warehouse as transaction  " + query + "", TransactionWarehouse.class)
                .getResultList();

        return list == null ? null : list.get(0);
    }
    
    public TransactionWarehouse findTransactionNewMinimalLike(String transactionReference) {
        String query = " where transaction.flwTxnReference like '%"+transactionReference+"%'";

        List<TransactionWarehouse> list = entityManager.createNativeQuery("select   from transaction_warehouse as transaction  " + query + "", TransactionWarehouse.class)
                .getResultList();

        return list == null ? null : list.get(0);
    }

    public List<TransactionWarehouse> getUnsettledPendingTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and settlementState <> 2 and (settled=0 or settled is NULL) and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<TransactionWarehouse> list = entityManager.createNativeQuery("select * from transaction_warehouse as transaction  " + query + "", TransactionWarehouse.class)
                .getResultList();

        return list;
    }

    public List<FetchRRNModel> getRRNModels(String keyName, String rrns){
    
        if(keyName == null || rrns == null)
            return null;
        
        String query = " where "+ ("flwRef".equalsIgnoreCase(keyName) ? "transaction.flwTxnReference" : "transaction.rrn")+" in ("+rrns+")";
    
        List<FetchRRNModel> model = entityManager.createNativeQuery("select id, amount,merchantTxnReference, flwTxnReference as flutterReference, rrn, datetime as transactionDate,"
                + " transactionCurrency as currency,merchantId, merchantId as merchantName, responseCode, responseMessage, vpcmerchant from transaction_warehouse as transaction "+query, FetchRRNModel.class).getResultList();
        
        if(model == null || model.isEmpty())
            return null;
        
        return model;
    }
    
    public List<TransactionWarehouse> getSettleableTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<TransactionWarehouse> list = entityManager.createNativeQuery("select * from transaction_warehouse as transaction  " + query + "", TransactionWarehouse.class)
                .getResultList();

        return list;
    }
    
    public List<TransactionWarehouse> getAllSettleableTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND' OR transactionType='ACCOUNT_PURCHASE') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<TransactionWarehouse> list = entityManager.createNativeQuery("select * from transaction_warehouse as transaction  " + query + "", TransactionWarehouse.class)
                .getResultList();

        return list;
    }

    public List<TransactionWarehouse> getUnsettledTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and (settled=0 or settled is NULL) and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<TransactionWarehouse> list = entityManager.createNativeQuery("select * from transaction_warehouse as transaction  " + query + "", TransactionWarehouse.class)
                .getResultList();

        return list;
    }

    public List<TransactionWarehouse> getUnsettledTransanctionAccount(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and "
                + "(settled=0 or settled is NULL) and responsecode='00' and (transactionType='ACCOUNT_PURCHASE') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<TransactionWarehouse> list = entityManager.createNativeQuery("select * from transaction_warehouse as transaction  " + query + "", TransactionWarehouse.class)
                .getResultList();

        return list;
    }

    public List<SettlementReportModel> getUnsettledSettlement(Date startDate, Date endDate, Provider provider, String currency) {

        String query = " where transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and (settled=0 or settled is NULL) and responsecode='00' "
                + "and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider_id =" + provider.getId() + "";
        }

        List<SettlementReportModel> list = entityManager.createNativeQuery("select * from transaction_warehouse as transaction  " + query + " group by merchantId", SettlementReportModel.class)
                .getResultList();

        return list;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<TransactionWarehouse> getTransactionNews(int pageNum, int pageSize, Date startDate, Date endDate, String reference,
            String currency, String provider, String merchantId, String category, String statusCode, String type, String cardScheme, String disbursementCode, Boolean isRefund) {

        String query = " where transaction.id <> 0 ";

        if (isRefund != null) {
            if (!isRefund) {
                query += " and transactionType <> 'CARD_REFUND'";
            } else {
                query += " and transactionType = 'CARD_REFUND'";
            }
        }
//        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateFormat.format(endDate) + "')";
        }

        if (cardScheme != null) {
            query += " and cardscheme = '" + cardScheme + "'";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (merchantId != null) {
            query += " and transaction.merchantId='" + merchantId + "'";
        }

        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (statusCode != null) {
            query += " and transaction.responseCode='" + statusCode + "'";
        }

        if (disbursementCode != null) {
            query += " and transaction.disburseResponseCode='" + disbursementCode + "'";
        }

//        if(category != null || type != null ){
//            query += " (";
//        }
//        
        if (category != null) {
            query += " and transaction.transactionType like '" + category + "%'";
        }

        if (type != null) {
            query += " and transaction.transactionType='" + type + "'";
        }

//        if(category != null || type != null ){
//            query += " )";
//        }
//        
        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (reference != null) {
            query += " and ( transaction.flwTxnReference = '" + reference + "' or transaction.merchantTxnReference = '" + reference + "' or transaction.rrn = '" + reference + "' or transaction.cardMask = '" + reference + "'  )";
        }

        List<TransactionWarehouse> list = entityManager.createNativeQuery("select id, rrn, merchantId,transactionCurrency, transactionCountry, amount, merchantTxnReference, flwTxnReference, transactionType, cardCountry, cardMask, datetime, responseCode,responseMessage, transactionData, processorReference, createdOn, "
                + "cardScheme, disburseResponseCode, disburseResponseMessage from transaction_warehouse as transaction  " + query + " order by datetime desc ", TransactionWarehouse.class)
                .setFirstResult(pageNum)
                .setMaxResults(pageSize)
                .getResultList();

        Object countResult = entityManager.createNativeQuery("select count(1) as count from transaction_warehouse as transaction  " + query + "").getSingleResult();

        long count = Long.parseLong(countResult + "");

        Page<TransactionWarehouse> transactions = new Page(count, list);

        return transactions;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<TransactionListModel> getTransactionNewsList(int pageNum, int pageSize, Date startDate, Date endDate, String searchKey, String reference,
            String currency, String provider, String merchantId, String category, String statusCode, String type, String cardScheme, String disbursementCode, boolean noCount, Boolean isRefund) {

        String query = " where transaction.id <> 0 ";

        if (isRefund != null) {
            if (!isRefund) {
                query += " and transactionType <> 'CARD_REFUND'";
            } else {
                query += " and transactionType = 'CARD_REFUND'";
            }
        }
//        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and transaction.datetime >= '" + dateFormat.format(startDate) + "' and transaction.datetime <= '" + dateFormat.format(endDate) + "'";
        }

        if (cardScheme != null) {
            query += " and cardscheme = '" + cardScheme + "'";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (merchantId != null) {
            query += " and transaction.merchantId='" + merchantId + "'";
        }

        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (statusCode != null) {
            query += " and transaction.responseCode='" + statusCode + "'";
        }

        if (disbursementCode != null) {
            query += " and transaction.disburseResponseCode='" + disbursementCode + "'";
        }

//        if(category != null || type != null ){
//            query += " (";
//        }
//        
        if (category != null) {
            query += " and transaction.transactionType like '" + category + "%'";
        }

        if (type != null) {
            query += " and transaction.transactionType='" + type + "'";
        }

//        if(category != null || type != null ){
//            query += " )";
//        }
//        
        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (Utility.emptyToNull(reference) != null && Utility.emptyToNull(searchKey) != null) {
            
            query += " and transaction."+searchKey+" like '%"+reference+"%' ";
            //query += " and ( transaction.flwTxnReference = '" + reference + "' or transaction.merchantTxnReference = '" + reference + "' or transaction.rrn = '" + reference + "' or transaction.cardMask = '" + reference + "'  )";
        }

        List<TransactionListModel> list = entityManager.createNativeQuery("select id, rrn, provider, merchantId,transactionCurrency, transactionCountry, amount, merchantTxnReference, flwTxnReference, transactionType, cardCountry, cardMask, datetime, responseCode,responseMessage, transactionData, processorReference, createdOn, "
                + "cardScheme, disburseResponseCode, disburseResponseMessage, disburseRetryCount,paymentId from transaction_warehouse as transaction  " + query + " order by datetime desc ", TransactionListModel.class)
                .setFirstResult(pageNum)
                .setHint("org.hibernate.cacheable", true)
                .setMaxResults(pageSize)
                .getResultList();

        long count = 0;
        
        if(noCount == false){
            Object countResult = entityManager.createNativeQuery("select count(1) from transaction_warehouse as transaction  " + query + "").getSingleResult();

            count = Long.parseLong(countResult + "");
        }

        Page<TransactionListModel> transactions = new Page(count, list);

        return transactions;
    }

    public Map<String, Object> getTransactionNewSummary(Date startDate, Date endDate, String reference,
            String currency, String provider, String merchantId, Boolean isRefund) {

        String query = " where transaction.id <> 0 ";

        if (isRefund != null) {
            if (!isRefund) {
                query += " and transactionType <> 'CARD_REFUND'";
            } else {
                query += " and transactionType = 'CARD_REFUND'";
            }
        }
//        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (merchantId != null) {
            query += " and transaction.merchantId='" + merchantId + "'";
        }

        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (reference != null) {
            query += " and (transaction.flwTxnReference = '" + reference + "' or transaction.merchantTxnReference = '" + reference + "' )";
        }

        Object result = entityManager.createNativeQuery("select sum(amount) from transaction_warehouse as transaction  " + query + "").getSingleResult();

        Object countResult = entityManager.createNativeQuery("select count(distinct(merchantId)) as count from transaction_warehouse as transaction  " + query + " ").getSingleResult();

        Map<String, Object> map = new HashMap<>();

        map.put("amount", result);
        map.put("merchant", countResult);

        return map;
    }

    public List<TransactionWarehouse> findBySettlement(Settlement settlement)
            throws DatabaseException {

        if (settlement == null) {
            return null;
        }

        try {
            // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<TransactionWarehouse> query = builder.createQuery(TransactionWarehouse.class);

            // This used to map the entities to a class and extract the fields involved
            Root<TransactionWarehouse> rootQuery = query.from(TransactionWarehouse.class);

            ParameterExpression keyParam = builder.parameter(Settlement.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("settlement"), keyParam));

            List<TransactionWarehouse> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, settlement)
                    .getResultList();

            if (list.isEmpty()) {
                return null;
            }

            return list;
        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }

    public List getSummaryWithDate(Date startDate, Date endDate, String currency, String provider) {

        String query = " where transaction.id <> 0 and (settled=0 or settled = NULL) and responsecode='00' ";
        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query += " and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('" + dateFormat.format(startDate) + "') and date(transaction.datetime) <= date('" + dateFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider + "'";
        }

        List list = entityManager.createNativeQuery("select sum(amount), 0 ,count(1), date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')), responseCode, responseMessage from transaction_warehouse as transaction  " + joinQuery + " " + query + " group by date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')), responseCode")
                .getResultList();

        return list;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public boolean exists(String param, String value) {

        List list = getEntityManager().createNativeQuery("select * from transaction_warehouse as transaction  where " + param + "='" + value + "' limit 1").getResultList();

        if (list == null) {
            return false;
        }

        return list.size() > 0;
    }

    public List getSummaryWithDate(Date startDate, Date endDate) {

        String query = " where transaction.id <> 0 and responsecode='00' ";
        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query += " and date(transaction.datetime) >= date('" + dateFormat.format(startDate) + "') and date(transaction.datetime) <= date('" + dateFormat.format(endDate) + "')";
        }

        List list = entityManager.createNativeQuery("select sum(amount), count(1), transactionCurrency,  merchantId, cardCountry from transaction_warehouse as transaction  " + joinQuery + " " + query + " group by date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')), transactionCurrency, merchantId")
                .getResultList();

        return list;
    }

    public List getTransactionNewSummarybyMonth(Date startDate, Date endDate) {

        String query = " where transaction.id <> 0 and responsecode='00' ";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query += " and date(transaction.datetime) >= date('" + dateFormat.format(startDate) + "') and date(transaction.datetime) <= date('" + dateFormat.format(endDate) + "')";
        }

        List list = entityManager.createNativeQuery("select sum(amount), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d') from transaction_warehouse as transaction  " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency ")
                .getResultList();

        return list;
    }

    public Map<String, String> getResponse(String category) {

        String whereClause = "";

        if (category != null && !"".equals(category)) {
            whereClause += " where t.transactionType like ?1";
        }

        Query query = entityManager.createQuery("SELECT distinct(t.responseCode), t.responseMessage FROM TransactionWarehouse as t " + whereClause + " group by t.responseCode");
        
        List list;
        
        if (category != null && !"".equals(category)) {
            query.setParameter(1, category+"%");
        }

        list = query.setHint("org.hibernate.cacheable", true)
                .getResultList();
        
        final Map<String, String> responses = new TreeMap<>();

        list.stream().forEachOrdered((data) -> {

            Object[] x = (Object[]) data;

            responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });

        return responses;
    }

    public Map<String, String> getDisburseResponse() {

        List list = entityManager.createNativeQuery("select disburseResponseCode, disburseResponseMessage from transaction_warehouse as transaction  group by disburseResponseCode").getResultList();

        final Map<String, String> responses = new TreeMap<>();

        list.stream().forEachOrdered((data) -> {

            Object[] x = (Object[]) data;

            responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });

        return responses;
    }

    public List<String> getTransactionNewType(String category) {

        List list = entityManager.createNativeQuery("select transactionType from transaction_warehouse as transaction  group by transactionType").getResultList();

        final List<String> responses = new ArrayList<>();

        list.stream().filter(x -> category == null ? x != null : x.toString().toLowerCase().startsWith(category))
                .forEachOrdered((data) -> {

                    responses.add(String.valueOf(data));
                });

        return responses;
    }

    public List getTransactionNewByMonth(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date startDate = calendar.getTime();

        calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date endDate = calendar.getTime();

        String query = " where transaction.id <> 0 and responsecode='00' ";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query += " and date(transaction.datetime) >= date('" + dateFormat.format(startDate) + "') and date(transaction.datetime) <= date('" + dateFormat.format(endDate) + "')";
        }

        List list = entityManager.createNativeQuery("select sum(amount), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d') from transaction_warehouse as transaction  " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency; ")
                .getResultList();

        return list;
    }

    public List getReport(Date startDate, Date endDate, boolean successful, boolean card) {

        String format = "yyyy-MM-dd";

        String startDateString = Utility.formatDate(startDate, format);
        String endDateString = Utility.formatDate(endDate, format);

        String query = " where transaction.id <> 0 and  " + (successful == true ? " responsecode = '00'" : "responsecode <> '00' ");

        if (card == true) {
            query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE')";
        } else {
            query += " and transactionType = 'ACCOUNT_PURCHASE'";
        }

        query += " and date(transaction.datetime) >= date('" + startDateString + "') and date(transaction.datetime) <= date('" + endDateString + "')";

        List result = entityManager.createNativeQuery("select sum(amount), count(1), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d') from transaction_warehouse as transaction  " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency ")
                .getResultList();

        return result;
    }

    public List getFailureReason(Date startDate, Date endDate, boolean card) {

        String format = "yyyy-MM-dd";

        String startDateString = Utility.formatDate(startDate, format);
        String endDateString = Utility.formatDate(endDate, format);

        String query = " where transaction.id <> 0 and responsecode <> '00'";

        if (card == true) {
            query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE')";
        } else {
            query += " and transactionType = 'ACCOUNT_PURCHASE'";
        }

        query += " and date(transaction.datetime) >= date('" + startDateString + "') and date(transaction.datetime) <= date('" + endDateString + "')";

        List result = entityManager.createNativeQuery("select sum(amount), count(1), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d'), responseMessage, cardScheme from transaction_warehouse as transaction  " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency,responsecode, cardScheme ")
                .getResultList();

        return result;
    }

    public List getSuccessAnalysis(Date startDate, Date endDate, boolean card) {

        String format = "yyyy-MM-dd";

        String startDateString = Utility.formatDate(startDate, format);
        String endDateString = Utility.formatDate(endDate, format);

        String query = " where transaction.id <> 0 and responsecode = '00'";

        if (card == true) {
            query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE')";
        } else {
            query += " and transactionType = 'ACCOUNT_PURCHASE'";
        }

        query += " and date(transaction.datetime) >= date('" + startDateString + "') and date(transaction.datetime) <= date('" + endDateString + "')";

        List result = entityManager.createNativeQuery("select sum(amount), count(1), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d'), cardScheme from transaction_warehouse as transaction  " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency, cardScheme ")
                .getResultList();

        return result;
    }

    public List<String> getDistinctMerchant(Date startDate, Date endDate) {

        String format = "yyyy-MM-dd";

        String startDateString = Utility.formatDate(startDate, format);
        String endDateString = Utility.formatDate(endDate, format);

        String query = " where transaction.id <> 0 and responsecode = '00'";

        query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE' or transactionType = 'ACCOUNT_PURCHASE')";

        query += " and date(transaction.datetime) >= date('" + startDateString + "') and date(transaction.datetime) <= date('" + endDateString + "')";

        List result = entityManager.createNativeQuery("select distinct(merchantId) from transaction_warehouse as transaction  " + query + "")
                .getResultList();

        return result;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<TransactionListModel> getTransactionsList(int pageNum, int pageSize, Date startDate, Date endDate, String searchKey, String reference,
            String currency, String provider, String merchantId, String category, String statusCode, String type, String cardScheme, String disbursementCode, boolean noCount, Boolean isRefund) {

        String query = " where transaction.id <> 0 ";

        if (isRefund != null) {
            if (!isRefund) {
                query += " and transactionType <> 'CARD_REFUND'";
            } else {
                query += " and transactionType = 'CARD_REFUND'";
            }
        }
//        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and transaction.datetime >= '" + dateFormat.format(startDate) + "' and transaction.datetime <= '" + dateFormat.format(endDate) + "'";
        }

        if (cardScheme != null) {
            query += " and cardscheme = '" + cardScheme + "'";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (merchantId != null) {
            query += " and transaction.merchantId='" + merchantId + "'";
        }

        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (statusCode != null) {
            query += " and transaction.responseCode='" + statusCode + "'";
        }

        if (disbursementCode != null) {
            query += " and transaction.disburseResponseCode='" + disbursementCode + "'";
        }

//        if(category != null || type != null ){
//            query += " (";
//        }
//        
        if (category != null) {
            query += " and transaction.transactionType like '" + category + "%'";
        }

        if (type != null) {
            query += " and transaction.transactionType='" + type + "'";
        }

//        if(category != null || type != null ){
//            query += " )";
//        }
//        
        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (Utility.emptyToNull(reference) != null && Utility.emptyToNull(searchKey) != null) {
            
            query += " and transaction."+searchKey+" like '"+reference+"%' ";
            //query += " and ( transaction.flwTxnReference = '" + reference + "' or transaction.merchantTxnReference = '" + reference + "' or transaction.rrn = '" + reference + "' or transaction.cardMask = '" + reference + "'  )";
        }

        List<TransactionListModel> list = entityManager.createNativeQuery("select id, rrn, provider, merchantId,transactionCurrency, transactionCountry, amount, merchantTxnReference, flwTxnReference, transactionType, cardCountry, cardMask, datetime, responseCode,responseMessage, transactionData, processorReference, createdOn, "
                + "cardScheme, disburseResponseCode, disburseResponseMessage, disburseRetryCount, merchantName, paymentId from transaction_warehouse as transaction " + query + " order by datetime desc ", TransactionListModel.class)
                .setFirstResult(pageNum)
                .setHint("org.hibernate.cacheable", true)
                .setMaxResults(pageSize)
                .getResultList();

        long count = 0;
        
        if(noCount == false){
            Object countResult = entityManager.createNativeQuery("select count(1) from transaction_warehouse as transaction " + query + "").getSingleResult();

            count = Long.parseLong(countResult + "");
        }

        Page<TransactionListModel> transactions = new Page(count, list);

        return transactions;
    }
    
    public FetchRRNModel getRRNModel(String rrn){
    
        if(rrn == null)
            return null;
        
        String query = " where transaction.rrn = '"+rrn+"'";
    
        List<FetchRRNModel> model = entityManager.createNativeQuery("select id, amount, flwTxnReference as flutterReference, rrn, datetime as transactionDate,"
                + " transactionCurrency as currency from transaction_warehouse as transaction"+query, FetchRRNModel.class).getResultList();
        
        if(model == null || model.isEmpty())
            return null;
        
        return model.get(0);
    }
}

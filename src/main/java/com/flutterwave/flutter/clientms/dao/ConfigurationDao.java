/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Configuration;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class ConfigurationDao extends HibernateDao<Configuration>{

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public ConfigurationDao() {
        super(Configuration.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        
        return entityManager;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String getConfig(String key){
        
        try{
            String query = "select value from configuration where name='"+key+"'";

            String value = entityManager.createNativeQuery(query).getSingleResult().toString();

            return value;
        }catch(Exception ex){
            return null;
        }
    }
    
}

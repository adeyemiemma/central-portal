/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.RaveTransactionWH;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class RaveTransactionWHDao extends HibernateDao<RaveTransactionWH> {

    @PersistenceContext(unitName = Global.RAVE_PERSISTENT_NAME)
    private EntityManager entityManager;

    public RaveTransactionWHDao() {
        super(RaveTransactionWH.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Map<String, String> getPaymentType() {

        List list = entityManager.createNativeQuery("select distinct(paymenttype) from txwarehouse;").getResultList();

        final Map<String, String> responses = new TreeMap<>();

        list.stream().forEachOrdered((data) -> {

//            Object x = () data;

            responses.put((String) data, (String) data );
        });

        return responses;
    }
    
    public Map<String, String> getPaymentCurrencies() {

        List list = entityManager.createNativeQuery("select abbreviation,name from currencies;").getResultList();

        final Map<String, String> responses = new TreeMap<>();

        list.stream().forEachOrdered((data) -> {

            Object[] x = (Object[]) data;

            responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });

        return responses;
    }
    
    public List getSettlement(Date startDate, Date endDate, String currency, String paymentType){
    
        String query = " where txwarehouse.status = 'successful'  ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and txwarehouse.created >= '" + dateTimeFormat.format(startDate) + "' and txwarehouse.created <= '" + dateTimeFormat.format(endDate) + "' ";
        }

        if (currency != null) {
            query += " and txwarehouse.currency='" + currency + "'";
        }

        if (paymentType != null) {

            query += " and txwarehouse.paymenttype ='" + paymentType + "'";
        }

        String baseQuery = "select acctbusinessname, paymenttype, sum(chargedAmount), sum(appFee), sum(merchantFee),  count(*), currency, acctparentbusinessname, " +
"settlement_account.account_number, settlement_account.bankCode, preference_value from txwarehouse " +
"left join (select preference_value, AccountId from preferences  where preferences.`availablesettingId` = 4 group by AccountId) as preferences on preferences.AccountId = txwarehouse.acctparent " +
"join (select account_number, bankCode, AccountId from settlement_account  where settlement_account.`AccountId` group by AccountId) as settlement_account on settlement_account.AccountId = txwarehouse.accountid ";
        
        List list = entityManager.createNativeQuery( baseQuery +" " + query + " group by txwarehouse.accountid ")
                .getResultList();

        return list;
    }
    
    public Page<RaveTransactionWH> search(int pageNum, int pageSize, Date startDate, Date endDate, String transactionType, 
            String fraudStatus, String chargeType, String cycle, String paymentEntity,
            String status, String search, String country, String merchant, Boolean isMerchantId, String currency){
   
            
        Page<RaveTransactionWH> page = new Page<>();
        
        String query = " where txwarehouse.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(txwarehouse.created) >= date('"+dateFormat.format(startDate)+"') and date(txwarehouse.created) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(fraudStatus != null){
            query += " and txwarehouse.fraudstatus='"+fraudStatus+"'";
        }
        
        if(chargeType != null){
            query += " and txwarehouse.chargetype='"+chargeType+"'";
        }
        
        if(cycle != null){
            query += " and txwarehouse.cycle='"+cycle+"'";
        }
        
        if(paymentEntity != null){
            query += " and txwarehouse.paymenttype='"+paymentEntity+"'";
        }
        
        if(status != null){
            query += " and txwarehouse.status='"+status+"'";
        }
        
        if(currency != null){
            query += " and txwarehouse.currency='"+currency+"'";
        }
        
        if((Utility.emptyToNull(merchant) != null)  || Utility.emptyToNull(country) != null){
//            joinQuery += " join Accounts on Accounts.id=txwarehouse.merchant";
            
            if(country != null)
                query += " and acctcountry='"+country+"'";
            
            if(merchant != null && (isMerchantId == null || isMerchantId == false))
                query += " and acctbusinessname like '%"+merchant+"%'";
            else if(merchant != null && isMerchantId == true)
                query += " and accountid ="+merchant+"";
        }
        
        if(search != null && !"".equals(search)){
            query += " and flwref like '%"+search+"%' or txref like '%"+search+"%'";
        }
        
        List<RaveTransactionWH> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select txwarehouse.* from txwarehouse "+joinQuery+" "+query+" order by id desc", RaveTransactionWH.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<RaveTransactionWH>)entityManager.createNativeQuery("select txwarehouse.* from txwarehouse "+joinQuery+" "+query +" order by id desc", RaveTransactionWH.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from txwarehouse "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);

        
        return page;
    }
    
    public Page<RaveTransactionWH> searchBank(int pageNum, int pageSize, Date startDate, Date endDate, String transactionType, 
            String fraudStatus, String chargeType, String cycle, String paymentEntity,
            String status, String search, String country, String merchant, Boolean isMerchantId, String currency, String parentId){
   
            
        Page<RaveTransactionWH> page = new Page<>();
        
        String query = " where txwarehouse.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(txwarehouse.created) >= date('"+dateFormat.format(startDate)+"') and date(txwarehouse.created) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(Utility.emptyToNull(fraudStatus) != null){
            query += " and txwarehouse.fraudstatus='"+fraudStatus+"'";
        }
        
        if(Utility.emptyToNull(chargeType) != null){
            query += " and txwarehouse.chargetype='"+chargeType+"'";
        }
        
        if(Utility.emptyToNull(cycle) != null){
            query += " and txwarehouse.cycle='"+cycle+"'";
        }
        
        if(Utility.emptyToNull(paymentEntity) != null){
            query += " and txwarehouse.paymenttype='"+paymentEntity+"'";
        }
        
        if(Utility.emptyToNull(status) != null){
            query += " and txwarehouse.status='"+status+"'";
        }
        
        if(Utility.emptyToNull(currency) != null){
            query += " and txwarehouse.currency='"+currency+"'";
        }
        
        if(Utility.emptyToNull(parentId) != null){
            query += " and txwarehouse.acctparent='"+parentId+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))  || (country != null && !"".equalsIgnoreCase(country) )){
//            joinQuery += " join Accounts on Accounts.id=txwarehouse.merchant";
            
            if(country != null)
                query += " and country='"+country+"'";
            
            if(merchant != null && (isMerchantId == null || isMerchantId == false))
                query += " and acctbusinessname like '%"+merchant+"%'";
            else if(merchant != null && isMerchantId == true)
                query += " and accountid ="+merchant+"";
        }
        
        if(Utility.emptyToNull(search) != null){
            query += " and flwref like '%"+search+"%' or txref like '%"+search+"%'";
        }
        
        List<RaveTransactionWH> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select txwarehouse.* from txwarehouse "+joinQuery+" "+query+" order by id desc", RaveTransactionWH.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<RaveTransactionWH>)entityManager.createNativeQuery("select txwarehouse.* from txwarehouse "+joinQuery+" "+query +" order by id desc", RaveTransactionWH.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from txwarehouse "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);

        
        return page;
    }
    
    public Page<RaveTransactionWH> searchByStatus(int pageNum, int pageSize, Date startDate, Date endDate, String transactionType, 
            String fraudStatus, String chargeType, String cycle, String paymentEntity,
            String search, String country, String merchant, Boolean isMerchantId, String currency, boolean failed){
   
            
        Page<RaveTransactionWH> page = new Page<>();
        
        String query = " where txwarehouse.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(txwarehouse.created) >= date('"+dateFormat.format(startDate)+"') and date(txwarehouse.created) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(fraudStatus != null){
            query += " and txwarehouse.fraudstatus='"+fraudStatus+"'";
        }
        
        if(chargeType != null){
            query += " and txwarehouse.chargetype='"+chargeType+"'";
        }
        
        if(cycle != null){
            query += " and txwarehouse.cycle='"+cycle+"'";
        }
        
        if(paymentEntity != null){
            query += " and txwarehouse.paymententity='"+paymentEntity+"'";
        }
        
        if(failed == true){
            query += " and txwarehouse.status <> 'successful'";
        }else
            query += " and txwarehouse.status = 'successful'";
        
        if(currency != null){
            query += " and txwarehouse.currency='"+currency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))  || (country != null && !"".equalsIgnoreCase(country) )){
            //joinQuery += " join Accounts on Accounts.id=txwarehouse.merchant_id";
            
            if(country != null)
                query += " and country='"+country+"'";
            
            if(merchant != null && (isMerchantId == null || isMerchantId == false))
                query += " and merchant like '%"+merchant+"%'";
            else if(merchant != null && isMerchantId == true)
                query += " and accountid ="+merchant+"";
        }
        
        if(search != null && !"".equals(search)){
            query += " and flwref like '%"+search+"%' or txref like '%"+search+"%'";
        }
        
        List<RaveTransactionWH> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select txwarehouse.* from txwarehouse "+joinQuery+" "+query, RaveTransactionWH.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<RaveTransactionWH>)entityManager.createNativeQuery("select txwarehouse.* from txwarehouse "+joinQuery+" "+query +"", RaveTransactionWH.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from txwarehouse "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);

        
        return page;
    }
    
    public Page<RaveTransactionWH> searchInternationalByStatus(int pageNum, int pageSize, Date startDate, Date endDate, String transactionType, 
            String fraudStatus, String chargeType, String cycle, String paymentEntity,
            String search, String country, String merchant, Boolean isMerchantId, String localCurrency, boolean failed){
   
            
        Page<RaveTransactionWH> page = new Page<>();
        
        String query = " where txwarehouse.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(txwarehouse.created) >= date('"+dateFormat.format(startDate)+"') and date(txwarehouse.created) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(fraudStatus != null){
            query += " and txwarehouse.fraudstatus='"+fraudStatus+"'";
        }
        
        if(chargeType != null){
            query += " and txwarehouse.chargetype='"+chargeType+"'";
        }
        
        if(cycle != null){
            query += " and txwarehouse.cycle='"+cycle+"'";
        }
        
        if(paymentEntity != null){
            query += " and txwarehouse.paymententity='"+paymentEntity+"'";
        }
        
        if(failed == true){
            query += " and txwarehouse.status <> 'successful'";
        }else
            query += " and txwarehouse.status = 'successful'";
        
        if(localCurrency != null){
            query += " and txwarehouse.currency <> '"+localCurrency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))  || (country != null && !"".equalsIgnoreCase(country) )){
//            joinQuery += " join Accounts on Accounts.id=txwarehouse.merchant_id";
            
            if(country != null)
                query += " and country='"+country+"'";
            
            if(merchant != null && (isMerchantId == null || isMerchantId == false))
                query += " and merchant like '%"+merchant+"%'";
            else if(merchant != null && isMerchantId == true)
                query += " and accountid ="+merchant+"";
        }
        
        if(search != null && !"".equals(search)){
            query += " and flwref like '%"+search+"%' or txref like '%"+search+"%'";
        }
        
        List<RaveTransactionWH> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select txwarehouse.* from txwarehouse "+joinQuery+" "+query, RaveTransactionWH.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<RaveTransactionWH>)entityManager.createNativeQuery("select txwarehouse.* from txwarehouse "+joinQuery+" "+query +"", RaveTransactionWH.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from txwarehouse "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);

        
        return page;
    }
    
    public List<String> getStatus(){
        
        List<String> list = entityManager.createNativeQuery("select distinct(status) from txwarehouse").getResultList();
        
        return list;
    }
    
    public List getSummaryWithDate(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where txwarehouse.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(txwarehouse.created) >= date('"+dateFormat.format(startDate)+"') and date(txwarehouse.created) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and txwarehouse.currency='"+currency+"'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
//            joinQuery += " join Accounts on Accounts.id=txwarehouse.merchant_id";
           
            query += " and merchant like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(appfee), count(*), date(txwarehouse.created), status  from txwarehouse "+joinQuery+" "+query + " group by date(txwarehouse.created), status")
                .getResultList();
        
        return list;
    }
    
    public List getTotalSummary(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where txwarehouse.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(txwarehouse.created) >= date('"+dateFormat.format(startDate)+"') and date(txwarehouse.created) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and txwarehouse.currency='"+currency+"'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
//            joinQuery += " join Accounts on Accounts.id=txwarehouse.merchant_id";
           
            query += " and merchant like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(appfee), status from txwarehouse "+joinQuery+" "+query + " group by status ")
                .getResultList();
        
        return list;
    }
    
    public List getTotalSummaryNoGrouping(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where txwarehouse.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(txwarehouse.created) >= date('"+dateFormat.format(startDate)+"') and date(txwarehouse.created) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and txwarehouse.currency='"+currency+"'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
//            joinQuery += " join Accounts on Accounts.id=txwarehouse.merchant_id";
           
            query += " and txwarehouse.acctbusinessname like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(appfee), status from txwarehouse "+joinQuery+" "+query + " ")
                .getResultList();
        
        return list;
    }
    
    public List getSummaryByStatus(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where txwarehouse.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(txwarehouse.created) >= date('"+dateFormat.format(startDate)+"') and date(txwarehouse.created) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and txwarehouse.currency='"+currency+"'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
//            joinQuery += " join Accounts on Accounts.id=txwarehouse.merchant_id";
           
            query += " and merchant like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), status from txwarehouse "+joinQuery+" "+query + " group by status ")
                .getResultList();
        
        return list;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List getReportSummary( Date startDate, Date endDate,
            String transactionStatus, String searchReference, String merchant, String currency){
    
        String query = " where txwarehouse.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(txwarehouse.created) >= date('"+dateFormat.format(startDate)+"') and date(txwarehouse.created) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(transactionStatus != null){
            query += " and status='"+transactionStatus+"'";
        }
        
        if(currency != null){
            query += " and txwarehouse.currency='"+currency+"'";
        }
        
        if(searchReference != null && !"".equals(searchReference)){
            query += " and flwref like '%"+searchReference+"%' or txref like '%"+searchReference+"%'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
//            joinQuery += " join Accounts on Accounts.id=txwarehouse.merchant_id";
           
            query += " and accountid='"+merchant+"'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), date(txwarehouse.created) as transaction_date, IF(`status` like '%successful%', 'success', 'failed') as t_status, count(*) as volume  from txwarehouse "+joinQuery+" "+ query +" group by `transaction_date`, t_status;")
                .getResultList();
        
        
        return list;
    }
    
}

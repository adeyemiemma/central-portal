/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.PosTransactionViewModelWeb;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class PosTransactionDao extends HibernateDao<PosTransaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public PosTransactionDao() {
        super(PosTransaction.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    public List<PosTransaction> findReschedule() {

        String query = " where reschedule=true ";

        List<PosTransaction> list = getEntityManager().createNativeQuery("select * from pos_transaction" + query, entityClass).getResultList();

        return list;

    }
    
//    public PosTransaction findRRNTerminal(String rrn, String terminalId, Date date) throws DatabaseException{
//        
//        try{
//            
//            if(rrn == null || terminalId == null || date == null)
//                return null;
//        // This is used to build the criteria
//            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
//
//            // This is used to build a cq instance from the entity
//            CriteriaQuery<PosTransaction> query = builder.createQuery(entityClass);
//
//            // This used to map the entities to a class and extract the fields involved
//            Root<PosTransaction> rootQuery = query.from(entityClass);
//
//            ParameterExpression terminalParam = builder.parameter(String.class);
//            ParameterExpression rrnParam = builder.parameter(String.class);
//            ParameterExpression<java.util.Date> dateParam = builder.parameter(Date.class);
//
//            Predicate predicate = builder.equal(rootQuery.get("rrn"), rrnParam);
//            
//            predicate = builder.and(predicate, builder.equal(rootQuery.get("terminalId"), terminalParam));
//            predicate = builder.and(predicate, builder.equal(rootQuery.get("requestDate"), dateParam));
//            
//            // This is used to build the cq that will allow the keyParam
//            query.select(rootQuery).where(predicate);
//
//            List<PosTransaction> list = getEntityManager().createQuery(query)
//                    .setParameter(rrnParam, rrn)
//                    .setParameter(terminalParam, terminalId)
//                    .setParameter(dateParam, date, TemporalType.DATE)
//                    .getResultList();
//
//            if(list.isEmpty())
//                return null;
//
//            return list.get(0);
//        }catch(Exception ex){
//            ex.printStackTrace();
//            throw new DatabaseException(ex);
//        }
//    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public PosTransaction findRRNTerminal(String rrn, String terminalId, Date date) throws DatabaseException{
        
        try{
            
            if(rrn == null || terminalId == null || date == null)
                return null;
        // This is used to build the criteria
            
            String query = " where transaction.rrn = '"+rrn+"'";
            
            query += " and transaction.terminalId = '"+terminalId+"'";
            
            query += " and date(transaction.requestDate) = date('"+Utility.formatDate(date, "yyyy-MM-dd")+"')";

            List<PosTransaction> list = getEntityManager().createNativeQuery("select * from pos_transaction as transaction "+query, entityClass)
                    .getResultList();

            if(list == null || list.isEmpty())
                return null;

            return list.get(0);
        }catch(Exception ex){
            ex.printStackTrace();
            throw new DatabaseException(ex);
        }
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public PosTransaction findRRNTerminalOnly(String rrn, String terminalId) throws DatabaseException{
        
        try{
            
            if(rrn == null || terminalId == null)
                return null;
        // This is used to build the criteria
            
            String query = " where transaction.rrn = '"+rrn+"'";
            
            query += " and transaction.terminalId = '"+terminalId+"'";
            
//            query += " and date(transaction.requestDate) = date('"+Utility.formatDate(date, "yyyy-MM-dd")+"')";

            List<PosTransaction> list = getEntityManager().createNativeQuery("select * from pos_transaction as transaction "+query, entityClass)
                    .getResultList();

            if(list == null || list.isEmpty())
                return null;

            return list.get(0);
        }catch(Exception ex){
            ex.printStackTrace();
            throw new DatabaseException(ex);
        }
    }
    
    
    public List<PosTransaction> findRecent(String terminalId, Date startDate, int limit) throws DatabaseException{
        
        try{
            
            if(terminalId == null || startDate == null)
                return null;
        // This is used to build the criteria
            
            String query = " where ";
            
            query += " and transaction.terminalId = '"+terminalId+"'";
            
            query += " and date(transaction.requestDate) >= date('"+Utility.formatDate(startDate, "yyyy-MM-dd")+"')";

            List<PosTransaction> list = getEntityManager().createNativeQuery("select * from pos_transaction as transaction "
                    +query + " order by transaction.requestDate desc limit "+limit, entityClass)
                    .getResultList();

            if(list == null || list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            ex.printStackTrace();
            throw new DatabaseException(ex);
        }
    }
    
    public PosTransaction findLastRecent(String terminalId, Date startDate) throws DatabaseException{
        
        try{
            
            if(terminalId == null || startDate == null)
                return null;
        // This is used to build the criteria
            
            String query = " where ";
            
            query += " and transaction.terminalId = '"+terminalId+"'";
            
            query += " and date(transaction.requestDate) >= date('"+Utility.formatDate(startDate, "yyyy-MM-dd")+"')";

            List<PosTransaction> list = getEntityManager().createNativeQuery("select * from pos_transaction as transaction "
                    +query + " order by transaction.requestDate desc limit 1 offset 1", entityClass)
                    .getResultList();

            if(list == null || list.isEmpty())
                return null;

            return list.get(0);
        }catch(Exception ex){
            ex.printStackTrace();
            throw new DatabaseException(ex);
        }
    }
    
    public PosTransaction findRRNTerminal(String rrn, String terminalId, String maskedPan) throws DatabaseException{
        
        try{
            
            if(rrn == null || terminalId == null || maskedPan == null)
                return null;
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<PosTransaction> query = builder.createQuery(entityClass);

            // This used to map the entities to a class and extract the fields involved
            Root<PosTransaction> rootQuery = query.from(entityClass);

            ParameterExpression terminalParam = builder.parameter(String.class);
            ParameterExpression rrnParam = builder.parameter(String.class);
            ParameterExpression maskedPanParam = builder.parameter(String.class);

            Predicate predicate = builder.equal(rootQuery.get("rrn"), rrnParam);
            
            predicate = builder.and(predicate, builder.equal(rootQuery.get("terminalId"), terminalParam));
            predicate = builder.and(predicate, builder.equal(rootQuery.get("pan"), maskedPanParam));
            
            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(predicate);

            List<PosTransaction> list = getEntityManager().createQuery(query)
                    .setParameter(rrnParam, rrn)
                    .setParameter(terminalParam, terminalId)
                    .setParameter(maskedPanParam, maskedPan)
                    .getResultList();

            if(list == null || list.isEmpty())
                return null;

            return list.get(0);
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<PosTransaction> find(int pageNo, int pageSize, String terminalId, String merchantId, String provider,
            String merchantCode, Date startDate, Date endDate, String responseCode, String status,
            String currency, String type, String posId, String scheme, String search) {

        String query = " where pos_transaction.id <> 0 ";

        String joinQuery = " join mpos_terminal on mpos_terminal.terminalId = pos_transaction.terminalId";

        joinQuery += " join mpos_merchant on (mpos_merchant.id = mpos_terminal.merchant_id or pos_transaction.posId = mpos_merchant.posId)";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and (pos_transaction.requestDate >= TIMESTAMP('" + dateFormat.format(startDate) + "') and pos_transaction.requestDate <= TIMESTAMP('" + dateFormat.format(endDate) + "'))";
        }

        if (scheme != null && !scheme.isEmpty()) {
            query += " and pos_transaction.scheme = '" + scheme + "'";
        }

        if (currency != null && !currency.isEmpty()) {
            query += " and pos_transaction.currency='" + currency + "'";
        }

        if (merchantId != null && !merchantId.isEmpty()) {
            query += " and mpos_merchant.merchantId='" + merchantId + "'";
        }

        if (merchantCode != null && !merchantCode.isEmpty()) {
            query += " and mpos_merchant.merchantCode='" + merchantCode + "'";
        }

        if (provider != null && !provider.isEmpty()) {
            query += " and mpos_terminal.provider_id ='" + provider + "'";
        }

        if (responseCode != null && !responseCode.isEmpty()) {
            query += " and pos_transaction.responseCode='" + responseCode + "'";
        }

        if (terminalId != null && !terminalId.isEmpty()) {
            query += " and pos_transaction.terminalId='" + terminalId + "'";
        }
//        
        if (type != null && !type.isEmpty()) {
            query += " and pos_transaction.type = '" + type + "%'";
        }

        if (search != null && !search.isEmpty()) {
            query += " and pos_transaction.rrn like '%" + search + "%'";
        }

        if (posId != null && !posId.isEmpty()) {
            query += " and pos_transaction.posId = '" + posId + "'";
        }

        List<PosTransaction> list = null;
        
        if(pageSize == 0)
            list = entityManager.createNativeQuery("select pos_transaction.*, mpos_merchant.name from pos_transaction " + joinQuery + " " + query + " group by pos_transaction.terminalId, pos_transaction.rrn order by requestDate desc ", PosTransaction.class)
                .setFirstResult(pageNo)
                .getResultList();
        else
            list = entityManager.createNativeQuery("select pos_transaction.*, mpos_merchant.name from pos_transaction " + joinQuery + " " + query + " group by pos_transaction.terminalId, pos_transaction.rrn order by requestDate desc ", PosTransaction.class)
                .setFirstResult(pageNo)
                .setMaxResults(pageSize)
                .getResultList();

        Object countResult = entityManager.createNativeQuery("select count(*) as count from pos_transaction " + joinQuery + " " + query).getSingleResult();

        long count = Long.parseLong(countResult + "");

        Page<PosTransaction> pos_transactions = new Page(count, list);

        return pos_transactions;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<PosTransaction> find(int pageNo, int pageSize, String terminalId, String merchantId, String provider,
            String merchantCode, Date startDate, Date endDate, String responseCode, String status,
            String currency, String type, String posId, String scheme, String search, Boolean callback) {

        String query = " where pos_transaction.id <> 0 ";

        String joinQuery = " join mpos_terminal on mpos_terminal.terminalId = pos_transaction.terminalId";

        joinQuery += " join mpos_merchant on (mpos_merchant.id = mpos_terminal.merchant_id or pos_transaction.posId = mpos_merchant.posId)";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and (pos_transaction.requestDate >= TIMESTAMP('" + dateFormat.format(startDate) + "') and pos_transaction.requestDate <= TIMESTAMP('" + dateFormat.format(endDate) + "'))";
        }

        if (scheme != null && !scheme.isEmpty()) {
            query += " and pos_transaction.scheme = '" + scheme + "'";
        }

        if (currency != null && !currency.isEmpty()) {
            query += " and pos_transaction.currency='" + currency + "'";
        }

        if (merchantId != null && !merchantId.isEmpty()) {
            query += " and mpos_merchant.merchantId='" + merchantId + "'";
        }

        if (merchantCode != null && !merchantCode.isEmpty()) {
            query += " and mpos_merchant.merchantCode='" + merchantCode + "'";
        }

        if (provider != null && !provider.isEmpty()) {
            query += " and mpos_terminal.provider_id ='" + provider + "'";
        }

        if (responseCode != null && !responseCode.isEmpty()) {
            query += " and pos_transaction.responseCode='" + responseCode + "'";
        }

        if (terminalId != null && !terminalId.isEmpty()) {
            query += " and pos_transaction.terminalId='" + terminalId + "'";
        }
//        
        if (type != null && !type.isEmpty()) {
            query += " and pos_transaction.type = '" + type + "%'";
        }

        if (search != null && !search.isEmpty()) {
            query += " and pos_transaction.rrn like '%" + search + "%'";
        }

        if (posId != null && !posId.isEmpty()) {
            query += " and pos_transaction.posId = '" + posId + "'";
        }
        
        if (callback != null) {
            query += " and pos_transaction.callback = " + callback + "";
        }

        List<PosTransaction> list = null;
        
        if(pageSize == 0)
            list = entityManager.createNativeQuery("select pos_transaction.*, mpos_merchant.name from pos_transaction " + joinQuery + " " + query + " group by pos_transaction.terminalId, pos_transaction.rrn order by requestDate desc ", PosTransaction.class)
                .setFirstResult(pageNo)
                .getResultList();
        else
            list = entityManager.createNativeQuery("select pos_transaction.*, mpos_merchant.name from pos_transaction " + joinQuery + " " + query + " group by pos_transaction.terminalId, pos_transaction.rrn order by requestDate desc ", PosTransaction.class)
                .setFirstResult(pageNo)
                .setMaxResults(pageSize)
                .getResultList();

        Object countResult = entityManager.createNativeQuery("select count(*) as count from pos_transaction " + joinQuery + " " + query).getSingleResult();

        long count = Long.parseLong(countResult + "");

        Page<PosTransaction> pos_transactions = new Page(count, list);

        return pos_transactions;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<PosTransaction> find(int pageNo, int pageSize, String terminalId, String merchantId, String provider,
            String merchantCode, Date startDate, Date endDate, String responseCode, String status,
            String currency, String type, String[] posId, String scheme, String search, Boolean callback) {

        String query = " where pos_transaction.id <> 0 ";

        String joinQuery = " join mpos_terminal on mpos_terminal.terminalId = pos_transaction.terminalId";

        joinQuery += " join mpos_merchant on (mpos_merchant.id = mpos_terminal.merchant_id or pos_transaction.posId = mpos_merchant.posId)";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and (pos_transaction.requestDate >= TIMESTAMP('" + dateFormat.format(startDate) + "') and pos_transaction.requestDate <= TIMESTAMP('" + dateFormat.format(endDate) + "'))";
        }

        if (scheme != null && !scheme.isEmpty()) {
            query += " and pos_transaction.scheme = '" + scheme + "'";
        }

        if (currency != null && !currency.isEmpty()) {
            query += " and pos_transaction.currency='" + currency + "'";
        }

        if (merchantId != null && !merchantId.isEmpty()) {
            query += " and mpos_merchant.merchantId='" + merchantId + "'";
        }

        if (merchantCode != null && !merchantCode.isEmpty()) {
            query += " and mpos_merchant.merchantCode='" + merchantCode + "'";
        }

        if (provider != null && !provider.isEmpty()) {
            query += " and mpos_terminal.provider_id ='" + provider + "'";
        }

        if (responseCode != null && !responseCode.isEmpty()) {
            query += " and pos_transaction.responseCode='" + responseCode + "'";
        }

        if (terminalId != null && !terminalId.isEmpty()) {
            query += " and pos_transaction.terminalId='" + terminalId + "'";
        }
//        
        if (type != null && !type.isEmpty()) {
            query += " and pos_transaction.type = '" + type + "%'";
        }

        if (search != null && !search.isEmpty()) {
            query += " and pos_transaction.rrn like '%" + search + "%'";
        }

        if (posId != null && posId.length > 0) {
            query += " and pos_transaction.posId in (" + Arrays.stream(posId).map(x -> "\""+x+"\"").collect(Collectors.joining(",")) + ")";
        }
        
        if (callback != null) {
            query += " and pos_transaction.callback = " + callback + "";
        }

        List<PosTransaction> list = null;
        
        if(pageSize == 0)
            list = entityManager.createNativeQuery("select pos_transaction.*, mpos_merchant.name from pos_transaction " + joinQuery + " " + query + " group by pos_transaction.terminalId, pos_transaction.rrn order by requestDate desc ", PosTransaction.class)
                .setFirstResult(pageNo)
                .getResultList();
        else
            list = entityManager.createNativeQuery("select pos_transaction.*, mpos_merchant.name from pos_transaction " + joinQuery + " " + query + " group by pos_transaction.terminalId, pos_transaction.rrn order by requestDate desc ", PosTransaction.class)
                .setFirstResult(pageNo)
                .setMaxResults(pageSize)
                .getResultList();

        Object countResult = entityManager.createNativeQuery("select count(*) as count from pos_transaction " + joinQuery + " " + query).getSingleResult();

        long count = Long.parseLong(countResult + "");

        Page<PosTransaction> pos_transactions = new Page(count, list);

        return pos_transactions;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<PosTransaction> find(String terminalId, String merchantId, String provider,
            String merchantCode, String rrn, String posId) {

        String query = " where pos_transaction.id <> 0 ";

        String joinQuery = "";// join mpos_terminal on mpos_terminal.terminalId = pos_transaction.terminalId";

        joinQuery += " join mpos_merchant on mpos_merchant.id = mpos_terminal.merchant_id";

        if (merchantId != null && !merchantId.isEmpty()) {
            query += " and mpos_merchant.merchantId='" + merchantId + "'";
        }

        if (merchantCode != null && !merchantCode.isEmpty()) {
            query += " and mpos_merchant.merchantCode='" + merchantCode + "'";
        }

        if (provider != null && !provider.isEmpty()) {
            query += " and mpos_terminal.provider_id ='" + provider + "'";
        }

        if (terminalId != null && !terminalId.isEmpty()) {
            query += " and pos_transaction.terminalId='" + terminalId + "'";
        }

        if (rrn != null && !rrn.isEmpty()) {
            query += " and pos_transaction.rrn = '" + rrn + "'";
        }

        if (posId != null && !posId.isEmpty()) {
            query += " and pos_transaction.posId = '" + posId + "'";
        }

        List<PosTransaction> list = entityManager.createNativeQuery("select pos_transaction.*, mpos_merchant.name from pos_transaction " + joinQuery + "" + query + " group by pos_transaction.terminalId, pos_transaction.rrn order by TIMESTAMP(requestDate) desc ", PosTransaction.class)
                .getResultList();

        return list;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<PosTransaction> find(int pageNo, int pageSize, String merchantId, String merchantCode, String provider,
            Date startDate, Date endDate, String responseCode,
            String currency, String type, String scheme, String bankName, String search) {

        String query = " where pos_transaction.id <> 0 ";

        String joinQuery = " left join mpos_terminal on mpos_terminal.terminalId = pos_transaction.terminalId";

        joinQuery += " left join mpos_merchant on  pos_transaction.posId = mpos_merchant.posId";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and (pos_transaction.requestDate >= TIMESTAMP('" + dateFormat.format(startDate) + "') and pos_transaction.requestDate <= TIMESTAMP('" + dateFormat.format(endDate) + "'))";
        }

        if (scheme != null && !scheme.isEmpty()) {
            query += " and pos_transaction.scheme = '" + scheme + "'";
        }

        if (currency != null && !currency.isEmpty()) {
            query += " and pos_transaction.currency='" + currency + "'";
        }

        if (merchantId != null && !merchantId.isEmpty()) {
            query += " and mpos_merchant.merchantId='" + merchantId + "'";
        }

        if (Utility.emptyToNull(bankName) != null) {
            query += " and mpos_terminal.bankName='" + bankName + "'";
        }

        if (merchantCode != null && !merchantCode.isEmpty()) {
            query += " and mpos_merchant.merchantCode='" + merchantCode + "'";
        }
        
        if (provider != null && !provider.isEmpty()) {
            query += " and mpos_terminal.provider_id ='" + provider + "'";
        }

        if (responseCode != null && !responseCode.isEmpty()) {
            query += " and pos_transaction.responseCode='" + responseCode + "'";
        }
//        
        if (type != null && !type.isEmpty()) {
            query += " and pos_transaction.type = '" + type + "'";
        }

        if (search != null && !search.isEmpty()) {
            query += " and (pos_transaction.rrn like '%" + search + "%' or pos_transaction.terminalId like '%" + search + "%' ";
            query += " or pos_transaction.posId like '%" + search + "%' or pos_transaction.pan like '%" + search + "' ";
            query += " or pos_transaction.refCode like '" + search + "%') ";
        }

//        if (posId != null && !posId.isEmpty()) {
//            query += " and pos_transaction.posId = '%" + posId + "%'";
//        }

        List<PosTransaction> list;
        
        if(pageSize > 0)
            list = entityManager.createNativeQuery("select pos_transaction.*, mpos_merchant.name from pos_transaction " + joinQuery + " " + query + "  order by TIMESTAMP(requestDate) desc ", PosTransaction.class)
                .setFirstResult(pageNo)
                .setMaxResults(pageSize)
                .getResultList();
        else
            list = entityManager.createNativeQuery("select pos_transaction.*, mpos_merchant.name from pos_transaction " + joinQuery + " " + query + " order by TIMESTAMP(requestDate) desc ", PosTransaction.class)
                .setFirstResult(pageNo)
                .getResultList();

        Object countResult = entityManager.createNativeQuery("select count(*) as count from pos_transaction " + joinQuery + " " + query ).getSingleResult();

        long count = Long.parseLong(countResult + "");

        Page<PosTransaction> pos_transactions = new Page(count, list);

        return pos_transactions;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<PosTransactionViewModelWeb> findForWebNew(int pageNo, int pageSize, String merchantId, String merchantCode, String provider,
            Date startDate, Date endDate, String responseCode,
            String currency, String type, String scheme, String bankName, String search, Boolean NoCount) {

        String query = " where pos_transaction.id <> 0 ";

//        String joinQuery = " left join mpos_terminal on mpos_terminal.terminalId = pos_transaction.terminalId";
//
//        joinQuery += " left join mpos_merchant on  pos_transaction.posId = mpos_merchant.posId";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and (pos_transaction.requestDate >= TIMESTAMP('" + dateFormat.format(startDate) + "') and pos_transaction.requestDate <= TIMESTAMP('" + dateFormat.format(endDate) + "'))";
        }

        if (scheme != null && !scheme.isEmpty()) {
            query += " and pos_transaction.scheme = '" + scheme + "'";
        }

        if (currency != null && !currency.isEmpty()) {
            query += " and pos_transaction.currency='" + currency + "'";
        }

        if (merchantId != null && !merchantId.isEmpty()) {
            query += " and pos_transaction.productId='" + merchantId + "'";
        }

        if (Utility.emptyToNull(bankName) != null) {
            query += " and pos_transaction.acquirerBank='" + bankName + "'";
        }

        if (merchantCode != null && !merchantCode.isEmpty()) {
            query += " and pos_transaction.merchantCode='" + merchantCode + "'";
        }
        
        if (provider != null && !provider.isEmpty()) {
            query += " and pos_transaction.providerName ='" + provider + "'";
        }

        if (responseCode != null && !responseCode.isEmpty()) {
            query += " and pos_transaction.responseCode='" + responseCode + "'";
        }
//        
        if (type != null && !type.isEmpty()) {
            query += " and pos_transaction.type = '" + type + "'";
        }

        if (search != null && !search.isEmpty()) {
            query += " and (pos_transaction.rrn like '%" + search + "%' or pos_transaction.terminalId like '%" + search + "%' ";
            query += " or pos_transaction.posId like '%" + search + "%' or pos_transaction.pan like '%" + search + "' ";
            query += " or pos_transaction.refCode like '" + search + "%') ";
        }

//        if (posId != null && !posId.isEmpty()) {
//            query += " and pos_transaction.posId = '%" + posId + "%'";
//        }

        // pos_transaction.id, pos_transaction.rrn, pos_transaction.terminalId, pos_transaction.pan, pos_transaction.amount,
        // pos_transaction.currency, pos_transaction.refCode, pos_transaction.type, pos_transaction.responseCode,
        // pos_transaction.responseMessage, pos_transaction.status, pos_transaction.requestDate as datetime, 
        // pos_transaction.merchantCode, pos_transaction.merchantId, pos_transaction.providerId, mpos_terminal.provider,
        // mpos_merchant.name as merchantName, pos_transaction.posId, pos_transaction.createdOn, mpos_terminal.bankName
        
        List<PosTransactionViewModelWeb> list;
        
        if(pageSize > 0)
            list = entityManager.createNativeQuery("select pos_transaction.source, pos_transaction.id, pos_transaction.rrn, pos_transaction.terminalId, pos_transaction.pan, pos_transaction.amount,"
                    + " pos_transaction.currency, pos_transaction.refCode, pos_transaction.type, pos_transaction.responseCode,"
                    + "pos_transaction.responseMessage, pos_transaction.status, pos_transaction.requestDate as datetime,"
                    + "pos_transaction.fileId, pos_transaction.callback, pos_transaction.callbackOn,pos_transaction.reversed,"
                    + "pos_transaction.merchantCode, pos_transaction.productId as merchantId,"
                    + "pos_transaction.merchantName , pos_transaction.posId, pos_transaction.createdOn, pos_transaction.acquirerBank as bankName, "
                    + "pos_transaction.providerName as provider from pos_transaction " + " " + query + " order by requestDate desc ", PosTransactionViewModelWeb.class)
                .setFirstResult(pageNo)
                .setMaxResults(pageSize)
                .getResultList();
        else
            list = entityManager.createNativeQuery("select pos_transaction.source, pos_transaction.id, pos_transaction.rrn, pos_transaction.terminalId, pos_transaction.pan, pos_transaction.amount,"
                    + " pos_transaction.currency, pos_transaction.refCode, pos_transaction.type, pos_transaction.responseCode,"
                    + "pos_transaction.responseMessage, pos_transaction.status, pos_transaction.requestDate as datetime,"
                    + "pos_transaction.fileId, pos_transaction.callback, pos_transaction.callbackOn,pos_transaction.reversed,"
                    + "pos_transaction.merchantCode, pos_transaction.productId as merchantId,"
                    + "pos_transaction.merchantName , pos_transaction.posId, pos_transaction.createdOn, pos_transaction.acquirerBank as bankName, "
                    + "pos_transaction.providerName as provider from pos_transaction " + " " + query + " order by requestDate desc ", PosTransactionViewModelWeb.class)
                .setFirstResult(pageNo)
                .getResultList();

        long count = 0;
        
        if(NoCount == null || NoCount != false ){
            Object countResult = entityManager.createNativeQuery("select count(*) as count from pos_transaction " + " " + query ).getSingleResult();
            count = Long.parseLong(countResult + "");
        }

        Page<PosTransactionViewModelWeb> pos_transactions = new Page(count, list);

        return pos_transactions;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<PosTransactionViewModelWeb> findForWeb(int pageNo, int pageSize, String merchantId, String merchantCode, String provider,
            Date startDate, Date endDate, String responseCode,
            String currency, String type, String scheme, String bankName, String search, Boolean NoCount) {

        String query = " where ";

        String joinQuery = " left join mpos_terminal on mpos_terminal.terminalId = pos_transaction.terminalId";

//        joinQuery += " left join mpos_merchant on  pos_transaction.posId = mpos_merchant.posId";

//        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " (pos_transaction.requestDate >= TIMESTAMP('" + dateFormat.format(startDate) + "') and pos_transaction.requestDate <= TIMESTAMP('" + dateFormat.format(endDate) + "'))";
//        }

        if (scheme != null && !scheme.isEmpty()) {
            query += " and pos_transaction.scheme = '" + scheme + "'";
        }

        if (currency != null && !currency.isEmpty()) {
            query += " and pos_transaction.currency='" + currency + "'";
        }

        if (merchantId != null && !merchantId.isEmpty()) {
            query += " and productId='" + merchantId + "'";
        }

        if (Utility.emptyToNull(bankName) != null) {
            query += " and mpos_terminal.bankName='" + bankName + "'";
        }

        if (merchantCode != null && !merchantCode.isEmpty()) {
            query += " and pos_transaction.merchantCode='" + merchantCode + "'";
        }
        
        if (provider != null && !provider.isEmpty()) {
            query += " and mpos_terminal.provider_id ='" + provider + "'";
        }

        if (responseCode != null && !responseCode.isEmpty()) {
            query += " and pos_transaction.responseCode='" + responseCode + "'";
        }
//        
        if (type != null && !type.isEmpty()) {
            query += " and pos_transaction.type = '" + type + "'";
        }

        if (search != null && !search.isEmpty()) {
            query += " and (pos_transaction.rrn like '%" + search + "%' or pos_transaction.terminalId like '%" + search + "%' ";
            query += " or pos_transaction.posId like '%" + search + "%' or pos_transaction.pan like '%" + search + "' ";
            query += " or pos_transaction.refCode like '" + search + "%') ";
        }

//        if (posId != null && !posId.isEmpty()) {
//            query += " and pos_transaction.posId = '%" + posId + "%'";
//        }

        // pos_transaction.id, pos_transaction.rrn, pos_transaction.terminalId, pos_transaction.pan, pos_transaction.amount,
        // pos_transaction.currency, pos_transaction.refCode, pos_transaction.type, pos_transaction.responseCode,
        // pos_transaction.responseMessage, pos_transaction.status, pos_transaction.requestDate as datetime, 
        // pos_transaction.merchantCode, pos_transaction.merchantId, pos_transaction.providerId, mpos_terminal.provider,
        // mpos_merchant.name as merchantName, pos_transaction.posId, pos_transaction.createdOn, mpos_terminal.bankName
        
        List<PosTransactionViewModelWeb> list = new ArrayList<>();
        
        try{
        if(pageSize > 0)
            list = entityManager.createNativeQuery("select pos_transaction.source, pos_transaction.id, pos_transaction.rrn, pos_transaction.terminalId, pos_transaction.pan, pos_transaction.amount,"
                    + " pos_transaction.currency, pos_transaction.refCode, pos_transaction.type, pos_transaction.responseCode,"
                    + "pos_transaction.responseMessage, pos_transaction.status, pos_transaction.requestDate as datetime,"
                    + "pos_transaction.fileId, pos_transaction.callback, pos_transaction.callbackOn,pos_transaction.reversed,"
                    + "pos_transaction.merchantCode, pos_transaction.productId as merchantId,"
                    + "merchantName, pos_transaction.posId, pos_transaction.createdOn, mpos_terminal.bankName,  "
                    + " providerName as provider from pos_transaction " + joinQuery + " " + query + "  order by requestDate desc ", PosTransactionViewModelWeb.class)
                .setFirstResult(pageNo)
                .setMaxResults(pageSize)
                .getResultList();
        else
            list = entityManager.createNativeQuery("select pos_transaction.source, pos_transaction.id, pos_transaction.rrn, pos_transaction.terminalId, pos_transaction.pan, pos_transaction.amount,"
                    + " pos_transaction.currency, pos_transaction.refCode, pos_transaction.type, pos_transaction.responseCode,"
                    + "pos_transaction.responseMessage, pos_transaction.status, pos_transaction.requestDate as datetime,"
                    + "pos_transaction.fileId, pos_transaction.callback, pos_transaction.callbackOn,pos_transaction.reversed,"
                    + "pos_transaction.merchantCode, pos_transaction.productId as merchantId,"
                    + "merchantName, pos_transaction.posId, pos_transaction.createdOn, mpos_terminal.bankName, "
                    + "providerName as provider from pos_transaction " + joinQuery + " " + query + " order by requestDate desc ", PosTransactionViewModelWeb.class)
                .setFirstResult(pageNo)
                .getResultList();
        }catch(Exception ex){
            ex.printStackTrace();
        }

        long count = 0;
        
        if(NoCount == null || NoCount != false ){
            Object countResult = entityManager.createNativeQuery("select count(*) as count from pos_transaction " + joinQuery + " " + query ).getSingleResult();
            count = Long.parseLong(countResult + "");
        }

        Page<PosTransactionViewModelWeb> pos_transactions = new Page(count, list);

        return pos_transactions;
    }
    
    public List<PosTransactionViewModelWeb> getFailedCallbacks(Date startDate, Date endDate){
        
        if(startDate == null || endDate == null)
           return null;
        
        String query = " where pos_transaction.id <> 0 ";

        String joinQuery = "";// left join mpos_terminal on mpos_terminal.terminalId = pos_transaction.terminalId";

//        joinQuery += " left join mpos_merchant on  pos_transaction.posId = mpos_merchant.posId";
        
        query += " and callback = false and pos_transaction.createdOn between '"+Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss")+"'"
                +" and '"+Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss")+"'";
        
        List<PosTransactionViewModelWeb> list = entityManager.createNativeQuery("select pos_transaction.source, pos_transaction.id, pos_transaction.rrn, pos_transaction.terminalId, pos_transaction.pan, pos_transaction.amount,"
                    + " pos_transaction.currency, pos_transaction.refCode, pos_transaction.type, pos_transaction.responseCode,"
                    + "pos_transaction.responseMessage, pos_transaction.status, pos_transaction.requestDate as datetime,"
                    + "pos_transaction.fileId, pos_transaction.callback, pos_transaction.callbackOn,pos_transaction.reversed,"
                    + "pos_transaction.merchantCode, pos_transaction.productId as merchantId,"
                    + "pos_transaction.merchantName, pos_transaction.posId, pos_transaction.createdOn, pos_transaction.acquirerBank as bankName, "
                    + "pos_transaction.providerName as provider from pos_transaction " + joinQuery + " " + query + " order by TIMESTAMP(pos_transaction.requestDate) desc ", PosTransactionViewModelWeb.class)
                .getResultList();
        
        return list;
    }
    
    public List<PosTransactionViewModelWeb> findForDownload(long id){
        
        String query = " where pos_transaction.id = "+id;
        
        String joinQuery = ""; // left join mpos_terminal on mpos_terminal.terminalId = pos_transaction.terminalId";

//        joinQuery += " left join mpos_merchant on  pos_transaction.posId = mpos_merchant.posId";

        List<PosTransactionViewModelWeb> list;
        list = entityManager.createNativeQuery("select pos_transaction.source, pos_transaction.id, pos_transaction.rrn, pos_transaction.terminalId, pos_transaction.pan, pos_transaction.amount,"
                    + " pos_transaction.currency, pos_transaction.refCode, pos_transaction.type, pos_transaction.responseCode,"
                    + "pos_transaction.responseMessage, pos_transaction.status, pos_transaction.requestDate as datetime,"
                    + "pos_transaction.fileId, pos_transaction.callback, pos_transaction.callbackOn,pos_transaction.reversed,"
                    + "pos_transaction.merchantCode, pos_transaction.productId as merchantId,"
                    + "pos_transaction.merchantName, pos_transaction.posId, pos_transaction.createdOn, pos_transaction.acquirerBank as bankName, "
                    + "pos_transaction.providerName as provider from pos_transaction "+joinQuery+"" + query + " order by TIMESTAMP(requestDate) desc ", PosTransactionViewModelWeb.class)
                .getResultList();

        return list;

    }

    public Map<String, String> getResponse(String category) {

        String whereClause = "";

        List list = entityManager.createNativeQuery("select distinct(responseCode), responseMessage from pos_transaction " + whereClause + " group by responseCode").getResultList();

        final Map<String, String> responses = new TreeMap<>();

        list.stream().forEachOrdered((data) -> {

            Object[] x = (Object[]) data;

            responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });

        return responses;
    }
}

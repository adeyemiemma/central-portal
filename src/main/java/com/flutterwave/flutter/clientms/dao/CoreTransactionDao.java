/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.CoreTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.MoneywaveUtil;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class CoreTransactionDao extends HibernateDao<CoreTransaction> {

    @PersistenceContext(unitName = Global.CORE_PERSISTENT_NAME)
    private EntityManager entityManager;

    public CoreTransactionDao() {
        super(CoreTransaction.class);
    }
    
    String token_string = Global.TOKEN_STRING; // test_token for test and live_token for live
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<CoreTransaction> getTransactions(int pageNum, int pageSize, Date startDate, Date endDate,
            String transactionStatus, String searchReference, String merchant, Boolean isMerchantId, String currency){
        
        Page<CoreTransaction> page = new Page<>();
        
        String query = " where transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.date_of_transaction) >= date('"+dateFormat.format(startDate)+"') and date(transaction.date_of_transaction) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(transactionStatus != null){
            query += " and status='"+transactionStatus+"'";
        }
        
        if(currency != null){
            query += " and (currency='"+currency+"'";
            
            if("NGN".equalsIgnoreCase(currency)){
                query += " or currency is null";
            }
            
            query += ")";
        }
        
//        custidref
        
        if(searchReference != null && !"".equals(searchReference)){
            query += " and (transactionIdentifier like '%"+searchReference+"%' or transactionReference like '%"+searchReference+"%' or custidref like '%"+searchReference+"%')";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join users on users."+token_string+"=transaction.usertoken"; // for test change live to test
            
            if((isMerchantId == null || isMerchantId == false))
                query += " and users.companyname like '%"+merchant+"%'";
            else if( isMerchantId == true)
                query += " and users.userID="+merchant;

        }
        
        List<CoreTransaction> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select distinct transaction.* from transaction "+joinQuery+" "+query+" order by id desc", CoreTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<CoreTransaction>)entityManager.createNativeQuery("select distinct transaction.* from transaction "+joinQuery+" "+query +" order by id desc", CoreTransaction.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select  count(*) from transaction "+joinQuery+" "+query+"").getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);
        
        return page;
    }
    
    public Page<CoreTransaction> getTransactions(int pageNum, int pageSize, Date startDate, Date endDate,
            String transactionStatus, String searchReference, long merchant, String currency){
        
        Page<CoreTransaction> page = new Page<>();
        
        String query = " where transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.date_of_transaction) >= date('"+dateFormat.format(startDate)+"') and date(transaction.date_of_transaction) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(transactionStatus != null){
            query += " and status='"+transactionStatus+"'";
        }
        
        if(currency != null){
            query += " and (currency='"+currency+"'";
            
            if("NGN".equalsIgnoreCase(currency)){
                query += " or currency is null";
            }
            
            query += ")";
        }
        
        if(searchReference != null && !"".equals(searchReference)){
            query += " and transactionIdentifier like '%"+searchReference+"%' or transactionReference like '%"+searchReference+"%'";
        }
        
        if(merchant > 0){
            joinQuery += " join users on users."+token_string+"=transaction.usertoken"; // for test change live to test
            
            query += " and users.companyname like '%"+merchant+"%'";
        }
        
        List<CoreTransaction> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select distinct transaction.* from transaction "+joinQuery+" "+query+" order by id desc", CoreTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<CoreTransaction>)entityManager.createNativeQuery("select distinct transaction.* from transaction "+joinQuery+" "+query +" order by id desc", CoreTransaction.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select  count(*) from transaction "+joinQuery+" "+query+"").getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);
        
        return page;
    }
    
    public Page<CoreTransaction> getTransactionsByStatus(int pageNum, int pageSize, Date startDate, Date endDate,
            String searchReference, String merchant, String currency, boolean failed){
        
        Page<CoreTransaction> page = new Page<>();
        
        String query = " where transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.date_of_transaction) >= date('"+dateFormat.format(startDate)+"') and date(transaction.date_of_transaction) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(failed == true){
            query += " and status <> 'approved' and not status like '%success%'";
        }else
            query += " and (status = 'approved' or status like '%success%')";
        
        if(currency != null){
            query += " and (currency='"+currency+"'";
            
            if("NGN".equalsIgnoreCase(currency)){
                query += " or currency is null";
            }
            
            query += ")";
        }
        
        if(searchReference != null && !"".equals(searchReference)){
            query += " and transactionIdentifier like '%"+searchReference+"%' or transactionReference like '%"+searchReference+"%'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join users on users."+token_string+"=transaction.usertoken";
            
            query += " and users.companyname like '%"+merchant+"%'";
        }
        
        List<CoreTransaction> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select distinct transaction.* from transaction "+joinQuery+" "+query, CoreTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<CoreTransaction>)entityManager.createNativeQuery("select distinct transaction.* from transaction "+joinQuery+" "+query +"", CoreTransaction.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*)  from transaction "+joinQuery+" "+query+"").getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);
        
        return page;
    }
    
    public List<String> getResponse(){
        
        List list = entityManager.createNativeQuery("select distinct(statuscode), status from transaction group by statuscode").getResultList();
        
        final List<String> responses = new ArrayList<>();
        
        list.stream().map((obj) -> ((Object[])obj)[1]).distinct().filter(x -> x != null && !"".equals(x)).forEachOrdered((objStr) -> {
            responses.add((String) objStr);
        });
        
        return responses;
    }
    
    public List getSummaryWithDate(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where transaction.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.date_of_transaction) >= date('"+dateFormat.format(startDate)+"') and date(transaction.date_of_transaction) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and (currency='"+currency+"'";
            
            if("NGN".equalsIgnoreCase(currency)){
                query += " or currency is null";
            }
            
            query += ")";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join users on users."+token_string+"=transaction.usertoken";
            
            query += " and users.companyname like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), 0 ,count(*), date(transaction.date_of_transaction), status from transaction "+joinQuery+" "+query + " group by date(transaction.date_of_transaction), status")
                .getResultList();
        
        return list;
    }
    
    public List getTotalSummary(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where transaction.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.date_of_transaction) >= date('"+dateFormat.format(startDate)+"') and date(transaction.date_of_transaction) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and (currency='"+currency+"'";
            
            if("NGN".equalsIgnoreCase(currency)){
                query += " or currency is null";
            }
            
            query += ")";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join users on users."+token_string+"=transaction.usertoken";
            
            query += " and users.companyname like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), 0, status  from transaction "+joinQuery+" "+query + " group by statuscode")
                .getResultList();
        
        return list;
    }
    
    public List getTotalSummaryNoGrouping(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where transaction.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.date_of_transaction) >= date('"+dateFormat.format(startDate)+"') and date(transaction.date_of_transaction) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and (currency='"+currency+"'";
            
            if("NGN".equalsIgnoreCase(currency)){
                query += " or currency is null";
            }
            
            query += ")";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join users on users."+token_string+"=transaction.usertoken";
            
            query += " and users.companyname like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), 0, status from transaction "+joinQuery+" "+query + " ")
                .getResultList();
        
        return list;
    }
    
    public List getSummaryStatus(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where transaction.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.date_of_transaction) >= date('"+dateFormat.format(startDate)+"') and date(transaction.date_of_transaction) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and (currency='"+currency+"'";
            
            if("NGN".equalsIgnoreCase(currency)){
                query += " or currency is null";
            }
            
            query += ")";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join users on users."+token_string+"=transaction.usertoken";
            
            query += " and users.companyname like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select * from (select sum(amount), statuscode, status from transaction "+joinQuery+" "+query + " group by statuscode) as transactions group by statuscode")
                .getResultList();
        
        return list;
    }
    
    public List getReportSummary(Date startDate, Date endDate, String currency, String merchant, MoneywaveUtil.ReportType reportType){
    
        String query, joinQuery = "";
        
        if(reportType == MoneywaveUtil.ReportType.Card){
            query = " where provider <> 'ACCESS BANK NIGERIA PLC' and provider <> 'MVVACCOUNT' and status = 'approved' or status like '%success%'";
        }else{
            query = " where provider = 'Send Money' and (status='approved' or status like '%success%') group by statuscode" ;
        }
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.date_of_transaction) >= date('"+dateFormat.format(startDate)+"') and date(transaction.date_of_transaction) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and (currency='"+currency+"'";
            
            if("NGN".equalsIgnoreCase(currency)){
                query += " or currency is null";
            }
            
            query += ")";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join users on users."+token_string+"=transaction.usertoken";
            
            query += " and users.companyname like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount) from transaction "+joinQuery+" "+query + "")
                .getResultList();
        
        return list;
    }
  
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List getReportSummary( Date startDate, Date endDate,
            String statusCode, String searchReference, String merchant, String currency){
    
        String query = " where transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.date_of_transaction) >= date('"+dateFormat.format(startDate)+"') and date(transaction.date_of_transaction) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(statusCode != null){
            query += " and statuscode='"+statusCode+"'";
        }
        
        if(currency != null){
            query += " and (currency='"+currency+"'";
            
            if("NGN".equalsIgnoreCase(currency)){
                query += " or currency is null";
            }
            
            query += ")";
        }
        
        if(searchReference != null && !"".equals(searchReference)){
            query += " and transactionIdentifier like '%"+searchReference+"%' or transactionReference like '%"+searchReference+"%'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join users on users."+token_string+"=transaction.usertoken";
            
            //query += " and users.companyname like '%"+merchant+"%'";
            query += " and users.userID="+merchant+"";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), date(`date_of_transaction`) as transaction_date, IF(`status` = 'approved' or `status` like '%success%', 'success', 'failed') as t_status, count(*) as volume  from transaction "+joinQuery+" "+ query +" group by `transaction_date`, t_status;")
                .getResultList();
        
        
        return list;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.MerchantLimit;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class MerchantLimitDao extends HibernateDao<MerchantLimit> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public MerchantLimitDao() {
        super(MerchantLimit.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<MerchantLimit> findRaw(Map<String, Object> searchParams){

        if(searchParams.isEmpty())
           return null; 
        
        String query = " where ";
        
        List<String> whereString = new ArrayList<>();
        
        for(Map.Entry<String , Object> entry : searchParams.entrySet()){
            
            if((entry.getValue() instanceof Integer) || (entry.getValue() instanceof Long))
                whereString.add(entry.getKey()+"="+entry.getValue()+" ");
            else if((entry.getValue() instanceof Utility.LimitCategory))
                whereString.add(entry.getKey()+"="+((Utility.LimitCategory)entry.getValue()).ordinal()+" ");
            else    
                whereString.add(entry.getKey()+"=\""+entry.getValue()+"\" ");
        }
        
        query += whereString.stream().collect(Collectors.joining(" and "));
        
        List<MerchantLimit> limits = entityManager.createNativeQuery("select * from merchant_limit "+query, MerchantLimit.class).getResultList();
        
        return limits;
    }
}

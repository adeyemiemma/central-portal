/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.CoreSettlement;
import com.flutterwave.flutter.clientms.model.CoreSettlementMerchant;
import com.flutterwave.flutter.clientms.model.CoreSettlementProvider;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class CoreSettlementDao extends HibernateDao<CoreSettlement> {

    @PersistenceContext(unitName = "CoreSettlementDSPU")
    private EntityManager entityManager;

    public CoreSettlementDao() {
        super(CoreSettlement.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    public List<CoreSettlement> findSummaryByQuery(Date date, String provider, String currency, String status, String merchantId) throws DatabaseException {

        try {
            // This is used to build the criteria
            String query = " where settletransaction.id <> 0 ";

            String joinQuery = "";

            if (date != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                query += " and date(settletransaction.transactiontime) = '" + dateFormat.format(date) + "'";
            }

            if (provider != null) {
                query += " and settletransaction.provider = " + provider;
            }

            if (merchantId != null) {
                query += " and settletransaction.merchantid = '" + merchantId + "'";
            }

            if (currency != null) {
                query += " and settletransaction.currency = '" + currency + "'";
            }

            List<CoreSettlement> response = entityManager.createNativeQuery("select * from settletransaction " + joinQuery + " " + query + " and responsecode='00' and `authmodel`!='BALANCE_ENQUIRY'", entityClass)
                    .getResultList();

            return response;

        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }

    public List<CoreSettlementMerchant> findSummaryMerchantByQuery(Date date, String provider, String currency, String status, String merchantId) throws DatabaseException {

        try {
            // This is used to build the criteria
            String query = " where settletransaction.id <> 0 ";

            String joinQuery = "";

            if (date != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                query += " and date(settletransaction.transactiontime) = '" + dateFormat.format(date) + "'";
            }

            if (provider != null) {
                query += " and settletransaction.provider = " + provider;
            }

            if (merchantId != null) {
                query += " and settletransaction.merchantid = '" + merchantId + "'";
            }
            if (currency != null) {
                query += " and settletransaction.currency = '" + currency + "'";
            }

            List<CoreSettlementMerchant> response = entityManager.createNativeQuery("select id, sum(amount) as total, count(*) as count, merchantname, merchantid, currency from settletransaction " + joinQuery + " " + query + " and responsecode='00' and `authmodel`!='BALANCE_ENQUIRY' group by merchantid, currency", CoreSettlementMerchant.class)
                    .getResultList();

            return response;

        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }
    
    public List<CoreSettlementProvider> findSummaryProviderByQuery(Date date, String provider, String currency, String status, String merchantId) throws DatabaseException {

        try {
            // This is used to build the criteria
            String query = " where settletransaction.id <> 0 ";

            String joinQuery = "";

            if (date != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                query += " and date(settletransaction.transactiontime) = '" + dateFormat.format(date) + "'";
            }

            if (provider != null) {
                query += " and settletransaction.provider = " + provider;
            }

            if (merchantId != null) {
                query += " and settletransaction.merchantid = '" + merchantId + "'";
            }
            if (currency != null) {
                query += " and settletransaction.currency = '" + currency + "'";
            }

            List<CoreSettlementProvider> response = entityManager.createNativeQuery("select id, sum(amount) as total, count(*) as count, provider, currency from settletransaction " + joinQuery + " " + query + " and responsecode='00' and `authmodel`!='BALANCE_ENQUIRY' group by provider, currency", CoreSettlementProvider.class)
                    .getResultList();

            return response;

        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }

}

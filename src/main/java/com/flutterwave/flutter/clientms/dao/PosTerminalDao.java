/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class PosTerminalDao extends HibernateDao<PosTerminal> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public PosTerminalDao() {
        super(PosTerminal.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public PosTerminal findUnassignedTerminal(){
        
        String query = "select * from mpos_terminal where merchant_id is NULL limit 1";
        
        List<PosTerminal> list =  getEntityManager().createNativeQuery(query, PosTerminal.class).getResultList();
        
        if(list == null || list.isEmpty())
            return null;
        
        return list.get(0);
    }
    
    public List<PosTerminal> findTerminaWithBankCode(String prefix){
        
        if(Utility.emptyToNull(prefix) == null)
            return null;
        
        String query = " where mpos_terminal.terminalId like '"+prefix+"%'";
        
        List<PosTerminal> list = entityManager.createNativeQuery("select * from mpos_terminal "+query, entityClass)
                .getResultList();
        
        return list;
    }
    
    public Page<PosTerminal> query(int pageNo, int pageSize, Date startDate, Date endDate,
            String provider, String bank, String searchParam){
        
        String query = "where id <> 0";
        String joinQuery = "";
        
        if(startDate != null)
            query += " and createdOn >= "+Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss");
        
        if(endDate != null)
            query += " and createdOn <= "+Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss");
        
        if (provider != null && !provider.isEmpty()) {
            query += " and mpos_terminal.provider_id ='" + provider + "'";
        }
        
        if (Utility.emptyToNull(bank) != null) {
            query += " and mpos_terminal.bankName ='" + bank + "'";
        }
        
        if(searchParam != null){
            query += " and concat_ws(' ', mpos_terminal.terminalId,mpos_terminal.serialNo) like '%" + searchParam + "%'";
        }
        
        List<PosTerminal> list = null;
        
        if(pageSize == 0)
            list = entityManager.createNativeQuery("select mpos_terminal.* from mpos_terminal " + joinQuery + " " + query + " order by id desc ", PosTerminal.class)
                .setFirstResult(pageNo)
                .getResultList();
        else
            list = entityManager.createNativeQuery("select mpos_terminal.* from mpos_terminal " + joinQuery + " " + query + " order by id desc ", PosTerminal.class)
                .setFirstResult(pageNo)
                .setMaxResults(pageSize)
                .getResultList();
        Object countResult = entityManager.createNativeQuery("select count(*) as count from mpos_terminal " + joinQuery + " " + query).getSingleResult();

        long count = Long.parseLong(countResult + "");

        Page<PosTerminal> page = new Page(count, list);

        return page;
    }
    
    public List<PosTerminal> findByMerchant(PosMerchant value) throws DatabaseException{
       
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<PosTerminal> query = builder.createQuery(entityClass);

            // This used to map the entities to a class and extract the fields involved
            Root<PosTerminal> rootQuery = query.from(entityClass);

            ParameterExpression keyParam = builder.parameter(PosTerminal.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.like(rootQuery.get("merchant"), keyParam)).orderBy(builder.desc(rootQuery.get("id")));

            List<PosTerminal> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, value)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
}

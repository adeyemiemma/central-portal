/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.ChargeBackDispute;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class ChargeBackDisputeDao extends HibernateDao<ChargeBackDispute> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public ChargeBackDisputeDao() {
        super(ChargeBackDispute.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<ChargeBackDispute> find(int pageNumber, int pageSize, Date startDate, Date endDate, String product, 
            long merchantId, String currency, long productMid) throws DatabaseException{
       
        String query = "", joinQuery = "";
        
        query += " where chargeback.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(chargeback.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(chargeback.createdOn) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(product != null){
            joinQuery += " join product on product.id = chargeback.product_id";
            query += " and product.name='"+product+"'";
        }
        
        if(currency != null && !currency.isEmpty()){
            query += " and chargeback.currency='"+currency+"'";
        }
        
        if(merchantId > 0){
            query += " and chargeback.merchantId="+merchantId;
        }
        
        if(productMid > 0){
            query += " and chargeback.productMid="+productMid;
        }
        
        List<ChargeBackDispute> list = entityManager.createNativeQuery("select * from chargeback "+joinQuery+" "+query, ChargeBackDispute.class)
                .setFirstResult(pageNumber)
                .setMaxResults(pageSize)
                .getResultList();
        
        Object object = (Object) entityManager.createNativeQuery("select count(*) from chargeback "+joinQuery+" "+query).getSingleResult();
        
        long count = Long.parseLong(object+"");
        
        Page<ChargeBackDispute> page = new Page<>(count, list);
        
        return page;
    }
    
    public List getReportSummary( Date startDate, Date endDate, String product, 
            long merchantId, String currency, long productMid){
    
        String query = "", joinQuery = "";
        
        query += " where chargeback.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(chargeback.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(chargeback.createdOn) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        joinQuery += " join product on product.id = chargeback.product_id";
        if(product != null){
            query += " and product.name='"+product+"'";
        }
        
        if(currency != null && !currency.isEmpty()){
            query += " and chargeback.currency='"+currency+"'";
        }
        
        if(merchantId > 0){
            query += " and chargeback.merchantId="+merchantId;
        }
        
        if(productMid > 0)
            query += " and chargeback.productMid="+productMid;
        
        List list = entityManager.createNativeQuery("select sum(amount), date(chargeback.createdOn) as transaction_date, count(*) as volume, currency, product.name  from chargeback "+joinQuery+" "+ query +" group by `transaction_date`, currency")
                .getResultList();
        
        
        return list;
    }
    
    public Map<String, Object> getSummary(Date startDate, Date endDate, String product, 
            String merchantId, String currency, long productMid) throws DatabaseException{
       
        String query = "", joinQuery = "";
        
        query += " where chargeback.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(chargeback.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(chargeback.createdOn) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(product != null){
            joinQuery += " join product on product.id = chargeback.product_id";
            query += " and product.name='"+product+"'";
        }
        
        if(currency != null && !currency.isEmpty()){
            query += " and chargeback.currency='"+currency+"'";
        }
        
        if(merchantId != null){
            query += " and chargeback.merchantId='"+merchantId+"'";
        }
        
        if(productMid > 0){
            query += " and chargeback.productMid="+productMid;
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(amountSettled) from chargeback "+joinQuery+" "+query)
                .getResultList();
        
        Map<String, Object> record = new HashMap<>();
        record.put("amount", list);
        
        Object obj = entityManager.createNativeQuery("select count(distinct(productMid)) from chargeback "+joinQuery+" "+query)
                .getSingleResult();
        
        record.put("merchantcount", obj);
        
        return record;
    }
    
    public List findSummaryByProvider(Date startDate, Date endDate,String providerName, String currency){
        
        String query = "", joinQuery = "";
        
        query += " where chargeback.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(chargeback.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(chargeback.createdOn) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null && !currency.isEmpty()){
            query += " and chargeback.currency='"+currency+"'";
        }
        
        if(providerName != null && !providerName.isEmpty()){
            joinQuery += " join transaction on transaction.flwTxnReference = chargeback.flwReference ";
            query += " and transaction.provider = '"+providerName+"'";
        }
        
        List list = entityManager.createNativeQuery("select sum(transaction.amount), sum(amountSettled) from chargeback "+joinQuery+" "+query)
                .getResultList();
        
        
        return list;
    }
    
    public List<ChargeBackDispute> findByProduct(int pageNum, int pageSize, Date startDate, Date endDate,String product, String currency){
        
        String query = "", joinQuery = "";
        
        query += " where chargeback.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(chargeback.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(chargeback.createdOn) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(product != null){
            joinQuery += " join product on product.id = chargeback.product_id";
            query += " and product.name='"+product+"'";
        }
        
        if(currency != null && !currency.isEmpty()){
            query += " and chargeback.currency='"+currency+"'";
        }
        
        List<ChargeBackDispute> list = entityManager.createNativeQuery("select * from chargeback "+joinQuery+" "+query, ChargeBackDispute.class)
                .setFirstResult(pageNum)
                .setMaxResults(pageSize)
                .getResultList();
        
        
        return list;
    }
    
}

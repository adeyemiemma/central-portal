/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.RewardTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jboss.annotation.ejb.TransactionTimeout;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class RewardTransactionDao extends HibernateDao<RewardTransaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public RewardTransactionDao() {
        super(RewardTransaction.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    @TransactionTimeout(value = 4800)
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean create(List<RewardTransaction> data) throws DatabaseException{
        
        try{           
            
            int index = 0;
            for(RewardTransaction t : data){
                
                try{
                    
                    boolean exists = exists(t.getTransactionReference());
                    
                    if(exists == true)
                        continue;
                    
                    getEntityManager().persist(t);
                }catch(Exception ex){
                }
                
                index++;
                
                if(index % 100 == 0){
                    getEntityManager().flush();
                    getEntityManager().clear();
                }
            }
            
            getEntityManager().flush();
            getEntityManager().clear();
            
            return true;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    @TransactionTimeout(value = 4800)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean createNoCheck(List<RewardTransaction> data) throws DatabaseException{
        
        try{           
            
            int index = 0;
            for(RewardTransaction t : data){
                
                try{
                    getEntityManager().persist(t);
                }catch(Exception ex){
                }
                
                index++;
                
                if(index % 100 == 0){
                    getEntityManager().flush();
                    getEntityManager().clear();
                }
            }
            
            getEntityManager().flush();
            getEntityManager().clear();
            
            return true;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
     
     
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED) 
    public boolean exists(String reference){
        
        String query = "select id,transactionReference from reward_transaction where transactionReference='"+reference+"'";
        
        List result = getEntityManager().createNativeQuery(query).getResultList();
        
        if(result == null)
            return false;
        
        return !result.isEmpty();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.maker;

import com.flutterwave.flutter.clientms.dao.*;
import com.flutterwave.flutter.clientms.model.maker.MerchantConfigM;
import com.flutterwave.flutter.clientms.util.Global;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class MerchantConfigMDao extends BaseHibernateDao<MerchantConfigM> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public MerchantConfigMDao() {
        super(MerchantConfigM.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
}

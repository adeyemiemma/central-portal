/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.SettlementCycle;
import com.flutterwave.flutter.clientms.model.RaveTransaction;
import com.flutterwave.flutter.clientms.model.SettlementCycle;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class SettlementCycleDao extends HibernateDao<SettlementCycle> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public SettlementCycleDao() {
        super(SettlementCycle.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<SettlementCycle> find(int pageNum, int pageSize, String product, String merchantId){
        
        Page<SettlementCycle> page = new Page<>();
        
        String query = " where settlement_cycle.id <> 0";
        
        String joinQuery = "";
        
        if(product != null)
            query += " and settlement_cycle.product='"+product+"'";
        
        if(merchantId != null)
            query += " and settlement_cycle.merchantId='"+merchantId+"'";
        
        List<SettlementCycle> transactions;
        
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select settlement_cycle.* from settlement_cycle "+joinQuery+" "+query, SettlementCycle.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = entityManager.createNativeQuery("select settlement_cycle.* from settlement_cycle "+joinQuery+" "+query +"", SettlementCycle.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from settlement_cycle "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);
        
        return page;
        
    }
}

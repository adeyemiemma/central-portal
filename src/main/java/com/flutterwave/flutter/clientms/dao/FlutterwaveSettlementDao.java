/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.FlutterwaveSettlement;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.Settlement;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class FlutterwaveSettlementDao extends HibernateDao<FlutterwaveSettlement> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public FlutterwaveSettlementDao() {
        super(FlutterwaveSettlement.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<FlutterwaveSettlement> findBySettlement(Settlement settlement)
        throws DatabaseException{
        
        if(settlement == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<FlutterwaveSettlement> query = builder.createQuery(FlutterwaveSettlement.class);

            // This used to map the entities to a class and extract the fields involved
            Root<FlutterwaveSettlement> rootQuery = query.from(FlutterwaveSettlement.class);

            ParameterExpression keyParam = builder.parameter(Settlement.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("settlement"), keyParam));

            List<FlutterwaveSettlement> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, settlement)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    public List<FlutterwaveSettlement> findByTransactionStatus(Utility.SettlementState state)
        throws DatabaseException{
        
        if(state == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<FlutterwaveSettlement> query = builder.createQuery(FlutterwaveSettlement.class);

            // This used to map the entities to a class and extract the fields involved
            Root<FlutterwaveSettlement> rootQuery = query.from(FlutterwaveSettlement.class);

            ParameterExpression keyParam = builder.parameter(Utility.SettlementState.class);
            
            Predicate predicate = builder.equal(rootQuery.get("transactionState"), keyParam);
            predicate = builder.or(predicate, builder.isNull(rootQuery.get("transactionState")));
            
//            Predicate predicate = builder.isNull(keyParam);
//            builder.or(predicate, builder.equal(rootQuery.get("transactionState"), keyParam));

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(predicate);

            List<FlutterwaveSettlement> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, state)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class MerchantFeeDao extends HibernateDao<MerchantFee> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public MerchantFeeDao() {
        super(MerchantFee.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<MerchantFee> findRaw(Map<String, Object> searchParams){

        if(searchParams.isEmpty())
           return null; 
        
        String query = " where ";
        
        for(Map.Entry<String , Object> entry : searchParams.entrySet()){
            
            if(!(entry.getValue() instanceof String))
                query += entry.getKey()+"="+entry.getValue()+"";
            else 
                query += entry.getKey()+"='"+entry.getValue()+"'";
        }
        
        List<MerchantFee> fees = entityManager.createNativeQuery("select * from merchant_fee "+query, MerchantFee.class).getResultList();
        
        return fees;
    }
    
    public MerchantFee findByProductAndMerchant(Product product, long merchantId ){
        
        String query = " where merchant_fee.id <> 0 ";
        
        if(product == null || merchantId <= 0)
            return null;
        
        String joinQuery = "";
        
        joinQuery += " join product on merchant_fee.productid = product.id ";
        query += " and merchant_fee.productid = "+product.getId();
        
        if(merchantId > 0){
            
            query += " and merchant_fee.merchantId="+merchantId;
        }
        
        List<MerchantFee> fees = entityManager.createNativeQuery("select * from merchant_fee "+joinQuery+" "+query, MerchantFee.class).getResultList();
        
        return fees == null ? null : fees.isEmpty() ? null : fees.get(0);
    }
}

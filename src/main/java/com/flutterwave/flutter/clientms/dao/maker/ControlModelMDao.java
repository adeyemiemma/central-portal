/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.maker;

import com.flutterwave.flutter.clientms.dao.BaseHibernateDao;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.maker.ProductM;
import com.flutterwave.flutter.clientms.model.maker.ControlModelM;
import com.flutterwave.flutter.clientms.util.Global;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class ControlModelMDao extends BaseHibernateDao<ControlModelM> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public ControlModelMDao() {
        super(ControlModelM.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
}

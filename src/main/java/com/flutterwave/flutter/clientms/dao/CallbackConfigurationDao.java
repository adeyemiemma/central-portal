/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.CallbackConfiguration;
import com.flutterwave.flutter.clientms.model.Configuration;
import com.flutterwave.flutter.clientms.util.Global;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author adeyemi
 */
@Stateless
public class CallbackConfigurationDao extends com.flutterwave.flutter.core.dao.HibernateDao<CallbackConfiguration> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public CallbackConfigurationDao() {
        super(CallbackConfiguration.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }  
    
    public List<CallbackConfiguration> getConfiguration(String type, String merchantId){
        
        String query = " where id <> 0";
        
        if(type != null)
            query += " and `type`= '"+type+"'";
        
        if(merchantId != null)
            query += " and `merchantId`= '"+merchantId+"'";
        
        List<CallbackConfiguration> configurations = getEntityManager().createNativeQuery("select * from callback_configuration "+query, entityClass).getResultList();
        
        return configurations;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.Settlement;
import com.flutterwave.flutter.clientms.model.SettlementReportModel;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.TransactionListModel;
import com.flutterwave.flutter.clientms.model.TransactionViewModel;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.FetchRRNModel;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class TransactionDao extends HibernateDao<Transaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public TransactionDao() {
        super(Transaction.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    public List<Transaction> findByKeyLike(String key, String value) throws DatabaseException {

        try {
            // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<Transaction> query = builder.createQuery(entityClass);

            // This used to map the entities to a class and extract the fields involved
            Root<Transaction> rootQuery = query.from(entityClass);

            ParameterExpression keyParam = builder.parameter(Long.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.like(rootQuery.get(key), keyParam)).orderBy(builder.desc(rootQuery.get("id")));

            List<Transaction> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, value)
                    .getResultList();

            if (list.isEmpty()) {
                return null;
            }

            return list;
        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }
    
    public List<TransactionViewModel> findMiniTransaction(String key, String value) throws DatabaseException {

        try {
            
            String query = " where "+key+" like '"+value+"'";
            
            List<TransactionViewModel> list = entityManager.createNativeQuery("select id, rrn, provider, merchantId,transactionCurrency, transactionCountry, amount, merchantTxnReference, flwTxnReference, transactionType, cardCountry, cardMask, datetime, responseCode,responseMessage, createdOn, "
                + "cardScheme,authenticationModel,transactionNarration from transaction " + query + " order by datetime desc ", TransactionViewModel.class)
                .getResultList();
            
            if(list == null || list.isEmpty())
                return null;
            
            return list;
            
        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }
    
    public TransactionViewModel findTransaction(String reference, String date) throws DatabaseException {

        try {
            
            String query = " where flwTxnReference = '"+reference+"' and date(datetime) = date('"+date+"')";
            
            List<TransactionViewModel> list = entityManager.createNativeQuery("select id, rrn, provider, merchantId,transactionCurrency, transactionCountry, amount, merchantTxnReference, flwTxnReference, transactionType, cardCountry, cardMask, datetime, responseCode,responseMessage, createdOn, "
                + "cardScheme,authenticationModel,transactionNarration from transaction " + query + " order by datetime desc ", TransactionViewModel.class)
                .getResultList();
            
            if(list == null || list.isEmpty())
                return null;
            
            return list.get(0);
            
        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }
    
    public TransactionViewModel findTransactionLike(String reference, String date) throws DatabaseException {

        try {
            
            String query = " where flwTxnReference like '%"+reference+"' and date(datetime) = date('"+date+"')";
            
            List<TransactionViewModel> list = entityManager.createNativeQuery("select id, rrn, provider, merchantId,transactionCurrency, transactionCountry, amount, merchantTxnReference, flwTxnReference, transactionType, cardCountry, cardMask, datetime, responseCode,responseMessage, createdOn, "
                + "cardScheme,authenticationModel,transactionNarration from transaction " + query + " order by datetime desc ", TransactionViewModel.class)
                .getResultList();
            
            if(list == null || list.isEmpty())
                return null;
            
            return list.get(0);
            
        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }

    public Transaction FindRefundTransaction(String transactionReference) {
        String query = " where transaction.id <> 0 and responsecode='00' and transactionType = 'CARD_REFUND'";

        List<Transaction> list = entityManager.createNativeQuery("select * from transaction " + query + "", Transaction.class)
                .getResultList();

        return list == null ? null : list.get(0);
    }
    
    public Transaction findTransactionMinimalLike(String transactionReference) {
        String query = " where transaction.flwTxnReference like '%"+transactionReference+"%'";

        List<Transaction> list = entityManager.createNativeQuery("select   from transaction " + query + "", Transaction.class)
                .getResultList();

        return list == null ? null : list.get(0);
    }

    public List<Transaction> getUnsettledPendingTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and settlementState <> 2 and (settled=0 or settled is NULL) and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<Transaction> list = entityManager.createNativeQuery("select * from transaction " + query + "", Transaction.class)
                .getResultList();

        return list;
    }

    public List<Transaction> getSettleableTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<Transaction> list = entityManager.createNativeQuery("select * from transaction " + query + "", Transaction.class)
                .getResultList();

        return list;
    }
    
    public List<Transaction> getAllSettleableTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = "  where transaction.id <> 0 ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and (transaction.datetime >='" + dateTimeFormat.format(startDate) + "' and transaction.datetime <= '" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }
        
        query += " and responsecode='00' and (transactionType in ('CARD_CAPTURE', 'Capture' ,'CARD_PURCHASE' ,'CARD_REFUND' ,'ACCOUNT_PURCHASE', 'EBILLS') )";

        List<Transaction> list = entityManager.createNativeQuery("select * from transaction force index (id_mnm_datetime) " + query + "", Transaction.class)
                .getResultList();

        return list;
    }

    public List<Transaction> getUnsettledTransanctions(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and (settled=0 or settled is NULL) and responsecode='00' and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE' OR transactionType='CARD_REFUND') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<Transaction> list = entityManager.createNativeQuery("select * from transaction " + query + "", Transaction.class)
                .getResultList();

        return list;
    }

    public List<Transaction> getUnsettledTransanctionAccount(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and "
                + "(settled=0 or settled is NULL) and responsecode='00' and (transactionType='ACCOUNT_PURCHASE') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<Transaction> list = entityManager.createNativeQuery("select * from transaction " + query + "", Transaction.class)
                .getResultList();

        return list;
    }

    public List<Transaction> getUnsettledTransanctionEbils(Date startDate, Date endDate, Provider provider, String currency, String merchantId) {

        String query = " where transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and "
                + "(settled=0 or settled is NULL) and responsecode='00' and (transactionType='EBILLS') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//             query +=" and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('"+dateTimeFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateTimeFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateTimeFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider.getShortName() + "'";
        }

        if (merchantId != null) {

            query += " and transaction.merchantId ='" + merchantId + "'";
        }

        List<Transaction> list = entityManager.createNativeQuery("select * from transaction " + query + "", Transaction.class)
                .getResultList();

        return list;
    }
    
    
    public List<SettlementReportModel> getUnsettledSettlement(Date startDate, Date endDate, Provider provider, String currency) {

        String query = " where transaction.id <> 0 and (settlementState = 2 or settlementState is NULL) and (settled=0 or settled is NULL) and responsecode='00' "
                + "and (transactionType='CARD_CAPTURE' OR transactionType='Capture' OR transactionType='CARD_PURCHASE') ";
//        String joinQuery = "";

        if (startDate != null) {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider_id =" + provider.getId() + "";
        }

        List<SettlementReportModel> list = entityManager.createNativeQuery("select * from transaction " + query + " group by merchantId", SettlementReportModel.class)
                .getResultList();

        return list;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<Transaction> getTransactions(int pageNum, int pageSize, Date startDate, Date endDate, String reference,
            String currency, String provider, String merchantId, String category, String statusCode, String type, String cardScheme, String disbursementCode, Boolean isRefund) {

        String query = " where transaction.id <> 0 ";

        if (isRefund != null) {
            if (!isRefund) {
                query += " and transactionType <> 'CARD_REFUND'";
            } else {
                query += " and transactionType = 'CARD_REFUND'";
            }
        }
//        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateFormat.format(endDate) + "')";
        }

        if (cardScheme != null) {
            query += " and cardscheme = '" + cardScheme + "'";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (merchantId != null) {
            query += " and transaction.merchantId='" + merchantId + "'";
        }

        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (statusCode != null) {
            query += " and transaction.responseCode='" + statusCode + "'";
        }

        if (disbursementCode != null) {
            query += " and transaction.disburseResponseCode='" + disbursementCode + "'";
        }

//        if(category != null || type != null ){
//            query += " (";
//        }
//        
        if (category != null) {
            query += " and transaction.transactionType like '" + category + "%'";
        }

        if (type != null) {
            query += " and transaction.transactionType='" + type + "'";
        }

//        if(category != null || type != null ){
//            query += " )";
//        }
//        
        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (reference != null) {
            query += " and ( transaction.flwTxnReference = '" + reference + "' or transaction.merchantTxnReference = '" + reference + "' or transaction.rrn = '" + reference + "' or transaction.cardMask = '" + reference + "'  )";
        }

        List<Transaction> list = entityManager.createNativeQuery("select id, rrn, merchantId,transactionCurrency, transactionCountry, amount, merchantTxnReference, flwTxnReference, transactionType, cardCountry, cardMask, datetime, responseCode,responseMessage, transactionData, processorReference, createdOn, "
                + "cardScheme, disburseResponseCode, disburseResponseMessage from transaction " + query + " order by datetime desc ", Transaction.class)
                .setFirstResult(pageNum)
                .setMaxResults(pageSize)
                .getResultList();

        Object countResult = entityManager.createNativeQuery("select count(1) as count from transaction " + query + "").getSingleResult();

        long count = Long.parseLong(countResult + "");

        Page<Transaction> transactions = new Page(count, list);

        return transactions;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<TransactionListModel> getTransactionsList(int pageNum, int pageSize, Date startDate, Date endDate, String searchKey, String reference,
            String currency, String provider, String merchantId, String category, String statusCode, String type, String cardScheme, String disbursementCode, boolean noCount, Boolean isRefund) {

        String query = " where transaction.id <> 0 ";

        if (isRefund != null) {
            if (!isRefund) {
                query += " and transactionType <> 'CARD_REFUND'";
            } else {
                query += " and transactionType = 'CARD_REFUND'";
            }
        }
//        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and transaction.datetime >= '" + dateFormat.format(startDate) + "' and transaction.datetime <= '" + dateFormat.format(endDate) + "'";
        }

        if (cardScheme != null) {
            query += " and cardscheme = '" + cardScheme + "'";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (merchantId != null) {
            query += " and transaction.merchantId='" + merchantId + "'";
        }

        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (statusCode != null) {
            query += " and transaction.responseCode='" + statusCode + "'";
        }

        if (disbursementCode != null) {
            query += " and transaction.disburseResponseCode='" + disbursementCode + "'";
        }

//        if(category != null || type != null ){
//            query += " (";
//        }
//        
        if (category != null) {
            query += " and transaction.transactionType like '" + category + "%'";
        }

        if (type != null) {
            query += " and transaction.transactionType='" + type + "'";
        }

//        if(category != null || type != null ){
//            query += " )";
//        }
//        
        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (Utility.emptyToNull(reference) != null && Utility.emptyToNull(searchKey) != null) {
            
            query += " and transaction."+searchKey+" like '%"+reference+"%' ";
            //query += " and ( transaction.flwTxnReference = '" + reference + "' or transaction.merchantTxnReference = '" + reference + "' or transaction.rrn = '" + reference + "' or transaction.cardMask = '" + reference + "'  )";
        }

        List<TransactionListModel> list = entityManager.createNativeQuery("select id, rrn, provider, merchantId,transactionCurrency, transactionCountry, amount, merchantTxnReference, flwTxnReference, transactionType, cardCountry, cardMask, datetime, responseCode,responseMessage, transactionData, processorReference, createdOn, "
                + "cardScheme, disburseResponseCode, disburseResponseMessage, disburseRetryCount, \"none\" as paymentId from transaction " + query + " order by datetime desc ", TransactionListModel.class)
                .setFirstResult(pageNum)
                .setHint("org.hibernate.cacheable", true)
                .setMaxResults(pageSize)
                .getResultList();

        long count = 0;
        
        if(noCount == false){
            Object countResult = entityManager.createNativeQuery("select count(1) from transaction " + query + "").getSingleResult();

            count = Long.parseLong(countResult + "");
        }

        Page<TransactionListModel> transactions = new Page(count, list);

        return transactions;
    }

    public Map<String, Object> getTransactionSummary(Date startDate, Date endDate, String reference,
            String currency, String provider, String merchantId, Boolean isRefund) {

        String query = " where transaction.id <> 0 ";

        if (isRefund != null) {
            if (!isRefund) {
                query += " and transactionType <> 'CARD_REFUND'";
            } else {
                query += " and transactionType = 'CARD_REFUND'";
            }
        }
//        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and TIMESTAMP(transaction.datetime) >= TIMESTAMP('" + dateFormat.format(startDate) + "') and TIMESTAMP(transaction.datetime) <= TIMESTAMP('" + dateFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (merchantId != null) {
            query += " and transaction.merchantId='" + merchantId + "'";
        }

        if (provider != null) {
            query += " and transaction.provider ='" + provider + "'";
        }

        if (reference != null) {
            query += " and (transaction.flwTxnReference = '" + reference + "' or transaction.merchantTxnReference = '" + reference + "' )";
        }

        Object result = entityManager.createNativeQuery("select sum(amount) from transaction " + query + "").getSingleResult();

        Object countResult = entityManager.createNativeQuery("select count(distinct(merchantId)) as count from transaction " + query + " ").getSingleResult();

        Map<String, Object> map = new HashMap<>();

        map.put("amount", result);
        map.put("merchant", countResult);

        return map;
    }

    public List<Transaction> findBySettlement(Settlement settlement)
            throws DatabaseException {

        if (settlement == null) {
            return null;
        }

        try {
            // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<Transaction> query = builder.createQuery(Transaction.class);

            // This used to map the entities to a class and extract the fields involved
            Root<Transaction> rootQuery = query.from(Transaction.class);

            ParameterExpression keyParam = builder.parameter(Settlement.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("settlement"), keyParam));

            List<Transaction> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, settlement)
                    .getResultList();

            if (list.isEmpty()) {
                return null;
            }

            return list;
        } catch (Exception ex) {
            throw new DatabaseException(ex);
        }
    }

    public List getSummaryWithDate(Date startDate, Date endDate, String currency, String provider) {

        String query = " where transaction.id <> 0 and (settled=0 or settled = NULL) and responsecode='00' ";
        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query += " and date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')) >= date('" + dateFormat.format(startDate) + "') and date(transaction.datetime) <= date('" + dateFormat.format(endDate) + "')";
        }

        if (currency != null) {
            query += " and transaction.transactionCurrency='" + currency + "'";
        }

        if (provider != null) {
//            joinQuery += " join pro on merchants.id=transaction.merchantId";

            query += " and transaction.provider ='" + provider + "'";
        }

        List list = entityManager.createNativeQuery("select sum(amount), 0 ,count(1), date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')), responseCode, responseMessage from transaction " + joinQuery + " " + query + " group by date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')), responseCode")
                .getResultList();

        return list;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public boolean exists(String param, String value) {

        List list = getEntityManager().createNativeQuery("select * from transaction where " + param + "='" + value + "' limit 1").getResultList();

        if (list == null) {
            return false;
        }

        return list.size() > 0;
    }

    public List getSummaryWithDate(Date startDate, Date endDate) {

        String query = " where transaction.id <> 0 and responsecode='00' ";
        String joinQuery = "";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query += " and date(transaction.datetime) >= date('" + dateFormat.format(startDate) + "') and date(transaction.datetime) <= date('" + dateFormat.format(endDate) + "')";
        }

        List list = entityManager.createNativeQuery("select sum(amount), count(1), transactionCurrency,  merchantId, cardCountry from transaction " + joinQuery + " " + query + " group by date(STR_TO_DATE(transaction.datetime, '%Y-%m-%d')), transactionCurrency, merchantId")
                .getResultList();

        return list;
    }

    public List getTransactionSummarybyMonth(Date startDate, Date endDate) {

        String query = " where transaction.id <> 0 and responsecode='00' ";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query += " and date(transaction.datetime) >= date('" + dateFormat.format(startDate) + "') and date(transaction.datetime) <= date('" + dateFormat.format(endDate) + "')";
        }

        List list = entityManager.createNativeQuery("select sum(amount), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d') from transaction " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency ")
                .getResultList();

        return list;
    }

    public Map<String, String> getResponse(String category) {

        String whereClause = "";

        if (category != null && !"".equals(category)) {
            whereClause += " where t.transactionType like ?1";
        }

        Query query = entityManager.createQuery("SELECT distinct(t.responseCode), t.responseMessage FROM Transaction as t " + whereClause + " group by t.responseCode");
        
        List list;
        
        if (category != null && !"".equals(category)) {
            query.setParameter(1, category+"%");
        }

        list = query.setHint("org.hibernate.cacheable", true)
                .getResultList();
        
        final Map<String, String> responses = new TreeMap<>();

        list.stream().forEachOrdered((data) -> {

            Object[] x = (Object[]) data;

            responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });

        return responses;
    }
    
    public Map<String, String> getResponseNew(String category) {

        String whereClause = "";

        if (category != null && !"".equals(category)) {
            whereClause += " where transactionType like '"+category+"%' ";
        }

        Query query = entityManager.createNativeQuery("SELECT distinct(responseCode),responseMessage FROM transaction force index (`index_transactionType`) " + whereClause + " group by responseCode");
        
        List list;
        
//        if (category != null && !"".equals(category)) {
//            query.setParameter(1, category+"%");
//        }

        list = query.getResultList();
        
        final Map<String, String> responses = new TreeMap<>();

        list.stream().forEachOrdered((data) -> {

            Object[] x = (Object[]) data;

            responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });

        return responses;
    }

    public Map<String, String> getDisburseResponse() {

        List list = entityManager.createNativeQuery("select disburseResponseCode, disburseResponseMessage from transaction group by disburseResponseCode").getResultList();

        final Map<String, String> responses = new TreeMap<>();

        list.stream().forEachOrdered((data) -> {

            Object[] x = (Object[]) data;

            responses.put(String.valueOf(x[1]), String.valueOf(x[0]));
        });

        return responses;
    }

    public List<String> getTransactionType(String category) {

        List list = entityManager.createNativeQuery("select transactionType from transaction group by transactionType").getResultList();

        final List<String> responses = new ArrayList<>();

        list.stream().filter(x -> category == null ? x != null : x.toString().toLowerCase().startsWith(category))
                .forEachOrdered((data) -> {

                    responses.add(String.valueOf(data));
                });

        return responses;
    }

    public List getTransactionByMonth(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date startDate = calendar.getTime();

        calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date endDate = calendar.getTime();

        String query = " where transaction.id <> 0 and responsecode='00' ";

        if (startDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query += " and date(transaction.datetime) >= date('" + dateFormat.format(startDate) + "') and date(transaction.datetime) <= date('" + dateFormat.format(endDate) + "')";
        }

        List list = entityManager.createNativeQuery("select sum(amount), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d') from transaction " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency; ")
                .getResultList();

        return list;
    }

    public List getReport(Date startDate, Date endDate, boolean successful, boolean card) {

        String format = "yyyy-MM-dd";

        String startDateString = Utility.formatDate(startDate, format);
        String endDateString = Utility.formatDate(endDate, format);

        String query = " where transaction.id <> 0 and  " + (successful == true ? " responsecode = '00'" : "responsecode <> '00' ");

        if (card == true) {
            query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE')";
        } else {
            query += " and transactionType = 'ACCOUNT_PURCHASE'";
        }

        query += " and date(transaction.datetime) >= date('" + startDateString + "') and date(transaction.datetime) <= date('" + endDateString + "')";

        List result = entityManager.createNativeQuery("select sum(amount), count(1), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d') from transaction " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency ")
                .getResultList();

        return result;
    }

    public List getFailureReason(Date startDate, Date endDate, boolean card) {

        String format = "yyyy-MM-dd";

        String startDateString = Utility.formatDate(startDate, format);
        String endDateString = Utility.formatDate(endDate, format);

        String query = " where transaction.id <> 0 and responsecode <> '00'";

        if (card == true) {
            query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE')";
        } else {
            query += " and transactionType = 'ACCOUNT_PURCHASE'";
        }

        query += " and date(transaction.datetime) >= date('" + startDateString + "') and date(transaction.datetime) <= date('" + endDateString + "')";

        List result = entityManager.createNativeQuery("select sum(amount), count(1), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d'), responseMessage, cardScheme from transaction " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency,responsecode, cardScheme ")
                .getResultList();

        return result;
    }

    public List getSuccessAnalysis(Date startDate, Date endDate, boolean card) {

        String format = "yyyy-MM-dd";

        String startDateString = Utility.formatDate(startDate, format);
        String endDateString = Utility.formatDate(endDate, format);

        String query = " where transaction.id <> 0 and responsecode = '00'";

        if (card == true) {
            query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE')";
        } else {
            query += " and transactionType = 'ACCOUNT_PURCHASE'";
        }

        query += " and date(transaction.datetime) >= date('" + startDateString + "') and date(transaction.datetime) <= date('" + endDateString + "')";

        List result = entityManager.createNativeQuery("select sum(amount), count(1), transactionCurrency, DATE_FORMAT(`datetime`,'%Y-%m-%d'), cardScheme from transaction " + query + " group by DATE_FORMAT(`datetime`,'%Y-%m-%d'),transactionCurrency, cardScheme ")
                .getResultList();

        return result;
    }

    public List<String> getDistinctMerchant(Date startDate, Date endDate) {

        String format = "yyyy-MM-dd";

        String startDateString = Utility.formatDate(startDate, format);
        String endDateString = Utility.formatDate(endDate, format);

        String query = " where transaction.id <> 0 and responsecode = '00'";

        query += " and (transactionType = 'CARD_PURCHASE' or transactionType = 'CARD_CAPTURE' or transactionType = 'ACCOUNT_PURCHASE')";

        query += " and date(transaction.datetime) >= date('" + startDateString + "') and date(transaction.datetime) <= date('" + endDateString + "')";

        List result = entityManager.createNativeQuery("select distinct(merchantId) from transaction " + query + "")
                .getResultList();

        return result;
    }
    
    public FetchRRNModel getRRNModel(String rrn){
    
        if(rrn == null)
            return null;
        
        String query = " where transaction.rrn = '"+rrn+"'";
    
        List<FetchRRNModel> model = entityManager.createNativeQuery("select id, amount,merchantTxnReference,flwTxnReference as flutterReference, rrn, datetime as transactionDate,"
                + " transactionCurrency as currency, merchantId, merchantId as merchantName from transaction"+query, FetchRRNModel.class).getResultList();
        
        if(model == null || model.isEmpty())
            return null;
        
        return model.get(0);
    }
    
    public List<FetchRRNModel> getRRNModels(String rrns){
    
        if(rrns == null)
            return null;
        
        String query = " where transaction.rrn in ("+rrns+")";
    
        List<FetchRRNModel> model = entityManager.createNativeQuery("select id, amount,merchantTxnReference, flwTxnReference as flutterReference, rrn, datetime as transactionDate,"
                + " transactionCurrency as currency,merchantId, merchantId as merchantName, responseCode, responseMessage from transaction"+query, FetchRRNModel.class).getResultList();
        
        if(model == null || model.isEmpty())
            return null;
        
        return model;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List getReportSummary( Date startDate, Date endDate,
            String statusCode, String searchReference, String merchant, String currency){
    
        String query = " where transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transaction.datetime) >= date('"+dateFormat.format(startDate)+"') and date(transaction.datetime) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(statusCode != null){
            query += " and responseCode='"+statusCode+"'";
        }
        
        if(currency != null){
            query += " and transactionCurrency='"+currency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            
            //query += " and users.companyname like '%"+merchant+"%'";
            query += " and merchantId="+merchant+"";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), date(`datetime`) as transaction_date, IF(`responseCode` = '00', 'success', 'failed') as t_status, count(*) as volume  from transaction "+joinQuery+" "+ query +" group by `transaction_date`, t_status;")
                .getResultList();
        
        
        return list;
    }
}

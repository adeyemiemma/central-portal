/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.AirtimeSettlement;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class AirtimeSettlementDao extends HibernateDao<AirtimeSettlement> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public AirtimeSettlementDao() {
        super(AirtimeSettlement.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public AirtimeSettlement findByKey(String key, String value) throws DatabaseException{
        
        try{
            
            if(value == null || key == null)
                return null;
            
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<AirtimeSettlement> query = builder.createQuery(entityClass);

            // This used to map the entities to a class and extract the fields involved
            Root<AirtimeSettlement> rootQuery = query.from(entityClass);

            ParameterExpression keyParam = builder.parameter(String.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get(key), keyParam));

            List<AirtimeSettlement> list = getEntityManager().createQuery(query)
                    .setHint("org.hibernate.cacheMode", "IGNORE")
                    .setParameter(keyParam, value)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list.get(0);
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public boolean exists(String reference){
        
        String query = "select id from airtime_settlement_txn where reference='"+reference+"'";
        
        List result = getEntityManager().createNativeQuery(query).getResultList();
        
        if(result == null)
            return false;
        
        return !result.isEmpty();
    }
    
    
    
    public Map<String, List> getSummary(String fileName, Date date){
        
        
        Map<String, List> summary = new HashMap<>();
        
        String query = "select sum(amount), count(*) from airtime_settlement_txn where fileName='"+fileName+"' "
                + "and date(createdOn)=date("+Utility.formatDate(date, "yyyy-MM-dd")+") && settled=1";
        
        List result = entityManager.createNativeQuery(query).getResultList();
        
        summary.put("settled", result);
        
        query = "select sum(amount), count(*) from airtime_settlement_txn where fileName='"+fileName+"' "
                + "and date(createdOn)=date("+Utility.formatDate(date, "yyyy-MM-dd")+") && settled=0";
        
        result = entityManager.createNativeQuery(query).getResultList();
        
        summary.put("unsettled", result);
        
        return summary;
        
    }
}

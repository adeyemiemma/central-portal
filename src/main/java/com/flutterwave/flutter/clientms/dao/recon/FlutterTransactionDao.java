/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.recon;

import com.flutterwave.flutter.clientms.model.recon.FlutterTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class FlutterTransactionDao extends com.flutterwave.flutter.core.dao.HibernateDao<FlutterTransaction>{

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;
    
    public FlutterTransactionDao() {
        super(FlutterTransaction.class);
    }

    @Override
    public EntityManager getEntityManager() {
        
        return entityManager;
    }
    
    public List<FlutterTransaction> findApproved(String rrn){
        
//        String query = " where (vpctxnresponsecode='0' or vpctxnresponsecode='0-Y') and vpcreceiptno='"+rrn+"'";
        String query = " where vpcreceiptno='"+rrn+"'";
       
        List<FlutterTransaction> transactions ;
        
        transactions = (List<FlutterTransaction>)entityManager.createNativeQuery("select * from flutter_transaction "+query, FlutterTransaction.class)
                .getResultList();
        
        return transactions;
    }
    
    public com.flutterwave.flutter.core.util.Page<FlutterTransaction> getTransaction(String column, Date data, int pageSize, int pageNum){
        
        String query = " where date("+column+")="+TransactionUtility.formatDateToString(data);
        
        Page<FlutterTransaction> page = new Page<>();
        
        List<FlutterTransaction> transactions ;
        
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select * from flutter_transaction "+query, FlutterTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<FlutterTransaction>)entityManager.createNativeQuery("select * from flutter_transaction "+query +"", FlutterTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from flutter_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    public Page<FlutterTransaction> getTransaction(String column, long data, int pageSize, int pageNum){
        
        String query = " where "+column+"="+data;
        
        Page<FlutterTransaction> page = new Page<>();
        
        List<FlutterTransaction> transactions ;
        
        if(pageSize > 0 )
            transactions = (List<FlutterTransaction>)entityManager.createNativeQuery("select * from flutter_transaction "+query, FlutterTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<FlutterTransaction>)entityManager.createNativeQuery("select * from flutter_transaction "+query +"", FlutterTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from flutter_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    public Page<FlutterTransaction> getTransaction(String column, String data, int pageSize, int pageNum){
        
        String query = " where "+column+" like '%"+data+"%'";
        
        Page<FlutterTransaction> page = new Page<>();
        
        List<FlutterTransaction> transactions ;
        
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select * from flutter_transaction "+query, FlutterTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<FlutterTransaction>)entityManager.createNativeQuery("select * from flutter_transaction "+query +"", FlutterTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from flutter_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    public Page<FlutterTransaction> getTransaction(String column, double data, int pageSize, int pageNum){
        
        String query = " where "+column+"="+data;
        
        Page<FlutterTransaction> page = new Page<>();
        
        List<FlutterTransaction> transactions ;
        
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select * from flutter_transaction "+query, FlutterTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<FlutterTransaction>)entityManager.createNativeQuery("select * from flutter_transaction "+query +"", FlutterTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from flutter_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    public List getReport(Date startDate, Date endDate, String currency, Boolean status, String merchant){
        
        String query;
        
        if(startDate == null){
            query = " where id <> 0 ";
        }else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query =" where date(timein) >= date('"+dateFormat.format(startDate)+"') and date(timein) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null)
            query += " and currencyin = '"+currency+"'";
        
        if(status != null)
            query += " and vpctxnresponsecode "+ (status == true ? "=" : "<>") +"'0'";
        
        if(merchant != null)
            query += " and vpcmerchant = '"+merchant+"'";
        
        List list = entityManager.createNativeQuery("select sum(vpcamount)/100 as total, vpctxnresponsecode, count(*) as count, vpccurrency, vpccard from flutter_transaction "+query+" and vpccard is not null group by vpctxnresponsecode,vpccard, vpccurrency").getResultList();
        
        return list;
    }
    
    public List<String> getMerchants(){
        
        String query = " where vpcmerchant <> ''";
        
        List merchant =  entityManager.createNativeQuery("select distinct(vpcmerchant) from flutter_transaction where "+query).getResultList();
        
        return merchant;
    }
    
    public Page<FlutterTransaction> getTransactions(Date startDate, Date endDate, String searchValue, Object searchColumn, int pageSize, int pageNum){
        
        String query;
        
        if(startDate == null){
            query = " where id <> 0 ";
        }else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query =" where date(timein) >= date('"+dateFormat.format(startDate)+"') and date(timein) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(searchValue != null && !searchValue.isEmpty()){
            query += " and concat_ws(' '"+TransactionUtility.populateIndex().get(searchColumn)+") like '%"+searchValue+"%'";
        }
        
        Page<FlutterTransaction> page = new Page<>();
        
        List<FlutterTransaction> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select * from flutter_transaction "+query, FlutterTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<FlutterTransaction>)entityManager.createNativeQuery("select * from flutter_transaction "+query +"", FlutterTransaction.class)
                    .getResultList();
       
        page.setContent(transactions);
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from flutter_transaction "+query).getSingleResult().toString());
        
        page.setCount(count);
        
        return page;
    }
    
    @Override
     public boolean create(List<FlutterTransaction> data) throws DatabaseException{
        
        try{
            System.out.println(getEntityManager());
            
            int index = 0;
            for(FlutterTransaction t : data){
                
                try{
                    
                    FlutterTransaction ft = findByKey("uniqueref", t.getUniqueref());
                    
                    if(ft != null)
                        continue;
                    
                    getEntityManager().persist(t);
                }catch(Exception ex){
                }
                
                index++;
                
                if(index % 50 == 0){
                    getEntityManager().flush();
                    getEntityManager().clear();
                }
            }
            
            getEntityManager().flush();
            getEntityManager().clear();
            
             // This ensure the id generated
            return true;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    } 
     
    public List getAnalysedData(Date startDate, Date endDate, String currency){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String startD = dateFormat.format(startDate);
        String endD = dateFormat.format(endDate);
        
        String queryString = " where date(timein) >= date('"+startD+"') and date(timein) <= date('"+endD+"') and currencyin='"+currency+"'";
        
        List obj = entityManager.createNativeQuery("select sum(vpcamount)/100 as total, vpctxnresponsecode, count(*) as count, vpccurrency, vpccard from flutter_transaction "+queryString+" group by vpctxnresponsecode,vpccard  ").getResultList();
        
        return obj;
    }
    
    public List getTransactionCount(Date startDate, Date endDate, String currency){
        
        String query = "where id <> 0 and currencyin='"+currency+"' ";
        
//        long skip = pageNum * pageSize;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(startDate != null){
            
            query += " and date(timein) >= date('"+dateFormat.format(startDate)+"')";
        }
        
        if(endDate != null){
            query += " and date(timein) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        List list = entityManager.createNativeQuery("select vpctxnresponsecode, count(*) as count, date(timein) from flutter_transaction "+query+" group by vpctxnresponsecode, date(timein) ").getResultList();
        
        return list;
    }
}

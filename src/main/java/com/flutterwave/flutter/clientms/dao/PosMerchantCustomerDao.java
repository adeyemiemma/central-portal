package com.flutterwave.flutter.clientms.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.flutterwave.flutter.clientms.model.PosMerchantCustomer;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class PosMerchantCustomerDao extends HibernateDao<PosMerchantCustomer> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME_NEW)
    private EntityManager entityManager;

    public PosMerchantCustomerDao() {
        super(PosMerchantCustomer.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<PosMerchantCustomer> findByOrder(String merchantCode, String customerId){
        
        String query = " where id <> 0";
        
        if(Utility.emptyToNull(merchantCode) == null)
            return null;
        
        query += " and merchantCode = '"+merchantCode+"'";
        
        if(Utility.emptyToNull(customerId) != null)
            query += " and customerId = '"+customerId+"'";
        
//        if(Utility.emptyToNull(posOrderId) != null)
//            query += " and pos = '"+posOrderId+"'";
        
        List<PosMerchantCustomer> customers = entityManager.createNativeQuery("select * from "+PosMerchantCustomer.TABLE_NAME+" "+query).getResultList();
        
        if(customers == null || customers.isEmpty())
            return null;
        
        return customers;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.PosOrder;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class PosOrderDao extends HibernateDao<PosOrder> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME_NEW)
    private EntityManager entityManager;

    public PosOrderDao() {
        super(PosOrder.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<PosOrder> findByOrder(String merchantCode, String customerOrderId, String posOrderId){
        
        String query = " where id <> 0";
        
        if(Utility.emptyToNull(merchantCode) == null)
            return null;
        
        query += " and merchantCode = '"+merchantCode+"'";
        
        if(Utility.emptyToNull(customerOrderId) != null)
            query += " and customerOrderId = '"+customerOrderId+"'";
        
        if(Utility.emptyToNull(posOrderId) != null)
            query += " and posOrderId = '"+posOrderId+"'";
        
        List<PosOrder> orders = entityManager.createNativeQuery("select * from "+PosOrder.TABLE_NAME+" "+query).getResultList();
        
        if(orders == null || orders.isEmpty())
            return null;
        
        return orders;
    }
    
}

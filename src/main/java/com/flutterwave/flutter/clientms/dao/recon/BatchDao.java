/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.recon;


import com.flutterwave.flutter.clientms.model.recon.Batch;
import com.flutterwave.flutter.clientms.util.Global;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author adeyemi
 */
@Stateless
public class BatchDao extends com.flutterwave.flutter.core.dao.HibernateDao<Batch> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public BatchDao() {
        super(Batch.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        
        return entityManager;
    }
    
}

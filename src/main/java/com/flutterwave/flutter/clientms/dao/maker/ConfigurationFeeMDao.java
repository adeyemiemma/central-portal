/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.maker;

import com.flutterwave.flutter.clientms.dao.BaseHibernateDao;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.maker.ConfigurationFeeM;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author adeyemi
 */
@Stateless
public class ConfigurationFeeMDao extends BaseHibernateDao<ConfigurationFeeM> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public ConfigurationFeeMDao() {
        super(ConfigurationFeeM.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }  
    
    
}

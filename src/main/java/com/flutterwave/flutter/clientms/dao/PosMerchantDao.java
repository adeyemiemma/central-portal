/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.util.Page;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class PosMerchantDao extends HibernateDao<PosMerchant> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public PosMerchantDao() {
        super(PosMerchant.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<PosMerchant> query(int pageNo, int pageSize, Date startDate, Date endDate, String owner, String search){
       
        String query = "where id <> 0";
        String joinQuery = "";
        
        if(startDate != null)
            query += " and mpos_merchant.createdOn >= '"+Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss")+"'";
        
        if(endDate != null)
            query += " and mpos_merchant.createdOn <= '"+Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss")+"'";
        
        if(owner != null){
            query += " and mpos_merchant.merchantId = "+owner;
        }
        
        if(search != null){
            query += " and concat_ws(' ', mpos_merchant.name,mpos_merchant.merchantCode,mpos_merchant.posId) like '%" + search + "%'";
        }
        
        List<PosMerchant> list = null;
        
        if(pageSize == 0)
            list = entityManager.createNativeQuery("select mpos_merchant.* from mpos_merchant " + joinQuery + " " + query + " order by id desc ", PosMerchant.class)
                .setFirstResult(pageNo)
                .getResultList();
        else
            list = entityManager.createNativeQuery("select mpos_merchant.* from mpos_merchant " + joinQuery + " " + query + " order by id desc ", PosMerchant.class)
                .setFirstResult(pageNo)
                .setMaxResults(pageSize)
                .getResultList();

        Object countResult = entityManager.createNativeQuery("select count(*) as count from mpos_merchant " + joinQuery + " " + query).getSingleResult();

        long count = Long.parseLong(countResult + "");

        Page<PosMerchant> posMerchants = new Page(count, list);

        return posMerchants;
    }
}

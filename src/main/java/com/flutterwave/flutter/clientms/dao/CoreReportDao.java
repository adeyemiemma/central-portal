/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.CoreReport;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class CoreReportDao extends HibernateDao<CoreReport> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public CoreReportDao() {
        super(CoreReport.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        
        return entityManager;
    }
    
    public List<CoreReport> findByReportDate(Date reportDate) throws DatabaseException{
        
        try{
            
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<CoreReport> query = builder.createQuery(entityClass);

            // This used to map the entities to a class and extract the fields involved
            Root<CoreReport> rootQuery = query.from(entityClass);

            ParameterExpression dateParam = builder.parameter(Date.class);
            
            javax.persistence.criteria.Predicate predicate = builder.equal(rootQuery.get("reportDate"), dateParam);

            // This is used to build the cq that will allow the monthParam
            query.select(rootQuery).where(predicate);

            List<CoreReport> list = getEntityManager().createQuery(query)
                    .setParameter(dateParam, reportDate)
                    .getResultList();


            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    public List getReport(Date startDate, Date endDate){
        
        String query = " where core_report.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(core_report.reportDate) >= date('"+dateFormat.format(startDate)+"') and date(core_report.reportDate) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(volume), sum(revenue) from core_report "+query + "")
                .getResultList();
        
        return list;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author adeyemi
 */
@Stateless
public class UserDao extends com.flutterwave.flutter.core.dao.HibernateDao<User> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public UserDao() {
        super(User.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }  
    
    public Page<User> findByDomainId(int pageNum, int pageSize, String domainId) throws DatabaseException{
        
        try{
            
            getEntityManager().getEntityManagerFactory().getCache().evictAll();
            
            Page<User> page = new Page<>();
            
//            int skip = pageNum * pageSize;
            
            page.setCount(count()); // This get the total available elements and set the page item
            
            CriteriaBuilder cb =getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<User> root = cq.from(User.class);
            ParameterExpression keyParam = cb.parameter(String.class);
            cq.select(root).where(cb.equal(root.get("domainId"), keyParam)).orderBy(cb.desc(root.get("id")));
            
            javax.persistence.Query query =  getEntityManager().createQuery(cq);
            query.setHint("org.hibernate.cacheMode", "IGNORE")
                    .setHint("javax.persistence.cache.storeMode", "REFRESH")
                    .setParameter(keyParam, domainId)
                    .setFirstResult(pageNum);
            
            // This is nd
            if(pageSize > 0)
                query.setMaxResults(pageNum+pageSize);
            
            List<User> content = query.getResultList();
            
            page.setContent(content);
            return page;
            
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
}

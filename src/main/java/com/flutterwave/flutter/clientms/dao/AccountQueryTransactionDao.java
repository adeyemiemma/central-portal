/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.AccountQuery;
import com.flutterwave.flutter.clientms.model.AccountQueryTransaction;
import com.flutterwave.flutter.clientms.model.Bank;
import com.flutterwave.flutter.clientms.model.AccountQueryTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class AccountQueryTransactionDao extends HibernateDao<AccountQueryTransaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public AccountQueryTransactionDao() {
        super(AccountQueryTransaction.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<AccountQueryTransaction> findByBank(AccountQuery bank)
        throws DatabaseException{
        
        if(bank == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<AccountQueryTransaction> query = builder.createQuery(AccountQueryTransaction.class);

            // This used to map the entities to a class and extract the fields involved
            Root<AccountQueryTransaction> rootQuery = query.from(AccountQueryTransaction.class);

            ParameterExpression keyParam = builder.parameter(Bank.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("accountQuery"), keyParam));

            List<AccountQueryTransaction> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, bank)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    public List<AccountQueryTransaction> findRecent(){
        
        String query = "SELECT * FROM ( SELECT * from account_query_transaction ORDER BY createdOn DESC) AS sub GROUP BY accountQuery_id;";
        
        List<AccountQueryTransaction> list = entityManager.createNativeQuery(query, AccountQueryTransaction.class)
                .getResultList();
        
        return list;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.maker;

import com.flutterwave.flutter.clientms.dao.*;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.maker.ExchangeRateM;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class ExchangeRateMDao extends BaseHibernateDao<ExchangeRateM> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public ExchangeRateMDao() {
        super(ExchangeRateM.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
//    public List<ExchangeRateM> findByCurrency(Currency origin, Currency destination) throws DatabaseException{
//        
//        if(origin == null)
//            return null;
//        
//        try{
//        // This is used to build the criteria
//            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
//
//            // This is used to build a cq instance from the entity
//            CriteriaQuery<ExchangeRateM> query = builder.createQuery(ExchangeRateM.class);
//
//            // This used to map the entities to a class and extract the fields involved
//            Root<ExchangeRateM> rootQuery = query.from(ExchangeRateM.class);
//
//            ParameterExpression origParam = builder.parameter(Currency.class);
//            ParameterExpression destParam = builder.parameter(Currency.class);
//
//            Predicate predicate = builder.equal(rootQuery.get("originCurrency"), origParam);
//            builder.and(predicate, builder.equal(rootQuery.get("destinationCurrency"), destParam));
//            // This is used to build the cq that will allow the keyParam
//            query.select(rootQuery).where(predicate);
//            
//
//            List<ExchangeRateM> list = getEntityManager().createQuery(query)
//                    .setParameter(origParam, origin)
//                    .setParameter(destParam, destination)
//                    .getResultList();
//
//            if(list.isEmpty())
//                return null;
//
//            return list;
//        }catch(Exception ex){
//            throw new DatabaseException(ex);
//        }
//        
//    }
    
    public List<ExchangeRateM> find(Currency origin, Currency destination) throws DatabaseException{
        
        if(origin == null)
            return null;
        
        try{
        // This is used to build the criteria
            String query = "select * from exchange_rate_m where originCurrency_id="+ origin.getId() +" "
                    + "and destinationCurrency_id="+ destination.getId();

            // This used to map the entities to a class and extract the fields involved
            
//            entityManager.createNativeQuery(query).;
            

            List<ExchangeRateM> list = getEntityManager().createNativeQuery(query, ExchangeRateM.class)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
        
    }
    
    
}

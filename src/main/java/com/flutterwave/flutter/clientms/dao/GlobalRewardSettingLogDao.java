/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.GlobalRewardSettingLog;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class GlobalRewardSettingLogDao extends HibernateDao<GlobalRewardSettingLog> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public GlobalRewardSettingLogDao() {
        super(GlobalRewardSettingLog.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
}

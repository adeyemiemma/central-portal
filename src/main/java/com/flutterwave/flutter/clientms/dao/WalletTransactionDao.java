/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Wallet;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Wallet;
import com.flutterwave.flutter.clientms.model.WalletTransaction;
import com.flutterwave.flutter.clientms.model.products.GenericMerchant;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.WalletTransactionType;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class WalletTransactionDao extends HibernateDao<WalletTransaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public WalletTransactionDao() {
        super(WalletTransaction.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<WalletTransaction> findByWallet(int pageNum, int pageSize, Wallet wallet) throws DatabaseException{
        
        Page<WalletTransaction> page = new Page<>();
                
        if(wallet == null)
            return page;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery query = builder.createQuery();

            // This used to map the entities to a class and extract the fields involved
            Root<WalletTransaction> rootQuery = query.from(WalletTransaction.class);

            ParameterExpression keyParam = builder.parameter(Wallet.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("wallet"), keyParam));
            
            query.select(builder.count(rootQuery)).where(builder.equal(rootQuery.get("wallet"), keyParam));
            javax.persistence.Query countQuery = getEntityManager().createQuery(query);

            countQuery.setParameter(keyParam, wallet);
            
            List<WalletTransaction> list = getEntityManager().createQuery(query)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .setParameter(keyParam, wallet)
                    .getResultList();
            
            
            if(list.isEmpty())
                return page;
            
            long count = ((Long) countQuery.getSingleResult());
           
            page = new Page<>(count, list);

            return page;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }   
    }
   
    public Page<WalletTransaction> findByQuery(int pageNum, int pageSize, Date startDate, Date endDate, 
            Wallet wallet, WalletTransactionType transactionType) throws DatabaseException{
        
        Page<WalletTransaction> page = new Page<>();
        
        String query = " where wallet_transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(wallet_transaction.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(wallet_transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(wallet != null){
            joinQuery += " join wallet on wallet.id = wallet_transaction.wallet_id";
            query += " and wallet_transaction.wallet_id="+wallet.getId()+"";
        }
        
        if(transactionType != null){
            joinQuery += " join wallet on wallet.id = wallet_transaction.transactionType";
            query += " and wallet_transaction.transactionType='"+transactionType+"'";
        }
        
        List<WalletTransaction> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select wallet_transaction.* from wallet_transaction "+joinQuery+" "+query, WalletTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = entityManager.createNativeQuery("select wallet_transaction.* from wallet_transaction "+joinQuery+" "+query +"", WalletTransaction.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from wallet_transaction "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);
        
        return page;
    }
    
}

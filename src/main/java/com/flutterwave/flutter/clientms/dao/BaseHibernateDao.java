/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityGraph;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


/**
 *
 * @author emmanueladeyemi
 * @param <T>
 */
public abstract class BaseHibernateDao<T> extends HibernateDao<T>  {
    
    
    public BaseHibernateDao(Class<T> entityClass) {
        super(entityClass);
    }
    
    public long countUnauth(){
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = cb.createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        ParameterExpression keyParam = cb.parameter(Status.class);
        
        Predicate predicate = cb.equal(rt.get("status"), keyParam);
        predicate = cb.or(predicate, cb.isNull(rt.get("status")));
        
        cq.select(getEntityManager().getCriteriaBuilder().count(rt))
                .where(predicate);
        
        javax.persistence.Query q = getEntityManager().createQuery(cq)
                .setParameter(keyParam, Status.PENDING);
        
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public Page<T> findUnauth(int pageNum, int pageSize, String... joinItems) throws DatabaseException{
        
        try{
            
            getEntityManager().getEntityManagerFactory().getCache().evictAll();
            
            Page<T> page = new Page<>();
            
            CriteriaBuilder cb =getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = cb.createQuery();
            Root<T> root = cq.from(entityClass);
            ParameterExpression keyParam = cb.parameter(Status.class);
            Predicate predicate = cb.equal(root.get("status"), keyParam);
            predicate = cb.or(predicate, cb.isNull(root.get("status")));
            cq.select(root).where(predicate).orderBy(cb.desc(root.get("id")));
            
            page.setCount(countUnauth()); 
            
            javax.persistence.Query query =  getEntityManager().createQuery(cq);
            
            query.setHint("org.hibernate.cacheMode", "IGNORE");
            
            if(joinItems != null && joinItems.length > 0){
                EntityGraph<T> entityGraph = getEntityManager().createEntityGraph(entityClass);
                entityGraph.addAttributeNodes(joinItems);
                    query.setHint("javax.persistence.loadgraph", entityGraph);                    
            }
            
            query.setParameter(keyParam, Status.PENDING);
            query.setFirstResult(pageNum);
            
            // This is nd
            if(pageSize > 0)
                query.setMaxResults(pageSize);
            
            List<T> content = query.getResultList();
            
            page.setContent(content);
            return page;
            
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    public List<T> find(String key, long value, String... joinItems) throws DatabaseException{
        
        try{
            
            getEntityManager().getEntityManagerFactory().getCache().evictAll();

            Map<String, Object> hints = new HashMap<>();
            
            EntityGraph<T> entityGraph = null;
            
            if(joinItems != null && joinItems.length <= 0){
                entityGraph = getEntityManager().createEntityGraph(entityClass);
                entityGraph.addAttributeNodes(joinItems);
                hints.put("javax.persistence.loadgraph", entityGraph);            
            }
            
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<T> query = builder.createQuery(entityClass);

            // This used to map the entities to a class and extract the fields involved
            Root<T> rootQuery = query.from(entityClass);

            ParameterExpression keyParam = builder.parameter(Long.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get(key), keyParam));

            Query q = getEntityManager().createQuery(query);
            
            if(joinItems != null && joinItems.length <= 0){
                q.setHint("javax.persistence.loadgraph", entityGraph);
            }
            
            List<T> list = q.setParameter(keyParam, value)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
}

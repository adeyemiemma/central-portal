/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.maker;

import com.flutterwave.flutter.clientms.dao.BaseHibernateDao;
import com.flutterwave.flutter.clientms.model.Bank;
import com.flutterwave.flutter.clientms.model.maker.AccountQueryM;
import com.flutterwave.flutter.clientms.model.maker.BankM;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class AccountQueryMDao extends BaseHibernateDao<AccountQueryM> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public AccountQueryMDao() {
        super(AccountQueryM.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<AccountQueryM> findByBank(Bank bank)
        throws DatabaseException{
        
        if(bank == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<AccountQueryM> query = builder.createQuery(AccountQueryM.class);

            // This used to map the entities to a class and extract the fields involved
            Root<AccountQueryM> rootQuery = query.from(AccountQueryM.class);

            ParameterExpression keyParam = builder.parameter(Bank.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("bank"), keyParam));

            List<AccountQueryM> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, bank)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
}

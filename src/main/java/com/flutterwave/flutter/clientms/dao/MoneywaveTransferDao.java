/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.products.MoneywaveTransfer;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.MoneywaveUtil;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class MoneywaveTransferDao extends HibernateDao<MoneywaveTransfer> {

    @PersistenceContext(unitName = Global.MONEYWAVE_PERSISTENT_NAME)
    private EntityManager entityManager;

    public MoneywaveTransferDao() {
        super(MoneywaveTransfer.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<MoneywaveTransfer> getTransactions(int pageNum, int pageSize, Date startDate, Date endDate,
            String systemType, String transactionStatus, String destination, String source, String searchReference, String flutterResponseMessage,
            String merchant, Boolean isMerchantId, String currency){
        
        Page<MoneywaveTransfer> page = new Page<>();
        
        String query = " where transfers.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transfers.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(transfers.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(destination != null)
            query += " and dest='"+destination+"'";
        
        if(source != null)
            query += " and source='"+source+"'";
        
        if(transactionStatus != null){
            query += " and status='"+transactionStatus+"'";
        }
        
        if(currency != null){
            query += " and currency.chargeCurrency='"+currency+"'";
        }
        
        if(searchReference != null && !"".equals(searchReference)){
            query += " and linkingReference like '%"+searchReference+"%' or flutterReference like '%"+searchReference+"%'";
        }
        
        if(flutterResponseMessage != null){
            query += " and flutterResponseMessage='"+flutterResponseMessage+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join merchants on merchants.id=transfers.merchantId";
            
            if(isMerchantId == null || isMerchantId == false)
                query += " and merchants.name like '%"+merchant+"%'";
            else
                query += " and merchants.id = "+merchant+"";
        }
        
        List<MoneywaveTransfer> transfers ;
        if(pageSize > 0 )
            transfers = entityManager.createNativeQuery("select transfers.* from transfers "+joinQuery+" "+query+" order by id desc", MoneywaveTransfer.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transfers = (List<MoneywaveTransfer>)entityManager.createNativeQuery("select transfers.* from transfers "+joinQuery+" "+query +" order by id desc", MoneywaveTransfer.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from transfers "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transfers);
        page.setCount(count);
        
        return page;
    }
    
    
    public Page<MoneywaveTransfer> getTransactionsByStatus(int pageNum, int pageSize, Date startDate, Date endDate,
            String systemType, String destination, String source, String searchReference, String flutterResponseMessage,
            String merchant, Boolean isMerchantId, String currency, boolean failed){
        
        Page<MoneywaveTransfer> page = new Page<>();
        
        String query = " where transfers.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transfers.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(transfers.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(destination != null)
            query += " and dest='"+destination+"'";
        
        if(source != null)
            query += " and source='"+source+"'";
        
        if(failed == true){
            query += " and status <> 'completed'";
        }else
            query += " and status = 'completed'";
        
        if(currency != null){
            joinQuery += " join currency on currency.id = transfers.currencyId";
            query += " and currency.shortCode='"+currency+"'";
        }
        
        if(searchReference != null && !"".equals(searchReference)){
            query += " and linkingReference like '%"+searchReference+"%' or flutterReference like '%"+searchReference+"%'";
        }
        
        if(flutterResponseMessage != null){
            query += " and flutterResponseMessage='"+flutterResponseMessage+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join merchants on merchants.id=transfers.merchantId";
            
            if(isMerchantId == null || isMerchantId == false)
                query += " and merchants.name like '%"+merchant+"%'";
            else
                query += " and merchants.id = "+merchant+"";
        }
        
        List<MoneywaveTransfer> transfers ;
        if(pageSize > 0 )
            transfers = entityManager.createNativeQuery("select transfers.* from transfers "+joinQuery+" "+query, MoneywaveTransfer.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transfers = (List<MoneywaveTransfer>)entityManager.createNativeQuery("select transfers.* from transfers "+joinQuery+" "+query +"", MoneywaveTransfer.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from transfers "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transfers);
        page.setCount(count);
        
        return page;
    }
    
    
    public List<String> getFlutterResponse(){
        
        List<String> list = entityManager.createNativeQuery("select distinct(flutterResponseMessage) from transfers").getResultList();
        
        return list;
    }
    
    public List getSummaryWithDate(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where transfers.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transfers.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(transfers.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            joinQuery += " join currency on currency.id = transfers.currencyId";
            query += " and currency.shortCode='"+currency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join merchants on merchants.id=transfers.merchantId";
            
            query += " and merchants.name like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(fee),count(*), date(transfers.createdAt), status from transfers "+joinQuery+" "+query + " group by date(transfers.createdAt), status")
                .getResultList();
        
        return list;
    }
    
    public List getTotalSummary(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where transfers.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transfers.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(transfers.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            joinQuery += " join currency on currency.id = transfers.currencyId";
            query += " and currency.shortCode='"+currency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join merchants on merchants.id=transfers.merchantId";
            
            query += " and merchants.name like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(fee), status  from transfers "+joinQuery+" "+query + " group by status")
                .getResultList();
        
        return list;
    }
    
    public List getTotalSummaryNoGrouping(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where transfers.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transfers.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(transfers.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            joinQuery += " join currency on currency.id = transfers.currencyId";
            query += " and currency.shortCode='"+currency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join merchants on merchants.id=transfers.merchantId";
            
            query += " and merchants.name like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(fee), status  from transfers "+joinQuery+" "+query)
                .getResultList();
        
        return list;
    }
    
    public List getSummaryStatus(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where transfers.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transfers.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(transfers.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            joinQuery += " join currency on currency.id = transfers.currencyId";
            query += " and currency.shortCode='"+currency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join merchants on merchants.id=transfers.merchantId";
            
            query += " and merchants.name like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), status  from transfers "+joinQuery+" "+query + " group by status")
                .getResultList();
        
        return list;
    }
    
    public List getReportSummary(Date startDate, Date endDate, String currency, String merchant, MoneywaveUtil.ReportType reportType){
        
        String query , joinQuery = "";
        
        query = " where transfers.id <> 0 ";
        
        if(reportType == MoneywaveUtil.ReportType.Card){
            query += " and system_type='wallet-fund' and source='card' and status='completed'";
        }else
            query += " and system_type='wallet-disburse' and status='completed'";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transfers.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(transfers.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            joinQuery += " join currency on currency.id = transfers.currencyId";
            query += " and currency.shortCode='"+currency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join merchants on merchants.id=transfers.merchantId";
            
            query += " and merchants.name like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), count(*)  from transfers "+joinQuery+" "+query + "")
                .getResultList();
        
        return list;
    }
    
    public List getReportSummary( Date startDate, Date endDate,
            String transactionStatus, String searchReference, String merchant, String currency){
    
        String query = " where transfers.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(transfers.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(transfers.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(transactionStatus != null){
            query += " and status='"+transactionStatus+"'";
        }
        
        if(currency != null){
            joinQuery += " join currency on currency.id = transfers.currencyId";
            query += " and currency.shortCode='"+currency+"'";
        }
        
        if(searchReference != null && !"".equals(searchReference)){
            query += " and linkingReference like '%"+searchReference+"%' or flutterReference like '%"+searchReference+"%'";
        }
        
        if(searchReference != null){
            query += " and flutterResponseMessage='"+searchReference+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))){
            joinQuery += " join merchants on merchants.id=transfers.merchantId";
            
            query += " and merchants.id ="+merchant+"";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), date(transfers.createdAt) as transaction_date, IF(`status` like '%completed%', 'success', 'failed') as t_status, count(*) as volume  from transfers "+joinQuery+" "+ query +" group by `transaction_date`, t_status;")
                .getResultList();
        
        
        return list;
    }
  
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.InternationalTransaction;
import com.flutterwave.flutter.clientms.model.RaveTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class InternationalTransactionDao extends HibernateDao<InternationalTransaction> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public InternationalTransactionDao() {
        super(InternationalTransaction.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<InternationalTransaction> find(int pageNum, int pageSize, Date startDate, Date endDate, String product, String merchantId, Boolean notificationState){
        
        Page<InternationalTransaction> page = new Page<>();
        
        String query = " where international_transaction.id <> 0";
        
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(international_transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(international_transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }

        if(notificationState != null){
            
            query += " and international_transaction.notificationStatus="+notificationState;
        }
        
        if(product != null)
            query += " and international_transaction.product='"+product+"'";
        
        if(merchantId != null)
            query += " and international_transaction.merchantId='"+merchantId+"'";
        
        List<InternationalTransaction> transactions;
        
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select international_transaction.* from international_transaction "+joinQuery+" "+query, InternationalTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = entityManager.createNativeQuery("select international_transaction.* from international_transaction "+joinQuery+" "+query +"", InternationalTransaction.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from international_transaction "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);
        
        return page;
    }
    
    public List<String> getMerchants(Date startDate, Date endDate, String product, Boolean notificationState){
        
        String query = " where international_transaction.id <> 0";
        
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(international_transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(international_transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }

        if(notificationState != null){
            
            query += " and international_transaction.notificationStatus="+notificationState;
        }
        
        if(product != null)
            query += " and international_transaction.product='"+product+"'";
        
        List<String> transactions = (List<String>)entityManager.createNativeQuery("select distinct(`merchantId`) as merchantId from international_transaction "+joinQuery+" "+query +"")
                    .getResultList();
        
        return transactions;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<InternationalTransaction> findTransaction(String key, String ref){
        
        String query = "where "+key+"='"+ref+"'";
        
        List<InternationalTransaction> transactions = entityManager.createNativeQuery("select * from international_transaction "+query +"", InternationalTransaction.class)
                    .getResultList();
        
        if(transactions == null || !transactions.isEmpty())
            return transactions;
        
        return transactions;
        
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean create(List<InternationalTransaction> data) throws DatabaseException{
        
        try{
            System.out.println(getEntityManager());
            
//            getEntityManager().setProperty("hibernate.jdbc.batch_size", BATCH_SIZE);
            
            int index = 0;
            for(InternationalTransaction t : data){
                
                try{
                    getEntityManager().persist(t);
                }catch(Exception ex){
                }
                
                index++;
                
                if(index % 30 == 0){
                    getEntityManager().flush();
                    getEntityManager().clear();
                }
            }
            
            getEntityManager().flush();
            getEntityManager().clear();
            
             // This ensure the id generated
            return true;
        }catch(Exception ex){
             throw new DatabaseException(ex);
        }
    } 
}

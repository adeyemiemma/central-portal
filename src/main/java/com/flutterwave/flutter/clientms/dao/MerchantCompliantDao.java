/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.MerchantCompliance;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.util.Page;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class MerchantCompliantDao extends BaseHibernateDao<MerchantCompliance> {
    
    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;
    
    public MerchantCompliantDao() {
        super(MerchantCompliance.class);
    }

    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<MerchantCompliance> findMerchant(int pageNum, int pageSize, Date startDate, Date endDate, String product, String merchantId, 
            String searchString, Boolean approved, String country, boolean rejected){
        
        Page<MerchantCompliance> page = new Page<>();
        
        String query = " where id <> 0";
        String joinQuery = "";
        
        if(merchantId != null)
            query += " and merchantId='"+merchantId+"'";
        
        if(product != null){
            query += " and ( products= '"+product+"'";
        
            if(product.equalsIgnoreCase("rave")){
                query += " or products= 'ravepay'";
            }

            query += ")";
        }
        
        if(startDate != null){
            query += " and createdOn >= '"+Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss")+"'";
        }
        
        if(endDate != null){
               query += " and createdOn <= '"+Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss")+"'";
        }
        
        if(searchString != null)
            query += " and ( merchantId like '%"+searchString+"%' or registeredName like '%"+searchString+"%' or registeredNumber like '%"+searchString+"%' )";
        
        if(approved != null)
            if(approved == true){
                query += " and (approvedOn is NOT NULL";
                
                if(rejected == true)
                   query += " and `status`='"+Status.REJECTED+"' )"; 
                else
                    query += " and (`status`='"+Status.APPROVED+"' or `status`='"+Status.UNKNOWN+"'))"; 
            }
            else
                query += " and approvedOn is NULL";
        
        if(country != null && !country.equalsIgnoreCase("")){
            query += " and country='"+country+"'";
        }
        
        List<MerchantCompliance> complianceList;
        
        if(pageSize > 0 )
            complianceList = entityManager.createNativeQuery("select merchant_compliance.* from merchant_compliance "+joinQuery+" "+query +"  order by id desc", MerchantCompliance.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            complianceList = entityManager.createNativeQuery("select merchant_compliance.* from merchant_compliance "+joinQuery+" "+query +" order by id desc", MerchantCompliance.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from merchant_compliance "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(complianceList);
        page.setCount(count);
        
        return page;
        
    }
    
    public MerchantCompliance findByMerchantId(String merchantId, String product){
     
        String query = " where id <> 0";
        
        if(merchantId != null)
            query += " and merchantId='"+merchantId+"'";
        
        if(product != null)
            query += " and products= '"+product+"'";
        
        
        List<MerchantCompliance> list = getEntityManager().createNativeQuery("select * from merchant_compliance "+query, MerchantCompliance.class).getResultList();
        
        if(list != null && !list.isEmpty())
            return list.get(0);
        
        return null;
    }
    
    public List<MerchantCompliance> search(String searchString){
        
        if(searchString == null)
            return new ArrayList<>();
        
        String query = " where  merchantId like '%"+searchString+"%' or registeredName like '%"+searchString+"%' or registeredNumber like '%"+searchString+"%' ";
        
        List<MerchantCompliance>  complianceList = entityManager.createNativeQuery("select merchant_compliance.* from merchant_compliance "+query +" order by id desc", MerchantCompliance.class)
                    .getResultList();
        
        
        return complianceList;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class ExchangeRateDao extends HibernateDao<ExchangeRate> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public ExchangeRateDao() {
        super(ExchangeRate.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
//    public ExchangeRate findByCurrency(Currency origin, Currency destination) throws DatabaseException{
//        
//        if(origin == null)
//            return null;
//        
//        try{
//        // This is used to build the criteria
//            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
//
//            // This is used to build a cq instance from the entity
//            CriteriaQuery<ExchangeRate> query = builder.createQuery(ExchangeRate.class);
//
//            // This used to map the entities to a class and extract the fields involved
//            Root<ExchangeRate> rootQuery = query.from(ExchangeRate.class);
//
//            ParameterExpression origParam = builder.parameter(Currency.class);
//            ParameterExpression destParam = builder.parameter(Currency.class);
//
//            Predicate predicate = builder.equal(rootQuery.get("originCurrency"), origParam);
//            builder.and(predicate, builder.equal(rootQuery.get("destinationCurrency"), destParam));
//            // This is used to build the cq that will allow the keyParam
//            query.select(rootQuery).where(predicate);
//            
//
//            List<ExchangeRate> list = getEntityManager().createQuery(query)
//                    .setParameter(origParam, origin)
//                    .getResultList();
//
//            if(list.isEmpty())
//                return null;
//
//            return list.get(0);
//        }catch(Exception ex){
//            throw new DatabaseException(ex);
//        }
//        
//    }
    
    public ExchangeRate find(Currency origin, Currency destination) throws DatabaseException{
        
        if(origin == null)
            return null;
        
        try{
        // This is used to build the criteria
            String query = "select * from exchange_rate where originCurrency_id="+ origin.getId() +" "
                    + "and destinationCurrency_id="+ destination.getId();

            // This used to map the entities to a class and extract the fields involved
            
//            entityManager.createNativeQuery(query).;
            

            List<ExchangeRate> list = getEntityManager().createNativeQuery(query, ExchangeRate.class)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list.get(0);
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.products.RaveAddressBook;
import com.flutterwave.flutter.clientms.model.products.RaveMerchant;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class RaveAddressBookDao extends HibernateDao<RaveAddressBook> {

    @PersistenceContext(unitName = Global.RAVE_PERSISTENT_NAME)
    private EntityManager entityManager;

    public RaveAddressBookDao() {
        super(RaveAddressBook.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public RaveAddressBook findByMerchant(RaveMerchant raveMerchant) throws DatabaseException{
        
        if(raveMerchant == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<RaveAddressBook> query = builder.createQuery(RaveAddressBook.class);

            // This used to map the entities to a class and extract the fields involved
            Root<RaveAddressBook> rootQuery = query.from(RaveAddressBook.class);

            ParameterExpression keyParam = builder.parameter(RaveMerchant.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("raveMerchant"), keyParam));

            List<RaveAddressBook> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, raveMerchant)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list.get(0);
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
}

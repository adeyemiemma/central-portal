/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Refund;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class RefundDao extends HibernateDao<Refund> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public RefundDao() {
        super(Refund.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<Refund> find(int pageNumber, int pageSize, Date startDate, Date endDate, 
            String merchantId, String provider, String currency) throws DatabaseException{
       
        String query = "", joinQuery = "";
        
        query += " where refund.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query +=" and TIMESTAMP(refund.datetime) >= TIMESTAMP('"+dateFormat.format(startDate)+"') and TIMESTAMP(refund.datetime) <= TIMESTAMP('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null && !currency.isEmpty()){
            query += " and refund.currency='"+currency+"'";
        }
        if(provider != null && !provider.isEmpty()){
            query += " and refund.provider='"+provider+"'";
        }
        
        if(merchantId != null){
            query += " and refund.merchantId='"+merchantId+"'";
        }
        
        List<Refund> list = entityManager.createNativeQuery("select * from refund "+joinQuery+" "+query, Refund.class)
                .setFirstResult(pageNumber)
                .setMaxResults(pageSize)
                .getResultList();
        
        Object object = (Object) entityManager.createNativeQuery("select count(*) from refund "+joinQuery+" "+query).getSingleResult();
        
        long count = Long.parseLong(object+"");
        
        Page<Refund> page = new Page<>(count, list);
        
        return page;
    }
    
//    public List getReportSummary( Date startDate, Date endDate, String product, 
//            long merchantId, String currency, long productMid){
//    
//        String query = "", joinQuery = "";
//        
//        query += " where refund.id <> 0 ";
//        
//        if(startDate != null){
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            query +=" and date(refund.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(refund.createdOn) <= date('"+dateFormat.format(endDate)+"')";
//        }
//        
//        joinQuery += " join product on product.id = refund.product_id";
//        if(product != null){
//            query += " and product.name='"+product+"'";
//        }
//        
//        if(currency != null && !currency.isEmpty()){
//            query += " and refund.currency='"+currency+"'";
//        }
//        
//        if(merchantId > 0){
//            query += " and refund.merchantId="+merchantId;
//        }
//        
//        if(productMid > 0)
//            query += " and refund.productMid="+productMid;
//        
//        List list = entityManager.createNativeQuery("select sum(amount), date(refund.createdOn) as transaction_date, count(*) as volume, currency, product.name  from refund "+joinQuery+" "+ query +" group by `transaction_date`, currency")
//                .getResultList();
//        
//        
//        return list;
//    }
//    
//    public Map<String, Object> getSummary(Date startDate, Date endDate, String product, 
//            String merchantId, String currency, long productMid) throws DatabaseException{
//       
//        String query = "", joinQuery = "";
//        
//        query += " where refund.id <> 0 ";
//        
//        if(startDate != null){
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            query +=" and date(refund.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(refund.createdOn) <= date('"+dateFormat.format(endDate)+"')";
//        }
//        
//        if(product != null){
//            joinQuery += " join product on product.id = refund.product_id";
//            query += " and product.name='"+product+"'";
//        }
//        
//        if(currency != null && !currency.isEmpty()){
//            query += " and refund.currency='"+currency+"'";
//        }
//        
//        if(merchantId != null){
//            query += " and refund.merchantId='"+merchantId+"'";
//        }
//        
//        if(productMid > 0){
//            query += " and refund.productMid="+productMid;
//        }
//        
//        List list = entityManager.createNativeQuery("select sum(amount), sum(amountSettled) from refund "+joinQuery+" "+query)
//                .getResultList();
//        
//        Map<String, Object> record = new HashMap<>();
//        record.put("amount", list);
//        
//        Object obj = entityManager.createNativeQuery("select count(distinct(productMid)) from refund "+joinQuery+" "+query)
//                .getSingleResult();
//        
//        record.put("merchantcount", obj);
//        
//        return record;
//    }
//    
//    public List findSummaryByProvider(Date startDate, Date endDate,String providerName, String currency){
//        
//        String query = "", joinQuery = "";
//        
//        query += " where refund.id <> 0 ";
//        
//        if(startDate != null){
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            query +=" and date(refund.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(refund.createdOn) <= date('"+dateFormat.format(endDate)+"')";
//        }
//        
//        if(currency != null && !currency.isEmpty()){
//            query += " and refund.currency='"+currency+"'";
//        }
//        
//        if(providerName != null && !providerName.isEmpty()){
//            joinQuery += " join transaction on refund.flwTxnReference = refund.flwReference ";
//            query += " and refund.provider = '"+providerName+"'";
//        }
//        
//        List list = entityManager.createNativeQuery("select sum(refund.amount), sum(amountSettled) from refund "+joinQuery+" "+query)
//                .getResultList();
//        
//        
//        return list;
//    }
//    
//    public List<Refund> findByProduct(int pageNum, int pageSize, Date startDate, Date endDate,String product, String currency){
//        
//        String query = "", joinQuery = "";
//        
//        query += " where refund.id <> 0 ";
//        
//        if(startDate != null){
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            query +=" and date(refund.createdOn) >= date('"+dateFormat.format(startDate)+"') and date(refund.createdOn) <= date('"+dateFormat.format(endDate)+"')";
//        }
//        
//        if(product != null){
//            joinQuery += " join product on product.id = refund.product_id";
//            query += " and product.name='"+product+"'";
//        }
//        
//        if(currency != null && !currency.isEmpty()){
//            query += " and refund.currency='"+currency+"'";
//        }
//        
//        List<Refund> list = entityManager.createNativeQuery("select * from refund "+joinQuery+" "+query, Refund.class)
//                .setFirstResult(pageNum)
//                .setMaxResults(pageSize)
//                .getResultList();
//        
//        
//        return list;
//    }
    
}

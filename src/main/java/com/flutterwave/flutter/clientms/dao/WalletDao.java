/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Wallet;
import com.flutterwave.flutter.clientms.model.products.GenericMerchant;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class WalletDao extends HibernateDao<Wallet> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public WalletDao() {
        super(Wallet.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<Wallet> findByProduct(Product product) throws DatabaseException{
        
        if(product == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<Wallet> query = builder.createQuery(Wallet.class);

            // This used to map the entities to a class and extract the fields involved
            Root<Wallet> rootQuery = query.from(Wallet.class);

            ParameterExpression keyParam = builder.parameter(Product.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("product"), keyParam));

            List<Wallet> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, product)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }   
    }
    
    /**
     * This is called to get merchant wallet
     * @param product
     * @param merchant
     * @return
     * @throws DatabaseException 
     */
    public Wallet findByProductAndMerchant(Product product, GenericMerchant merchant) throws DatabaseException{
        
        if(product == null || merchant == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<Wallet> query = builder.createQuery(Wallet.class);

            // This used to map the entities to a class and extract the fields involved
            Root<Wallet> rootQuery = query.from(Wallet.class);

            ParameterExpression keyParam = builder.parameter(Product.class);
            ParameterExpression merchantParam = builder.parameter(String.class);
            
            Predicate predicate = builder.equal(rootQuery.get("product"), keyParam);
            
            predicate = builder.and(predicate, builder.equal(rootQuery.get("categoryId"), merchantParam));

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(predicate);

            List<Wallet> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, product)
                    .setParameter(merchantParam, merchant.getId())
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list.get(0);
            
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }   
    }
    
    public Wallet findByQuery(Product product, String id, Utility.WalletCategory category, Currency currency) throws DatabaseException{
       
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<Wallet> query = builder.createQuery(Wallet.class);

            // This used to map the entities to a class and extract the fields involved
            Root<Wallet> rootQuery = query.from(Wallet.class);

            ParameterExpression productParam = null;
                    
            Predicate predicate = builder.isNotNull(rootQuery.get("id"));
            if(product != null){
                productParam = builder.parameter(Product.class);
                predicate = builder.equal(rootQuery.get("product"), productParam);
            }
            
            ParameterExpression merchantParam = builder.parameter(Long.class);
            ParameterExpression currencyParam = builder.parameter(Currency.class);
            ParameterExpression catParam = builder.parameter(Utility.WalletCategory.class);
                
            predicate = builder.and(predicate, builder.equal(rootQuery.get("categoryId"), merchantParam));
            
            predicate = builder.and(predicate, builder.equal(rootQuery.get("category"), catParam));
            predicate = builder.and(predicate, builder.equal(rootQuery.get("currency"), currencyParam));

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(predicate);

            Query q = getEntityManager().createQuery(query);
            if(product != null)
                q.setParameter(productParam, product);
            
            q.setParameter(catParam, category);
            q.setParameter(merchantParam, id);
            q.setParameter(currencyParam, currency);
            List<Wallet> list = q.getResultList();

            if(list.isEmpty())
                return null;

            return list.get(0);
            
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }   
    }
    
}

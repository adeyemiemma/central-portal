/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class CoreMerchantDao extends HibernateDao<CoreMerchant> {

    @PersistenceContext(unitName = Global.CORE_PERSISTENT_NAME)
    private EntityManager entityManager;

    public CoreMerchantDao() {
        super(CoreMerchant.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<CoreMerchant> search(int pageNum, int pageSize, 
            String search, Boolean live, String country, Date startDate, Date endDate){
    
        getEntityManager().getEntityManagerFactory().getCache().evictAll();
            
        Page<CoreMerchant> page = new Page<>();

        CriteriaBuilder cb =getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<CoreMerchant> root = cq.from(entityClass);
        
        ParameterExpression keyParamId = cb.parameter(Long.class);
        ParameterExpression keyParamName = cb.parameter(String.class);
        ParameterExpression keyParamLive = cb.parameter(String.class);
        ParameterExpression keyParamCountry = cb.parameter(String.class);
        ParameterExpression keyParamOwner = cb.parameter(Integer.class);
        
        ParameterExpression keyParamStartDate = cb.parameter(Date.class);
        ParameterExpression keyParamEndDate = cb.parameter(Date.class);
        
        Predicate predicate = cb.gt(root.get("id"), keyParamId);
        
//        CoreMerchant.class.newInstance().();
        if(search != null && !"".equals(search)){
            
            Predicate p = cb.like(root.get("companyname"), keyParamName);
            p = cb.or(p, cb.like(root.get("merchantID"), keyParamName));
            
            predicate = cb.and(predicate, p);
        }
        
        if(live != null)
            predicate = cb.and(predicate, cb.equal(root.get("live"), keyParamLive));
        
//        if(deleted != null)
//            predicate = cb.and(predicate, cb.equal(root.get("deletedAt"), keyParamDeleted));
        
        if(country != null && !"".equals(country))
            predicate = cb.and(predicate, cb.equal(root.get("country"), keyParamCountry));
        
        if(startDate != null){
            
            Predicate p = cb.between(root.get("datecreated"), keyParamStartDate, keyParamEndDate);
            
            predicate = cb.and(predicate,p);
        }
        
        predicate = cb.and(predicate, cb.equal(root.get("owner"), keyParamOwner));
        
        cq.select(root).where(predicate).orderBy(cb.desc(root.get("id")));
        
        javax.persistence.Query query =  getEntityManager().createQuery(cq);
        
        query.setParameter(keyParamId, 0L);
        query.setParameter(keyParamOwner, 1);
        
        cq.select(cb.count(root)).where(predicate);
        javax.persistence.Query countQuery = getEntityManager().createQuery(cq);
        
        countQuery.setParameter(keyParamId, 0L);
        countQuery.setParameter(keyParamOwner, 1);
        
        if(search != null && !"".equals(search)){
            query.setParameter(keyParamName, "%"+search+"%");
            countQuery.setParameter(keyParamName, "%"+search+"%");
        }
        
        if(live != null){
            query.setParameter(keyParamLive, live);
            countQuery.setParameter(keyParamLive, live);
        }
        
//        if(deleted != null){
//            query.setParameter(keyParamDeleted, deleted);
//            countQuery.setParameter(keyParamDeleted, deleted);
//        }
        
        if(country != null && !"".equals(country)){
            query.setParameter(keyParamCountry, country);
            countQuery.setParameter(keyParamCountry, country);
        }
        
        if(startDate != null){
            query.setParameter(keyParamStartDate, startDate);
            query.setParameter(keyParamEndDate, endDate);
            
            countQuery.setParameter(keyParamStartDate, startDate);
            countQuery.setParameter(keyParamEndDate, endDate);
        }
        
        query.setFirstResult(pageNum);

        // This is nd
        if(pageSize > 0)
            query.setMaxResults(pageNum+pageSize);

        List<CoreMerchant> content = query.getResultList();
        
        long count = ((Long)countQuery.getSingleResult());

        page.setContent(content);
        page.setCount(count);

        query.setHint("org.hibernate.cacheMode", "IGNORE");
        
        page.setContent(content);
        return page;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String getMerchantName(String merchantId){
        
        try{
            if(merchantId == null)
                return null;

            String query = "select companyname from users where merchantID='"+merchantId+"'";

            List data  = getEntityManager().createNativeQuery(query).getResultList();

            if(data == null || data.isEmpty())
                return null;               

            return data.get(0).toString();
        }catch(Exception ex){
            return null;
        }
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public List<CoreMerchant> find(String key, String value, String... joinItems) throws DatabaseException{
        
        try{
            
            getEntityManager().getEntityManagerFactory().getCache().evictAll();

            Map<String, Object> hints = new HashMap<>();
            
            EntityGraph<CoreMerchant> entityGraph = null;
            
            if(joinItems != null && joinItems.length <= 0){
                entityGraph = getEntityManager().createEntityGraph(entityClass);
                entityGraph.addAttributeNodes(joinItems);
                hints.put("javax.persistence.loadgraph", entityGraph);            
            }
            
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<CoreMerchant> query = builder.createQuery(entityClass);

            // This used to map the entities to a class and extract the fields involved
            Root<CoreMerchant> rootQuery = query.from(entityClass);

            ParameterExpression keyParam = builder.parameter(String.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get(key), keyParam));

            Query q = getEntityManager().createQuery(query);
            
            if(joinItems != null && joinItems.length <= 0){
                q.setHint("javax.persistence.loadgraph", entityGraph);
            }
            
            List<CoreMerchant> list = q.setParameter(keyParam, value)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
}

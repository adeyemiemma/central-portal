/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao.maker;

import com.flutterwave.flutter.clientms.dao.*;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.maker.SettlementM;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class SettlementMDao extends BaseHibernateDao<SettlementM> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public SettlementMDao() {
        super(SettlementM.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<SettlementM> findByQuery(int pageNum, int pageSize, Date startDate, Date endDate, Provider provider, Product product, long merchantId, String currency) throws DatabaseException{
        
        Page<SettlementM> page = new Page();
        
        if(currency == null)
            return page;
        
        try{
        // This is used to build the criteria 
            
            String query = " where settlementM.id <> 0 ";
 
            String joinQuery = "";

            if(startDate != null){
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                query +=" and (date(settlementM.startDate) >= date('"+dateFormat.format(startDate)+"') and date(Transaction.endDate) <= date('"+dateFormat.format(endDate)+"') )";
            }
            
            if(provider != null){
                joinQuery += "join provider on provider.id = settlementM.provider_id";
                query += " and settlementM.provider_id = "+provider.getId();
            }
            
            if(product != null){
                joinQuery += "join provider on product.id = settlementM.product_id";
                query += " and settlementM.product_id = "+product.getId();
            }
            
            if(merchantId > 0){
                query += " and settlementM.merchantId = "+merchantId;
            }
            
            query += " and settlementM.currency = "+currency;
            
            List<SettlementM> transactions ;
            if(pageSize > 0 )
                transactions = entityManager.createNativeQuery("select settlementM.* from settlementM "+joinQuery+" "+query, SettlementM.class)
                        .setFirstResult(pageNum)
                        .setMaxResults(pageSize)
                        .getResultList();
            else
                transactions = entityManager.createNativeQuery("select settlementM.* from settlementM "+joinQuery+" "+query +"", SettlementM.class)
                        .getResultList();

            long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from settlementM "+joinQuery+" "+query).getSingleResult().toString());

            page.setContent(transactions);
            page.setCount(count);
            
            return page;
            
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
}

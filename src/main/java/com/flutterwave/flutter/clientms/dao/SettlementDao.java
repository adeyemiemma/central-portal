/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.Settlement;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class SettlementDao extends HibernateDao<Settlement> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public SettlementDao() {
        super(Settlement.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<Settlement> findByProvider(Provider provider) throws DatabaseException{
        
        if(provider == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<Settlement> query = builder.createQuery(Settlement.class);

            // This used to map the entities to a class and extract the fields involved
            Root<Settlement> rootQuery = query.from(Settlement.class);

            ParameterExpression keyParam = builder.parameter(Provider.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("provider"), keyParam));

            List<Settlement> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, provider)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    public List<Settlement> findByQuery(Date startDate, Date endDate, Provider provider) throws DatabaseException{
        
        if(provider == null)
            return null;
        
        try{
        // This is used to build the criteria
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();

            // This is used to build a cq instance from the entity
            CriteriaQuery<Settlement> query = builder.createQuery(Settlement.class);

            // This used to map the entities to a class and extract the fields involved
            Root<Settlement> rootQuery = query.from(Settlement.class);

            ParameterExpression keyParam = builder.parameter(Provider.class);

            // This is used to build the cq that will allow the keyParam
            query.select(rootQuery).where(builder.equal(rootQuery.get("provider"), keyParam));

            List<Settlement> list = getEntityManager().createQuery(query)
                    .setParameter(keyParam, provider)
                    .getResultList();

            if(list.isEmpty())
                return null;

            return list;
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    public List findSummaryByQuery(Date startDate, Date endDate, Provider provider, String currency, Status status, String merchantId) throws DatabaseException{
        
        try{
        // This is used to build the criteria
            String query = " where settlement.id <> 0 ";
 
            String joinQuery = "";

            if(startDate != null){
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                query +=" and (date(settlement.startDate) >= date('"+dateFormat.format(startDate)+"') and date(settlement.endDate) <= date('"+dateFormat.format(endDate)+"') )";
            }
            
            if(provider != null){
                joinQuery += "join provider on provider.id = settlement.provider_id";
                query += " and settlement.provider_id = "+provider.getId();
            }
            
            if(status != null){
                query += " and settlement.status = "+status.ordinal();
            }
            
            if(status != null){
                query += " and settlement.merchantId = '"+merchantId+"'";
            }
            
            query += " and settlement.currency = '"+currency+"'";
            
            List response = entityManager.createNativeQuery("select sum(amount), count(*) from settlement "+joinQuery+" "+query +"")
                        .getResultList();
            
            return response;
            
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
    public Page<Settlement> findByQuery(int pageNum, int pageSize, Date startDate, Date endDate, Provider provider, Product product, long merchantId, String currency) throws DatabaseException{
        
        Page<Settlement> page = new Page();
        
        if(currency == null)
            return page;
        
        try{
        // This is used to build the criteria 
            
            String query = " where settlement.id <> 0 ";
 
            String joinQuery = "";

            if(startDate != null){
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                query +=" and (date(settlement.startDate) >= date('"+dateFormat.format(startDate)+"') and date(settlement.endDate) <= date('"+dateFormat.format(endDate)+"') )";
            }
            
            if(provider != null){
                joinQuery += "join provider on provider.id = settlement.provider_id";
                query += " and settlement.provider_id = "+provider.getId();
            }
            
            if(product != null){
                joinQuery += "join provider on product.id = settlement.product_id";
                query += " and settlement.product_id = "+product.getId();
            }
            
            if(merchantId > 0){
                query += " and settlement.merchantId = "+merchantId;
            }
            
            query += " and settlement.currency = '"+currency+"'";
            
            List<Settlement> transactions ;
            if(pageSize > 0 )
                transactions = entityManager.createNativeQuery("select settlement.* from settlement "+joinQuery+" "+query, Settlement.class)
                        .setFirstResult(pageNum)
                        .setMaxResults(pageSize)
                        .getResultList();
            else
                transactions = entityManager.createNativeQuery("select settlement.* from settlement "+joinQuery+" "+query +"", Settlement.class)
                        .getResultList();

            long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from settlement "+joinQuery+" "+query).getSingleResult().toString());

            page.setContent(transactions);
            page.setCount(count);
            
            return page;
            
        }catch(Exception ex){
            throw new DatabaseException(ex);
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.GlobalRewardSetting;
import com.flutterwave.flutter.clientms.model.RewardSetting;
import com.flutterwave.flutter.clientms.util.EnumUtil;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class GlobalRewardSettingDao extends HibernateDao<GlobalRewardSetting> {

    @PersistenceContext(unitName = Global.PERSISTENT_NAME)
    private EntityManager entityManager;

    public GlobalRewardSettingDao() {
        super(GlobalRewardSetting.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public List<GlobalRewardSetting> find( EnumUtil.TransactionType type, 
            EnumUtil.WeekDay weekDay, EnumUtil.Status status, String currency){
        
        String query = " where id <> 0";
        
        if(type != null){
            query += " and type="+type.ordinal()+"";
        }
        
        if(weekDay != null)
            query += " and weekDay="+weekDay.ordinal()+"";
        
        if(status != null)
            query += " and status="+status.ordinal()+"";
        
        if(currency != null)
            query += " and currency='"+currency+"'";
        
        List<GlobalRewardSetting> settings = getEntityManager().createNativeQuery("select * from global_reward_setting "+query, entityClass).getResultList();
        
        return settings;        
    }
}

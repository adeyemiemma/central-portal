/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.dao;

import com.flutterwave.flutter.clientms.model.RaveTransaction;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.core.dao.HibernateDao;
import com.flutterwave.flutter.core.util.Page;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class RaveTransactionDao extends HibernateDao<RaveTransaction> {

    @PersistenceContext(unitName = Global.RAVE_PERSISTENT_NAME)
    private EntityManager entityManager;

    public RaveTransactionDao() {
        super(RaveTransaction.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public Page<RaveTransaction> search(int pageNum, int pageSize, Date startDate, Date endDate, String transactionType, 
            String fraudStatus, String chargeType, String cycle, String paymentEntity,
            String status, String search, String country, String merchant, Boolean isMerchantId, String currency){
   
            
        Page<RaveTransaction> page = new Page<>();
        
        String query = " where Transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(Transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(Transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(transactionType != null){
            query += " and Transaction.transaction_type='"+transactionType+"'";
        }
        
        if(fraudStatus != null){
            query += " and Transaction.fraud_status='"+fraudStatus+"'";
        }
        
        if(chargeType != null){
            query += " and Transaction.charge_type='"+chargeType+"'";
        }
        
        if(cycle != null){
            query += " and Transaction.cycle='"+cycle+"'";
        }
        
        if(paymentEntity != null){
            query += " and Transaction.payment_entity='"+paymentEntity+"'";
        }
        
        if(status != null){
            query += " and Transaction.status='"+status+"'";
        }
        
        if(currency != null){
            query += " and Transaction.transaction_currency='"+currency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))  || (country != null && !"".equalsIgnoreCase(country) )){
            joinQuery += " join Accounts on Accounts.id=Transaction.merchant_id";
            
            if(country != null)
                query += " and Accounts.country='"+country+"'";
            
            if(merchant != null && (isMerchantId == null || isMerchantId == false))
                query += " and Accounts.business_name like '%"+merchant+"%'";
            else if(merchant != null && isMerchantId == true)
                query += " and Accounts.id ="+merchant+"";
        }
        
        if(search != null && !"".equals(search)){
            query += " and flw_ref like '%"+search+"%' or tx_ref like '%"+search+"%'";
        }
        
        List<RaveTransaction> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select Transaction.* from Transaction "+joinQuery+" "+query+" order by id desc", RaveTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<RaveTransaction>)entityManager.createNativeQuery("select Transaction.* from Transaction "+joinQuery+" "+query +" order by id desc", RaveTransaction.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from Transaction "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);

        
        return page;
    }
    
    public Page<RaveTransaction> searchByStatus(int pageNum, int pageSize, Date startDate, Date endDate, String transactionType, 
            String fraudStatus, String chargeType, String cycle, String paymentEntity,
            String search, String country, String merchant, Boolean isMerchantId, String currency, boolean failed){
   
            
        Page<RaveTransaction> page = new Page<>();
        
        String query = " where Transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(Transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(Transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(transactionType != null){
            query += " and Transaction.transaction_type='"+transactionType+"'";
        }
        
        if(fraudStatus != null){
            query += " and Transaction.fraud_status='"+fraudStatus+"'";
        }
        
        if(chargeType != null){
            query += " and Transaction.charge_type='"+chargeType+"'";
        }
        
        if(cycle != null){
            query += " and Transaction.cycle='"+cycle+"'";
        }
        
        if(paymentEntity != null){
            query += " and Transaction.payment_entity='"+paymentEntity+"'";
        }
        
        if(failed == true){
            query += " and Transaction.status <> 'successful'";
        }else
            query += " and Transaction.status = 'successful'";
        
        if(currency != null){
            query += " and Transaction.transaction_currency='"+currency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))  || (country != null && !"".equalsIgnoreCase(country) )){
            joinQuery += " join Accounts on Accounts.id=Transaction.merchant_id";
            
            if(country != null)
                query += " and Accounts.country='"+country+"'";
            
            if(merchant != null && (isMerchantId == null || isMerchantId == false))
                query += " and Accounts.business_name like '%"+merchant+"%'";
            else if(merchant != null && isMerchantId == true)
                query += " and Accounts.id ="+merchant+"";
        }
        
        if(search != null && !"".equals(search)){
            query += " and flw_ref like '%"+search+"%' or tx_ref like '%"+search+"%'";
        }
        
        List<RaveTransaction> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select Transaction.* from Transaction "+joinQuery+" "+query, RaveTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<RaveTransaction>)entityManager.createNativeQuery("select Transaction.* from Transaction "+joinQuery+" "+query +"", RaveTransaction.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from Transaction "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);

        
        return page;
    }
    
    public Page<RaveTransaction> searchInternationalByStatus(int pageNum, int pageSize, Date startDate, Date endDate, String transactionType, 
            String fraudStatus, String chargeType, String cycle, String paymentEntity,
            String search, String country, String merchant, Boolean isMerchantId, String localCurrency, boolean failed){
   
            
        Page<RaveTransaction> page = new Page<>();
        
        String query = " where Transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(Transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(Transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(transactionType != null){
            query += " and Transaction.transaction_type='"+transactionType+"'";
        }
        
        if(fraudStatus != null){
            query += " and Transaction.fraud_status='"+fraudStatus+"'";
        }
        
        if(chargeType != null){
            query += " and Transaction.charge_type='"+chargeType+"'";
        }
        
        if(cycle != null){
            query += " and Transaction.cycle='"+cycle+"'";
        }
        
        if(paymentEntity != null){
            query += " and Transaction.payment_entity='"+paymentEntity+"'";
        }
        
        if(failed == true){
            query += " and Transaction.status <> 'successful'";
        }else
            query += " and Transaction.status = 'successful'";
        
        if(localCurrency != null){
            query += " and Transaction.transaction_currency <> '"+localCurrency+"'";
        }
        
        if((merchant != null && !"".equalsIgnoreCase(merchant))  || (country != null && !"".equalsIgnoreCase(country) )){
            joinQuery += " join Accounts on Accounts.id=Transaction.merchant_id";
            
            if(country != null)
                query += " and Accounts.country='"+country+"'";
            
            if(merchant != null && (isMerchantId == null || isMerchantId == false))
                query += " and Accounts.business_name like '%"+merchant+"%'";
            else if(merchant != null && isMerchantId == true)
                query += " and Accounts.id ="+merchant+"";
        }
        
        if(search != null && !"".equals(search)){
            query += " and flw_ref like '%"+search+"%' or tx_ref like '%"+search+"%'";
        }
        
        List<RaveTransaction> transactions ;
        if(pageSize > 0 )
            transactions = entityManager.createNativeQuery("select Transaction.* from Transaction "+joinQuery+" "+query, RaveTransaction.class)
                    .setFirstResult(pageNum)
                    .setMaxResults(pageSize)
                    .getResultList();
        else
            transactions = (List<RaveTransaction>)entityManager.createNativeQuery("select Transaction.* from Transaction "+joinQuery+" "+query +"", RaveTransaction.class)
                    .getResultList();
        
        long count = Long.parseLong(entityManager.createNativeQuery("select count(*) from Transaction "+joinQuery+" "+query).getSingleResult().toString());

        page.setContent(transactions);
        page.setCount(count);

        
        return page;
    }
    
    public List<String> getStatus(){
        
        List<String> list = entityManager.createNativeQuery("select distinct(status) from Transaction").getResultList();
        
        return list;
    }
    
    public List getSummaryWithDate(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where Transaction.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(Transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(Transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and Transaction.transaction_currency='"+currency+"'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
            joinQuery += " join Accounts on Accounts.id=Transaction.merchant_id";
           
            query += " and Accounts.business_name like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(appfee), count(*), date(Transaction.createdAt), status  from Transaction "+joinQuery+" "+query + " group by date(Transaction.createdAt), status")
                .getResultList();
        
        return list;
    }
    
    public List getTotalSummary(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where Transaction.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(Transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(Transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and Transaction.transaction_currency='"+currency+"'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
            joinQuery += " join Accounts on Accounts.id=Transaction.merchant_id";
           
            query += " and Accounts.business_name like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(appfee), status from Transaction "+joinQuery+" "+query + " group by status ")
                .getResultList();
        
        return list;
    }
    
    public List getMCashSummaryResult(Date startDate, Date endDate, String currency){
        
        String query = " where Transaction.id <> 0 and preferences.availablesettingId = 14 and payment_entity='mcash-offline'";
        
        String joinQuery = " join Accounts on Accounts.id=Transaction.merchant_id join preferences on preferences.AccountId = Transaction.merchant_id ";
        
        joinQuery += " join (select account_number, bankCode, AccountId from settlement_account  where settlement_account.`AccountId` group by AccountId) as settlement_account on settlement_account.AccountId = Transaction.merchant_id ";
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            query += " and TIMESTAMP(Transaction.createdAt) between TIMESTAMP('" + dateFormat.format(startDate) + "') and TIMESTAMP('" + dateFormat.format(endDate) + "')";
        }
        
        if(currency != null)
            query += " and Transaction.transaction_currency='"+currency+"'";
            
        
        List result = entityManager.createNativeQuery("select sum(amount), count(*), Accounts.business_name, preferences.preference_value, sum(appfee), settlement_account.account_number, settlement_account.bankCode  from Transaction "+joinQuery+" "+query+" group by Transaction.merchant_id").getResultList();
        
        return result;
        
    }
    
    public List getTotalSummaryNoGrouping(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where Transaction.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(Transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(Transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and Transaction.transaction_currency='"+currency+"'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
            joinQuery += " join Accounts on Accounts.id=Transaction.merchant_id";
           
            query += " and Accounts.business_name like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), sum(appfee), status from Transaction "+joinQuery+" "+query + " ")
                .getResultList();
        
        return list;
    }
    
    public List getSummaryByStatus(Date startDate, Date endDate, String currency, String merchant){
        
        String query = "", joinQuery = "";
        
        query += " where Transaction.id <> 0 ";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(Transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(Transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(currency != null){
            query += " and Transaction.transaction_currency='"+currency+"'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
            joinQuery += " join Accounts on Accounts.id=Transaction.merchant_id";
           
            query += " and Accounts.business_name like '%"+merchant+"%'";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), status from Transaction "+joinQuery+" "+query + " group by status ")
                .getResultList();
        
        return list;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List getReportSummary( Date startDate, Date endDate,
            String transactionStatus, String searchReference, String merchant, String currency){
    
        String query = " where Transaction.id <> 0";
        String joinQuery = "";
        
        if(startDate != null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            query +=" and date(Transaction.createdAt) >= date('"+dateFormat.format(startDate)+"') and date(Transaction.createdAt) <= date('"+dateFormat.format(endDate)+"')";
        }
        
        if(transactionStatus != null){
            query += " and status='"+transactionStatus+"'";
        }
        
        if(currency != null){
            query += " and Transaction.transaction_currency='"+currency+"'";
        }
        
        if(searchReference != null && !"".equals(searchReference)){
            query += " and tx_ref like '%"+searchReference+"%' or flw_ref like '%"+searchReference+"%'";
        }
        
        if(merchant != null && !"".equalsIgnoreCase(merchant)){
            joinQuery += " join Accounts on Accounts.id=Transaction.merchant_id";
           
            query += " and Accounts.id = "+merchant+"";
        }
        
        List list = entityManager.createNativeQuery("select sum(amount), date(Transaction.createdAt) as transaction_date, IF(`status` like '%successful%', 'success', 'failed') as t_status, count(*) as volume  from Transaction "+joinQuery+" "+ query +" group by `transaction_date`, t_status;")
                .getResultList();
        
        
        return list;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;


//import fr.opensagres.xdocreport.itext.extension.IPdfWriterConfiguration;
//import com.documents4j.api.DocumentType;
//import com.documents4j.api.IConverter;
//import com.documents4j.job.LocalConverter;
import com.flutterwave.flutter.clientms.service.SmtpMailSender;
import fr.opensagres.poi.xwpf.converter.core.XWPFConverterException;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
//import fr.opensagres.poi.xwpf.converter.core.XWPFConverterException;
//import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
//import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
//import fr.opensagres.poi.xwpf.converter.core.XWPFConverterException;
//import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
//import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
//import com.itextpdf.xmp.options.Options;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
//import org.apache.poi.xwpf.converter.core.XWPFConverterException;
//import org.apache.poi.xwpf.converter.pdf.PdfConverter;
//import org.apache.poi.xwpf.converter.pdf.PdfOptions;
//import org.apache.poi.xwpf.converter.core.XWPFConverterException;
//import org.apache.poi.xwpf.converter.pdf.PdfConverter;
//import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class WordUtil {

    private final String username = Global.EMAIL_USERNAME;
    private final String password = Global.EMAIL_PASSWORD;
    
    static String compliantFolder =  "/home/ubuntu/flw_client_ms_compliance"; //"/Users/emmanueladeyemi/Downloads"; //"/home/ubuntu/flw_client_ms_compliance";

    static PdfOptions options;
    
    Logger logger = Logger.getLogger(WordUtil.class);
    
    private VelocityEngine velocityEngine;
    
    @PostConstruct
    public void init(){
        
        try {
            Properties props = new Properties();
//            props.put("resource.loader", "class");
//            props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
//            Properties props = new Properties();
            props.put("resource.loader", "class");
            props.put("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.SimpleLog4JLogSystem");
            props.put("runtime.log.logsystem.log4j.category", "velocity");
            props.put("runtime.log.logsystem.log4j.logger", "velocity");
            props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            VelocityEngine engine = new VelocityEngine(props);
//            VelocityEngine engine = new VelocityEngine(props);
            engine.init();
            engine.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.CommonsLogLogChute");

            setVelocityEngine(engine);
            
        } catch (VelocityException ex) {
        }
    }
    
    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

    public VelocityEngine getVelocityEngine() {
        return velocityEngine;
    }
    
    @Asynchronous
    public void getComplianceFile(String companyName, String product, String address, 
            String country, String email, String category, String []cc) {

        try {
            
            String fName;
            if(product.equalsIgnoreCase("rave") || product.equalsIgnoreCase("ravepay"))
                fName = "Rave";
            else if(product.equalsIgnoreCase("moneywave")){
                fName = "Moneywave";
            }else
                fName = "Core";
                
            logger.info("GETTING COMPLIANCE FILE "+fName);
            
//            InputStream filePath = PdfUtil.class.getClassLoader().getResourceAsStream("compliance_"+fName.toLowerCase()+".docx");
            // /Users/emmanueladeyemi/Downloads
            // /home/ubuntu/compliance_template/
            File filePath = new File("/home/ubuntu/compliance_template/compliance_"+fName.toLowerCase()+".docx");
            
            
//            logger.info("SOMEONE GOT FILE PATH "+filePath.isFile());
            logger.info("SOMEONE GOT FILE PATH ");
            
            File directory = new File(compliantFolder);

            if (!directory.exists()) {
                Files.createDirectories(directory.toPath(), new FileAttribute<?>[0]);
            }

//            File file = new File(filePath); // This is the compliance file itself

            logger.info("GETTING FILE BASE");
            
            String fileBase = compliantFolder+"/"+companyName.replaceAll(" ", "_");
            
            File companyFile = new File(fileBase+".docx"); // This is the company file to be created
            
            if (companyFile.exists()) {
                companyFile.delete();
            }
            
            companyFile.createNewFile();
            
            logger.info("CREATED NEW FILE");
            
//            if(file == null){
//                throw new Exception("Compliance file not found");
//            }
            
            XWPFDocument document = new XWPFDocument(new FileInputStream(filePath));
            
            List<XWPFParagraph> paragraphs = document.getParagraphs();

            SimpleDateFormat monthFormatter = new SimpleDateFormat("MMMM");
            
            String month = monthFormatter.format(new Date());
            int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            int year = Calendar.getInstance().get(Calendar.YEAR);
            
            paragraphs.stream().map((paragraph) -> {
                replace("<INSERT COMPANY NAME>", companyName, true, 20, true, paragraph);
                return paragraph;
            }).map((paragraph) -> {
                replace("<INSERT JURIDICTION>", country, true, 10, true, paragraph);
                return paragraph;
            }).map((paragraph) -> {
                replace("<INSERT ADDRESS>", address, false, 10, true,paragraph );
                return paragraph;
            }).map((paragraph) -> {
                replace("<INSERT DAY>", day+"", true, 10, true, paragraph);
                return paragraph;
            }).map((paragraph) -> {
                replace("<INSERT YEAR>", year+"", true, 10, true, paragraph);
                return paragraph;
            }).map((paragraph) -> {
                replace("<INSERT MONTH>", month+"", true, 10, true, paragraph);
                return paragraph;
            }).forEachOrdered((paragraph) -> {
                replace("<INSERT CATEGORY>", category+"", true, 10, true, paragraph);
            });
            
            logger.info("WRITING TO OUTPUT STREAM");
            
            FileOutputStream fileOutputStream = new FileOutputStream(companyFile);
            
            document.write(fileOutputStream);
            
            
            
            logger.info("DONE WRITING FILE");
            
            options = PdfOptions.create();
            
            logger.info("CREATING PDF ");
            
            File generatedPDf = new File(fileBase +".pdf");
            
            logger.info("DONE CREATING PDF ");
            
            FileOutputStream outputStream = new FileOutputStream(generatedPDf);
            
            logger.info("SENDING PDF ");
            
//            document.createNumbering();
            
            
//            ConvertDocToPdfitext cwordToPdf = new (ConvertDocToPdfitext);
            PdfConverter.getInstance().convert( document, outputStream, options );
            
//            IConverter converter = LocalConverter.builder().build();
//            converter.convert(new FileInputStream(companyFile)).as(DocumentType.DOCX.DOCX).to(outputStream).as(DocumentType.PDF).execute();
            
            outputStream.close();
            
            sendMail(email, companyName, product, generatedPDf.getAbsolutePath(), cc);
            
            logger.info("DONE SENDING PDF ");
//            companyFile.delete();
            
//            generatedPDf.delete();
            
        }catch(IOException  | XWPFConverterException ex){
            
            ex.printStackTrace();
            logger.error(ex);
        }        
    }

    
    private boolean sendMail(String email, String companyName, String product, String attachmentPath, String[] cc){
    
        try {
                
            VelocityContext context = new VelocityContext();
            context.put("companyName", companyName);
            context.put("product", product);
            
            Template template = getVelocityEngine().getTemplate("submit_.vm");
            
            StringWriter writer = new StringWriter();   
            template.merge(context, writer);
            
            
            SmtpMailSender mailSender = new SmtpMailSender(username, password,Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);
            
            mailSender.sendHtmlMail("Flutterwave Compliance Team", "compliance@flutterwavego.com", "Flutterwave Compliance", writer.toString(), new String[]{email}, cc, null, attachmentPath);
            
            return true;
            
        } catch (Exception ex) {
            logger.error(ex);
        }
        
        return false;
    }
    
    public static void replace(String searchValue, String replaceText, boolean bold, int size, boolean capitalized, XWPFParagraph paragraph) {

        if (hasReplaceableItem(paragraph.getText(), searchValue)) {
            String replacedText = paragraph.getText().replaceAll(searchValue, replaceText);

            removeAllRuns(paragraph);

            insertReplacementRuns(paragraph, replacedText, bold, size, capitalized);
        }
        
    }
    
    public static void replace(String searchValue, String replaceText,  boolean bold,int size, boolean capitalized, XWPFDocument document) {
        List<XWPFParagraph> paragraphs = document.getParagraphs();

        for (XWPFParagraph paragraph : paragraphs) {
            if (hasReplaceableItem(paragraph.getText(), searchValue)) {
                String replacedText = paragraph.getText().replaceAll(searchValue, replaceText);

                removeAllRuns(paragraph);

                insertReplacementRuns(paragraph, replacedText, bold, size, capitalized);
            }
        }
    }

    private static void insertReplacementRuns(XWPFParagraph paragraph, String replacedText, boolean bold, int fontSize, boolean capitalized) {
        String[] replacementTextSplitOnCarriageReturn = replacedText.split("\n");

        for (int j = 0; j < replacementTextSplitOnCarriageReturn.length; j++) {
            String part = replacementTextSplitOnCarriageReturn[j];

            XWPFRun newRun = paragraph.insertNewRun(j);
            newRun.setText(part);
            newRun.setBold(bold);
            newRun.setFontSize(fontSize);
            newRun.setCapitalized(capitalized);

            if (j + 1 < replacementTextSplitOnCarriageReturn.length) {
                newRun.addCarriageReturn();
            }
        }
    }

    private static void removeAllRuns(XWPFParagraph paragraph) {
        int size = paragraph.getRuns().size();
        for (int i = 0; i < size; i++) {
            paragraph.removeRun(0);
        }
    }

    private static boolean hasReplaceableItem(String runText, String searchValue) {
        return runText.contains(searchValue);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.clientms.model.recon.PdfExportModel;
import com.lowagie.text.DocumentException;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPage;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author emmanueladeyemi
 */
public class ReconPdfUtil {

    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 8,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 7,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 8,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 7,
            Font.BOLD);
    private static Font small = new Font(Font.FontFamily.TIMES_ROMAN, 7,
            Font.NORMAL);

    public static String generatePdf(String author, String title, List<String> headers, List<PdfExportModel> values, String... exclude) {

        try {

            File file = new File("tempdf.pdf");
            
            if(file.exists())
                file.delete();
            
            file.createNewFile();
//            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("tempdf.pdf"));
            document.setPageSize(PageSize.A4.rotate());
            document.open();
            Rotate event = new Rotate();
//            event.setOrientation(PdfPage.LANDSCAPE);
//            pdfWriter.setPageEvent(event);
//            float fntSize = 6.7f;
//            float lineSpacing = 10f;
//                p = new Paragraph(new Phrase(lineSpacing,line,
//                   FontFactory.getFont(FontFactory.COURIER, fntSize)));
    
            addMetaData(document, author);
            addTitlePage(document, title);
            createTable(document, headers, values, exclude);
            
            document.close();
            pdfWriter.close();
            
            return "tempdf.pdf";
            
        } catch (Exception ex) {
            Logger.getLogger(ReconPdfUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    private static void addMetaData(Document document, String author) {
        document.addTitle("Settlement Report");
        document.addSubject("Settlement Report");
//        document.addKeywords("Java, PDF, iText");
        document.addAuthor(author);
        document.addCreator(author);
    }

    private static void addTitlePage(Document document, String title)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph(title, catFont));

        addEmptyLine(preface, 1);
        // Will create: Report generated by: _name, _date
        preface.add(new Paragraph(
                "Report generated on " + new Date(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                catFont));
        addEmptyLine(preface, 3);

        try {
            document.add(preface);
            // Start a new page
//        document.newPage();
        } catch (com.itextpdf.text.DocumentException ex) {
            Logger.getLogger(ReconPdfUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void createTable(Document document, List<String> headers, List<PdfExportModel> values, String... exclude)
                        throws BadElementException, DocumentException {

        int excludeSize = 0;
        if(exclude != null)
            excludeSize = exclude.length;

        PdfPTable table = new PdfPTable(headers.size()-excludeSize);
        table.setWidthPercentage(100);
        table.setSpacingBefore(0f);
        table.setSpacingAfter(0f);
        
        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).map(x -> x.toLowerCase()).collect(Collectors.toList());
        }
        
        List<String> excludeAll = exclusions;
        
        headers = headers.stream().filter(x -> excludeAll != null && excludeAll.indexOf(x.toLowerCase()) <= 0).map(x -> x.toUpperCase()).collect(Collectors.toList());
        
        for(String header : headers){
            
            PdfPCell c1 = new PdfPCell(new Phrase(header,smallBold ));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
        }
        
        table.setHeaderRows(1);

        for (PdfExportModel transaction : values) {

            // This used reflection to fill the field in the date
            for (Field field : transaction.getClass().getDeclaredFields()) {

                field.setAccessible(true);

                try {
                    Object object = field.get(transaction);

                    if (exclusions != null) {

                        int index = exclusions.indexOf(field.getName());

                        if (index > -1) {
                            continue;
                        }
                    }

                    String content = object != null ? object.toString() : "";

//                    System.out.println(content);
                    
                    PdfPCell c1 = new PdfPCell(new Phrase(content, small));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(c1);

                } catch (Exception ex) {
                    
                    if(ex != null)
                        ex.printStackTrace();
                }
            }
        }
        
        try {
            document.add(table);
        } catch (com.itextpdf.text.DocumentException ex) {
            Logger.getLogger(ReconPdfUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    
    public static class Rotate extends PdfPageEventHelper {
 
        protected PdfNumber orientation = PdfPage.PORTRAIT;
 
        public void setOrientation(PdfNumber orientation) {
            this.orientation = orientation;
        }
 
        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            writer.addPageDictEntry(PdfName.ROTATE, orientation);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosMerchantCustomer;
import com.flutterwave.flutter.clientms.model.PosProvider;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.model.PosTransactionNew;
import com.flutterwave.flutter.clientms.model.TransactionNew;
import com.flutterwave.flutter.clientms.model.TransactionWarehouse;
import com.flutterwave.flutter.clientms.model.recon.FlutterTransaction;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.collections.transformation.SortedList;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author adeyemi
 */
public class TransactionUtility {

    public static Map<Class, Class> map = new HashMap();
    static final Map<String, String> mapString = new HashMap<>();

    static Logger logger = Logger.getLogger(TransactionUtility.class);

    static {
        mapString.put("566", "NGN");
        mapString.put("840", "USD");
        mapString.put("936", "GHS");
        mapString.put("710", "ZAR");
        buildWrapperClass();
    }

    public static Map populateIndex() {

        Map<String, String> fileHeader = new HashMap<>();
        fileHeader.put("ID", "fileId");
        fileHeader.put("BATCHID", "batchId");
        fileHeader.put("DIRECTION", "direction");
        fileHeader.put("EXPID", "expId");
        fileHeader.put("FILENAME", "fileName");
        fileHeader.put("ASSOCIATIONID", "associationId");
        fileHeader.put("BATCH_IMPORTLOCATIONID", "batchImportLocationId");
        fileHeader.put("BATCH_EXPORTLOCATIONID", "batchExportLocationId");
        fileHeader.put("OPERDATE", "operationDate");
        fileHeader.put("GROUPCOUNT", "groupCount");
        fileHeader.put("TRANCOUNT", "transactionCount");
        fileHeader.put("MESSCOUNT", "messageCount");
        fileHeader.put("TRAN_IMPORTLOCATIONID", "transactionImportLocationId");
        fileHeader.put("TRAN_EXPORTLOCATIONID", "transactionExportLocationId");
        fileHeader.put("RRN", "rrn");
        fileHeader.put("ORIGCLEARAMT", "originClearanceAmount");
        fileHeader.put("ORIGCLEARCCY", "originClearanceCurrency");
        fileHeader.put("ORIGINALAMT", "originalAmount");
        fileHeader.put("ORIGINALCCY", "originalCurrency");
        fileHeader.put("DESTCLEARAMT", "destinationClearanceAmount");
        fileHeader.put("DESTCLEARCCY", "destinationClearanceCurrency");
        fileHeader.put("DESTRECONAMT", "destinationReconAmount");
        fileHeader.put("DESTRECONCCY", "destinationReconCurrency");
        fileHeader.put("ISSCOUNTRY", "issCurrency");
        fileHeader.put("ORIGCLEARDATE", "originClearDate");
        fileHeader.put("ORIGTIME", "originTime");
        fileHeader.put("MASKEDPAN", "maskedPan");
        fileHeader.put("TERMNAME", "terminalName");
        fileHeader.put("CARDACCEPTORID", "cardAcceptorId");
        fileHeader.put("MERCHANTTITLE", "merchantTitle");
        fileHeader.put("MERCHANTLOCATION", "merchantLocation");
        fileHeader.put("MERCHANTCITY", "merchantCity");
        fileHeader.put("MERCHANTREGION", "merchantRegion");
        fileHeader.put("MCC", "mcc");
        fileHeader.put("APPROVALCODE", "approvalCode");
        fileHeader.put("RESPCODE", "responseCode");
        fileHeader.put("EXTRESPCODE", "externalResponseCode");
        fileHeader.put("EXPARN", "expArn");
        fileHeader.put("EXPTRANID", "expTransactionId");
        fileHeader.put("ACQUIRERID", "acquiredId");
        fileHeader.put("IMPBATCHID", "impBatchId");
        fileHeader.put("EXPBATCHID", "expBatchId");
        fileHeader.put("NOTIFBATCHID", "notificationBatchId");
        fileHeader.put("SETTLERBATCHID", "settlerBatchId");
        fileHeader.put("REJECTMESSAGE", "rejectMessage");
        fileHeader.put("ORIGIIN", "origIIN");
        fileHeader.put("DESTIIN", "destIIN");
        fileHeader.put("AID", "aid");
        fileHeader.put("TRANTYPE", "tranType");
        fileHeader.put("TRANTYPE_DESC", "tranTypeDesc");
        fileHeader.put("TRANCODE", "tranCode");
        fileHeader.put("TRANCODE_DESC", "tranCodeDesc");
        fileHeader.put("BATCHPHASE", "batchPhase");
        fileHeader.put("TRANPHASE", "tranPhase");
        fileHeader.put("ACCOUNTAMT", "accountAmount");
        fileHeader.put("ACCOUNTCCY", "accountCurrency");
        fileHeader.put("SETTLEAMT", "settleAmount");
        fileHeader.put("SETTLECCY", "settleCurrency");
        fileHeader.put("FEERULE_IRD", "feeRule");

        buildWrapperClass();

        return fileHeader;
    }

    public static Map populateTransactionHeaders() {
        Map<String, String> fileHeader = new HashMap<>();

        fileHeader.put("Transaction Number", "transactionNumber");
        fileHeader.put("Transaction ID", "transactionId");
        fileHeader.put("Date", "date");
        fileHeader.put("Merchant ID", "merchantId");
        fileHeader.put("Order Reference", "orderReference");
        fileHeader.put("Merchant Transaction Reference", "merchantTransactionReference");
        fileHeader.put("Transaction Type", "transactionType");
        fileHeader.put("Acquirer ID", "acquirerID");
        fileHeader.put("Bank Merchant ID/SE Number", "bankMerchantId");
        fileHeader.put("Batch Number", "batchNumber");
        fileHeader.put("Currency", "currency");
        fileHeader.put("Currency Code", "currencyCode");
        fileHeader.put("Amount", "amount");
        fileHeader.put("RRN", "rrn");
        fileHeader.put("Response Code", "responseCode");
        fileHeader.put("Acquirer Response Code", "acquirerResponseCode");
        fileHeader.put("Authorisation Code", "authorisationCode");
        fileHeader.put("Operator ID", "operatorId");
        fileHeader.put("Merchant Transaction Source", "merchantTransactionSource");
        fileHeader.put("Order Date", "orderDate");
        fileHeader.put("Card Type", "cardType");
        fileHeader.put("Card Number", "pan");
        fileHeader.put("Card Expiry Month", "cardExpiryMonth");
        fileHeader.put("Card Expiry Year", "cardExpiryYear");
        fileHeader.put("Dialect CSC Result Code", "dialectCSCResultCode");

        return fileHeader;

    }

    public static Class getFieldType(Class type, String fieldName) {

        try {
            Field field = type.getDeclaredField(fieldName);

            return field.getType();
        } catch (NoSuchFieldException ex) {
            logger.log(Priority.ERROR, ex);
        } catch (SecurityException ex) {
            logger.log(Priority.ERROR, ex);
        }

        return null;
    }

    public static boolean set(Object object, String fieldName, Object fieldValue) {
        Class<?> clazz = object.getClass();
        while (clazz != null) {
            try {
                Field field = clazz.getDeclaredField(fieldName);

                Object value = convertToType(fieldValue, map.get(field.getType()));

                field.setAccessible(true);
                field.set(object, value);
                return true;
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            } catch (Exception e) {
                if (e != null) {
                    e.printStackTrace();
                }

                throw new IllegalStateException(e);
            }
        }
        return false;
    }

    public static void buildWrapperClass() {

        map.put(boolean.class, Boolean.class);
        map.put(byte.class, Byte.class);
        map.put(short.class, Short.class);
        map.put(char.class, Character.class);
        map.put(int.class, Integer.class);
        map.put(long.class, Long.class);
        map.put(float.class, Float.class);
        map.put(double.class, Double.class);
        map.put(String.class, String.class);
        map.put(Date.class, Date.class);

    }

    public static Object convertToType(Object obj, Class type) {

        try {
            if (type == String.class || type == Date.class) {
                return obj;
            }

            String value = obj.toString();

            if (type == Integer.class || type == Long.class) {

                if (value.contains(".")) {
                    value = value.substring(0, value.indexOf("."));
                }
            }

            Method method = type.getMethod("valueOf", String.class);

            return method.invoke(type, value);

        } catch (NoSuchMethodException | SecurityException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException ex) {
            logger.error(ex);
        }

        return null;
    }

    public static String getFieldNames(Class type, String concatStr) {

        String temp = "";
        for (Field field : type.getDeclaredFields()) {

            field.setAccessible(true);

            temp = String.join(concatStr, temp, field.getName());
        }

        return temp;
    }

    public static String generateBatchId() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHmmss");

        String batchId = "batch" + dateFormat.format(new Date());

        return batchId;
    }

    public static String getCurrencyName(String currencyCode) {

        if (currencyCode == null) {
            return null;
        }

        return mapString.getOrDefault(currencyCode, currencyCode);
    }

    public static String getCurrencyCode(String currencyName) {

        if (currencyName == null) {
            return null;
        }

        return mapString.entrySet().stream().filter(x -> x.getValue().equalsIgnoreCase(currencyName)).map(x -> x.getKey())
                .findFirst().orElse(currencyName);
    }

    public static Date formatDate(String date) {

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

            Date parsedDate = dateFormat.parse(date);

            return parsedDate;
        } catch (ParseException ex) {
            logger.error(ex);
        }

        return null;
    }

    public static String formatDateToString(Date date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        return dateFormat.format(date);
    }

    public static String formatDateToStringWithTime(Date date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return dateFormat.format(date);
    }

    public static FlutterTransaction getTransaction(JsonObject jsonObject) {

        FlutterTransaction flutterTransaction = new FlutterTransaction();

        for (Field field : flutterTransaction.getClass().getDeclaredFields()) {

            field.setAccessible(true);

            if (field.getName().equalsIgnoreCase("id")) {
                continue;
            }

            JsonValue jsonValue = jsonObject.get(field.getName());

            if (jsonValue == null) {
                continue;
            }

//            if(jsonValue.getValueType() == JsonValue.ValueType.STRING || jsonValue.getValueType() == JsonValue.ValueType.NUMBER)
            String value = jsonValue.toString().replaceAll("\"", "");

            if ("null".equalsIgnoreCase(value)) {
                continue;
            }

            set(flutterTransaction, field.getName(), value);
//            else
//                set(flutterTransaction, field.getName(), jsonValue.toString());

        }

        return flutterTransaction;
    }

    public static List<String> getScheme() {

        List<String> schemes = new ArrayList<>();
        schemes.add("Verve");
        schemes.add("Visa");
        schemes.add("MasterCard");

        return schemes;
    }

    public static List<String> getClassFields(Class object, String... exclude) {

        List<String> list = new ArrayList<>();

        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).map(x -> x.toUpperCase()).collect(Collectors.toList());
        }

        for (Field field : object.getDeclaredFields()) {

            field.setAccessible(true);
//            if(field.getType() == String.class){
            //                System.out.println(field.getName());

            try {

                if (exclusions != null) {

                    int index = exclusions.indexOf(field.getName().toUpperCase());

                    if (index > -1) {
                        continue;
                    }
                }

                list.add(field.getName().toUpperCase());

                //                System.out.println(object.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

//        }
        return list;
    }

    public static String getResponseCode(String response, boolean isResponseCode) {

        String[] codes = response.split("-");

        if (isResponseCode == true) {
            if ("0".equalsIgnoreCase(codes[0].trim())) {
                return "00";
            } else {
                return codes[0];
            }
        }

        return codes[1];
    }

    public static String getSchemeCode(String name) {

        if (name == null) {
            return null;
        }

        switch (name.toLowerCase()) {
            case "mastercard":
                return "MC";
            case "visa":
                return "VC";
            case "verve":
                return "VERVE";
            case "american_express":
            case "americanexpress":
                return "AMEX";
            case "discover":
                return "DISC";
            default:
                return name;
        }
    }

    public static TransactionNew buildTransaction(String data) {

        if (data == null) {
            return null;
        }

        JSONObject jsonObject = new JSONObject(data);
        TransactionNew transaction = new TransactionNew();

        transaction.setProvider(jsonObject.optString("sourceid", null));
        
        String amount  = jsonObject.optString("transactionamount", "0.0");
        
        if("null".equalsIgnoreCase(amount))
                transaction.setAmount(0.00);
           else
               transaction.setAmount(Double.parseDouble(amount));
        
//        transaction.setAmount(Double.parseDouble(jsonObject.optString("transactionamount", "0.0") + ""));
        transaction.setAuthenticationModel(jsonObject.optString("authenticationmodel", null));
        transaction.setCardCountry(jsonObject.optString("cardcountry", "NG") + "");
        transaction.setCardMask((String) jsonObject.optString("cardmask", null));
        transaction.setCreatedOn(new Date());
        transaction.setDatetime((String) jsonObject.optString("datetime", null));
        transaction.setResponseCode((String) jsonObject.optString("responsecode", null));
        transaction.setResponseMessage((String) jsonObject.optString("responsemessage", null));
        transaction.setTransactionNarration((String) jsonObject.optString("transactionnarration", null));
        transaction.setTransactionType((String) jsonObject.optString("transactiontype", null));
        transaction.setFlwTxnReference((String) jsonObject.optString("flutterwavetransactionreference", null));
        transaction.setMerchantTxnReference((String) jsonObject.optString("merchanttranscationreference", null));
        transaction.setTransactionCountry((String) jsonObject.optString("transactioncountry", null));
        transaction.setTransactionCurrency((String) jsonObject.optString("transactioncurrency", null));
        transaction.setMerchantId((String) jsonObject.optString("merchantid", null));
        transaction.setCardScheme((String) jsonObject.optString("cardscheme", null));
        transaction.setSettlementState(Utility.SettlementState.NONE);

        JSONArray listData = jsonObject.optJSONArray("processorreference");

        if (listData != null) {


            for(int i = 0; i < listData.length(); i++){
                JSONObject value = (JSONObject) listData.optJSONObject(i);

                String key = value.optString("name");
                String keyValue = value.optString("value");
                
                switch(key){
                    case "transactionnumber" :
                        transaction.setTransactionNumber(keyValue);
                        break;
                    case "retrievalreferencenumber" :                       
                        transaction.setRrn(keyValue);
                        break;
                        
                    case "vpcmerchant" : { 
                        transaction.setMerchantName(keyValue);
//                        transaction.setV(keyValue);
                        break;
                    }
                        
                   case "postresponsemessage" :  
//                        transaction.setPostResponseCode(keyValue);
                        transaction.setDisburseResponseMessage(keyValue);
                        break;   
                    
                    case "postresponsecode" :    
//                        transaction.setPostResponseCode(keyValue);
                        transaction.setDisburseResponseCode(keyValue);
                        break;  
                        
                    case "sendername" :                       
                        transaction.setSenderName(keyValue);
                        break;     
                    
                    case "sourcebankcode" :                       
                        transaction.setSourceBankCode(keyValue);
                        break; 
                    case "sourceaccount" :                       
                        transaction.setSourceAccount(keyValue);
                        break;       
                }

            }
        }
        
        return transaction;
    }
    
    public static TransactionWarehouse buildTransactionWH(String data) {

        if (data == null) {
            return null;
        }

        JSONObject jsonObject = new JSONObject(data);
        TransactionWarehouse transaction = new TransactionWarehouse();

        transaction.setProvider(jsonObject.optString("sourceid", null));
        
        String amount  = jsonObject.optString("transactionamount", "0.0");
        
        if("null".equalsIgnoreCase(amount))
                transaction.setAmount(BigDecimal.ZERO);
           else
               transaction.setAmount(new BigDecimal(amount));
        
        transaction.setAuthenticationModel(jsonObject.optString("authenticationmodel", null));
        transaction.setCardCountry(jsonObject.optString("cardcountry", "NG") + "");
        transaction.setCardMask((String) jsonObject.optString("cardmask", null));
        transaction.setCreatedOn(new Date());
        transaction.setDatetime((String) jsonObject.optString("datetime", null));
        transaction.setResponseCode((String) jsonObject.optString("responsecode", null));
        transaction.setResponseMessage((String) jsonObject.optString("responsemessage", null));
        transaction.setTransactionNarration((String) jsonObject.optString("transactionnarration", null));
        transaction.setTransactionType((String) jsonObject.optString("transactiontype", null));
        transaction.setFlwTxnReference((String) jsonObject.optString("flutterwavetransactionreference", null));
        transaction.setMerchantTxnReference((String) jsonObject.optString("merchanttranscationreference", null));
        transaction.setTransactionCountry((String) jsonObject.optString("transactioncountry", null));
        transaction.setTransactionCurrency((String) jsonObject.optString("transactioncurrency", null));
        transaction.setMerchantId((String) jsonObject.optString("merchantid", null));
        transaction.setCardScheme((String) jsonObject.optString("cardscheme", null));
        transaction.setSettlementState(Utility.SettlementState.NONE);

        JSONArray listData = jsonObject.optJSONArray("processorreference");

        if (listData != null) {


            for(int i = 0; i < listData.length(); i++){
                JSONObject value = (JSONObject) listData.optJSONObject(i);

                String key = value.optString("name");
                String keyValue = value.optString("value");
                
                switch(key){
                    case "transactionnumber" :
                        transaction.setTransactionNumber(keyValue);
                        break;
                    case "retrievalreferencenumber" :                       
                        transaction.setRrn(keyValue);
                        break;
                        
                    case "vpcmerchant" :       {                
                        transaction.setMerchantName(keyValue);
                        transaction.setVpcmerchant(keyValue);
                        break;
                    }
                        
                   case "postresponsemessage" :                       
                        transaction.setDisburseResponseMessage(keyValue);
                        break;   
                    
                    case "postresponsecode" : {                      
                        transaction.setDisburseResponseCode(keyValue);
                        transaction.setPostResponseCode(keyValue);
                        break;  
                    }
                        
                    case "sendername" :                       
                        transaction.setSenderName(keyValue);
                        break;     
                    
                    case "sourcebankcode" :                       
                        transaction.setSourceBankCode(keyValue);
                        break; 
                    case "sourceaccount" :                       
                        transaction.setSourceAccount(keyValue);
                        break; 
                        
                    case "recipientname" :                       
                        transaction.setRecipientName(keyValue);
                        break;     
                    case "recipientaccount" :                       
                        transaction.setRecipientAccount(keyValue);
                        break;   
                        
                     case "recipientbankcode" :                       
                        transaction.setRecipientBankCode(keyValue);
                        break;  
                    case "merchantcode" :                       
                        transaction.setMerchantCode(keyValue);
                        break; 
                    case "paymentreference" :                       
                        transaction.setPaymentReference(keyValue);
                        break;      
                    case "merchantphoneno" :                       
                        transaction.setMerchantPhone(keyValue);
                        break;  
                    case "phoneno" :   
                    case "payerphone":    
                        transaction.setCustomerPhone(keyValue);
                        break;          
                    case "customerid" :  
                    case "payername" :     
                        transaction.setCustomerName(keyValue);
                        break;  
                    case "authorizeid" :                       
                        transaction.setPaymentId(keyValue);
                        break;      
                }

            }
        }
        
        listData = jsonObject.optJSONArray("transactiondata");

        if (listData != null) {


            for(int i = 0; i < listData.length(); i++){
                JSONObject value = (JSONObject) listData.optJSONObject(i);

                String key = value.optString("name");
                String keyValue = value.optString("value");
                
                switch(key){
                    case "maskcvv" :
                        transaction.setMaskedCvv(keyValue);
                        break;
                    case "maskexp" :                       
                        transaction.setMaskedExpiry(keyValue);
                        break;    
                }

            }
        }
        
        return transaction;
    }
    
    public static PosTransactionNew buildPosTransaction(PosTransaction posTransaction,
            String orderId, String acquiringBank, String cardHolderName, String managerName, 
            String parentName, String productId, String productName, String providerName, 
            String merchantCode, String cardHolderBank, PosMerchant posMerchant, PosProvider posProvider, PosMerchantCustomer posMerchantCustomer){
        
        PosTransactionNew posTransactionNew = new PosTransactionNew();
        posTransactionNew.setAcquirerBank(acquiringBank);
        posTransactionNew.setAmount(posTransaction.getAmount());
        posTransactionNew.setCallBack(posTransaction.isCallBack());
        posTransactionNew.setCallbackOn(posTransaction.getCallbackOn());
        posTransactionNew.setCardholder(cardHolderName);
        posTransactionNew.setCreatedBy(posTransaction.getCreatedBy());
        posTransactionNew.setCreatedOn(posTransaction.getCreatedOn());
        posTransactionNew.setCurrency(posTransaction.getCurrency());
        posTransactionNew.setCurrencyCode(posTransaction.getCurrencyCode());
        posTransactionNew.setFileId(posTransaction.getFileId());
        posTransactionNew.setFileName(posTransaction.getFileName());
        posTransactionNew.setLoggedBy(posTransaction.getLoggedBy());
        posTransactionNew.setManagerName(managerName);
        posTransactionNew.setMerchantName(posTransaction.getMerchantName());
        posTransactionNew.setOrderId(orderId);
        posTransactionNew.setPan(posTransaction.getPan());
//        posTransactionNew.setPro
        posTransactionNew.setParentName(parentName);
        posTransactionNew.setPosId(posTransaction.getPosId());
        posTransactionNew.setProductId(productId);
        posTransactionNew.setProviderName(providerName);
        posTransactionNew.setRefCode(posTransaction.getRefCode());
        posTransactionNew.setRequestDate(posTransaction.getRequestDate());
        posTransactionNew.setReschedule(posTransaction.isReschedule());
        posTransactionNew.setRescheduleOn(posTransaction.getRescheduleOn());
        posTransactionNew.setResponseCode(posTransaction.getResponseCode());
        posTransactionNew.setResponseMessage(posTransaction.getResponseMessage());
        posTransactionNew.setReversed(posTransaction.isReversed());
        posTransactionNew.setReversedOn(posTransaction.getReversedOn());
        posTransactionNew.setRrn(posTransaction.getRrn());
        posTransactionNew.setScheme(posTransaction.getScheme());
        posTransactionNew.setSource(posTransaction.getSource());
        posTransactionNew.setStatus(posTransaction.getStatus());
        posTransactionNew.setTerminalId(posTransaction.getTerminalId());
        posTransactionNew.setTraceref(posTransaction.getTraceref());
        posTransactionNew.setType(posTransaction.getType());
        posTransactionNew.setProductName(productName);
        posTransactionNew.setMerchantCode(merchantCode);
        posTransactionNew.setCardHolderIssuingBank(cardHolderBank);
        
        posTransactionNew.setTransRef(posTransaction.getTransRef());
        
        if(posProvider != null){
            posTransactionNew.setProviderId(posProvider.getId()+"");
        }
        
        if(posMerchant != null){
            posTransactionNew.setMerchantName(posMerchant.getName());
            posTransactionNew.setMerchantBank(posMerchant.getBankName());
            posTransactionNew.setProductId(posMerchant.getMerchantId());
            
            if(posMerchant.getParent() != null){
                posTransactionNew.setParentPosId(posMerchant.getParent().getPosId());
                posTransactionNew.setParentName(posMerchant.getParent().getName());
            }
        }
        
        if(posMerchantCustomer != null){
            
            String meta = posMerchantCustomer.getParameterName()+":"+posMerchantCustomer.getCustomerId();

            posTransactionNew.setMeta(meta);
        }
        
        return posTransactionNew;
    }
}

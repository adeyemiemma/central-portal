/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.clientms.model.Transaction;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import org.apache.poi.hssf.usermodel.HSSFCell;

/**
 *
 * @author emmanueladeyemi
 */
public class CsvUtil {

    private static final String DEFAULT_SEPARATOR = ",";

    public static String writeLine(List<String> headers, List<Object> values, String... exclude) {

        List<String> exclusions = null;

        if (exclude != null) {
//            .map(x -> x.toLowerCase())
            exclusions = Stream.of(exclude).collect(Collectors.toList());
        }

        List<String> excludeAll = exclusions;

        String headerString = headers.stream().filter(x -> excludeAll != null && excludeAll.indexOf(x) <= 0).map(x -> x.toUpperCase()).collect(Collectors.joining(DEFAULT_SEPARATOR));

        String output = headerString + "\n";
        int i = 0;

//        values.stream().parallel().forEach(transaction -> {
//
//        });

        for (Object transaction : values) {

            List<String> fields = new ArrayList<>();
            // This used reflection to fill the field in the date
            for (Field field : transaction.getClass().getDeclaredFields()) {

                field.setAccessible(true);

                try {
                    Object object = field.get(transaction);

                    if (exclusions != null) {

                        int index = exclusions.indexOf(field.getName());

                        if (index > -1) {
                            continue;
                        }
                    }

                    if (transaction.getClass() == Transaction.class) {

                        String fieldName = field.getName().toLowerCase();

                        if (fieldName.equalsIgnoreCase("provider")) {

                            Transaction x = (Transaction) transaction;

                            String prov = x.getProvider();

                            if (prov != null && prov.toUpperCase().startsWith("MCASH")) {

                                String processReference = x.getProcessorReference();

                                if (processReference != null) {

                                    JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();

                                    String merchantCode = Utility.getData(array, "merchantcode");
                                    //                        String merchantName = jsonObject.getString("merchantname", status)

                                    String temp = x.getMerchantId();

                                    temp += " (" + merchantCode + ") ";

                                    x.setMerchantId(temp);

                                    fields.add(object != null ? object.toString() : "");
                                }
                            } else if (field.getType() == Date.class) {
                                fields.add(object != null ? Utility.formatDate((Date) object, "yyyy-MM-dd HH:mm") : "");
                            } else {
                                fields.add(object != null ? object.toString() : "");
                            }

                        } else if (field.getType() == Date.class) {
                            fields.add(object != null ? Utility.formatDate((Date) object, "yyyy-MM-dd HH:mm") : "");
                        } else {
                            fields.add(object != null ? object.toString() : "");
                        }

                    } else if (field.getType() == Date.class) {
                        fields.add(object != null ? Utility.formatDate((Date) object, "yyyy-MM-dd HH:mm") : "");
                    } else {
                        fields.add(object != null ? object.toString() : "");
                    }

//                    output += ",";
                } catch (Exception ex) {

                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }
            }

            output += fields.stream().collect(Collectors.joining(","));

            output += "\n";
        }

        return output;
    }

    public static Map<String, Object> processMoneywaveFile(InputStream inputStream, Date selectedDate, String currency, Map<String, Double> bankReference) throws IOException {

        int index = 0, available = 0;

        double sum = 0.0;

        String tempReference = "";

        if (bankReference != null) {
            tempReference = bankReference.keySet().stream().collect(Collectors.joining(" "));
            tempReference = tempReference.toLowerCase();
        }

        Map<String, Double> references = new HashMap<>();

        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        while ((line = br.readLine()) != null) {

            String[] data = line.split(",");
            // This implies that we will not pick the header 

            if (index == 0) {
                index++;
                continue;
            }

            String amountString = data[4];
            String reference = data[7];

            double amount = Double.parseDouble(amountString);

            if (reference.startsWith("0")) {
                references.put(reference, amount);
            } else {
                references.put("0" + reference, amount);
            }

            if ((tempReference != null && !tempReference.isEmpty()) && (!reference.isEmpty() && tempReference.contains(reference.toLowerCase()))) {
                available++;
                sum += amount;
            }

//            amountCount++;
            index++;

        }

        index = index - 1;
        double fee = index * 42.75;

        Map<String, Object> result = new HashMap<>();
        result.put("Fee", fee);
        result.put("Total", sum);
        result.put("Reference", references);
        result.put("Found", available);
        result.put("Count", index);

        return result;
    }

    public static Map<String, Object> processMoneywaveFileCard(InputStream inputStream, Date selectedDate, String currency, Map<String, Double> bankReference) throws IOException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("E, MMM d, yyyy hh:mm a");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");

        String selectedDateString = dateFormat2.format(selectedDate);

        // This gets the row iteratir that allows user to go over the sheet
//        Iterator<Row> rowIterator = sheet.iterator();
        int index = 0;

        String tempString = "";

        if (bankReference != null) {
            tempString = bankReference.keySet().stream().collect(Collectors.joining(" "));
            tempString = tempString.toLowerCase();
        }

        Map<Long, String> headers = new HashMap<>();

        double sum = 0.0;
        double sumAccount = 0.0;
        long available = 0;
        int amountCount = 0;

//        List<String> references = new ArrayList<>();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        Map<String, Double> references = new HashMap<>();

        while ((line = br.readLine()) != null) {

            String[] data = line.split(",");
            // This implies that we will not pick the header 

            if (index == 0) {
                index++;
                continue;
            }

            String amountString = data[15 + (data.length > 20 ? data.length - 20 : 0)];
            String reference = data[4] == null ? "" : data[4];

            double amount = 0.0;

            try {
                amount = Double.parseDouble(amountString);
            } catch (Exception ex) {

                if (ex != null) {
                    ex.printStackTrace();
                }

                amountString = data[15];

                amount = Double.parseDouble(amountString);
            }

            String temp = reference.toLowerCase().replace("'", "").replaceAll("r/", "");

            if (temp.startsWith("flw")) {

                temp = temp.substring(temp.indexOf("flw") + 3);
//                rawStr = temp;
                references.put(temp, amount);
            } else {
//                rawStr = temp;
                if (reference.startsWith("0")) {
                    temp = reference;
                    references.put(temp, amount);
                } else {
                    temp = "0" + reference;
                    references.put(temp, amount);
                }
            }

            sum += amount;

            if ((tempString != null && !tempString.isEmpty()) && (!reference.isEmpty() && tempString.contains(reference.toLowerCase()))) {
                System.out.print(reference + " ");
                available++;
                sumAccount += amount;
            }

//            amountCount++;
            index++;

        }

        double fee = (index - 1) * 42.75;

        Map<String, Object> result = new HashMap<>();
        result.put("Fee", fee);
        result.put("Total", sum);
        result.put("Found", available);
        result.put("TotalAccount", sumAccount);
        result.put("Reference", references);

        return result;
    }
}

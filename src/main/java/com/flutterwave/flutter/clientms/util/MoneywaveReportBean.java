/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author emmanueladeyemi
 */
public class MoneywaveReportBean implements Serializable {

    /**
     * @return the moneywaveExceptions
     */
    public Map<String, Double> getMoneywaveExceptions() {
        return moneywaveExceptions;
    }

    /**
     * @param moneywaveExceptions the moneywaveExceptions to set
     */
    public void setMoneywaveExceptions(Map<String, Double> moneywaveExceptions) {
        this.moneywaveExceptions = moneywaveExceptions;
    }

    /**
     * @return the bankExceptions
     */
    public Map<String, Double> getBankExceptions() {
        return bankExceptions;
    }

    /**
     * @param bankExceptions the bankExceptions to set
     */
    public void setBankExceptions(Map<String, Double> bankExceptions) {
        this.bankExceptions = bankExceptions;
    }
    
    private Map<String, Double> moneywaveExceptions;
    private Map<String, Double> bankExceptions;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.clientms.viewmodel.PosTransactionViewModelWeb;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPage;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.DispatcherType;

/**
 *
 * @author emmanueladeyemi
 */
public class PdfUtilExport {

    private static Font catFont = new Font(Font.FontFamily.COURIER, 8,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.COURIER, 7,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.COURIER, 8,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.COURIER, 7,
            Font.BOLD);
    private static Font small = new Font(Font.FontFamily.COURIER, 7,
            Font.NORMAL);
    
    private static Font mediumBold = new Font(Font.FontFamily.COURIER, 9,
            Font.BOLD);
    private static Font medium = new Font(Font.FontFamily.COURIER, 9,
            Font.NORMAL);

    public static String generatePdf(String author, String title, List<String> headers, List<Object> values, String... exclude) {

        try {

            File file = new File("tempdf.pdf");
            
            if(file.exists())
                file.delete();
            
            file.createNewFile();
//            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("tempdf.pdf"));
            document.setPageSize(PageSize.A4.rotate());
            document.open();
//            Rotate event = new Rotate();
//            event.setOrientation(PdfPage.LANDSCAPE);
//            pdfWriter.setPageEvent(event);
//            float fntSize = 6.7f;
//            float lineSpacing = 10f;
//                p = new Paragraph(new Phrase(lineSpacing,line,
//                   FontFactory.getFont(FontFactory.COURIER, fntSize)));
    
            addMetaData(document, author);
            addTitlePage(document, title);
            createTable(document, headers, values, exclude);
            
            document.close();
            pdfWriter.close();
            
            return "tempdf.pdf";
            
        } catch (Exception ex) {
            Logger.getLogger(PdfUtilExport.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public static File generatePosReceipt(String author, PosTransactionViewModelWeb viewModelWeb, String chName) {

        try {

            File file = File.createTempFile("transaction",".pdf");
            
            if(file.exists())
                file.delete();
            
            file.createNewFile();
//            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(file));
            document.setPageSize(PageSize.A4.rotate());
            document.open();
    
            boolean extraLine = true;
            
            addMetaData(document, author);
            addTitlePage1(document, "****** MERCHANT COPY *******", !extraLine);
            addTitlePage1(document, "********** REPRINT *********", extraLine);
            
            String terminalid = viewModelWeb.getTerminalId();
            
            String terminalPrefix = terminalid.substring(0,4);

            
            Properties properties = Utility.getConfigProperty();
            
            String temp = properties.getProperty("pos.address.street");
            
            addItem(document, temp == null ? "19 OLUBUNMI ROTIMI STR" : temp, false, !extraLine);
            
            temp = properties.getProperty("pos.address.city");
            
            addItem(document, temp == null ? "LEKKI LAGOS" : temp, false,extraLine);
            
            addItem(document, "DATE      :   "+ viewModelWeb.getDatetime(), true,!extraLine);
            
            String mid = properties.getProperty(""+terminalPrefix, null);
            
            
            if(mid != null){
                String data[] = mid.split(",");
                
                addItem(document, "MID      :    "+ data[0], false, !extraLine);
                addItem(document, "ACQUIRER :    "+ data[1], false, !extraLine);
            }else{
                
                addItem(document, "MID      :     MID", false, !extraLine);
                addItem(document, "ACQUIRER :    "+viewModelWeb.getBankName(), false, !extraLine);
            }

            addItem(document, "TID      :   "+ viewModelWeb.getTerminalId(), false, !extraLine);
            
            addItem(document, "CARD     :   "+ viewModelWeb.getPan(), false, !extraLine);
            
            
            if(viewModelWeb.getProvider() == null)
                addItem(document, "PTSP      :    PTSP", false, extraLine);
            else
                addItem(document, "PTSP      :    "+viewModelWeb.getProvider().replace("DIRECT", ""), false, extraLine);
            
            addItem(document, "VERIFIED BY :   PIN", false, !extraLine);
            
            addItem(document, "-----------------------------------", false, extraLine);
            
            addItem(document, "FLUTTERWAVE SUMMARY", true, extraLine);
            
            addItem(document, "MERCHANT :   "+ viewModelWeb.getMerchantName(), false, !extraLine);
            addItem(document, "POS ID   :   "+ viewModelWeb.getPosId(), false, !extraLine);
            addItem(document, "REF      :   "+ viewModelWeb.getRefCode(), false, !extraLine);
            addItem(document, "CH NAME  :   "+ chName, false, !extraLine);
            addItem(document, "AMOUNT   :   "+ Utility.convertTo2DP(viewModelWeb.getAmount()), false, !extraLine);
            addItem(document, "TX REF   :   "+ viewModelWeb.getRrn(), false, extraLine);
            
            addItem(document, "-----------------------------------", false, !extraLine);
            
            addItem(document, " "+ viewModelWeb.getResponseMessage().toUpperCase()+" ", true, !extraLine);
            
            addItem(document, "-----------------------------------", false, extraLine);
            
            addItem(document, ""+ properties.getProperty("pos.footer"), false, !extraLine);
            addItem(document, "         Thank you        ", false, !extraLine);
            
            addItem(document, "   Powered By Flutterwave   ", false, !extraLine);
//            addItem(document, "      www.flutterwave.com   ", false, !extraLine);
            addItem(document, "     hi@flutterwaveego.com  ", false, !extraLine);
            
            
            document.close();
            pdfWriter.close();
            
            return file;
            
        } catch (Exception ex) {
            Logger.getLogger(PdfUtilExport.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    private static void addItem(Document document, String text, boolean bold, boolean extraLine) throws DocumentException{
        
        Paragraph preface = new Paragraph();
        // We add one empty line
//        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph(text, bold == true ? mediumBold : medium));

        if(extraLine)
            addEmptyLine(preface, 1);

        document.add(preface);
    }
    
    private static void addItem(Document document, String text, boolean bold,boolean extraLine, Font font) throws DocumentException{
        
        Paragraph preface = new Paragraph();
        
        preface.setFont(font);
        
        // We add one empty line
//        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph(text, bold == true ? smallBold : small));

        if(extraLine )
            addEmptyLine(preface, 1);

        document.add(preface);
    }

    private static void addMetaData(Document document, String author) {
        document.addTitle("Settlement Report");
        document.addSubject("Settlement Report");
//        document.addKeywords("Java, PDF, iText");
        document.addAuthor(author);
        document.addCreator(author);
    }
    
    private static void addTitlePage1(Document document, String title, boolean extraline)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
//        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph(title, mediumBold));

        if(extraline)
            addEmptyLine(preface, 1);

        document.add(preface);
        // Start a new page
//        document.newPage();
    }

    private static void addTitlePage(Document document, String title)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph(title, catFont));

        addEmptyLine(preface, 1);
        // Will create: Report generated by: _name, _date
        preface.add(new Paragraph(
                "Report generated on " + new Date(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                catFont));
        addEmptyLine(preface, 3);

        document.add(preface);
        // Start a new page
//        document.newPage();
    }

    private static void createTable(Document document, List<String> headers, List<Object> values, String... exclude)
                        throws BadElementException, DocumentException {

        int excludeSize = 0;
        if(exclude != null)
            excludeSize = exclude.length;

        PdfPTable table = new PdfPTable(headers.size()-excludeSize);
        table.setWidthPercentage(100);
        table.setSpacingBefore(0f);
        table.setSpacingAfter(0f);
        
        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).map(x -> x.toLowerCase()).collect(Collectors.toList());
        }
        
        List<String> excludeAll = exclusions;
        
        headers = headers.stream().filter(x -> excludeAll != null && excludeAll.indexOf(x.toLowerCase()) <= 0).map(x -> x.toUpperCase()).collect(Collectors.toList());
        
        for(String header : headers){
            
            PdfPCell c1 = new PdfPCell(new Phrase(header,smallBold ));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
        }
        
        table.setHeaderRows(1);

        for (Object transaction : values) {

            // This used reflection to fill the field in the date
            for (Field field : transaction.getClass().getDeclaredFields()) {

                field.setAccessible(true);

                try {
                    Object object = field.get(transaction);

                    if (exclusions != null) {

                        int index = exclusions.indexOf(field.getName());

                        if (index > -1) {
                            continue;
                        }
                    }

                    String content = object != null ? object.toString() : "";

//                    System.out.println(content);
                    
                    PdfPCell c1 = new PdfPCell(new Phrase(content, small));
                    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(c1);

                } catch (Exception ex) {
                    
                    if(ex != null)
                        ex.printStackTrace();
                }
            }
        }
        
        document.add(table);

    }
    
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    
    public static class Rotate extends PdfPageEventHelper {
 
        protected PdfNumber orientation = PdfPage.PORTRAIT;
 
        public void setOrientation(PdfNumber orientation) {
            this.orientation = orientation;
        }
 
        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            writer.addPageDictEntry(PdfName.ROTATE, orientation);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

/**
 *
 * @author emmanueladeyemi
 */
public class RaveUtil {
    
    public enum PaymentEntity{
        card,
        account,
        wallet
    }
    
    public enum FraudStatus{
        ok,
        flagged,
        review
    }
    
    public enum TransactionType{
        debit,
        credit
    }
    
    public enum ChargeType{
        normal,
        preauth
    }
}

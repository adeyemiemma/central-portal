/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.faces.flow.Flow;
import javax.faces.flow.builder.FlowBuilder;
import javax.faces.flow.builder.FlowBuilderParameter;
import javax.faces.flow.builder.FlowDefinition;
import javax.inject.Named;

/**
 *
 * @author emmanueladeyemi
 */
@Named
@ApplicationScoped
public class FlowManager implements Serializable {
    
    /**
     * This is for the flow definition
     * @param flowBuilder
     * @return 
     */
    @Produces
    @FlowDefinition
    public Flow defineFlow(@FlowBuilderParameter FlowBuilder flowBuilder) {

        String flowId = "upload";
        flowBuilder.id("", flowId);
        flowBuilder.viewNode(flowId, 
                "/recon/upload/" + flowId + ".xhtml").
                markAsStartNode();
        flowBuilder.viewNode("uploadpreview", "/recon/upload/uploadpreview.xhtml");
        flowBuilder.viewNode("uploadanalysis", "/recon/upload/uploadanalysis.xhtml");
        flowBuilder.returnNode("uploadanalysis");
        
        return flowBuilder.getFlow();
    }
    
    @Produces
    @FlowDefinition
    public Flow updateFlow(@FlowBuilderParameter FlowBuilder flowBuilder) {

        String flowId = "transactionupload";
        flowBuilder.id("", flowId);
        flowBuilder.viewNode(flowId, 
                "/transaction/upload/" + flowId + ".xhtml").
                markAsStartNode();
        flowBuilder.viewNode("transactionpreview", "/transaction/upload/preview.xhtml");
        flowBuilder.viewNode("transactionuploadcompleted", "/transaction/upload/completed.xhtml");
        flowBuilder.returnNode("transactionuploadcompleted");
        
        return flowBuilder.getFlow();
    }
    
    
}

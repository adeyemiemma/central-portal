/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author emmanueladeyemi
 */
public class ExcelFileProcessor {
    
    public static Map<String, Object> processFile(Iterator<Row> rowIterator, Date startDate, Date endDate, String currency){
        
        SimpleDateFormat dateFormat  = new SimpleDateFormat("dd-MM-YYYY");
        
        //String selectedDateString = dateFormat.format(startDate);
        
        startDate = removeTime(startDate);
        endDate = removeTime(endDate);
        
        
       
        // This gets the row iteratir that allows user to go over the sheet
//        Iterator<Row> rowIterator = sheet.iterator();
        
        int index = 0;
        
        Map<Long, String> headers = new HashMap<>();
        
        double sum = 0.0;
        
//        List<String> references = new ArrayList<>();
        
        Map<String, Double> references = new HashMap<>();
        
        while(rowIterator.hasNext()){
            
            Row row = rowIterator.next();
            // This implies that we will not pick the header 
            
            Iterator<Cell> cellIterator = row.cellIterator();
            
            long cellIndex = 0;
            double amount = 0.0;
//            String selectedCurrency;
            String trnxType = "";
            double feeData = 0.0;
            String debitStatus = "";
            boolean breakRow = false;
            
            while(cellIterator.hasNext()){
                
                Cell cell = cellIterator.next();
                
//                String tempVal = cell.getStringCellValue();
                
                switch(cell.getCellType()){
                    case NUMERIC : {
                            
                        if(DateUtil.isCellDateFormatted(cell)){
                            
                            
                            Date date =  DateUtil.getJavaDate(cell.getNumericCellValue());
                            
                            date = removeTime(date);
                            
                            
                            if( (startDate.compareTo(date) <= 0 && endDate.compareTo(date) >= 0 ) && cellIndex == 18){
                                
                               if(feeData ==  42.75 && trnxType.contains("_2"))
                                    index++;
                               
                            }else if(!(startDate.compareTo(date) <= 0 && endDate.compareTo(date) >= 0) && cellIndex == 18){
                                breakRow = true;
                                break;
                            }
                            
                            if(cellIndex >= 25){
                                breakRow = true;
                                break;
                            }
                            
                        }else{
                            double value = cell.getNumericCellValue();
                            
                            if(cellIndex == 14 && value != 42.75)
                                amount = value;
                            else if(cellIndex == 14)
                                feeData = value;
                        }
                        break;
                        
                    }
                    
                    case STRING : {
                        
                        String value = cell.getStringCellValue();
                        
                        // This rules out transactions not with specified currency
                        if(cellIndex == 6 && !currency.equalsIgnoreCase(value)){
                            breakRow = true;
                            break;
                        }
//                        else if(cellIndex == 3)
//                            selectedCurrency = value;
                        if(cellIndex == 7)
                            debitStatus = value;

                        if(cellIndex == 7 && !"D".equalsIgnoreCase(value)){
                            breakRow = true;
                            
                            debitStatus = "";
                            break;
                        }
//                        else if(cellIndex == 4)
//                            trnxType = value;
                        
                        if(cellIndex == 22)
                            trnxType = value;
                        
                        if(cellIndex == 22 && !value.toLowerCase().contains("fee") &&  !value.toLowerCase().contains("_2") && !value.toLowerCase().contains("_3")){
                            sum += amount;
                            
                            String temp = value.substring(value.lastIndexOf("/")+1);
                            temp = temp.substring(0, temp.contains("_") ? temp.indexOf("_") : temp.length() );
                            
//                            references.add(temp);
                              references.put(temp, amount);
                        }
                        
                        if(feeData ==  42.75 && trnxType.contains("_2")){
                            index++;
//                            System.out.println(debitStatus+ " - " +feeData + " - "+ trnxType);
                            feeData = 0.0;
                        }
                        
                        else if(cellIndex >= 25){
                            breakRow = true;
                            break;
                        }
                        
                        break;
                    }
                }
                
                cellIndex++;
                
                if(breakRow == true)
                    break;
            } 
        }
        
        double fee = index * 42.75;
        
        Map<String, Object> result = new HashMap<>();
        result.put("Fee", fee);
        result.put("Total", sum);
        result.put("Reference", references);
        result.put("Found", index);
        
        return result ;
    }
    
    public static Map<String, Object> processFileFormat2(Iterator<Row> rowIterator, Date startDate, Date endDate, String currency){
        
        SimpleDateFormat dateFormat  = new SimpleDateFormat("dd-MM-YYYY");
        
        //String selectedDateString = dateFormat.format(startDate);
        
        startDate = removeTime(startDate);
        endDate = removeTime(endDate);
        
        
       
        // This gets the row iteratir that allows user to go over the sheet
//        Iterator<Row> rowIterator = sheet.iterator();
        
        int index = 0;
        
        double sum = 0.0;
        
//        List<String> references = new ArrayList<>();
        
        Map<String, Double> references = new HashMap<>();
        
        while(rowIterator.hasNext()){
            
            Row row = rowIterator.next();
            // This implies that we will not pick the header 
            
//            Iterator<Cell> cellIterator = row.cellIterator();
            
            long cellIndex = 0;
            double amount = 0.0;
//            String selectedCurrency;
            String trnxType = "";
            double feeData = 0.0;
//            String debitStatus = "";
            boolean breakRow = false;
            
            int lastCellNumber = row.getLastCellNum();
            
            Date date = null;
            for(int i =0 ; i < lastCellNumber; i++){
                
//                Cell cell = cellIterator.next();
                Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                
//                String tempVal = cell.getStringCellValue();
                
                switch(cell.getCellType()){
                    case NUMERIC : {
                            
                        if(DateUtil.isCellDateFormatted(cell)){
                            
                            if(cellIndex == 3){
                                date =  DateUtil.getJavaDate(cell.getNumericCellValue());
                                date = removeTime(date);
                                
                                if(!(startDate.compareTo(date) <= 0 && endDate.compareTo(date) >= 0)){
                                    breakRow = true;
                                    break;
                                }
                            }
                            
                        }else{
                            double value = cell.getNumericCellValue();
                            
                            if(cellIndex == 4 && value != 42.75 && (!trnxType.toLowerCase().contains("fee") &&  !trnxType.toLowerCase().contains("_2")) && !trnxType.toLowerCase().contains("_3")){
                                amount = value;
                                
                                sum += amount;
                            
                                String temp = trnxType.substring(trnxType.lastIndexOf("/")+1);
                                temp = temp.substring(0, temp.contains("_") ? temp.indexOf("_") : temp.length() );

                                temp = temp.startsWith("0") == false ? "0"+temp : temp;
                                references.put(temp, value);
                            }
                            else if(cellIndex == 4)
                                feeData = value;
                            
                            if( cellIndex == 4 && (startDate.compareTo(date) <= 0 && endDate.compareTo(date) >= 0 )){
                                
                               if(feeData ==  42.75 && trnxType.contains("_2"))
                                    index++;
                               
                            }
                            
                            if(cellIndex == 1)
                                trnxType = BigDecimal.valueOf(value).toPlainString()+"";
                        }
                        
                        break;
                        
                    }
                    
                    case STRING : {
                        
                        String value = cell.getStringCellValue();
                        
//                        if(cellIndex == 3 && (!value.toLowerCase().contains("fee") &&  !value.toLowerCase().contains("_2")) && !value.toLowerCase().contains("_3")){
//                            sum += amount;
//                            
//                            String temp = value.substring(value.lastIndexOf("/")+1);
//                            temp = temp.substring(0, temp.contains("_") ? temp.indexOf("_") : temp.length() );
//                            
//                            references.add(temp);
//                        }
                        
                        if(cellIndex == 1)
                            trnxType = value;
                        
                        if(feeData ==  42.75 && trnxType.contains("_2")){
                            index++;
                            feeData = 0.0;
                        }
                        
                        break;
                    }
                    
                    case BLANK :{
                        
                        if(cellIndex == 1){
                            breakRow = true; 
                            break;
                        }
                    }
                }
                
                cellIndex++;
                
                if(breakRow == true)
                    break;
            } 
        }
        
        double fee = index * 42.75;
        
        Map<String, Object> result = new HashMap<>();
        result.put("Fee", fee);
        result.put("Total", sum);
        result.put("Reference", references);
        result.put("Found", index);
        
        return result ;
    }
    
    public static Map<String, Object> processMoneywaveFile(Iterator<Row> rowIterator, Date selectedDate, String currency, Map<String, Double> bankReference){
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("E, MMM d, yyyy hh:mm a");
        
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
        
        String selectedDateString = dateFormat2.format(selectedDate);
       
        // This gets the row iteratir that allows user to go over the sheet
//        Iterator<Row> rowIterator = sheet.iterator();
        
        int index = 0, available = 0;
        
        Map<Long, String> headers = new HashMap<>();
        
        double sum = 0.0, sumAccount = 0.0 ;
        long amountCount = 0;
        
        String tempReference = "";
        
        if(bankReference != null){
            tempReference = bankReference.keySet().stream().collect(Collectors.joining(" "));
            tempReference = tempReference.toLowerCase();
        }
        
//        List<String> references = new ArrayList<>();
        
        Map<String, Double> references = new HashMap<>();
        
        while(rowIterator.hasNext()){
            
            Row row = rowIterator.next();
            // This implies that we will not pick the header 
            
            if(index == 0){
                index++;
                continue;
            }
            
            int lastCellNumber = row.getLastCellNum();
            
//            Iterator<Cell> cellIterator = row.cellIterator();
            
            long cellIndex = 0;
            double amount = 0.0;
//            String selectedCurrency;
            String trnxType = "";
            double feeData = 0.0;
            boolean breakRow = false;
            
            
            for(int i =0 ; i < lastCellNumber; i++){
                
                Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                
//                String tempVal = cell.getStringCellValue();
                
                switch(cell.getCellType()){
                    case NUMERIC : {
                            
                        if(cellIndex == 2){
                            double value = cell.getNumericCellValue();
                            amount += value;
                            
                            amountCount++;
                            //System.out.println("STR- "+value);
                            

                            
                        }else if (cellIndex == 5){

                            String value = ""+BigDecimal.valueOf(cell.getNumericCellValue()).toPlainString();
                            
                            if(value != null && !value.isEmpty()){
                                trnxType = value;

                                if(value.startsWith("0"))
                                    references.put(value, amount);
                                else
//                                    references.add("0"+value); 
                                    references.put("0"+value, amount);

                                if((tempReference != null && !tempReference.isEmpty() ) && tempReference.contains(value.toLowerCase())){
                                    available++;
                                }
                            }
                        }
                        
                        break;
                    }
                    
                    case STRING : {
                        
                        String value = cell.getStringCellValue();
                        
                        if(cellIndex == 1){
                            
                            Date date;
                            
                            String excelDate = "";
                            try{
                                date = dateFormat.parse(cell.getStringCellValue()+"");
                                excelDate = dateFormat2.format(date);
                            }catch(ParseException ex){
                                if(ex != null)
                                    ex.printStackTrace();
                            }
                            
                            
//                            if(!excelDate.equals(selectedDateString)){
//                                breakRow = true;
//                                break;
//                            }else{
                                index++;
                            
                        }else if(cellIndex == 2){
                            double valueD = Double.parseDouble(value);
                            amount += valueD;
                            
                            amountCount++;
//                            System.out.println(value);
                        }else if (cellIndex == 5){
                            
                            if(value != null && !value.isEmpty()){
                                trnxType = value;
                                if(value.startsWith("0"))
                                    references.put(value, amount);
                                else
                                    references.put("0"+value, amount);

                                if( tempReference != null && tempReference.contains(value.toLowerCase()))
                                    available++;
                            }
                        }
                    }
                }
                
                cellIndex++;
                
                if(breakRow == true)
                    break;
            } 
            
//            System.out.println(amntList);
//            index += cellIndex;
            if( !trnxType.isEmpty() && (tempReference != null && !tempReference.isEmpty() ) && tempReference.contains(trnxType.toLowerCase()))
                sum += amount;
        }
        
        index = index -1;
        double fee = index * 42.75;
        
        
        Map<String, Object> result = new HashMap<>();
        result.put("Fee", fee);
        result.put("Total", sum);
        result.put("Reference", references);
        result.put("Found", available);
        result.put("Count", index);
        
        return result ;
    }
    
    public static Map<String, Object> processMoneywaveFileCard(Iterator<Row> rowIterator, Date selectedDate, String currency, Map<String,Double> bankReference){
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("E, MMM d, yyyy hh:mm a");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
        
        String selectedDateString = dateFormat2.format(selectedDate);
       
        // This gets the row iteratir that allows user to go over the sheet
//        Iterator<Row> rowIterator = sheet.iterator();
        
        int index = 0;
        
        String tempString = "";
        
        if(bankReference != null){
            tempString = bankReference.keySet().stream().collect(Collectors.joining(" "));
            tempString = tempString.toLowerCase();
        }
        
        Map<Long, String> headers = new HashMap<>();
        
        double sum = 0.0;
        double sumAccount = 0.0;
        long available = 0;
        int amountCount = 0;
        
//        List<String> references = new ArrayList<>();
        
        Map<String, Double> references = new HashMap<>();
        
        while(rowIterator.hasNext()){
            

            Row row = rowIterator.next();
            // This implies that we will not pick the header 
            
            int lastCellNumber = row.getLastCellNum();
            
            if(index == 0){
                index++;
                continue;
            }
            
            Iterator<Cell> cellIterator = row.cellIterator();
            
            long cellIndex = 0;
            double amount = 0.0;
//            String selectedCurrency;
            String trnxType = "";
            double feeData = 0.0;
            boolean breakRow = false;
            
            
            for(int i =0 ; i < lastCellNumber; i++){
                
                Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                
//                String tempVal = cell.getStringCellValue();
                
                switch(cell.getCellType()){
                    case NUMERIC : {

                        if(cellIndex == 1){
                            
                            Date date;
                            
                            String excelDate = "";
                            if(DateUtil.isCellDateFormatted(cell)){
                                date = DateUtil.getJavaDate(cell.getNumericCellValue());
                                excelDate = dateFormat.format(date);
                            }else{
                                try{
                                    date = dateFormat.parse(cell.getNumericCellValue()+"");
                                    excelDate = dateFormat.format(date);
                                }catch(ParseException ex){
                                    if(ex != null)
                                        ex.printStackTrace();
                                }
                            }
                            
//                            if(!excelDate.equals(selectedDateString)){
//                                breakRow = true;
//                                break;
//                            }else{
//                            }
                        }else if (cellIndex == 2){
                            
                            String value = ""+BigDecimal.valueOf(cell.getNumericCellValue()).toPlainString();
                            
                            if(value != null && !value.isEmpty()){
                                String temp = value.toLowerCase().replace("'", "").replaceAll("r/", "");
                                String rawStr;
                                if(temp.startsWith("flw")){

                                    temp = temp.substring(temp.indexOf("flw")+3) ;
                                    rawStr = temp;
                                    references.put(temp, amount);
                                }
                                else{
                                    rawStr = temp;
                                    if(value.startsWith("0")){
                                        temp = value;
                                        references.put(temp, amount);
                                    }
                                    else{
                                        temp = "0"+value;
                                        references.put(temp, amount);
                                    }
                                }

                                trnxType = rawStr;
                            }
                        }else  {
                        
                            if(cellIndex == 13){
                                double value = cell.getNumericCellValue();
                                amount += value;
                                
                                if((trnxType != null && !trnxType.isEmpty()) && (tempString != null && !tempString.isEmpty() ) && tempString.contains(trnxType.toLowerCase())){
                                    
                                    System.out.print(trnxType + " ");
                                    sumAccount += amount;
                                    available++;
                                }
                                
                                amountCount++;
                                index++;
                            }
//                            else if (cellIndex == 3){
//                            
//                                String value = ""+cell.getNumericCellValue();
//                                
//                                if(value.startsWith("0"))
//                                    references.add(value);
//                                else
//                                    references.add("0"+value);
//                            }
                        }
                        
                        break;
                    }
                    
                    case STRING : {
                        
                        String value = cell.getStringCellValue();
                        
                        if(cellIndex == 1){
                            
                            Date date;
                            
                            String excelDate = "";
//                            if(DateUtil.isCellDateFormatted(cell)){
//                                date = DateUtil.getJavaDate(cell.getNumericCellValue());
//                                excelDate = dateFormat.format(date);
//                            }else{
                                try{
                                    date = dateFormat.parse(cell.getStringCellValue());
                                    
                                    excelDate = dateFormat2.format(date);
                                }catch(ParseException ex){
                                    if(ex != null)
                                        ex.printStackTrace();
                                }
//                            }
                            
//                            if(!excelDate.equals(selectedDateString)){
//                                breakRow = true;
//                                break;
//                            }else{
//                                index++;
//                            }
                        }else if (cellIndex == 2){
                            
                            if(value != null && !value.isEmpty()){
                                String temp = value.toLowerCase().replace("'", "").replaceAll("r/", "");
                                String rawStr;
                                if(temp.startsWith("flw")){

                                    temp = temp.substring(temp.indexOf("flw")+3) ;
                                    rawStr = temp;
                                    references.put(temp, amount);
                                }
                                else{
                                    rawStr = temp;
                                    if(value.startsWith("0")){
                                        temp = value;
                                        references.put(temp, amount);
                                    }
                                    else{
                                        temp = "0"+value;
                                        references.put(temp, amount);
                                    }
                                }

                                trnxType = rawStr;
                            }
                        }
                        else if(cellIndex == 13){
                                double valueD = Double.parseDouble(value);
                                amount += valueD;
                                
                                if((trnxType != null && !trnxType.isEmpty()) && (tempString != null && !tempString.isEmpty() ) && tempString.contains(trnxType.toLowerCase())){
                                    
                                    System.out.println(trnxType+"");
                                    sumAccount += amount;
                                    available++;
                                }
                                
                                index++;
                            }
                    }
                }
                
                cellIndex++;
                
                if(cellIndex > 14)
                    break;
                
                if(breakRow == true)
                    break;
            } 
            
//            cellIndex;
            sum += amount;
        }
        
        double fee = (index-1) * 42.75;
        
        Map<String, Object> result = new HashMap<>();
        result.put("Fee", fee);
        result.put("Total", sum);
        result.put("Found", available);
        result.put("TotalAccount", sumAccount);
        result.put("Reference", references);
        
        return result ;
    }
    
    private static Date removeTime(Date date){
            
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        date = calendar.getTime();
        return date;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

/**
 *
 * @author emmanueladeyemi
 */
public class CreateRuleModel {

    /**
     * @return the debitAccountNo
     */
    public String getDebitAccountNo() {
        return debitAccountNo;
    }

    /**
     * @param debitAccountNo the debitAccountNo to set
     */
    public void setDebitAccountNo(String debitAccountNo) {
        this.debitAccountNo = debitAccountNo;
    }

    /**
     * @return the debitBankCode
     */
    public String getDebitBankCode() {
        return debitBankCode;
    }

    /**
     * @param debitBankCode the debitBankCode to set
     */
    public void setDebitBankCode(String debitBankCode) {
        this.debitBankCode = debitBankCode;
    }

    /**
     * @return the debitCurrency
     */
    public String getDebitCurrency() {
        return debitCurrency;
    }

    /**
     * @param debitCurrency the debitCurrency to set
     */
    public void setDebitCurrency(String debitCurrency) {
        this.debitCurrency = debitCurrency;
    }

    /**
     * @return the debitCountry
     */
    public String getDebitCountry() {
        return debitCountry;
    }

    /**
     * @param debitCountry the debitCountry to set
     */
    public void setDebitCountry(String debitCountry) {
        this.debitCountry = debitCountry;
    }

    /**
     * @return the creditAccountNo
     */
    public String getCreditAccountNo() {
        return creditAccountNo;
    }

    /**
     * @param creditAccountNo the creditAccountNo to set
     */
    public void setCreditAccountNo(String creditAccountNo) {
        this.creditAccountNo = creditAccountNo;
    }

    /**
     * @return the creditBankCode
     */
    public String getCreditBankCode() {
        return creditBankCode;
    }

    /**
     * @param creditBankCode the creditBankCode to set
     */
    public void setCreditBankCode(String creditBankCode) {
        this.creditBankCode = creditBankCode;
    }

    /**
     * @return the creditCurrency
     */
    public String getCreditCurrency() {
        return creditCurrency;
    }

    /**
     * @param creditCurrency the creditCurrency to set
     */
    public void setCreditCurrency(String creditCurrency) {
        this.creditCurrency = creditCurrency;
    }

    /**
     * @return the creditCountry
     */
    public String getCreditCountry() {
        return creditCountry;
    }

    /**
     * @param creditCountry the creditCountry to set
     */
    public void setCreditCountry(String creditCountry) {
        this.creditCountry = creditCountry;
    }
    
   private String debitAccountNo;
   private String debitBankCode;
   private String debitCurrency;
   private String debitCountry;
   private String creditAccountNo;
   private String creditBankCode;
   private String creditCurrency;
   private String creditCountry;

   
}

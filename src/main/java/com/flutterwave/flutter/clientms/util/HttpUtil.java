/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Merchant;
import com.flutterwave.flutter.clientms.model.MerchantLimit;
import com.flutterwave.flutter.clientms.service.LogService;
import static com.flutterwave.flutter.clientms.util.TransactionUtility.logger;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.net.ssl.SSLContext;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.log4j.Logger;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class HttpUtil implements Serializable {

//    private static final String merchantId = "test"; // test
//    private static final String token = "f71e247c6a7498c8c17ea0e8167c5584dbe5b7b95fc26f6c25035a6f40a584e4cba182a2d81a250873e1eb9bd586397e767c5d303960d298518aec7703ec349d";
//    private static final String url_limit = "https://flutterwavestaging.com:9443/flwreportal/services/reports/merchants/limit";
//    private static final String privateKey = "1123459846fedcbb"; // This is test credentials
////////
////    /**
////     * staging *
////     */
//    private static final String RULE_URL = "https://flutterwavestaging.com:9443/flwreportal/services/settlement/rule/create";
//    private static final String TRANSACTION_RULE = "https://flutterwavestaging.com:9443/flwreportal/services/settlement/transaction/create";
//    private static final String CORE_SETTLEMENT_STATUS = "https://flutterwavestaging.com:9443/flwreportal/services/settlement/transaction/status";
//    private static final String CORE_RULE_STATUS = "https://flutterwavestaging.com:9443/flwreportal/services/settlement/rule/status";
////    
//    static String raveHost = "http://flw-pms-dev.eu-west-1.elasticbeanstalk.com"; // staging
//    static String moneywaveHost = "http://moneywave.herokuapp.com";
    /* live */
    private static final String merchantId = "acctcharge"; // live
    private static final String token = "e2310c314984d494f26d4e28004aa98c0e693223720a67349ad247777df7a9ccdefcee568fc18301f03de2fb30528a4c8718ade6602077300e29ae7881e74ed4"; //live
    private static final String url_limit = "http://localhost:9090/flwreportal/services/reports/merchants/limit"; //live
    private static final String privateKey = "k4lwLPJnGjsJO4qK/qinCP5DgyovHAHf"; // This is live credentials

    // live
    private static final String RULE_URL = "http://localhost:9090/flwreportal/services/settlement/rule/create";
    private static final String TRANSACTION_RULE = "http://localhost:9090/flwreportal/services/settlement/transaction/create";
    private static final String CORE_SETTLEMENT_STATUS = "http://localhost:9090/flwreportal/services/settlement/transaction/status";
    private static final String CORE_RULE_STATUS = "http://localhost:9090/flwreportal/services/settlement/rule/status";

    static String raveHost = "http://api.ravepay.co"; // live

    static String moneywaveHost = "https://live.moneywaveapi.co"; //live

    private static final String RAVE_LIMIT = "/limits/manage";

    static String raveMMKEY = "WQiOjIsImZ1bGxuYW1lIjoiRGF5byBPZ3VubW9rdW4ifSwiaWF0IjoxNDkwNzkwMDk3LCJleHAiOjE0OTExMzU2OTd9.0hTxO8RxdCoDwwKSMXT92OhuSXHTdBxHbXqIaI_nEkE$$$%";

    @EJB
    private LogService logService;

    private final Logger logger = Logger.getLogger(HttpUtil.class);

    public JsonArray getLimitFromCore() {
        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(url_limit)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            JsonArray jsonArray = arrayBuilder.build();

            String content = "merchantid=" + merchantId;
            content += "&token=" + token;

            JsonObject object = Json.createObjectBuilder()
                    .add("merchantid", merchantId)
                    .add("token", token).build();

            Log log = new Log();
            log.setAction("Pulling limits from core request");
            log.setCreatedOn(new Date());
            log.setDescription(content);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("responsecode", "99");

            log = new Log();
            log.setAction("Pulling limits from core response");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"00".equalsIgnoreCase(responseCode)) {

                return null;
            }

            jsonArray = response.getJsonArray("limits");

            return jsonArray;

        } catch (Exception ex) {
            logger.error(ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public String createRule(CreateRuleModel createRuleModel) {
        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(RULE_URL)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            String hashToken = CryptoUtil.generateHash(createRuleModel, privateKey);

            JsonObject jsonObjectCredit = Json.createObjectBuilder().add("accountnumber", createRuleModel.getCreditAccountNo())
                    .add("bankcode", createRuleModel.getCreditBankCode())
                    .add("country", createRuleModel.getCreditCountry())
                    .add("currency", createRuleModel.getCreditCurrency()).build();

            JsonObject jsonObjectDebit = Json.createObjectBuilder().add("accountnumber", createRuleModel.getDebitAccountNo())
                    .add("bankcode", createRuleModel.getDebitBankCode())
                    .add("country", createRuleModel.getDebitCountry())
                    .add("currency", createRuleModel.getDebitCurrency()).build();

            JsonObject object = Json.createObjectBuilder()
                    .add("merchantid", merchantId)
                    .add("token", hashToken)
                    .add("creditrule", jsonObjectCredit)
                    .add("debitrule", jsonObjectDebit)
                    .build();

            Log log = new Log();
            log.setAction("creating settlement rule on core");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("responsecode", "99");

            log = new Log();
            log.setAction("creating settlement rule on core");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"00".equalsIgnoreCase(responseCode)) {

                return null;
            }

            String settlementToken = response.getString("settlementtoken");

            return settlementToken;

        } catch (Exception ex) {
            logger.error(ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public Map<String, String> createTransaction(CreateTransactionModel createTransactionModel) {
        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(TRANSACTION_RULE)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            String plainSettlementToken = CryptoUtil.DecryptData(createTransactionModel.getSettlementToken(), privateKey);

            String encryptedToken = createTransactionModel.getSettlementToken();

            createTransactionModel.setSettlementToken(plainSettlementToken);

            String hashToken = CryptoUtil.generateHash(createTransactionModel, privateKey);

            JsonObject object = Json.createObjectBuilder()
                    .add("settlementtoken", encryptedToken)
                    .add("transactionreference", createTransactionModel.getTransactionReference())
                    .add("narration", createTransactionModel.getNarration())
                    .add("currency", createTransactionModel.getCurrency())
                    .add("amount", createTransactionModel.getAmount())
                    .add("merchantid", merchantId)
                    .add("token", hashToken)
                    .build();

            Log log = new Log();
            log.setAction("creating setttlement transaction on core");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("responsecode", "99");

            log = new Log();
            log.setAction("creating setttlement transaction on core");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            Map<String, String> map = new HashMap<>();

//            if(! "00".equalsIgnoreCase(responseCode) || "R00".equalsIgnoreCase(responseCode) || "R00".equalsIgnoreCase(responseCode)){
//                
//                map.put("responsecode", responseCode);
//                
//                map.put("responsecode", responseCode);
//                return null;
//            }
            map.put("responsecode", responseCode);
            map.put("reference", response.getString("internalreference", null));

//            String reference = response.getString("internalreference");
            return map;

        } catch (Exception ex) {
            logger.error(ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    /**
     * This is used to get settlement status based on transaction reference
     *
     * @param transactionReference
     * @return
     */
    public Map<String, String> getSettlementStatus(String transactionReference) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(CORE_SETTLEMENT_STATUS)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            String hashToken = CryptoUtil.generateHash(transactionReference, privateKey);

            JsonObject object = Json.createObjectBuilder()
                    .add("transactionreference", transactionReference)
                    .add("merchantid", merchantId)
                    .add("token", hashToken)
                    .build();

            Log log = new Log();
            log.setAction("Core Settlement Status Query");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("responsecode", "99");

            log = new Log();
            log.setAction("Core Settlement Status Query");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            Map<String, String> map = new HashMap<>();

            map.put("responsecode", responseCode);
            map.put("reference", response.getString("internalreference", null));
            map.put("responsemessage", response.getString("responsemessage", null));

            return map;

        } catch (Exception ex) {
            logger.error(ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public Map<String, String> getRuleStatus(String settlementToken) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(CORE_RULE_STATUS)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            String hashToken = CryptoUtil.generateHash(settlementToken, privateKey);

            JsonObject object = Json.createObjectBuilder()
                    .add("settlementtoken", settlementToken)
                    .add("merchantid", merchantId)
                    .add("token", hashToken)
                    .build();

            Log log = new Log();
            log.setAction("Core Settlement Status Query");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("responsecode", "99");

            log = new Log();
            log.setAction("Core Settlement Status Query");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            Map<String, String> map = new HashMap<>();

            map.put("responsecode", responseCode);
            map.put("reference", response.getString("internalreference", null));
            map.put("responsemessage", response.getString("responsemessage", null));

            return map;

        } catch (Exception ex) {
            logger.error(ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public JsonObject getMerchantInformation(String merchantId) {

        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(Global.QUERY_MERCHANT_URL)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("flw-msearch-key", Global.QUERY_MERCHANT_URL_KEY);

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            JsonArray jsonArray = arrayBuilder.build();

            String content = "merchant_id=" + merchantId;

            Log log = new Log();
            log.setAction("fetching merchant details");
            log.setCreatedOn(new Date());
            log.setDescription(content);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "99");

            log = new Log();
            log.setAction("fetching merchant details");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            jsonArray = response.getJsonArray("data");

            return jsonArray.getJsonObject(0);
        } catch (Exception exception) {
            if (exception != null) {
                exception.printStackTrace();
            }
        }

        return null;
    }

    public JsonArray getSubmerchant(String parent) {

        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(Global.QUERY_MERCHANT_URL)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("flw-msearch-key", Global.QUERY_MERCHANT_URL_KEY);

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            JsonArray jsonArray = arrayBuilder.build();

            String content = "parent=" + parent;

            Log log = new Log();
            log.setAction("fetching merchant details");
            log.setCreatedOn(new Date());
            log.setDescription(content);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "99");

            log = new Log();
            log.setAction("fetching merchant details");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            jsonArray = response.getJsonArray("data");

            return jsonArray;
        } catch (Exception exception) {
            if (exception != null) {
                exception.printStackTrace();
            }
        }

        return null;
    }

    public JsonObject changeMerchantStatus(String token, boolean block) {

        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(block == true ? Global.BLOCK_MERCHANT_URL : Global.UNBLOCK_MERCHANT_URL)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
//            connection.setRequestProperty("flw-msearch-key", Global.QUERY_MERCHANT_URL_KEY);

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            JsonArray jsonArray;

            JsonObject object = Json.createObjectBuilder()
                    .add("token", token)
                    .build();

            String content = object.toString();

            Log log = new Log();
            log.setAction("changing core merchant status");
            log.setCreatedOn(new Date());
            log.setDescription(content);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "99");

            log = new Log();
            log.setAction("changing core merchant status");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            jsonArray = response.getJsonArray("data");

            return jsonArray.getJsonObject(0);
        } catch (Exception exception) {
            if (exception != null) {
                exception.printStackTrace();
            }
        }

        return null;
    }

    public String logRefund(String merchantToken, double amount, String reference) {

        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(Global.REFUND_URL)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
//            connection.setRequestProperty("flw-msearch-key", Global.QUERY_MERCHANT_URL_KEY);

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            JsonObject jsonResponse;

            JsonObject object = Json.createObjectBuilder()
                    .add("paymentreference", reference)
                    .add("refundamount", amount)
                    .add("merchantid", merchantToken)
                    .build();

            String content = object.toString();

            Log log = new Log();
            log.setAction("logging refund");
            log.setCreatedOn(new Date());
            log.setDescription(content);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "99");

            log = new Log();
            log.setAction("logging refund");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            jsonResponse = response.getJsonObject("data");

            return jsonResponse.toString();
        } catch (Exception exception) {
            if (exception != null) {
                exception.printStackTrace();
            }
        }

        return null;
    }

    /// end of core section
    //// Rave section begins
    /**
     *
     * @param merchant
     * @return
     */
    public JsonObject registerRaveMerchant(Merchant merchant) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(raveHost + "/register")).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonObject object = Json.createObjectBuilder()
                    .add("first_name", merchant.getFirstName())
                    .add("email", merchant.getEmail())
                    .add("business_name", merchant.getCompanyName())
                    .add("phone_number", merchant.getPhone())
                    .add("contact_person", merchant.getContactPerson())
                    .add("country", merchant.getCountry().getShortName())
                    .add("business_bank", merchant.getBank())
                    .add("business_accountnumber", merchant.getAccountNo())
                    .add("currency", merchant.getCurrency().getShortName())
                    .add("address_l1", merchant.getAddressLine1())
                    .add("address_l2", (merchant.getAddressLine2() == null || "".equals(merchant.getAddressLine2())) ? merchant.getAddressLine1() : merchant.getAddressLine2())
                    .add("password", "12345677")
                    .add("password_confirmation", "12345677")
                    .add("parent_account_id", 1)
                    .add("last_name", merchant.getLastName()).build();

            Log log = new Log();
            log.setAction("registering rave request");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                InputStream is = new BufferedInputStream(connection.getErrorStream());
                JsonObject response = Json.createReader(is).readObject();

                throw new Exception(response.getString("message", response.getString("message", "server error")));
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "failed");

            log = new Log();
            log.setAction("creating merchant on rave");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            object = response.getJsonObject("data");

            return object;

        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }

//        return null;
    }

    public JsonObject enableRaveMerchant(String email, boolean status) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(raveHost + "/account/status/update")).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonObject object = Json.createObjectBuilder()
                    .add("MMSKEY", "" + raveMMKEY)
                    .add("email", email)
                    .add("app", 1)
                    .add("action", status == true ? "ENABLE" : "DISABLE").build();

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            Log log = new Log();
            log.setAction("enable merchant on rave request");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "failed");

            log = new Log();
            log.setAction("creating merchant on rave");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            object = response.getJsonObject("data");

            return object;

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

//        return null;
    }

    public JsonObject goLiveRaveMerchant(String id, boolean status) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(raveHost + "/compliance/account/golive")).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonObject object = Json.createObjectBuilder()
                    .add("MMSKEY", "" + raveMMKEY)
                    .add("AccountId", id)
                    .add("is_approved", status == true ? 1 : 0)
                    .build();

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            Log log = new Log();
            log.setAction("push merchant to live");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "failed");

            log = new Log();
            log.setAction("creating merchant on rave");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            object = response.getJsonObject("data");

            return object;

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

//        return null;
    }

    public JsonObject setRaveLimit(long merchantId, MerchantLimit merchantLimit) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(raveHost + "" + RAVE_LIMIT)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonObject object = Json.createObjectBuilder()
                    .add("MMSKEY", "" + raveMMKEY)
                    .add("AccountId", merchantId)
                    .add("limit_daily", merchantLimit.getDailyLimit())
                    .add("name", "CCMG-LIMIT")
                    .add("entity", "merchant")
                    .add("limit_minimum", merchantLimit.getMinLimit())
                    .add("limit_maximum", merchantLimit.getMaxLimit()).build();

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            Log log = new Log();
            log.setAction("setting merchant limit on rave");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "failed");

            log = new Log();
            log.setAction("creating merchant on rave");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            object = response.getJsonObject("data");

            return object;

        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }

    /// end of rave section
    ///. Moneywave section begins
    public JsonObject registerMoneywaveMerchant(Merchant merchant) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(moneywaveHost + "/v1/merchant")).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonObject object = Json.createObjectBuilder()
                    .add("name", merchant.getCompanyName())
                    .add("email", merchant.getEmail())
                    .add("password", merchant.getCompanyName() + "1234")
                    .build();

            Log log = new Log();
            log.setAction("creating merchant on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "failed");

            log = new Log();
            log.setAction("creating merchant on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            object = response.getJsonObject("data");

            return object;

        } catch (Exception ex) {

        }

        return null;
    }

    public JsonObject updateMoneywaveMerchant(String secret, boolean status) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(moneywaveHost + "/v1/merchant/status")).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonObject object = Json.createObjectBuilder()
                    .add("merchant_secret", secret)
                    .add("active", status).build();

            Log log = new Log();
            log.setAction("updating merchant on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "failed");

            log = new Log();
            log.setAction("updating merchant on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            //object = response.getJsonObject("data");
            return response;

        } catch (Exception ex) {

        }

        return null;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public JsonObject complianceMoneywaveMerchant(String secret, boolean status) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(moneywaveHost + "/v1/merchant/status")).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonObject object = Json.createObjectBuilder()
                    .add("merchant_secret", secret)
                    .add("compliance_status", status).build();

            Log log = new Log();
            log.setAction("updating merchant on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "failed");

            log = new Log();
            log.setAction("updating merchant on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            //object = response.getJsonObject("data");
            return response;

        } catch (Exception ex) {

        }

        return null;
    }

    public JsonObject setMoneywaveLimit(String secret, MerchantLimit merchantLimit) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(moneywaveHost + "/v1/admin/merchant/limit")).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", generateMoneywaveToken());

            JsonObjectBuilder builder = Json.createObjectBuilder();
            builder.add("merchant_secret", secret);
            if (merchantLimit.getDailyLimit() > 0.0) {
                builder.add("daily_limit", merchantLimit.getDailyLimit());
            }
            if (merchantLimit.getMaxLimit() > 0.0) {
                builder.add("maximum_limit", merchantLimit.getMaxLimit());
            }
            if (merchantLimit.getMinLimit() > 0.0) {
                builder.add("minimum_limit", merchantLimit.getMinLimit());
            }

            JsonObject object = builder.build();

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            Log log = new Log();
            log.setAction("setting merchant limit on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "failed");

            log = new Log();
            log.setAction("setting merchant limit on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            object = response.getJsonObject("data");

            return object;

        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }

    public String creditMoneywaveWallet(long mId, long walletId, String currency, double amount) {

        try {

            HttpURLConnection connection = (HttpURLConnection) (new URL(moneywaveHost + "/v1/wallet/admin/transfer")).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", generateMoneywaveToken());

            JsonObjectBuilder builder = Json.createObjectBuilder();
            builder.add("merchant_id", mId + "");
            builder.add("wallet_id", walletId + "");
            builder.add("amount", amount + "");
            builder.add("currency", currency);
            builder.add("description", "settlement for merchant");
            builder.add("ref", "FLWMS" + (new Date()).getTime());

            JsonObject object = builder.build();

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            Log log = new Log();
            log.setAction("settling merchant on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("status", "failed");

            log = new Log();
            log.setAction("settling merchant on moneywave");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log, null);
            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"success".equalsIgnoreCase(responseCode)) {

                return null;
            }

            String message = response.getString("data", null);

            return message;

        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }

    private String generateMoneywaveToken() {

        long value = new Date().getTime();
        String va = String.format("%.3f", (double) value / 1000);
//        
        String data = CryptoUtil.encryptAES("aGTjQa/Ntz+cB5E010yPLw==", "FU3d4yLp7z+rzGagw9qIWQ==", "1|" + va + "|" + (int) (Math.random() * 1000000));

        return data;
    }

    public boolean sendNotification(String subject, String fromName, String fromEmail, String toEmail, String toName, String content) {

        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(Global.NOTIFICATION_ENDPOINT)).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonObjectBuilder builder = Json.createObjectBuilder();
            builder.add("merchantid", Global.MERCHANT_ID_LIVE + "");
            builder.add("subject", subject + "");
            builder.add("fromName", fromName + "");
            builder.add("from", fromEmail);
            builder.add("html", "" + content);

            // this creates address array
            JsonArray array = Json.createArrayBuilder().add(toEmail).build();
            builder.add("addressArray", array);

            // This creates name array
            array = Json.createArrayBuilder().add(toName).build();
            builder.add("nameArray", array);

            String stoken = CryptoUtil.sha512(Global.PRIVATE_KEY_LIVE + "" + fromName, null);

            builder.add("token", stoken);

            JsonObject object = builder.build();

            Log log = new Log();
            log.setAction("sending token to merchant");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log.toString());

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            String responseCode = response.getString("responseCode", "99");

            log = new Log();
            log.setAction("sending token ");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log);

            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            if (!"00".equalsIgnoreCase(responseCode)) {

                return false;
            }

            array = response.getJsonArray("list");

            object = array.getJsonObject(0);

            String status = object.getString("status", null);

            return "sent".equalsIgnoreCase(status);
        } catch (Exception ex) {

            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return false;
    }

    public JsonObject doRetryDisburse(String reference) {

        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(Global.REDISBURSEMENT_ENDPOINT)).openConnection();

//            reference = "PT-FLW00397006"; // is to be used for staging
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            String hash = CryptoUtil.sha512(privateKey + reference, null);

            String content = "uniquereference=" + reference + "&token=" + hash + "&merchantid=" + merchantId; // test3 - test

            Log log = new Log();
            log.setAction("Sending Redisbursement message");
            log.setCreatedOn(new Date());
            log.setDescription(content);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log.toString());

            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {

                InputStream is = new BufferedInputStream(connection.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                String responseMessage = br.lines().collect(Collectors.joining());

                log = new Log();
                log.setAction("Redisbursement Response ");
                log.setCreatedOn(new Date());
                log.setDescription(responseMessage);
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.FAILED);

                logService.log(log);

                return null;

            }

            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            log = new Log();
            log.setAction("Redisbursement Response ");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log);

            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            return response;
        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }

    public JsonObject doRetryDisburseStatus(String reference) {

        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(Global.REDISBURSEMENT_ENDPOINT + "/status")).openConnection();

//            reference = "PT-FLW00397006"; // is to be used for staging
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            String hash = CryptoUtil.sha512(privateKey + reference, null);

            String content = "uniquereference=" + reference + "&token=" + hash + "&merchantid=" + merchantId; // test3 - test

            Log log = new Log();
            log.setAction("Sending Query Redisbursement message");
            log.setCreatedOn(new Date());
            log.setDescription(content);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log.toString());

            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {

                InputStream is = new BufferedInputStream(connection.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                String responseMessage = br.lines().collect(Collectors.joining());

                log = new Log();
                log.setAction("Query Redisbursement Response");
                log.setCreatedOn(new Date());
                log.setDescription(responseMessage);
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.FAILED);

                logService.log(log);

                return null;

            }

            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            log = new Log();
            log.setAction("Query Redisbursement Response");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log);

            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            return response;
        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }
    
    public JsonObject doDisburseStatus(String reference) {

        try {
//            reference = "TTMW001123420";
            HttpURLConnection connection = (HttpURLConnection) (new URL(Global.REDISBURSEMENT_ENDPOINT_NEW + "/gettransactionstatus")).openConnection();

//            reference = "PT-FLW00397006"; // is to be used for staging
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            String hash = CryptoUtil.sha512(privateKey + reference, null).toUpperCase();

            String content = "uniquereference=" + reference + "&token=" + hash + "&merchantid=" + merchantId; // test3 - test

            Log log = new Log();
            log.setAction("Sending Query Redisbursement message");
            log.setCreatedOn(new Date());
            log.setDescription(content);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log.toString());

            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {

                InputStream is = new BufferedInputStream(connection.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                String responseMessage = br.lines().collect(Collectors.joining());

                log = new Log();
                log.setAction("Query Redisbursement Response");
                log.setCreatedOn(new Date());
                log.setDescription(responseMessage);
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.FAILED);

                logService.log(log);

                return null;

            }

            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            log = new Log();
            log.setAction("Query Redisbursement Response");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setStatus(LogStatus.SUCCESSFUL);

            logService.log(log);

            Logger.getLogger(HttpUtil.class.getName()).info(log.toString());

            return response;
        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }

    public String doHttpPost(String url, String body, Map<String, String> header) {

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");

            if (header != null) {
                for (Map.Entry<String, String> key : header.entrySet()) {
                    connection.addRequestProperty(key.getKey(), key.getValue());
                }
            }

            String temp = "U:" + url + ",B:" + body + ",H:" + ((header == null) ? "" : "" + header.toString());

            logger.info(temp);
//            Logger.getLogger(HttpUtil.class.getName()).info(temp);

//            String result  = null;
            if (body != null) {
                byte[] postData = body.getBytes();
                OutputStream os = connection.getOutputStream();

                os.write(postData);

                os.flush();
                os.close();
            }
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201 && resultCode != 204) {

                InputStream errorStream = connection.getErrorStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(errorStream));
                String result = br.lines().collect(Collectors.joining());

                throw new Exception(result);
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String result = br.lines().collect(Collectors.joining(""));

            logger.info(result);

            return result;
        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }
    
    public String doHttpPut(String url, String body, Map<String, String> header) {

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("PUT");

            if (header != null) {
                for (Map.Entry<String, String> key : header.entrySet()) {
                    connection.addRequestProperty(key.getKey(), key.getValue());
                }
            }

            String temp = "U:" + url + ",B:" + body + ",H:" + ((header == null) ? "" : "" + header.toString());

            logger.info(temp);
//            Logger.getLogger(HttpUtil.class.getName()).info(temp);

//            String result  = null;
            if (body != null) {
                byte[] postData = body.getBytes();
                OutputStream os = connection.getOutputStream();

                os.write(postData);

                os.flush();
                os.close();
            }
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201 && resultCode != 204) {

                InputStream errorStream = connection.getErrorStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(errorStream));
                String result = br.lines().collect(Collectors.joining());

                throw new Exception(result);
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String result = br.lines().collect(Collectors.joining(""));

            logger.info(result);

            return result;
        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }
    
    public InputStream doHttpPostRaw(String url, String body, Map<String, String> header) {

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");

            if (header != null) {
                for (Map.Entry<String, String> key : header.entrySet()) {
                    connection.addRequestProperty(key.getKey(), key.getValue());
                }
            }

            String temp = "U:" + url + ",B:" + body + ",H:" + ((header == null) ? "" : "" + header.toString());

            logger.info(temp);
//            Logger.getLogger(HttpUtil.class.getName()).info(temp);

//            String result  = null;
            if (body != null) {
                byte[] postData = body.getBytes();
                OutputStream os = connection.getOutputStream();

                os.write(postData);

                os.flush();
                os.close();
            }
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201 && resultCode != 204) {

                InputStream errorStream = connection.getErrorStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(errorStream));
                String result = br.lines().collect(Collectors.joining());

                logger.info(result);
                return null;
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
//            BufferedReader br = new BufferedReader(new InputStreamReader(is));
//
//            String result = br.lines().collect(Collectors.joining(""));

            

            return is;
        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }

    public String doHttpGet(String url, Map<String, String> header) {

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
//            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("GET");

            if (header != null) {
                header.entrySet().forEach((key) -> {
                    connection.addRequestProperty(key.getKey(), key.getValue());
                });
            }

            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String result = br.lines().collect(Collectors.joining(""));

            logger.info(result);

            return result;
        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }

    /// end of moneywave utilities
    public Map<String, String> doHttpPostC(String url, String body, Map<String, String> headers) {

        try {
            HttpPost httpPost = new HttpPost(url);

            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true).build();

            CloseableHttpClient client = HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier(new NoopHostnameVerifier()).build();

            httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");

            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpPost.addHeader(entry.getKey(), entry.getValue());
            }

            Log log = new Log();
            log.setAction("Call Back to URL - " + url);
            log.setCreatedOn(new Date());
            log.setDescription(body);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.STARTED);
            log.setLogDomain(LogDomain.ADMIN);
            log.setUsername("ADMIN");

            logService.log(log);

            StringEntity entity = new StringEntity(body);

            httpPost.setEntity(entity);
            HttpResponse response = client.execute(httpPost);
//            byte[] postData = object.toString().getBytes();
//            OutputStream os = connection.getOutputStream();

            Logger.getLogger(HttpUtil.class.getName()).info(body);

//            os.write(postData);
//
//            os.flush();
//            os.close();
//            long resultCode = (long) connection.getResponseCode();
            int resultCode = response.getStatusLine().getStatusCode();

            java.util.logging.Logger.getLogger(HttpUtil.class.getName()).log(Level.INFO, "Result code is {0}", resultCode);
            InputStream stream = response.getEntity().getContent();

            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            String result = br.lines().collect(Collectors.joining());

            System.out.println(result);

            Map<String, String> responseMap = new HashMap<>();

            if (resultCode != 200 && resultCode != 201) {
//
//                InputStream is = new BufferedInputStream(connection.getErrorStream());
//                BufferedReader br = new BufferedReader(new InputStreamReader(is));
//                String response = br.lines().collect(Collectors.joining(""));

                log = new Log();
                log.setAction("Call Back to URL - " + url);
                log.setCreatedOn(new Date());
                log.setDescription(result);
                log.setLevel(LogLevel.Info);
                log.setStatus(LogStatus.FAILED);
                log.setLogState(LogState.FINISH);
                log.setLogDomain(LogDomain.ADMIN);
                log.setUsername("ADMIN");

                logService.log(log);

                responseMap.put("status", "failed");
                responseMap.put("result", result);

                return responseMap;
//                return null;
            }

//            InputStream is = new BufferedInputStream(connection.getInputStream());
//            BufferedReader buffer = new BufferedReader(new InputStreamReader(is));
//            String response = buffer.lines().collect(Collectors.joining("\n"));
            log = new Log();
            log.setAction("Call Back to URL - " + url);
            log.setCreatedOn(new Date());
            log.setDescription(result);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.STARTED);
            log.setLogDomain(LogDomain.ADMIN);
            log.setUsername("ADMIN");

            logService.log(log);

            responseMap.put("status", "success");
            responseMap.put("result", result);

            return responseMap;
        } catch (Exception ex) {
            Logger.getLogger(HttpUtil.class.getName()).error( null, ex);
        }

        return null;
    }
    
    public InputStream doHttpGetCRaw(String url, Map<String, String> headers) {

        try {
            HttpGet httpPost = new HttpGet(url);

            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true).build();

            CloseableHttpClient client = HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier(new NoopHostnameVerifier()).build();

            httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");

            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpPost.addHeader(entry.getKey(), entry.getValue());
            }

            Log log = new Log();
            log.setAction("Call Back to URL - " + url);
            log.setCreatedOn(new Date());
            log.setDescription("No Body");
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLogState(LogState.STARTED);
            log.setLogDomain(LogDomain.ADMIN);
            log.setUsername("ADMIN");

            logService.log(log);

//            StringEntity entity = new StringEntity(body);

//            httpPost.setEntity(entity);
            HttpResponse response = client.execute(httpPost);
//            byte[] postData = object.toString().getBytes();
//            OutputStream os = connection.getOutputStream();

//            Logger.getLogger(HttpUtil.class.getName()).info(body);

//            os.write(postData);
//
//            os.flush();
//            os.close();
//            long resultCode = (long) connection.getResponseCode();
            int resultCode = response.getStatusLine().getStatusCode();

            java.util.logging.Logger.getLogger(HttpUtil.class.getName()).log(Level.INFO, "Result code is {0}", resultCode);
            InputStream stream = response.getEntity().getContent();

//            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
//            String result = br.lines().collect(Collectors.joining());

//            System.out.println(result);

//            /Map<String, String> responseMap = new HashMap<>();

            if (resultCode != 200 && resultCode != 201) {
//
//                InputStream is = new BufferedInputStream(connection.getErrorStream());
//                BufferedReader br = new BufferedReader(new InputStreamReader(is));
//                String response = br.lines().collect(Collectors.joining(""));

//                log = new Log();
//                log.setAction("Call Back to URL - " + url);
//                log.setCreatedOn(new Date());
//                log.setDescription(result);
//                log.setLevel(LogLevel.Info);
//                log.setStatus(LogStatus.FAILED);
//                log.setLogState(LogState.FINISH);
//                log.setLogDomain(LogDomain.ADMIN);
//                log.setUsername("ADMIN");
//
//                logService.log(log);
//
//                responseMap.put("status", "failed");
//                responseMap.put("result", result);

                return null;
//                return null;
            }

//            InputStream is = new BufferedInputStream(connection.getInputStream());
//            BufferedReader buffer = new BufferedReader(new InputStreamReader(is));
//            String response = buffer.lines().collect(Collectors.joining("\n"));
//            log = new Log();
//            log.setAction("Call Back to URL - " + url);
//            log.setCreatedOn(new Date());
//            log.setDescription(result);
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setLogState(LogState.STARTED);
//            log.setLogDomain(LogDomain.ADMIN);
//            log.setUsername("ADMIN");
//
//            logService.log(log);
//
//            responseMap.put("status", "success");
//            responseMap.put("result", result);

            return stream;
        } catch (Exception ex) {
            Logger.getLogger(HttpUtil.class.getName()).error( null, ex);
        }

        return null;
    }
    
    public String doHttpDelete(String url, String body, Map<String,String> header){
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("DELETE");
            
            if(header != null)
                header.entrySet().forEach((key) -> {
                    connection.addRequestProperty(key.getKey(), key.getValue());
                });
            
            
            if(body != null){
                byte[] postData = body.getBytes();
                OutputStream os = connection.getOutputStream();

                os.write(postData);

                os.flush();
                os.close();
            }
            
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 204) {
                throw new Exception("Server not available");
            }

            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            
            result = br.lines().collect(Collectors.joining(""));
            
            return result;
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(HttpUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.clientms.model.DateModel;
import com.flutterwave.flutter.clientms.model.PosSettlementModel;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.TransactionListModel;
import com.flutterwave.flutter.clientms.viewmodel.CoreReportViewModel;
import com.flutterwave.flutter.clientms.viewmodel.FailureAnalysisModel;
import com.flutterwave.flutter.clientms.viewmodel.RaveSettlementModel;
import com.flutterwave.flutter.clientms.viewmodel.RaveSettlementViewModel;
import com.flutterwave.flutter.clientms.viewmodel.SettlementViewModel;
import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.json.Json;
import javax.json.JsonArray;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.charts.AxisCrosses;
import org.apache.poi.ss.usermodel.charts.AxisPosition;
import org.apache.poi.ss.usermodel.charts.ChartDataSource;
import org.apache.poi.ss.usermodel.charts.DataSources;
import org.apache.poi.ss.usermodel.charts.LegendPosition;
import org.apache.poi.ss.usermodel.charts.LineChartSeries;
import org.apache.poi.ss.usermodel.charts.ValueAxis;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.charts.XSSFChartAxis;
import org.apache.poi.xssf.usermodel.charts.XSSFChartLegend;
import org.apache.poi.xssf.usermodel.charts.XSSFLineChartData;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Day;
import org.jfree.data.time.Month;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleInsets;

public class FillManager {

    {
        TransactionUtility.buildWrapperClass();
    }

    /**
     * This is called to provide report for transactions
     *
     * @param worksheet
     * @param startRowIndex
     * @param startColIndex
     * @param datasource
     * @param exclude
     */
    public static void fillTransactionReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<Object> datasource,
            String... exclude) {
        // Row offset
        //startRowIndex += 2;

        // Create cell style for the body
        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int listSize = datasource.size();

        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).collect(Collectors.toList());
        }
        // Create body
        for (int i = startRowIndex; i < listSize + 2; i++) {
            // Create a new row
            HSSFRow row = worksheet.createRow((short) i + 1);

            Object transaction = datasource.get(i - 2);

            int counter = 0;

            String internalReference = null;
            // This used reflection to fill the field in the date
            for (Field field : transaction.getClass().getDeclaredFields()) {

                field.setAccessible(true);
//        if(field.getType() == String.class){
                //                System.out.println(field.getName());

                try {
                    Object object = field.get(transaction);

                    String fieldName = field.getName().toLowerCase();
                    
                    if (exclusions != null) {

                        int index = exclusions.indexOf(field.getName());

                        if (index > -1) {
                            
                            if (!fieldName.equalsIgnoreCase("transactionData")) {
                                continue;
                            }
                        }
                    }

//                    String fieldName = field.getName().toLowerCase();
                    
                    HSSFCell cell = row.createCell(startColIndex + counter++);
                    //cell2.setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(datasource.get(i-2).getExpiryDate()));

                    Class ccc = TransactionUtility.map.getOrDefault(field.getType(), field.getType());

                    if (ccc == Integer.class) {
                        cell.setCellValue(object != null ? Integer.parseInt(object.toString()) : null);
                    } else if (ccc == Long.class) {
                        cell.setCellValue(object != null ? Long.parseLong(object.toString()) : null);
                    } else if (ccc == Double.class) {
                        cell.setCellValue(object != null ? Double.parseDouble(object.toString()) : null);
                    } else if (ccc == Date.class) {
                        cell.setCellValue(Utility.formatDate((Date)object, "yyyy-MM-dd HH:mm:ss"));
                    } 
                    else {

                        if (transaction.getClass() == Transaction.class) {

                            if (fieldName.equalsIgnoreCase("transactionData")) {
                                
                                Transaction x = (Transaction) transaction;

                                String transactionData = x.getTransactionData();
                                 

                                    if (transactionData != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(transactionData.getBytes())).readArray();

                                        internalReference = Utility.getData(array, "internalreference");
                                        
                                        cell.setCellValue(transactionData);
                                    }
                                
                            }
                            else if (fieldName.equalsIgnoreCase("provider")) {

                                Transaction x = (Transaction) transaction;

                                String prov = x.getProvider();

                                if (prov != null && prov.toUpperCase().startsWith("MCASH")) {

                                    String processReference = x.getProcessorReference();

                                    if (processReference != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();

                                        String merchantCode = Utility.getData(array, "merchantcode");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)
//                                    x.setMerchantId(x.getMerchantId() +"("+merchantCode+")");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)

                                        String temp = x.getMerchantId();

                                        temp += " (" + merchantCode + ") ";
                                        
                                        x.setMerchantId(temp);
                                        cell.setCellValue(object != null ? object.toString() : "");
                                    }
                                } else {
                                    cell.setCellValue(object != null ? object.toString() : null);
                                }

                            } else {
                                cell.setCellValue(object != null ? object.toString() : null);
                            }

                        } else if (transaction.getClass() == TransactionListModel.class) {

                            if (fieldName.equalsIgnoreCase("transactionData")) {
                                
                                TransactionListModel x = (TransactionListModel) transaction;

                                String transactionData = x.getTransactionData();
                                 

                                    if (transactionData != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(transactionData.getBytes())).readArray();

                                        internalReference = Utility.getData(array, "internalreference");
                                        
                                        cell.setCellValue(transactionData);
                                    }
                                
                            }
                            else if (fieldName.equalsIgnoreCase("provider")) {

                                TransactionListModel x = (TransactionListModel) transaction;

                                String prov = x.getProvider();

                                if (prov != null && prov.toUpperCase().startsWith("MCASH")) {

                                    String processReference = x.getProcessorReference();

                                    if (processReference != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();

                                        String merchantCode = Utility.getData(array, "merchantcode");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)
//                                    x.setMerchantId(x.getMerchantId() +"("+merchantCode+")");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)

                                        String temp = x.getMerchantId();

                                        temp += " (" + merchantCode + ") ";
                                        
                                        x.setMerchantId(temp);
                                        cell.setCellValue(object != null ? object.toString() : "");
                                    }
                                } else {
                                    cell.setCellValue(object != null ? object.toString() : null);
                                }

                            } else {
                                cell.setCellValue(object != null ? object.toString() : null);
                            }
                        
                        } else {
                            cell.setCellValue(object != null ? object.toString() : null);
                        }

//                        cell.setCellValue(object != null ? object.toString() : null);
                    }

                    cell.setCellStyle(bodyCellStyle);
//                System.out.println(object.toString());

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
//        }
            }
            
            HSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(internalReference);
            cell.setCellStyle(bodyCellStyle);

        }
    }
    
    public static void fillTransactionReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, String dateformat, List<Object> datasource,
            String... exclude) {
        // Row offset
        //startRowIndex += 2;

        // Create cell style for the body
        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int listSize = datasource.size();

        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).collect(Collectors.toList());
        }
        // Create body
        for (int i = startRowIndex; i < listSize + 2; i++) {
            // Create a new row
            HSSFRow row = worksheet.createRow((short) i + 1);

            Object transaction = datasource.get(i - 2);

            int counter = 0;

            String internalReference = null;
            // This used reflection to fill the field in the date
            for (Field field : transaction.getClass().getDeclaredFields()) {

                field.setAccessible(true);
//        if(field.getType() == String.class){
                //                System.out.println(field.getName());

                try {
                    Object object = field.get(transaction);

                    String fieldName = field.getName().toLowerCase();
                    
                    if (exclusions != null) {

                        int index = exclusions.indexOf(field.getName());

                        if (index > -1) {
                            
                            if (!fieldName.equalsIgnoreCase("transactionData")) {
                                continue;
                            }
                        }
                    }

//                    String fieldName = field.getName().toLowerCase();
                    
                    HSSFCell cell = row.createCell(startColIndex + counter++);
                    //cell2.setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(datasource.get(i-2).getExpiryDate()));

                    Class ccc = TransactionUtility.map.getOrDefault(field.getType(), field.getType());

                    if (ccc == Integer.class) {
                        cell.setCellValue(object != null ? Integer.parseInt(object.toString()) : null);
                    } else if (ccc == Long.class) {
                        cell.setCellValue(object != null ? Long.parseLong(object.toString()) : null);
                    } else if (ccc == Double.class) {
                        cell.setCellValue(object != null ? Double.parseDouble(object.toString()) : null);
                    } else if (ccc == Date.class) {
                        cell.setCellValue(Utility.formatDate((Date)object, dateformat == null ? "yyyy-MM-dd HH:mm:ss" : dateformat));
                    } 
                    else {

                        if (transaction.getClass() == Transaction.class) {

                            if (fieldName.equalsIgnoreCase("transactionData")) {
                                
                                Transaction x = (Transaction) transaction;

                                String transactionData = x.getTransactionData();
                                 

                                    if (transactionData != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(transactionData.getBytes())).readArray();

                                        internalReference = Utility.getData(array, "internalreference");
                                        
                                        cell.setCellValue(transactionData);
                                    }
                                
                            }
                            else if (fieldName.equalsIgnoreCase("provider")) {

                                Transaction x = (Transaction) transaction;

                                String prov = x.getProvider();

                                if (prov != null && prov.toUpperCase().startsWith("MCASH")) {

                                    String processReference = x.getProcessorReference();

                                    if (processReference != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();

                                        String merchantCode = Utility.getData(array, "merchantcode");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)
//                                    x.setMerchantId(x.getMerchantId() +"("+merchantCode+")");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)

                                        String temp = x.getMerchantId();

                                        temp += " (" + merchantCode + ") ";
                                        
                                        x.setMerchantId(temp);
                                        cell.setCellValue(object != null ? object.toString() : "");
                                    }
                                } else {
                                    cell.setCellValue(object != null ? object.toString() : null);
                                }

                            } else {
                                cell.setCellValue(object != null ? object.toString() : null);
                            }

                        } else if (transaction.getClass() == TransactionListModel.class) {

                            if (fieldName.equalsIgnoreCase("transactionData")) {
                                
                                TransactionListModel x = (TransactionListModel) transaction;

                                String transactionData = x.getTransactionData();
                                 

                                    if (transactionData != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(transactionData.getBytes())).readArray();

                                        internalReference = Utility.getData(array, "internalreference");
                                        
                                        cell.setCellValue(transactionData);
                                    }
                                
                            }
                            else if (fieldName.equalsIgnoreCase("provider")) {

                                TransactionListModel x = (TransactionListModel) transaction;

                                String prov = x.getProvider();

                                if (prov != null && prov.toUpperCase().startsWith("MCASH")) {

                                    String processReference = x.getProcessorReference();

                                    if (processReference != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();

                                        String merchantCode = Utility.getData(array, "merchantcode");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)
//                                    x.setMerchantId(x.getMerchantId() +"("+merchantCode+")");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)

                                        String temp = x.getMerchantId();

                                        temp += " (" + merchantCode + ") ";
                                        
                                        x.setMerchantId(temp);
                                        cell.setCellValue(object != null ? object.toString() : "");
                                    }
                                } else {
                                    cell.setCellValue(object != null ? object.toString() : null);
                                }

                            } else {
                                cell.setCellValue(object != null ? object.toString() : null);
                            }
                        
                        } else {
                            cell.setCellValue(object != null ? object.toString() : null);
                        }

//                        cell.setCellValue(object != null ? object.toString() : null);
                    }

                    cell.setCellStyle(bodyCellStyle);
//                System.out.println(object.toString());

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
//        }
            }
            
            HSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(internalReference);
            cell.setCellStyle(bodyCellStyle);

        }
    }

    public static void fillTransactionReport(XSSFSheet worksheet, int startRowIndex, int startColIndex, List<Object> datasource,
            String... exclude) {
        // Row offset
        //startRowIndex += 2;

        // Create cell style for the body
        XSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int listSize = datasource.size();

        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).collect(Collectors.toList());
        }
        // Create body
        for (int i = startRowIndex; i < listSize + 2; i++) {
            // Create a new row
            XSSFRow row = worksheet.createRow(i + 1);

            Object transaction = datasource.get(i - 2);

            int counter = 0;

            String internalReference = null;
            // This used reflection to fill the field in the date
            for (Field field : transaction.getClass().getDeclaredFields()) {

                field.setAccessible(true);
//        if(field.getType() == String.class){
                //                System.out.println(field.getName());

                try {
                    Object object = field.get(transaction);

                    String fieldName = field.getName().toLowerCase();
                    
                    if (exclusions != null) {

                        int index = exclusions.indexOf(field.getName());

                        if (index > -1) {
                            
                            if (!fieldName.equalsIgnoreCase("transactionData")) {
                                continue;
                            }
                        }
                    }

//                    String fieldName = field.getName().toLowerCase();
                    
                    XSSFCell cell = row.createCell(startColIndex + counter++);
                    //cell2.setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(datasource.get(i-2).getExpiryDate()));

                    Class ccc = TransactionUtility.map.getOrDefault(field.getType(), field.getType());

                    if (ccc == Integer.class) {
                        cell.setCellValue(object != null ? Integer.parseInt(object.toString()) : null);
                    } else if (ccc == Long.class) {
                        cell.setCellValue(object != null ? Long.parseLong(object.toString()) : null);
                    } else if (ccc == Double.class) {
                        cell.setCellValue(object != null ? Double.parseDouble(object.toString()) : null);
                    } else if (ccc == Date.class) {
                        cell.setCellValue(Utility.formatDate((Date)object, "yyyy-MM-dd HH:mm:ss"));
                    } 
                    else {

                        if (transaction.getClass() == Transaction.class) {

                            if (fieldName.equalsIgnoreCase("transactionData")) {
                                
                                Transaction x = (Transaction) transaction;

                                String transactionData = x.getTransactionData();
                                 

                                    if (transactionData != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(transactionData.getBytes())).readArray();

                                        internalReference = Utility.getData(array, "internalreference");
                                        
                                        cell.setCellValue(transactionData);
                                    }
                                
                            }
                            else if (fieldName.equalsIgnoreCase("provider")) {

                                Transaction x = (Transaction) transaction;

                                String prov = x.getProvider();

                                if (prov != null && prov.toUpperCase().startsWith("MCASH")) {

                                    String processReference = x.getProcessorReference();

                                    if (processReference != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();

                                        String merchantCode = Utility.getData(array, "merchantcode");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)
//                                    x.setMerchantId(x.getMerchantId() +"("+merchantCode+")");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)

                                        String temp = x.getMerchantId();

                                        temp += " (" + merchantCode + ") ";
                                        
                                        x.setMerchantId(temp);
                                        cell.setCellValue(object != null ? object.toString() : "");
                                    }
                                } else {
                                    cell.setCellValue(object != null ? object.toString() : null);
                                }

                            } else {
                                cell.setCellValue(object != null ? object.toString() : null);
                            }

                        } else if (transaction.getClass() == TransactionListModel.class) {

                            if (fieldName.equalsIgnoreCase("transactionData")) {
                                
                                TransactionListModel x = (TransactionListModel) transaction;

                                String transactionData = x.getTransactionData();
                                 

                                    if (transactionData != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(transactionData.getBytes())).readArray();

                                        internalReference = Utility.getData(array, "internalreference");
                                        
                                        cell.setCellValue(transactionData);
                                    }
                                
                            }
                            else if (fieldName.equalsIgnoreCase("provider")) {

                                TransactionListModel x = (TransactionListModel) transaction;

                                String prov = x.getProvider();

                                if (prov != null && prov.toUpperCase().startsWith("MCASH")) {

                                    String processReference = x.getProcessorReference();

                                    if (processReference != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();

                                        String merchantCode = Utility.getData(array, "merchantcode");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)
//                                    x.setMerchantId(x.getMerchantId() +"("+merchantCode+")");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)

                                        String temp = x.getMerchantId();

                                        temp += " (" + merchantCode + ") ";
                                        
                                        x.setMerchantId(temp);
                                        cell.setCellValue(object != null ? object.toString() : "");
                                    }
                                } else {
                                    cell.setCellValue(object != null ? object.toString() : null);
                                }

                            } else {
                                cell.setCellValue(object != null ? object.toString() : null);
                            }
                        
                        } else {
                            cell.setCellValue(object != null ? object.toString() : null);
                        }

//                        cell.setCellValue(object != null ? object.toString() : null);
                    }

                    cell.setCellStyle(bodyCellStyle);
//                System.out.println(object.toString());

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
//        }
            }
            
            XSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(internalReference);
            cell.setCellStyle(bodyCellStyle);

        }
    }

    public static void fillTransactionReport(XSSFSheet worksheet, int startRowIndex, int startColIndex, String dateformat, List<Object> datasource,
            String... exclude) {
        // Row offset
        //startRowIndex += 2;

        // Create cell style for the body
        XSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int listSize = datasource.size();

        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).collect(Collectors.toList());
        }
        // Create body
        for (int i = startRowIndex; i < listSize + 2; i++) {
            // Create a new row
            XSSFRow row = worksheet.createRow(i + 1);

            Object transaction = datasource.get(i - 2);

            int counter = 0;

            String internalReference = null;
            // This used reflection to fill the field in the date
            for (Field field : transaction.getClass().getDeclaredFields()) {

                field.setAccessible(true);
//        if(field.getType() == String.class){
                //                System.out.println(field.getName());

                try {
                    Object object = field.get(transaction);

                    String fieldName = field.getName().toLowerCase();
                    
                    if (exclusions != null) {

                        int index = exclusions.indexOf(field.getName());

                        if (index > -1) {
                            
                            if (!fieldName.equalsIgnoreCase("transactionData")) {
                                continue;
                            }
                        }
                    }

//                    String fieldName = field.getName().toLowerCase();
                    
                    XSSFCell cell = row.createCell(startColIndex + counter++);
                    //cell2.setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(datasource.get(i-2).getExpiryDate()));

                    Class ccc = TransactionUtility.map.getOrDefault(field.getType(), field.getType());

                    if (ccc == Integer.class) {
                        cell.setCellValue(object != null ? Integer.parseInt(object.toString()) : null);
                    } else if (ccc == Long.class) {
                        cell.setCellValue(object != null ? Long.parseLong(object.toString()) : null);
                    } else if (ccc == Double.class) {
                        cell.setCellValue(object != null ? Double.parseDouble(object.toString()) : null);
                    } else if (ccc == Date.class) {
                        
                        
                        cell.setCellValue(Utility.formatDate((Date)object, dateformat == null ? "yyyy-MM-dd HH:mm:ss" : dateformat));
                    } 
                    else {

                        if (transaction.getClass() == Transaction.class) {

                            if (fieldName.equalsIgnoreCase("transactionData")) {
                                
                                Transaction x = (Transaction) transaction;

                                String transactionData = x.getTransactionData();
                                 

                                    if (transactionData != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(transactionData.getBytes())).readArray();

                                        internalReference = Utility.getData(array, "internalreference");
                                        
                                        cell.setCellValue(transactionData);
                                    }
                                
                            }
                            else if (fieldName.equalsIgnoreCase("provider")) {

                                Transaction x = (Transaction) transaction;

                                String prov = x.getProvider();

                                if (prov != null && prov.toUpperCase().startsWith("MCASH")) {

                                    String processReference = x.getProcessorReference();

                                    if (processReference != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();

                                        String merchantCode = Utility.getData(array, "merchantcode");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)
//                                    x.setMerchantId(x.getMerchantId() +"("+merchantCode+")");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)

                                        String temp = x.getMerchantId();

                                        temp += " (" + merchantCode + ") ";
                                        
                                        x.setMerchantId(temp);
                                        cell.setCellValue(object != null ? object.toString() : "");
                                    }
                                } else {
                                    cell.setCellValue(object != null ? object.toString() : null);
                                }

                            } else {
                                cell.setCellValue(object != null ? object.toString() : null);
                            }

                        } else if (transaction.getClass() == TransactionListModel.class) {

                            if (fieldName.equalsIgnoreCase("transactionData")) {
                                
                                TransactionListModel x = (TransactionListModel) transaction;

                                String transactionData = x.getTransactionData();
                                 

                                    if (transactionData != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(transactionData.getBytes())).readArray();

                                        internalReference = Utility.getData(array, "internalreference");
                                        
                                        cell.setCellValue(transactionData);
                                    }
                                
                            }
                            else if (fieldName.equalsIgnoreCase("provider")) {

                                TransactionListModel x = (TransactionListModel) transaction;

                                String prov = x.getProvider();

                                if (prov != null && prov.toUpperCase().startsWith("MCASH")) {

                                    String processReference = x.getProcessorReference();

                                    if (processReference != null) {

                                        JsonArray array = Json.createReader(new ByteArrayInputStream(processReference.getBytes())).readArray();

                                        String merchantCode = Utility.getData(array, "merchantcode");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)
//                                    x.setMerchantId(x.getMerchantId() +"("+merchantCode+")");
                                        //                        String merchantName = jsonObject.getString("merchantname", status)

                                        String temp = x.getMerchantId();

                                        temp += " (" + merchantCode + ") ";
                                        
                                        x.setMerchantId(temp);
                                        cell.setCellValue(object != null ? object.toString() : "");
                                    }
                                } else {
                                    cell.setCellValue(object != null ? object.toString() : null);
                                }

                            } else {
                                cell.setCellValue(object != null ? object.toString() : null);
                            }
                        
                        } else {
                            cell.setCellValue(object != null ? object.toString() : null);
                        }

//                        cell.setCellValue(object != null ? object.toString() : null);
                    }

                    cell.setCellStyle(bodyCellStyle);
//                System.out.println(object.toString());

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
//        }
            }
            
            XSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(internalReference);
            cell.setCellStyle(bodyCellStyle);

        }
    }
    
    /**
     * This build report for transactions
     *
     * @param worksheet
     * @param startRowIndex
     * @param startColIndex
     * @param datasource
     */
    public static void productReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, Map<String, Object> datasource) {
        // Row offset
        //startRowIndex += 2;

        // Create cell style for the body
        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int currentRow = 2;

        for (String data : datasource.keySet()) {

            Map<String, Double> rowData = (HashMap<String, Double>) datasource.get(data);

            // This create the cell and set the content to be the header of the data
            HSSFRow row = worksheet.createRow(currentRow++);
            HSSFCell cell = row.createCell(0);
            cell.setCellValue(data);

            // This is called to create a row for each entry in the map
            row = worksheet.createRow(currentRow++);

            int col = 0;

            for (String title : rowData.keySet()) {
                HSSFCell sFCell = row.createCell(col++);
                sFCell.setCellValue(title);
            }

            row = worksheet.createRow(currentRow++);

            for (Double value : rowData.values()) {
                HSSFCell sFCell = row.createCell(col++);
                sFCell.setCellValue(value);
            }

            worksheet.createRow(currentRow++);
        }
    }

    public static void coreReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List datasource) {

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int currentRow = 3;

        for (Object obj : datasource) {

            Map<String, Object> rowData = (HashMap<String, Object>) obj;

            int count = 0;

            HSSFRow row = null;

            int counter = 0;

            row = worksheet.createRow(currentRow++);

            HSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(String.valueOf(rowData.getOrDefault("Date", null)));

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(Utility.convertTo2DP((rowData.getOrDefault("volume", 0.0)) + ""));

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(Utility.convertTo2DP((rowData.getOrDefault("value", 0) + "")));

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(Utility.convertTo2DP((rowData.getOrDefault("revenue", 0.0)) + ""));
            cell = row.createCell(startColIndex + counter++);
            cell.setCellStyle(getCellStyle(rowData.getOrDefault("volumegrowth", 0) + "", worksheet));
            cell.setCellValue(Utility.convertTo2DP((rowData.getOrDefault("volumegrowth", 0.0) + "")));
            cell = row.createCell(startColIndex + counter++);
            cell.setCellStyle(getCellStyle(rowData.getOrDefault("valuegrowth", 0) + "", worksheet));
            cell.setCellValue(Utility.convertTo2DP((rowData.getOrDefault("valuegrowth", 0.0)) + ""));
            cell = row.createCell(startColIndex + counter++);
            cell.setCellStyle(getCellStyle(rowData.getOrDefault("revenuegrowth", 0) + "", worksheet));
            cell.setCellValue(Utility.convertTo2DP((rowData.getOrDefault("revenuegrowth", 0)) + ""));

        }

    }

    public static void coreReportToDate(HSSFSheet worksheet, int startRowIndex, int startColIndex, Map<String, Map> datasource) {

        Font font = worksheet.getWorkbook().createFont();
        font.setFontHeightInPoints((short) 10);
        font.setBold(true);

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
//        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);
        bodyCellStyle.setFont(font);

        int currentRow = 3;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM");
        for (String data : datasource.keySet()) {

            HSSFRow row = null;

            int counter = 0;

            row = worksheet.createRow(currentRow++);

            HSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellStyle(bodyCellStyle);
            cell.setCellValue(String.valueOf(data));

            Map<Date, Object> rowD = new TreeMap<>(datasource.getOrDefault(data, new TreeMap()));

            for (Date date : rowD.keySet()) {

                worksheet.createRow(currentRow++);

                row = worksheet.createRow(currentRow++);

                counter = 0;

                cell = row.createCell(startColIndex + counter++);

                cell.setCellStyle(bodyCellStyle);
                cell.setCellValue(dateFormat.format(date));

                Map<String, Object> values = (HashMap) rowD.getOrDefault(date, new HashMap<>());

                row = worksheet.createRow(currentRow++);

                counter = 0;

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(values.getOrDefault("currency", null) + "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(values.getOrDefault("volume", 0.0) + ""));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(values.getOrDefault("value", 0.0) + ""));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(values.getOrDefault("revenue", 0.0) + ""));

            }

        }

    }

    public static void buildCoreReportTrans(XSSFSheet worksheet, int startRowIndex, int startColIndex, Map<String, Map<Date, CoreReportViewModel>> datasource, int diff) throws IOException {

        Font font = worksheet.getWorkbook().createFont();
        font.setFontHeightInPoints((short) 10);
        font.setBold(true);

        XSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
//        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);
        bodyCellStyle.setFont(font);

        int currentRow = 3;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM");

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        DefaultCategoryDataset datasetVolume = new DefaultCategoryDataset();

        int i = 0, j = 0;

        int start = datasource.size() * diff + startRowIndex + 6;

        String format = "dd-MM-yyyy", format2 = "dd-MM";

        for (String data : datasource.keySet()) {

            i += 1;
            XSSFRow row = null;

            int counter = 0;

            row = worksheet.createRow(currentRow++);

            XSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellStyle(bodyCellStyle);
            cell.setCellValue(data);

            Map<Date, CoreReportViewModel> rowD = new TreeMap<>(datasource.getOrDefault(data, new TreeMap()));

            int rowStart = currentRow;

            int k = 0;

            TimeSeries successSeries = new TimeSeries("Card Successful");
            TimeSeries failedSeries = new TimeSeries("Card Failed");

            TimeSeries successVolSeries = new TimeSeries("Card Successful");
            TimeSeries failedVolSeries = new TimeSeries("Card Failed");

            for (Date date : rowD.keySet()) {

                j++;

                k++;

                row = worksheet.createRow(currentRow++);

                counter = 0;

                cell = row.createCell(startColIndex + counter++);

                cell.setCellStyle(bodyCellStyle);
                cell.setCellValue(Utility.formatDate(date, "dd-MM-yyyy"));

                CoreReportViewModel model = (CoreReportViewModel) rowD.getOrDefault(date, new CoreReportViewModel());

//                row = worksheet.createRow(currentRow++);
//                counter = 1;
                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(model.getCardSuccessValue());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(model.getCardSuccessVolume().longValue());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(model.getCardFailureValue());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(model.getCardFailureVolume().longValue());

                //Account section
                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(model.getAccountSuccessValue());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(model.getAccountSuccessVolume().longValue());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(model.getAccountFailureValue());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(model.getAccountFailureVolume().longValue());

//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(date);
                DateModel dateModel = new DateModel();
                dateModel.setTime(date.getTime());

                dataset.setValue(model.getCardSuccessValue(), "Card Successful", dateModel);
                dataset.setValue(model.getCardFailureValue(), "Card Failed", dateModel);

                RegularTimePeriod period = new Day(date);
                successSeries.add(period, model.getCardSuccessValue());
                failedSeries.add(period, model.getCardFailureValue());

//                period = new Day(date);
                successVolSeries.add(period, model.getCardSuccessVolume());
                failedVolSeries.add(period, model.getCardFailureVolume());

//                dataset.
                datasetVolume.setValue(model.getCardSuccessVolume().longValue(), "Card Successful", dateModel);
                datasetVolume.setValue(model.getCardFailureVolume().longValue(), "Card Failed", dateModel);

            }

//            JFreeChart jchart = ChartFactory.createBarChart3D(
//            data + " Transaction Value Chart", "Date", "Value",
//                dataset, PlotOrientation.VERTICAL, true, true, false);
//            jchart.setBackgroundPaint(Color.white);
//            jchart.getTitle().setPaint(Color.BLACK);
//            CategoryPlot plot = jchart.getCategoryPlot();
//            BarRenderer br = (BarRenderer) plot.getRenderer();
//            br.setSeriesPaint(0, new Color(6, 86, 6));
//            br.setSeriesPaint(1, new Color(149, 37, 9));
////            br.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(IntervalCategoryToolTipGenerator.DEFAULT_TOOL_TIP_FORMAT_STRING, new SimpleDateFormat("dd-MM")));
//            br.setItemMargin(0);
//            DateAxis dateAxis = new DateAxis();
//            dateAxis.setVerticalTickLabels(true);
//            //dateAxis.setTickUnit(new DateTickUnit(DateTickUnitType.HOUR, 1));
////            dateAxis.setDateFormatOverride(new SimpleDateFormat("dd-MM"));
//            CategoryAxis domain = plot.getDomainAxis();
//            
////            plot.setRangeAxis(dateAxis);
//            domain.setLowerMargin(0.25);
//            domain.setUpperMargin(0.25);
            TimeSeriesCollection seriesCollection = new TimeSeriesCollection();
            seriesCollection.addSeries(successSeries);
            seriesCollection.addSeries(failedSeries);

            JFreeChart freeChart = createChart(seriesCollection, data + " Transaction Value");

            ByteArrayOutputStream chart_out = new ByteArrayOutputStream();
            ChartUtilities.writeChartAsJPEG(chart_out, freeChart, 750, 700);
//             
            int my_picture_id = worksheet.getWorkbook().addPicture(chart_out.toByteArray(), Workbook.PICTURE_TYPE_JPEG);
            /* Close the output stream */
            chart_out.close();
            /* Create the jdrawing container */
            XSSFDrawing jdrawing = worksheet.createDrawingPatriarch();
            /* Create an anchor point */
            ClientAnchor my_anchor = new XSSFClientAnchor();
            /* Define top left corner, and we can resize picture suitable from there */
            my_anchor.setCol1(6);
            my_anchor.setRow1(start + i + j);
            /* Invoke createPicture and pass the anchor point and ID */
            XSSFPicture my_picture = jdrawing.createPicture(my_anchor, my_picture_id);
            /* Call resize method, which resizes the image */
            my_picture.resize();

//                jchart = ChartFactory.createBarChart3D(
//            data + " Transaction Volume Chart", "Date", "Value",
//                datasetVolume, PlotOrientation.VERTICAL, true, true, false);
//            jchart.setBackgroundPaint(Color.white);
//            jchart.getTitle().setPaint(Color.BLACK);
//            plot = jchart.getCategoryPlot();
//            plot.setRangeAxis(dateAxis);
////            plot.setDomainAxis(dateAxis);
//            plot.getRangeAxis().setInverted(true);
//            br = (BarRenderer) plot.getRenderer();
//            br.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(IntervalCategoryToolTipGenerator.DEFAULT_TOOL_TIP_FORMAT_STRING, new SimpleDateFormat("dd-MM")));
//            br.setItemMargin(0);
////            br.setSeriesPaint(0, new Color(6, 86, 6));
////            br.setSeriesPaint(1, new Color(149, 37, 9));
//            domain = plot.getDomainAxis();
//            domain.setLowerMargin(0.25);
//            domain.setUpperMargin(0.25);
            chart_out = new ByteArrayOutputStream();

//            XYDataset xYDataset = createDataset();
            seriesCollection = new TimeSeriesCollection();
            seriesCollection.addSeries(successVolSeries);
            seriesCollection.addSeries(failedVolSeries);

            freeChart = createChart(seriesCollection, data + " Transaction Volume");

//        return chart;
//            JFreeChart freeChart = createChart(seriesCollection);
            ChartUtilities.writeChartAsJPEG(chart_out, freeChart, 750, 700);

            my_picture_id = worksheet.getWorkbook().addPicture(chart_out.toByteArray(), Workbook.PICTURE_TYPE_JPEG);
            /* Close the output stream */
            chart_out.close();
            /* Create the jdrawing container */
            jdrawing = worksheet.createDrawingPatriarch();
            /* Create an anchor point */
            my_anchor = new XSSFClientAnchor();
            /* Define top left corner, and we can resize picture suitable from there */
            my_anchor.setCol1(6);
            my_anchor.setRow1(start + i + j + 4);
            /* Invoke createPicture and pass the anchor point and ID */
            my_picture = jdrawing.createPicture(my_anchor, my_picture_id);
            /* Call resize method, which resizes the image */
            my_picture.resize();

            // end of jdrawing picture
            XSSFDrawing drawing = worksheet.createDrawingPatriarch();
            ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 1, start + i + j, 5, start + 20 + j);

            j += 4;

            XSSFChart chart = drawing.createChart(anchor);
            XSSFChartLegend legend = chart.getOrCreateLegend();
            legend.setPosition(LegendPosition.TOP_RIGHT);

            XSSFChartAxis bottomAxis = chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM);
            ValueAxis leftAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
            leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

            XSSFLineChartData chartData = chart.getChartDataFactory().createLineChartData();

            //        CellRangeAddress address = new CellRangeAddress(0,0,0)
            ChartDataSource<Number> xs = DataSources.fromNumericCellRange(worksheet, new CellRangeAddress(rowStart, currentRow, 0, 0));
            ChartDataSource<Number> ys1 = DataSources.fromNumericCellRange(worksheet, new CellRangeAddress(rowStart, currentRow, 1, 1));
            ChartDataSource<Number> ys2 = DataSources.fromNumericCellRange(worksheet, new CellRangeAddress(rowStart, currentRow, 3, 3));

            LineChartSeries series = chartData.addSeries(xs, ys1);
//            series.
            series.setTitle(data + " Successful Transaction");

            LineChartSeries series2 = chartData.addSeries(xs, ys2);
            series2.setTitle(data + " Failed Transaction");

            chart.plot(chartData, bottomAxis, leftAxis);
//            chart.setTitle(data + " Transaction Value Trend");

            drawChart(worksheet, rowStart, currentRow, data, 2, 4, i + j, j, start);

            j += 4;

            worksheet.createRow(currentRow++);

        }
    }

    private static void drawChart(XSSFSheet worksheet, int rowStart, int currentRow, String currency, int col1, int col2, int index, int index2, int start) {

        XSSFDrawing drawing = worksheet.createDrawingPatriarch();
        ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 1, start + index, 5, 20 + start + index2);

        XSSFChart chart = drawing.createChart(anchor);
        XSSFChartLegend legend = chart.getOrCreateLegend();
        legend.setPosition(LegendPosition.TOP_RIGHT);

        XSSFChartAxis bottomAxis = chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM);
        ValueAxis leftAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

        XSSFLineChartData chartData = chart.getChartDataFactory().createLineChartData();

        ChartDataSource<Number> xs = DataSources.fromNumericCellRange(worksheet, new CellRangeAddress(rowStart, currentRow, 0, 0));
        ChartDataSource<Number> vs1 = DataSources.fromNumericCellRange(worksheet, new CellRangeAddress(rowStart, currentRow, col1, col1));
        ChartDataSource<Number> vs2 = DataSources.fromNumericCellRange(worksheet, new CellRangeAddress(rowStart, currentRow, col2, col2));

        LineChartSeries series = chartData.addSeries(xs, vs1);
//            series.
        series.setTitle(currency + " Successful Transaction");

        LineChartSeries series2 = chartData.addSeries(xs, vs2);
        series2.setTitle(currency + " Failed Transaction");

        chart.plot(chartData, bottomAxis, leftAxis);
//        chart.setTitle(currency + " Transaction Volume Trend");
    }

    public static void buildCoreReportFailure(XSSFSheet worksheet, int startRowIndex, int startColIndex, Map<Date, Map<String, Map<String, List<FailureAnalysisModel>>>> datasource) {

        Font font = worksheet.getWorkbook().createFont();
        font.setFontHeightInPoints((short) 10);
        font.setBold(true);

        XSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
//        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);
        bodyCellStyle.setFont(font);

        int currentRow = 3;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM");
        for (Date data : datasource.keySet()) {

            XSSFRow row = null;

            int counter = 0;

            row = worksheet.createRow(currentRow++);

            XSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellStyle(bodyCellStyle);
            cell.setCellValue(Utility.formatDate(data, "yyyy-MM-dd"));

            Map<String, Map<String, List<FailureAnalysisModel>>> rowD = new TreeMap<>(datasource.getOrDefault(data, new TreeMap()));

            for (String currency : rowD.keySet()) {

                worksheet.createRow(currentRow++);

                row = worksheet.createRow(currentRow++);

                counter = 0;

                cell = row.createCell(startColIndex + counter++);

                cell.setCellStyle(bodyCellStyle);
                cell.setCellValue(currency);

                Map<String, List<FailureAnalysisModel>> model = (Map<String, List<FailureAnalysisModel>>) rowD.getOrDefault(currency, new HashMap<>());

                for (String cardScheme : model.keySet()) {

                    row = worksheet.createRow(currentRow++);

                    counter = 1;

                    cell = row.createCell(startColIndex + counter++);

                    cell.setCellStyle(bodyCellStyle);
                    cell.setCellValue(cardScheme);

                    List<FailureAnalysisModel> dataList = (List<FailureAnalysisModel>) model.getOrDefault(cardScheme, new ArrayList<>());

                    for (FailureAnalysisModel analysisModel : dataList) {

                        cell = row.createCell(startColIndex + counter++);
                        cell.setCellValue(analysisModel.getValue());

                        cell = row.createCell(startColIndex + counter++);
                        cell.setCellValue(analysisModel.getVolume().longValue());

                        //Account section
                        cell = row.createCell(startColIndex + counter++);
                        cell.setCellValue(analysisModel.getReason());

                        row = worksheet.createRow(currentRow++);
                        counter = 2;
                    }

                }

            }

        }

    }

    public static void buildCoreReportSuccess(XSSFSheet worksheet, int startRowIndex, int startColIndex, Map<Date, Map<String, Map<String, FailureAnalysisModel>>> datasource) {

        Font font = worksheet.getWorkbook().createFont();
        font.setFontHeightInPoints((short) 10);
        font.setBold(true);

        XSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
//        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);
        bodyCellStyle.setFont(font);

        int currentRow = 3;

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM");
        for (Date data : datasource.keySet()) {

            XSSFRow row = null;

            int counter = 0;

            row = worksheet.createRow(currentRow++);

            XSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellStyle(bodyCellStyle);
            cell.setCellValue(Utility.formatDate(data, "yyyy-MM-dd"));

            Map<String, Map<String, FailureAnalysisModel>> rowD = new TreeMap<>(datasource.getOrDefault(data, new TreeMap()));

            for (String currency : rowD.keySet()) {

//                worksheet.createRow(currentRow++);
                row = worksheet.createRow(currentRow++);

                counter = 0;

                cell = row.createCell(startColIndex + counter++);

                cell.setCellStyle(bodyCellStyle);
                cell.setCellValue(currency);

                Map<String, FailureAnalysisModel> model = (Map<String, FailureAnalysisModel>) rowD.getOrDefault(currency, new HashMap<>());

                for (String cardString : model.keySet()) {

                    worksheet.createRow(currentRow++);

                    row = worksheet.createRow(currentRow++);

                    counter = 1;

//                    cell = row.createCell(startColIndex+counter++);  
//
//                    cell.setCellStyle(bodyCellStyle);
//                    cell.setCellValue(currency);
                    FailureAnalysisModel models = (FailureAnalysisModel) model.getOrDefault(cardString, new FailureAnalysisModel());

                    //                row = worksheet.createRow(currentRow++);
                    cell = row.createCell(startColIndex + counter++);
                    cell.setCellValue(models.getCardScheme());

                    cell = row.createCell(startColIndex + counter++);
                    cell.setCellValue(models.getValue());

                    cell = row.createCell(startColIndex + counter++);
                    cell.setCellValue(models.getVolume().longValue());

                }

            }

        }

    }

    public static void coreReportSummary(HSSFSheet worksheet, int startRowIndex, int startColIndex, Map<String, Map> datasource, Map<String, Map> nineDaySource) {

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
//        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int currentRow = 3;

        for (String obj : datasource.keySet()) {

            Map<String, Object> rowData = datasource.getOrDefault(obj, new HashMap<>());

            int count = 0;

            HSSFRow row = null;

            int counter = 0;

            row = worksheet.createRow(currentRow++);

            HSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(obj);

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(String.valueOf(rowData.getOrDefault("volume", 0)));

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(String.valueOf(rowData.getOrDefault("value", 0.0)));

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(Utility.convertTo2DP(rowData.getOrDefault("revenue", 0.0) + ""));
            cell = row.createCell(startColIndex + counter++);
//            cell.setCellValue(String.valueOf(rowData.getOrDefault("volumegrowth", 0.0)));
            cell.setCellStyle(getCellStyle(rowData.getOrDefault("volumegrowth", 0) + "", worksheet));
            cell.setCellValue(Utility.convertTo2DP(rowData.getOrDefault("volumegrowth", 0.0) + ""));

            cell = row.createCell(startColIndex + counter++);
//            cell.setCellValue(String.valueOf(rowData.getOrDefault("valuegrowth", 0.0)));
            cell.setCellValue(Utility.convertTo2DP(rowData.getOrDefault("valuegrowth", 0.0) + ""));
            cell = row.createCell(startColIndex + counter++);
//            cell.setCellValue(String.valueOf(rowData.getOrDefault("revenuegrowth", 0)));
            cell.setCellStyle(getCellStyle(rowData.getOrDefault("revenuegrowth", 0) + "", worksheet));
            cell.setCellValue(Utility.convertTo2DP(rowData.getOrDefault("revenuegrowth", 0.0) + ""));

        }

        // create header for nine days
        HSSFRow row;

        int counter = 0;

        worksheet.createRow(currentRow++);

        row = worksheet.createRow(currentRow++);

        Font font = worksheet.getWorkbook().createFont();
        font.setBold(true);

        // Create cell style for the headers
        HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
        headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerCellStyle.setWrapText(true);
        headerCellStyle.setFont(font);
        headerCellStyle.setBorderBottom(BorderStyle.THIN);

        HSSFCell cell = row.createCell(startColIndex + counter++);
        cell.setCellStyle(headerCellStyle);
        cell.setCellValue("Month to date (1-9th)".toUpperCase());

        cell = row.createCell(startColIndex + counter++);
        cell.setCellStyle(headerCellStyle);
        cell.setCellValue("Volume".toUpperCase());

        cell = row.createCell(startColIndex + counter++);
        cell.setCellStyle(headerCellStyle);
        cell.setCellValue("Value".toUpperCase());

        cell = row.createCell(startColIndex + counter++);
        cell.setCellStyle(headerCellStyle);
        cell.setCellValue("Revenue".toUpperCase());

        cell = row.createCell(startColIndex + counter++);
        cell.setCellStyle(headerCellStyle);
        cell.setCellValue("%Growth in Volume".toUpperCase());

        cell = row.createCell(startColIndex + counter++);
        cell.setCellStyle(headerCellStyle);
        cell.setCellValue("%Growth in Value".toUpperCase());

        cell = row.createCell(startColIndex + counter++);
        cell.setCellStyle(headerCellStyle);
        cell.setCellValue("%Growth in Revenue".toUpperCase());

        for (String obj : nineDaySource.keySet()) {

            Map<String, Object> rowData = nineDaySource.getOrDefault(obj, new HashMap<>());

            counter = 0;

            row = worksheet.createRow(currentRow++);

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(obj);

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(String.valueOf(rowData.getOrDefault("volume", 0)));

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(String.valueOf(rowData.getOrDefault("value", 0.0)));

            cell = row.createCell(startColIndex + counter++);
            cell.setCellStyle(getCellStyle(rowData.getOrDefault("revenue", 0) + "", worksheet));
            cell.setCellValue(String.valueOf(rowData.getOrDefault("revenue", 0.0)));

            cell = row.createCell(startColIndex + counter++);

//            cell.setCellValue(String.valueOf(rowData.getOrDefault("volumegrowth", 0.0)));
            cell.setCellStyle(getCellStyle(rowData.getOrDefault("volumegrowth", 0) + "", worksheet));
            cell.setCellValue(String.valueOf(rowData.getOrDefault("volumegrowth", 0.0)));
            cell = row.createCell(startColIndex + counter++);
//            cell.setCellValue(String.valueOf(rowData.getOrDefault("valuegrowth", 0.0)));
            cell.setCellStyle(getCellStyle(rowData.getOrDefault("valuegrowth", 0) + "", worksheet));
            cell.setCellValue(String.valueOf(rowData.getOrDefault("valuegrowth", 0.0)));

            cell = row.createCell(startColIndex + counter++);
            cell.setCellStyle(getCellStyle(rowData.getOrDefault("revenuegrowth", 0) + "", worksheet));
            cell.setCellValue(String.valueOf(rowData.getOrDefault("revenuegrowth", 0)));

        }

    }

    public static void build(HSSFSheet worksheet, int startRowIndex, int startColIndex, Map<String, Double> datasource) {

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int currentRow = 3;

        for (Map.Entry obj : datasource.entrySet()) {

            HSSFRow row = null;

            int counter = 0;

            row = worksheet.createRow(currentRow++);

            HSSFCell cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(obj.getKey() + "");

            cell = row.createCell(startColIndex + counter++);
            cell.setCellValue(Utility.convertTo2DP(obj.getValue()+""));

        }

    }

    public static void build(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<SettlementViewModel> datasource) {

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int currentRow = 3;

        if (datasource != null) {
            for (SettlementViewModel obj : datasource) {

                HSSFRow row = null;

                int counter = 0;

                row = worksheet.createRow(currentRow++);

                HSSFCell cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getProvider() + "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getMerchantId() + "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getMerchantName() + "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(obj.getAmount()));
                
                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getVolume());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(obj.getAmountToSettled()));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(obj.getFee()));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getCost() + "");

            }
        }

    }

    public static void buildRave(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<RaveSettlementViewModel> datasource) {

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int currentRow = 3;

        if (datasource != null) {
            for (RaveSettlementViewModel obj : datasource) {

                HSSFRow row = null;

                int counter = 0;

                row = worksheet.createRow(currentRow++);

                HSSFCell cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getProvider() + "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getMerchantId() + "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getMerchantName() + "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getAmount());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getAmountToSettled());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getVolume());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(obj.getFee()));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getAccountNo() + "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getBankCode() + "");

            }
        }

    }
    
    public static void buildRaveSettlement(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<RaveSettlementModel> datasource) {

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int currentRow = 3;

        if (datasource != null) {
            for (RaveSettlementModel obj : datasource) {

                HSSFRow row = null;

                int counter = 0;

                row = worksheet.createRow(currentRow++);
        
                HSSFCell cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getBusinessName()+ "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getParent()+ "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(obj.getAmount()));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(obj.getAppFee()));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(obj.getMerchantFee()));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getVolume());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getCurrency());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getAccountNumber());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getBankCode());
                
                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getParentSettlementType());

            }
        }

    }

    public static void buildPosSettlement(HSSFSheet worksheet, int startRowIndex, int startColIndex, List<PosSettlementModel> datasource) {

        HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
        bodyCellStyle.setAlignment(HorizontalAlignment.CENTER);
        bodyCellStyle.setWrapText(true);

        int currentRow = 3;

        if (datasource != null) {
            for (PosSettlementModel obj : datasource) {

                HSSFRow row = null;

                int counter = 0;

                row = worksheet.createRow(currentRow++);
        
                HSSFCell cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getName()+ "");

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getTerminalId());
                
                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(obj.getAmount()));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(Utility.convertTo2DP(obj.getFee()));

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getCurrency());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getType());

                cell = row.createCell(startColIndex + counter++);
                cell.setCellValue(obj.getTransactionDate());

            }
        }

    }
    
    private static CellStyle getCellStyle(String value, HSSFSheet worksheet) {

        HSSFCellStyle cellStyle = worksheet.getWorkbook().createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.LEFT);
        cellStyle.setWrapText(true);

        double valueDouble = Double.parseDouble(value);

        if (valueDouble < 0.0) {
            Font font = worksheet.getWorkbook().createFont();
//            font.setBold(true)
            font.setColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
            cellStyle.setFont(font);
        }

        return cellStyle;
    }

    private static IntervalXYDataset createDataset() {

        TimeSeries s1 = new TimeSeries("L&G European Index Trust");
        s1.add(new Month(2, 2001), 181.8);
        s1.add(new Month(3, 2001), 167.3);
        s1.add(new Month(4, 2001), 153.8);
        s1.add(new Month(5, 2001), 167.6);
        s1.add(new Month(6, 2001), 158.8);
        s1.add(new Month(7, 2001), 148.3);
        s1.add(new Month(8, 2001), 153.9);
        s1.add(new Month(9, 2001), 142.7);
        s1.add(new Month(10, 2001), 123.2);
        s1.add(new Month(11, 2001), 131.8);
        s1.add(new Month(12, 2001), 139.6);
        s1.add(new Month(1, 2002), 142.9);
        s1.add(new Month(2, 2002), 138.7);
        s1.add(new Month(3, 2002), 137.3);
        s1.add(new Month(4, 2002), 143.9);
        s1.add(new Month(5, 2002), 139.8);
        s1.add(new Month(6, 2002), 137.0);
        s1.add(new Month(7, 2002), 132.8);

        TimeSeries s2 = new TimeSeries("L&G UK Index Trust");
        s2.add(new Month(2, 2001), 129.6);
        s2.add(new Month(3, 2001), 123.2);
        s2.add(new Month(4, 2001), 117.2);
        s2.add(new Month(5, 2001), 124.1);
        s2.add(new Month(6, 2001), 122.6);
        s2.add(new Month(7, 2001), 119.2);
        s2.add(new Month(8, 2001), 116.5);
        s2.add(new Month(9, 2001), 112.7);
        s2.add(new Month(10, 2001), 101.5);
        s2.add(new Month(11, 2001), 106.1);
        s2.add(new Month(12, 2001), 110.3);
        s2.add(new Month(1, 2002), 111.7);
        s2.add(new Month(2, 2002), 111.0);
        s2.add(new Month(3, 2002), 109.6);
        s2.add(new Month(4, 2002), 113.2);
        s2.add(new Month(5, 2002), 111.6);
        s2.add(new Month(6, 2002), 108.8);
        s2.add(new Month(7, 2002), 101.6);

        // ******************************************************************
        //  More than 150 demo applications are included with the JFreeChart
        //  Developer Guide...for more information, see:
        //
        //  >   http://www.object-refinery.com/jfreechart/guide.html
        //
        // ******************************************************************
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.addSeries(s1);
        dataset.addSeries(s2);

        return dataset;

    }

    private static JFreeChart createChart(IntervalXYDataset dataset) {
        JFreeChart chart = ChartFactory.createXYBarChart(
                "State Executions - USA",
                "Year",
                true,
                "Number of People",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                false,
                false
        );

        // then customise it a little...
        chart.addSubtitle(new TextTitle(
                "Source: http://www.amnestyusa.org/abolish/listbyyear.do",
                new java.awt.Font("Dialog", Font.COLOR_NORMAL, 10)));

//        XYPlot plot = (XYPlot) chart.getPlot();
//        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
//        StandardXYToolTipGenerator generator = new StandardXYToolTipGenerator(
//            "{1} = {2}", new SimpleDateFormat("MM-yyyy"), new DecimalFormat("0"));
//        renderer.setBaseToolTipGenerator(generator);
//        renderer.setMargin(0.10);
//
//        DateAxis axis = (DateAxis) plot.getDomainAxis();
//        axis.setTickMarkPosition(DateTickMarkPosition.END);
//        axis.setLowerMargin(0.01);
//        axis.setUpperMargin(0.01);
//
//        ChartUtilities.applyCurrentTheme(chart);
        chart.setBackgroundPaint(Color.white);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(true);
            renderer.setBaseShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
        }

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        return chart;

//        return chart;
    }

    private static JFreeChart createChart(XYDataset dataset) {

        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                "Legal & General Unit Trust Prices", // title
                "Date", // x-axis label
                "Price Per Unit", // y-axis label
                dataset, // data
                true, // create legend?
                true, // generate tooltips?
                false // generate URLs?
        );

        chart.setBackgroundPaint(Color.white);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(true);
            renderer.setBaseShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
        }

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        return chart;

    }

    public static JFreeChart createChart(XYDataset dataset, String title) {

        JFreeChart freeChart = ChartFactory.createTimeSeriesChart(
                title, // title
                "Date", // x-axis label
                "Value", // y-axis label
                dataset, // data
                true, // create legend?
                true, // generate tooltips?
                false // generate URLs?
        );

        freeChart.setBackgroundPaint(Color.white);

        XYPlot plot = (XYPlot) freeChart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(true);
            renderer.setBaseShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
            renderer.setSeriesPaint(0, new Color(6, 86, 6));
            renderer.setSeriesPaint(1, new Color(149, 37, 9));
        }

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("dd-MM-yyyy"));

//            domain = plot.getDomainAxis();
        axis.setLowerMargin(0.1);
        axis.setUpperMargin(0.1);

        return freeChart;
    }
}

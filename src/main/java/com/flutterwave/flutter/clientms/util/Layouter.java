/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import java.util.Date;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.CellRangeAddress8Bit;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * @author mayowa.olurin
 * @edited by adeyemi emmanuel on 27/11/2014
 */
public class Layouter {

    //private static Logger logger = Logger.getLogger("layouter");
    /**
     * This doesn't have any data yet. This is the template.
     * @param worksheet
     * @param startRowIndex
     * @param startColIndex
     * @param headers
     * @param title
     * @param totalColumn
     * @return 
     */
    public static int buildReport(HSSFSheet worksheet, int startRowIndex, int startColIndex, List headers, String title, int totalColumn) {
        // Set column widths
        for (int i = 0; i < headers.size(); i++) {

            worksheet.setColumnWidth(i, 5000);
        }

        // Build the title and date headers
        buildTitle(worksheet, startRowIndex, startColIndex, title, totalColumn);
        // Build the column headers
        buildHeaders(worksheet, startRowIndex, startColIndex, headers);

        return 3;
    }

    /**
     * the report title and the date header
     *
     * @param worksheet
     * @param startRowIndex starting row offset
     * @param startColIndex starting column offset
     * @param title
     * @param totalColumn
     */
    public static void buildTitle(HSSFSheet worksheet, int startRowIndex, int startColIndex, String title, int totalColumn) {
        // Create font style for the report title
        Font fontTitle = worksheet.getWorkbook().createFont();
        fontTitle.setBold(true);
        fontTitle.setFontHeight((short) 280);

        // Create cell style for the report title
        HSSFCellStyle cellStyleTitle = worksheet.getWorkbook().createCellStyle();
        cellStyleTitle.setAlignment(HorizontalAlignment.CENTER);
        cellStyleTitle.setWrapText(true);
        cellStyleTitle.setFont(fontTitle);

        // Create report title
        HSSFRow rowTitle = worksheet.createRow((short) startRowIndex);
        rowTitle.setHeight((short) 500);
        HSSFCell cellTitle = rowTitle.createCell(startColIndex);
        cellTitle.setCellValue(title);
        cellTitle.setCellStyle(cellStyleTitle);

        // Create merged region for the report title
        worksheet.addMergedRegion(new CellRangeAddress(0, 0, 0,totalColumn ));

        // Create date header
        HSSFRow dateTitle = worksheet.createRow((short) startRowIndex + 1);
        HSSFCell cellDate = dateTitle.createCell(startColIndex);
        cellDate.setCellValue("This report was generated at " + new Date());
    }

    public static void buildHeaders(HSSFSheet worksheet, int startRowIndex, int startColIndex, List headers) {
        // Create font style for the headers
        Font font = worksheet.getWorkbook().createFont();
        font.setBold(true);

        // Create cell style for the headers
        HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
        headerCellStyle.setFillBackgroundColor((short)25);
        headerCellStyle.setFillPattern(FillPatternType.LEAST_DOTS);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headerCellStyle.setWrapText(true);
        headerCellStyle.setFont(font);
        headerCellStyle.setBorderBottom(BorderStyle.THIN);

        // Create the column headers
        HSSFRow rowHeader = worksheet.createRow((short) startRowIndex + 2);
        rowHeader.setHeight((short) 500);

        for (int i = 0; i < headers.size(); i++) {

            HSSFCell cell1 = rowHeader.createCell(startColIndex + i);
            cell1.setCellValue((String) headers.get(i));
            cell1.setCellStyle(headerCellStyle);

        }
    }


}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.clientms.model.recon.BatchResolved;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Future;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author emmanueladeyemi
 */
@SessionScoped
public class Global implements Serializable {

    /**
     * @return the downloadCheck
     */
    public Future<String> getDownloadCheck() {
        return downloadCheck;
    }

    /**
     * @param downloadCheck the downloadCheck to set
     */
    public void setDownloadCheck(Future<String> downloadCheck) {
        this.downloadCheck = downloadCheck;
    }

    /**
     * @return the posSettlementReport
     */
    public List getPosSettlementReport() {
        return posSettlementReport;
    }

    /**
     * @param posSettlementReport the posSettlementReport to set
     */
    public void setPosSettlementReport(List posSettlementReport) {
        this.posSettlementReport = posSettlementReport;
    }

    /**
     * @return the raveTransactionSettlement
     */
    public List getRaveTransactionSettlement() {
        
        if(raveTransactionSettlement == null)
            raveTransactionSettlement = new ArrayList<>();
        return raveTransactionSettlement;
    }

    /**
     * @param raveTransactionSettlement the raveTransactionSettlement to set
     */
    public void setRaveTransactionSettlement(List raveTransactionSettlement) {
        this.raveTransactionSettlement = raveTransactionSettlement;
    }

    /**
     * @return the settlementReport
     */
    public List getSettlementReport() {
        return settlementReport;
    }

    /**
     * @param settlementReport the settlementReport to set
     */
    public void setSettlementReport(List settlementReport) {
        this.settlementReport = settlementReport;
    }

    /**
     * @return the settlementReport
     */
    public List getRaveSettlementReport() {
        return raveSettlementReport;
    }

    /**
     * @param settlementReport the settlementReport to set
     */
    public void setRaveSettlementReport(List settlementReport) {
        this.raveSettlementReport = settlementReport;
    }
    
    /**
     * @return the moneywaveReportBean
     */
    public MoneywaveReportBean getMoneywaveReportBean() {
        return moneywaveReportBean;
    }

    /**
     * @param moneywaveReportBean the moneywaveReportBean to set
     */
    public void setMoneywaveReportBean(MoneywaveReportBean moneywaveReportBean) {
        this.moneywaveReportBean = moneywaveReportBean;
    }

    /**
     * @return the domainId
     */
    public String getDomainId() {
        return domainId;
    }

    /**
     * @param domainId the domainId to set
     */
    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
    
    public static final String NOTIFICATION_ENDPOINT = "https://flutterwaveprod.com:9443/FlutterwaveEmail/services/EmailService/SendEmail";
    
//    //staging
//    public static final String REDISBURSEMENT_ENDPOINT = "https://flutterwavestaging.com:9443/flwmvva/services/disburseprocessor/pay/retry";
    //live
    public static final String REDISBURSEMENT_ENDPOINT = "https://flutterwaveprod.com:9443/flwmvva/services/disburseprocessor/pay/retry";
    public static final String REDISBURSEMENT_ENDPOINT_NEW = "https://coreflutterwaveprod.com/flwmvva/services/disburseprocessor/pay";
    
    public static final String PERSISTENT_NAME = "FlutterClientMSPU";
    public static final String PERSISTENT_NAME_NEW = "FlutterClientNewMSPU";
    public static final String RAVE_PERSISTENT_NAME = "RaveMSPU";
    public static final String MONEYWAVE_PERSISTENT_NAME = "MoneywaveMSPU";
    public static final String CORE_PERSISTENT_NAME = "CoreDSPU";
    
    public static final String MERCHANT_ID = "test"; // test
    public static final String TOKEN = "f71e247c6a7498c8c17ea0e8167c5584dbe5b7b95fc26f6c25035a6f40a584e4cba182a2d81a250873e1eb9bd586397e767c5d303960d298518aec7703ec349d";
    public static final String URL_LIMIT = "https://flutterwavestaging.com:9443/flwreportal/services/reports/merchants/limit";
    public static final String PRIVATE_KEY = "1123459846fedcbb"; // This is test credentials
    
    //live
    public static final String MERCHANT_ID_LIVE = "acctcharge"; // live
    public static final String TOKEN_LIVE = "e2310c314984d494f26d4e28004aa98c0e693223720a67349ad247777df7a9ccdefcee568fc18301f03de2fb30528a4c8718ade6602077300e29ae7881e74ed4"; //live
    public static final String URL_LIMIT_LIVE = "http://localhost:9090/flwreportal/services/reports/merchants/limit"; //live
    public static final String PRIVATE_KEY_LIVE = "k4lwLPJnGjsJO4qK/qinCP5DgyovHAHf"; // This is live credentials
            
    //core -angus
    public static final String QUERY_MERCHANT_URL = "https://www.flutterwavedev.com/requests/merchants/list/";
    public static final String QUERY_MERCHANT_URL_KEY = "7b78acbb-4318-4dfe-8339-760e83dc4dcd";
    
    //core-joe - staging
//    public static final String BLOCK_MERCHANT_URL = "http://staging1flutterwave.co:8080/pwc/rest/fw/block/";
//    public static final String UNBLOCK_MERCHANT_URL = "http://staging1flutterwave.co:8080/pwc/rest/fw/unblock/";
//    public static final String REFUND_URL = "http://staging1flutterwave.co:8080/pwc/rest/fw/refund/";
//    
//     core-joe live
    public static final String BLOCK_MERCHANT_URL = "http://prod1flutterwave.co:8080/pwc/rest/fw/block/";
    public static final String UNBLOCK_MERCHANT_URL = "http://prod1flutterwave.co:8080/pwc/rest/fw/unblock/";
    public static final String REFUND_URL = "http://prod1flutterwave.co:8080/pwc/rest/fw/refund/";
//    
    public static final String TOKEN_STRING = "live_token"; //  live_token for live test_token for test and
    public static final String TOKEN_STRING_C = "liveToken"; // liveToken for live testToken for test
//    
    private List settlementReport, raveSettlementReport, raveTransactionSettlement;
    private List posSettlementReport;
    
    private String domainId;
    
    private Future<BatchResolved> future;
    
    public Future<BatchResolved> getFuture(){
        return future;
    } 
    
    public void setFuture(Future<BatchResolved> future){
        this.future = future;
    }
    
    private MoneywaveReportBean moneywaveReportBean;
    
    public static int BASE_LIMIT = 10;
    
    // This is for PWC
    //public static String PWC_HOST_URL = "https://lion.paywcapp.com";
    private final static String PWC_HOST_URL = "https://pwcstaging.herokuapp.com";
    
    public static String PWC_PRFORMANCE = PWC_HOST_URL+"/transactions/metrics";
    
    public static boolean mailSend = false;
    
    final static Properties properties = Utility.getConfigProperty();

    public final static String EMAIL_USERNAME = properties.getProperty("email_username"); // "Flutterwave";
    public final static String EMAIL_PASSWORD = properties.getProperty("email_password");
    public final static String EMAIL_SERVER = properties.getProperty("email_host");// "smtp.mandrillapp.com";
    public final static int  EMAIL_PORT = Integer.parseInt(properties.getProperty("email_port"));
    public final static boolean USE_STARTTLS = true;
    
    public final static String ETOP = "https://tellerpoint.hextremelabs.net:38443/Tellerpoint/rpc/partner/receipts/";
    
    public static final String CONFIG_FILE_PATH 
//            = "/Users/emmanueladeyemi/Documents/flutterwave/configurations/config-cp.properties";
            = "/home/ubuntu/config-cp.properties";
    
    private Future<String> downloadCheck;
    
    //"/Users/emmanueladeyemi/Documents/flutterwave/configurations/config-cp.properties";
    // "/home/ubuntu/config-cp.properties";
    // This is for PWC LIVE
    //public static String PWC_PERFORMANCE_REPORT_URL_LIVE = "https://lion.paywcapp.com";
    
}

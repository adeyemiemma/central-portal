/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.clientms.model.recon.ReconTransaction;
import com.flutterwave.flutter.otp.OtpMacAlgorithm;
import com.flutterwave.flutter.otp.totp.Totp;
import com.flutterwave.flutter.otp.totp.TotpTimeMode;
import com.flutterwave.flutter.otp.totp.TotpToken;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author emmanueladeyemi
 */
public class Utility {

    public final static long POS_MERCHANT_COUNTER = 80000;
    
    public static String generateAccessToken(String uniqueId) {
        String token = "";

        try {

            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update((uniqueId + "" + (new Date()).toString()).getBytes("UTF-8"));

            byte[] bytes = digest.digest();

            java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();

            token = encoder.encodeToString(bytes);

        } catch (NoSuchAlgorithmException nsae) {

        } catch (UnsupportedEncodingException ex) {
            java.util.logging.Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return token;
    }
    
    public static boolean expired(Date date, int earlierHour, ChronoUnit chronoUnit ){
        
        if(date == null )
            return true;
        
//        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern(format);
        
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).compareTo(LocalDateTime.now().plus(earlierHour, chronoUnit)) < 0;   
    }

    public static String sha512(String msg) {

        try {

            MessageDigest md = MessageDigest.getInstance("SHA-512");
            String response = toHexStr(md.digest(msg.getBytes()));

            System.out.println(response);

            return response;

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public static String toHexStr(byte[] bytes) {

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < bytes.length; i++) {
            builder.append(String.format("%02x", bytes[i]));
        }

        return builder.toString();
    }

    public enum FeeType {
        FLAT,
        PERCENTAGE,
        BOTH
    }

    public enum Location {
        LOCAL,
        INTERNATIONAL
    }

    public enum Category {
        Fee,
        Limit
    }

    public enum LimitCategory {
//        Country,
//        Currency,
        Merchant
    }

    public enum ReportSource {
        Database,
        Excel
    }

    public enum SettlementState {
        PENDING,
        APPROVED,
        NONE
    }

    public enum WalletCategory {
        SOURCE,
        MERCHANT
    }

    public enum FetchStatus {
        FAILED,
        SUCCESSFUL
    }

    public enum QueryStatus {
        FAILED,
        SUCCESSFUL
    }

    public enum ExchangeRateCategory {
        FLUTTERWAVE,
        CBN,
        BARTER
    }

    public static List<String> getClassFields(Class object, String... exclude) {

        List<String> list = new ArrayList<>();

        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).collect(Collectors.toList());
        }

        for (Field field : object.getDeclaredFields()) {

            field.setAccessible(true);
//            if(field.getType() == String.class){
            //                System.out.println(field.getName());

            try {

                if (exclusions != null) {

                    int index = exclusions.indexOf(field.getName());

                    if (index > -1) {
                        continue;
                    }
                }

                list.add(field.getName().toUpperCase());

                //                System.out.println(object.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

//        }
        return list;
    }
    
    public static List<String> getClassFields(Class object, List<String> extraFields, String... exclude) {

        List<String> list = new ArrayList<>();

        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).collect(Collectors.toList());
        }

        for (Field field : object.getDeclaredFields()) {

            field.setAccessible(true);

            try {

                if (exclusions != null) {

                    int index = exclusions.indexOf(field.getName());

                    if (index > -1) {
                        continue;
                    }
                }

                list.add(field.getName().toUpperCase());

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        if(extraFields != null && !extraFields.isEmpty()){
            
            list.addAll(extraFields);
        }

//        }
        return list;
    }
    
    public static List<String> getClassFields(Class object, Map<String, String> replacement, String... exclude) {

        List<String> list = new ArrayList<>();

        List<String> exclusions = null;

        if (exclude != null) {
            exclusions = Stream.of(exclude).collect(Collectors.toList());
        }

        for (Field field : object.getDeclaredFields()) {

            field.setAccessible(true);
//            if(field.getType() == String.class){
            //                System.out.println(field.getName());

            try {

                if (exclusions != null) {

                    int index = exclusions.indexOf(field.getName());

                    if (index > -1) {
                        continue;
                    }
                }

                if(replacement != null && !replacement.isEmpty()){
                 
                    String temp = replacement.getOrDefault(field.getName(), field.getName());
                    list.add(temp.toUpperCase());
                    
                }else                
                    list.add(field.getName().toUpperCase());

                //                System.out.println(object.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

//        }
        return list;
    }

    public static long getAge(ReconTransaction transaction) {

        long currentDateInMilliseconds = transaction.isSettled() == true ? transaction.getSettlementDate().getTime() : new Date().getTime();
        long createdDateInMillisecond = transaction.getOriginTime().getTime();

        long value = ((currentDateInMilliseconds - createdDateInMillisecond) / (1000 * 60 * 60 * 24));

        return value;
    }

    public static long getAge(Date date, Date settlementDate) {

        long currentDateInMilliseconds = settlementDate != null ? settlementDate.getTime() : new Date().getTime();
        long createdDateInMillisecond = date.getTime();

        long value = ((currentDateInMilliseconds - createdDateInMillisecond) / (1000 * 60 * 60 * 24));

        return value;
    }

    public static double getExchangeRateInNGN(String orig) {

        switch (orig.toUpperCase()) {

            case "NGN":
                return 1.0;
            case "GHS":
                return 108;
            case "EUR":
                return 480;
            case "USD":
                return 500;
            case "CAD":
                return 270;
            case "GBP":
                return 540;
            case "KES":
                return 3.06;
            default:
                return 1;
        }
    }

    public static String formatNumber(double value) {
        String formattedValue = NumberFormat.getNumberInstance(Locale.US).format(value);
        return formattedValue;
    }
    

    public static String formatNumber(String value) {
        String formattedValue = NumberFormat.getNumberInstance(Locale.US).format(Double.parseDouble(value));
        return formattedValue;
    }

    public static String formatDate(Date date, String format) {

        if(date == null || format == null)
            return null;
        
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        return dateFormat.format(date);
    }

    public static String getLast(String data, int size) {

        if (data == null) {
            return null;
        }

        if (data.length() <= size) {
            return data;
        }

        String selectedElements = data.substring(data.length() - size);

        return selectedElements;
    }

    public static String getData(JsonArray listData, String nameKey) {

        String data = null;
        
        if (listData != null) {

            for(JsonValue jsonValue : listData){
             
                JsonObject jsonObject = (JsonObject) jsonValue; 

               String name = jsonObject.getString("name", null);
               
               if(name.equalsIgnoreCase(nameKey)){
                   data = jsonObject.getString("value", "");
                   break;
               }
                
            }

        }
        
        return data;
    }
    
    public static int compare(Date original, Date comparer, boolean dateOnly){
        
        if(dateOnly == true){
            
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(original);
            
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            
            original = calendar.getTime();
            
            calendar.setTime(comparer);
            
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            
            comparer = calendar.getTime();
        }
        
        return original.compareTo(comparer);
    }
    
    public static long age(Date original, Date comparer, boolean dateOnly){
        
        if(dateOnly == true){
            
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(original);
            
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            
            original = calendar.getTime();
            
            calendar.setTime(comparer);
            
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            
            comparer = calendar.getTime();
        }
        
        long value = ((original.getTime() - comparer.getTime()) / (1000 * 60 * 60 * 24));

        return value;
    }
    
    public static double convertTo2DP(double amount) {

        if(amount == 0.0)
            return 0.0;
        
        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        String value = decimalFormat.format(amount);

        return Double.parseDouble(value);
    }
    
    public static double convertTo2DP(String amountString) {

        if(emptyToNull(amountString) == null){
            
            return 0.0;
        }
        
        double amount = Double.parseDouble(amountString);
        
        if(amount == 0.0)
            return 0.0;
        
        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        String value = decimalFormat.format(amount);

        return Double.parseDouble(value);
    }
    
    
    public  static String fixMoneywavePath(String path){
        
        if(path == null || "".equalsIgnoreCase(path))
            return null;
        
        String tempPath = path;
        
        path = path.toLowerCase();
        
        if(!path.startsWith("http")){
            tempPath = "https://s3-eu-west-1.amazonaws.com/moneywave-resources/"+tempPath;
        }
        
        return tempPath;
    }
    
    public static String generateUniqueCode(int length) {

        try {
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

            // generate a random number
            String randomString = Integer.toString(random.nextInt(new BigDecimal(Math.pow(10, length)).intValue()));

            System.out.println("Sample data " + randomString);
            if (randomString.length() < length) {

                for (int i = 0; i < length - randomString.length(); i++) {
                    randomString = "0" + randomString;
                }

            }

            return randomString;
        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return null;
    }
    
    public static Date parseDate(String date, String format){
        
        try {
            
            if(date == null || format == null)
                return null;
            
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            
            Date result = dateFormat.parse(date);
            
            return result;
        } catch (ParseException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    
    public static boolean isExpired(Date original){
        
        
        LocalDate dateOriginal = original.toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate();
        
        return dateOriginal.compareTo(LocalDate.now()) < 0;   
    }
    
    public static String generateOTP(int length){
        
        Totp totp = new Totp((new Date().getTime()+""+Utility.generateUniqueCode(4)).getBytes(), 
                OtpMacAlgorithm.HmacSHA256,length, TotpTimeMode.Second);
        
        TotpToken totpToken = totp.generateOtp();
        
        return totpToken.getOtp();
    }
    
    public static String nullToEmpty(String data){
        
        if(data == null)
            return "";
        
        return data;
    }
    
    public static String emptyToNull(String data){
        
        if(data == null || "".equals(data))
            return null;
        
        return data;
    }
    
    public static String generateReference(){
     
        String reference = (new Date().getTime())+""+generateUniqueCode(4);
        
        return reference;
    }
    
    public static Properties getConfigProperty(){
        
        Properties property = new Properties();
        
        InputStream inputStream = null;
        
        try{
            inputStream = new FileInputStream(Global.CONFIG_FILE_PATH);
            
            property.load(inputStream);
            
            return property;
            
        }catch(Exception ex){
            
            ex.printStackTrace();
        }finally{
            
            if(inputStream != null)
                try {
                    inputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         
        return null;
    }
    
    public static boolean saveConfigProperty(Properties properties){
        
//        Properties property = new Properties();
        
        OutputStream outputStream = null;
        
        try{
            outputStream = new FileOutputStream(Global.CONFIG_FILE_PATH);
            
            properties.store(outputStream, null);
            
            return true;
            
        }catch(Exception ex){
            
            ex.printStackTrace();
        }finally{
            
            if(outputStream != null)
                try {
                    outputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         
        return false;
    }
    
    public static String removeNonLetterChar(String str) {

        StringBuffer buff = new StringBuffer();
        char chars[] = str.toCharArray();

        for (int i = 0; i < chars.length; i++) {

            if (Character.isLetterOrDigit(chars[i]) || Character.isWhitespace(chars[i])) {

                buff.append(chars[i]);
            }

        }
        return buff.toString();

    } 
    
    public static Object stringToObject(String result, Class type) {

        try {
            JAXBContext contextObj = JAXBContext.newInstance(type);

            Unmarshaller marshallerObj = contextObj.createUnmarshaller();
//            marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(result.getBytes());

//        BufferedOutputStream  bufferedOutputStream = new BufferedOutputStream(outputStream);
            Object cardData = Class.forName(type.getName()).cast(marshallerObj.unmarshal(arrayInputStream));

            return cardData;
        } catch (JAXBException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    public static String objectToString(Object object, Class type) {

        try {
            JAXBContext contextObj = JAXBContext.newInstance(type);

            Marshaller marshallerObj = contextObj.createMarshaller();
            marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
//        BufferedOutputStream  bufferedOutputStream = new BufferedOutputStream(outputStream);
            marshallerObj.marshal(object, arrayOutputStream);

            String data = new String(arrayOutputStream.toByteArray());

            return data;
        } catch (JAXBException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    public static String preOrAppString(String data, String joinStr, int size, boolean prepend){
        
        if(data.length() >= size)
            return data;
        
        int lenDiff = size - data.length();
        
        String temp = "";
        
        for (int i = 0 ; i < lenDiff ; i++){
            
            temp += joinStr;
        }
        
        return prepend? (data + "" + temp) : (temp + ""+data); 
    }
    
//    public static void main(String[] args){
//        
//        System.out.print(); preOrAppString("0", "0", 2, false);
//    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

/**
 *
 * @author emmanueladeyemi
 */
public enum CompanyType {
    
    Sole_Proprietorship,
    Partnership,
    Limited_Liability
}

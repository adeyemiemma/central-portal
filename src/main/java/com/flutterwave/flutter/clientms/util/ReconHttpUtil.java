/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.clientms.model.recon.FlutterTransaction;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;

/**
 *
 * @author emmanueladeyemi
 */
public class ReconHttpUtil {
    
//    private static String url = "https://flutterwavestaging.com:9443/flwreportal/services/reports/card";
//    private static String token = "F71E247C6A7498C8C17EA0E8167C5584DBE5B7B95FC26F6C25035A6F40A584E4CBA182A2D81A250873E1EB9BD586397E767C5D303960D298518AEC7703EC349D";
//    private static String merchantid = "test";
    
    // THis section is for live credentials
    private static String url = "http://flutterwaveprod.com:9090/flwreportal/services/reports/card";
    private static String token = "e8e40b23ad5cde518d6cd45a711589ee3508d119f82678f305840ce5db89c957338a217358617a857dcf97cb8b45f6dcbeb3744233c8d9f4ffc26894f7a28062";
    private static String merchantid = "report";
//    
    public static List<FlutterTransaction> getFlutterTransactions(String startDate, String endDate, long start, long limit){
        
        try {
            
            HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
            
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            
            Stream.of(new String[]{}).forEach(x->{
                arrayBuilder.add(x);
            });
            
            JsonArray jsonArray = arrayBuilder.build();
                    
            String content = "merchantid="+merchantid;
            content +="&token="+token;
            content +="&startdate="+startDate;
            content +="&enddate="+endDate;
            content +="&limit="+limit;
            content +="&start="+start;
            
            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();
            
            os.write(postData);
            
            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();
            
            if(resultCode != 200 && resultCode != 201){
                throw new Exception("Server not available");
            }
            
//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            
            JsonObject response = Json.createReader(is).readObject();
            
            if("R404".equalsIgnoreCase(response.getString("responsecode"))){
                return new ArrayList<>();
            }
            
            if(! "00".equalsIgnoreCase(response.getString("responsecode", "99"))){
                throw new Exception(""+response.getString("responsemessage"));
            }
            
            jsonArray = response.getJsonArray("transactions");
            
            List<FlutterTransaction> flutterTransactions = new ArrayList<>();            
            
            for(JsonValue value : jsonArray){
                
                JsonObject jsonObject = (JsonObject) value;
                
                FlutterTransaction ft = TransactionUtility.getTransaction(jsonObject);
                
                flutterTransactions.add(ft);
            }
            
            return flutterTransactions;
            
        } catch (Exception ex) {
            Logger.getLogger(ReconHttpUtil.class.getName()).log(Level.SEVERE, null, ex);
            
            throw new RuntimeException(ex);
        }
    }
    
     public static List<FlutterTransaction> getFlutterTransactionHTTP(String startDate, String endDate, long start, long limit){
        
        try {
            
            HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
            
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            
            Stream.of(new String[]{}).forEach(x->{
                arrayBuilder.add(x);
            });
            
            JsonArray jsonArray = arrayBuilder.build();
                    
            String content = "merchantid="+merchantid;
            content +="&token="+token;
            content +="&startdate="+startDate;
            content +="&enddate="+endDate;
            content +="&limit="+limit;
            content +="&start="+start;
            
            byte[] postData = content.getBytes();
            OutputStream os = connection.getOutputStream();
            
            os.write(postData);
            
            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();
            
            if(resultCode != 200 && resultCode != 201){
                throw new Exception("Server not available");
            }
            
//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            
            JsonObject response = Json.createReader(is).readObject();
            
            if("R404".equalsIgnoreCase(response.getString("responsecode"))){
                return new ArrayList<>();
            }
            
            if(! "00".equalsIgnoreCase(response.getString("responsecode", "99"))){
                throw new Exception(""+response.getString("responsemessage"));
            }
            
            jsonArray = response.getJsonArray("transactions");
            
            List<FlutterTransaction> flutterTransactions = new ArrayList<>();            
            
            for(JsonValue value : jsonArray){
                
                JsonObject jsonObject = (JsonObject) value;
                
                FlutterTransaction ft = TransactionUtility.getTransaction(jsonObject);
                
                flutterTransactions.add(ft);
            }
            
            return flutterTransactions;
            
        } catch (Exception ex) {
            Logger.getLogger(ReconHttpUtil.class.getName()).log(Level.SEVERE, null, ex);
            
            throw new RuntimeException(ex);
        }
    }
}

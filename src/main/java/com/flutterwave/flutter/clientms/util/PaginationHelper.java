package com.flutterwave.flutter.clientms.util;

import javax.faces.model.DataModel;

public abstract class PaginationHelper {

    private int pageSize;
    private int page;

    public PaginationHelper(int pageSize) {
        this.pageSize = pageSize;
    }

    public abstract long getItemsCount();

    public abstract DataModel createPageDataModel();

    public int getPageFirstItem() {
//        return page * pageSize;

//        System.out.println("First Item"+page);
        
        return page;
    }

    public long getPageLastItem() {
        long i = getPageFirstItem() + pageSize - 1;
        long count = getItemsCount() - 1;
        if (i > count) {
            i = count;
        }
        if (i < 0) {
            i = 0;
        }
        return i;
    }

    public boolean isHasNextPage() {
        
//        System.out.println((page + 1) * pageSize + " Current page");
        
        return ((page + 1) * pageSize + 1) <= getItemsCount();
    }

    public void nextPage() {
        if (isHasNextPage()) {
            page++;
        }
    }

    public boolean isHasPreviousPage() {
        return page > 0;
    }

    public void previousPage() {
        if (isHasPreviousPage()) {
            page--;
        }
    }

    public int getPageSize() {
        return pageSize;
    }

}

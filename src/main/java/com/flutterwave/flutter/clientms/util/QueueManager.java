///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.flutterwave.flutter.clientms.util;
//
//import com.flutterwave.flutter.clientms.dao.ProviderDao;
//import com.flutterwave.flutter.clientms.dao.TransactionDao;
//import com.flutterwave.flutter.clientms.model.Provider;
//import com.flutterwave.flutter.clientms.model.Transaction;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.Future;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.ejb.Asynchronous;
//import javax.ejb.EJB;
//import javax.ejb.Singleton;
//import javax.ejb.Stateless;
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageConsumer;
//
///**
// *
// * @author emmanueladeyemi
// */
//
//@Stateless
//public class QueueManager {
//    
//    @EJB
//    private TransactionDao transactionDao;
//    @EJB
//    private ProviderDao providerDao;
//    
////    @Asynchronous
//    public Future<Void> listen(MessageConsumer consumer){
//        
//        while(true){
//            
//            try{
//                
//                Message message = consumer.receive();
//                
//                try{
//                    Map<String, Object> data = (Map)message.getObjectProperty("data");
//                    
//                    Transaction transaction = new Transaction();
//                    
//                    Provider provider = providerDao.findByKey("shortName", (String)data.getOrDefault("sourceid", null));
//                    
//                    transaction.setProvider(provider);
//                    transaction.setAmount(Double.parseDouble(data.getOrDefault("transactionamount", "0.0") + ""));
//                    transaction.setAuthenticationModel((String)data.getOrDefault("authenticationmodel", null));
//                    transaction.setCardCountry(data.getOrDefault("cardcountry", "NG") + "");
//                    transaction.setCardMask((String)data.getOrDefault("cardmask", null));
//                    transaction.setCreatedOn(new Date());
//                    transaction.setDatetime((String)data.getOrDefault("datetime", null));
//                    transaction.setResponseCode((String)data.getOrDefault("responsecode", null));
//                    transaction.setResponseMessage((String)data.getOrDefault("responsemessage", null));
//                    transaction.setTransactionNarration((String)data.getOrDefault("transactionnarration", null));
//                    transaction.setTransactionType((String)data.getOrDefault("transactiontype", null));
//                    transaction.setFlwTxnReference((String)data.getOrDefault("flutterwavetransactionreference", null));
//                    transaction.setMerchantTxnReference((String)data.getOrDefault("merchanttransactionreference", null));
//                    transaction.setTransactionCountry((String)data.getOrDefault("transactioncountry", null));
//                    transaction.setMerchantId((String)data.getOrDefault("merchantid", null));
//                    
//                    
//                    List<Object> listData = (List<Object>)data.getOrDefault("processorreference",null);
//                    
//                    if(listData != null){
//                        transaction.setProcessorReference(listData.toString());
//                    }
//                    
//                    listData = (List<Object>)data.getOrDefault("transactiondata",null);
//                    
//                    if(listData != null){
//                        transaction.setTransactionData(listData.toString());
//                    }
//                    
//                    transactionDao.create(transaction);
//                }catch(Exception ex){
//                    if(ex != null)
//                        ex.printStackTrace();
//                }
//            }catch(JMSException ex){
//                Logger.getLogger(QueueManager.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
//}

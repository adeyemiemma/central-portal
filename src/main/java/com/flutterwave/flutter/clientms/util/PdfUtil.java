/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author emmanueladeyemi
 */
public class PdfUtil {
    
    static String complianFolder = "flw_client_ms_compliance";
     
//    public static String getComplianceFile(String companyName, String product, String juridiction, String address, String country){
//        
//        try{
//            String filePath = PdfUtil.class.getClassLoader().getResource("CoreCompliance.pdf").getFile();
//
//            File directory = new File(complianFolder);
//
//            if(!directory.exists()){
//                Files.createDirectories(directory.toPath(), new FileAttribute<?>[0]);
//            }
//
//            File file = new File(filePath); // This is the compliance file itself
//
//            File companyFile = new File(directory+"/"+companyName+".pdf"); // This is the company file to be created
//
//            if(companyFile.exists())
//                companyFile.delete();
//
//            companyFile.createNewFile();
//
//            FileInputStream fileInputStream = new FileInputStream(file); // This takes the source file
//            FileChannel sourceChannel = fileInputStream.getChannel(); // This gets the source channel from 
//
//            FileOutputStream fileOutputStream = new FileOutputStream(companyFile); // This gets the output stream for the company file
//
//            FileChannel destinationChannel = fileOutputStream.getChannel();
//            sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel); // This is used to copy file from one source to another
//            
//            PdfReader reader = new PdfReader(filePath);
//            int pageCount = reader.getNumberOfPages();
////            Document document = new Document
//
//            SimpleDateFormat monthFormatter = new SimpleDateFormat("MMM");
//            
//            String month = monthFormatter.format(new Date());
//            int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
//
//            for(int i = 1; i <= pageCount; i++){
//                PdfDictionary pageDictionary = reader.getPageN(i);
//                
//                PdfObject object = pageDictionary.getDirectObject(PdfName.CONTENTS);
//                
//                if (object instanceof PRStream) {
//                    PRStream stream = (PRStream)object;
//                    byte[] data = PdfReader.getStreamBytes(stream);
//                    stream.setData(new String(data).replace("<INSERT COMPANY NAME>", companyName).getBytes("ISO-8859-2"));
//                    stream.setData(new String(data).replace("<INSERT JURIDICTION>", country).getBytes("ISO-8859-2"));
//                    stream.setData(new String(data).replace("<INSERT ADDRESS>", address).getBytes("ISO-8859-2"));
//                    stream.setData(new String(data).replace("INSERT DAY", day+"").getBytes("ISO-8859-2"));
//                    stream.setData(new String(data).replace("INSERT MONTH", month).getBytes("ISO-8859-2"));
//                }
//            }
//            
//            PdfStamper pdfStamper = new PdfStamper(reader, fileOutputStream);
//            pdfStamper.close();
//            reader.close();
//            
//        }catch(Exception ex){
//            if(ex != null)
//                ex.printStackTrace();
//        }
//        
//        return complianFolder+"/"+companyName+".pdf";
//    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

import com.flutterwave.flutter.core.util.PageResult;
import java.util.List;
import java.util.Map;

/**
 *
 * @author emmanueladeyemi
 * @param <T>
 */
public class PageResultWithExtra<T> extends PageResult<T> {

    public PageResultWithExtra() {
    }

    public PageResultWithExtra(Map<String, String> extra, List<T> data, Long recordsTotal, Long recordsFiltered) {
        super(data, recordsTotal, recordsFiltered);
        this.extra = extra;
    }

    /**
     * @return the extra
     */
    public Map<String, String> getExtra() {
        return extra;
    }

    /**
     * @param extra the extra to set
     */
    public void setExtra(Map<String, String> extra) {
        this.extra = extra;
    }
    
    private Map<String, String> extra;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

/**
 *
 * @author emmanueladeyemi
 */
public class EnumUtil {
    
    public enum TransactionType{
        ACCOUNT,
        CARD,
        INTERNETBANKING,
        USSD
    } 
    
    public enum WeekDay{
        All,
        Monday,
        Tuesday,
        Wednesday,
        Thurday,
        Friday,
        Saturday,
        Sunday
    }
    
    public enum Status{
        Successful,
        Failed
    }
}

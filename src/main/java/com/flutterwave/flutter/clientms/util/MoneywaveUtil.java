/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.util;

/**
 *
 * @author emmanueladeyemi
 */
public class MoneywaveUtil {
    
    
    public enum MoneywaveSystemType{
        unknown,
        promo,
        wallet_disburse,
        wallet_fund,
        wallet_transfer_out,
        wallet_transfer_in
    }
    
    public enum MoneywaveStatus{
        pending,
        processing,
        retrying,
        completed,
        failed,
        escalate
    }
    
    public enum Source {
        card,
        wallet,
        voucher,
        settlement,
        transfer
    }
    
    public enum ReportType{
        Card,
        Disbursement
    }
}

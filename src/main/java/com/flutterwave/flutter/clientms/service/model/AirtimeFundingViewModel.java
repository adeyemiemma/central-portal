/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

import com.flutterwave.flutter.clientms.model.User;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author emmanueladeyemi
 */
public class AirtimeFundingViewModel {

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the voucher
     */
    public String getVoucher() {
        return voucher;
    }

    /**
     * @param voucher the voucher to set
     */
    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the appliedOn
     */
    public Date getAppliedOn() {
        return appliedOn;
    }

    /**
     * @param appliedOn the appliedOn to set
     */
    public void setAppliedOn(Date appliedOn) {
        this.appliedOn = appliedOn;
    }

    /**
     * @return the appliedBy
     */
    public String getAppliedBy() {
        return appliedBy;
    }

    /**
     * @param appliedBy the appliedBy to set
     */
    public void setAppliedBy(String appliedBy) {
        this.appliedBy = appliedBy;
    }

    /**
     * @return the balanceBefore
     */
    public BigDecimal getBalanceBefore() {
        return balanceBefore;
    }

    /**
     * @param balanceBefore the balanceBefore to set
     */
    public void setBalanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    /**
     * @return the balanceAfter
     */
    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    /**
     * @param balanceAfter the balanceAfter to set
     */
    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the initiatedBy
     */
    public String getInitiatedBy() {
        return initiatedBy;
    }

    /**
     * @param initiatedBy the initiatedBy to set
     */
    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }
    
    private Long id;
    private Date createdOn;
    private String voucher;
    private String merchantId;
    private Date appliedOn;
    private String appliedBy;
    private BigDecimal balanceBefore;
    private BigDecimal balanceAfter;
    private BigDecimal amount;
    private String initiatedBy;
    private String merchantName;
    private String currency;
    private String reference;
}

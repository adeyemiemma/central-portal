///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.flutterwave.flutter.clientms.service.recon;
//
//import com.flutterwave.flutter.clientms.model.Log;
//import com.flutterwave.flutter.clientms.service.LogService;
//import com.flutterwave.flutter.clientms.util.LogLevel;
//import com.flutterwave.flutter.clientms.util.LogStatus;
//import java.io.Serializable;
//import java.util.Date;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//import javax.annotation.PostConstruct;
//import javax.annotation.Resource;
//import javax.ejb.EJB;
//import javax.ejb.Singleton;
//import javax.ejb.Startup;
//import javax.jms.Connection;
//import javax.jms.ConnectionFactory;
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageProducer;
//import javax.jms.Queue;
//import javax.jms.Session;
//
///**
// *
// * @author emmanueladeyemi
// */
//@Singleton
//public class TransactionFetchScheduler {
//    
//    @Resource(lookup = "java:/ConnectionFactory")
//    private ConnectionFactory connectionFactory;
//    
//    @Resource(lookup = "java:jboss/activemq/queue/TransactionQueue")
//    private Queue queue;
//    
//    Session session;
//    
//    @EJB
//    private LogService logService;
//    
//    @PostConstruct
//    public void start(){
//        
//        try {
//            
//            Log log = new Log();
//            log.setAction("START UP QUEUE CONNECTION");
//            log.setCreatedOn(new Date());
//            log.setDescription("Starting up queue connection");
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setLevel(LogLevel.Info);
//            logService.log(log);
//            
//            Connection connection = connectionFactory.createConnection();
//            // Create a Session
//            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//            connection.start();
//            
//            log = new Log();
//            log.setAction("START UP QUEUE CONNECTION");
//            log.setCreatedOn(new Date());
//            log.setDescription("queue connection started successfully");
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setLevel(LogLevel.Info);
//            logService.log(log);
//            
//        } catch (JMSException ex) {
//            Logger.getLogger(TransactionFetchScheduler.class.getName()).log(Level.SEVERE, null, ex);
//            
//            try{
//                Log log = new Log();
//                log.setAction("START QUEUE");
//                log.setCreatedOn(new Date());
//                log.setDescription("START QUEUE FAILED");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                
//                if(ex != null){
////                    log.setStatusMessage(ex.getMessage());
//                    
//                    String detailedString = Stream.of(ex.getStackTrace())
//                            .map(x -> x.toString()).collect(Collectors.joining(","));
//                    logService.log(log, detailedString);
//                }
//                else
//                    logService.log(log);
//            }catch(Exception e){
//            }
//        }
//    }
//    
//    public void scheduleTransactions(FetchTransactionRequest request){
//        
//        try {
//            
//            Log log = new Log();
//            log.setAction("SCHEDULING REQUEST");
//            log.setCreatedOn(new Date());
//            log.setDescription("scheduling request started");
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setLevel(LogLevel.Info);
//            logService.log(log);
//            
//            if(session == null)
//                start();
//            
//            MessageProducer producer = session.createProducer(queue);
//            
//            Message message = session.createObjectMessage(request);
//            
//            // This is called to set delay on the queue
//            if(request.getSchedule() > 0L)
//                message.setLongProperty("AMQ_SCHEDULED_DELAY", request.getSchedule());
//            
//            // This is called to send the message to the queue
//            producer.send(message);
//            
//            log = new Log();
//            log.setAction("SCHEDULING REQUEST");
//            log.setCreatedOn(new Date());
//            log.setDescription("scheduling request ended");
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setLevel(LogLevel.Info);
//            logService.log(log);
//            
//            
//        } catch (JMSException ex) {
//            Logger.getLogger(TransactionFetchScheduler.class.getName()).log(Level.SEVERE, null, ex);
//            
//            try{
//                Log log = new Log();
//                log.setAction("SCHEDULING MESSAGE");
//                log.setCreatedOn(new Date());
//                log.setDescription("SCHEDULING MESSAGE FAILED");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                
//                if(ex != null){
////                    log.setStatusMessage(ex.getMessage());
//                    
//                    String detailedString = Stream.of(ex.getStackTrace())
//                            .map(x -> x.toString()).collect(Collectors.joining(","));
//                    logService.log(log, detailedString);
//                }
//                else
//                    logService.log(log);
//            }catch(Exception e){
//            }
//        }
//    }
//    
//    public void scheduleTransactions(Serializable request){
//        
//        try {
//            
//            Log log = new Log();
//            log.setAction("SCHEDULING REQUEST");
//            log.setCreatedOn(new Date());
//            log.setDescription("scheduling request started");
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setLevel(LogLevel.Info);
//            logService.log(log);
//            
//            if(session == null)
//                start();
//            
//            MessageProducer producer = session.createProducer(queue);
//            
//            Message message = session.createObjectMessage(request);
//            message.setLongProperty("AMQ_SCHEDULED_DELAY", 60*1000);
//            
//            // This is called to send the message to the queue
//            producer.send(message);
//            
//            log = new Log();
//            log.setAction("SCHEDULING REQUEST");
//            log.setCreatedOn(new Date());
//            log.setDescription("scheduling request ended");
//            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setLevel(LogLevel.Info);
//            logService.log(log);
//            
//            
//        } catch (JMSException ex) {
//            Logger.getLogger(TransactionFetchScheduler.class.getName()).log(Level.SEVERE, null, ex);
//            
//            try{
//                Log log = new Log();
//                log.setAction("SCHEDULING MESSAGE");
//                log.setCreatedOn(new Date());
//                log.setDescription("SCHEDULING MESSAGE FAILED");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                
//                if(ex != null){
////                    log.setStatusMessage(ex.getMessage());
//                    
//                    String detailedString = Stream.of(ex.getStackTrace())
//                            .map(x -> x.toString()).collect(Collectors.joining(","));
//                    logService.log(log, detailedString);
//                }
//                else
//                    logService.log(log);
//            }catch(Exception e){
//            }
//        }
//    }
//}

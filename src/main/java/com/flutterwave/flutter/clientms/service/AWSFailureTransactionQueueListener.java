///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.flutterwave.flutter.clientms.service;
//
//import com.amazon.sqs.javamessaging.ProviderConfiguration;
//import com.amazon.sqs.javamessaging.SQSConnectionFactory;
//import com.amazonaws.auth.AWSStaticCredentialsProvider;
//import com.amazonaws.auth.BasicAWSCredentials;
//import com.amazonaws.regions.Regions;
//import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
//import com.flutterwave.flutter.clientms.dao.AirtimeTransactionDao;
//import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
//import com.flutterwave.flutter.clientms.dao.ProviderDao;
//import com.flutterwave.flutter.clientms.dao.TransactionDao;
//import com.flutterwave.flutter.clientms.model.AirtimeTransaction;
//import com.flutterwave.flutter.clientms.model.Log;
//import com.flutterwave.flutter.clientms.model.Provider;
//import com.flutterwave.flutter.clientms.model.Transaction;
//import com.flutterwave.flutter.clientms.util.LogDomain;
//import com.flutterwave.flutter.clientms.util.LogLevel;
//import com.flutterwave.flutter.clientms.util.LogState;
//import com.flutterwave.flutter.clientms.util.LogStatus;
//import com.flutterwave.flutter.clientms.util.Utility;
//import java.io.Serializable;
//import java.io.StringReader;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import javax.ejb.ConcurrencyManagement;
//import javax.ejb.ConcurrencyManagementType;
//import javax.ejb.EJB;
//import javax.ejb.LocalBean;
//import javax.ejb.Singleton;
//import javax.ejb.Startup;
//import javax.jms.Connection;
//import javax.jms.Destination;
//import javax.jms.ExceptionListener;
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageConsumer;
//import javax.jms.MessageListener;
//import javax.jms.ObjectMessage;
//import javax.jms.Session;
//import javax.jms.TextMessage;
//import javax.json.Json;
//import javax.json.JsonArray;
//import javax.json.JsonArrayBuilder;
//import javax.json.JsonObject;
//import javax.json.JsonObjectBuilder;
//import javax.json.JsonReader;
//import javax.json.stream.JsonParser;
//import org.apache.activemq.ActiveMQConnectionFactory;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author emmanueladeyemi
// */
////@MessageDriven(activationConfig = {
////    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
////    @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:jboss/exported/SETTLEMENT_TRANSACTION_QUEUE"),             
////    @ActivationConfigProperty(propertyName = "user", propertyValue = "defaultUsername"),
////    @ActivationConfigProperty(propertyName = "password", propertyValue = "defaultPassword"),  
////    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") 
////    }
////)
//@Startup
//@LocalBean
//@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
//@Singleton
//public class AWSFailureTransactionQueueListener implements MessageListener, Runnable, ExceptionListener{
//
//    SQSConnectionFactory connectionFactory;
//    
//    private final static String URL_STRING = "tcp://localhost:61616";
//    private final static String QUEUE_NAME = "SETTLEMENT_FAILURE_TRANSACTION_QUEUE";
//    
//    Session session;
//    Connection connection;
//            
//    @EJB
//    private LogService logService;
//    
//    @EJB
//    private TransactionDao transactionDao;
//    @EJB
//    private ProviderDao providerDao;
//    @EJB
//    private AirtimeTransactionDao airtimeTransactionDao;
//    @EJB
//    private ConfigurationDao configurationDao;
//    
//    boolean foundRRN = false, isDisbursement, isDisbursementMessage;
//    boolean transactresultresponsecode = false, clienttxnref = false, reverseresponsecode = false;
//    boolean reverseresponsemessage = false;
//    boolean foundSourceIdentifier = false,foundTargetIdentifier = false;
//    
//    @PostConstruct
//    public void startup(){
//         
//        run();
//    }
//    
//    public void run() {
//
//        try {
//
//            String url = configurationDao.getConfig("aws_airtime_queue");
//        
//        if(url == null){
//            url = "https://sqs.eu-west-2.amazonaws.com";
//        }
//        
//        String accessKey = configurationDao.getConfig("aws_access_key");
//        
//        if(accessKey == null)
//            accessKey = "AKIAJ3CVNCSR5SKEUGYQ";
//        
//        String secretKey = configurationDao.getConfig("aws_secret_key");
//        
//        if(secretKey == null)
//            secretKey = "wR2HYAdVQeJ0waLd9vzkAg4tNhFezsCcY3sRdur0";
//        
//        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
//        
//        connectionFactory = new SQSConnectionFactory(
//            new ProviderConfiguration(),
//                AmazonSQSClientBuilder.standard().withRegion(Regions.EU_WEST_2).withCredentials(new AWSStaticCredentialsProvider(credentials))
//            );
//
//        // Create a Connection
//        connection = connectionFactory.createConnection();
//
//        connection.start();
//
//            connection.setExceptionListener(this);
//            
//            // Create a Session
//            session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
//
//            // Create the destination (Topic or Queue)
//            Destination destination = session.createQueue(QUEUE_NAME);
//
////            session.close();
//            // Create a MessageConsumer from the Session to the Topic or Queue
//            MessageConsumer consumer = session.createConsumer(destination);
//            
//            consumer.setMessageListener(this);
//            
//        } catch (JMSException ex) {
//            Logger.getLogger(AWSFailureTransactionQueueListener.class.getName()).error(null, ex);
//        }catch(Exception ex){
//            Logger.getLogger(AWSFailureTransactionQueueListener.class.getName()).error(null, ex);
//        }
//        
//    }
//    
//    
//    @Override
//    public synchronized void onException(JMSException jmse) {
//        System.out.println("JMS Exception occured.  Shutting down client.");
//    }
//
//    @Override
//    public void onMessage(Message message) {
//
//        try {
//
//            if (message instanceof TextMessage) {
//                TextMessage textMessage = (TextMessage) message;
//                String text = textMessage.getText();
//                System.out.println("Received T : " + text);
//                
//            Log log = new Log();
//            log.setAction("Failed Failed Queue Called back SQS ");
//            log.setCreatedOn(new Date());
//            log.setLevel(LogLevel.Info);
//            log.setLogDomain(LogDomain.ADMIN);
//            log.setDescription(text);
//            log.setLogState(LogState.STARTED);
//            log.setStatus(LogStatus.SUCCESSFUL);
//            
////            log.setLogDomainId(0);
//            logService.log(log);  
//            
//                JsonReader jReader =  Json.createReader(new StringReader(text));
//                JsonObject jsonObject = jReader.readObject();
//                
//                String type = jsonObject.getString("transactiontype", null);
//
//                if ("AIRTIME".equalsIgnoreCase(type)) {
//
//                    AirtimeTransaction transaction = new AirtimeTransaction();
//                    
//                    transaction.setProvider((String) jsonObject.getString("sourceid", null));
//                    transaction.setAmount(Double.parseDouble(jsonObject.getString("transactionamount", "0.0") + ""));
//                    transaction.setAuthenticationModel((String) jsonObject.getString("authenticationmodel", null));
//                    transaction.setCardCountry(jsonObject.getString("cardcountry", "NG") + "");
//                    transaction.setCardMask((String) jsonObject.getString("cardmask", null));
//                    transaction.setCreatedOn(new Date());
//                    transaction.setDatetime((String) jsonObject.getString("datetime", null));
//                    transaction.setResponseCode((String) jsonObject.getString("responsecode", null));
//                    transaction.setResponseMessage((String) jsonObject.getString("responsemessage", null));
//                    transaction.setTransactionNarration((String) jsonObject.getString("transactionnarration", null));
//                    transaction.setTransactionType((String) jsonObject.getString("transactiontype", null));
//                    transaction.setFlwTxnReference((String) jsonObject.getString("flutterwavetransactionreference", null));
//                    transaction.setMerchantTxnReference((String) jsonObject.getString("merchanttranscationreference", null));
//                    transaction.setTransactionCountry((String) jsonObject.getString("transactioncountry", null));
//                    transaction.setTransactionCurrency((String) jsonObject.getString("transactioncurrency", null));
//                    transaction.setMerchantId((String) jsonObject.getString("merchantid", null));
//                    transaction.setCardScheme((String) jsonObject.getString("cardscheme", null));
//                    
//                    JsonArray listData = jsonObject.getJsonArray("processorreference");
//                    
//                    if (listData != null) {
//
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//
////                        foundRRN = false;
//                        listData.stream().forEach(x -> {
//                            JsonObject value = (JsonObject) x;
//
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//
//                            foundSourceIdentifier = false;
//                            foundTargetIdentifier = false;
//                            transactresultresponsecode = false;
//                            foundRRN = false;
//                            clienttxnref = false; reverseresponsecode = false;
//                            reverseresponsemessage = false;
//
//                            value.entrySet().stream().forEach(y -> {
//
//                                String str = String.valueOf(y.getValue()).replaceAll("\"", "");
//                                builder.add(y.getKey(), str);
//
//                                if (foundRRN == true) {
//                                    transaction.setRrn(str);
//                                    foundRRN = false;
//                                }
//                                if (str.equalsIgnoreCase("retrievalreferencenumber")) {
//                                    foundRRN = true;
//                                }
//
//                                if (foundSourceIdentifier == true) {
//                                    transaction.setSourceIdentifier(str);
//                                    foundSourceIdentifier = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("sourceidentifier")) {
//                                    foundSourceIdentifier = true;
//                                }
//                                
//                                if (foundTargetIdentifier == true) {
//                                    transaction.setTargetIdentifier(str);
//                                    foundTargetIdentifier = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("targetidentifier")) {
//                                    foundTargetIdentifier = true;
//                                }
//                                
//                                if (transactresultresponsecode == true) {
//                                    transaction.setTransactResultResponseCode(str);
//                                    transactresultresponsecode = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("transactresultresponsecode")) {
//                                    transactresultresponsecode = true;
//                                }
//                                
//                                if (clienttxnref == true) {
//                                    transaction.setClientTxnRef(str);
//                                    clienttxnref = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("clienttxnref")) {
//                                    clienttxnref = true;
//                                }
//
//                                if (reverseresponsecode == true) {
//                                    transaction.setReverseResponseCode(str);
//                                    reverseresponsecode = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("reverseresponsecode")) {
//                                    reverseresponsecode = true;
//                                }
//
//                                if (reverseresponsemessage == true) {
//                                    transaction.setReverseResponseMessage(str);
//                                    reverseresponsemessage = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("reverseresponsemessage")) {
//                                    reverseresponsemessage = true;
//                                }
//                                
//                                
//                            });
//
//                            arrayBuilder.add(builder.build());
//                        });
//                        transaction.setProcessorReference(arrayBuilder.build().toString());
//                    }
//                    
//                    listData = jsonObject.getJsonArray("transactiondata");
////                    listData = (List<Object>)jsonObject.getOrDefault("transactionjsonObject",null);
//
//                    if (listData != null) {
//
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//                        listData.stream().forEach(x -> {
//                            JsonObject value = (JsonObject) x;
//
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//
//                            value.entrySet().stream().forEach(y -> {
//                                builder.add(y.getKey(), String.valueOf(y.getValue()).replaceAll("\"", ""));
//                            });
//
//                            arrayBuilder.add(builder.build());
//                        });
//
////                        String str = arrayBuilder.toString();
//
//                        transaction.setTransactionData(arrayBuilder.build().toString());
//                    }
//
//                    AirtimeTransaction txn = airtimeTransactionDao.findByKey("flwTxnReference", transaction.getFlwTxnReference());
//
//                    if (txn == null) {
//                        airtimeTransactionDao.create(transaction);
//                        log = new Log();
//                        log.setAction("Failed Transaction Queue Called back");
//                        log.setCreatedOn(new Date());
//                        log.setLevel(LogLevel.Info);
//                        log.setLogDomain(LogDomain.ADMIN);
//                        log.setDescription("successful callback queue processed for " + transaction.getFlwTxnReference());
//                        log.setLogState(LogState.FINISH);
//                        log.setStatus(LogStatus.SUCCESSFUL);
//
//                        logService.log(log);
//                    } else {
//
//                        if ("00".equalsIgnoreCase(txn.getResponseCode()) && "00".equalsIgnoreCase(transaction.getResponseCode())) {
//
//                            log = new Log();
//                            log.setAction("Failed Transaction Queue Called back");
//                            log.setCreatedOn(new Date());
//                            log.setLevel(LogLevel.Info);
//                            log.setLogDomain(LogDomain.ADMIN);
//                            log.setDescription("Replacing failed duplicate with successful for transaction " + transaction.getFlwTxnReference());
//                            log.setLogState(LogState.FINISH);
//                            log.setStatus(LogStatus.SUCCESSFUL);
//
//                            transaction.setId(txn.getId());
//
//                            airtimeTransactionDao.update(transaction);
//                        } else {
//                            log = new Log();
//                            log.setAction("Failed Transaction Queue Called back");
//                            log.setCreatedOn(new Date());
//                            log.setLevel(LogLevel.Info);
//                            log.setLogDomain(LogDomain.ADMIN);
//                            log.setDescription("Duplicate reference for transaction " + transaction.getFlwTxnReference());
//                            log.setLogState(LogState.FINISH);
//                            log.setStatus(LogStatus.SUCCESSFUL);
//                        }
//
//                        logService.log(log);
//                    }
//                    
//                }else{
//                
//                
//                Transaction transaction = new Transaction();
//                    
////                    Provider provider = providerDao.findByKey("shortName", (String)jsonObject.getString("sourceid", null));
//                    
//                    transaction.setProvider((String)jsonObject.getString("sourceid", null));
//                    transaction.setAmount(Double.parseDouble(jsonObject.getString("transactionamount", "0.0") + ""));
//                    transaction.setAuthenticationModel((String)jsonObject.getString("authenticationmodel", null));
//                    transaction.setCardCountry(jsonObject.getString("cardcountry", "NG") + "");
//                    transaction.setCardMask((String)jsonObject.getString("cardmask", null));
//                    transaction.setCreatedOn(new Date());
//                    transaction.setDatetime((String)jsonObject.getString("datetime", null));
//                    transaction.setResponseCode((String)jsonObject.getString("responsecode", null));
//                    transaction.setResponseMessage((String)jsonObject.getString("responsemessage", null));
//                    transaction.setTransactionNarration((String)jsonObject.getString("transactionnarration", null));
//                    transaction.setTransactionType((String)jsonObject.getString("transactiontype", null));
//                    transaction.setFlwTxnReference((String)jsonObject.getString("flutterwavetransactionreference", null));
//                    transaction.setMerchantTxnReference((String)jsonObject.getString("merchanttranscationreference", null));
//                    transaction.setTransactionCountry((String)jsonObject.getString("transactioncountry", null));
//                    transaction.setTransactionCurrency((String)jsonObject.getString("transactioncurrency", null));
//                    transaction.setMerchantId((String)jsonObject.getString("merchantid", null));
//                    transaction.setSettlementState(Utility.SettlementState.NONE);
//                    transaction.setCardScheme((String) jsonObject.getString("cardscheme", null));
//                    
//                    
//                    JsonArray listData = jsonObject.getJsonArray("processorreference");
//                    
//                    if(listData != null){
//                        
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//                        
//                        listData.stream().forEach(x->{
//                            JsonObject value = (JsonObject) x;
//                            
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//                            
//                            foundRRN = false;
//                            
//                            value.entrySet().stream().forEach(y ->{
//                                
//                                String str = String.valueOf(y.getValue()).replaceAll("\"", "");
//                                builder.add(y.getKey(), str);
//                                
//                                if(isDisbursement == true){                                
//                                    transaction.setDisburseResponseCode(str);
//                                    isDisbursement = false;
//                                }
//
//                                if(str.equalsIgnoreCase("postresponsecode")){
//                                    isDisbursement = true;
//                                }
//
//                                if(isDisbursementMessage == true){                                
//                                    transaction.setDisburseResponseMessage(str);
//                                    isDisbursementMessage = false;
//                                }
//
//                                if(str.equalsIgnoreCase("postresponsemessage")){
//                                    isDisbursementMessage = true;
//                                }
//                                
//                                if(foundRRN == true){
//                                    transaction.setRrn(str);
//                                    foundRRN = false;
//                                }
//                                
//                                if(str.equalsIgnoreCase("retrievalreferencenumber")){
//                                    foundRRN = true;
//                                }
//                            });
//                            
//                            arrayBuilder.add(builder.build());
//                        });
//                        transaction.setProcessorReference(arrayBuilder.build().toString());
//                    }
//                    
//                    listData = jsonObject.getJsonArray("transactiondata");
////                    listData = (List<Object>)jsonObject.getOrDefault("transactionjsonObject",null);
//                    
//                    if(listData != null){
//                        
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//                        listData.stream().forEach(x->{
//                            JsonObject value = (JsonObject) x;
//                            
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//                            
//                            value.entrySet().stream().forEach(y ->{
//                                builder.add(y.getKey(), String.valueOf(y.getValue()).replaceAll("\"", ""));
//                            });
//                            
//                            arrayBuilder.add(builder.build());
//                        });
//                        
//                        String str = arrayBuilder.toString();
//                        
//                        transaction.setTransactionData(arrayBuilder.build().toString());
//                    }
//                    
//                   Transaction txn = transactionDao.findByKey("flwTxnReference", transaction.getFlwTxnReference());
//                   
//                   if(txn == null){
//                        transactionDao.create(transaction);
//                        log = new Log();
//                        log.setAction("Failed Transaction Queue Called back");
//                        log.setCreatedOn(new Date());
//                        log.setLevel(LogLevel.Info);
//                        log.setLogDomain(LogDomain.ADMIN);
//                        log.setDescription("successful callback queue processed for "+transaction.getFlwTxnReference());
//                        log.setLogState(LogState.FINISH);
//                        log.setStatus(LogStatus.SUCCESSFUL);
//                        
//                        logService.log(log);
//                   }
//                   else{
//                       log = new Log();
//                        log.setAction("Failed Transaction Queue Called back");
//                        log.setCreatedOn(new Date());
//                        log.setLevel(LogLevel.Info);
//                        log.setLogDomain(LogDomain.ADMIN);
//                        log.setDescription("Duplicate reference for transaction "+transaction.getFlwTxnReference());
//                        log.setLogState(LogState.FINISH);
//                        log.setStatus(LogStatus.SUCCESSFUL);
//                        
//                        logService.log(log);
//                   }
//                }
//                   
////                   com.sun.messaging.jms.Message msg = ((com.sun.messaging.jms.Message)message);
////                    msg.acknowledgeThisMessage();
//                    message.acknowledge();
//                    
//                
//            } else 
//            if (message instanceof ObjectMessage) {                
//                
//                try{
//                    
//                    Log log = new Log();
//                log.setAction("SUCCESS TRansaxction Queue Called back");
//                log.setCreatedOn(new Date());
//                log.setLevel(LogLevel.Info);
//                log.setLogDomain(LogDomain.ADMIN);
////                log.setDescription(text);
//                log.setLogState(LogState.STARTED);
//                log.setStatus(LogStatus.SUCCESSFUL);
//            
////            log.setLogDomainId(0);
////            logService.log(log);  
//                    Serializable sdata = ((ObjectMessage) message).getObject();
//                    
//                    log.setDescription(sdata.toString());
//                    logService.log(log);
//                    
//                    System.out.println("Received T : " + sdata.toString());
//                    
//                    Map<String, Object> data = (Map) sdata;
//                    
//                    Transaction transaction = new Transaction();
//                    
////                    Provider provider = providerDao.findByKey("shortName", (String)data.getOrDefault("sourceid", null));
//                    
//                    transaction.setProvider((String)data.getOrDefault("sourceid", null));
//                    transaction.setAmount(Double.parseDouble(data.getOrDefault("transactionamount", "0.0") + ""));
//                    transaction.setAuthenticationModel((String)data.getOrDefault("authenticationmodel", null));
//                    transaction.setCardCountry(data.getOrDefault("cardcountry", "NG") + "");
//                    transaction.setCardMask((String)data.getOrDefault("cardmask", null));
//                    transaction.setCreatedOn(new Date());
//                    transaction.setDatetime((String)data.getOrDefault("datetime", null));
//                    transaction.setResponseCode((String)data.getOrDefault("responsecode", null));
//                    transaction.setResponseMessage((String)data.getOrDefault("responsemessage", null));
//                    transaction.setTransactionNarration((String)data.getOrDefault("transactionnarration", null));
//                    transaction.setTransactionType((String)data.getOrDefault("transactiontype", null));
//                    transaction.setFlwTxnReference((String)data.getOrDefault("flutterwavetransactionreference", null));
//                    transaction.setMerchantTxnReference((String)data.getOrDefault("merchanttranscationreference", null));
//                    transaction.setTransactionCountry((String)data.getOrDefault("transactioncountry", null));
//                    transaction.setTransactionCurrency((String)data.getOrDefault("transactioncurrency", null));
//                    transaction.setMerchantId((String)data.getOrDefault("merchantid", null));
//                    transaction.setCardScheme((String) data.getOrDefault("cardscheme", null));
//                    transaction.setSettlementState(Utility.SettlementState.NONE);
//                    
//                    List<Object> listData = (List<Object>)data.getOrDefault("processorreference",null);
//                    
//                    if(listData != null){
//                        
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//                        listData.stream().forEach(x->{
//                            Map<String, Object> value = (HashMap<String, Object>) x;
//                            
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//                            
//                            value.entrySet().stream().forEach(y ->{
//                                builder.add(y.getKey(), String.valueOf(y.getValue()));
//                            });
//                            
//                            arrayBuilder.add(builder.build());
//                        });
//                        transaction.setProcessorReference(arrayBuilder.build().toString());
//                    }
//                    
//                    listData = (List<Object>)data.getOrDefault("transactiondata",null);
//                    
//                    if(listData != null){
//                        
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//                        listData.stream().forEach(x->{
//                            Map<String, Object> value = (HashMap<String, Object>) x;
//                            
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//                            
//                            value.entrySet().stream().forEach(y ->{
//                                builder.add(y.getKey(), String.valueOf(y.getValue()));
//                            });
//                            
//                            arrayBuilder.add(builder.build());
//                        });
//                        transaction.setTransactionData(arrayBuilder.build().toString());
//                    }
//                    
//                   Transaction txn = transactionDao.findByKey("flwTxnReference", transaction.getFlwTxnReference());
//                   
//                   if(txn == null){
//                        transactionDao.create(transaction);
//                        log = new Log();
//                        log.setAction("Failed Transaction Queue Called back");
//                        log.setCreatedOn(new Date());
//                        log.setLevel(LogLevel.Info);
//                        log.setLogDomain(LogDomain.ADMIN);
//                        log.setDescription("successful callback queue processed for "+transaction.getFlwTxnReference());
//                        log.setLogState(LogState.FINISH);
//                        log.setStatus(LogStatus.SUCCESSFUL);
//                        
//                        logService.log(log);
//                   }
//                   else{
//                       log = new Log();
//                        log.setAction("Failed Transaction Queue Called back");
//                        log.setCreatedOn(new Date());
//                        log.setLevel(LogLevel.Info);
//                        log.setLogDomain(LogDomain.ADMIN);
//                        log.setDescription("Duplicate reference for transaction "+transaction.getFlwTxnReference());
//                        log.setLogState(LogState.FINISH);
//                        log.setStatus(LogStatus.SUCCESSFUL);
//                        
//                        logService.log(log);
//                   }
//                       
////                   com.sun.messaging.jms.Message msg = ((com.sun.messaging.jms.Message)message);
////                    msg.acknowledgeThisMessage();
//                    message.acknowledge();
//                }catch(Exception ex){
//                    if(ex != null)
//                        ex.printStackTrace();
//                }
//
//                
////                if(map != null)
//                
//            }
//        } catch (Exception ex) {
//            if (ex != null) {
//                ex.printStackTrace();
//            }
//        }
//    }
//    
//    @PreDestroy
//    public void tidyUp(){
//        
//        try{
//            
//            if(session != null)
//                session.close();
//            
//            if(connection != null)
//                connection.close();
//            
//        }catch(Exception ex){
//            if(ex != null)
//                ex.printStackTrace();;
//        }
//    }
//    
//    
//}

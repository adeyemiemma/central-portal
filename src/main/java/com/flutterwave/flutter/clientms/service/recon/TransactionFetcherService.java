///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.flutterwave.flutter.clientms.service.recon;
//
//import com.flutterwave.flutter.core.exception.DatabaseException;
//import com.flutterwavego.automated.recon.system.dao.FetchTransactionStatusDao;
//import com.flutterwavego.automated.recon.system.dao.FlutterTransactionDao;
//import com.flutterwavego.automated.recon.system.model.FetchTransactionRequest;
//import com.flutterwavego.automated.recon.system.model.FetchTransactionStatus;
//import com.flutterwavego.automated.recon.system.model.FlutterTransaction;
//import com.flutterwavego.automated.recon.system.model.Log;
//import com.flutterwavego.automated.recon.system.model.ResolveTransactionRequest;
//import com.flutterwavego.automated.recon.system.util.FetchStatus;
//import com.flutterwavego.automated.recon.system.util.HttpUtil;
//import com.flutterwavego.automated.recon.system.util.LogLevel;
//import com.flutterwavego.automated.recon.system.util.LogStatus;
//import com.flutterwavego.automated.recon.system.util.TransactionUtility;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//import javax.ejb.Asynchronous;
//import javax.ejb.DependsOn;
//import javax.ejb.EJB;
//import javax.ejb.Schedule;
//import javax.ejb.Singleton;
//import javax.ejb.Startup;
//import javax.transaction.Transactional;
//
///**
// *
// * @author emmanueladeyemi
// */
//@Startup
//@Singleton
//@DependsOn(value = {"FlutterTransactionDao"})
//public class TransactionFetcherService {
//    
//    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//    
//    @EJB
//    private FlutterTransactionDao flutterTransactionDao;
//    @EJB
//    private LogService logService;
//    @EJB
//    private FetchTransactionStatusDao fetchTransactionStatusDao;
//    @EJB 
//    private TransactionFetchScheduler fetchScheduler;
//    
//    private static final long BATCH_SIZE = 1000L;
//    
//    @Schedule(dayOfMonth = "*", hour = "3,22", minute = "45", second = "0")
//    public void pullTransactions(){
//        
//        FetchTransactionStatus status = new FetchTransactionStatus();
//        
//        TransactionUtility.buildWrapperClass();
//        
//        Log log = new Log();
//        
//        try {
//            String date = dateFormat.format(new Date());
//            
//            log.setAction("FETCH FLUTTERWAVE TRANSACTION STARTED");
//            log.setCreatedOn(new Date());
//            log.setDescription("Fetching transaction from flutterwave started");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//            Calendar calendar = Calendar.getInstance();
//            calendar.add(Calendar.DAY_OF_MONTH, -1);
//            status.setAttempt(1);
//            status.setCreatedOn(new Date());
//            status.setStartDate(dateFormat.format(calendar.getTime()));
//            calendar.add(Calendar.DAY_OF_MONTH, 1);
//            status.setEndDate(dateFormat.format(calendar.getTime()));
//                    
//            List<FlutterTransaction> transactions = HttpUtil.getFlutterTransactions(status.getStartDate(), status.getEndDate(), 0, BATCH_SIZE);
//            
//            logTransactions(transactions, status);
//            
//            if(transactions.size() >= BATCH_SIZE){
//                FetchTransactionRequest fetchTransactionRequest = new FetchTransactionRequest();
//                fetchTransactionRequest.setEndDate(status.getEndDate());
//                fetchTransactionRequest.setStartDate(status.getStartDate());
//                fetchTransactionRequest.setStart(BATCH_SIZE);
//                fetchTransactionRequest.setLimit(BATCH_SIZE);
//                
//                fetchScheduler.scheduleTransactions(fetchTransactionRequest);
//            }
//            
//            log = new Log();
//            log.setAction("FETCH FLUTTERWAVE TRANSACTION");
//            log.setCreatedOn(new Date());
//            log.setDescription("fetching transaction from flutterwave completed");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//        } catch (Exception ex) {
//            Logger.getLogger(TransactionFetcherService.class.getName()).log(Level.SEVERE, null, ex);
//            
//            try{
//                log = new Log();
//                log.setAction("FETCH FLUTTERWAVE TRANSACTION");
//                log.setCreatedOn(new Date());
//                log.setDescription("fetching flutterwave transactions");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                
//                if(ex != null){
////                    log.setStatusMessage(ex.getMessage());
//                    
//                    String detailedString = Stream.of(ex.getStackTrace())
//                            .map(x -> x.toString()).collect(Collectors.joining(","));
//                    logService.log(log, detailedString);
//                }
//                else
//                    logService.log(log);
//            }catch(Exception e){
//            }
//        }
//    }
//    
//    @Schedule(month = "3", dayOfMonth = "20", hour = "2", minute = "0", second = "0")
//    public void pullOneMonthTransactions(){
//        FetchTransactionStatus status = new FetchTransactionStatus();
//        
//        TransactionUtility.buildWrapperClass();
//        
//        Log log = new Log();
//        
//        try {
//            String date = dateFormat.format(new Date());
//            
//            log.setAction("FETCH FLUTTERWAVE TRANSACTION STARTED");
//            log.setCreatedOn(new Date());
//            log.setDescription("Fetching transaction from flutterwave started");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//            Calendar calendar = Calendar.getInstance();
//            calendar.set(Calendar.DAY_OF_MONTH, 1);
//            status.setAttempt(1);
//            status.setCreatedOn(new Date());
//            status.setStartDate(dateFormat.format(calendar.getTime()));
//            calendar.setTime(new Date());
//            calendar.add(Calendar.DAY_OF_MONTH, -1);
//            
//            status.setEndDate(dateFormat.format(calendar.getTime()));
//                    
//            List<FlutterTransaction> transactions = HttpUtil.getFlutterTransactions(status.getStartDate(), status.getEndDate(), 0, BATCH_SIZE);
//            
//            logTransactions(transactions, status);
//            
//            if(transactions.size() >= BATCH_SIZE){
//                FetchTransactionRequest fetchTransactionRequest = new FetchTransactionRequest();
//                fetchTransactionRequest.setEndDate(status.getEndDate());
//                fetchTransactionRequest.setStartDate(status.getStartDate());
//                fetchTransactionRequest.setStart(BATCH_SIZE);
//                fetchTransactionRequest.setLimit(BATCH_SIZE);
//                
//                logService.log(fetchTransactionRequest.toString());
//                
//                fetchScheduler.scheduleTransactions(fetchTransactionRequest);
//            }
//            
//            log = new Log();
//            log.setAction("FETCH FLUTTERWAVE TRANSACTION");
//            log.setCreatedOn(new Date());
//            log.setDescription("fetching transaction from flutterwave completed");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//        } catch (Exception ex) {
//            Logger.getLogger(TransactionFetcherService.class.getName()).log(Level.SEVERE, null, ex);
//            
//            try{
//                log = new Log();
//                log.setAction("FETCH FLUTTERWAVE TRANSACTION");
//                log.setCreatedOn(new Date());
//                log.setDescription("fetching flutterwave transactions");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                
//                if(ex != null){
////                    log.setStatusMessage(ex.getMessage());
//                    
//                    String detailedString = Stream.of(ex.getStackTrace())
//                            .map(x -> x.toString()).collect(Collectors.joining(","));
//                    logService.log(log, detailedString);
//                }
//                else
//                    logService.log(log);
//            }catch(Exception e){
//            }
//        }
//    }
//    
//    @Transactional
//    private void logTransactions(List<FlutterTransaction> transactions, FetchTransactionStatus status) 
//            throws DatabaseException{
//        
//        status.setSize(transactions.size());
//        status.setStatus(FetchStatus.SUCCESSFUL);
//
//        fetchTransactionStatusDao.create(status);
//
//        if(transactions.size() > 0)
//            flutterTransactionDao.create(transactions);
//            
//    }
//    public void fetchTransaction(FetchTransactionRequest request){
//        FetchTransactionStatus status = new FetchTransactionStatus();
//        
//        TransactionUtility.buildWrapperClass();
//        
//        Log log = new Log();
//        
//        try {
//            String date = dateFormat.format(new Date());
//            
//            log.setAction("FETCH FLUTTERWAVE TRANSACTION STARTED");
//            log.setCreatedOn(new Date());
//            log.setDescription("Fetching transaction from flutterwave started");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//            status.setAttempt(1);
//            status.setCreatedOn(new Date());
//            status.setStartDate(request.getStartDate());
//            status.setEndDate(request.getEndDate());
//                    
//            List<FlutterTransaction> transactions = HttpUtil.getFlutterTransactions(request.getStartDate(), request.getEndDate(),request.getStart(),BATCH_SIZE);
//            
//            status.setSize(transactions.size());
//            status.setStatus(FetchStatus.SUCCESSFUL);
//            
//            logTransactions(transactions, status);
//            
//            if(transactions.size() >= BATCH_SIZE){
//                FetchTransactionRequest fetchTransactionRequest = new FetchTransactionRequest();
//                fetchTransactionRequest.setEndDate(request.getEndDate());
//                fetchTransactionRequest.setStartDate(request.getStartDate());
//                fetchTransactionRequest.setStart(request.getStart()+BATCH_SIZE);
//                fetchTransactionRequest.setLimit(BATCH_SIZE);
//                
//                logService.log(fetchTransactionRequest.toString());
//                
//                fetchScheduler.scheduleTransactions(fetchTransactionRequest);
//            }
//            
//            if(request.getTransactionBatch() != null && ! "".equalsIgnoreCase(request.getTransactionBatch())){
//                
//                ResolveTransactionRequest rtr = new ResolveTransactionRequest();
//                rtr.setBatchId(request.getTransactionBatch());
//                rtr.setCreatedOn(new Date());
//                fetchScheduler.scheduleTransactions(rtr);
//            }
//            
//            log = new Log();
//            log.setAction("FETCH FLUTTERWAVE TRANSACTION");
//            log.setCreatedOn(new Date());
//            log.setDescription("fetching transaction from flutterwave completed");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//        } catch (DatabaseException ex) {
//            Logger.getLogger(TransactionFetcherService.class.getName()).log(Level.SEVERE, null, ex);
//            
//            try{
//                log = new Log();
//                log.setAction("FETCH FLUTTERWAVE TRANSACTION");
//                log.setCreatedOn(new Date());
//                log.setDescription("creation country");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                
//                if(ex != null){
////                    log.setStatusMessage(ex.getMessage());
//                    
//                    String detailedString = Stream.of(ex.getStackTrace())
//                            .map(x -> x.toString()).collect(Collectors.joining(","));
//                    logService.log(log, detailedString);
//                }
//                else
//                    logService.log(log);
//            }catch(Exception e){
//            }
//        }
//    }
//    
//    @Asynchronous
//    public void queueFetchTransaction(String startDate, String endDate, long delay, String batch){
////        FetchTransactionStatus status = new FetchTransactionStatus();
////        
////        TransactionUtility.buildWrapperClass();
////        
//        Log log = new Log();
//        
//        try {
//            
//            log.setAction("FETCH FLUTTERWAVE TRANSACTION  SCHEDULING STARTED");
//            log.setCreatedOn(new Date());
//            log.setDescription("Fetching transaction from flutterwave started");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//            FetchTransactionRequest request = new FetchTransactionRequest();
//            request.setEndDate(endDate);
//            request.setStartDate(startDate);
//            request.setStart(0);
//            request.setLimit(BATCH_SIZE);
//            request.setDoSettlement(true);
//            request.setSchedule(delay);
//            request.setTransactionBatch(batch);
//            
//            fetchScheduler.scheduleTransactions(request);
//            
//            log = new Log();
//            log.setAction("FETCH FLUTTERWAVE TRANSACTION SCHEDULING");
//            log.setCreatedOn(new Date());
//            log.setDescription("fetching transaction from flutterwave completed");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//        } catch (Exception ex) {
//            Logger.getLogger(TransactionFetcherService.class.getName()).log(Level.SEVERE, null, ex);
//            
//            try{
//                log = new Log();
//                log.setAction("FETCH FLUTTERWAVE TRANSACTION SCHEDULING");
//                log.setCreatedOn(new Date());
//                log.setDescription("creation country");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                
//                if(ex != null){
////                    log.setStatusMessage(ex.getMessage());
//                    
//                    String detailedString = Stream.of(ex.getStackTrace())
//                            .map(x -> x.toString()).collect(Collectors.joining(","));
//                    logService.log(log, detailedString);
//                }
//                else
//                    logService.log(log);
//            }catch(Exception e){
//            }
//        }
//    }
//    
//    public void resolveTransaction(ResolveTransactionRequest request){
//        FetchTransactionStatus status = new FetchTransactionStatus();
//        
//        TransactionUtility.buildWrapperClass();
//        
//        Log log = new Log();
//        
//        try {
//            String date = dateFormat.format(new Date());
//            
//            log.setAction("RESOLVING TRANSACTION STARTED");
//            log.setCreatedOn(new Date());
//            log.setDescription("Fetching transaction from flutterwave started");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//            if(request.getBatchId()!= null && ! "".equalsIgnoreCase(request.getBatchId())){
//                
//                ResolveTransactionRequest rtr = new ResolveTransactionRequest();
//                rtr.setBatchId(request.getBatchId());
//                rtr.setCreatedOn(new Date());
//                fetchScheduler.scheduleTransactions(rtr);
//            }
//            
//            log = new Log();
//            log.setAction("RESOLVING TRANSACTION");
//            log.setCreatedOn(new Date());
//            log.setDescription("fetching transaction from flutterwave completed");
//            log.setLevel(LogLevel.Info);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setStatusMessage(LogStatus.SUCCESSFUL.name());
//            logService.log(log);
//            
//        } catch (Exception ex) {
//            Logger.getLogger(TransactionFetcherService.class.getName()).log(Level.SEVERE, null, ex);
//            
//            try{
//                log = new Log();
//                log.setAction("RESOLVING TRANSACTION");
//                log.setCreatedOn(new Date());
//                log.setDescription("resolving country");
//                log.setLevel(LogLevel.Severe);
//                log.setStatus(LogStatus.FAILED);
//                
//                if(ex != null){
////                    log.setStatusMessage(ex.getMessage());
//                    
//                    String detailedString = Stream.of(ex.getStackTrace())
//                            .map(x -> x.toString()).collect(Collectors.joining(","));
//                    logService.log(log, detailedString);
//                }
//                else
//                    logService.log(log);
//            }catch(Exception e){
//            }
//        }
//    }
//}

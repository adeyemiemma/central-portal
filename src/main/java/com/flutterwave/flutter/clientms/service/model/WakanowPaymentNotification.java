/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanueladeyemi
 */
@XmlRootElement(name = "Payments" )
public class WakanowPaymentNotification {

    /**s
     * @return the item
     */
    @XmlElement(name = "Payment")
    public WakanowPaymentResponseItem getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(WakanowPaymentResponseItem item) {
        this.item = item;
    }

    /**
     * @return the statusMessage
     */
    @XmlElement(name = "StatusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage the statusMessage to set
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
    
//    @XmlElement(name = "Payment")
    private WakanowPaymentResponseItem item;
    private String statusMessage;
}

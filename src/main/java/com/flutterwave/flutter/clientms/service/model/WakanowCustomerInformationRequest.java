/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@XmlRootElement(name = "CustomerInformationRequest")
public class WakanowCustomerInformationRequest {

    /**
     * @return the version
     */
    @XmlAttribute(name = "Version")
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the hash
     */
    @XmlElement(name = "Hash")
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the paymentItemCode
     */
    @XmlElement(name = "PaymentItemCode")
    public String getPaymentItemCode() {
        return paymentItemCode;
    }

    /**
     * @param paymentItemCode the paymentItemCode to set
     */
    public void setPaymentItemCode(String paymentItemCode) {
        this.paymentItemCode = paymentItemCode;
    }

    /**
     * @return the service
     */
    @XmlElement(name = "Service")
    public String getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return the merchantReference
     */
    @XmlElement(name = "MerchantReference")
    public String getMerchantReference() {
        return merchantReference;
    }

    /**
     * @param merchantReference the merchantReference to set
     */
    public void setMerchantReference(String merchantReference) {
        this.merchantReference = merchantReference;
    }

    /**
     * @return the custReference
     */
    @XmlElement(name = "CustReference")
    public String getCustReference() {
        return custReference;
    }

    /**
     * @param custReference the custReference to set
     */
    public void setCustReference(String custReference) {
        this.custReference = custReference;
    }
    
    private String paymentItemCode;
    private String service;
    private String merchantReference;
    private String custReference;
    private String hash;
    private String version = "2";

    @Override
    public String toString() {
        return new JSONObject(this).toString();
    }
    
    
}

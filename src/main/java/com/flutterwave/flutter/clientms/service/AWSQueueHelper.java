/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.Message;
import com.flutterwave.flutter.clientms.dao.AirtimeTransactionDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.dao.TransactionNewDao;
import com.flutterwave.flutter.clientms.dao.TransactionWarehouseDao;
//import com.flutterwave.flutter.clientms.dao.TransactionNewDao;
//import com.flutterwave.flutter.clientms.dao.TransactionWarehouseDao;
import com.flutterwave.flutter.clientms.model.AirtimeTransaction;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.TransactionNew;
import com.flutterwave.flutter.clientms.model.TransactionWarehouse;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.StringReader;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.AccessTimeout;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 *
 * @author emmanueladeyemi
 */
@Singleton
@AccessTimeout(value=60000)
public class AWSQueueHelper {
    
    @EJB
    private LogService logService;
    @EJB
    private TransactionDao transactionDao;
    @EJB
    private AirtimeTransactionDao airtimeTransactionDao;
    @EJB
    private TransactionNewDao transactionNewDao;
    @EJB
    private TransactionWarehouseDao transactionWarehouseDao;
    
    boolean foundRRN = false, isDisbursement, isDisbursementMessage;
    boolean foundSourceIdentifier = false,foundTargetIdentifier = false;
    boolean transactresultresponsecode = false, clienttxnref = false, reverseresponsecode = false;
    boolean reverseresponsemessage = false;
    
    
//    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)    
    public void onMessage(com.amazonaws.services.sqs.model.Message message, String url, AmazonSQS sqs) {

        try {
            
//            if (message instanceof TextMessage) {

                String text = message.getBody();
//                System.out.println("Received Success T : " + text);
//
                Log log;
//                log.setAction("Success Transaction Queue Called back SQS");
//                log.setCreatedOn(new Date());
//                log.setLevel(LogLevel.Info);
//                log.setLogDomain(LogDomain.ADMIN);
//                log.setDescription(message.toString());
//                log.setLogState(LogState.STARTED);
//                log.setStatus(LogStatus.SUCCESSFUL);
//
////            log.setLogDomainId(0);
//                logService.log(log);

                JsonReader jReader = Json.createReader(new StringReader(text));
                JsonObject jsonObject = jReader.readObject();

                String type = jsonObject.getString("transactiontype", null);

                if ("AIRTIME".equalsIgnoreCase(type)) {

                    AirtimeTransaction transaction = new AirtimeTransaction();
                    
                    transaction.setProvider((String) jsonObject.getString("sourceid", null));
                    transaction.setAmount(Double.parseDouble(jsonObject.getString("transactionamount", "0.0") + ""));
                    transaction.setAuthenticationModel((String) jsonObject.getString("authenticationmodel", null));
                    transaction.setCardCountry(jsonObject.getString("cardcountry", "NG") + "");
                    transaction.setCardMask((String) jsonObject.getString("cardmask", null));
                    transaction.setCreatedOn(new Date());
                    transaction.setDatetime((String) jsonObject.getString("datetime", null));
                    transaction.setResponseCode((String) jsonObject.getString("responsecode", null));
                    transaction.setResponseMessage((String) jsonObject.getString("responsemessage", null));
                    transaction.setTransactionNarration((String) jsonObject.getString("transactionnarration", null));
                    transaction.setTransactionType((String) jsonObject.getString("transactiontype", null));
                    transaction.setFlwTxnReference((String) jsonObject.getString("flutterwavetransactionreference", null));
                    transaction.setMerchantTxnReference((String) jsonObject.getString("merchanttranscationreference", null));
                    transaction.setTransactionCountry((String) jsonObject.getString("transactioncountry", null));
                    transaction.setTransactionCurrency((String) jsonObject.getString("transactioncurrency", null));
                    transaction.setMerchantId((String) jsonObject.getString("merchantid", null));
                    transaction.setCardScheme((String) jsonObject.getString("cardscheme", null));
                    
                    JsonArray listData = jsonObject.getJsonArray("processorreference");
                    
                    if (listData != null) {

                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

//                        foundRRN = false;
                        listData.stream().forEach(x -> {
                            JsonObject value = (JsonObject) x;

                            JsonObjectBuilder builder = Json.createObjectBuilder();

                            foundSourceIdentifier = false;
                            foundTargetIdentifier = false;
                            transactresultresponsecode = false;
                            foundRRN = false;
                            clienttxnref = false; reverseresponsecode = false;
                            reverseresponsemessage = false;

                            value.entrySet().stream().forEach(y -> {

                                String str = String.valueOf(y.getValue()).replaceAll("\"", "");
                                builder.add(y.getKey(), str);

                                if (foundRRN == true) {
                                    transaction.setRrn(str);
                                    foundRRN = false;
                                }
                                if (str.equalsIgnoreCase("retrievalreferencenumber")) {
                                    foundRRN = true;
                                }

                                if (foundSourceIdentifier == true) {
                                    transaction.setSourceIdentifier(str);
                                    foundSourceIdentifier = false;
                                }
                                
                                if (str.equalsIgnoreCase("sourceidentifier")) {
                                    foundSourceIdentifier = true;
                                }
                                
                                if (foundTargetIdentifier == true) {
                                    transaction.setTargetIdentifier(str);
                                    foundTargetIdentifier = false;
                                }
                                
                                if (str.equalsIgnoreCase("targetidentifier")) {
                                    foundTargetIdentifier = true;
                                }
                                
                                if (transactresultresponsecode == true) {
                                    transaction.setTransactResultResponseCode(str);
                                    transactresultresponsecode = false;
                                }
                                
                                if (str.equalsIgnoreCase("transactresultresponsecode")) {
                                    transactresultresponsecode = true;
                                }
                                
                                if (clienttxnref == true) {
                                    transaction.setClientTxnRef(str);
                                    clienttxnref = false;
                                }
                                
                                if (str.equalsIgnoreCase("clienttxnref")) {
                                    clienttxnref = true;
                                }

                                if (reverseresponsecode == true) {
                                    transaction.setReverseResponseCode(str);
                                    reverseresponsecode = false;
                                }
                                
                                if (str.equalsIgnoreCase("reverseresponsecode")) {
                                    reverseresponsecode = true;
                                }

                                if (reverseresponsemessage == true) {
                                    transaction.setReverseResponseMessage(str);
                                    reverseresponsemessage = false;
                                }
                                
                                if (str.equalsIgnoreCase("reverseresponsemessage")) {
                                    reverseresponsemessage = true;
                                }
                            });

                            arrayBuilder.add(builder.build());
                        });
                        transaction.setProcessorReference(arrayBuilder.build().toString());
                    }
                    
                    listData = jsonObject.getJsonArray("transactiondata");
//                    listData = (List<Object>)jsonObject.getOrDefault("transactionjsonObject",null);

                    if (listData != null) {

                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                        listData.stream().forEach(x -> {
                            JsonObject value = (JsonObject) x;

                            JsonObjectBuilder builder = Json.createObjectBuilder();

                            value.entrySet().stream().forEach(y -> {
                                builder.add(y.getKey(), String.valueOf(y.getValue()).replaceAll("\"", ""));
                            });

                            arrayBuilder.add(builder.build());
                        });

//                        String str = arrayBuilder.toString();

                        transaction.setTransactionData(arrayBuilder.build().toString());
                    }

                    AirtimeTransaction txn = airtimeTransactionDao.findByKey("flwTxnReference", transaction.getFlwTxnReference());

                    if (txn == null) {
                        airtimeTransactionDao.create(transaction);
                        log = new Log();
                        log.setAction(" Transaction Queue Called back");
                        log.setCreatedOn(new Date());
                        log.setLevel(LogLevel.Info);
                        log.setLogDomain(LogDomain.ADMIN);
                        log.setDescription("successful callback queue processed for " + transaction.getFlwTxnReference());
                        log.setLogState(LogState.FINISH);
                        log.setStatus(LogStatus.SUCCESSFUL);

                        logService.log(log);
                    } else {

                        if ("00".equalsIgnoreCase(txn.getResponseCode()) && "00".equalsIgnoreCase(transaction.getResponseCode())) {

                            log = new Log();
                            log.setAction(" Transaction Queue Called back");
                            log.setCreatedOn(new Date());
                            log.setLevel(LogLevel.Info);
                            log.setLogDomain(LogDomain.ADMIN);
                            log.setDescription("updating with successful for transaction " + transaction.getFlwTxnReference());
                            log.setLogState(LogState.FINISH);
                            log.setStatus(LogStatus.SUCCESSFUL);

                            transaction.setId(txn.getId());

                            airtimeTransactionDao.update(transaction);
                        } else {
                            log = new Log();
                            log.setAction(" Transaction Queue Called back");
                            log.setCreatedOn(new Date());
                            log.setLevel(LogLevel.Info);
                            log.setLogDomain(LogDomain.ADMIN);
                            log.setDescription("Duplicate reference for transaction " + transaction.getFlwTxnReference());
                            log.setLogState(LogState.FINISH);
                            log.setStatus(LogStatus.SUCCESSFUL);
                        }

                        logService.log(log);
                    }
                    
                } else {

                    Transaction transaction = new Transaction();

//                    Provider provider = providerDao.findByKey("shortName", (String)jsonObject.getString("sourceid", null));
                    transaction.setProvider((String) jsonObject.getString("sourceid", null));
                    
                    
                    String amount = jsonObject.getString("transactionamount", "0.0");
                    
                    if("null".equalsIgnoreCase(amount))
                         transaction.setAmount(0.00);
                    else
                        transaction.setAmount(Double.parseDouble(amount));
                    
                    
                    transaction.setAuthenticationModel((String) jsonObject.getString("authenticationmodel", null));
                    transaction.setCardCountry(jsonObject.getString("cardcountry", "NG") + "");
                    transaction.setCardMask((String) jsonObject.getString("cardmask", null));
                    transaction.setCreatedOn(new Date());
                    transaction.setDatetime((String) jsonObject.getString("datetime", null));
                    transaction.setResponseCode((String) jsonObject.getString("responsecode", null));
                    transaction.setResponseMessage((String) jsonObject.getString("responsemessage", null));
                    transaction.setTransactionNarration((String) jsonObject.getString("transactionnarration", null));
                    transaction.setTransactionType((String) jsonObject.getString("transactiontype", null));
                    transaction.setFlwTxnReference((String) jsonObject.getString("flutterwavetransactionreference", null));
                    transaction.setMerchantTxnReference((String) jsonObject.getString("merchanttranscationreference", null));
                    transaction.setTransactionCountry((String) jsonObject.getString("transactioncountry", null));
                    transaction.setTransactionCurrency((String) jsonObject.getString("transactioncurrency", null));
                    transaction.setMerchantId((String) jsonObject.getString("merchantid", null));
                    transaction.setCardScheme((String) jsonObject.getString("cardscheme", null));
                    transaction.setSettlementState(Utility.SettlementState.NONE);

                    JsonArray listData = jsonObject.getJsonArray("processorreference");

                    if (listData != null) {

                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

//                        foundRRN = false;
                        listData.stream().forEach(x -> {
                            JsonObject value = (JsonObject) x;

                            JsonObjectBuilder builder = Json.createObjectBuilder();

                            foundRRN = false;
                            isDisbursement = false;
                            isDisbursementMessage = false;

                            value.entrySet().stream().forEach(y -> {

                                String str = String.valueOf(y.getValue()).replaceAll("\"", "");
                                builder.add(y.getKey(), str);

                                if (foundRRN == true) {
                                    transaction.setRrn(str);
                                    foundRRN = false;
                                }
                                if (str.equalsIgnoreCase("retrievalreferencenumber")) {
                                    foundRRN = true;
                                }

                                if (isDisbursement == true) {
                                    transaction.setDisburseResponseCode(str);
                                    isDisbursement = false;
                                }

                                if (str.equalsIgnoreCase("postresponsecode")) {
                                    isDisbursement = true;
                                }

                                if (isDisbursementMessage == true) {
                                    transaction.setDisburseResponseMessage(str);
                                    isDisbursementMessage = false;
                                }

                                if (str.equalsIgnoreCase("postresponsemessage")) {
                                    isDisbursementMessage = true;
                                }
                            });

                            arrayBuilder.add(builder.build());
                        });
                        transaction.setProcessorReference(arrayBuilder.build().toString());
                    }

                    listData = jsonObject.getJsonArray("transactiondata");
//                    listData = (List<Object>)jsonObject.getOrDefault("transactionjsonObject",null);

                    if (listData != null) {

                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                        listData.stream().forEach(x -> {
                            JsonObject value = (JsonObject) x;

                            JsonObjectBuilder builder = Json.createObjectBuilder();

                            value.entrySet().stream().forEach(y -> {
                                builder.add(y.getKey(), String.valueOf(y.getValue()).replaceAll("\"", ""));
                            });

                            arrayBuilder.add(builder.build());
                        });

//                        String str = arrayBuilder.toString();

                        transaction.setTransactionData(arrayBuilder.build().toString());
                    }

                    Transaction txn = transactionDao.findByKey("flwTxnReference", transaction.getFlwTxnReference());

                    if (txn == null) {
                        transactionDao.create(transaction);
                        log = new Log();
                        log.setAction(" Transaction Queue Called back");
                        log.setCreatedOn(new Date());
                        log.setLevel(LogLevel.Info);
                        log.setLogDomain(LogDomain.ADMIN);
                        log.setDescription("successful callback queue processed for " + transaction.getFlwTxnReference());
                        log.setLogState(LogState.FINISH);
                        log.setStatus(LogStatus.SUCCESSFUL);

                        logService.log(log);
                    } else {

                        if ("00".equalsIgnoreCase(transaction.getResponseCode())) {

                            log = new Log();
                            log.setAction(" Transaction Queue Called back");
                            log.setCreatedOn(new Date());
                            log.setLevel(LogLevel.Info);
                            log.setLogDomain(LogDomain.ADMIN);
                            log.setDescription("Replacing duplicate with successful for transaction " + transaction.getFlwTxnReference());
                            log.setLogState(LogState.FINISH);
                            log.setStatus(LogStatus.SUCCESSFUL);

                            transaction.setId(txn.getId());

                            transactionDao.update(transaction);
                        } else {
                            log = new Log();
                            log.setAction(" Transaction Queue Called back");
                            log.setCreatedOn(new Date());
                            log.setLevel(LogLevel.Info);
                            log.setLogDomain(LogDomain.ADMIN);
                            log.setDescription("Duplicate reference for transaction " + transaction.getFlwTxnReference());
                            log.setLogState(LogState.FINISH);
                            log.setStatus(LogStatus.SUCCESSFUL);
                        }

                        logService.log(log);
                    }
                    
                    asyncLog(text);
                }

//            } 
            deleteMessage(message, url, sqs);

        } catch (Exception ex) {
            if (ex != null) {
                ex.printStackTrace();
            }

            throw new RuntimeException(ex);
        }

    }
    
    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void asyncLog(String data) throws DatabaseException{
        
//        try {
            TransactionNew transactionNew = TransactionUtility.buildTransaction(data);
            
            TransactionNew transaction = transactionNewDao.findByKey("flwTxnReference", transactionNew.getFlwTxnReference());
            if(transaction == null){
                transactionNewDao.create(transactionNew);
            }else{
                transactionNew.setId(transaction.getId());
                transactionNew.setModifiedOn(new Date());
                transactionNewDao.update(transactionNew);
            }
            
            TransactionWarehouse transactionWarehouse = TransactionUtility.buildTransactionWH(data);
            
            
            TransactionWarehouse txn = transactionWarehouseDao.findByKey("flwTxnReference", transactionWarehouse.getFlwTxnReference());
            
            if(txn == null){
                transactionWarehouseDao.create(transactionWarehouse);
            }else{
                transactionWarehouse.setId(txn.getId());
                transactionWarehouse.setModifiedOn(new Date());
                transactionWarehouseDao.update(transactionWarehouse);
            }
            
            
//        } catch (Exception ex) {
//            Logger.getLogger(AWSQueueHelper.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
    
    @Lock(LockType.READ)
    public void deleteMessage(Message message, String queueUrl, AmazonSQS async){
        
    
        DeleteMessageResult messageResult = async.deleteMessage(new DeleteMessageRequest(queueUrl, message.getReceiptHandle()));
        
        Log log;
        
        if(messageResult == null){
            log = new Log();
            log.setAction("Deleting message from queue");
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setDescription("Unable to delete message from queue");
            log.setLogState(LogState.FINISH);
            log.setStatus(LogStatus.SUCCESSFUL);
        }else{
            log = new Log();
            log.setAction("Deleting message from queue");
            log.setCreatedOn(new Date());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setDescription(messageResult.toString());
            log.setLogState(LogState.FINISH);
            log.setStatus(LogStatus.SUCCESSFUL);
        }
        
        logService.log(log);
    }
}

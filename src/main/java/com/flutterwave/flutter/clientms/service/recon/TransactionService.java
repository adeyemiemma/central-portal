/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.recon;

import com.flutterwave.flutter.clientms.controller.api.model.recon.DashboardReport;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.dao.recon.BatchDao;
import com.flutterwave.flutter.clientms.dao.recon.BatchResolvedDao;
import com.flutterwave.flutter.clientms.dao.recon.FlutterTransactionDao;
import com.flutterwave.flutter.clientms.dao.recon.ReconTransactionDao;
import com.flutterwave.flutter.clientms.model.recon.Batch;
import com.flutterwave.flutter.clientms.model.recon.BatchResolved;
import com.flutterwave.flutter.clientms.model.recon.FlutterTransaction;
import com.flutterwave.flutter.clientms.model.recon.ReconTransaction;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.jboss.ejb3.annotation.TransactionTimeout;

/**
 *
 * @author adeyemi
 */
@Stateless
public class TransactionService {

    @EJB
    private ReconTransactionDao transactionDao;
    @EJB
    private BatchDao batchDao;
    @EJB
    private FlutterTransactionDao flutterTransactionDao;
    @EJB
    private BatchResolvedDao batchResolvedDao;
    @EJB
    private TransactionDao coreTransactionDao; 

    public Map<String, Object> getDashboardSummary() {

        Map<String, Object> data = new HashMap<>();

        double[] sumSettledVisa = transactionDao.getSum(null, null, null, "Visa", true);

        double[] sumSettledMC = transactionDao.getSum(null, null, null, "MasterCard", true);

        double[] sumUnsettledVisa = transactionDao.getSum(null, null, null, "Visa", true);

        double[] sumUnsettledMC = transactionDao.getSum(null, null, null, "MasterCard", true);

        data.put("sumSVisa", sumSettledVisa);
        data.put("sumSMC", sumSettledMC);

        data.put("sumUVisa", sumUnsettledVisa);
        data.put("sumUMC", sumUnsettledMC);

        data.put("total", transactionDao.getSum(null, null, null, null, null));

        long countSVisa = transactionDao.getTransactionCount(null, null, null, "Visa", true);
        long countUVisa = transactionDao.getTransactionCount(null, null, null, "Visa", false);
        long countSMC = transactionDao.getTransactionCount(null, null, null, "MasterCard", true);
        long countUMC = transactionDao.getTransactionCount(null, null, null, "MasterCard", false);

        data.put("visaSCount", countSVisa);
        data.put("visaUCount", countUVisa);
        data.put("mcSCount", countSMC);
        data.put("mcUCount", countUMC);
        data.put("totalCount", transactionDao.getTransactionCount(null, null, null, null, null));
        data.put("currencies", getCurrenciesCount());

        return data;
    }

    public long getCurrenciesCount() {

        try {
            Map<String, Object> response = transactionDao.getCountQuery("select count(distinct(originalCurrency)) as count from transaction");

            Object obj = response.get("result");

            List l = (List) obj;

            return Long.parseLong(l.get(0) + "");
        } catch (Exception ex) {

            if (ex != null) {
                ex.printStackTrace();
            }
        }

        return 0;
    }

    public Map<String, Object> getCurrencyAnalysis(Date startDate, Date endDate) {

        String query = " where id <> 0";

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if (startDate != null) {
            query += " date(operationDate) >= date('" + dateFormat.format(startDate) + "')";
        }

        if (endDate != null) {
            query += " and date(operationDate) <= date('" + dateFormat.format(endDate) + "')";
        }

        Map<String, Object> originCurrencyAnalysis = transactionDao.getCountQuery("select originClearanceCurrency,sum(originClearanceAmount) as amount from transaction " + query + " group by originClearanceCurrency");

        Map<String, Object> currencyState = new HashMap<>();

        currencyState.put("origin", processD(originCurrencyAnalysis));

        Map<String, Object> destCurrency = transactionDao.getCountQuery("select destinationClearanceCurrency, sum(destinationClearanceAmount) as amount from transaction " + query + " group by destinationClearanceCurrency");

        currencyState.put("destination", processD(destCurrency));

        return currencyState;
    }

    private Map<String, Object> processD(Map originCurrencyAnalysis) {

        Object object = originCurrencyAnalysis.get("result");

        Map<String, Object> map = new HashMap<>();
        if (object != null) {
            List list = (List) object;

            if (!list.isEmpty()) {

                List mainList = new ArrayList<>();

                for (Object obj : list) {

                    List temp = new ArrayList();
                    Object[] l = (Object[]) obj;

                    temp.add(TransactionUtility.getCurrencyName(l[0].toString()));
                    temp.add(l[1].toString());

                    mainList.add(temp);
                }

                map.put("result", mainList);

            }
        }

        return map;
    }

//    @TransactionTimeout(value = 10, unit = TimeUnit.MINUTES)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public BatchResolved uploadData(List<ReconTransaction> transactions, Batch batch) throws DatabaseException {

        batch = batchDao.create(batch);
        
        try {

//            Batch batch = transactions.get(0).getBatch();
            BatchResolved batchResolved = new BatchResolved();
            batchResolved.setBatchId(batch.getBatchId());
            batchResolved.setCreatedOn(new Date());
            batchResolved.setCount(batch.getCount());

            List<ReconTransaction> reconList = new ArrayList<>();
            
            if (!transactions.isEmpty()) {

                long resolved = 0;

                for (ReconTransaction transaction : transactions) {

                    boolean status = coreTransactionDao.exists("rrn",transaction.getRrn());

                    if (status == true) {
                        transaction.setSettled(true);
                        transaction.setSettlementDate(new Date());
                        resolved++;
                    }
                    
                    transaction.setBatch(batch);

                    reconList.add(transaction);
                }

                transactionDao.createNew(reconList);
                
                batchResolved.setResolved(resolved);
            }

            batchResolved = batchResolvedDao.create(batchResolved);
            
            return batchResolved;
            
        }catch(Exception ex){
            if(ex != null)
                ex.printStackTrace();
        }
        
        return null;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Asynchronous
    public Future<BatchResolved> uploadDataBackground(List<ReconTransaction> transactions, Batch batch) throws DatabaseException {

        batch = batchDao.create(batch);
        
        try {

//            Batch batch = transactions.get(0).getBatch();
            BatchResolved batchResolved = new BatchResolved();
            batchResolved.setBatchId(batch.getBatchId());
            batchResolved.setCreatedOn(new Date());
            batchResolved.setCount(batch.getCount());

//            List<ReconTransaction> reconList = new ArrayList<>();
            
            if (!transactions.isEmpty()) {

                long resolved = 0;

                for (ReconTransaction transaction : transactions) {

                    boolean status = coreTransactionDao.exists("rrn",transaction.getRrn());

                    if (status == true) {
                        transaction.setSettled(true);
                        transaction.setSettlementDate(new Date());
                        resolved++;
                    }
                    
                    transaction.setBatch(batch);

//                    reconList.add(transaction);
                    
                    transactionDao.create(transaction);
                }
                
                batchResolved.setResolved(resolved);
            }

            batchResolved = batchResolvedDao.create(batchResolved);
            
            return new AsyncResult<>(batchResolved);
            
        }catch(Exception ex){
            if(ex != null)
                ex.printStackTrace();
        }
        
        return null;
    }

    public boolean uploadFlutterTransaction(List<FlutterTransaction> transactions,
            String startDate, String endDate) throws DatabaseException {

        return flutterTransactionDao.create(transactions);
    }
    
    @TransactionTimeout(value = 60, unit = TimeUnit.MINUTES)
    public BatchResolved resolveTransactions(List<ReconTransaction> transactions, Batch batch) {

        try {

//            Batch batch = transactions.get(0).getBatch();
            BatchResolved batchResolved = new BatchResolved();
            batchResolved.setBatchId(batch.getBatchId());
            batchResolved.setCreatedOn(new Date());
            batchResolved.setCount(batch.getCount());

            List<ReconTransaction> reconList = new ArrayList<>();
            
            if (!transactions.isEmpty()) {

                long resolved = 0;

                for (ReconTransaction transaction : transactions) {

                    boolean status = coreTransactionDao.exists("rrn",transaction.getRrn());

                    if (status != true) {
                        continue;
                    }

                    transaction.setSettled(true);
                    transaction.setSettlementDate(new Date());
                    resolved++;
                    
                    reconList.add(transaction);
                }

                if(!reconList.isEmpty())
                    transactionDao.update(reconList);
                
                batchResolved.setResolved(resolved);
            }

            batchResolved = batchResolvedDao.create(batchResolved);

            return batchResolved;

        } catch (DatabaseException ex) {
            Logger.getLogger(TransactionService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public DashboardReport getDashboardReport(Date startDate, Date endDate, String currency, String bank) {

        List list = transactionDao.getSummary(startDate, endDate, TransactionUtility.getCurrencyCode(currency), bank, null);

        Object[] obj = (Object[]) list.get(0);

        DashboardReport report = new DashboardReport();

        report.setCards(transactionDao.getCardCount(startDate, endDate, TransactionUtility.getCurrencyCode(currency), bank));
        report.setTransactionValue(Double.parseDouble(obj[0] == null ? "0" : obj[0].toString()));
        report.setTransactionVolume(Long.parseLong(obj[1] == null ? "0" : obj[1].toString()));
        report.setMerchant(Long.parseLong(obj[3] == null ? "0" : obj[3].toString()));
        List data = transactionDao.getWorthSettlement(startDate, endDate, bank, TransactionUtility.getCurrencyCode(currency), null);
        
        if(!data.isEmpty()){
            Object[] objArray = (Object[]) data.get(0);
            report.setTransactionSettledValue(Double.parseDouble(objArray[0] == null ? "0" : objArray[0].toString()));
        }
        
        return report;
    }

    public Map<Date, Double> getTransactionTrend(Date startDate, Date endDate, String currency, String bank, String merchant) {

        List transactions = transactionDao.getTrend(startDate, endDate, TransactionUtility.getCurrencyCode(currency), bank, merchant);


        Map<Date, Double> params = new TreeMap<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYY");

        for (Object obj : transactions) {

            Object[] list = (Object[]) obj;

            Double amount = (Double) list[0];
            Date date = (Date) list[1];
            BigInteger count = (BigInteger) list[2];

            Double amountD = params.getOrDefault(date, 0.0);

            amountD = amountD + (amount == null ? 0 : amount.longValue());

            params.put(date, amountD);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        Date tempDate = startDate;

        while (tempDate.before(endDate) || tempDate.equals(endDate)) {

            Double m = params.getOrDefault(tempDate, null);

            if (m == null) {
                m = 0.0;
                params.put(tempDate, m);
            }

            calendar.add(Calendar.DAY_OF_MONTH, 1);
            tempDate = calendar.getTime();
        }
        
        return params;
    }
}

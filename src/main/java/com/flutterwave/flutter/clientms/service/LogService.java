/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.LoggerDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

/**
 *
 * @author emmanueladeyemi
 */
@LocalBean
@Stateless
public class LogService{
    
//    private final static String LOGNAME = "client_ms";
//    private final static String LOGFOLDER = "client_ms_logs";
    private static SimpleDateFormat dateFormat;
//    private ScheduledFuture<?> future;
    private String fileName;
    
    Logger logger = Logger.getLogger(LogService.class);
    
    @EJB
    private LoggerDao loggerDao;
    
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public boolean log(Log log){
        
        FileOutputStream outputStream = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
            String date = new Date().toString();
            
            if(log.getCreatedOn() == null)
                log.setCreatedOn(new Date());
            
            log.setId(null);
            
            String messageToSave = date+" "+log.toString()+"\n";
//            
//           createOutputFolder();
            
//            File file = new File(LOGFOLDER+"/"+fileName);
//            
//           if(!file.exists())
//                file.createNewFile();
//            
//            outputStream = new FileOutputStream(file,true);
//            
//            outputStream.write(messageToSave.getBytes());

            logger.info(messageToSave);
//            outputStream.flush();
            
//            loggerDao.create(log);
            
            return true;
        }  catch (Exception ex) {
            logger.error("Error in log service", ex);
        } finally {
            try {
                if(outputStream != null)
                    outputStream.close();
            } catch (IOException ex) {
                logger.error("Error in log service", ex);
            }
        }
        
        return false;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public boolean log(Log log, String detailedDescription){
        
        FileOutputStream outputStream = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
            String date = new Date().toString();
            
            log.setId(null);
            
            if(log.getCreatedOn() == null)
                log.setCreatedOn(new Date());
            
//            createOutputFolder();
            
            String messageToSave = date+" "+log.toString()+"\n"+detailedDescription+"";
            
//            if(!file.exists())
//                file.createNewFile();
//
//            outputStream = new FileOutputStream(file,true);
//            
//            outputStream.write(messageToSave.getBytes());
//            outputStream.flush();

            logger.log(Priority.INFO, messageToSave);
            
//            loggerDao.create(log);
            
            return true;
        } catch (Exception ex) {
            logger.error("Error in log service", ex);
        } finally {
            try {
                if(outputStream != null)
                    outputStream.close();
            } catch (IOException ex) {
                logger.error("Error in log service", ex);
            }
        }
        
        return false;
    }
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public boolean log(String detailedDescription){
        
        FileOutputStream outputStream = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
            String date = new Date().toString();
            
//            log.setId(0L);
            
//            createOutputFolder();
            
            String messageToSave = date+" "+detailedDescription+"";
//            File file = new File(LOGFOLDER+"/"+fileName);
//            
//            if(!file.exists())
//                file.createNewFile();
//
//            outputStream = new FileOutputStream(file,true);
//            
//            outputStream.write(messageToSave.getBytes());
//            outputStream.flush();   

            logger.log(Priority.INFO, messageToSave);
            
            return true;
        } catch (Exception ex) {
            logger.error("Error in log service", ex);
        } finally {
            try {
                if(outputStream != null)
                    outputStream.close();
            } catch (IOException ex) {
                logger.error("Error in log service", ex);
            }
        }
        
        return false;
    }
    
//    private class FileManager implements Runnable{
//
//        @Override
//        public void run() {
//            
//            createOutputFolder();
//
//            dateFormat = new SimpleDateFormat("dd-MM-YYYY");
//
//            String currentDate = dateFormat.format(new Date());
//
//            fileName = LOGNAME+"_"+currentDate;
//
//            createFile();
//        }
        
//        private void createFile(){
//            
//            try {
//                if(fileName == null)
//                    throw new NullPointerException();
//                
//                File file = new File(LOGFOLDER +"/"+fileName);
//                
//                if(file.exists())
//                    return;
//                
//                boolean state = file.createNewFile();
//                
//                if(!state)
//                    throw new IOException("Unable to create file with name "+fileName);
//                
//                
//            } catch (IOException ex) {
//                java.util.logging.Logger.getLogger(LogService.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
//    
//    private boolean createOutputFolder(){
//        
//        try {
//            if(LOGFOLDER == null)
//                return false;
//            
//            File file =new File(LOGFOLDER);
//            
//            if(file.exists()){
//                
//                file.getAbsoluteFile().getAbsolutePath();
//                return true;
//            }
//            
//            Path path = Files.createDirectories(file.toPath(), new FileAttribute<?>[0]);
//            
//            if(path == null){
//                System.out.println("Unable to create files");
//                return false;
//            }
//            
//            //System.out.println("");
//            
//            return true;
//        } catch (IOException ex) {
//            
//        }
//        
//        return false;
//    }
}

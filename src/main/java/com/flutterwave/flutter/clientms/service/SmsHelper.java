/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.util.PageResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class SmsHelper {
    
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private ConfigurationDao configurationDao;        
    
    String url = "http://35.176.189.32:8080/notification/api/v1";
    
    
    public PageResult getTransactions(int start, int length, String sender, String status, 
            String startDate, String endDate, String search, String provider){
        
        int pageNo = start / length;
        
        JSONObject jSONObject = new JSONObject()
                .put("start_date_time", Utility.emptyToNull(startDate))
                .put("end_date_time", Utility.emptyToNull(endDate))
                .put("end_date_time", Utility.emptyToNull(endDate))
                .put("page_no", pageNo)
                .put("search", Utility.emptyToNull(search))
                .put("sender", Utility.emptyToNull(sender))
                .put("status", Utility.emptyToNull(status))
                .put("provider", Utility.emptyToNull(provider))
                .put("page_size", length);
        
        String response = httpUtil.doHttpPost(url+"/sms/all", jSONObject.toString(), getHeader());
        
        PageResult pageResult = new PageResult();
        
        if(response == null){
            
            pageResult.setData(new ArrayList());
            pageResult.setRecordsFiltered(0L);
            pageResult.setRecordsTotal(0L);
            
            return pageResult;
        }
        
        jSONObject = new JSONObject(response);
        
        JSONObject dataObject = jSONObject.getJSONObject("data");
        pageResult.setData(dataObject.getJSONArray("transactions").toList());
        pageResult.setRecordsFiltered(dataObject.getLong("total"));
        pageResult.setRecordsTotal(dataObject.getLong("total"));
            
        return pageResult;
    }
    
    public List<String> getStatus(){
        
        String response = httpUtil.doHttpGet(url+"/transaction/status", getHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");
        
        List<String> list = array.toList().stream().map(x -> x.toString()).collect(Collectors.toList());
        
        return list;
    }
    
    public List<String> getProvider(){
        
        String response = httpUtil.doHttpGet(url+"/provider", getHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");
        
        List<String> list = array.toList().stream().map(x -> x.toString()).collect(Collectors.toList());
        
        return list;
    }
    
    public List<String> getSender(){
        
        String response = httpUtil.doHttpGet(url+"/sender", getHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");
        
        List<String> list = array.toList().stream().map(x -> x.toString()).collect(Collectors.toList());
        
        return list;
    }
    
    public List getBalance(){
        
        String response = httpUtil.doHttpGet(url+"/provider/balance", getHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");

        return array.toList();
    }
    
    private Map<String, String> getHeader(){
        
        String key = configurationDao.getConfig("flw_sms_key");
        
        if(key == null){
            key = "7554713270654d5a724969697365514b6d71676d41595961657a69584b74674e2f4d6866302b7774597654656255487a736651447a6b31456a4367772f6f4d6f7361557636646d37437866565a5a7857795a45357a513d3d";
        }
        
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        header.put("Authorization", "Bearer "+key);
        
        return header;
    }
}

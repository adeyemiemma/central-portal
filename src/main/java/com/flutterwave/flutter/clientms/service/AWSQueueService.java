/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClient;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.model.AirtimeSettlement;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class AWSQueueService {
        
    @EJB
    private ConfigurationDao configurationDao;
    @Resource
    private ManagedExecutorService executorService;
            
    Logger logger = Logger.getLogger(AWSQueueService.class);
//    public void init(){
        
//    }
    
    @Asynchronous
    public void queueAirtimeTransaction(AirtimeSettlement airtimeSettlement){
        
        String url = configurationDao.getConfig("aws_airtime_queue");
        
        if(url == null){
            url = "https://sqs.eu-west-2.amazonaws.com";
        }
        
        String accessKey = configurationDao.getConfig("aws_access_key");
        
        if(accessKey == null)
            accessKey = "AKIAJ3CVNCSR5SKEUGYQ";
        
        String secretKey = configurationDao.getConfig("aws_secret_key");
        
        if(secretKey == null)
            secretKey = "wR2HYAdVQeJ0waLd9vzkAg4tNhFezsCcY3sRdur0";
        
        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        
        AmazonSQSAsync sqs = new AmazonSQSAsyncClient(credentials, executorService);
        
        sqs.setEndpoint(url);

        
        GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest("OLD_AWS_AIRTIME_SPLIT_QUEUE");
        
        GetQueueUrlResult queueUrlResult = sqs.getQueueUrl(getQueueUrlRequest);
        
        JSONObject jSONObject = new JSONObject();
        
        String narration = airtimeSettlement.getNarration();
        
        String[] data = narration.split("\\|");
        
        String indexData = data[data.length -1];
        
        String[] splittedIndexData = indexData.split("/");
        
        jSONObject.put("reservefundstxnref", data[0]);
        jSONObject.put("raastxnref", splittedIndexData[splittedIndexData.length - 1]);
        jSONObject.put("sourceidentifier", splittedIndexData[1]);
        jSONObject.put("targetidentifier", splittedIndexData[1]);
        jSONObject.put("purchaseamount", splittedIndexData[0]);
        jSONObject.put("responsecode", "00");
        jSONObject.put("responsemessage", "Approved Or Completed Successfully");
        jSONObject.put("transactresultresponsecode", "0000");
        jSONObject.put("reversestatus", 0);
        jSONObject.put("reverseretry", 0);
        jSONObject.put("splitstatus", 0);
        jSONObject.put("splitretry", 0);
        
        SendMessageRequest messageRequest = new SendMessageRequest(queueUrlResult.getQueueUrl(), jSONObject.toString());
        
        SendMessageResult messageResult = sqs.sendMessage(messageRequest);
        
        if(messageResult != null)
            logger.info(messageResult.toString());

    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanueladeyemi
 */
@XmlRootElement(name = "CustomerInformationResponse")
public class WakanowCustomerInformationResponse {

    /**
     * @return the merchantReference
     */
    @XmlElement(name = "MerchantReference")
    public String getMerchantReference() {
        return merchantReference;
    }

    /**
     * @param merchantReference the merchantReference to set
     */
    public void setMerchantReference(String merchantReference) {
        this.merchantReference = merchantReference;
    }

    /**
     * @return the wakanowCustomers
     */
    @XmlElementWrapper(name = "Customers")
    @XmlElement(name = "Customer")
    public List<WakanowCustomer> getWakanowCustomers() {
        return wakanowCustomers;
    }

    /**
     * @param wakanowCustomers the wakanowCustomers to set
     */
    public void setWakanowCustomers(List<WakanowCustomer> wakanowCustomers) {
        this.wakanowCustomers = wakanowCustomers;
    }
    
    private String merchantReference;
    private List<WakanowCustomer> wakanowCustomers;
    
}

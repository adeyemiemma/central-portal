/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.net.ssl.HttpsURLConnection;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

/**
 *
 * @author emmanueladeyemi
 */
public class NotificationService {
    
    //7c1e181ea5d65c2e48e9eb57ec2a04714c266fe7d42ade4f4817e3647f8cd98c
    static String url = "http://flutterwavestaging.com:10100/notification/api/mail/send";
    
    static Logger logger = Logger.getLogger(NotificationService.class);
    
    public static boolean sendComplianceSuccessEmail(String email, String name){
        
        
        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
            
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(50 * 1000);
            connection.setRequestProperty("Content-Type", "application/json");
            
            connection.setRequestProperty("apikey", "7c1e181ea5d65c2e48e9eb57ec2a04714c266fe7d42ade4f4817e3647f8cd98c");
            
            JsonObject object = Json.createObjectBuilder()
                    .add("name", name)
                    .add("email", email).build();
            
            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();
            
            os.write(postData);
            
            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();
            
            if(resultCode != 200 && resultCode != 201){
                throw new Exception("Server not available");
            }
            
            //            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            
            JsonObject response = Json.createReader(is).readObject();
            
            String responseCode = response.getString("responsecode", "99");
            
            if(! "00".equalsIgnoreCase(responseCode))
                return false;
            
            return true;
            
        } catch (Exception ex) {
            logger.error("Error in notification service", ex);
        }
        
        return false;
        
    }
    
}

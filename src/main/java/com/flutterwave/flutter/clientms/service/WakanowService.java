/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.model.PosTransactionNew;
import com.flutterwave.flutter.clientms.service.model.WakanowCustomer;
import com.flutterwave.flutter.clientms.service.model.WakanowCustomerInformationRequest;
import com.flutterwave.flutter.clientms.service.model.WakanowCustomerInformationResponse;
import com.flutterwave.flutter.clientms.service.model.WakanowPayment;
import com.flutterwave.flutter.clientms.service.model.WakanowPaymentItem;
import com.flutterwave.flutter.clientms.service.model.WakanowPaymentNotificationRequest;
import com.flutterwave.flutter.clientms.service.model.WakanowPaymentNotificationResponse;
import com.flutterwave.flutter.clientms.util.CryptoUtil;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.Utility;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class WakanowService {
    
    @EJB
    private HttpUtil httpUtil;
    
    
    private final static Logger LOGGER = Logger.getLogger(WakanowService.class.getName());
    
    public WakanowCustomerInformationResponse validateReference(String customerReference){
        
        LOGGER.info("*********** WAKANOW CUSTOMER QUERY SERVICE *****");
        
        Properties properties = Utility.getConfigProperty();
        
        String url = properties.getProperty("wakanow_url");
        
        String serviceCode = properties.getProperty("wakanow_service_code");
        String paymentCode = properties.getProperty("wakanow_payment_code");
        
        String hashKey = properties.getProperty("wakanow_hash_key");
        

        
        String merchantReference = properties.getProperty("wakanow_merchant_reference");
        
        WakanowCustomerInformationRequest customerInformationRequest = new WakanowCustomerInformationRequest();
        customerInformationRequest.setService(serviceCode);
        customerInformationRequest.setCustReference(customerReference);
        customerInformationRequest.setPaymentItemCode(paymentCode);
        customerInformationRequest.setMerchantReference(merchantReference);
        
        String hashParams = paymentCode+""+merchantReference+""+customerReference+""+hashKey;
                
        String hashValue = CryptoUtil.sha512(hashParams, null);
        
        customerInformationRequest.setHash(hashValue);
        customerInformationRequest.setVersion("2");
        
        
        String data = Utility.objectToString(customerInformationRequest, WakanowCustomerInformationRequest.class);
        
        LOGGER.info("REQUEST TO SERVER: "+data);
        
        
        Map<String, String> header = new HashMap<>();
        header.put("content-type", "text/xml");
        
        String response = httpUtil.doHttpPost(url, data, header);
        
        LOGGER.info("RESPONSE DATA "+response);
        
        if(response == null){
            
            
            LOGGER.info("No Response from the server");
            
            return null;
        }        
        

        WakanowCustomerInformationResponse  informationResponse;
        
        informationResponse = (WakanowCustomerInformationResponse) Utility.stringToObject(response, WakanowCustomerInformationResponse.class);
        
        
        LOGGER.info("*********** END WAKANOW CUSTOMER QUERY SERVICE *****");
        
        return informationResponse;
    }
    
    public WakanowPaymentNotificationResponse logPayment(PosTransactionNew posTransaction){
        
        LOGGER.info("*********** WAKANOW PAYMENT LOG SERVICE *****");
        
        WakanowCustomerInformationResponse customerInformationResponse = validateReference(posTransaction.getTraceref());
        
        if(customerInformationResponse == null){
            
            LOGGER.info("unable to log transaction as validation ");
            
            return null;
        }
        

        
        String alternateReference = customerInformationResponse.getWakanowCustomers().get(0).getCustomerReferenceAlternate();

        
        Properties properties = Utility.getConfigProperty();
        
        String url = properties.getProperty("wakanow_url");
        
//        String serviceCode = properties.getProperty("wakanow_service_code");
        String paymentCode = properties.getProperty("wakanow_payment_code");
        
        String hashKey = properties.getProperty("wakanow_hash_key");
        
//        String merchantReference = properties.getProperty("wakanow_merchant_reference");
        
        WakanowPayment wakanowPayment = new WakanowPayment();
        wakanowPayment.setAmount(String.valueOf(posTransaction.getAmount()));
        wakanowPayment.setAlternateCustReference(alternateReference);
        wakanowPayment.setPaymentLogId(posTransaction.getRefCode());
//        wakanowPayment.setBranchName(posTransaction.getAcquirerBank());
        wakanowPayment.setCustReference(posTransaction.getTraceref());
        wakanowPayment.setChannelName("WEB");
//        wakanowPayment.setDepositSlipNumber(posTransaction.getRrn());
//        wakanowPayment.setDepositorName(posTransaction.getCardholder());
        wakanowPayment.setBankName(posTransaction.getAcquirerBank());
        wakanowPayment.setPaymentCurrency("566");
        wakanowPayment.setPaymentCurrencyCode("NGN");
        wakanowPayment.setPaymentDate(Utility.formatDate(posTransaction.getRequestDate(), "MM/dd/yyyy HH:mm:ss"));
        wakanowPayment.setPaymentReference(posTransaction.getRefCode());
//        wakanowPayment.setPaymentMethod("Debit Card");
        wakanowPayment.setPaymentStatus("0");
        wakanowPayment.setCustomerName(posTransaction.getCardholder() == null ? "Customer" : posTransaction.getCardholder());
//        wak
        
        if(posTransaction.isReversed() == true)
            wakanowPayment.setReversal(Boolean.TRUE);
        else
            wakanowPayment.setReversal(Boolean.FALSE);
        
//        wakanowPayment.setTeller("Flutterwave");
        
        WakanowPaymentItem paymentItem = new WakanowPaymentItem();
        
        paymentItem.setItemAmount(String.valueOf(posTransaction.getAmount()));
        paymentItem.setItemCode(paymentCode);
//        paymentItem.setLeadBankName(posTransaction.getAcquirerBank());
        
        List<WakanowPaymentItem> wakanowPaymentItems = wakanowPayment.getPaymentItems();
        
        if(wakanowPaymentItems == null)
            wakanowPaymentItems = new ArrayList<>();
 
        wakanowPaymentItems.add(paymentItem);
                
        WakanowPaymentNotificationRequest notificationRequest = new WakanowPaymentNotificationRequest();
        
        notificationRequest.setVersion("2");
        
        wakanowPayment.setPaymentItems(wakanowPaymentItems);
        
        String hashParams=  wakanowPayment.getPaymentLogId() +""+wakanowPayment.getCustReference()+""+alternateReference
                +""+wakanowPayment.getAmount()+""+paymentItem.getItemCode()+""+paymentItem.getItemAmount()+""+wakanowPayment.getPaymentStatus()
                +""+wakanowPayment.getPaymentCurrency()+""+wakanowPayment.getPaymentCurrencyCode()+""+hashKey;
         
        String hashValue = CryptoUtil.sha512(hashParams, null);
        
        notificationRequest.setHash(hashValue);
        
        List<WakanowPayment> payments = notificationRequest.getItems();
        
        if(payments == null)
            payments = new ArrayList<>();
        
        payments.add(wakanowPayment);
        
        notificationRequest.setItems(payments);
        
        String data = Utility.objectToString(notificationRequest, WakanowPaymentNotificationRequest.class);
        
        LOGGER.info("REQUEST TO SERVER: "+data);
        
        
        Map<String, String> header = new HashMap<>();
        header.put("content-type", "text/xml");
        
        String response = httpUtil.doHttpPost(url, data, header);
        
        LOGGER.info("RESPONSE DATA "+response);
        
        if(response == null){
            
            
            LOGGER.info("No Response from the server");
            
            return null;
        }
        
        WakanowPaymentNotificationResponse  informationResponse = (WakanowPaymentNotificationResponse) Utility.stringToObject(response, WakanowPaymentNotificationResponse.class);
        
        
        LOGGER.info("*********** END WAKANOW PAYMENT NOTIFICATION SERVICE *****");
        
        return informationResponse;
    }
}

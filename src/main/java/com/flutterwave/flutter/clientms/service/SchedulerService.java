/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.controller.TransactionController;
import com.flutterwave.flutter.clientms.dao.AccountQueryDao;
import com.flutterwave.flutter.clientms.dao.AccountQueryTransactionDao;
import com.flutterwave.flutter.clientms.dao.AirtimeTransactionDao;
import com.flutterwave.flutter.clientms.dao.CallbackConfigurationDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationFeeDao;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CountryDao;
import com.flutterwave.flutter.clientms.dao.CurrencyDao;
import com.flutterwave.flutter.clientms.dao.ExchangeRateDao;
import com.flutterwave.flutter.clientms.dao.FlutterwaveSettlementDao;
import com.flutterwave.flutter.clientms.dao.InternationalTransactionDao;
import com.flutterwave.flutter.clientms.dao.MerchantFeeDao;
import com.flutterwave.flutter.clientms.dao.PosTerminalDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.ProviderDao;
import com.flutterwave.flutter.clientms.dao.RaveTransactionDao;
import com.flutterwave.flutter.clientms.dao.SettlementCycleDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.model.AccountQuery;
import com.flutterwave.flutter.clientms.model.AccountQueryTransaction;
import com.flutterwave.flutter.clientms.model.AirtimeTransaction;
import com.flutterwave.flutter.clientms.model.ConfigurationFee;
import com.flutterwave.flutter.clientms.model.Country;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.model.FlutterwaveSettlement;
import com.flutterwave.flutter.clientms.model.InternationalTransaction;
import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.RaveTransaction;
import com.flutterwave.flutter.clientms.model.Settlement;
import com.flutterwave.flutter.clientms.model.SettlementCycle;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.util.FillManager;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.Layouter1;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.AirtimeTransactionExportModel;
import com.flutterwave.flutter.clientms.viewmodel.CoreReportViewModel;
import com.flutterwave.flutter.clientms.viewmodel.FailureAnalysisModel;
import com.flutterwave.flutter.clientms.viewmodel.GenericTransactionExportModel;
import com.flutterwave.flutter.clientms.viewmodel.TransactionExportModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.transaction.Transactional;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author emmanueladeyemi
 */
@Startup
@Singleton
@AccessTimeout(value = 60000)
public class SchedulerService {

    @EJB
    private FlutterwaveSettlementDao flutterwaveSettlementDao;
    @EJB
    private RaveTransactionDao raveTransactionDao;
    @EJB
    private TransactionDao transactionDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private WalletService walletService;
    @EJB
    private CurrencyDao currencyDao;
    @EJB
    private FlutterwaveApiService flutterwaveApiService;
    @EJB
    private AccountQueryDao accountQueryDao;
    @EJB
    private AccountQueryTransactionDao queryTransactionDao;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private NotificationManager notificationManager;
    @EJB
    private ConfigurationFeeDao configurationFeeDao;
    @EJB
    private MerchantFeeDao merchantFeeDao;
    @EJB
    private ProviderDao providerDao;
    @EJB
    private CountryDao countryDao;
    @EJB
    private ExchangeRateDao exchangeRateDao;
    @EJB
    private AirtimeTransactionDao airtimeTransactionDao;
    @EJB
    private InternationalTransactionDao internationalTransactionDao;
    @EJB
    private SettlementCycleDao settlementCycleDao;
    @EJB
    private PosTransactionDao posTransactionDao;
    @EJB
    private PosTerminalDao posTerminalDao;
    @EJB
    private UtilityService utilityService;

    private final String username = Global.EMAIL_USERNAME;
    private final String password = Global.EMAIL_PASSWORD;

//    @Schedule(hour = "*/1", minute = "0", second = "0", persistent = false)
    public void checkSettlementStatusFromCore() {

        try {
            List<FlutterwaveSettlement> settlements = flutterwaveSettlementDao.findByTransactionStatus(Utility.SettlementState.PENDING);

            if (settlements == null || settlements.isEmpty()) {
                return;
            }

            for (FlutterwaveSettlement settlement : settlements) {

                if (settlement.getRuleState() == Utility.SettlementState.PENDING) {
                    Map<String, String> response = httpUtil.getRuleStatus(settlement.getSettlementToken());

                    if (!"00".equalsIgnoreCase(response.getOrDefault("responsecode", null))) {
                        continue;
                    }

                    settlement.setRuleStatus(true);
                    settlement.setRuleState(Utility.SettlementState.APPROVED);
                }

                Map<String, String> response = httpUtil.getSettlementStatus(settlement.getTransactionReference());

                if (response == null) {
                    continue;
                }

                if ("00".equalsIgnoreCase(response.getOrDefault("responsecode", null))) {
                    settlement.setTransactionStatus(true);
                    settlement.setTransactionState(Utility.SettlementState.APPROVED);

                    flutterwaveSettlementDao.update(settlement);
                }

                Settlement settlem = settlement.getSettlement();

                Currency currency = currencyDao.findByKey("shortName", settlem.getCurrency());

                // Fund provider 
                walletService.fundWallet(settlem.getProvider(), settlem.getCost(), currency, null);

                // Fund merchant
                String merchantId = settlem.getMerchantId();

                Product product = settlem.getProduct();

                walletService.fundWallet(product, merchantId, settlem.getCost(), currency, null);

            }

        } catch (DatabaseException ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    @Schedule(minute = "*/5", second = "0", hour = "*", persistent = false)
    public void processAccountStatus() {

        try {

            List<AccountQuery> list = accountQueryDao.findAll();

            List<AccountQueryTransaction> transactions;

            if (list == null) {
                return;
            }

            transactions = list.stream().map((AccountQuery accountQuery) -> {

                boolean status = flutterwaveApiService.accountEnquiry(accountQuery.getAccountNo(), accountQuery.getBank().getCode());

                Date date = new Date();
                AccountQueryTransaction accountQueryTransaction = new AccountQueryTransaction();
                accountQueryTransaction.setAccountNo(accountQuery.getAccountNo());
                accountQueryTransaction.setAccountQuery(accountQuery);
                accountQueryTransaction.setCreatedOn(date);
                accountQueryTransaction.setStatus(status == true ? Utility.QueryStatus.SUCCESSFUL : Utility.QueryStatus.FAILED);

                return accountQueryTransaction;
            }).collect(Collectors.<AccountQueryTransaction>toList());

            if (transactions == null || transactions.isEmpty()) {
                return;
            }

            queryTransactionDao.create(transactions);

        } catch (DatabaseException ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Schedule(dayOfWeek = "Mon", second = "0", hour = "4", minute = "30", persistent = false)
    public void processSummaryReport() {

        try {
            
            System.out.println("Running monday reports");
            
            String environment = configurationDao.getConfig("environment");
            
            if("test".equalsIgnoreCase(environment))
                return;
            
            String ids = configurationDao.getConfig("merchant_report_id");

            if (ids == null) {
                return;
            }

            String reportEmail = configurationDao.getConfig("merchant_summary_email");

            if (reportEmail == null) {
                reportEmail = "emmanuel@flutterwavego.com";
            }

            String[] merchantIds = ids.split(",");

            List report = new ArrayList();

            Calendar calendar = Calendar.getInstance();

            calendar.add(Calendar.DAY_OF_MONTH, -1);

            Date endDate = calendar.getTime();

            calendar.add(Calendar.DAY_OF_MONTH, -6);

            Date startDate = calendar.getTime();

            String arikId = "" + merchantIds[0];

            report = raveTransactionDao.getReportSummary(startDate, endDate, "successful", null, arikId, "NGN");

            Map arikReport = getReport(report);

            Map<String, Object> arikMain = new HashMap<>();

            arikMain.put("arikValue", arikReport.getOrDefault("total", 0.0));
            arikMain.put("arikVolume", arikReport.getOrDefault("totalV", 0));
            arikMain.put("arikMID", arikId);

            List<CoreMerchant> coreMerchants = coreMerchantDao.find("merchantID", merchantIds[1]);

            report = null;

            if (coreMerchants != null && !coreMerchants.isEmpty()) {

                CoreMerchant merchant = coreMerchants.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);

                if (merchant != null) {
                    report = transactionDao.getReportSummary(startDate, endDate, "00", null, merchantIds[1], "NGN");
                }

            }

            Map<String, Object> dataReport = new HashMap<>();

            if (report == null) {

                dataReport.put("danaValue", 0);
                dataReport.put("danaVolume", 0);
                dataReport.put("danaMID", merchantIds[1]);
            } else {

                Map<String, Object> tempReport = getReport(report);

                dataReport.put("danaValue", tempReport.getOrDefault("total", 0.0));
                dataReport.put("danaVolume", tempReport.getOrDefault("totalV", 0));
                dataReport.put("danaMID", merchantIds[1]);
            }

//            coreMerchants = coreMerchantDao.find("merchantID", merchantIds[2]);

//            report = null;

//            if (coreMerchants != null && !coreMerchants.isEmpty()) {
//
//                CoreMerchant merchant = coreMerchants.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
//
//                if (merchant != null) {
//                    report = coreTransactionDao.getReportSummary(startDate, endDate, null, null, merchant.getId() + "", "NGN");
//                }
//
//            }

            report = raveTransactionDao.getReportSummary(startDate, endDate, "successful", null, "1138", "NGN");

            Map medReport = getReport(report);

            Map<String, Object> medMain = new HashMap<>();

            medMain.put("medValue", medReport.getOrDefault("total", 0.0));
            medMain.put("medVolume", medReport.getOrDefault("totalV", 0));
            medMain.put("medMID", "1138");
            
//            Map<String, Object> medReport = new HashMap<>();

//            if (report == null) {
//
//                medReport.put("medValue", 0);
//                medReport.put("medVolume", 0);
//                medReport.put("medMID", merchantIds[2]);
//            } else {
//
////                Map<String, Object> tempReport = getReport(report);
//
//                medReport.put("medValue", tempReport.getOrDefault("total", 0.0));
//                medReport.put("medVolume", tempReport.getOrDefault("totalV", 0));
//                medReport.put("medMID", merchantIds[2]);
//            }

            Map<String, Map> mainReport = new HashMap<>();
            mainReport.put("arik", arikMain);
            mainReport.put("dana", dataReport);
            mainReport.put("med", medMain);

            if(!reportEmail.toLowerCase().contains("rotimi@paywithcapture.com"))
                reportEmail = reportEmail + ",rotimi@paywithcapture.com";
                
            String[] emails = reportEmail.split(",");

            
            for (String email : emails) {
                
                if(Utility.emptyToNull(email) == null)
                    continue;
                
                notificationManager.sendSelectedMerchantTransactionSummary(mainReport, email, startDate, endDate);
            }
            
            notificationManager.sendSelectedMerchantTransactionSummary(mainReport, "Solomon.Salau@accessbankplc.com", startDate, endDate);

        } catch (DatabaseException ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private Map getReport(List report) {

        double total = 0, successful = 0, failed = 0;
        long totalCount = 0, totalSuccessful = 0, totalFailed = 0;
        Map<Date, Object> data = new TreeMap<>();
        Map<Date, Object> volume = new TreeMap<>();

        for (Object obj : report) {

            Map<String, Double> map;

            Object[] reportElement = (Object[]) obj;
//                    
            Double amount;
            try {
                amount = ((BigDecimal) reportElement[0]) == null ? 0 : ((BigDecimal) reportElement[0]).doubleValue();
            } catch (Exception ex) {
                amount = ((Double) reportElement[0]);
            }

            Date date = (Date) (reportElement[1]);
            String status = (String) (reportElement[2]);
            Long count = (Long) ((BigInteger) reportElement[3] == null ? 0 : ((BigInteger) reportElement[3]).longValue());
            Long c = count;
//                    String d = dateFormat.format(date);
            Map<String, Long> mapVolume;

            map = (TreeMap<String, Double>) data.getOrDefault(date, new TreeMap<>());
            mapVolume = (TreeMap<String, Long>) volume.getOrDefault(date, new TreeMap<>());

            double amnt = amount;

            if ("success".equalsIgnoreCase(status)) {
                amnt += map.getOrDefault("successful", 0.0);
                map.put("successful", amnt);

                count += mapVolume.getOrDefault("successful", 0L);
                mapVolume.put("successful", count);

                successful += amount;
                totalSuccessful += c;

            } else {
                amnt += map.getOrDefault("failed", 0.0);
                map.put("failed", amnt);

                count += mapVolume.getOrDefault("failed", 0L);
                mapVolume.put("failed", count);

                totalFailed += c;
                failed += amount;
            }

            total += amount;
            totalCount += c;

            volume.put(date, mapVolume);
            data.put(date, map);
        }

        String pattern = "###,###.###";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);

        Map<String, Object> values = new HashMap<>();
        values.put("value", data);
        values.put("volume", volume);
        values.put("total", decimalFormat.format(total));
        values.put("successful", decimalFormat.format(successful));
        values.put("failed", decimalFormat.format(failed));
        values.put("totalV", totalCount);
        values.put("successfulV", totalSuccessful);
        values.put("failedV", totalFailed);

        return values;

    }

//    @Schedule(second = "0", hour = "1", minute = "30", persistent = false)
    public void processMerchantTransactions() {

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -1);
//            calendar.set(Calendar.DAY_OF_MONTH, 5);
//            calendar.set(Calendar.MONTH, Calendar.APRIL);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            Date startDate = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));

            Date endDate = calendar.getTime();

            List<Product> list = productDao.findAll();

            if (list == null) {
                return;
            }

            List<String> merchants = transactionDao.getDistinctMerchant(startDate, endDate);
            Product product = list.stream().filter(x -> "core".equalsIgnoreCase(x.getName()) || "flutterwave core".equalsIgnoreCase(x.getName())).findFirst().orElse(null);

            ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);

            String email = configurationDao.getConfig("merchant_trans_email");

            if (email == null) {
                email = "emmanuel@flutterwavego.com";
            }

            String[] emails = email.split(",");

            SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);

            for (String merchant : merchants) {

                List<CoreMerchant> coreMerchants = coreMerchantDao.find("merchantID", merchant);

                CoreMerchant coreMerchant = null;

                if (coreMerchants != null) {
                    coreMerchant = coreMerchants.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
                }

                List<Transaction> transactions = transactionDao.getSettleableTransanctions(startDate, endDate, null, null, merchant);

                MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(product, coreMerchant == null ? 0 : coreMerchant.getId());

                String merchantName = "";

                List<TransactionExportModel> exportModels = new ArrayList<>();
                for (Transaction t : transactions) {

                    try {
                        TransactionExportModel exportModel = new TransactionExportModel();

                        Provider provider = providerDao.findByKey("shortName", t.getProvider());

                        Currency providerCurrency = null;

                        if (provider != null) {
                            providerCurrency = provider.getCurrency();
                        } else {
                            providerCurrency = currencyDao.findByKey("shortName", "NGN");
                        }

                        if (providerCurrency == null) {
                            continue;
                        }

                        exportModel.setCurrency(t.getTransactionCurrency());
                        exportModel.setTransactionAmount(t.getAmount());
                        exportModel.setLast4(Utility.getLast(t.getCardMask(), 4));
                        exportModel.setMerchantId(t.getMerchantId());
                        exportModel.setReference(t.getFlwTxnReference());
                        exportModel.setTransactionDate(t.getDatetime());
                        exportModel.setTransactionType(t.getTransactionType());
                        exportModel.setMerchantName(coreMerchant != null ? coreMerchant.getCompanyname() : null);
                        exportModel.setMerchantReference(t.getMerchantTxnReference());

                        merchantName = exportModel.getMerchantName() == null ? "" : exportModel.getMerchantName().replaceAll(" ", "_");
                        String currencyCode = providerCurrency.getShortName();

                        Country currencyCountry = null;

//                        if (providerCurrency != null) {
                            currencyCountry = countryDao.findByCurrency(providerCurrency);
//                        }

                        String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                        String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                        String cardCountry = t.getCardCountry() == null ? "NG" : t.getCardCountry();

                        boolean local = t.getTransactionCurrency().equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));

                        exportModel.setInternational(local == false ? 1 : 0);

                        double configFee = getFee(configurationFee, t.getAmount(), local, t.getTransactionCurrency(), 1);

                        double merchantFe = getFee(merchantFee, t.getAmount(), local, t.getTransactionCurrency(), 1);

//                value += t.getAmount();
                        double fee = merchantFe == 0 ? configFee : merchantFe;

                        exportModel.setTransactionFee(fee);

                        exportModels.add(exportModel);

                    } catch (Exception ex) {
                        Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                notificationManager.buildExcelAndSend(exportModels, endDate, merchantName, merchant, emails, mailSender);
            }
        } catch (DatabaseException ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
//
    @Schedule(second = "0", hour = "4", minute = "30", persistent = false)
    public void processMerchantTransactionsSpecific() {

        processMerchTransactions();
    }

    public void processMerchTransactions() {

        try {

//            Map<String, String> emailReceipients = new HashMap<>();
            
            
            Properties properties  = Utility.getConfigProperty();
            
            
//            emailReceipients.put("144209", "fogundapo@flydanaair.com,oatunbi@flydanaair.com,sbajaj@danagroup.com,yharuna@flydanaair.com,ofasina@flydanaair.com");
//            emailReceipients.put("144106", "vivian@kudi.ai,pelumi@kudi.ai,yinka@kudi.ai,teniola@kudi.ai");
//            emailReceipients.put("144127", "bi.pay@jumia.com,desmond.okocha@jumia.com,afolake.akinboro@jumia.com");
//            emailReceipients.put("144020", "kakinola@pagatech.com,takinrinola@pagatech.com,iezebuiro@pagatech.com,morubu@pagatech.com,jadebayo@pagatech.com");
//            emailReceipients.put("144108", "olanrewaju.morakinyo@9mobile.com.ng,tessy.adams@9mobile.com.ng,olubunmi.falaki@9mobile.com.ng,electronicchannel@9mobile.com.ng,rukayat.arowogbadamu@9mobile.com.ng");
//            emailReceipients.put("144157", "info@dusupay.com");
//            emailReceipients.put("144159", "finance@wallet.ng");

            String cc = "settlement@flutterwavego.com,operations@flutterwavego.com";

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -1);
//            calendar.set(Calendar.DAY_OF_MONTH, 5);
//            calendar.set(Calendar.MONTH, Calendar.APRIL);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            Date startDate = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));

            Date endDate = calendar.getTime();

            List<Product> list = productDao.findAll();

            if (list == null) {
                return;
            }

            String merchantString = configurationDao.getConfig("merchant_get_settlement_report");

            if (merchantString == null || "".equalsIgnoreCase(merchantString)) {
                return;
            }

            String[] merchants = merchantString.split(",");

            Product product = list.stream().filter(x -> "core".equalsIgnoreCase(x.getName()) || "flutterwave core".equalsIgnoreCase(x.getName())).findFirst().orElse(null);

            ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);

            String[] ccRecipients = (cc == null) ? null : cc.split(",");

            SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);

            for (String merchantS : merchants) {

                String[] merchantInfo = merchantS.split(":");

                String merchantId = merchantInfo[0].trim();

                String email = properties.getProperty(merchantId, "emmanuel@flutterwavego.com");

                String[] emails = email.split(",");

                String category = merchantInfo[1];

                List<CoreMerchant> coreMerchants = coreMerchantDao.find("merchantID", merchantId);

                CoreMerchant coreMerchant = null;

                if (coreMerchants != null) {
                    coreMerchant = coreMerchants.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
                }

                List<Transaction> transactions = transactionDao.getAllSettleableTransanctions(startDate, endDate, null, null, merchantId);

                MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(product, coreMerchant == null ? 0 : coreMerchant.getId());

                String merchantName = "";

                boolean airline = false;

                if ("airline".equalsIgnoreCase(category)) {
                    airline = true;
                }

                List<GenericTransactionExportModel> exportModels = new ArrayList<>();
                for (Transaction t : transactions) {

                    try {
                        GenericTransactionExportModel exportModel = new GenericTransactionExportModel();

                        Provider provider = providerDao.findByKey("shortName", t.getProvider());
                    
                        Currency providerCurrency;
                        
                        if(provider == null){
                            
                            providerCurrency = currencyDao.findByKey("shortName", "NGN");
                            
                        }else
                            providerCurrency = provider.getCurrency();
                        
                         
                        exportModel.setNarration(t.getTransactionNarration());
                        exportModel.setCurrency(t.getTransactionCurrency());
                        exportModel.setTransactionAmount(t.getAmount());
                        exportModel.setMerchantId(t.getMerchantId());

                        String tempReference = t.getFlwTxnReference();

                        int index = tempReference.indexOf("FLW");
                        String subStr = tempReference.substring(index > -1 ? index : 0);

                        exportModel.setReference(subStr);
                        exportModel.setTransactionDate(t.getDatetime());
                        exportModel.setMerchantName(coreMerchant != null ? coreMerchant.getCompanyname() : null);
                        
                        if(!"144127".equalsIgnoreCase(merchantId))
                            exportModel.setMerchantTransactionReference(t.getMerchantTxnReference());
                        else
                            exportModel.setMerchantTransactionReference(t.getTransactionNarration());

                        merchantName = exportModel.getMerchantName() == null ? "" : exportModel.getMerchantName().replaceAll(" ", "_");
                        String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                        Country currencyCountry = null;

                        if (providerCurrency != null) {
                            currencyCountry = countryDao.findByCurrency(providerCurrency);
                        }

                        String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                        String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                        String cardCountry = t.getCardCountry() == null ? "NG" : t.getCardCountry();

                        boolean local = t.getTransactionCurrency().equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));

                        double configFee = getFee(configurationFee, t.getAmount(), local, t.getTransactionCurrency(), 1);

                        double merchantFe = getFee(merchantFee, t.getAmount(), local, t.getTransactionCurrency(), 1);

//                value += t.getAmount();
                        double fee = merchantFe == 0 ? configFee : merchantFe;

                        exportModel.setTransactionFee(fee);

                        if (airline == true) {

                            String narration = t.getTransactionNarration();

                            if (narration != null && !"".equals(narration)) {

                                index = narration.toUpperCase().indexOf("PNR");

                                if (index < 0) {

                                    narration = narration.trim();

                                    if (narration.length() > 6) {
                                        exportModel.setPnr(narration.substring(0, 6));
                                    } else {
                                        exportModel.setPnr(narration);
                                    }

                                } else {
                                    String pnr = narration.substring(index + 3).trim();

                                    if (pnr.length() > 6) {

                                        pnr = pnr.substring(0, 6);
                                    }

                                    exportModel.setPnr(pnr.trim());
                                }

                            }

                        }

                        exportModels.add(exportModel);

                    } catch (Exception ex) {
                        Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                if (airline == false) {
                    if(!"144127".equalsIgnoreCase(merchantId))
                        notificationManager.buildExcelAndSend(exportModels, endDate, merchantName, merchantId,
                                emails, ccRecipients, false, mailSender, "Transaction Settlement Report", "Pnr");
                    else
                        notificationManager.buildExcelAndSendXlsx(exportModels, endDate, merchantName, merchantId,
                                emails, ccRecipients, false, mailSender, "Transaction Settlement Report", "Pnr");
                } else {
                    
                        notificationManager.buildExcelAndSend(exportModels, endDate, merchantName, merchantId,
                                emails, ccRecipients, false, mailSender, "Transaction Settlement Report");
                }
            }
        } catch (DatabaseException ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
//    @Schedule(second = "0", minute = "0", hour = "*/1", persistent = false)
    public void processAirtimeTransaction() {

        Calendar calendar = Calendar.getInstance();

//        calendar.set(Calendar.DATE, 22);
//        calendar.set(Calendar.MONTH, Calendar.JUNE);
//        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.HOUR_OF_DAY, -1);

        Date startDate = calendar.getTime();

//        calendar.add(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));

        Date endDate = calendar.getTime();

        Page<AirtimeTransaction> page = airtimeTransactionDao.getAirtimeTransactions(0, 0, startDate, endDate, null, null, null, null, null, null, null, null, null, null);

        List<AirtimeTransaction> airtimeTransactions = page.getContent();

        if (airtimeTransactions != null && !airtimeTransactions.isEmpty()) {

            List<AirtimeTransactionExportModel> exportModels = airtimeTransactions.stream().map((AirtimeTransaction transaction) -> {

                AirtimeTransactionExportModel exportModel = new AirtimeTransactionExportModel();
                exportModel.setAccountNo(transaction.getCardMask());
                exportModel.setAmount(transaction.getAmount());
                exportModel.setBeneficiary(transaction.getTargetIdentifier());
                exportModel.setClientTxnRef(transaction.getClientTxnRef());
                exportModel.setCurrency(transaction.getTransactionCurrency());
                exportModel.setDatetime(transaction.getDatetime());
                exportModel.setMerchantId(transaction.getMerchantId());
                exportModel.setResponseCode(transaction.getResponseCode());
                exportModel.setResponseMessage(transaction.getResponseMessage());
                exportModel.setSender(transaction.getSourceIdentifier());
                exportModel.setTransactResultResponseCode(transaction.getTransactResultResponseCode());
                return exportModel;
            }).collect(Collectors.toList());

            String email = configurationDao.getConfig("merchant_airtime_trans_email");

            if (email == null) {
                email = "emmanuel@flutterwavego.com";
            }

            String[] emails = email.split(",");

            SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);

            notificationManager.buildExcelAndSend(exportModels, startDate, endDate, emails, mailSender);
        }

    }

    /**
     * This is called to process transaction daily
     */
//    @Schedule(second = "0", minute = "0", hour = "0", persistent = false)
    public void processAirtimeTransactionTill8() {

        Calendar calendar = Calendar.getInstance();

//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));

        Date endDate = calendar.getTime();

//        calendar.add(Calendar.DATE, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date startDate = calendar.getTime();

        Page<AirtimeTransaction> page = airtimeTransactionDao.getAirtimeTransactions(0, 0, startDate, endDate, null, null, null, null, null, null, null, null, null, null);

        List<AirtimeTransaction> airtimeTransactions = page.getContent();

        if (airtimeTransactions != null && !airtimeTransactions.isEmpty()) {

            List<AirtimeTransactionExportModel> exportModels = airtimeTransactions.stream().map((AirtimeTransaction transaction) -> {

                AirtimeTransactionExportModel exportModel = new AirtimeTransactionExportModel();
                exportModel.setAccountNo(transaction.getCardMask());
                exportModel.setAmount(transaction.getAmount());
                exportModel.setBeneficiary(transaction.getTargetIdentifier());
                exportModel.setClientTxnRef(transaction.getClientTxnRef());
                exportModel.setCurrency(transaction.getTransactionCurrency());
                exportModel.setDatetime(transaction.getDatetime());
                exportModel.setMerchantId(transaction.getMerchantId());
                exportModel.setResponseCode(transaction.getResponseCode());
                exportModel.setResponseMessage(transaction.getResponseMessage());
                exportModel.setSender(transaction.getSourceIdentifier());
                exportModel.setTransactResultResponseCode(transaction.getTransactResultResponseCode());

                return exportModel;
            }).collect(Collectors.toList());

            String email = configurationDao.getConfig("merchant_airtime_trans_email");

            if (email == null) {
                email = "emmanuel@flutterwavego.com";
            }

            String[] emails = email.split(",");

            SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);

            notificationManager.buildExcelAndSend(exportModels, startDate, endDate, emails, mailSender);
        }

    }

    @Schedule(second = "0", hour = "2", minute = "0", persistent = false)
    public void processCoreReport() {

        String environment = configurationDao.getConfig("environment");
        
        if("test".equalsIgnoreCase(environment))
                return ;
        
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);

        getCoreReport(calendar.getTime());

    }

//    @Schedule(second = "0", hour = "0", minute = "30", persistent = false)
    @Transactional
    public void getRaveInternationalTransaction() {

        try {

            String environment = configurationDao.getConfig("environment");
            
            if("test".equalsIgnoreCase(environment))
                return ;
            
            Page<RaveTransaction> raveTransactions = getRaveIntTrans(1);

            if (raveTransactions == null) {
                return;
            }

            List<RaveTransaction> transactions = raveTransactions.getContent();

            if (transactions == null || transactions.isEmpty()) {
                return;
            }

            List<RaveTransaction> list = new ArrayList<>();

            for (RaveTransaction rt : transactions) {
                try {
                    List<InternationalTransaction> it = internationalTransactionDao.findTransaction("flwReference", rt.getFlutterReference());

                    if (it != null && !it.isEmpty()) {
                        continue;
                    }

                    list.add(rt);

                } catch (Exception ex) {
                    Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            processInternationalTransaction(list);

        } catch (Exception ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

//    @Schedule(second = "0", dayOfMonth = "17", month = "8", hour = "16", minute = "15", persistent = false)
    @Transactional
    public void getRaveInternationalTransactionStartUp() {

        try {

            Page<RaveTransaction> raveTransactions = getRaveIntTrans(20);

            if (raveTransactions == null) {
                return;
            }

            List<RaveTransaction> transactions = raveTransactions.getContent();

            if (transactions == null || transactions.isEmpty()) {
                return;
            }

            List<RaveTransaction> list = new ArrayList<>();

            for (RaveTransaction rt : transactions) {
                try {
                    List<InternationalTransaction> it = internationalTransactionDao.findTransaction("transRef", rt.getTransactionReference());

                    if (it != null && !it.isEmpty()) {
                        continue;
                    }

                    list.add(rt);

                } catch (Exception ex) {
                    Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            processInternationalTransaction(list);

        } catch (Exception ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

//    @Transactional
//    public void runStartUp(){
//        
//        
//    }
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Page<RaveTransaction> getRaveIntTrans(int duration) {

        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.DATE, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND) - 1);

        Date endDate = calendar.getTime();

        if (duration > 1) {
            calendar.add(Calendar.DATE, -1 * duration);
        }

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date startDate = calendar.getTime();

        Page<RaveTransaction> raveTransactions = raveTransactionDao.searchInternationalByStatus(0, 0, startDate, endDate, null, null, null, null, null, null, null, null, null, "NGN", false);

        return raveTransactions;
    }

    @Transactional
    public void processInternationalTransaction(List<RaveTransaction> list) {

        try {
            
            if(list == null || list.isEmpty())
                return ;
            
            List<InternationalTransaction> internationalTransactions = list.stream()
                    .parallel()
                    .map((RaveTransaction t) -> {

                        InternationalTransaction internationalTransaction = new InternationalTransaction();
                        internationalTransaction.setAmount(t.getAmount());
                        internationalTransaction.setAppfee(t.getAppfee());
                        internationalTransaction.setChargedAmount(t.getChargedAmount());
                        internationalTransaction.setMerchantId(t.getMerchant().getId() + "");
                        internationalTransaction.setCreatedAt(t.getCreatedAt());
                        internationalTransaction.setCreatedOn(new Date());
                        internationalTransaction.setCurrency(t.getCurrency());
                        internationalTransaction.setCycle(t.getCycle());
                        internationalTransaction.setFlwReference(t.getFlutterReference());
                        internationalTransaction.setTransRef(t.getTransactionReference());
                        internationalTransaction.setMerchantName(t.getMerchant().getBusinessName());
                        internationalTransaction.setNarration(t.getNarration());
                        internationalTransaction.setPaymentType(t.getPaymentEntity());
                        internationalTransaction.setProduct("rave");
                        internationalTransaction.setTransactionType(t.getTransactionType());

                        return internationalTransaction;
                    }).collect(Collectors.<InternationalTransaction>toList());

            if (internationalTransactions != null && !internationalTransactions.isEmpty()) {
                logInternationalTransaction(internationalTransactions);
            }
        } catch (DatabaseException ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Transactional(Transactional.TxType.MANDATORY)
    public void logInternationalTransaction(List<InternationalTransaction> internationalTransactions) throws DatabaseException {
        internationalTransactionDao.create(internationalTransactions);
    }

//    @Schedule(second = "0", hour = "1,12,23", minute = "0", persistent = false)
    public void notifyAdminOfRaveIntTransaction() {

        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.DATE, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND) - 1);

        Date endDate = calendar.getTime();

        calendar.add(Calendar.DATE, -19);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date startDate = calendar.getTime();

        Page<InternationalTransaction> page = internationalTransactionDao.find(0, 0, startDate, endDate, null, null, Boolean.FALSE);
//        
        if (page == null) {
            return;
        }

        if (page.getContent() == null || page.getContent().isEmpty()) {
            return;
        }

        String cycleString = configurationDao.getConfig("inter_settlement_cycle");

        int defaultCycle;

        if (cycleString == null) {
            defaultCycle = 14;
        } else {
            defaultCycle = Integer.parseInt(cycleString);
        }

        List<InternationalTransaction> transactions = page.getContent();

        Map<String, List<InternationalTransaction>> groupedTransactions = transactions.stream().collect(Collectors.groupingBy(InternationalTransaction::getMerchantId));

        String emails = configurationDao.getConfig("inter_settlement_emails");

        if (emails == null) {
            emails = "emmanuel@flutterwavego.com";
        }

        List<InternationalTransaction> list = new ArrayList<>();

        for (String merchantId : groupedTransactions.keySet()) {

            Page<SettlementCycle> settlementCycles = settlementCycleDao.find(0, 0, "rave", merchantId);

            int cycle = defaultCycle;

            if (settlementCycles.getContent() != null && !settlementCycles.getContent().isEmpty()) {
                cycle = settlementCycles.getContent().get(0).getCycle();
            }

            int age = cycle;

            List<InternationalTransaction> internationalTransactions = groupedTransactions.get(merchantId);

            for (InternationalTransaction transaction : internationalTransactions) {

                if (Utility.age(new Date(), transaction.getCreatedAt(), true) == (age - 1)) {
//                    try {
                    transaction.setNotificationDate(new Date());
                    transaction.setNotificationStatus(true);

//                        internationalTransactionDao.update(transaction);
                    list.add(transaction);
//                    } catch (DatabaseException ex) {
//                        Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
//                    }
                }
            }
//            internationalTransactions = internationalTransactions.stream()
//                    .filter(x -> Utility.age(x.getCreatedAt(), new Date(), true) == (age - 1) )
//                    .collect(Collectors.toList());

        }

        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 1);

        if (list.isEmpty()) {
            return;
        }

        try {
            internationalTransactionDao.update(list);
        } catch (Exception ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }

        SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT, true,
                Global.USE_STARTTLS, false);

        notificationManager.buildExcelAndSendSettlement((List) list, InternationalTransaction.class, "All Merchant",
                calendar.getTime(), emails.split(","), new String[]{"account", "cycle", "notificationStatus", "notificationDate"}, false, mailSender);

    }

//    @Schedule(second = "0", hour = "0,9,12", minute = "0", persistent = false)
    public void notifyAdminOfRaveIntTransactionToday() {

        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.DATE, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND) - 1);

        Date endDate = calendar.getTime();

        calendar.add(Calendar.DATE, -19);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date startDate = calendar.getTime();

        Page<InternationalTransaction> page = internationalTransactionDao.find(0, 0, startDate, endDate, null, null, Boolean.FALSE);
//        
        if (page == null) {
            return;
        }

        if (page.getContent() == null || page.getContent().isEmpty()) {
            return;
        }

        String cycleString = configurationDao.getConfig("inter_settlement_cycle");

        int defaultCycle;

        if (cycleString == null) {
            defaultCycle = 14;
        } else {
            defaultCycle = Integer.parseInt(cycleString);
        }

        List<InternationalTransaction> transactions = page.getContent();

        Map<String, List<InternationalTransaction>> groupedTransactions = transactions.stream().collect(Collectors.groupingBy(InternationalTransaction::getMerchantId));

        String emails = configurationDao.getConfig("inter_settlement_emails");

        if (emails == null) {
            emails = "emmanuel@flutterwavego.com";
        }

        List<InternationalTransaction> list = new ArrayList<>();

        for (String merchantId : groupedTransactions.keySet()) {

            Page<SettlementCycle> settlementCycles = settlementCycleDao.find(0, 0, "rave", merchantId);

            int cycle = defaultCycle;

            if (settlementCycles.getContent() != null && !settlementCycles.getContent().isEmpty()) {
                cycle = settlementCycles.getContent().get(0).getCycle();
            }

            int age = cycle;

            List<InternationalTransaction> internationalTransactions = groupedTransactions.get(merchantId);

            for (InternationalTransaction transaction : internationalTransactions) {

                if (Utility.age(new Date(), transaction.getCreatedAt(), true) == age) {
//                    try {
                    transaction.setNotificationDate(new Date());
                    transaction.setNotificationStatus(true);

//                        internationalTransactionDao.update(transaction);
                    list.add(transaction);
//                    } catch (DatabaseException ex) {
//                        Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
//                    }
                }
            }
//            internationalTransactions = internationalTransactions.stream()
//                    .filter(x -> Utility.age(x.getCreatedAt(), new Date(), true) == (age - 1) )
//                    .collect(Collectors.toList());

        }

        calendar.setTime(new Date());
//        calendar.add(Calendar.DATE, 1);

        if (list.isEmpty()) {
            return;
        }

        try {
            internationalTransactionDao.update(list);
        } catch (Exception ex) {
            Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }

        SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                true, Global.USE_STARTTLS, false);

        notificationManager.buildExcelAndSendSettlement((List) list, InternationalTransaction.class, "All Merchant",
                calendar.getTime(), emails.split(","), new String[]{"account", "cycle", "notificationStatus", "notificationDate"}, true, mailSender);

    }

    public void getCoreReport(Date dateParam) {

        try {

            if (dateParam == null) {
                return;
            }

            Date startDate = null, endDate = null;

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            startDate = dateParam;
            Date tempStartDate = startDate;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            startDate = calendar.getTime();
            calendar = Calendar.getInstance();
            calendar.setTime(tempStartDate);
            //                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 59);
            endDate = calendar.getTime();
            // This get successful card account
            List cardSuccessSummary = transactionDao.getReport(startDate, endDate, true, true);
            // card failure
            List cardFailureSummary = transactionDao.getReport(startDate, endDate, false, true);
            // account success
            List accountSuccessSummary = transactionDao.getReport(startDate, endDate, true, false);
            // account failure
            List accountFailureSummary = transactionDao.getReport(startDate, endDate, false, false);
            // card failure reasons
            List cardFailureReasons = transactionDao.getFailureReason(startDate, endDate, true);

            List cardSuccessful = transactionDao.getSuccessAnalysis(startDate, endDate, true);

            Map<String, Map<Date, CoreReportViewModel>> map = new TreeMap<>();

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            if (cardSuccessSummary != null && !cardSuccessSummary.isEmpty()) {

                for (Object data : cardSuccessSummary) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());

                    Map<Date, CoreReportViewModel> result = map.getOrDefault(currency, new HashMap<>());

                    CoreReportViewModel model = result.getOrDefault(date, new CoreReportViewModel());

                    model.setCurrency(currency);
                    if (sum != null) {
                        model.setCardSuccessValue(model.getCardSuccessValue() + (sum));
                    }
                    if (count != null) {
                        model.setCardSuccessVolume(model.getCardSuccessVolume().add(count));
                    }

                    result.put(date, model);

                    map.put(currency, result);
                }
            }

            if (cardFailureSummary != null && !cardFailureSummary.isEmpty()) {

                for (Object data : cardFailureSummary) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());

                    Map<Date, CoreReportViewModel> result = map.getOrDefault(currency, new HashMap<>());

                    CoreReportViewModel model = result.getOrDefault(date, new CoreReportViewModel());

                    model.setCurrency(currency);
                    if (sum != null) {
                        model.setCardFailureValue(model.getCardFailureValue() + sum);
                    }
                    if (count != null) {
                        model.setCardFailureVolume(model.getCardFailureVolume().add(count));
                    }

                    result.put(date, model);

                    map.put(currency, result);
                }
            }

            if (accountSuccessSummary != null && !accountSuccessSummary.isEmpty()) {

                for (Object data : accountSuccessSummary) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());

                    Map<Date, CoreReportViewModel> result = map.getOrDefault(currency, new HashMap<>());

                    CoreReportViewModel model = result.getOrDefault(date, new CoreReportViewModel());

                    model.setCurrency(currency);

                    if (sum != null) {
                        model.setAccountSuccessValue(model.getAccountSuccessValue() + sum);
                    }
                    if (count != null) {
                        model.setAccountSuccessVolume(model.getAccountSuccessVolume().add(count));
                    }

                    result.put(date, model);

                    map.put(currency, result);
                }
            }

            if (accountFailureSummary != null && !accountFailureSummary.isEmpty()) {

                for (Object data : accountFailureSummary) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());

                    Map<Date, CoreReportViewModel> result = map.getOrDefault(currency, new HashMap<>());

                    CoreReportViewModel model = result.getOrDefault(date, new CoreReportViewModel());

                    model.setCurrency(currency);
                    if (sum != null) {
                        model.setAccountFailureValue(model.getAccountFailureValue() + sum);
                    }
                    if (count != null) {
                        model.setAccountFailureVolume(model.getAccountFailureVolume().add(count));
                    }

                    result.put(date, model);

                    map.put(currency, result);
                }
            }

            Map<Date, Map<String, Map<String, List<FailureAnalysisModel>>>> failureAnalysis = new TreeMap<>(Collections.reverseOrder());

            if (cardFailureReasons != null && !cardFailureReasons.isEmpty()) {

                for (Object data : cardFailureReasons) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(String.valueOf(objects[3]));
                    String reason = String.valueOf(objects[4]);
                    String cardScheme = String.valueOf(objects[5]);

                    Map<String, Map<String, List<FailureAnalysisModel>>> result = failureAnalysis.getOrDefault(date, new HashMap<>());

                    Map<String, List<FailureAnalysisModel>> models = result.getOrDefault(currency.toUpperCase(), new HashMap<>());

                    List<FailureAnalysisModel> failureAnalysisModels = models.getOrDefault(cardScheme + "", new ArrayList<>());

                    FailureAnalysisModel analysisModel = new FailureAnalysisModel();
                    analysisModel.setCurrency(currency);
                    analysisModel.setCardScheme(cardScheme);
                    analysisModel.setReason(reason);
                    analysisModel.setValue(sum);
                    analysisModel.setVolume(count);

                    failureAnalysisModels.add(analysisModel);

                    models.put(cardScheme + "", failureAnalysisModels);

                    result.put(currency, models);

                    failureAnalysis.put(date, result);
                }
            }

            Map<Date, Map<String, Map<String, FailureAnalysisModel>>> successAnalysis = new TreeMap<>(Collections.reverseOrder());

            if (cardSuccessful != null && !cardSuccessful.isEmpty()) {

                for (Object data : cardSuccessful) {

                    Object[] objects = (Object[]) data;

                    Double sum = (Double) objects[0];
                    BigInteger count = (BigInteger) objects[1];
                    String currency = (String) objects[2];
                    Date date = format.parse(objects[3].toString());
                    String cardScheme = String.valueOf(objects[4]);

                    Map<String, Map<String, FailureAnalysisModel>> result = successAnalysis.getOrDefault(date, new HashMap<>());

                    Map<String, FailureAnalysisModel> models = result.getOrDefault(currency.toUpperCase(), new HashMap<>());

                    FailureAnalysisModel analysisModel = models.getOrDefault(cardScheme + "", new FailureAnalysisModel());

                    analysisModel.setCurrency(currency);
                    analysisModel.setCardScheme(cardScheme);
                    analysisModel.setValue(sum);
                    analysisModel.setVolume(count);

                    models.put(cardScheme + "", analysisModel);

                    result.put(currency, models);

                    successAnalysis.put(date, result);
                }
            }

//            successAnalysis.
            XSSFWorkbook workbook = new XSSFWorkbook();
//            
            XSSFSheet worksheet = workbook.createSheet("Card And Account Transactions Analysis");

            List<String> headers = new ArrayList<>();
            headers.add("Date/Currency");
            headers.add("Card Successful Value");
            headers.add("Card Successful Volume");
            headers.add("Card Failed Value");
            headers.add("Card Failed Volume");

            headers.add("Account Successful Value");
            headers.add("Account Successful Volume");
            headers.add("Account Failed Value");
            headers.add("Account Failed Volume");

            Layouter1.buildHeaders(worksheet, 0, 0, headers);

            Long val = ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));

            int diff = val.intValue();

            FillManager.buildCoreReportTrans(worksheet, 0, 0, map, diff);

            worksheet = workbook.createSheet("Card Failure Analysis");

            headers = new ArrayList<>();
            headers.add("Date/Currency");
            headers.add("Card Scheme");
            headers.add("Value");
            headers.add("Volume");
            headers.add("Reason");

            Layouter1.buildHeaders(worksheet, 0, 0, headers);

            FillManager.buildCoreReportFailure(worksheet, 0, 0, failureAnalysis);

            worksheet = workbook.createSheet("Card Success Analysis");

            headers = new ArrayList<>();
            headers.add("Date/Currency");
            headers.add("Card Scheme");
            headers.add("Value");
            headers.add("Volume");

            Layouter1.buildHeaders(worksheet, 0, 0, headers);

            FillManager.buildCoreReportSuccess(worksheet, 0, 0, successAnalysis);

            File tempFile = File.createTempFile("Core_Txn_Report" + Utility.formatDate(endDate, "dd-MM-yyyy"), ".xlsx");

            OutputStream outputStream = new FileOutputStream(tempFile);

//            response.setHeader("Content-Disposition", "attachment; filename=Core_Transaction_Report_Week_to_"+Utility.formatDate(endDate, "dd-MM-yyyy")+".xlsx");
//            response.setContentType("application/vnd.ms-excel");
            workbook.write(outputStream);

//            String token = configurationDao.getConfig("slack_api_key");
//            String channel = configurationDao.getConfig("slack_api_channel");

            String title = "Core_Transaction_Report_from_" + Utility.formatDate(startDate, "dd-MM-yyyy") + "_to_" + Utility.formatDate(endDate, "dd-MM-yyyy") + ".xlsx";

            //notificationManager.uploadToSlack(tempFile, title, token, channel);
            
            SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);
            
            notificationManager.buildExcelAndSend(title.replace("_", " "), "henry@flutterwavego.com", "noreply@flutterwavego.com", 
                    Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss"), Utility.formatDate(endDate, "yyyy-MM-dd HH:mm:ss"), tempFile, mailSender);

        } catch (Exception ex) {
            Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private double getFee(ConfigurationFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    private double getFee(MerchantFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

//    @Schedule(second = "0", hour = "*", minute = "*/2", persistent = false)
//    public void checkMposTransactionRescheduleCallback() {
//
//        List<PosTransaction> list = posTransactionDao.findReschedule();
//
//        if (list == null || list.isEmpty()) {
//            return;
//        }
//
//        for (PosTransaction posTransaction : list) {
//
//            try {
//                PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());
//                if (posTerminal == null || posTerminal.getMerchant() == null) {
//                    return;
//                }
//
//                JSONObject jSONObject = new JSONObject();
//                jSONObject.append("terminalid", posTransaction.getTerminalId());
//                jSONObject.append("merchantid", posTerminal.getMerchant().getMerchantId());
//                jSONObject.append("merchantcode", posTerminal.getMerchant().getMerchantCode());
//                jSONObject.append("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
//                jSONObject.append("datetime", Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
//                jSONObject.append("responsecode", Utility.nullToEmpty(posTransaction.getResponseCode()));
//                jSONObject.append("responsemessage", Utility.nullToEmpty(posTransaction.getResponseMessage()));
//                jSONObject.append("mask", Utility.nullToEmpty(posTransaction.getPan()));
//                jSONObject.append("type", Utility.nullToEmpty(posTransaction.getType()));
//                jSONObject.append("status", Utility.nullToEmpty(posTransaction.getStatus()));
//                jSONObject.append("amount", posTransaction.getAmount());
//                jSONObject.append("fileid", posTransaction.getFileId());
//                Log log = new Log();
//                log.setAction("MPOS Call back for terminalId " + posTransaction.getTerminalId());
//                log.setCreatedOn(new Date());
//                log.setDescription(jSONObject.toString());
//                log.setLevel(LogLevel.Info);
//                log.setLogDomain(LogDomain.ADMIN);
//                log.setLogState(LogState.STARTED);
//                log.setUsername("API");
//                log.setStatus(LogStatus.SUCCESSFUL);
//                logService.log(log);
//                List<CallbackConfiguration> configurations = callbackConfigurationDao.getConfiguration("pos", posTerminal.getMerchant().getMerchantId());
//                if (configurations == null || configurations.isEmpty()) {
//                    return;
//                }
//                CallbackConfiguration callbackConfiguration = configurations.get(0);
//                String urls = callbackConfiguration.getUrls();
//                String[] urlArray = urls.split(",");
//                Map<String, String> header = new HashMap<>();
//                header.put("Content-Type", "application/json");
//
//                String result = "";
//                for (String urlString : urlArray) {
//
//                    result += " U: " + urlString;
//                    Map<String, String> map = httpUtil.doHttpPostC(urlString, jSONObject.toString(), header);
//
//                    if (map == null) {
//                        result += " S: failed,";
//                    } else {
//
//                        result += " S: " + map.getOrDefault("status", "failed");
//                        result += " R: " + map.getOrDefault("result", "");
//                    }
//                }
//
//                posTransaction.setCallBack(true);
//                posTransaction.setCallBackResponse(result);
//                posTransaction.setCallbackOn(new Date());
//                posTransactionDao.update(posTransaction);
//            } catch (DatabaseException ex) {
//                Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//
//    }
    
    
    @Schedule(second = "0", hour = "*", minute = "*/2", persistent = false)
    public void checkMposTransactionRescheduleCallback() {

        List<PosTransaction> list = posTransactionDao.findReschedule();

        if (list == null || list.isEmpty()) {
            return;
        }

        for (PosTransaction posTransaction : list) {

            try {
                PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());
                
                if (posTerminal == null) {
                    return;
                }

                utilityService.callBackUserReschedule(posTransaction);
                
            } catch (Exception ex) {
                Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}

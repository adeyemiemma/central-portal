/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

/**
 *
 * @author emmanueladeyemi
 */
public class VasMerchant {

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the nameAndId
     */
    public String getNameAndId() {
        return nameAndId;
    }

    /**
     * @param nameAndId the nameAndId to set
     */
    public void setNameAndId(String nameAndId) {
        this.nameAndId = nameAndId;
    }
    
    private String name;
    private String merchantId;
    private String nameAndId;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.service.model.VasMerchant;
import com.flutterwave.flutter.clientms.service.model.VoucherFundingResponse;
import com.flutterwave.flutter.clientms.util.CryptoUtil;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.util.PageResult;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class AirtimeHelper {
    
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private ConfigurationDao configurationDao;   
    
    @Inject
    LogService logService;
    
//    String url = null;
    
    
    public PageResult getTransactions(int start, int length, String network, String status, 
            String startDate, String endDate, String search, String provider){
        
        int pageNo = start / length;
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
            
        String url = properties.getProperty("ussd_airtime_url");
        
        JSONObject jSONObject = new JSONObject()
                .put("start_date_time", Utility.emptyToNull(startDate))
                .put("end_date_time", Utility.emptyToNull(endDate))
                .put("end_date_time", Utility.emptyToNull(endDate))
                .put("page_no", pageNo)
                .put("search", Utility.emptyToNull(search))
                .put("network", Utility.emptyToNull(network))
                .put("status", Utility.emptyToNull(status))
                .put("provider", Utility.emptyToNull(provider))
                .put("page_size", length);
        
        String response = httpUtil.doHttpPost(url+"/transaction/fetch", jSONObject.toString(), getUssdHeader());
        
        PageResult pageResult = new PageResult();
        
        if(response == null){
            
            pageResult.setData(new ArrayList());
            pageResult.setRecordsFiltered(0L);
            pageResult.setRecordsTotal(0L);
            
            return pageResult;
        }
        
        jSONObject = new JSONObject(response);
        
        JSONObject dataObject = jSONObject.getJSONObject("data");
        pageResult.setData(dataObject.getJSONArray("transactions").toList());
        pageResult.setRecordsFiltered(dataObject.getLong("total"));
        pageResult.setRecordsTotal(dataObject.getLong("total"));
            
        return pageResult;
    }
    
    public PageResult getTransactionOtherss(int start, int length, String network, String status, 
            String startDate, String endDate, String search, String provider){
        
        int pageNo = start / length;
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
            
        String url = properties.getProperty("mobile_airtime_url");
        
        JSONObject jSONObject = new JSONObject()
                .put("start_date_time", Utility.emptyToNull(startDate))
                .put("end_date_time", Utility.emptyToNull(endDate))
                .put("end_date_time", Utility.emptyToNull(endDate))
                .put("page_no", pageNo)
                .put("search", Utility.emptyToNull(search))
                .put("network", Utility.emptyToNull(network))
                .put("status", Utility.emptyToNull(status))
                .put("provider", Utility.emptyToNull(provider))
                .put("page_size", length);
        
        String response = httpUtil.doHttpPost(url+"/transaction/fetch", jSONObject.toString(), getMobileAppHeader());
        
        PageResult pageResult = new PageResult();
        
        if(response == null){
            
            pageResult.setData(new ArrayList());
            pageResult.setRecordsFiltered(0L);
            pageResult.setRecordsTotal(0L);
            
            return pageResult;
        }
        
        jSONObject = new JSONObject(response);
        
        JSONObject dataObject = jSONObject.getJSONObject("data");
        pageResult.setData(dataObject.getJSONArray("transactions").toList());
        pageResult.setRecordsFiltered(dataObject.getLong("total"));
        pageResult.setRecordsTotal(dataObject.getLong("total"));
            
        return pageResult;
    }
    
    public JSONObject downloadTransaction(String network, String status, 
            String startDate, String endDate, String search, String provider, String email){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
            
        String urlPath = properties.getProperty("ussd_airtime_url");
        
        JSONObject jSONObject = new JSONObject()
                .put("start_date_time", Utility.nullToEmpty(startDate))
                .put("end_date_time", Utility.nullToEmpty(endDate))
                .put("search", Utility.nullToEmpty(search))
                .put("network", Utility.nullToEmpty(network))
                .put("status", Utility.nullToEmpty(status))
                .put("email", Utility.nullToEmpty(email))
                .put("title", Utility.nullToEmpty("Airtime Transactions from "+startDate+" to "+ endDate))
                .put("provider", Utility.nullToEmpty(provider));
        
        String response = httpUtil.doHttpPost(urlPath+"/transaction/download", jSONObject.toString(), getUssdHeader());
        
        if(response == null)
            return null;
        
        try{
            jSONObject = new JSONObject(response);
        } catch(JSONException exception){
            exception.printStackTrace();
        }
        
        return jSONObject;
    }
    
    /**
     * This is call the non-ussd access bank airtime
     * @param network
     * @param status
     * @param startDate
     * @param endDate
     * @param search
     * @param provider
     * @param email
     * @return 
     */
    public JSONObject downloadTransactionMobileApp(String network, String status, 
            String startDate, String endDate, String search, String provider, String email){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
            
        String urlPath = properties.getProperty("mobile_airtime_url");
        
        JSONObject jSONObject = new JSONObject()
                .put("start_date_time", Utility.nullToEmpty(startDate))
                .put("end_date_time", Utility.nullToEmpty(endDate))
                .put("search", Utility.nullToEmpty(search))
                .put("network", Utility.nullToEmpty(network))
                .put("status", Utility.nullToEmpty(status))
                .put("email", Utility.nullToEmpty(email))
                .put("title", Utility.nullToEmpty("Airtime Transactions Report "))
                .put("provider", Utility.nullToEmpty(provider));
        
        String response = httpUtil.doHttpPost(urlPath+"/transaction/download", jSONObject.toString(), getMobileAppHeader());
        
        if(response == null)
            return null;
        
        try{
            jSONObject = new JSONObject(response);
        } catch(JSONException exception){
            exception.printStackTrace();
        }
        
        return jSONObject;
    }
    
    public List<String> getStatus(){
        
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
            
        String urlPath = properties.getProperty("ussd_airtime_url");
        
        String response = httpUtil.doHttpGet(urlPath+"/transaction/status", getUssdHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");
        
        List<String> list = array.toList().stream().map(x -> x.toString()).collect(Collectors.toList());
        
        return list;
    }
    
    public List<String> getNetwork(){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
            
        String urlPath = properties.getProperty("ussd_airtime_url");
        
        String response = httpUtil.doHttpGet(urlPath+"/network", getUssdHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");
        
        List<String> list = array.toList().stream().map(x -> x.toString()).collect(Collectors.toList());
        
        return list;
    }
    
    public List<String> getNetworkFromOthers(){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
            
        String urlPath = properties.getProperty("mobile_airtime_url");
        
        String response = httpUtil.doHttpGet(urlPath+"/airtime/network", getMobileAppHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");
        
        List<String> list = array.toList().stream().map(x -> x.toString()).collect(Collectors.toList());
        
        return list;
    }

    public List getBalance(){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
        String urlPath = properties.getProperty("ussd_airtime_url");
            
        String response = httpUtil.doHttpGet(urlPath+"/provider/balance", getUssdHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");

        return array.toList();
    }
    
    public List getProvider(){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
        
        String url = properties.getProperty("ussd_airtime_url");
        
        String response = httpUtil.doHttpGet(url+"/provider", getUssdHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");
        
        List list = new ArrayList();
        
        for(Object object : array){
            
            JSONObject sONObject = (JSONObject) object;
            
            Map<String, String> responseData = new HashMap<>();
            responseData.put("name", sONObject.optString("name"));
            responseData.put("shortname", sONObject.optString("short_name"));
            
            list.add(responseData);           
        }
        
        return list;
    }
    
    public List getProviderOthers(){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
        
        String url = properties.getProperty("mobile_airtime_url");
        
        String response = httpUtil.doHttpGet(url+"/provider", getMobileAppHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");
        
        List list = new ArrayList();
        
        for(Object object : array){
            
            JSONObject sONObject = (JSONObject) object;
            
            Map<String, String> responseData = new HashMap<>();
            responseData.put("name", sONObject.optString("name"));
            responseData.put("shortname", sONObject.optString("short_name"));
            
            list.add(responseData);           
        }
        
        return list;
    }
    
    public List getProviderConfigurations(){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
        
        String url = properties.getProperty("ussd_airtime_url");
        
        
        String response = httpUtil.doHttpGet(url+"/provider/config", getUssdHeader());
        
        if(response == null)
            return new ArrayList<>();
        
        JSONObject jSONObject = new JSONObject(response);
        
        JSONArray array = jSONObject.getJSONArray("data");
        
        List list = new ArrayList();
        
        for(Object object : array){
            
            JSONObject sONObject = (JSONObject) object;

//            Map<String, String> responseData = new HashMap<>();
//            responseData.put("network", sONObject.optString("network"));
//            responseData.put("provider_name", sONObject.optString("provider_name"));
//            responseData.put("provider_short_name", sONObject.optString("provider_short_name"));
            
            list.add(sONObject.toMap());           
        }
        
        return list;
    }
    
    public JSONObject setDefaultProvider(String shortName, String network){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
        
        String key = properties.getProperty("flw_airtime_key");
        
        String url = configurationDao.getConfig("ussd_airtime_url");
        
        String hash = key+""+shortName+""+key.substring(0,10);
        
        hash = CryptoUtil.sha512(hash, null);
        
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("network", network)
                  .put("provider_short_name", shortName)
                  .put("hash", hash);
        
        
        String response = httpUtil.doHttpPut(url +"/provider", jSONObject.toString(), getUssdHeader());
        
        if(response == null)
            return null;
        
        jSONObject = new JSONObject(response);
        
        return jSONObject;
    }
    
    private Map<String, String> getUssdHeader(){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
        
        String key = properties.getProperty("flw_airtime_ussd_key");
        
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        header.put("Authorization", "Bearer "+key);
        
        return header;
    }
    
    private Map<String, String> getMobileAppHeader(){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
        
        String key = properties.getProperty("flw_airtime_mobile_key");
        
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        header.put("Authorization", "Bearer "+key);
        
        return header;
    }
    
    private Map<String, String> getWalletHeader(){
        
        Properties properties = Utility.getConfigProperty();
        
        if(properties == null)
            return null;
        
        String key = properties.getProperty("flw_wallet_key");
        
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        header.put("Authorization", "Bearer "+key);
        
        return header;
    }
    
    public Response getMerchant(){
        
        Properties properties = Utility.getConfigProperty();
        
        String url = properties.getProperty("mobile_airtime_url");
        
        String response = httpUtil.doHttpGet(url+"/merchants", getMobileAppHeader());
        
        JSONArray jSONArray = new JSONArray(response);
        
        return Response.status(Response.Status.OK).entity(jSONArray.toList()).build();
    }
    
    public List<VasMerchant> getMerchantList(){
        
        Properties properties = Utility.getConfigProperty();
        
        String url = properties.getProperty("mobile_airtime_base_url");
        
        String response = httpUtil.doHttpGet(url+"/merchants", getMobileAppHeader());
        
        JSONArray jSONArray = new JSONArray(response);
        
        List<VasMerchant> vasMerchants = new ArrayList<>();
        
        for(int i = 0; i < jSONArray.length(); i++){
            
            JSONObject item = jSONArray.getJSONObject(i);
            
            VasMerchant vasMerchant = new VasMerchant();
            vasMerchant.setMerchantId(item.optString("token"));
            vasMerchant.setName(item.optString("name"));
            vasMerchant.setNameAndId(item.optString("name") + "|"+vasMerchant.getMerchantId());
            
            vasMerchants.add(vasMerchant);
        }
        
        return vasMerchants;
        
    }
    
    public String generateVoucher(double amount, String token, 
            String currency, String reference){
        
        Properties properties = Utility.getConfigProperty();
        
        String url = properties.getProperty("mobile_wallet_base_url");
        
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("amount", amount)
                  .put("currency", currency)
                  .put("reference", reference)
                  .put("merchant_token", token);
        
        String response = httpUtil.doHttpPost(url+"/vouchers/generate", jSONObject.toString(), getWalletHeader());
        
        logService.log("VOUCHER GENERATION RESPONSE: "+response);
        
        JSONObject responseObject = new JSONObject(response);
        
        String status = responseObject.optString("status");
        
        if(!"success".equalsIgnoreCase(status))
            return null;
        
        return responseObject.optJSONObject("data").optString("voucher");
    }
    
    
    public VoucherFundingResponse applyVoucher(String voucher, String token){
        
        Properties properties = Utility.getConfigProperty();
        
        String url = properties.getProperty("mobile_wallet_base_url");
        
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("token", token)
                  .put("voucher", voucher);
        
        String response = httpUtil.doHttpPost(url+"/voucher/redeem", jSONObject.toString(), getWalletHeader());
        
        logService.log("VOUCHER REDEMPTION RESPONSE: "+response);
        
        JSONObject responseObject = new JSONObject(response);
        
        String status = responseObject.optString("status");
        
        if(!"success".equalsIgnoreCase(status))
            return null;
        
        JSONObject dataObject = responseObject.optJSONObject("data");
        
        VoucherFundingResponse fundingResponse = new VoucherFundingResponse();
        
        fundingResponse.setBalanceAfter(dataObject.optDouble("balance"));
        
        fundingResponse.setBalanceBefore(dataObject.optDouble("balance") -  dataObject.optDouble("amount"));
        
        return fundingResponse;
    }
    
    
}

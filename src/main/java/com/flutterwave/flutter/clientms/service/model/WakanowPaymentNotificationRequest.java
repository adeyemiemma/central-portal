/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanueladeyemi
 */
@XmlRootElement(name = "PaymentNotificationRequest")
public class WakanowPaymentNotificationRequest {

    /**
     * @return the hash
     */
    @XmlElement(name = "Hash")
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return the version
     */
    @XmlAttribute(name = "Version")
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the items
     */
    @XmlElementWrapper(name = "Payments")
    @XmlElement(name = "Payment")
    public List<WakanowPayment> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<WakanowPayment> items) {
        this.items = items;
    }
    
   private List<WakanowPayment> items;
   private String version = "2";
   private String hash;
}

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.flutterwave.flutter.clientms.service;
//
//import com.flutterwave.flutter.clientms.dao.FlutterwaveSettlementDao;
//import com.flutterwave.flutter.clientms.dao.ProviderDao;
//import com.flutterwave.flutter.clientms.dao.TransactionDao;
//import com.flutterwave.flutter.clientms.model.FlutterwaveSettlement;
//import com.flutterwave.flutter.clientms.model.Log;
//import com.flutterwave.flutter.clientms.model.Provider;
//import com.flutterwave.flutter.clientms.model.Transaction;
//import com.flutterwave.flutter.clientms.util.CreateRuleModel;
//import com.flutterwave.flutter.clientms.util.CreateTransactionModel;
//import com.flutterwave.flutter.clientms.util.HttpUtil;
//import com.flutterwave.flutter.clientms.util.LogDomain;
//import com.flutterwave.flutter.clientms.util.LogLevel;
//import com.flutterwave.flutter.clientms.util.LogState;
//import com.flutterwave.flutter.clientms.util.LogStatus;
//import java.io.Serializable;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import javax.ejb.ConcurrencyManagement;
//import javax.ejb.ConcurrencyManagementType;
//import javax.ejb.EJB;
//import javax.ejb.LocalBean;
//import javax.ejb.Singleton;
//import javax.ejb.Startup;
//import javax.jms.Connection;
//import javax.jms.Destination;
//import javax.jms.ExceptionListener;
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageConsumer;
//import javax.jms.MessageListener;
//import javax.jms.ObjectMessage;
//import javax.jms.Session;
//import javax.jms.TextMessage;
//import javax.json.Json;
//import javax.json.JsonArrayBuilder;
//import javax.json.JsonObjectBuilder;
//import org.apache.activemq.ActiveMQConnectionFactory;
//
///**
// *
// * @author emmanueladeyemi
// */
//@Startup
//@LocalBean
//@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
//@Singleton
//public class SettlementQueueListener implements MessageListener, Runnable, ExceptionListener{
//
//    private final static String URL_STRING = "tcp://localhost:61616";
//    private final static String QUEUE_NAME = "SETTLEMENT_QUEUE";
//    
//    Session session;
//    Connection connection;
//            
//    @EJB
//    private LogService logService;
//    @EJB
//    private HttpUtil httpUtil;
//    
//    @EJB
//    FlutterwaveSettlementDao flutterwaveSettlementDao;
//    @EJB
//    private FlutterwaveSettlementQueueManager queueManager;
//    
//    
//    @PostConstruct
//    public void startup(){
//         
//        run();
//    }
//    
//    public void run() {
//
//        try {
//
//            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL_STRING);
//
//            // Create a Connection
//            connection = connectionFactory.createConnection();
//            
//            connection.start();
//
//            connection.setExceptionListener(this);
//            
//            // Create a Session
//            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//
//            // Create the destination (Topic or Queue)
//            Destination destination = session.createQueue(QUEUE_NAME);
//
////            session.close();
//            // Create a MessageConsumer from the Session to the Topic or Queue
//            MessageConsumer consumer = session.createConsumer(destination);
//            
//            consumer.setMessageListener(this);
//            
//        } catch (JMSException ex) {
//            Logger.getLogger(SettlementQueueListener.class.getName()).log(Level.SEVERE, null, ex);
//        }catch(Exception ex){
//            Logger.getLogger(SettlementQueueListener.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//    }
//    
//    
//    @Override
//    public synchronized void onException(JMSException jmse) {
//        System.out.println("JMS Exception occured.  Shutting down client.");
//    }
//
//    @Override
//    public void onMessage(Message message) {
//
//        try {
//            if (message instanceof TextMessage) {
//                TextMessage textMessage = (TextMessage) message;
//                String text = textMessage.getText();
////                System.out.println("Received: " + text);
//            } else {
////                System.out.println("Received: " + message);
//            }
//
//            Log log = new Log();
//            log.setAction("SUCCESS TRansaxction Queue Called back");
//            log.setCreatedOn(new Date());
//            log.setLevel(LogLevel.Info);
//            log.setLogDomain(LogDomain.ADMIN);
//            log.setDescription("processing successful callback queue");
//            log.setLogState(LogState.STARTED);
//            log.setStatus(LogStatus.SUCCESSFUL);
////            log.setLogDomainId(0);
//            logService.log(log);
//
//            
//            if (message instanceof ObjectMessage) {                
//                
//                try{
//                    
//                    Serializable serializable = ((ObjectMessage) message).getObject();
//                    
//                    if(serializable instanceof FlutterwaveSettlement){
//                        
//                        FlutterwaveSettlement settlement = (FlutterwaveSettlement) serializable;
//                        
//                        if(settlement.isRuleStatus() == false){
//                            
//                            CreateRuleModel createRuleModel = new CreateRuleModel();
//                            createRuleModel.setCreditAccountNo(settlement.getCreditAccountNo());
//                            createRuleModel.setCreditBankCode(settlement.getCreditBankCode());
//                            createRuleModel.setCreditCountry(settlement.getCreditCountry());
//                            createRuleModel.setDebitAccountNo(settlement.getDebitAccountNo());
//                            createRuleModel.setDebitBankCode(settlement.getDebitBankCode());
//                            createRuleModel.setDebitCountry(settlement.getDebitCountry());
//                            
//                            String token = httpUtil.createRule(createRuleModel);
//                            
//                            if(token == null){
//                                
//                                log = new Log();
//                                log.setAction("Settlement create rule");
//                                log.setCreatedOn(new Date());
//                                log.setLevel(LogLevel.Info);
//                                log.setLogDomain(LogDomain.ADMIN);
//                                log.setDescription("successful callback queue processed forsettleent with id "+settlement.getId());
//                                log.setLogState(LogState.FINISH);
//                                log.setStatus(LogStatus.FAILED);
//                                
//                                settlement.setRuleCreationTrial(settlement.getRuleCreationTrial() + 1);
//                                
//                                settlement.setRuleCreatedOn(new Date());
//                                flutterwaveSettlementDao.update(settlement);
//                                
//                                if(settlement.getRuleCreationTrial() < 2){
//                                    queueManager.queueSettlement(settlement);
//                                }
//   
//                            }else{
//                            
//                                settlement.setSettlementToken(token);
//                                settlement.setRuleStatus(true);
//                                settlement.setRuleCreatedOn(new Date());
//                                settlement.setRuleCreationTrial(settlement.getRuleCreationTrial() + 1);
//                                
//                                flutterwaveSettlementDao.update(settlement);
//                                
//                                CreateTransactionModel transactionModel = new CreateTransactionModel();
//                                transactionModel.setAmount(settlement.getAmount()+"");
//                                transactionModel.setCurrency(settlement.getCurrency());
//                                transactionModel.setSettlementToken(token);
//                                transactionModel.setTransactionReference(settlement.getTransactionReference());
//                                transactionModel.setNarration(settlement.getNarration());
//                                
//                                settlement.setTransactionCreatedOn(new Date());
//                                Map<String, String> data  =  httpUtil.createTransaction(transactionModel);
//                                
//                                String reference =  data.getOrDefault("reference", null);
//                                
//                                if(reference == null){
//                                    
//                                    log = new Log();
//                                    log.setAction("Settlement create transaction");
//                                    log.setCreatedOn(new Date());
//                                    log.setLevel(LogLevel.Info);
//                                    log.setLogDomain(LogDomain.ADMIN);
//                                    log.setDescription("successful callback queue processed forsettleent with id "+settlement.getId());
//                                    log.setLogState(LogState.FINISH);
//                                    log.setStatus(LogStatus.FAILED);
//
//                                    settlement.setTransactionCreationTrial(settlement.getTransactionCreationTrial() + 1);
//                                    
//                                    flutterwaveSettlementDao.update(settlement);
//                                    
//                                    if(settlement.getTransactionCreationTrial() < 2 )
//                                        queueManager.queueSettlement(settlement);
//                                    
//                                }else{
//                                    
//                                    settlement.setTransactionStatus(true);
//                                    settlement.setInternalReference(reference);
//                                
//                                    flutterwaveSettlementDao.update(settlement);
//                                    
//                                }
//                            }
//                        }else if(settlement.isTransactionStatus() == false){
//                            
//                            CreateTransactionModel transactionModel = new CreateTransactionModel();
//                            transactionModel.setAmount(settlement.getAmount()+"");
//                            transactionModel.setCurrency(settlement.getCurrency());
//                            transactionModel.setSettlementToken(settlement.getSettlementToken());
//                            transactionModel.setTransactionReference(settlement.getTransactionReference());
//                            transactionModel.setNarration(settlement.getNarration());
//
//                            settlement.setTransactionCreatedOn(new Date());
//                            Map<String, String> data =  httpUtil.createTransaction(transactionModel);
//
//                            String reference =  data.getOrDefault("reference", null);
//                            
//                            if(reference == null){
//
//                                log = new Log();
//                                log.setAction("Settlement create transaction");
//                                log.setCreatedOn(new Date());
//                                log.setLevel(LogLevel.Info);
//                                log.setLogDomain(LogDomain.ADMIN);
//                                log.setDescription("successful callback queue processed for settleent with id "+settlement.getId());
//                                log.setLogState(LogState.FINISH);
//                                log.setStatus(LogStatus.FAILED);
//
//                                settlement.setTransactionCreationTrial(settlement.getTransactionCreationTrial() + 1);
//
//                                flutterwaveSettlementDao.update(settlement);
//
//                                if(settlement.getTransactionCreationTrial() < 2 )
//                                    queueManager.queueSettlement(settlement);
//
//                            }else{
//
//                                settlement.setTransactionStatus(true);
//                                settlement.setInternalReference(reference);
//
//                                flutterwaveSettlementDao.update(settlement);
//
//                            }
//                        }
//                    }
//                }catch(Exception ex){
//                    if(ex != null)
//                        ex.printStackTrace();
//                }
//
//                
////                if(map != null)
//                
//            }
//        } catch (Exception ex) {
//            if (ex != null) {
//                ex.printStackTrace();
//            }
//        }
//    }
//    
//    @PreDestroy
//    public void tidyUp(){
//        
//        try{
//            
//            if(session != null)
//                session.close();
//            
//            if(connection != null)
//                connection.close();
//            
//        }catch(Exception ex){
//            if(ex != null)
//                ex.printStackTrace();;
//        }
//    }
//    
//    
//}

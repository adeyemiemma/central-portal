/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.TransactionDao;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class TransactionUtil {
    
    @EJB
    private TransactionDao transactionDao;
    
    @Inject
    private SimpleCache simpleCache;
    
    public Map<String, String> getCoreResponses(String category){
        
        
        Object data = simpleCache.get(category);
        
        if(data == null){
            
            Map<String, String> array = transactionDao.getResponseNew(category);
            
            simpleCache.add(category, array);
            
            return array;
        }
        
        
        return (Map<String, String>) data;
        
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.PosMerchantCustomerDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.dao.PosOrderDao;
import com.flutterwave.flutter.clientms.dao.PosTerminalDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionDao;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosMerchantCustomer;
import com.flutterwave.flutter.clientms.model.PosOrder;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.model.PosTransactionNew;
import com.flutterwave.flutter.clientms.util.PosMerchantCategory;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Singleton
@Startup
public class PosReversalSchedulerService {
    
    @Inject
    private PosReversalCache posReversalCache;
    
    @EJB
    private PosTransactionDao posTransactionDao;
    
    @EJB
    private PosTerminalDao posTerminalDao;
    
    @EJB
    private PosMerchantDao posMerchantDao;
    
    @EJB
    private PosOrderDao posOrderDao;
    
    @EJB
    private PosMerchantCustomerDao posMerchantCustomerDao;
    
    @EJB
    private UtilityService utilityService;
    
    
    
    @Schedule(minute = "10", hour = "*", persistent = false)
    public void processItexReversalOfTransactions(){
        
        String processReversal = Utility.getConfigProperty().getProperty("process_pos_reversal_queue");
        
        if(processReversal != null && "no".equalsIgnoreCase(processReversal))
            return;
        
       if( posReversalCache.getSize() <= 0)
           return;
       
       posReversalCache.getMap().keySet().forEach((String key) -> {
           try{
               
               Map<String, String> object = (Map<String, String>) posReversalCache.get(key);
               
               boolean processed = processPosTransaction(object, "itex");
               
               if(processed == true)
                   posReversalCache.getMap().remove(key);
               
           }catch(DatabaseException ex){
               
               ex.printStackTrace();
           }
        });
       
    }
    
    private boolean processPosTransaction(Map<String, String> data, String provider) throws DatabaseException{
        
        PosMerchantCustomer posMerchantCustomer = null;
        
        PosMerchant merchant = null;
        
        String merchantReference = null;
                    
        String orderId = null, customerId = null;
                    
        JSONObject object = new JSONObject(data);

            switch (provider.toLowerCase()) {

                case "itex": {

                    String terminalId = object.optString("terminalid");

//                    response.put("terminalid", terminalId);

                    PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", terminalId);

                    String posId = object.optString("posid");

                    if (Utility.emptyToNull(posId) != null) {

                        merchant = posMerchantDao.findByKey("posId", posId);

                        if (merchant == null) {

                            PosOrder order = posOrderDao.findByKey("posOrderId", posId);

                            if (order != null) {

                                merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());

                                orderId = order.getCustomerOrderId();

                            } else {

                                posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", posId);

                                if (posMerchantCustomer == null) {

                                    PosMerchant posMerchant = posTerminal.getMerchant();

                                    if (posMerchant != null) {

//                                        if (posMerchant.getCategory() == PosMerchantCategory.NPP) {

                                            merchant = posMerchant;

                                            merchantReference = posId;
//                                        }

                                    } else {

                                        return false;
                                    }
                                }

                                merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());

                                customerId = posMerchantCustomer.getCustomerId();

                            }
                        }
                    }

                    if (posTerminal == null) {

                        return true;
                    }
                    
                    if(merchant == null)
                        merchant = posTerminal.getMerchant();

                    String amountString = object.optString("amount");

                    double amount = Double.parseDouble(amountString);

                    amount = amount / 100;
                    
                    PosTransaction posTransaction = new PosTransaction();

                    posTransaction.setAmount(amount);

                    posTransaction.setCreatedOn(new Date());
                    posTransaction.setCurrency("NGN");
                    posTransaction.setCurrencyCode("566");

                    String requestDateString = object.optString("transactiontime");

//                    Date requestDate = Utility.parseDate(requestDateString, "yyyy-MM-dd HH:mm:ss");
                    
                    Date requestDate = null; // Utility.parseDate(requestDateString, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    
//                    if(requestDate == null || Utility.isExpired(requestDate) == true){
//                        requestDate = Utility.parseDate(requestDateString, "yyyy-MM-dd HH:mm:ss");
//                    }
                              
                    if(requestDate == null || Utility.isExpired(requestDate) == true)
                        requestDate = Utility.parseDate(requestDateString, "dd-MM-yyyy HH:mm:ss");

                    String rrn = object.optString("rrn");

                    try {

                        Long rrnLong = Long.parseLong(rrn);

                        rrn = String.valueOf(rrnLong);

                    } catch (Exception ex) {

                        return false;
                    }

                    
                    PosTransaction transaction;
                    
                    if(requestDate == null)
                        transaction = posTransactionDao.findRRNTerminal(rrn, terminalId, requestDate);
                    else
                        transaction = posTransactionDao.findRRNTerminalOnly(rrn, terminalId);
                    
                    Boolean reversal = object.getBoolean("reversal");
                    
                    if(Objects.equals(reversal, Boolean.TRUE) && transaction == null){


                        return false ;
                    }

                    if (transaction != null) {

                        if (Objects.equals(reversal, Boolean.TRUE) && !transaction.isReversed()) {

                            transaction.setReversed(true);
                            transaction.setReversedOn(requestDate);

                            posTransactionDao.update(transaction);
                            
                            utilityService.callBackUser(posTransaction, orderId, posMerchantCustomer);

                            return true;
                        }

                       return false;
                    }

                    

                    return false;
                }
                default: {

                    return true ;
                }
                    
            }
    }
}

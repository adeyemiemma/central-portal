///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.flutterwave.flutter.clientms.service.recon;
//
//import com.flutterwavego.automated.recon.system.util.Global;
//import java.io.IOException;
//import javax.inject.Inject;
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletContext;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// *
// * @author emmanueladeyemi
// */
//@WebFilter(urlPatterns = {"/*"}, filterName = "emma")
//public class MyFilter implements Filter {
//
//    @Inject
//    private Global global;
//    
//    private ServletContext context;
//    
//    
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//        this.context = filterConfig.getServletContext();
//    }
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        
//        String url = ((HttpServletRequest) request).getRequestURL().toString();
//
//        String domainId = global.getDomainId();
//        
//        if(domainId != null && "access bank".equalsIgnoreCase(domainId)){
//            
//            if(url.contains("login.xhtml")){
//                
//                HttpServletResponse httpResponse = (HttpServletResponse) response;
//                httpResponse.sendRedirect( url.substring(0,url.indexOf("login.xhtml"))+"accessbank.xhtml");
//                
//                global.setDomainId(null);
//                return;
//            }
//        }
//        
//        chain.doFilter(request, response);
//    }
//
//    @Override
//    public void destroy() {
//        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    
//    
//}

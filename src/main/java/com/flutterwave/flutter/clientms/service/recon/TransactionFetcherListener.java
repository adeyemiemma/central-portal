///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.flutterwave.flutter.clientms.service.recon;
//
//import com.flutterwave.flutter.core.exception.DatabaseException;
//import com.flutterwave.flutter.core.util.Page;
//import com.flutterwavego.automated.recon.system.dao.BatchDao;
//import com.flutterwavego.automated.recon.system.dao.BatchResolvedDao;
//import com.flutterwavego.automated.recon.system.dao.FlutterTransactionDao;
//import com.flutterwavego.automated.recon.system.dao.TransactionDao;
//import com.flutterwavego.automated.recon.system.model.Batch;
//import com.flutterwavego.automated.recon.system.model.BatchResolved;
//import com.flutterwavego.automated.recon.system.model.FetchTransactionRequest;
//import com.flutterwavego.automated.recon.system.model.FlutterTransaction;
//import com.flutterwavego.automated.recon.system.model.Log;
//import com.flutterwavego.automated.recon.system.model.ResolveTransactionRequest;
//import com.flutterwavego.automated.recon.system.model.Transaction;
//import com.flutterwavego.automated.recon.system.model.User;
//import com.flutterwavego.automated.recon.system.util.LogLevel;
//import com.flutterwavego.automated.recon.system.util.LogStatus;
//import com.flutterwavego.automated.recon.system.util.PageResult;
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//import javax.ejb.ActivationConfigProperty;
//import javax.ejb.Asynchronous;
//import javax.ejb.EJB;
//import javax.ejb.MessageDriven;
//import javax.ejb.Stateless;
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageListener;
//import javax.jms.ObjectMessage;
//import javax.transaction.Transactional;
//import javax.ws.rs.GET;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.QueryParam;
//import javax.ws.rs.core.MediaType;
//
///**
// *
// * @author emmanueladeyemi
// */
//@MessageDriven(activationConfig = {
//        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
//        @ActivationConfigProperty(propertyName = "destination", propertyValue = "activemq/queue/TransactionQueue") 
//    }
//)
//public class TransactionFetcherListener implements MessageListener {
//
//    @EJB
//    private LogService logService;
//    @EJB
//    private TransactionFetcherService fetcherService;
//    @EJB
//    private BatchDao batchDao;
//    @EJB
//    private BatchResolvedDao batchResolvedDao;
//    @EJB
//    private TransactionDao transactionDao;
//    @EJB
//    private FlutterTransactionDao flutterTransactionDao;
//    
//    @Override
//    public void onMessage(Message msg) {
//        
//        if(msg instanceof ObjectMessage){
//            
//            try {
//                
//                Log log = new Log();
//                log.setAction("CALLBACK TO PULL TRANSACTION");
//                log.setCreatedOn(new Date());
//                log.setDescription("Callback to pull data started");
//                log.setLevel(LogLevel.Info); 
//                logService.log(log);
//                
//                Serializable serializable = ((ObjectMessage) msg).getObject();
//                
//                if(serializable instanceof  FetchTransactionRequest) {
//                    FetchTransactionRequest request = (FetchTransactionRequest)serializable;
//                    logService.log(request.toString());
//                    fetcherService.fetchTransaction(request);
//                    
//                }else if(serializable instanceof ResolveTransactionRequest){
//                    ResolveTransactionRequest rtr = (ResolveTransactionRequest)serializable;
//                    resolveTransactions(rtr.getBatchId());
//                }
//                
//                log = new Log();
//                log.setAction("CALLBACK TO PULL TRANSACTION");
//                log.setCreatedOn(new Date());
//                log.setDescription("Callback to pull data done");
//                log.setLevel(LogLevel.Info); 
//                logService.log(log);
//                
//            } catch (JMSException ex) {
//                Logger.getLogger(TransactionFetcherListener.class.getName()).log(Level.SEVERE, null, ex);
//                
//                try{
//                    Log log = new Log();
//                    log.setAction("CALLBACK MESSAGE");
//                    log.setCreatedOn(new Date());
//                    log.setDescription("CALLBACK MESSAGE FAILED");
//                    log.setLevel(LogLevel.Severe);
//                    log.setStatus(LogStatus.FAILED);
//
//                    if(ex != null){
////                        log.setStatusMessage(ex.getMessage());
//
//                        String detailedString = Stream.of(ex.getStackTrace())
//                                .map(x -> x.toString()).collect(Collectors.joining(","));
//                        logService.log(log, detailedString);
//                    }
//                    else
//                        logService.log(log);
//                }catch(Exception e){
//                }
//            }
//            
//            
//        }
//    }
//    
//    @Asynchronous
//    @Transactional
//    public void resolveTransactions(String batchId){
//        
//        try {
//            Batch batch = batchDao.findByKey("batchId", batchId);
//            
//            List<Transaction> transactions =  transactionDao.findByBatch(batch);
//            
//            BatchResolved batchResolved = new BatchResolved();
//            batchResolved.setBatchId(batch.getBatchId());
//            batchResolved.setCreatedOn(new Date());
//            batchResolved.setCount(batch.getCount());
//            
//            if(transactions != null && !transactions.isEmpty()){
//                
//                long resolved = 0;
//                
//                for(Transaction transaction : transactions){
//                    
//                    List<FlutterTransaction> list = flutterTransactionDao.findApproved(transaction.getRrn());
//                    
//                    if(list == null || list.isEmpty())
//                        continue;
//                    
//                    transaction.setFlutterTransaction(list.get(0));
//                    transaction.setSettled(true);
//                    transaction.setSettlementDate(new Date());
//                    transactionDao.update(transaction);
//                    resolved++;
//                }
//                
//                batchResolved.setResolved(resolved);
//            }
//            
//            batchResolvedDao.create(batchResolved);
//            
//        } catch (DatabaseException ex) {
//            Logger.getLogger(TransactionFetcherListener.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//    }
//    
//}

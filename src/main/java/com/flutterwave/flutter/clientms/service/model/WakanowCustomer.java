/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanueladeyemi
 */
@XmlRootElement(name = "Customer")
public class WakanowCustomer {

    /**
     * @return the status
     */
    @XmlElement(name = "Status")
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the custReference
     */
    @XmlElement(name = "CustReference")
    public String getCustReference() {
        return custReference;
    }

    /**
     * @param custReference the custReference to set
     */
    public void setCustReference(String custReference) {
        this.custReference = custReference;
    }

    /**
     * @return the customerReferenceAlternate
     */
    @XmlElement(name = "CustomerReferenceAlternate")
    public String getCustomerReferenceAlternate() {
        return customerReferenceAlternate;
    }

    /**
     * @param customerReferenceAlternate the customerReferenceAlternate to set
     */
    public void setCustomerReferenceAlternate(String customerReferenceAlternate) {
        this.customerReferenceAlternate = customerReferenceAlternate;
    }

    /**
     * @return the customerReferenceDescription
     */
    @XmlElement(name = "CustomerReferenceDescription")
    public String getCustomerReferenceDescription() {
        return customerReferenceDescription;
    }

    /**
     * @param customerReferenceDescription the customerReferenceDescription to set
     */
    public void setCustomerReferenceDescription(String customerReferenceDescription) {
        this.customerReferenceDescription = customerReferenceDescription;
    }

    /**
     * @return the firstName
     */
    @XmlElement(name = "FirstName")
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    @XmlElement(name = "LastName")
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the phone
     */
    @XmlElement(name = "Phone")
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the email
     */
    @XmlElement(name = "Email")
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the amount
     */
    @XmlElement(name = "Amount")
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the items
     */
    @XmlElement(name = "PaymentItems")
    public List<WakanowCustomerPaymentItem> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<WakanowCustomerPaymentItem> items) {
        this.items = items;
    }
    
    private Integer status;
    private String custReference;
    private String customerReferenceAlternate;
    private String customerReferenceDescription;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String amount;
    private List<WakanowCustomerPaymentItem> items;
    
}

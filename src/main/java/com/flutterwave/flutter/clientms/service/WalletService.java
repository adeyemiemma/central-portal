/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.WalletDao;
import com.flutterwave.flutter.clientms.dao.WalletTransactionDao;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.Wallet;
import com.flutterwave.flutter.clientms.model.WalletTransaction;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.util.WalletTransactionType;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class WalletService {
    
    @EJB
    private WalletDao walletDao;
    @EJB
    private WalletTransactionDao walletTransactionDao;
    
    @Transactional
    public boolean fundWallet(Provider provider, double amount, Currency currency, User user){
        
        try {
            
            Wallet wallet = walletDao.findByQuery(null, provider.getShortName(), Utility.WalletCategory.SOURCE, currency);
            
            wallet.setAmount(wallet.getAmount() + amount);
            
            WalletTransaction transaction = new WalletTransaction();
            transaction.setWallet(wallet);
            transaction.setAmount(amount);
            transaction.setCreatedBy(user);
            transaction.setApprovedBy(user);
            transaction.setApprovedOn(new Date());
            transaction.setCreatedOn(new Date());
            transaction.setTransactionType(WalletTransactionType.CREDIT);
            
            transaction = walletTransactionDao.create(transaction);
            walletDao.update(wallet);
            
            return transaction != null;
        } catch (DatabaseException ex) {
            Logger.getLogger(WalletService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    @Transactional
    public boolean fundWallet(Product product, String id, double amount, Currency currency, User user){
        
        try {
            
            Wallet wallet = walletDao.findByQuery(product, id, Utility.WalletCategory.MERCHANT, currency);
            
            wallet.setAmount(wallet.getAmount() + amount);
            
            WalletTransaction transaction = new WalletTransaction();
            transaction.setWallet(wallet);
            transaction.setAmount(amount);
            transaction.setCreatedBy(user);
            transaction.setApprovedBy(user);
            transaction.setApprovedOn(new Date());
            transaction.setCreatedOn(new Date());
            transaction.setTransactionType(WalletTransactionType.CREDIT);
            
            transaction = walletTransactionDao.create(transaction);
            walletDao.update(wallet);
            
            return transaction != null;
        } catch (DatabaseException ex) {
            Logger.getLogger(WalletService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
     @Transactional
    public boolean debitWallet(Provider provider, double amount, Currency currency, User user){
        
        try {
            
            Wallet wallet = walletDao.findByQuery(null, provider.getShortName(), Utility.WalletCategory.SOURCE, currency);
            
            wallet.setAmount(wallet.getAmount() - amount);
            
            WalletTransaction transaction = new WalletTransaction();
            transaction.setWallet(wallet);
            transaction.setAmount(amount);
            transaction.setCreatedBy(user);
            transaction.setApprovedBy(user);
            transaction.setApprovedOn(new Date());
            transaction.setCreatedOn(new Date());
            transaction.setTransactionType(WalletTransactionType.DEBIT);
            
            walletDao.update(wallet);
            
            transaction = walletTransactionDao.create(transaction);
            
            return transaction != null;
        } catch (DatabaseException ex) {
            Logger.getLogger(WalletService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    @Transactional
    public boolean debitWallet(Product product, String id, double amount, Currency currency, User user){
        
        try {
            
            Wallet wallet = walletDao.findByQuery(product, id, Utility.WalletCategory.MERCHANT, currency);
            
            wallet.setAmount(wallet.getAmount() - amount);
            
            WalletTransaction transaction = new WalletTransaction();
            transaction.setWallet(wallet);
            transaction.setAmount(amount);
            transaction.setCreatedBy(user);
            transaction.setApprovedBy(user);
            transaction.setApprovedOn(new Date());
            transaction.setCreatedOn(new Date());
            transaction.setTransactionType(WalletTransactionType.DEBIT);
            
            transaction = walletTransactionDao.create(transaction);
            
            walletDao.update(wallet);
            
            return transaction != null;
        } catch (DatabaseException ex) {
            Logger.getLogger(WalletService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
}

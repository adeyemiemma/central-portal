/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.service.model.SetupCardMerchantModel;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.Utility;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class CardUtilService {
    
    @EJB
    private HttpUtil httpUtil;
    
    public String setUpMerchantOnCoreAndProvisionCard(SetupCardMerchantModel merchantModel){
        
        
        Properties properties = Utility.getConfigProperty();
        
        JSONObject jSONObject = new JSONObject()
                .put("bank_code", merchantModel.getBankCode())
                .put("program_id", properties.getProperty("amadeus_program_id"))
                .put("customer_id", merchantModel.getCustomerId())
                .put("business_name", merchantModel.getBusinessName())
                .put("email", merchantModel.getEmail())
                .put("first_name", merchantModel.getFirstName())
                .put("last_name", merchantModel.getLastName())
                .put("phone", merchantModel.getPhone())
                .put("account_no", merchantModel.getAccountNo());
        
        
        String baseUrl = properties.getProperty("flw_merchant_ms_url");
        String key = properties.getProperty("flw_merchant_ms_key");

        Map<String,String> header = new HashMap<>();
        header.put("authorization", "Bearer "+key);
        header.put("content-type", "application/json");
        
        String response = httpUtil.doHttpPost(baseUrl+"/merchant/platform/setup", jSONObject.toString(), header);
        
        if(response == null){
            
            return null;
        }
           
        
        JSONObject responseObject = new JSONObject(response);
        
        String status =  responseObject.optString("status");
        
        if(!"success".equalsIgnoreCase(status))
            return null;
        
        String token = responseObject.optString("token");
        
        
        return token;
    }
}

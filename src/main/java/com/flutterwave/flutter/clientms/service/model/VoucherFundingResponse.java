/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

/**
 *
 * @author emmanueladeyemi
 */
public class VoucherFundingResponse {

    /**
     * @return the balanceAfter
     */
    public double getBalanceAfter() {
        return balanceAfter;
    }

    /**
     * @param balanceAfter the balanceAfter to set
     */
    public void setBalanceAfter(double balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    /**
     * @return the balanceBefore
     */
    public double getBalanceBefore() {
        return balanceBefore;
    }

    /**
     * @param balanceBefore the balanceBefore to set
     */
    public void setBalanceBefore(double balanceBefore) {
        this.balanceBefore = balanceBefore;
    }
    
    private double balanceAfter;
    private double balanceBefore;
}

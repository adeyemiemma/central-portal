/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanueladeyemi
 */
@XmlRootElement(name = "Payment")
public class WakanowPayment {

    /**
     * @return the paymentCurrencyCode
     */
    @XmlElement(name = "PaymentCurrencyCode")
    public String getPaymentCurrencyCode() {
        return paymentCurrencyCode;
    }

    /**
     * @param paymentCurrencyCode the paymentCurrencyCode to set
     */
    public void setPaymentCurrencyCode(String paymentCurrencyCode) {
        this.paymentCurrencyCode = paymentCurrencyCode;
    }


    /**
     * @return the paymentLogId
     */
    @XmlElement(name = "PaymentLogId")
    public String getPaymentLogId() {
        return paymentLogId;
    }

    /**
     * @param paymentLogId the paymentLogId to set
     */
    public void setPaymentLogId(String paymentLogId) {
        this.paymentLogId = paymentLogId;
    }

    /**
     * @return the custReference
     */
    @XmlElement(name = "CustReference")
    public String getCustReference() {
        return custReference;
    }

    /**
     * @param custReference the custReference to set
     */
    public void setCustReference(String custReference) {
        this.custReference = custReference;
    }

    /**
     * @return the alternateCustReference
     */
    @XmlElement(name = "AlternateCustReference")
    public String getAlternateCustReference() {
        return alternateCustReference;
    }

    /**
     * @param alternateCustReference the alternateCustReference to set
     */
    public void setAlternateCustReference(String alternateCustReference) {
        this.alternateCustReference = alternateCustReference;
    }

    /**
     * @return the amount
     */
    @XmlElement(name = "Amount")
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the paymentMethod
     */
    @XmlElement(name = "PaymentMethod")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * @param paymentMethod the paymentMethod to set
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * @return the paymentReference
     */
    @XmlElement(name = "PaymentReference")
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * @param paymentReference the paymentReference to set
     */
    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    /**
     * @return the terminalId
     */
    @XmlElement(name = "TerminalId")
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the channelName
     */
    @XmlElement(name = "ChannelName")
    public String getChannelName() {
        return channelName;
    }

    /**
     * @param channelName the channelName to set
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    /**
     * @return the location
     */
    @XmlElement(name = "Location")
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the paymentDate
     */
    @XmlElement(name = "PaymentDate")
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the institutionId
     */
    @XmlElement(name = "InstitutionId")
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the institutionName
     */
    @XmlElement(name = "InstitutionName")
    public String getInstitutionName() {
        return institutionName ;
    }

    /**
     * @param institutionName the institutionName to set
     */
    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    /**
     * @return the branchName
     */
    @XmlElement(name = "BranchName")
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchName to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * @return the bankName
     */
    @XmlElement(name = "BankName")
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the customerName
     */
    @XmlElement(name = "CustomerName")
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the receiptNumber
     */
    @XmlElement(name = "ReceiptNumber")
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * @param receiptNumber the receiptNumber to set
     */
    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    /**
     * @return the collectionsAccount
     */
    @XmlElement(name = "CollectionAccount")
    public String getCollectionsAccount() {
        return collectionsAccount;
    }

    /**
     * @param collectionsAccount the collectionsAccount to set
     */
    public void setCollectionsAccount(String collectionsAccount) {
        this.collectionsAccount = collectionsAccount;
    }

    /**
     * @return the bankCode
     */
    @XmlElement(name = "BankCode")
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the customerAddress
     */
    @XmlElement(name = "CustomerAddress")
    public String getCustomerAddress() {
        return customerAddress;
    }

    /**
     * @param customerAddress the customerAddress to set
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     * @return the customerPhoneNumber
     */
    @XmlElement(name = "CustomerPhoneNumber")
    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    /**
     * @param customerPhoneNumber the customerPhoneNumber to set
     */
    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    /**
     * @return the depositorName
     */
    @XmlElement(name = "DepositorName")
    public String getDepositorName() {
        return depositorName;
    }

    /**
     * @param depositorName the depositorName to set
     */
    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    /**
     * @return the depositSlipNumber
     */
    @XmlElement(name = "DepositorSlipNumber")
    public String getDepositSlipNumber() {
        return depositSlipNumber;
    }

    /**
     * @param depositSlipNumber the depositSlipNumber to set
     */
    public void setDepositSlipNumber(String depositSlipNumber) {
        this.depositSlipNumber = depositSlipNumber;
    }

    /**
     * @return the paymentCurrency
     */
    @XmlElement(name = "PaymentCurrency")
    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    /**
     * @param paymentCurrency the paymentCurrency to set
     */
    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    /**
     * @return the paymentItems
     */
    @XmlElementWrapper(name = "PaymentItems")
    @XmlElement(name = "PaymentItem")
    public List<WakanowPaymentItem> getPaymentItems() {
        return paymentItems;
    }

    /**
     * @param paymentItems the paymentItems to set
     */
    public void setPaymentItems(List<WakanowPaymentItem> paymentItems) {
        this.paymentItems = paymentItems;
    }

    /**
     * @return the productGroupCode
     */
    @XmlElement(name = "ProductGroupCode")
    public String getProductGroupCode() {
        return productGroupCode;
    }

    /**
     * @param productGroupCode the productGroupCode to set
     */
    public void setProductGroupCode(String productGroupCode) {
        this.productGroupCode = productGroupCode;
    }

    /**
     * @return the paymentStatus
     */
    @XmlElement(name = "PaymentStatus")
    public String getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * @param paymentStatus the paymentStatus to set
     */
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     * @return the reversal
     */
    @XmlElement(name = "IsReversal")
    public Boolean getReversal() {
        return reversal;
    }

    /**
     * @param reversal the reversal to set
     */
    public void setReversal(Boolean reversal) {
        this.reversal = reversal;
    }

    /**
     * @return the settlementDate
     */
    @XmlElement(name = "SettlementDate")
    public String getSettlementDate() {
        return settlementDate;
    }

    /**
     * @param settlementDate the settlementDate to set
     */
    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    /**
     * @return the teller
     */
    @XmlElement(name = "Teller")
    public String getTeller() {
        return teller;
    }

    /**
     * @param teller the teller to set
     */
    public void setTeller(String teller) {
        this.teller = teller;
    }

    /**
     * @return the originalPaymentLogId
     */
    @XmlElement(name = "OriginalPaymentLogId")
    public String getOriginalPaymentLogId() {
        return originalPaymentLogId;
    }

    /**
     * @param originalPaymentLogId the originalPaymentLogId to set
     */
    public void setOriginalPaymentLogId(String originalPaymentLogId) {
        this.originalPaymentLogId = originalPaymentLogId;
    }

    /**
     * @return the feeName
     */
    @XmlElement(name = "FeeName")
    public String getFeeName() {
        return feeName;
    }

    /**
     * @param feeName the feeName to set
     */
    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    /**
     * @return the OriginalPaymentReference
     */
    @XmlElement(name = "OriginalPaymentReference")
    public String getOriginalPaymentReference() {
        return OriginalPaymentReference;
    }

    /**
     * @param OriginalPaymentReference the OriginalPaymentReference to set
     */
    public void setOriginalPaymentReference(String OriginalPaymentReference) {
        this.OriginalPaymentReference = OriginalPaymentReference;
    }
    
    private String paymentLogId;
    private String custReference;
    private String alternateCustReference;
    private String amount;
    private String paymentMethod;
    private String paymentReference;
    private String terminalId;
    private String channelName = "WEB";
    private String location;
    private String paymentDate;
    private String institutionId;
    private String institutionName; // = "wakanow";
    private String branchName;
    private String bankName;
    private String customerName;
    private String receiptNumber;
    private String collectionsAccount;
    private String bankCode;
    private String customerAddress;
    private String customerPhoneNumber;
    private String depositorName;
    private String depositSlipNumber;
    private String paymentCurrency;
    private List<WakanowPaymentItem> paymentItems;
    private String productGroupCode;
    private String paymentStatus;
    private Boolean reversal;
    private String settlementDate;
    private String teller;
    private String originalPaymentLogId;
    private String feeName;
    private String OriginalPaymentReference;
    private String paymentCurrencyCode;
    
}

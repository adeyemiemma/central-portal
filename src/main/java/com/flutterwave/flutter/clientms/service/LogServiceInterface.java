/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.model.Log;
import javax.ejb.Remote;

/**
 *
 * @author emmanueladeyemi
 */
@Remote
public interface LogServiceInterface {
    
    public boolean log(Log log);
    public boolean log(Log log, String detailedDescription);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.AirtimeTransactionDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.model.AirtimeExportModel;
import com.flutterwave.flutter.clientms.model.AirtimeTransaction;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.util.FillManager;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.Layouter1;
import com.flutterwave.flutter.clientms.util.MultipartUtility;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.util.WordUtil;
import com.flutterwave.flutter.clientms.viewmodel.AirtimeTransactionExportModel;
import com.flutterwave.flutter.clientms.viewmodel.GenericTransactionExportModel;
import com.flutterwave.flutter.clientms.viewmodel.TransactionExportModel;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class NotificationManager {

    @EJB
    HttpUtil httpUtil;
    @EJB
    private AirtimeTransactionDao airtimeTransactionDao;
    @EJB
    private ConfigurationDao configurationDao;
    
    private final String username = Global.EMAIL_USERNAME;
    private final String password = Global.EMAIL_PASSWORD;
    
    private VelocityEngine velocityEngine;

    @PostConstruct
    public void init() {

        try {
            Properties props = new Properties();
//            props.put("resource.loader", "class");
//            props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
//            Properties props = new Properties();
            props.put("resource.loader", "class");
            props.put("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.SimpleLog4JLogSystem");
            props.put("runtime.log.logsystem.log4j.category", "velocity");
            props.put("runtime.log.logsystem.log4j.logger", "velocity");
            props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            VelocityEngine engine = new VelocityEngine(props);
//            VelocityEngine engine = new VelocityEngine(props);
            engine.init();
            engine.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.CommonsLogLogChute");

            setVelocityEngine(engine);

        } catch (VelocityException ex) {
        }
    }

    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

    public VelocityEngine getVelocityEngine() {
        return velocityEngine;
    }

    @Asynchronous
    public void sendComplianceSuccessEmail(String email, String name, String product){
        
        try {
           
            VelocityContext context = new VelocityContext();
            context.put("name", name);
            Template template = getVelocityEngine().getTemplate("compliance.vm");
            
            StringWriter writer = new StringWriter();   
            template.merge(context, writer);

            String message = writer.toString();


            httpUtil.sendNotification(product+" Merchant Compliance","Flutterwave Compliance", "compliance@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));

            
        } catch (Exception ex) {
            Logger.getLogger(NotificationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @Asynchronous
    public void sendComplianceApprovedEmail(String email, String name, String product){
        
        try {
           
            VelocityContext context = new VelocityContext();
            context.put("name", name);
            context.put("product", product);
            Template template = getVelocityEngine().getTemplate("compliance_approved.vm");
            
            StringWriter writer = new StringWriter();   
            template.merge(context, writer);

            String message = writer.toString();


            httpUtil.sendNotification(product+" Merchant Compliance","Flutterwave Compliance", "compliance@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));
            
            
            context = new VelocityContext();
            context.put("name", name);
            context.put("product", product);
            context.put("operation", "approved");
            context.put("reason", " ");
            
            template = getVelocityEngine().getTemplate("compliance_admin_op.vm");
            
            writer = new StringWriter();   
            template.merge(context, writer);

            message = writer.toString();
            
            String emails =  configurationDao.getConfig("compliance_admin");
            
            if(emails == null)
                return ;
            
            if(!emails.toLowerCase().contains("customersuccess@flutterwavego.com"))
                emails += ",customersuccess@flutterwavego.com";
            
            String[] emailArray = emails.split(",");
            
            
            
            String adminMessage = message;
            
            Stream.of(emailArray).forEach(emailValue -> {
                httpUtil.sendNotification(product+" Merchant Compliance","Flutterwave Compliance", "compliance@flutterwavego.com", emailValue, "Admin", Base64.getEncoder().encodeToString(adminMessage.getBytes()));
            });
            
            
        } catch (Exception ex) {
            Logger.getLogger(NotificationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    @Asynchronous
    public void sendComplianceEmail(String email, String name, String product, String url, String messageBody){
        
        try {
           
            messageBody = messageBody.replace("_nl", "<br>");
            
            VelocityContext context = new VelocityContext();
            context.put("name", name);
            context.put("link", url);
            context.put("product", product);
            context.put("reason", messageBody);
            Template template = getVelocityEngine().getTemplate("compliance_message.vm");
            
            StringWriter writer = new StringWriter();   
            template.merge(context, writer);

            String message = writer.toString();


            httpUtil.sendNotification(product+" Merchant Compliance Update","Flutterwave Compliance", "compliance@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));
            
            
//            context = new VelocityContext();
//            context.put("name", name);
//            context.put("product", product);
//            context.put("reason", message);
//            
//            template = getVelocityEngine().getTemplate("compliance_admin_op.vm");
//            
//            writer = new StringWriter();   
//            template.merge(context, writer);
//
//            message = writer.toString();
//            
//            String emails =  configurationDao.getConfig("compliance_admin");
//            
//            if(emails == null)
//                return ;
//            
//            if(!emails.toLowerCase().contains("customersuccess@flutterwavego.com"))
//                emails += ",customersuccess@flutterwavego.com";
//            
//            String[] emailArray = emails.split(",");
//            
//            String adminMessage = message;
//            
//            Stream.of(emailArray).forEach(emailValue -> {
//                httpUtil.sendNotification(product+" Merchant Compliance","Flutterwave Compliance", "compliance@flutterwavego.com", emailValue, "Admin", Base64.getEncoder().encodeToString(adminMessage.getBytes()));
//            });
            
            
        } catch (Exception ex) {
            Logger.getLogger(NotificationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @Asynchronous
    public void sendComplianceRejectedEmail(String email, String name, String product, String url, String reason){
        
        try {
           
            reason = reason.replace("_nl", "<br>");
            
            VelocityContext context = new VelocityContext();
            context.put("name", name);
            context.put("link", url);
            context.put("product", product);
            context.put("reason", reason);
            Template template = getVelocityEngine().getTemplate("compliance_rejected.vm");
            
            StringWriter writer = new StringWriter();   
            template.merge(context, writer);

            String message = writer.toString();


            httpUtil.sendNotification(product+" Merchant Compliance","Flutterwave Compliance", "compliance@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));
            
            
            context = new VelocityContext();
            context.put("name", name);
            context.put("product", product);
            context.put("reason", reason);
            context.put("operation", "rejected with reason ");
            
            template = getVelocityEngine().getTemplate("compliance_admin_op.vm");
            
            writer = new StringWriter();   
            template.merge(context, writer);

            message = writer.toString();
            
            String emails =  configurationDao.getConfig("compliance_admin");
            
            if(emails == null)
                return ;
            
            if(!emails.toLowerCase().contains("customersuccess@flutterwavego.com"))
                emails += ",customersuccess@flutterwavego.com";
            
            String[] emailArray = emails.split(",");
            
            String adminMessage = message;
            
            Stream.of(emailArray).forEach(emailValue -> {
                httpUtil.sendNotification(product+" Merchant Compliance","Flutterwave Compliance", "compliance@flutterwavego.com", emailValue, "Admin", Base64.getEncoder().encodeToString(adminMessage.getBytes()));
            });
            
            
        } catch (Exception ex) {
            Logger.getLogger(NotificationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
//    @Asynchronous
    public boolean sendAccountCreationEmail(String password, String firstName, String lastName,
            String email, String domainName, String domainId) {

        try {

            VelocityContext context = new VelocityContext();
            context.put("domainName", "");
            context.put("firstName", firstName);
            context.put("lastName", lastName);
            context.put("username", email);
            context.put("password", password);
            context.put("link", "https://cp.coreflutterwaveprod.com/flw-clientms/"); // live
//            context.put("link", "http://flutterwavestaging.com:10100/flw-clientms/");

            String portalName = "bank".equalsIgnoreCase(domainName) ? domainId + " Reconciliation Portal" : domainId == null ? " Flutterwave Merchant Portal" : domainId + " Portal";

            context.put("portalName", portalName);

            Template template = getVelocityEngine().getTemplate("account_creation.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            String name = firstName == null ? "" : firstName + " " + lastName == null ? "" : lastName;

            boolean status = httpUtil.sendNotification(portalName + " Account", portalName + "", "account@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));

            return status;

        } catch (Exception ex) {
            Logger.getLogger(WordUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

//    @Asynchronous
    public boolean sendPasswordChangeEmail(String firstName, String lastName,
            String email, String domainName, String domainId) {

        try {

            if (email.equalsIgnoreCase("admin@flutterwavego.com")) {
                email = "emmanuel@flutterwavego.com";
            }

            VelocityContext context = new VelocityContext();
            context.put("domainName", "");
            context.put("firstName", firstName);
            context.put("lastName", lastName);
            context.put("username", email);

            String portalName = "bank".equalsIgnoreCase(domainName) ? domainId + " Reconciliation Portal" : domainId == null ? " Flutterwave Merchant Portal" : domainId + " Portal";

            context.put("portalName", portalName);

            Template template = getVelocityEngine().getTemplate("password_change.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            String name = firstName == null ? "" : firstName + " " + lastName == null ? "" : lastName;

            boolean status = httpUtil.sendNotification(portalName + " Account", portalName + "", "account@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));

            return status;

        } catch (Exception ex) {
            Logger.getLogger(WordUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

//    @Asynchronous
    public boolean sendLoginToken(String email, String domainName, String domainId, String name, String otp, String duration) {

        if (email.equalsIgnoreCase("admin@flutterwavego.com")) {
            email = "emmanuel@flutterwavego.com";
        }

        VelocityContext context = new VelocityContext();
        context.put("name", name);
        context.put("token", otp);
        context.put("duration", duration + " second(s)");
        context.put("domainName", "");

        Template template = getVelocityEngine().getTemplate("token.vm");

        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        String message = writer.toString();

        String portalName = "bank".equalsIgnoreCase(domainName) ? domainId + " Reconciliation Portal" : domainId == null ? " Flutterwave Merchant Portal" : domainId + " Portal Login";

        
        boolean status = httpUtil.sendNotification(portalName + "", portalName + "", "account@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));

        return status;
    }
//    SmtpMailSender mailSender
    public boolean sendLoginTokenSMTP(String email, String domainName, String domainId, String name, String otp, 
            String duration ) {

        if (email.equalsIgnoreCase("admin@flutterwavego.com")) {
            email = "emmanuel@flutterwavego.com";
        }
        
        SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);

        VelocityContext context = new VelocityContext();
        context.put("name", name);
        context.put("token", otp);
        context.put("duration", duration + " second(s)");
        context.put("domainName", "");

        Template template = getVelocityEngine().getTemplate("token.vm");

        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        String message = writer.toString();

        String portalName = "bank".equalsIgnoreCase(domainName) ? domainId + " Reconciliation Portal" : domainId == null ? " Flutterwave Merchant Portal" : domainId + " Portal Login";

        try {
            mailSender.sendHtmlMail(portalName, "account@flutterwavego.com", "Login Token", message, new String[]{email}, null, null);
        } catch (MessagingException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
//        boolean status = httpUtil.sendNotification(portalName + "", portalName + "", "account@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));

        return true;
    }

    @Asynchronous
    public void sendSelectedMerchantTransactionSummary(Map<String, Map> data, String email, Date startDate, Date endDate) {

        VelocityContext context = new VelocityContext();

        for (String key : data.keySet()) {

            Map<String, String> merchantData = data.getOrDefault(key, new HashMap<>());

            context.put(key + "Value", merchantData.getOrDefault(key + "Value", "0"));
            context.put(key + "MID", merchantData.getOrDefault(key + "MID", "0"));
            context.put(key + "Volume", merchantData.getOrDefault(key + "Volume", "0"));
        }

        context.put("startDate", Utility.formatDate(startDate, "yyyy-MM-dd"));
        context.put("endDate", Utility.formatDate(endDate, "yyyy-MM-dd"));

        Template template = getVelocityEngine().getTemplate("week_trans.vm");

        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        String message = writer.toString();

        String portalName = "Paywithcapture";

//        for(String email : emails)
        httpUtil.sendNotification(portalName + "", portalName + "", "support@paywithcapture.com", email, "", Base64.getEncoder().encodeToString(message.getBytes()));
    }

    @Asynchronous
    public void sendSelectedMerchantTransactionSummary(Map<String, Map> data, String email, Date startDate, Date endDate, SmtpMailSender mailSender) {

        try {
            VelocityContext context = new VelocityContext();

            for (String key : data.keySet()) {

                Map<String, String> merchantData = data.getOrDefault(key, new HashMap<>());

                context.put(key + "Value", merchantData.getOrDefault(key + "Value", "0"));
                context.put(key + "MID", merchantData.getOrDefault(key + "MID", "0"));
                context.put(key + "Volume", merchantData.getOrDefault(key + "Volume", "0"));
            }

            context.put("startDate", Utility.formatDate(startDate, "yyyy-MM-dd"));
            context.put("endDate", Utility.formatDate(endDate, "yyyy-MM-dd"));

            Template template = getVelocityEngine().getTemplate("week_trans.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            String portalName = "Paywithcapture";

            mailSender.sendHtmlMail(portalName, "support@paywithcapture.com", "", message, new String[]{email}, null, null);

//        for(String email : emails)
//httpUtil.sendNotification(portalName + "", portalName + "", "support@paywithcapture.com", email, "", Base64.getEncoder().encodeToString(message.getBytes()));
        } catch (MessagingException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Asynchronous
    public void buildExcelAndSend(List<TransactionExportModel> data, Date date, String merchantName, String merchantId, String[] email, SmtpMailSender mailSender) {

        try {
            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("TransactionRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex,
                    (List) TransactionUtility.getClassFields(TransactionExportModel.class), "Transaction Export", null);

            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) data);

            File tempFile = File.createTempFile(merchantName + "_" + merchantId + "_transaction_", ".xls");

            OutputStream outputStream = new FileOutputStream(tempFile);

            workbook.write(outputStream);

            outputStream.close();

            List<String> emailList = Stream.of(email).collect(Collectors.toList());
            
            if(emailList.indexOf("emmanuel@flutterwavego.com") < 0)
                emailList.add("emmanuel@flutterwavego.com");

            VelocityContext context = new VelocityContext();

            context.put("date", Utility.formatDate(date, "yyyy-MM-dd"));
            context.put("merchantName", merchantName == null ? "Merchant" : merchantName);

            Template template = getVelocityEngine().getTemplate("weekly_txn_report.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            mailSender.sendHtmlMail("Flutterwave Merchant Management", "account@flutterwavego.com", "Daily Successful Transaction Report", message, emailList.toArray(new String[]{}), null, null, tempFile);

            tempFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @Asynchronous
    public void buildExcelAndSend(String title, String to, String from, String startDate, String endDate, File tempFile, SmtpMailSender mailSender){
        
        
        VelocityContext context = new VelocityContext();

        context.put("startDate",startDate);
        context.put("endDate", endDate);

        Template template = getVelocityEngine().getTemplate("core_report.vm");

        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        String message = writer.toString();
            
        try {
            mailSender.sendHtmlMail("Flutterwave Core Report", from , title, message, new String[]{to}, new String[]{"emmanuel@flutterwavego.com"}, null, tempFile);
        } catch (MessagingException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Asynchronous
    public void buildExcelAndSend(List<GenericTransactionExportModel> data, Date date, String merchantName, 
            String merchantId, String[] email, String ccEmail[], boolean single, SmtpMailSender mailSender, String title, String... exclude) {

        File tempFile = null;
        try {
            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("Daily Transaction Report");

            int startRowIndex = 0;
            int startColIndex = 0;
            
            
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex,
                    (List) TransactionUtility.getClassFields(GenericTransactionExportModel.class, exclude), "Daily Transaction Report", null);

            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) data, exclude);

            tempFile = File.createTempFile(merchantName+"_"+merchantId+"_", ".xls");

            
            System.out.println(tempFile.getAbsolutePath());
            
            OutputStream outputStream = new FileOutputStream(tempFile);

            workbook.write(outputStream);

            outputStream.close();

            List<String> emailList = Stream.of(email).collect(Collectors.toList());
            
//            if(emailList.indexOf("emmanuel@flutterwavego.com") < 0)
//                emailList.add("emmanuel@flutterwavego.com");

            VelocityContext context = new VelocityContext();

            context.put("date", Utility.formatDate(date, "yyyy-MM-dd"));
            context.put("merchantName", merchantName == null ? "Merchant" : merchantName);

            Template template = getVelocityEngine().getTemplate("daily_txn_report.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            if(single == true){
                
                for(String s : emailList){
                    mailSender.sendHtmlMail("Flutterwave Settlement", "account@flutterwavego.com", title, message, new String[]{s}, ccEmail, new String[]{"emmanuel@flutterwavego.com"}, tempFile);
                }
            }
            else
                mailSender.sendHtmlMail("Flutterwave Settlement", "account@flutterwavego.com", title, message, emailList.toArray(new String[]{}), ccEmail, new String[]{"emmanuel@flutterwavego.com"}, tempFile);

        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            
            try{
                if(tempFile != null)
                    tempFile.delete();
            }catch(Exception ex){
                ex.printStackTrace();;
            }
        }

    }
    
    @Asynchronous
    public void buildExcelAndSendXlsx(List<GenericTransactionExportModel> data, Date date, String merchantName, 
            String merchantId, String[] email, String ccEmail[], boolean single, SmtpMailSender mailSender, String title, String... exclude) {

        try {
            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet worksheet = workbook.createSheet("Daily Transaction Report");

            int startRowIndex = 0;
            int startColIndex = 0;
            
            
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex,
                    (List) TransactionUtility.getClassFields(GenericTransactionExportModel.class, exclude), "Daily Transaction Report", null);

            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) data, exclude);

            String dateString = Utility.formatDate(new Date(), "yyyy-MM-dd");
            
            File tempFile = File.createTempFile(merchantName + "_" + merchantId + "_settlement_"+dateString+"_", ".xlsx");

            OutputStream outputStream = new FileOutputStream(tempFile);

            workbook.write(outputStream);

            outputStream.close();

            List<String> emailList = Stream.of(email).collect(Collectors.toList());
            
//            if(emailList.indexOf("emmanuel@flutterwavego.com") < 0)
//                emailList.add("emmanuel@flutterwavego.com");

            VelocityContext context = new VelocityContext();

            context.put("date", Utility.formatDate(date, "yyyy-MM-dd"));
            context.put("merchantName", merchantName == null ? "Merchant" : merchantName);

            Template template = getVelocityEngine().getTemplate("daily_txn_report.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            if(single == true){
                
                for(String s : emailList){
                    mailSender.sendHtmlMail("Flutterwave Settlement", "account@flutterwavego.com", title, message, new String[]{s}, ccEmail, new String[]{"emmanuel@flutterwavego.com"}, tempFile);
                }
            }
            else
                mailSender.sendHtmlMail("Flutterwave Settlement", "account@flutterwavego.com", title, message, emailList.toArray(new String[]{}), ccEmail, new String[]{"emmanuel@flutterwavego.com"}, tempFile);

            tempFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @Asynchronous
    public void sendXlsxFile( String range, SmtpMailSender mailSender, String title, String email, HSSFWorkbook workbook ) {

        try {

            VelocityContext context = new VelocityContext();

            String message = "Please find attached file requested";
            
            File tempFile = File.createTempFile("transactions" , ".xls");

            OutputStream outputStream = new FileOutputStream(tempFile);

            workbook.write(outputStream);

            outputStream.close();


            mailSender.sendHtmlMail("Flutterwave Report", "report@flutterwavego.com", title, message, new String[]{email}, null, new String[]{"emmanuel@flutterwavego.com"}, tempFile);

            tempFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @Asynchronous
    public void sendXlsxFile( String range, SmtpMailSender mailSender, String title, String email, XSSFWorkbook workbook ) {

        try {

            System.out.println("Send XLSX file");
            VelocityContext context = new VelocityContext();

            String message = "Please find attached the file requested";
            
            File tempFile = File.createTempFile("transactions" , ".xlsx");

            OutputStream outputStream = new FileOutputStream(tempFile);

            workbook.write(outputStream);

            outputStream.close();


            mailSender.sendHtmlMail("Flutterwave Report", "report@flutterwavego.com", title, message, new String[]{email}, null, new String[]{"emmanuel@flutterwavego.com"}, tempFile);

            tempFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @Asynchronous
    public void sendPOSSettlementFile(SmtpMailSender mailSender, Date date, String title, String merchantName, String email[], HSSFWorkbook workbook ) {

        try {

            VelocityContext context = new VelocityContext();
            
            context.put("date", Utility.formatDate(date, "yyyy-MM-dd"));
            context.put("merchantName", merchantName == null ? "Merchant" : merchantName);

            Template template = getVelocityEngine().getTemplate("settlement_pos.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();
            
            File tempFile = File.createTempFile("POS_Settlement" , ".xls");

            OutputStream outputStream = new FileOutputStream(tempFile);

            workbook.write(outputStream);

            outputStream.close();


            mailSender.sendHtmlMail("Flutterwave POS Report", "settlement@flutterwavego.com", title, message, email, null, new String[]{"emmanuel@flutterwavego.com"}, tempFile);

            tempFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Asynchronous
    public void buildExcelAndSend(List<AirtimeTransactionExportModel> data, Date startDate, Date endDate, String[] email, SmtpMailSender mailSender) {

        try {
            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("TransactionRecord");

            int startRowIndex = 0;
            int startColIndex = 0;

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex,
                    (List) TransactionUtility.getClassFields(AirtimeTransactionExportModel.class), "Transaction Export", null);

            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) data);

            File tempFile = File.createTempFile("Transaction_", ".xls");

            OutputStream outputStream = new FileOutputStream(tempFile);

            workbook.write(outputStream);

            outputStream.close();

            List<String> emailList = Stream.of(email).collect(Collectors.toList());
            
            if(emailList.indexOf("emmanuel@flutterwavego.com") < 0)
                emailList.add("emmanuel@flutterwavego.com");

            VelocityContext context = new VelocityContext();

            context.put("startdate", Utility.formatDate(startDate, "yyyy-MM-dd HH:mm:ss"));
            context.put("enddate", Utility.formatDate(endDate, "yyyy-MM-dd H:mm:ss"));
            context.put("teamName", "Paywithcapture");

            Template template = getVelocityEngine().getTemplate("airtime.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            mailSender.sendHtmlMail("Paywithcapture", "account@flutterwavego.com", "Airtime Transaction", message, emailList.toArray(new String[]{}), null, null, tempFile);

            tempFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @Asynchronous
    public void buildExcelAndSendSettlement(List<Object> data, Class<?> type, String merchant, Date date, 
            String[] email, String[] exception, boolean today, SmtpMailSender mailSender) {

        try {
            HSSFWorkbook workbook = new HSSFWorkbook();

            HSSFSheet worksheet = workbook.createSheet("Settlement");

            int startRowIndex = 0;
            int startColIndex = 0;

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex,
                    (List) TransactionUtility.getClassFields(type, exception), "Report", null);

            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) data, exception);

            File tempFile = File.createTempFile("Rave_Int_Settle_", "_.xls");

            OutputStream outputStream = new FileOutputStream(tempFile);

            workbook.write(outputStream);

            outputStream.close();

            List<String> emailList = Stream.of(email).collect(Collectors.toList());
            
            if(emailList.indexOf("emmanuel@flutterwavego.com") < 0)
                emailList.add("emmanuel@flutterwavego.com");

            VelocityContext context = new VelocityContext();

//            context.put("cycle", cycle);
            context.put("date", Utility.formatDate(date, "yyyy-MM-dd"));
            context.put("merchant", merchant);
            
            Template template;
            
            if(today == false)
                template = getVelocityEngine().getTemplate("settlement.vm");
            else
                template = getVelocityEngine().getTemplate("settlement_1.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            mailSender.sendHtmlMail("Flutterwave Central Portal", "settlement@flutterwavego.com", "Settlement Transaction", message, emailList.toArray(new String[]{}), new String[]{"jo-sharon@flutterwavego.com"}, null, tempFile);

            tempFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Asynchronous
    public void uploadToSlack(File file, String title, String token, String channels) {

        try {
//            String token = "xoxp-36723108545-125659493383-208282072256-2ec8835294c9bbe19cbafa019d742126";
            String url = "https://slack.com/api/files.upload";

//        String resource = .class.getResource("report_week_to_07-07-2017.xlsx").getFile();
            if (file == null) {
                return;
            }

//        File file = new File(resource);
            MultipartUtility multipartUtility = new MultipartUtility(url, "UTF-8");
            multipartUtility.addFormField("token", token);
            multipartUtility.addFormField("filename", "" + file.getName());
            multipartUtility.addFormField("title", "" + title);
            multipartUtility.addFormField("channels", "" + channels);
            multipartUtility.addFilePart("file", file, "application/vnd.ms-excel");
            multipartUtility.addFormField("filetype", "application/vnd.ms-excel");
            String response = multipartUtility.finish();

            Logger.getGlobal().log(Level.INFO, "Upload to slack Response {0}", response);

        } catch (Exception ex) {

            ex.printStackTrace();
        } finally {

            if (file != null) {
                try {
                    file.deleteOnExit();
                } catch (Exception ex) {
                    if (ex != null) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }

    @Asynchronous
    public void processAirtimeTransaction(InputStream inputStream, String[] emails, SmtpMailSender mailSender) {

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            
            String line = null;
            
            List<AirtimeExportModel> fetchRRNModels = new ArrayList<>();
            
            int i = 0;
            
            while ((line = reader.readLine()) != null) {
                
                if (i++ == 0) {
                    continue;
                }
                
                String reference = line.trim();
                
                reference = reference.substring(reference.indexOf("/") + 1);
                
                AirtimeTransaction transaction = airtimeTransactionDao.findByKey("flwTxnReference", reference);
                
                AirtimeExportModel exportModel = new AirtimeExportModel();
                exportModel.setExternalReference(line);
                
                if (transaction == null) {
                    
                    continue;
                }
                
                exportModel.setAccountNo(transaction.getCardMask());
                exportModel.setAmount(transaction.getAmount());
                exportModel.setBeneficiary(transaction.getTargetIdentifier());
                exportModel.setClientTxnRef(transaction.getClientTxnRef());
                exportModel.setCurrency(transaction.getTransactionCurrency());
                exportModel.setDatetime(transaction.getDatetime());
                exportModel.setMerchantId(transaction.getMerchantId());
                exportModel.setResponseCode(transaction.getResponseCode());
                exportModel.setResponseMessage(transaction.getResponseMessage());
                exportModel.setSender(transaction.getSourceIdentifier());
                exportModel.setTransactResultResponseCode(transaction.getTransactResultResponseCode());
                exportModel.setReversalCode(transaction.getReverseResponseCode());
                exportModel.setReversalMessage(transaction.getReverseResponseMessage());
                
                fetchRRNModels.add(exportModel);
            }
            
            HSSFWorkbook workbook = new HSSFWorkbook();
            
            HSSFSheet worksheet = workbook.createSheet("AirtimeTransactionRecord");
            
            int startRowIndex = 0;
            int startColIndex = 0;
            
            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex,
                    (List) TransactionUtility.getClassFields(AirtimeExportModel.class), "Transaction Export", null);
            
            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) fetchRRNModels);
            
            File tempFile = File.createTempFile("Airtime_Transaction_", ".xls");
            
            OutputStream outputStream = new FileOutputStream(tempFile);
            
            workbook.write(outputStream);
            
            outputStream.close();
            
            List<String> emailList = Stream.of(emails).collect(Collectors.toList());
            
            if(emailList.indexOf("emmanuel@flutterwavego.com") < 0)
                emailList.add("emmanuel@flutterwavego.com");
            
            VelocityContext context = new VelocityContext();
            context.put("teamName", "Paywithcapture");
            Template template = getVelocityEngine().getTemplate("airtime_export.vm");
            
            StringWriter writer = new StringWriter();
            template.merge(context, writer);
            
            String message = writer.toString();
            
            mailSender.sendHtmlMail("Paywithcapture", "support@paywithcapture.com", "Airtime Transaction", message, emailList.toArray(new String[]{}), null, null, tempFile);
            
            tempFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (Exception ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @Asynchronous
    public void notifyComplianceAdmin(String[] emails, SmtpMailSender mailSender, String product, String merchantName){
        
        try {
            VelocityContext context = new VelocityContext();

            context.put("product", product);
            context.put("name", merchantName);

            Template template = getVelocityEngine().getTemplate("compliance_admin.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            String portalName = "Flutterwave Central Portal";

            mailSender.sendHtmlMail(portalName, "complaince@flutterwavego.com", "Pending Approval Compliance Details", message, emails, null, null);

//        for(String email : emails)
//httpUtil.sendNotification(portalName + "", portalName + "", "support@paywithcapture.com", email, "", Base64.getEncoder().encodeToString(message.getBytes()));
        } catch (MessagingException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Asynchronous
    public void notifyAirtimeAdmin(String[] emails, SmtpMailSender mailSender, String fileName){
        
        try {
            VelocityContext context = new VelocityContext();

            context.put("fileName", fileName);
//            context.put("fileSize", fileSize);

            Template template = getVelocityEngine().getTemplate("airtime_settlement.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            String portalName = "Flutterwave Central Portal";

            mailSender.sendHtmlMail(portalName, "settlement@flutterwavego.com", "Airtime Settlement", message, emails, null, null);

//        for(String email : emails)
//httpUtil.sendNotification(portalName + "", portalName + "", "support@paywithcapture.com", email, "", Base64.getEncoder().encodeToString(message.getBytes()));
        } catch (MessagingException ex) {
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception ex){
            Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Asynchronous
    public void sendUploadCompleted(String name, long total, long duplicate, String email, String duplicatesString ) {

        try {

            if (email.equalsIgnoreCase("admin@flutterwavego.com")) {
                email = "emmanuel@flutterwavego.com";
            }

            VelocityContext context = new VelocityContext();
            context.put("name", name);
            context.put("total", total);
            context.put("duplicate", duplicate);
            context.put("transactions", duplicatesString);

            String portalName = "Flutterwave Central Portal";

            Template template = getVelocityEngine().getTemplate("pos_transaction.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            httpUtil.sendNotification(portalName, portalName + "", "pos@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));

        } catch (Exception ex) {
            Logger.getLogger(WordUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean sendPasswordResetEmail(String firstName, String lastName,
            String email, String domainName, String domainId, String newPassword) {

        try {

            if (email.equalsIgnoreCase("admin@flutterwavego.com")) {
                email = "emmanuel@flutterwavego.com";
            }

            VelocityContext context = new VelocityContext();
            context.put("domainName", "");
            context.put("firstName", firstName);
            context.put("lastName", lastName);
            context.put("username", email);
            context.put("password", newPassword);

            
            String portalName = "bank".equalsIgnoreCase(domainName) ? domainId +" Reconciliation Portal" : domainId == null ? " Flutterwave Merchant Portal" : domainId  +" Portal";

            context.put("portalName", portalName);

            Template template = getVelocityEngine().getTemplate("password_reset.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            String name = firstName == null ? "" : firstName  +" " +lastName == null ? "" : lastName;

            boolean status = httpUtil.sendNotification(portalName  +" Account", portalName  +"", "account@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));

            return status;

        } catch (Exception ex) {
            Logger.getLogger(WordUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }
    
    @Asynchronous
    public void sendPosTransactionFlaggedReport(PosTransaction posTransaction, 
            String lastUsedPosId, String lastMerchantName) {

        try {

            VelocityContext context = new VelocityContext();
            context.put("current_pos_id", posTransaction.getPosId());
            context.put("current_pos_merchant", posTransaction.getMerchantName());
            context.put("rrn", posTransaction.getRrn());
            context.put("last_pos_id", lastUsedPosId);
            context.put("last_pos_merchant", lastMerchantName);

            Properties properties = Utility.getConfigProperty();
            
            String email = properties.getProperty("pos_admin", "emmanuel@flutterwavego.com");
            
            String portalName = "POS Tracker";

            context.put("portalName", portalName);

            Template template = getVelocityEngine().getTemplate("pos_alert.vm");

            StringWriter writer = new StringWriter();
            template.merge(context, writer);

            String message = writer.toString();

            String name = "Admin";

            try {
                SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);
                
                mailSender.sendHtmlMail(portalName, "pos@flutterwavego.com", "Suspected Wrong POS ID", message, new String[]{email}, null, null);
            } catch (MessagingException ex) {
                Logger.getLogger(NotificationManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            
//            boolean status = httpUtil.sendNotification("Suspected Wrong POS ID ", portalName  +"", "pos@flutterwavego.com", email, name, Base64.getEncoder().encodeToString(message.getBytes()));

//            return status;

        } catch (Exception ex) {
            Logger.getLogger(WordUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

//        return false;
    }
}

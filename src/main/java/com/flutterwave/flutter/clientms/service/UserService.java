/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.LoginModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.otp.OtpMacAlgorithm;
import com.flutterwave.flutter.otp.Window;
import com.flutterwave.flutter.otp.totp.Totp;
import com.flutterwave.flutter.otp.totp.TotpTimeMode;
import com.flutterwave.flutter.otp.totp.TotpToken;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashingPasswordService;
import org.apache.shiro.crypto.hash.Sha256Hash;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class UserService {
    
    @EJB
    private UserDao userDao;
    
    private final String key = "EE8A7A86-89E8-49FB-A84E-B50AC71368C6";
    
    public static String HOME_URL = "/index.xhtml";
    
    public boolean validatePassword(LoginModel loginModel) throws DatabaseException{
        
        String password = new Sha256Hash(loginModel.getPassword()).toHex();
        
        User user = userDao.findByKey("username", loginModel.getUsername());
        
        if(user == null)
            return false;
        
        return (password.equals(user.getPassword()));
    }
    
    public boolean validatePassword(String clearPassword, String encryptedPassword){
        
        String password = new Sha256Hash(clearPassword).toHex();
        
        return (password.equals(encryptedPassword));
    }
    
    public String hashPassword(String password){
        
        password = new Sha256Hash(password).toHex();
        
        return password;
    }
    
    public boolean loginUser(LoginModel loginModel){
        
        try{
            
            String password = new Sha256Hash(loginModel.getPassword()).toHex();
            
            SecurityUtils.getSubject().login(new UsernamePasswordToken(loginModel.getUsername(),
                    password, loginModel.isRememberMe()));

            return true;
        }catch(AuthenticationException exception){
            return false;
        }
    }
    
    public String generateOTP(String email, String password){
        
        Totp totp = new Totp((key+""+(email== null ? "" : email.toLowerCase())+""+password).getBytes(), OtpMacAlgorithm.HmacSHA256, 8, TotpTimeMode.Second);
        
        TotpToken totpToken = totp.generateOtp();
        
        String otp = totpToken.getOtp();
        
        System.out.println("OTP for "+email + " is "+otp);
        
        return otp;
    }

    
    public boolean validate(String email, String password, String token, int duration){
        
        Totp totp = new Totp((key+""+(email== null ? "" : email.toLowerCase())+""+password).getBytes(), OtpMacAlgorithm.HmacSHA256, 8, TotpTimeMode.Second);
        boolean status = totp.validate(token, new Window(duration));
        
        return status;
    }
    
    public boolean validatePassword(String password){
        
        Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*(_|[^\\w])).+$");
        
        Matcher matcher = pattern.matcher(password);
        boolean status = matcher.find();
        
        if(password.length() <= 5 || status == false )
            return false;
        
        return true;
    }
    
    public String maskEmail(String email){
        
        String temp = email.substring(0,email.indexOf("@"));

        String maskedPan;
        
        if(temp.length() <= 2)
            maskedPan = temp;
        else if(temp.length() > 3){
            
            maskedPan = temp.substring(0, 3);
            
            for(int i = 0; i < temp.length() -3; i++){
                maskedPan += "*";
            }
            
        }else{
            
            maskedPan = temp.substring(0, 2);
            
            for(int i = 0; i < temp.length()-2; i++){
                maskedPan += "*";
            }
        }
        
        return maskedPan+"@"+email.substring(email.indexOf("@")+1);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanueladeyemi
 */
@XmlRootElement(name = "PaymentItem")
public class WakanowPaymentItem {

    /**
     * @return the itemName
     */
    @XmlElement(name = "ItemName")
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName the itemName to set
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * @return the itemCode
     */
    @XmlElement(name = "ItemCode")
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the itemAmount
     */
    @XmlElement(name = "ItemAmount")
    public String getItemAmount() {
        return itemAmount;
    }

    /**
     * @param itemAmount the itemAmount to set
     */
    public void setItemAmount(String itemAmount) {
        this.itemAmount = itemAmount;
    }

    /**
     * @return the leadBankCode
     */
    @XmlElement(name = "LeadBankCode")
    public String getLeadBankCode() {
        return leadBankCode;
    }

    /**
     * @param leadBankCode the leadBankCode to set
     */
    public void setLeadBankCode(String leadBankCode) {
        this.leadBankCode = leadBankCode;
    }

    /**
     * @return the leadBankCbnCode
     */
    @XmlElement(name = "LeadBankCbnCode")
    public String getLeadBankCbnCode() {
        return leadBankCbnCode;
    }

    /**
     * @param leadBankCbnCode the leadBankCbnCode to set
     */
    public void setLeadBankCbnCode(String leadBankCbnCode) {
        this.leadBankCbnCode = leadBankCbnCode;
    }

    /**
     * @return the leadBankName
     */
    @XmlElement(name = "LeadBankName")
    public String getLeadBankName() {
        return leadBankName;
    }

    /**
     * @param leadBankName the leadBankName to set
     */
    public void setLeadBankName(String leadBankName) {
        this.leadBankName = leadBankName;
    }
    
    private String itemName;
    private String itemCode;
    private String itemAmount;
    private String leadBankCode;
    private String leadBankCbnCode;
    private String leadBankName;
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.controller.api.PosApiController;
import com.flutterwave.flutter.clientms.controller.api.model.ProvisionTerminalRequest;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.dao.PosTerminalDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.MposTerminalViewModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.flutterwave.flutter.core.util.PageResult;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Singleton
public class TerminalService {

    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private LogService logService;
    @EJB
    private PosTerminalDao mposTerminalDao;
    @EJB
    private PosMerchantDao posMerchantDao;

    public JSONObject activateTerminal(String terminalId) {

        String url = configurationDao.getConfig("mpos_activation_url");
        String platformId = configurationDao.getConfig("mpos_platform_id");
        String apiKey = configurationDao.getConfig("mpos_api_key");

        if (url == null) {
            url = "https://ibeta.paypad.com.ng/paypad/api/v2/terminal/activate";
        }

        if (apiKey == null) {
            apiKey = "hmf+OMQe6L7kvk41socyw28pl4I/Ae7HGHnZvS7pfyw=";
        }

        JSONObject jSONObject = new JSONObject();
        jSONObject.put("terminalId", terminalId);

        Map<String, String> header = new HashMap<>();

        header.put("apikey", apiKey);
        header.put("platform", platformId == null ? "rave" : platformId);
        header.put("Content-Type", "application/json");

        Log log = new Log();
        log.setAction("Activate Terminal");
        log.setCreatedOn(new Date());
        log.setDescription(jSONObject.toString());
        log.setLevel(LogLevel.Info);
        log.setLogState(LogState.STARTED);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setLogDomain(LogDomain.API);
        log.setUsername(header.toString());

        logService.log(log);

        String response = httpUtil.doHttpPost(url, jSONObject.toString(), header);

        log = new Log();
        log.setAction("Activate Terminal Response");
        log.setCreatedOn(new Date());
        log.setDescription(response);
        log.setLevel(LogLevel.Info);
        log.setLogState(LogState.STARTED);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setLogDomain(LogDomain.API);
        log.setUsername(header.toString());

        logService.log(log);

        if (response == null) {
            return null;
        }

        jSONObject = new JSONObject(response);

        return jSONObject;
    }

    public int fetchTerminals() {

        String url = configurationDao.getConfig("mpos_fetch_terminal_url");
        String apiKey = configurationDao.getConfig("mpos_api_key");

        if (url == null) {
            url = "https://ibeta.paypad.com.ng/paypad/api/v2/merchant/terminals";
        }

        if (apiKey == null) {
            apiKey = "hmf+OMQe6L7kvk41socyw28pl4I/Ae7HGHnZvS7pfyw=";
        }

        Map<String, String> header = new HashMap<>();

        header.put("apikey", apiKey);
        header.put("Content-Type", "application/json");

        Log log = new Log();
        log.setAction("Fetch Terminal");
        log.setCreatedOn(new Date());
        log.setDescription("Fetching mPOS Terminals ");
        log.setLevel(LogLevel.Info);
        log.setLogState(LogState.STARTED);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setLogDomain(LogDomain.API);
        log.setUsername(header.toString());

        logService.log(log);

        String response = httpUtil.doHttpGet(url, header);

        log = new Log();
        log.setAction("Fetch Terminal Response");
        log.setCreatedOn(new Date());
        log.setDescription(response);
        log.setLevel(LogLevel.Info);
        log.setLogState(LogState.STARTED);
        log.setStatus(LogStatus.SUCCESSFUL);
        log.setLogDomain(LogDomain.API);
        log.setUsername(header.toString());

        logService.log(log);

        if (response == null) {
            return -1;
        }

        JSONObject jSONObject = new JSONObject(response);

        JSONObject dataObject = jSONObject.optJSONArray("data").getJSONObject(0);

        JSONArray terminalArrays = dataObject.optJSONArray("terminals");

        int terminalAdded = 0;

        if (terminalArrays != null) {

            for (Object terminal : terminalArrays.toList()) {

                try {
                    String terminalId = terminal.toString();

                    PosTerminal mposTerminal = mposTerminalDao.findByKey("terminalId", terminalId);

                    if (mposTerminal != null) {
                        continue;
                    }

                    mposTerminal = new PosTerminal();
                    mposTerminal.setCreatedOn(new Date());
                    mposTerminal.setTerminalId(terminalId);
                    mposTerminal.setEnabled(false);

                    mposTerminalDao.create(mposTerminal);

                    terminalAdded++;
                } catch (DatabaseException ex) {
                    Logger.getLogger(TerminalService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return terminalAdded;
    }

    public PageResult<MposTerminalViewModel> getTerminals(int pageNo, int pageSize, String provider, 
            String bank,String search) {

        PageResult<MposTerminalViewModel> pageResult = new PageResult<>();

        try {
            Page<PosTerminal> terminals = mposTerminalDao.query(pageNo, pageSize, null, null, provider, bank, search );

//            PageResult<MposTerminalViewModel> pageResult = new PageResult<>();
            if (terminals == null || terminals.getCount() <= 0) {

                pageResult.setRecordsFiltered(0L);
                pageResult.setRecordsTotal(0L);
                pageResult.setData(new ArrayList<>());
            } else {

                List<MposTerminalViewModel> models = terminals.getContent().stream().map((PosTerminal terminal) -> {

                    MposTerminalViewModel model = new MposTerminalViewModel();
                    model.setActivated(terminal.isActivated());
                    model.setEnabled(terminal.isEnabled());
                    model.setCreatedOn(terminal.getCreatedOn());
                    model.setId(terminal.getId());
                    model.setActivatedOn(terminal.getActivatedOn());
                    model.setTerminalId(terminal.getTerminalId());
                    model.setMerchant(terminal.getMerchant() == null ? null : terminal.getMerchant().getName());
                    model.setAssignedBy(terminal.getAssignedBy());
                    model.setAssignedOn(terminal.getAssignedOn());
                    model.setProvider(terminal.getProvider() == null ? null : terminal.getProvider().getName());
                    model.setSerialNo(terminal.getSerialNo());
                    model.setActivationCode(terminal.getActivationCode());
                    model.setModel(terminal.getModel());
                    model.setFccId(terminal.getFccId());
                    model.setBankName(terminal.getBankName());
                    return model;
                }).collect(Collectors.<MposTerminalViewModel>toList());

                pageResult.setData(models);
                pageResult.setRecordsFiltered(terminals.getCount());
                pageResult.setRecordsTotal(terminals.getCount());
            }

//            return pageResult;
        } catch (Exception ex) {
            Logger.getLogger(TerminalService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pageResult;
    }

    public synchronized Map<String, Object> provisionTerminal(String uniqueId, ProvisionTerminalRequest request, PosMerchant mposMerchant) {

        Map<String, Object> response = new HashMap<>();

        try {
            String maximumTerminals = configurationDao.getConfig("max_merchant_terminal");

            int terminalsCount;

            if (maximumTerminals != null) {
                terminalsCount = Integer.parseInt(maximumTerminals);
            } else {
                terminalsCount = 1;
            }

            List<PosTerminal> list = mposTerminalDao.findByMerchant(mposMerchant);

            if (list != null && list.size() >= terminalsCount) {

                response.put("responsecode", "01");
                response.put("responsemessage", "Maximum assignable terminal reached");
                return response;
            }

            PosTerminal mposTerminal = mposTerminalDao.findUnassignedTerminal();

            if (mposTerminal == null) {
                response.put("responsecode", "02");
                response.put("responsemessage", "No terminal available to be assigned");
                return response;
            }

//            mposTerminal.setMerchant(mposMerchant);
            mposTerminal.setAssignedOn(new Date());
            mposTerminal.setActivationCode(Utility.generateOTP(8));
            mposTerminal.setAssignedBy(uniqueId);

            mposTerminalDao.update(mposTerminal);

            response.put("responsecode", "00");
            response.put("responsemessage", "Terminal Provisioned successfully");
            response.put("terminalid", mposTerminal.getTerminalId());
            response.put("provisionedon", Utility.formatDate(mposTerminal.getAssignedOn(), "yyyy-MM-dd HH:mm"));
            response.put("merchantcode", mposMerchant.getMerchantCode());
            response.put("pwcmerchantid", mposMerchant.getMerchantId());
            response.put("activationcode", mposTerminal.getActivationCode());
            return response;

        } catch (DatabaseException ex) {
            org.apache.log4j.Logger.getLogger(PosApiController.class.getName()).error("Assign terminal", ex);
        }

        response.put("code", "03");
        response.put("status", "Unable to provision terminal");

        return response;
    }
    
    public String getProvider(String terminalId){
        
        try {
            PosTerminal terminal = mposTerminalDao.findByKey("terminalId", terminalId);
            
            if(terminal == null)
                return null;
            
            if(terminal.getProvider() == null)
                return null;
            
            return terminal.getProvider().getName();
        } catch (DatabaseException ex) {
            Logger.getLogger(TerminalService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public PosMerchant getMerchantName(String posId, String merchantCode){
        
        try {
            PosMerchant posMerchant = null;
            
//            = posMerchantDao.findByKey("posId", posId);
//            
//            if(posMerchant != null)
//                return posMerchant;

            if(Utility.emptyToNull(posId) != null){
                posMerchant = posMerchantDao.findByKey("posId", posId);
            }
            
            if(posMerchant == null && Utility.emptyToNull(merchantCode) != null)            
                posMerchant = posMerchantDao.findByKey("merchantCode", merchantCode);
            
            return posMerchant;
        } catch (DatabaseException ex) {
            Logger.getLogger(TerminalService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public double getFee(String posId, String merchantCode, double amount){
        
        PosMerchant posMerchant = null;
        
        if(Utility.emptyToNull(posId) != null){
            try {
                posMerchant = posMerchantDao.findByKey("posId", posId);
            } catch (DatabaseException ex) {
                Logger.getLogger(TerminalService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if(posMerchant == null && Utility.emptyToNull(merchantCode) != null)            
            try {
                posMerchant = posMerchantDao.findByKey("merchantCode", merchantCode);
        } catch (DatabaseException ex) {
            Logger.getLogger(TerminalService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(posMerchant == null)
            return 0.0;
        
        Utility.FeeType feeType = posMerchant.getFeeType();
        
        if(feeType == null)
            return 0.0;
        
        double fee = 0.0;
        
        switch(feeType){
            case FLAT:
                fee = posMerchant.getFlatFee();
                break;
            case PERCENTAGE:{
                fee = posMerchant.getPercentageFee() * amount;
                
                if(posMerchant.getFeeCap() >= 0.0 && fee > posMerchant.getFeeCap())
                    fee = posMerchant.getFeeCap();
                break;
            }
            case BOTH:{
                
                fee = posMerchant.getPercentageFee() * amount + posMerchant.getFlatFee();
                
                if(posMerchant.getFeeCap() >= 0.0 && fee > posMerchant.getFeeCap())
                    fee = posMerchant.getFeeCap();
                break;
            }
                
        }
        
        return fee;
    }
    
    public PosTerminal getTerminal(String terminalId){
        
        try {
            PosTerminal terminal = mposTerminalDao.findByKey("terminalId", terminalId);
            
            return terminal;
        } catch (DatabaseException ex) {
            Logger.getLogger(TerminalService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

}

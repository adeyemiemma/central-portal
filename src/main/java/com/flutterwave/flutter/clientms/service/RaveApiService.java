/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.service.model.RaveKeyItem;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.Utility;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class RaveApiService {
    
    @EJB
    private HttpUtil httpUtil;
    
    private final static Logger LOGGER = Logger.getLogger(RaveApiService.class);
    
    
    public RaveKeyItem getKey(String merchantCode){
        
        LOGGER.info("*********** RAVE MERCHANT QUERY SERVICE *****");
        
        Properties properties = Utility.getConfigProperty();
        
        String url = properties.getProperty("rave_url");
       
        
        Map<String, String> header = new HashMap<>();
        header.put("content-type", "application/json");
        
        String response = httpUtil.doHttpPost(url + "/"+merchantCode, new JSONObject().toString(), header);
        
        LOGGER.info("RESPONSE DATA "+response);
        
        if(response == null){
               
            LOGGER.info("No Response from the server");
            
            return null;
        }
        
        JSONObject raveObject = new JSONObject(response);
        
        LOGGER.info("*********** END RAVE CUSTOMER QUERY SERVICE *****");
        
        String status = raveObject.optString("status");
        
        if(!"success".equalsIgnoreCase(status)){
            
            return null;
        }
        
        JSONObject data = raveObject.optJSONObject("data");
        
        RaveKeyItem keyItem = new RaveKeyItem();
        keyItem.setPublicKey(data.optString("publickey"));
        keyItem.setEncryptionKey(data.optString("encryptionkey"));
        
        return keyItem;
    }
}

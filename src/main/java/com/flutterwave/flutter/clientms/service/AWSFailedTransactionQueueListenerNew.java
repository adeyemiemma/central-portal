/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.flutterwave.flutter.clientms.dao.AirtimeTransactionDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.ProviderDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.jms.Connection;
import javax.jms.Session;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

/**
 *
 * @author emmanueladeyemi
 */
@Startup
@LocalBean
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Singleton
public class AWSFailedTransactionQueueListenerNew implements Runnable {

//    private final static String URL_STRING = "tcp://localhost:61616";
    
    SQSConnectionFactory connectionFactory;
            
    private final static String QUEUE_NAME = "SETTLEMENT_FAILURE_TRANSACTION_QUEUE";
    boolean foundRRN = false, isDisbursement, isDisbursementMessage;
    boolean foundSourceIdentifier = false,foundTargetIdentifier = false;
    boolean transactresultresponsecode = false, clienttxnref = false, reverseresponsecode = false;
    boolean reverseresponsemessage = false;
    
    Session session;
    Connection connection;
    
    Logger logger = Logger.getLogger(AWSFailedTransactionQueueListenerNew.class);

    @EJB
    private LogService logService;

    @EJB
    private TransactionDao transactionDao;
    @EJB
    private ProviderDao providerDao;
    @EJB
    private AirtimeTransactionDao airtimeTransactionDao;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private AWSQueueHelper aWSQueueHelper;
    
    @Resource
    private ManagedExecutorService executorService;
    
    boolean state = true;

    @PostConstruct
    public void startup() {
        
        String environment = configurationDao.getConfig("environment");
        
        if("test".equalsIgnoreCase(environment))
            return;
        
        state = true;
        
        executorService.execute(this);
    }


    @Override
    public void run() {

        try {

            
            String url = configurationDao.getConfig("aws_airtime_queue");
        
        if(url == null){
            url = "https://sqs.eu-west-2.amazonaws.com";
        }
        
        String accessKey = configurationDao.getConfig("aws_access_key");
        
        String secretKey = configurationDao.getConfig("aws_secret_key");
        
        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        
            AmazonSQS sqs = new AmazonSQSClient(credentials);
        
        sqs.setEndpoint(url);
        
        GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest(QUEUE_NAME);
        
        GetQueueUrlResult queueUrlResult = sqs.getQueueUrl(getQueueUrlRequest);
        
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrlResult.getQueueUrl());
        receiveMessageRequest.setMaxNumberOfMessages(10);
        receiveMessageRequest.setWaitTimeSeconds(4);
        
        while(state == true){
            
            List<com.amazonaws.services.sqs.model.Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
            
            for (com.amazonaws.services.sqs.model.Message message : messages) {
                
                System.out.println("Received Failed T : " + message.getBody());

                Log log = new Log();
                log.setAction("Failed Transaction Queue Called back SQS");
                log.setCreatedOn(new Date());
                log.setLevel(LogLevel.Info);
                log.setLogDomain(LogDomain.ADMIN);
                log.setDescription(message.toString());
                log.setLogState(LogState.STARTED);
                log.setStatus(LogStatus.SUCCESSFUL);

                logService.log(log);
                
                aWSQueueHelper.onMessage(message, queueUrlResult.getQueueUrl(), sqs);
                
            }
           
            try{
                Thread.sleep(5000);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }

        }catch (Exception ex) {
            Logger.getLogger(AWSFailedTransactionQueueListenerNew.class.getName()).log(Priority.ERROR, ex);
            
            try{
            if(state == true)
                executorService.execute(this);
            }catch(Exception e){
                
            }
//            tidyUp();
        }

    }

//    @Override
//    public synchronized void onException(JMSException jmse) {
//        System.out.println("JMS Exception occured.  Shutting down client.");
//    }

//    public void onMessage(com.amazonaws.services.sqs.model.Message message) {
//
//        try {
//            
////            if (message instanceof TextMessage) {
//
//                String text = message.getBody();
//                System.out.println("Received Success T : " + text);
//
//                Log log = new Log();
//                log.setAction("Failed Transaction Queue Called back SQS");
//                log.setCreatedOn(new Date());
//                log.setLevel(LogLevel.Info);
//                log.setLogDomain(LogDomain.ADMIN);
//                log.setDescription(message.toString());
//                log.setLogState(LogState.STARTED);
//                log.setStatus(LogStatus.SUCCESSFUL);
//
////            log.setLogDomainId(0);
//                logService.log(log);
//
//                JsonReader jReader = Json.createReader(new StringReader(text));
//                JsonObject jsonObject = jReader.readObject();
//
//                String type = jsonObject.getString("transactiontype", null);
//
//                if ("AIRTIME".equalsIgnoreCase(type)) {
//
//                    AirtimeTransaction transaction = new AirtimeTransaction();
//                    
//                    transaction.setProvider((String) jsonObject.getString("sourceid", null));
//                    transaction.setAmount(Double.parseDouble(jsonObject.getString("transactionamount", "0.0") + ""));
//                    transaction.setAuthenticationModel((String) jsonObject.getString("authenticationmodel", null));
//                    transaction.setCardCountry(jsonObject.getString("cardcountry", "NG") + "");
//                    transaction.setCardMask((String) jsonObject.getString("cardmask", null));
//                    transaction.setCreatedOn(new Date());
//                    transaction.setDatetime((String) jsonObject.getString("datetime", null));
//                    transaction.setResponseCode((String) jsonObject.getString("responsecode", null));
//                    transaction.setResponseMessage((String) jsonObject.getString("responsemessage", null));
//                    transaction.setTransactionNarration((String) jsonObject.getString("transactionnarration", null));
//                    transaction.setTransactionType((String) jsonObject.getString("transactiontype", null));
//                    transaction.setFlwTxnReference((String) jsonObject.getString("flutterwavetransactionreference", null));
//                    transaction.setMerchantTxnReference((String) jsonObject.getString("merchanttranscationreference", null));
//                    transaction.setTransactionCountry((String) jsonObject.getString("transactioncountry", null));
//                    transaction.setTransactionCurrency((String) jsonObject.getString("transactioncurrency", null));
//                    transaction.setMerchantId((String) jsonObject.getString("merchantid", null));
//                    transaction.setCardScheme((String) jsonObject.getString("cardscheme", null));
//                    
//                    JsonArray listData = jsonObject.getJsonArray("processorreference");
//                    
//                    if (listData != null) {
//
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//
////                        foundRRN = false;
//                        listData.stream().forEach(x -> {
//                            JsonObject value = (JsonObject) x;
//
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//
//                            foundSourceIdentifier = false;
//                            foundTargetIdentifier = false;
//                            transactresultresponsecode = false;
//                            foundRRN = false;
//                            clienttxnref = false; reverseresponsecode = false;
//                            reverseresponsemessage = false;
//
//                            value.entrySet().stream().forEach(y -> {
//
//                                String str = String.valueOf(y.getValue()).replaceAll("\"", "");
//                                builder.add(y.getKey(), str);
//
//                                if (foundRRN == true) {
//                                    transaction.setRrn(str);
//                                    foundRRN = false;
//                                }
//                                if (str.equalsIgnoreCase("retrievalreferencenumber")) {
//                                    foundRRN = true;
//                                }
//
//                                if (foundSourceIdentifier == true) {
//                                    transaction.setSourceIdentifier(str);
//                                    foundSourceIdentifier = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("sourceidentifier")) {
//                                    foundSourceIdentifier = true;
//                                }
//                                
//                                if (foundTargetIdentifier == true) {
//                                    transaction.setTargetIdentifier(str);
//                                    foundTargetIdentifier = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("targetidentifier")) {
//                                    foundTargetIdentifier = true;
//                                }
//                                
//                                if (transactresultresponsecode == true) {
//                                    transaction.setTransactResultResponseCode(str);
//                                    transactresultresponsecode = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("transactresultresponsecode")) {
//                                    transactresultresponsecode = true;
//                                }
//                                
//                                if (clienttxnref == true) {
//                                    transaction.setClientTxnRef(str);
//                                    clienttxnref = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("clienttxnref")) {
//                                    clienttxnref = true;
//                                }
//
//                                if (reverseresponsecode == true) {
//                                    transaction.setReverseResponseCode(str);
//                                    reverseresponsecode = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("reverseresponsecode")) {
//                                    reverseresponsecode = true;
//                                }
//
//                                if (reverseresponsemessage == true) {
//                                    transaction.setReverseResponseMessage(str);
//                                    reverseresponsemessage = false;
//                                }
//                                
//                                if (str.equalsIgnoreCase("reverseresponsemessage")) {
//                                    reverseresponsemessage = true;
//                                }
//                            });
//
//                            arrayBuilder.add(builder.build());
//                        });
//                        transaction.setProcessorReference(arrayBuilder.build().toString());
//                    }
//                    
//                    listData = jsonObject.getJsonArray("transactiondata");
////                    listData = (List<Object>)jsonObject.getOrDefault("transactionjsonObject",null);
//
//                    if (listData != null) {
//
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//                        listData.stream().forEach(x -> {
//                            JsonObject value = (JsonObject) x;
//
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//
//                            value.entrySet().stream().forEach(y -> {
//                                builder.add(y.getKey(), String.valueOf(y.getValue()).replaceAll("\"", ""));
//                            });
//
//                            arrayBuilder.add(builder.build());
//                        });
//
////                        String str = arrayBuilder.toString();
//
//                        transaction.setTransactionData(arrayBuilder.build().toString());
//                    }
//
//                    AirtimeTransaction txn = airtimeTransactionDao.findByKey("flwTxnReference", transaction.getFlwTxnReference());
//
//                    if (txn == null) {
//                        airtimeTransactionDao.create(transaction);
//                        log = new Log();
//                        log.setAction("SUCCESS Transaction Queue Called back");
//                        log.setCreatedOn(new Date());
//                        log.setLevel(LogLevel.Info);
//                        log.setLogDomain(LogDomain.ADMIN);
//                        log.setDescription("successful callback queue processed for " + transaction.getFlwTxnReference());
//                        log.setLogState(LogState.FINISH);
//                        log.setStatus(LogStatus.SUCCESSFUL);
//
//                        logService.log(log);
//                    } else {
//
//                        if ("00".equalsIgnoreCase(txn.getResponseCode()) && "00".equalsIgnoreCase(transaction.getResponseCode())) {
//
//                            log = new Log();
//                            log.setAction("SUCCESS Transaction Queue Called back");
//                            log.setCreatedOn(new Date());
//                            log.setLevel(LogLevel.Info);
//                            log.setLogDomain(LogDomain.ADMIN);
//                            log.setDescription("updating with successful for transaction " + transaction.getFlwTxnReference());
//                            log.setLogState(LogState.FINISH);
//                            log.setStatus(LogStatus.SUCCESSFUL);
//
//                            transaction.setId(txn.getId());
//
//                            airtimeTransactionDao.update(transaction);
//                        } else {
//                            log = new Log();
//                            log.setAction("SUCCESS Transaction Queue Called back");
//                            log.setCreatedOn(new Date());
//                            log.setLevel(LogLevel.Info);
//                            log.setLogDomain(LogDomain.ADMIN);
//                            log.setDescription("Duplicate reference for transaction " + transaction.getFlwTxnReference());
//                            log.setLogState(LogState.FINISH);
//                            log.setStatus(LogStatus.SUCCESSFUL);
//                        }
//
//                        logService.log(log);
//                    }
//                    
//                } else {
//
//                    Transaction transaction = new Transaction();
//
////                    Provider provider = providerDao.findByKey("shortName", (String)jsonObject.getString("sourceid", null));
//                    transaction.setProvider((String) jsonObject.getString("sourceid", null));
//                    transaction.setAmount(Double.parseDouble(jsonObject.getString("transactionamount", "0.0") + ""));
//                    transaction.setAuthenticationModel((String) jsonObject.getString("authenticationmodel", null));
//                    transaction.setCardCountry(jsonObject.getString("cardcountry", "NG") + "");
//                    transaction.setCardMask((String) jsonObject.getString("cardmask", null));
//                    transaction.setCreatedOn(new Date());
//                    transaction.setDatetime((String) jsonObject.getString("datetime", null));
//                    transaction.setResponseCode((String) jsonObject.getString("responsecode", null));
//                    transaction.setResponseMessage((String) jsonObject.getString("responsemessage", null));
//                    transaction.setTransactionNarration((String) jsonObject.getString("transactionnarration", null));
//                    transaction.setTransactionType((String) jsonObject.getString("transactiontype", null));
//                    transaction.setFlwTxnReference((String) jsonObject.getString("flutterwavetransactionreference", null));
//                    transaction.setMerchantTxnReference((String) jsonObject.getString("merchanttranscationreference", null));
//                    transaction.setTransactionCountry((String) jsonObject.getString("transactioncountry", null));
//                    transaction.setTransactionCurrency((String) jsonObject.getString("transactioncurrency", null));
//                    transaction.setMerchantId((String) jsonObject.getString("merchantid", null));
//                    transaction.setCardScheme((String) jsonObject.getString("cardscheme", null));
//                    transaction.setSettlementState(Utility.SettlementState.NONE);
//
//                    JsonArray listData = jsonObject.getJsonArray("processorreference");
//
//                    if (listData != null) {
//
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//
////                        foundRRN = false;
//                        listData.stream().forEach(x -> {
//                            JsonObject value = (JsonObject) x;
//
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//
//                            foundRRN = false;
//                            isDisbursement = false;
//                            isDisbursementMessage = false;
//
//                            value.entrySet().stream().forEach(y -> {
//
//                                String str = String.valueOf(y.getValue()).replaceAll("\"", "");
//                                builder.add(y.getKey(), str);
//
//                                if (foundRRN == true) {
//                                    transaction.setRrn(str);
//                                    foundRRN = false;
//                                }
//                                if (str.equalsIgnoreCase("retrievalreferencenumber")) {
//                                    foundRRN = true;
//                                }
//
//                                if (isDisbursement == true) {
//                                    transaction.setDisburseResponseCode(str);
//                                    isDisbursement = false;
//                                }
//
//                                if (str.equalsIgnoreCase("postresponsecode")) {
//                                    isDisbursement = true;
//                                }
//
//                                if (isDisbursementMessage == true) {
//                                    transaction.setDisburseResponseMessage(str);
//                                    isDisbursementMessage = false;
//                                }
//
//                                if (str.equalsIgnoreCase("postresponsemessage")) {
//                                    isDisbursementMessage = true;
//                                }
//                            });
//
//                            arrayBuilder.add(builder.build());
//                        });
//                        transaction.setProcessorReference(arrayBuilder.build().toString());
//                    }
//
//                    listData = jsonObject.getJsonArray("transactiondata");
////                    listData = (List<Object>)jsonObject.getOrDefault("transactionjsonObject",null);
//
//                    if (listData != null) {
//
//                        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//                        listData.stream().forEach(x -> {
//                            JsonObject value = (JsonObject) x;
//
//                            JsonObjectBuilder builder = Json.createObjectBuilder();
//
//                            value.entrySet().stream().forEach(y -> {
//                                builder.add(y.getKey(), String.valueOf(y.getValue()).replaceAll("\"", ""));
//                            });
//
//                            arrayBuilder.add(builder.build());
//                        });
//
////                        String str = arrayBuilder.toString();
//
//                        transaction.setTransactionData(arrayBuilder.build().toString());
//                    }
//
//                    Transaction txn = transactionDao.findByKey("flwTxnReference", transaction.getFlwTxnReference());
//
//                    if (txn == null) {
//                        transactionDao.create(transaction);
//                        log = new Log();
//                        log.setAction("SUCCESS Transaction Queue Called back");
//                        log.setCreatedOn(new Date());
//                        log.setLevel(LogLevel.Info);
//                        log.setLogDomain(LogDomain.ADMIN);
//                        log.setDescription("successful callback queue processed for " + transaction.getFlwTxnReference());
//                        log.setLogState(LogState.FINISH);
//                        log.setStatus(LogStatus.SUCCESSFUL);
//
//                        logService.log(log);
//                    } else {
//
//                        if ("00".equalsIgnoreCase(transaction.getResponseCode())) {
//
//                            log = new Log();
//                            log.setAction("SUCCESS Transaction Queue Called back");
//                            log.setCreatedOn(new Date());
//                            log.setLevel(LogLevel.Info);
//                            log.setLogDomain(LogDomain.ADMIN);
//                            log.setDescription("Replacing duplicate with successful for transaction " + transaction.getFlwTxnReference());
//                            log.setLogState(LogState.FINISH);
//                            log.setStatus(LogStatus.SUCCESSFUL);
//
//                            transaction.setId(txn.getId());
//
//                            transactionDao.update(transaction);
//                        } else {
//                            log = new Log();
//                            log.setAction("SUCCESS Transaction Queue Called back");
//                            log.setCreatedOn(new Date());
//                            log.setLevel(LogLevel.Info);
//                            log.setLogDomain(LogDomain.ADMIN);
//                            log.setDescription("Duplicate reference for transaction " + transaction.getFlwTxnReference());
//                            log.setLogState(LogState.FINISH);
//                            log.setStatus(LogStatus.SUCCESSFUL);
//                        }
//
//                        logService.log(log);
//                    }
//                }
//
////            } 
//
//        } catch (Exception ex) {
//            if (ex != null) {
//                ex.printStackTrace();
//            }
//
//            throw new RuntimeException(ex);
//        }
//
//    }
    

    @PreDestroy
    public void tidyUp() {
        state = false;
        
//        try{
//            executorService.shutdown();
//        }catch(Exception ex){
//            
//        }
    }

}

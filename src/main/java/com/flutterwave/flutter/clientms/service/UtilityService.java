/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.controller.api.model.TellerPointCallbackRequest;
import com.flutterwave.flutter.clientms.dao.CallbackConfigurationDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.CoreSettlementDao;
import com.flutterwave.flutter.clientms.dao.MPosSettlementDao;
import com.flutterwave.flutter.clientms.dao.MerchantCompliantDao;
import com.flutterwave.flutter.clientms.dao.MerchantCompliantLogDao;
import com.flutterwave.flutter.clientms.dao.MoneywaveMerchantDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantCustomerDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantProductSettlementDao;
import com.flutterwave.flutter.clientms.dao.PosProviderDao;
import com.flutterwave.flutter.clientms.dao.PosTerminalDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionNewDao;
import com.flutterwave.flutter.clientms.dao.RewardTransactionDao;
import com.flutterwave.flutter.clientms.dao.TellerPointTransactionDao;
import com.flutterwave.flutter.clientms.model.CallbackConfiguration;
import com.flutterwave.flutter.clientms.model.Configuration;
import com.flutterwave.flutter.clientms.model.CoreSettlement;
import com.flutterwave.flutter.clientms.model.CoreSettlementMerchant;
import com.flutterwave.flutter.clientms.model.CoreSettlementProvider;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.MPosSettlement;
import com.flutterwave.flutter.clientms.model.MerchantCompliance;
import com.flutterwave.flutter.clientms.model.MerchantComplianceLog;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosMerchantCustomer;
import com.flutterwave.flutter.clientms.model.PosMerchantProduct;
import com.flutterwave.flutter.clientms.model.PosMerchantProductSettlement;
import com.flutterwave.flutter.clientms.model.PosProvider;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.model.PosTransactionNew;
import com.flutterwave.flutter.clientms.model.RewardTransaction;
import com.flutterwave.flutter.clientms.model.TellerPointTransaction;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.products.MoneywaveMerchant;
import com.flutterwave.flutter.clientms.util.FillManager;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.Layouter1;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Status;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.PosTransactionViewModelWeb;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.Page;
import com.monitorjbl.xlsx.StreamingReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.stream.Collectors;
import javax.ejb.AccessTimeout;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.json.JsonObject;
import javax.net.ssl.SSLContext;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class UtilityService {

    @EJB
    private LogService logService;
    @EJB
    private MPosSettlementDao mPosSettlementDao;
    @EJB
    private RewardTransactionDao rewardTransactionDao;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private PosTransactionDao posTransactionDao;
    @EJB
    private PosTerminalDao posTerminalDao;
    @EJB
    private NotificationManager notificationManager;
    @EJB
    private CallbackConfigurationDao callbackConfigurationDao;
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private PosMerchantDao posMerchantDao;
    @EJB
    private MerchantCompliantDao merchantCompliantDao;
    @EJB
    private MerchantCompliantLogDao merchantCompliantLogDao;
    @EJB
    private MoneywaveMerchantDao moneywaveMerchantDao;
    @EJB
    private ETopPosService eTopPosService;
    @EJB
    private TellerPointTransactionDao pointTransactionDao;
    @EJB
    private PosTransactionNewDao posTransactionNewDao;
    @EJB
    private PosProviderDao posProviderDao;
    @EJB
    private PosMerchantCustomerDao posMerchantCustomerDao;
    @EJB
    private CoreSettlementDao coreSettlementDao;

    @EJB
    private PosMerchantProductSettlementDao posMerchantProductSettlementDao;

    Logger logger = Logger.getLogger(UtilityService.class);

    private final String username = Global.EMAIL_USERNAME;
    private final String password = Global.EMAIL_PASSWORD;

    List<String> bankNames = new ArrayList<>();

    @Lock(LockType.READ)
    @Asynchronous
    @AccessTimeout(value = 12000)
    public void makeCallBackAsync(String request, String signature, String reference, String url) {

        try {

            Log log = new Log();
            log.setAction("Calling Back URL");
            log.setCreatedOn(new Date());
            log.setDescription(request);
            log.setLogDomain(LogDomain.API);
            log.setLogState(LogState.STARTED);
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setUsername("API");

//            HashMap<String, String> header =  new HashMap<>();
//            header.put("content-type", "application/json");
            HttpPost httpPost = new HttpPost(url);

            httpPost.setHeader("x-paypad-signature", signature);

            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true).build();

            CloseableHttpClient client = HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier(new NoopHostnameVerifier()).build();

            httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            httpPost.addHeader("Content-Type", "application/json");

            StringEntity entity = new StringEntity(request);

            httpPost.setEntity(entity);
            HttpResponse response = client.execute(httpPost);

            int resultCode = response.getStatusLine().getStatusCode();

            logger.info("Result code is " + resultCode);

//            System.out.println("Result code : " + resultCode);
            InputStream stream = response.getEntity().getContent();

            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            String result = br.lines().collect(Collectors.joining());

            System.out.println(result);

            MPosSettlement transaction = mPosSettlementDao.findByKey("uniqueref", reference);

            if (transaction != null) {

                transaction.setRaveCalled(true);
                transaction.setRaveCalledOn(new Date());
                transaction.setRaveResponse(result);

                mPosSettlementDao.update(transaction);

            }

            if (resultCode != 200 && resultCode != 201) {
//
//                InputStream is = new BufferedInputStream(connection.getErrorStream());
//                BufferedReader br = new BufferedReader(new InputStreamReader(is));
//                String response = br.lines().collect(Collectors.joining(""));

                log = new Log();
                log.setAction("Call Back to URL - " + url);
                log.setCreatedOn(new Date());
                log.setDescription(result);
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);
//                log.set("FINISHED");

                if (transaction != null) {
                    transaction.setRaveResponse(result);
                }

                logService.log(log);

                return;
            }

            log = new Log();
            log.setAction("Call Back to URL - " + url);
            log.setCreatedOn(new Date());
            log.setDescription(result);
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);
//            log.setStatusMessage("FINISHED");

//            if(transaction != null)
//                
            logService.log(log);

//            mPosSettlementDao.update(transaction);
        } catch (Exception ex) {
            logger.error(ex);
        }

    }

    @Asynchronous
    @AccessTimeout(value = 12000)
    public void uploadRewardTransaction(List<RewardTransaction> rewardTransaction) {

        try {
            rewardTransactionDao.create(rewardTransaction);
        } catch (DatabaseException ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }
    }

    @Asynchronous
    @AccessTimeout(value = 12000)
    public void uploadRewardTransaction(JSONArray rewardTransactionArray) {

        try {

            List<RewardTransaction> list = new ArrayList<>();

            for (int i = 0; i < rewardTransactionArray.length(); i++) {

                JSONObject jSONObject = rewardTransactionArray.optJSONObject(i);

                if (jSONObject == null) {
                    continue;
                }

                RewardTransaction rewardTransaction = RewardTransaction.from(jSONObject);

                if (rewardTransaction.getTransactionReference() != null) {
                    boolean status = rewardTransactionDao.exists(rewardTransaction.getTransactionReference());

                    if (status == true) {
                        continue;
                    }
                }

                list.add(rewardTransaction);

                if (list.size() >= 500) {

                    List<RewardTransaction> tempList = list;

                    rewardTransactionDao.createNoCheck(tempList);

                    list = new ArrayList<>();
                }
            }

            if (!list.isEmpty()) {
                rewardTransactionDao.createNoCheck(list);
            }

        } catch (DatabaseException ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

    }

    public synchronized String generateMerchantCode() {

        try {
            Configuration configuration = configurationDao.findByKey("name", "pos_merchant_count");

            String merchantCode = "MPF";

            if (configuration == null) {
                configuration = new Configuration();
                configuration.setName("pos_merchant_count");
                configuration.setValue("" + Utility.POS_MERCHANT_COUNTER);

                merchantCode += "" + Utility.POS_MERCHANT_COUNTER;

                configuration.setCreatedOn(new Date());
                configurationDao.create(configuration);

            } else {

                String value = configuration.getValue();

                long counter = Long.parseLong(value);

                counter = counter + 1;

                configuration.setName("pos_merchant_count");
                configuration.setValue("" + counter);

                merchantCode += "" + counter;

                configuration.setCreatedOn(new Date());
                configurationDao.update(configuration);
            }

            return merchantCode;
        } catch (DatabaseException ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

        return null;
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void processCSVFile(InputStream inputStream, User user, String fileName) {

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            String line = null;

            int counter = 0;

            long duplicateId = 0, total = 0;

            List<String> duplicates = new ArrayList<>();

            while ((line = reader.readLine()) != null) {

                try {
                    if (counter++ == 0) {
                        continue;
                    }

                    total++;

                    String[] data = line.split(",");

                    String terminalId = data[5].trim();
                    String rrn = data[3].replace("\"", "");

                    Long rrnLong = Long.parseLong(rrn);

                    rrn = String.valueOf(rrnLong);

                    PosTransaction posTransaction;

                    Date requestDate = Utility.parseDate(data[11], "yyyy-MM-dd HH:mm:ss");

                    if (requestDate == null || terminalId == null) {
                        continue;
                    }

                    posTransaction = posTransactionDao.findRRNTerminal(rrn, terminalId, requestDate);

                    if (posTransaction != null) {

                        duplicateId++;

                        duplicates.add(rrn);

                        continue;
                    }

                    posTransaction = new PosTransaction();
                    posTransaction.setRequestDate(requestDate);

                    try {
                        posTransaction.setAmount(Double.parseDouble(data[7]));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    posTransaction.setCreatedBy(user);
                    posTransaction.setCreatedOn(new Date());
                    posTransaction.setFileName(fileName);

                    String currency = data[8];

                    String currencyArray[] = currency.split("\\(");

                    if (currencyArray.length > 1) {
                        posTransaction.setCurrency(currencyArray[1].replace(")", "").trim());
                        posTransaction.setCurrencyCode(currencyArray[0].trim());
                    } else {
                        posTransaction.setCurrency(currencyArray[0].trim());
                        posTransaction.setCurrencyCode(currencyArray[0].trim());
                    }

                    posTransaction.setPan(data[6]);
                    posTransaction.setRefCode(data[9]);

                    posTransaction.setResponseDate(Utility.parseDate(data[12], "yyyy-MM-dd HH:mm:ss"));

                    String responseDescription = data[13];

                    if (responseDescription != null) {
                        String[] respArray = responseDescription.split("-");

                        if (respArray.length > 1) {

                            posTransaction.setResponseCode(Utility.preOrAppString(respArray[0].replace("\"", "").trim(),
                                    "0", 2, false));

                            posTransaction.setResponseMessage(respArray[1].replace("\"", "").trim());
                        } else {

                            posTransaction.setResponseCode(Utility.preOrAppString(responseDescription, "0", 2, false));
                            posTransaction.setResponseMessage(responseDescription);
                        }
                    }

                    posTransaction.setRrn(rrn);
                    posTransaction.setSource(data[0]);
                    posTransaction.setStatus(data[14]);
                    posTransaction.setTerminalId(terminalId);
                    posTransaction.setType(data[10]);
                    posTransaction.setFileId(data[2].replace("\"", ""));

                    PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());

                    if (posTerminal != null) {
                        if (posTerminal.getMerchant() != null) {
                            posTransaction.setPosId(posTerminal.getMerchant().getPosId());
                            posTransaction.setMerchantName(posTerminal.getMerchant().getName());
//                            posTransaction.setMerchant(posTerminal.getMerchant().getName());
                        }
                    }

//                    posTransaction.setPosId(rrn);
                    if ("REVERSAL".equalsIgnoreCase(posTransaction.getType())) {
                        posTransaction.setReversed(true);
                        posTransaction.setReversedOn(posTransaction.getRequestDate());
                    }

                    boolean status = uploadTransaction(posTransaction);

                    if (status == false) {
                        duplicates.add(rrn);
                        duplicateId++;
                    }

                } catch (DatabaseException ex) {
                    Logger.getLogger(UtilityService.class.getName()).error(null, ex);
                }
            }

            notificationManager.sendUploadCompleted(user.getFirstName(), total, duplicateId, user.getEmail(), duplicates.stream().collect(Collectors.joining(",")));

        } catch (IOException ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void processITexFile(InputStream inputStream, User user, String fileName) {

        try {

            Workbook workbook = StreamingReader.builder()
                    .rowCacheSize(100)
                    .bufferSize(4096)
                    .open(inputStream);

//            // This assumes the sheet is first sheet
            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.rowIterator();

            int skipTill = 1;
            int counter = 0;

            long duplicateId = 0, total = 0;

            List<String> duplicates = new ArrayList<>();

            String mti = null;

            while (rowIterator.hasNext()) {

                try {
                    Row row = rowIterator.next();
                    // This implies that we will not pick the header 

                    int lastCellNumber = row.getLastCellNum();

                    PosTransaction posTransaction = new PosTransaction();

                    if (counter++ < skipTill) {
                        continue;
                    }

                    for (int i = 0; i < lastCellNumber; i++) {

                        Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        switch (cell.getCellType()) {
                            case NUMERIC: {

                                double value = cell.getNumericCellValue();

                                switch (i) {
                                    case 0: {
                                        posTransaction.setRrn(BigDecimal.valueOf(value).toPlainString());
                                        break;
                                    }
                                    case 1:
                                        posTransaction.setRrn(BigDecimal.valueOf(value).toPlainString());
                                        break;
                                    case 4: {

                                        posTransaction.setTerminalId(BigDecimal.valueOf(value).toPlainString());
                                        break;
                                    }
                                    case 5: {

                                        posTransaction.setFileId(BigDecimal.valueOf(value).toPlainString());
                                        break;
                                    }
                                    case 12: {

                                        posTransaction.setAmount(BigDecimal.valueOf(value).doubleValue() / 100);
                                        break;
                                    }
                                    case 13: {

                                        posTransaction.setResponseMessage(value + "");
                                        break;
                                    }
                                    case 14: {

                                        posTransaction.setResponseCode(BigDecimal.valueOf(value).toPlainString());
                                        break;
                                    }

                                }

                            }
                            case STRING: {

                                String value = cell.getStringCellValue();

                                switch (i) {
                                    case 0: {

                                        Long rrnLong = Long.parseLong(value);

                                        posTransaction.setRrn(rrnLong + "");
                                        break;
                                    }
                                    case 4: {

                                        posTransaction.setTerminalId(value);
                                        break;
                                    }
                                    case 5: {

                                        posTransaction.setFileId(value);
                                        break;
                                    }
                                    case 6: {

                                        posTransaction.setResponseDate(Utility.parseDate(value, "EEE MMM dd yyyy HH:mm:ss 'GMT'Z "));
                                        posTransaction.setRequestDate(Utility.parseDate(value, "EEE MMM dd yyyy HH:mm:ss 'GMT'Z "));
                                        break;
                                    }
                                    case 9: {

//                                        if(!"0200".equalsIgnoreCase(value))
//                                            continue;
                                        mti = value;
                                        break;
                                    }
                                    case 10: {

                                        posTransaction.setPan(value);
                                        break;
                                    }
                                    case 12: {

                                        posTransaction.setAmount(new BigDecimal(value).doubleValue() / 100);
                                        break;
                                    }
                                    case 13: {

                                        posTransaction.setResponseMessage(value);
                                        break;
                                    }
                                    case 14: {

//                                        posTransaction.setResponseCode(value);
                                        posTransaction.setResponseCode(Utility.preOrAppString(value, "0", 2, false));
                                        break;
                                    }
                                    case 15: {

                                        posTransaction.setRefCode(value);
                                        break;
                                    }
                                }

                            }
                        }
                    }

                    PosTransaction existingTransaction = posTransactionDao.findRRNTerminal(posTransaction.getRrn(),
                            posTransaction.getTerminalId(), posTransaction.getRequestDate());

                    if (existingTransaction == null) {
                        existingTransaction = posTransactionDao.findRRNTerminalOnly(posTransaction.getRrn(), posTransaction.getTerminalId());
                    }

                    if (existingTransaction != null) {

                        duplicateId++;

                        duplicates.add(posTransaction.getRrn());

                        continue;
                    }

                    if (!"0200".equalsIgnoreCase(mti)) {
                        continue;
                    }

                    posTransaction.setCreatedOn(new Date());
                    posTransaction.setReversed(false);

                    if ("00".equalsIgnoreCase(posTransaction.getResponseCode()) || "0".equalsIgnoreCase(posTransaction.getResponseCode())) {

                        posTransaction.setStatus("Approved");
                    } else {
                        posTransaction.setStatus("Declined");
                    }

                    posTransaction.setSource("ITEX.");
                    posTransaction.setCurrency("NGN");
                    posTransaction.setCurrencyCode("566");
                    posTransaction.setFileName(fileName);
                    posTransaction.setType("Purchase");
                    posTransaction.setLoggedBy(user.getEmail());

                    PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());

                    if (posTerminal != null) {
                        if (posTerminal.getMerchant() != null) {
                            posTransaction.setPosId(posTerminal.getMerchant().getPosId());
                            posTransaction.setMerchantName(posTerminal.getMerchant().getName());
                            posTransaction.setMerchantCode(posTerminal.getMerchant().getMerchantCode());
                            posTransaction.setProductId(posTerminal.getMerchant().getMerchantId());
                            posTransaction.setProductName(posTerminal.getMerchant().getMerchantId());
//                            posTransaction.setMerchant(posTerminal.getMerchant().getName());
                        }
                    }

                    if (posTransaction.getRequestDate() == null) {
                        continue;
                    }

                    boolean status = uploadTransaction(posTransaction);

                    if (status == false) {
                        duplicates.add(posTransaction.getRrn());
                        duplicateId++;
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

            notificationManager.sendUploadCompleted(user.getFirstName(), total, duplicateId, user.getEmail(), duplicates.stream().collect(Collectors.joining(",")));

        } catch (Exception ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void processMerchantFile(InputStream inputStream, User user, String fileName) {

        try {
//            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            Workbook workbook = StreamingReader.builder()
                    .rowCacheSize(100)
                    .bufferSize(4096)
                    .open(inputStream);

//            // This assumes the sheet is first sheet
            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.rowIterator();

            int skipTill = 2;
            int counter = 0;

            while (rowIterator.hasNext()) {

                try {
                    Row row = rowIterator.next();
                    // This implies that we will not pick the header 

                    int lastCellNumber = row.getLastCellNum();

                    PosMerchant posMerchant = new PosMerchant();

                    if (counter++ <= 2) {
                        continue;
                    }

                    {
                        for (int i = 0; i < lastCellNumber; i++) {

//                Cell cell = cellIterator.next();
                            Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                            switch (cell.getCellType()) {
                                case NUMERIC: {

                                    double value = cell.getNumericCellValue();

                                    switch (i) {
                                        case 0:
                                            posMerchant.setName(value + "");
                                            break;
                                        case 1: {

                                            if (Utility.emptyToNull(value + "") != null) {
                                                PosMerchant parent = posMerchantDao.findByKey("name", value + "");

                                                if (parent != null) {
                                                    posMerchant.setParent(parent);
                                                }

                                            }
                                        }
                                        break;
                                        case 2:
                                            posMerchant.setPhone(value + "");
                                            break;
                                        case 3:
                                            posMerchant.setEmail(value + "");
                                            break;
                                        case 4:
                                            posMerchant.setStreet(value + "");
                                            break;
                                        case 5:
                                            posMerchant.setCity(value + "");
                                            break;
                                        case 6:
                                            posMerchant.setState(value + "");
                                            break;
                                        case 7:
                                            try {
                                            posMerchant.setFeeType(Utility.FeeType.valueOf(value + ""));
                                        } catch (Exception x) {
                                            posMerchant.setFeeType(Utility.FeeType.PERCENTAGE);
                                        }
                                        break;
                                        case 8:
                                            try {
                                            posMerchant.setFlatFee(Double.parseDouble(value + ""));
                                        } catch (Exception ex) {
                                        }
                                        break;
                                        case 9:
                                            try {
                                            posMerchant.setPercentageFee(value);
                                        } catch (Exception ex) {
                                        }
                                        break;
                                        case 10:
                                            try {
                                            posMerchant.setRelationshipMan(value + "");
                                        } catch (Exception ex) {
                                        }
                                        break;
                                        case 11:
                                            posMerchant.setAccountNo(value + "");
                                            break;
                                        case 12:
                                            posMerchant.setBankName(value + "");
                                            break;
                                        case 13:
                                            posMerchant.setAccountName(value + "");
                                            break;
                                        case 14:
                                            try {
                                            posMerchant.setFeeCap(value);
                                        } catch (Exception ex) {
                                        }
                                        break;
                                        case 15:
                                            posMerchant.setMerchantId(value + "");
                                            break;
                                    }

                                }
                                case STRING: {

                                    String value = cell.getStringCellValue();

                                    switch (i) {
                                        case 0:
                                            posMerchant.setName(value);
                                            break;
                                        case 1: {

                                            if (Utility.emptyToNull(value) != null) {
                                                PosMerchant parent = posMerchantDao.findByKey("name", value);

                                                if (parent != null) {
                                                    posMerchant.setParent(parent);
                                                }

                                            }
                                        }
                                        break;
                                        case 2:
                                            posMerchant.setPhone(value);
                                            break;
                                        case 3:
                                            posMerchant.setEmail(value);
                                            break;
                                        case 4:
                                            posMerchant.setStreet(value);
                                            break;
                                        case 5:
                                            posMerchant.setCity(value);
                                            break;
                                        case 6:
                                            posMerchant.setState(value);
                                            break;
                                        case 7:
                                            try {
                                            posMerchant.setFeeType(Utility.FeeType.valueOf(value));
                                        } catch (Exception x) {
                                            posMerchant.setFeeType(Utility.FeeType.PERCENTAGE);
                                        }
                                        break;
                                        case 8:
                                            try {
                                            posMerchant.setFlatFee(Double.parseDouble(value));
                                        } catch (Exception ex) {
                                        }
                                        break;
                                        case 9:
                                            try {
                                            posMerchant.setPercentageFee(Double.parseDouble(value));
                                        } catch (Exception ex) {
                                        }
                                        break;
                                        case 10:
                                            try {
                                            posMerchant.setRelationshipMan(value);
                                        } catch (Exception ex) {
                                        }
                                        break;
                                        case 11:
                                            posMerchant.setAccountNo(value);
                                            break;
                                        case 12:
                                            posMerchant.setBankName(value);
                                            break;
                                        case 13:
                                            posMerchant.setAccountName(value);
                                            break;
                                        case 14:
                                            try {
                                            posMerchant.setFeeCap(Double.parseDouble(value));
                                        } catch (Exception ex) {
                                        }
                                        break;
                                        case 15:
                                            posMerchant.setMerchantId(value + "");
                                            break;
                                    }

                                }
                            }
                        }
                    }

                    if (Utility.emptyToNull(posMerchant.getMerchantId()) == null) {
                        continue;
                    }

                    String code = generateMerchantCode();

                    PosMerchant merchant = posMerchantDao.findByKey("merchantCode", code);

                    while (merchant != null) {

                        code = generateMerchantCode();

                        merchant = posMerchantDao.findByKey("merchantCode", code);
                    }

                    posMerchant.setAddress(posMerchant.getStreet() + " " + posMerchant.getCity() + " " + posMerchant.getState());

                    posMerchant.setMerchantCode(code);

                    String posId = eTopPosService.createMerchant(code, posMerchant.getName());

                    posMerchant.setPosId(posId);
                    posMerchant.setCreatedOn(new Date());
                    posMerchant.setCreatedBy(username);
                    posMerchantDao.create(posMerchant);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void processMerchantCustomerFile(InputStream inputStream, User user, String parameterName, PosMerchant posMerchant) {

        try {
//            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            Workbook workbook = StreamingReader.builder()
                    .rowCacheSize(100)
                    .bufferSize(4096)
                    .open(inputStream);

//            // This assumes the sheet is first sheet
            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.rowIterator();

            int counter = 0;

            while (rowIterator.hasNext()) {

                try {
                    Row row = rowIterator.next();
                    // This implies that we will not pick the header 

                    int lastCellNumber = row.getLastCellNum();

//                    PosMerchant posMerchant = new PosMerchant();
                    if (counter++ < 1) {
                        continue;
                    }

                    PosMerchantCustomer merchantCustomer = new PosMerchantCustomer();

//                    {
                    Cell cell = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                    String customerId = getValue(cell);

                    if (Utility.emptyToNull(customerId) == null) {
                        continue;
                    }

                    if (posMerchantCustomerDao.findByKey("customerId", customerId) != null) {
                        continue;
                    }

                    merchantCustomer.setCustomerId(customerId);
                    merchantCustomer.setCreatedOn(new Date());
                    merchantCustomer.setMerchantCode(posMerchant.getMerchantCode());
                    merchantCustomer.setMerchantName(posMerchant.getName());
                    merchantCustomer.setParameterName(parameterName);

                    cell = row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                    String customerName = getValue(cell);

                    merchantCustomer.setCustomerName(customerName);

                    cell = row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                    customerName = getValue(cell);

                    merchantCustomer.setCustomerName(customerName);

                    if (lastCellNumber > 2) {

                        cell = row.getCell(2, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        customerName = getValue(cell);

                        merchantCustomer.setOtherId(customerName);
                    }
//                    }

//                    if(Utility.emptyToNull(posMerchant.getMerchantId()) == null)
//                        continue;
//                    
//                    String code = generateMerchantCode();
//
//                    PosMerchant merchant = posMerchantDao.findByKey("merchantCode", code);
//
//                    while (merchant != null) {
//
//                        code = generateMerchantCode();
//
//                        merchant = posMerchantDao.findByKey("merchantCode", code);
//                    }
//                    posMerchant.setAddress(posMerchant.getStreet()+" "+posMerchant.getCity()+" "+posMerchant.getState());
//            
//                    posMerchant.setMerchantCode(code);
//                    
//                    String posId = eTopPosService.createMerchant(code, posMerchant.getName());
                    posMerchantCustomerDao.create(merchantCustomer);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private String getValue(Cell cell) {

        if (cell == null) {
            return null;
        }

        switch (cell.getCellType()) {
            case NUMERIC: {

                return cell.getNumericCellValue() + "";
            }
            case STRING: {

                return cell.getStringCellValue() + "";
            }
        }

        return null;
    }

    @Lock(LockType.READ)
    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void processTerminalFile(InputStream inputStream, User user, String fileName) {

        try {
//            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            Workbook workbook = StreamingReader.builder()
                    .rowCacheSize(100)
                    .bufferSize(4096)
                    .open(inputStream);

//            // This assumes the sheet is first sheet
            Sheet sheet = workbook.getSheetAt(0);

            Iterator<Row> rowIterator = sheet.rowIterator();

            int skipTill = 1;
            int counter = 0;

            Properties properties = Utility.getConfigProperty();

            while (rowIterator.hasNext()) {

                try {
                    Row row = rowIterator.next();
                    // This implies that we will not pick the header 

                    int lastCellNumber = row.getLastCellNum();

                    PosTerminal posTerminal = new PosTerminal();

                    if (counter++ <= skipTill) {
                        continue;
                    }

                    {
                        for (int i = 0; i < lastCellNumber; i++) {

//                Cell cell = cellIterator.next();
                            Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                            switch (cell.getCellType()) {
                                case NUMERIC: {

                                    double value = cell.getNumericCellValue();

                                }
                                case STRING: {

                                    String value = cell.getStringCellValue();

                                    switch (i) {
                                        case 0:
                                            posTerminal.setTerminalId(value);
                                            break;
                                        case 1:
                                            PosMerchant merchant = posMerchantDao.findByKey("merchantCode",value);
                                            
                                            if(merchant == null)
                                                merchant = posMerchantDao.findByKey("posId",value);
                                                
                                            posTerminal.setMerchant(merchant);
                                                    
                                            break;    
                                        case 2: {

                                            try {
                                                String bankPrefix = posTerminal.getTerminalId().substring(0, 4);

                                                String bankName = properties.getProperty("T_" + bankPrefix);

                                                if (Utility.emptyToNull(bankName) != null) {

                                                    posTerminal.setBankName(bankName);
                                                } else {
                                                    posTerminal.setBankName(value);
                                                }

                                            } catch (Exception ex) {
                                                ex.printStackTrace();

                                                posTerminal.setBankName(value);
                                            }

                                            break;
                                        }
                                        case 3: {

                                            PosProvider posProvider = posProviderDao.findByKey("shortName", value);
                                            posTerminal.setProvider(posProvider);
                                            break;
                                        }
                                        case 4:
                                            posTerminal.setReceivedBy(value);
                                            break;
                                        case 5:
                                            posTerminal.setAssignedBy(value);
                                            break;
                                        case 6:
                                            posTerminal.setSerialNo(value);
                                            break;
                                        case 7:
                                            posTerminal.setFccId(value);
                                            break;
                                        case 8:
                                            posTerminal.setBatchNo(value);
                                            break;
                                        case 9:
                                            posTerminal.setModel(value);
                                            break;
                                    }

                                }
                            }
                        }
                    }

                    if (Utility.emptyToNull(posTerminal.getTerminalId()) == null) {
                        continue;
                    }

                    posTerminal.setActivated(true);
                    posTerminal.setCreatedBy(user);
                    posTerminal.setCreatedOn(new Date());
                    posTerminal.setEnabled(true);

                    if (posTerminalDao.findByKey("terminalId", posTerminal.getTerminalId()) != null) {
                        continue;
                    }

                    posTerminalDao.create(posTerminal);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Lock(LockType.WRITE)
    @AccessTimeout(value = 120000)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public synchronized boolean uploadTransaction(PosTransaction posTransaction) {

        try {
            PosTransaction pt = posTransactionDao.findRRNTerminal(posTransaction.getRrn(), posTransaction.getTerminalId(), posTransaction.getRequestDate());

            if (pt != null) {
                return false;
            }

            String transRef = posTransaction.getTerminalId() + "" + posTransaction.getRrn() + "" + posTransaction.getAmount()
                    + "" + Utility.formatDate(posTransaction.getRequestDate(), "yyyyMMdd");

            posTransaction.setTransRef(transRef);
            posTransaction.setRefCode(transRef);

            posTransaction = posTransactionDao.create(posTransaction);

            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());

            PosTransactionNew posTransactionNew;

            if (posTerminal != null && posTerminal.getMerchant() != null) {

                PosMerchant merchant = posTerminal.getMerchant();

                posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, null, posTerminal.getBankName(),
                        null, null, merchant.getParent() == null ? null : merchant.getParent().getName(), merchant.getMerchantId(),
                        merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null, merchant, posTerminal.getProvider(), null);

                posTransactionNew.setMerchantName(merchant.getName());
                posTransactionNew.setId(posTransaction.getId());

                posTransactionNewDao.update(posTransactionNew);

            }

            if ("00".equalsIgnoreCase(posTransaction.getResponseCode()) && posTransaction.isCallBack() == false) {
                callBackUser(posTransaction);
            }

            return true;
        } catch (Exception ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

        return false;
    }

    public void exportPOSTransaction(String email, String merchantId, String merchantcode, String range, String type,
            String provider, String currency, String responseCode, String bankName, String search) {

//        PageResult<GenericTransaction> pageResult = new PageResult<>();
        Date startDate = null, endDate = null;

        if (range != null && !range.equalsIgnoreCase("")) {

            try {
                String[] splitDate = range.split("-");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                startDate = dateFormat.parse(splitDate[0]);

//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(startDate);
//                calendar.set(Calendar.HOUR_OF_DAY, 0);
//                calendar.set(Calendar.MINUTE, 0);
//                calendar.set(Calendar.SECOND, 0);
//                calendar.set(Calendar.MILLISECOND, 0);
//                startDate = calendar.getTime();
                endDate = dateFormat.parse(splitDate[1]);

//                calendar.setTime(endDate);
//                calendar.set(Calendar.HOUR_OF_DAY, 23);
//                calendar.set(Calendar.MINUTE, 59);
//                calendar.set(Calendar.SECOND, 59);
//                calendar.set(Calendar.MILLISECOND, 59);
//                endDate = calendar.getTime();
            } catch (ParseException pe) {
                startDate = endDate = null;
            }
        }

        long totalCount = 0;

        Page<PosTransactionViewModelWeb> page = posTransactionNewDao.findForWebNew(0, 0, Utility.emptyToNull(merchantId), Utility.emptyToNull(merchantcode),
                Utility.emptyToNull(provider), startDate, endDate, Utility.emptyToNull(responseCode), Utility.emptyToNull(currency),
                Utility.emptyToNull(type), null, Utility.emptyToNull(bankName), Utility.emptyToNull(search), true);

        List<PosTransactionViewModelWeb> list = page.getContent();

//        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        String date = Utility.formatDate(new Date(), "yyyyMMddHHmmss");

        System.out.println("Building report");
//        {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet worksheet = workbook.createSheet("TransactionRecord");

        int startRowIndex = 0;
        int startColIndex = 0;

        if ("admin@flutterwavego.com".equalsIgnoreCase(email)) {
            email = "emmanuel@flutterwavego.com";
        }

        int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) Utility.getClassFields(PosTransactionViewModelWeb.class), "Transaction Export", null);

        FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) list);

        SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                true, Global.USE_STARTTLS, false);

        System.out.println("Sending report");

        notificationManager.sendXlsxFile(range, mailSender, "POS Transaction from " + range, email, workbook);

    }

//    @Asynchronous
//    public void callBackUserMain(PosTransaction posTransaction) {
//
//        try {
//            if (posTransaction == null) {
//                return;
//            }
//            if (posTransaction.isCallBack() == true) {
//                return;
//            }
//            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());
//            
//            if (posTerminal == null || posTerminal.getMerchant() == null) {
//                return;
//            }
//            
//            JSONObject jSONObject = new JSONObject();
//            jSONObject.put("terminalid", posTransaction.getTerminalId());
//            jSONObject.put("merchantid",  Utility.nullToEmpty(posTerminal.getMerchant().getMerchantId()));
//            jSONObject.put("merchantcode",  Utility.nullToEmpty(posTerminal.getMerchant().getMerchantCode()));
//            jSONObject.put("posid", Utility.nullToEmpty(posTransaction.getPosId()));
//            jSONObject.put("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
//            jSONObject.put("datetime", Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
//            jSONObject.put("responsecode", Utility.nullToEmpty(posTransaction.getResponseCode()));
//            jSONObject.put("responsemessage", Utility.nullToEmpty(posTransaction.getResponseMessage()));
//            jSONObject.put("mask", Utility.nullToEmpty(posTransaction.getPan()));
//            jSONObject.put("type", Utility.nullToEmpty(posTransaction.getType()));
//            jSONObject.put("status", Utility.nullToEmpty(posTransaction.getStatus()));
//            jSONObject.put("amount", posTransaction.getAmount());
//            jSONObject.put("currency", posTransaction.getCurrency());
//            jSONObject.put("fileid", posTransaction.getFileId());
//            Log log = new Log();
//            log.setAction("MPOS Call back for terminalId " + posTransaction.getTerminalId());
//            log.setCreatedOn(new Date());
//            log.setDescription(jSONObject.toString());
//            log.setLevel(LogLevel.Info);
//            log.setLogDomain(LogDomain.ADMIN);
//            log.setLogState(LogState.STARTED);
//            log.setUsername("API");
//            log.setStatus(LogStatus.SUCCESSFUL);
//            logService.log(log);
//            List<CallbackConfiguration> configurations = callbackConfigurationDao.getConfiguration("pos", posTerminal.getMerchant().getMerchantId());
//            if (configurations == null || configurations.isEmpty()) {
//                return;
//            }
//            CallbackConfiguration callbackConfiguration = configurations.get(0);
//            String urls = callbackConfiguration.getUrls();
//            String[] urlArray = urls.split(",");
//            Map<String, String> header = new HashMap<>();
//            header.put("Content-Type", "application/json");
//            
//            
//            String result = "";
//            for (String urlString : urlArray) {
//
//                result += " U: " + urlString;
//                Map<String, String> map = httpUtil.doHttpPostC(urlString, jSONObject.toString(), header);
//
//                if (map == null) {
//                    result += " S: failed,";
//                } else {
//
//                    result += " S: " + map.getOrDefault("status", "failed");
//                    result += " R: " + map.getOrDefault("result", "");
//                }
//            }
//            
//            posTransaction.setCallBack(true);
//            posTransaction.setCallBackResponse(result);
//            posTransaction.setCallbackOn(new Date());
//            posTransactionDao.update(posTransaction);
//            
//        } catch (DatabaseException ex) {
//            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
//        }
//
//    }
    public boolean shouldFlagTransaction(PosTransaction lastUsed, PosTransaction transaction) {

        if (lastUsed == null) {
            return false;
        }

        Properties properties = Utility.getConfigProperty();

        String permission = properties.getProperty("enable_pos_txn_check", "no");

        if (!"yes".equalsIgnoreCase(permission)) {
            return true;
        }

        return !transaction.getPosId().equalsIgnoreCase(lastUsed.getPosId());
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void callSubscribers(PosTransactionNew posTransactionNew, PosTransaction posTransaction, PosTerminal posTerminal,
            String orderId, PosMerchantCustomer posMerchantCustomer) {

        PosTransaction txn = null;

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -4);

        try {
            txn = posTransactionDao.findLastRecent(posTransaction.getTerminalId(), calendar.getTime());
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(UtilityService.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (posTerminal.isFlagged()) {

            if (txn != null) {
                notificationManager.sendPosTransactionFlaggedReport(posTransaction, txn.getPosId(), txn.getMerchantName());
            } else {
                notificationManager.sendPosTransactionFlaggedReport(posTransaction, null, null);
            }

            return;
        }

        if (shouldFlagTransaction(txn, posTransaction)) {

            posTerminal.setFlagged(true);
            posTerminal.setFlaggedOn(new Date());

            posTransactionNew.setFlagged(true);
            posTransactionNew.setFlaggedOn(new Date());

            try {
                posTerminalDao.update(posTerminal);
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(UtilityService.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                posTransactionNewDao.update(posTransactionNew);
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(UtilityService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        callBackUser(posTransaction, orderId, posMerchantCustomer);
    }

    @Lock(LockType.READ)
    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void callBackUser(PosTransaction posTransaction) {

        try {
            if (posTransaction == null) {
                return;
            }
            if (posTransaction.isCallBack() == true & !posTransaction.isReversed()) {
                return;
            }
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());

            if (posTerminal == null) {
                return;
            }

            if (posTerminal.isFlagged() == true) {
                return;
            }

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("terminalid", posTransaction.getTerminalId());

            String merchantId = null;

            PosMerchant merchant = null;

            PosMerchantCustomer merchantCustomer = null;

            if (Utility.emptyToNull(posTransaction.getPosId()) == null) {

                if (posTerminal.getMerchant() != null) {
                    jSONObject.put("merchantid", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantId()));
                    jSONObject.put("merchantcode", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantCode()));

                    merchantId = posTerminal.getMerchant().getMerchantId();

                    merchant = posTerminal.getMerchant();
                } else {

                    jSONObject.put("merchantid", "");
                    jSONObject.put("merchantcode", "");
                }
            } else {

                PosMerchant posMerchant = posMerchantDao.findByKey("posId", posTransaction.getPosId());

                if (posMerchant == null) {

                    merchantCustomer = posMerchantCustomerDao.findByKey("customerId", posTransaction.getPosId());

                    if (merchantCustomer != null) {

                        posMerchant = posMerchantDao.findByKey("merchantCode", merchantCustomer.getMerchantCode());

//                        customerId = merchantCustomer.getCustomerId();
                    } else {
                        return;
                    }
                }

                jSONObject.put("merchantid", Utility.nullToEmpty(posMerchant.getMerchantId()));
                jSONObject.put("merchantcode", Utility.nullToEmpty(posMerchant.getMerchantCode()));

                merchant = posMerchant;
                merchantId = posMerchant.getMerchantId();
            }

            if (merchantId == null || merchant == null) {
                return;
            }

            
            if (posTerminal.getProvider() != null) {
                jSONObject.put("custom_ptsp", posTerminal.getProvider().getName());
            } else {
                jSONObject.put("custom_ptsp", "");
            }
            
            if ("144091".equalsIgnoreCase(merchantId) || "123456".equalsIgnoreCase(merchantId)) {

//                if (!"00".equalsIgnoreCase(posTransaction.getResponseCode())) {
//                    return;
//                }
                jSONObject.put("custom_chname", "");

                jSONObject.put("custom_ptspref", Utility.nullToEmpty(posTransaction.getRefCode()));

            } else {

                double fee = getPosFee(merchant, posTransaction.getAmount());

                jSONObject.put("applicable_fee", fee);
            }

            jSONObject.put("posid", Utility.nullToEmpty(posTransaction.getPosId()));
            jSONObject.put("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
            jSONObject.put("datetime", Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
            jSONObject.put("responsecode", Utility.nullToEmpty(posTransaction.getResponseCode()));
            jSONObject.put("responsemessage", Utility.nullToEmpty(posTransaction.getResponseMessage()));
            jSONObject.put("mask", Utility.nullToEmpty(posTransaction.getPan()));
            jSONObject.put("type", Utility.nullToEmpty(posTransaction.getType()));
            jSONObject.put("status", Utility.nullToEmpty(posTransaction.getStatus()));
            jSONObject.put("amount", posTransaction.getAmount());
            jSONObject.put("currency", "NGN");
            jSONObject.put("fileid", posTransaction.getFileId());
            jSONObject.put("address", Utility.nullToEmpty(merchant.getAddress()));
            jSONObject.put("merchantname", Utility.nullToEmpty(merchant.getName()));
            jSONObject.put("reversed", posTransaction.isReversed());

            if ("144163".equalsIgnoreCase(merchantId) || "123456".equalsIgnoreCase(merchantId)) {
//                jSONObject.put("custom_chname", "");
                double fee = getPosFee(merchant, posTransaction.getAmount());

                jSONObject.put("applicable_fee", fee);

                jSONObject.put("flw_reference", Utility.nullToEmpty(posTransaction.getTransRef()));

//                if (posTerminal.getProvider() != null) {
//                    jSONObject.put("custom_ptsp", posTerminal.getProvider().getName());
//                } else {
//                    jSONObject.put("custom_ptsp", "");
//                }
            }

            Log log = new Log();
            log.setAction("MPOS Call back for terminalId " + posTransaction.getTerminalId());
            log.setCreatedOn(new Date());
            log.setDescription(jSONObject.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.STARTED);
            log.setUsername("API");
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            List<CallbackConfiguration> configurations = callbackConfigurationDao.getConfiguration("pos", merchantId);

            if (configurations == null || configurations.isEmpty()) {
                return;
            }

            CallbackConfiguration callbackConfiguration = configurations.get(0);
            String urls = callbackConfiguration.getUrls();
            String[] urlArray = urls.split(",");
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");

            String result = "";

            String status = "failed";

            for (String urlString : urlArray) {

                result += " U: " + urlString;
                Map<String, String> map = httpUtil.doHttpPostC(urlString, jSONObject.toString(), header);

                if (map == null) {
                    result += " S: failed,";
                } else {

                    status = map.getOrDefault("status", "failed");
                    result += " S: " + status;
                    result += " R: " + map.getOrDefault("result", "");
                }
            }

            try {
                PosTransactionNew posTransactionNew = null;

                if (merchant != null) {
                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, null, posTerminal.getBankName(),
                            null, null, merchant.getParent() == null ? null : merchant.getParent().getName(), merchant.getMerchantId(),
                            merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null, merchant, posTerminal.getProvider(), merchantCustomer);

                    posTransactionNew.setMerchantName(merchant.getName());
                } else {
                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, null, posTerminal.getBankName(),
                            null, null, null, null,
                            null, posTerminal.getProvider().getName(), null, null, merchant, posTerminal.getProvider(), merchantCustomer);
                }

                PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(posTransaction.getRrn(),
                        posTransaction.getTerminalId(), posTransaction.getRequestDate());

                if (posTransactionN == null) {
                    posTransactionNewDao.create(posTransactionNew);
                } else {

                    posTransactionNew.setId(posTransactionN.getId());
                    posTransactionNewDao.update(posTransactionNew);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if ("success".equalsIgnoreCase(status)) {
                posTransaction.setCallBack(true);
//                posTransaction.setCallBackResponse(result);
                posTransaction.setCallbackOn(new Date());
                posTransaction.setReschedule(false);
                posTransactionDao.update(posTransaction);
            }

        } catch (DatabaseException ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

    }

    @Lock(LockType.READ)
    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void callBackUser(PosTransaction posTransaction, String orderId, PosMerchantCustomer posMerchantCustomer) {

        try {
            if (posTransaction == null) {
                return;
            }
            if (posTransaction.isCallBack() == true & !posTransaction.isReversed()) {
                return;
            }
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());

            if (posTerminal == null) {
                return;
            }

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("terminalid", posTransaction.getTerminalId());
            
            if("internalmobile".equalsIgnoreCase(posTransaction.getSource())){
                
                jSONObject.put("channel", "MOBILE");
            }
            else
                jSONObject.put("channel", "POS");

            String merchantId = null;

            PosMerchant merchant = null;

            if (Utility.emptyToNull(posTransaction.getPosId()) == null) {

                if (posTerminal.getMerchant() != null) {
                    jSONObject.put("merchantid", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantId()));
                    jSONObject.put("merchantcode", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantCode()));

                    merchantId = posTerminal.getMerchant().getMerchantId();

                    merchant = posTerminal.getMerchant();
                } else {

                    jSONObject.put("merchantid", "");
                    jSONObject.put("merchantcode", "");
                }
            } else {

                PosMerchant posMerchant = posMerchantDao.findByKey("posId", posTransaction.getPosId());

                if (posMerchant == null) {
                    return;
                }

                jSONObject.put("merchantid", Utility.nullToEmpty(posMerchant.getMerchantId()));
                jSONObject.put("merchantcode", Utility.nullToEmpty(posMerchant.getMerchantCode()));

                merchant = posMerchant;
                merchantId = posMerchant.getMerchantId();
            }

            if (merchantId == null || merchant == null) {
                return;
            }
            
            if (posTerminal.getProvider() != null) {
                jSONObject.put("custom_ptsp", posTerminal.getProvider().getName());
            } else {
                jSONObject.put("custom_ptsp", "");
            }

            if ("144091".equalsIgnoreCase(merchantId) || "123456".equalsIgnoreCase(merchantId)) {

//                if (!"00".equalsIgnoreCase(posTransaction.getResponseCode())) {
//                    return;
//                }
                jSONObject.put("custom_chname", "");

                jSONObject.put("custom_ptspref", Utility.nullToEmpty(posTransaction.getRefCode()));

                
            } else if ("144163".equalsIgnoreCase(merchantId)) {

                jSONObject.put("flw_reference", Utility.nullToEmpty(posTransaction.getTransRef()));
                JSONArray array = new JSONArray();

                if (orderId != null) {

                    JSONObject meta = new JSONObject();
                    meta.put("metaname", "order_id")
                            .put("metavalue", orderId);

                    array.put(meta);
                }

                if (Utility.nullToEmpty(posTransaction.getTraceref()) != null) {

                    JSONObject meta = new JSONObject();
                    meta.put("metaname", "payment_reference")
                            .put("metavalue", posTransaction.getTraceref());

                    array.put(meta);
                }

                double fee = getPosFee(merchant, posTransaction.getAmount());

                jSONObject.put("applicable_fee", fee);

                if (posMerchantCustomer != null) {

                    JSONObject meta = new JSONObject();
                    meta.put("metaname", posMerchantCustomer.getParameterName())
                            .put("metavalue", posMerchantCustomer.getCustomerId());

                    array.put(meta);

                    meta = new JSONObject();
                    meta.put("metaname", posMerchantCustomer.getParameterName().replace("_id", "_name"))
                            .put("metavalue", posMerchantCustomer.getCustomerName());

                    array.put(meta);
                }

                jSONObject.put("meta", array);
                
                try{
                    jSONObject.put("provider", posTerminal.getProvider().getShortName());
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }

            jSONObject.put("posid", Utility.nullToEmpty(posTransaction.getPosId()));
            jSONObject.put("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
            jSONObject.put("datetime", Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
            jSONObject.put("responsecode", Utility.nullToEmpty(posTransaction.getResponseCode()));
            jSONObject.put("responsemessage", Utility.nullToEmpty(posTransaction.getResponseMessage()));
            jSONObject.put("mask", Utility.nullToEmpty(posTransaction.getPan()));
            jSONObject.put("type", Utility.nullToEmpty(posTransaction.getType()));
            jSONObject.put("status", Utility.nullToEmpty(posTransaction.getStatus()));
            jSONObject.put("amount", posTransaction.getAmount());
            jSONObject.put("currency", "NGN");
            jSONObject.put("fileid", posTransaction.getFileId());
            jSONObject.put("address", Utility.nullToEmpty(merchant.getAddress()));
            jSONObject.put("merchantname", Utility.nullToEmpty(merchant.getName()));
            jSONObject.put("reversed", posTransaction.isReversed());

            Log log = new Log();
            log.setAction("MPOS Call back for terminalId " + posTransaction.getTerminalId());
            log.setCreatedOn(new Date());
            log.setDescription(jSONObject.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.STARTED);
            log.setUsername("API");
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            List<CallbackConfiguration> configurations = callbackConfigurationDao.getConfiguration("pos", merchantId);

            if (configurations == null || configurations.isEmpty()) {
                return;
            }

            CallbackConfiguration callbackConfiguration = configurations.get(0);
            String urls = callbackConfiguration.getUrls();
            String[] urlArray = urls.split(",");
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");

            String result = "";

            String status = "failed", statusMessage = "Failed";

            for (String urlString : urlArray) {

                result += " U: " + urlString;
                Map<String, String> map = httpUtil.doHttpPostC(urlString, jSONObject.toString(), header);

                if (map == null) {
                    result += " S: failed,";
                } else {

                    status = map.getOrDefault("status", "failed");
                    result += " S: " + status;
                    result += " R: " + map.getOrDefault("result", "");

                    String dataPayload = map.getOrDefault("result", null);

                    try {

                        if (dataPayload != null) {
                            JSONObject sONObject = new JSONObject(dataPayload);

                            statusMessage = sONObject.optString("message", null);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            if ("success".equalsIgnoreCase(status)) {
                posTransaction.setCallBack(true);
                posTransaction.setCallBackResponse(result);
            } else if ("failed".equalsIgnoreCase(status)) {

                if (statusMessage != null) {

                    if (statusMessage.toLowerCase().contains("duplicate")) {
                        posTransaction.setCallBack(true);
                    }
                }
            }

            posTransaction.setCallbackOn(new Date());
            posTransaction.setReschedule(false);
            posTransactionDao.update(posTransaction);

        } catch (DatabaseException ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

    }

    @Lock(LockType.READ)
    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void callBackUserHospital(PosTransactionNew posTransaction, String merchantRef, PosMerchantProduct merchantProduct) {

        try {
            if (posTransaction == null) {
                return;
            }
            if (posTransaction.isCallBack() == true) {
                return;
            }
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());

            if (posTerminal == null) {
                return;
            }

//            if (!"00".equalsIgnoreCase(posTransaction.getResponseCode())) {
//                return;
//            }

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("terminalid", posTransaction.getTerminalId());

            String merchantId = posTransaction.getProductId();

            PosMerchant merchant = null;

            if (Utility.emptyToNull(posTransaction.getPosId()) == null) {

                if (posTerminal.getMerchant() != null) {
                    jSONObject.put("merchantid", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantId()));
                    jSONObject.put("merchantcode", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantCode()));

                    merchantId = posTerminal.getMerchant().getMerchantId();

                    merchant = posTerminal.getMerchant();
                }

            } else {

                PosMerchant posMerchant = posMerchantDao.findByKey("posId", posTransaction.getPosId());

                if (posMerchant == null) {
                    return;
                }

                jSONObject.put("merchantid", Utility.nullToEmpty(posMerchant.getMerchantId()));
                jSONObject.put("merchantcode", Utility.nullToEmpty(posMerchant.getMerchantCode()));

                merchant = posMerchant;
                merchantId = posMerchant.getMerchantId();
            }

            if (merchantId == null || merchant == null) {
                return;
            }

            if (posTerminal.getProvider() != null) {
                jSONObject.put("custom_ptsp", posTerminal.getProvider().getName());
                jSONObject.put("provider", posTerminal.getProvider().getShortName());
            } else {
                jSONObject.put("custom_ptsp", "");
            }
            
            if ("144091".equalsIgnoreCase(merchantId) || "123456".equalsIgnoreCase(merchantId)) {
                jSONObject.put("custom_chname", "");

                jSONObject.put("custom_ptspref", Utility.nullToEmpty(posTransaction.getRefCode()));

                
            } else if ("144163".equalsIgnoreCase(merchantId)) {

                jSONObject.put("flw_reference", Utility.nullToEmpty(posTransaction.getTransRef()));
                JSONArray array = new JSONArray();

                if (merchantProduct != null) {

                    JSONObject meta = new JSONObject();
                    meta.put("metaname", "revenuecode")
                            .put("metavalue", merchantProduct.getProductCode());

                    array.put(meta);

                    meta = new JSONObject();
                    meta.put("metaname", "servicename")
                            .put("metavalue", merchantProduct.getProductName());

                    array.put(meta);

                    meta = new JSONObject();
                    meta.put("metaname", "departmentname")
                            .put("metavalue", merchantProduct.getProductName());

                    array.put(meta);

                    meta = new JSONObject();
                    meta.put("metaname", "description")
                            .put("metavalue", merchantProduct.getProductDescription());

                    array.put(meta);
                    
                    String subAccount = getSubAccounts(merchantProduct);
                    
                    if(subAccount != null){
                        
                        jSONObject.put("subaccounts",new JSONArray(subAccount));
                    }
                    
                }
                else {
                    
                    String subAccount = getSubAccounts(posTransaction.getMerchantCode(), merchantRef);
                    
                    if(subAccount != null){
                        
                        jSONObject.put("subaccounts",new JSONArray(subAccount));
                    }
                }
                
                try{
                    
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                double fee = getPosFee(merchant, posTransaction.getAmount());

                jSONObject.put("applicable_fee", fee);

                if (Utility.emptyToNull(posTransaction.getMeta()) != null) {

                    try {
                        JSONObject metaObject = new JSONObject(posTransaction.getMeta());

                        metaObject.keySet().stream().map((key) -> {
                            JSONObject meta = new JSONObject();

                            meta.put("metavalue", metaObject.optString(key));
                            switch (((String) key).toUpperCase()) {

                                case "NAME":
                                    meta.put("metaname", "patientname");
                                    break;
                                default:
                                    meta.put("metaname", key.toLowerCase());
                                    break;
                            }

                            return meta;
                        }).forEachOrdered((meta) -> {
                            array.put(meta);
                        });

                    } catch (Exception ex) {
                    }
                }

                jSONObject.put("meta", array);
            }

            jSONObject.put("posid", Utility.nullToEmpty(posTransaction.getPosId()));
            jSONObject.put("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
            jSONObject.put("datetime", Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
            jSONObject.put("responsecode", Utility.nullToEmpty(posTransaction.getResponseCode()));
            jSONObject.put("responsemessage", Utility.nullToEmpty(posTransaction.getResponseMessage()));
            jSONObject.put("mask", Utility.nullToEmpty(posTransaction.getPan()));
            jSONObject.put("type", Utility.nullToEmpty(posTransaction.getType()));
            jSONObject.put("status", Utility.nullToEmpty(posTransaction.getStatus()));
            jSONObject.put("amount", posTransaction.getAmount());
            jSONObject.put("currency", "NGN");
            jSONObject.put("fileid", posTransaction.getFileId());
            jSONObject.put("address", Utility.nullToEmpty(merchant.getAddress()));
            jSONObject.put("merchantname", Utility.nullToEmpty(merchant.getName()));
            jSONObject.put("reversed", posTransaction.isReversed());

            Log log = new Log();
            log.setAction("MPOS Call back for terminalId " + posTransaction.getTerminalId());
            log.setCreatedOn(new Date());
            log.setDescription(jSONObject.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.STARTED);
            log.setUsername("API");
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            List<CallbackConfiguration> configurations = callbackConfigurationDao.getConfiguration("pos", merchantId);

            if (configurations == null || configurations.isEmpty()) {
                return;
            }

            CallbackConfiguration callbackConfiguration = configurations.get(0);
            String urls = callbackConfiguration.getUrls();
            String[] urlArray = urls.split(",");
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");

            String result = "";

            String status = "failed", statusMessage = "Failed";
            

            for (String urlString : urlArray) {

                result += " U: " + urlString;
                Map<String, String> map = httpUtil.doHttpPostC(urlString, jSONObject.toString(), header);

                if (map == null) {
                    result += " S: failed,";
                } else {

                    status = map.getOrDefault("status", "failed");
                    result += " S: " + status;
                    result += " R: " + map.getOrDefault("result", "");

                    String dataPayload = map.getOrDefault("result", null);

                    try {

                        if (dataPayload != null) {
                            JSONObject sONObject = new JSONObject(dataPayload);

                            statusMessage = sONObject.optString("message", null);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            if ("success".equalsIgnoreCase(status)) {
                posTransaction.setCallBack(true);
//                posTransaction.setCallBackResponse(result);
            } else if ("failed".equalsIgnoreCase(status)) {

                if (statusMessage != null) {

                    if (statusMessage.toLowerCase().contains("duplicate")) {
                        posTransaction.setCallBack(true);
                    }
                }
            }

            if (merchant.getNotificationUrl() != null) {

                try {

                    Map<String, String> map = httpUtil.doHttpPostC(merchant.getNotificationUrl(), jSONObject.toString(), header);

                } catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

            posTransaction.setCallbackOn(new Date());
            posTransaction.setReschedule(false);
            posTransactionNewDao.update(posTransaction);

        } catch (DatabaseException ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

    }

    public String getSubAccounts(PosMerchantProduct merchantProduct) {

        try {
            if (merchantProduct == null) {
                return null;
            }
            
            String prefix = merchantProduct.getProductCode().substring(0, 2);

            PosMerchantProductSettlement settlement = posMerchantProductSettlementDao.findByMerchantAndProductPrefix(merchantProduct.getMerchantCode(), prefix);
            
            Double parentCommission = 0.0; 
            
            String subAccounts = null;
            
            if (settlement != null) {
                parentCommission = settlement.getParentCommission() == null ? 0.0 : settlement.getParentCommission().doubleValue();
                
                subAccounts = settlement.getSubAccount();
            }

            if (Utility.emptyToNull(merchantProduct.getSubAccount()) != null) {
                
                subAccounts = merchantProduct.getSubAccount();
            }

            if( Utility.nullToEmpty(subAccounts) != null){
                
                JSONArray sONArray = new JSONArray(subAccounts);
                
                int length  = sONArray.length();
                
//                if(length > 1){
                
                    parentCommission = parentCommission / (length * 1.0);
                    
                    parentCommission /= 100.0;
                    
                    JSONArray newSubAccount = new JSONArray();
                    
                    double sum = getSummation(sONArray);
                    
                    for(int i = 0 ; i < length ; i++){
                        
                        JSONObject item = sONArray.optJSONObject(i);
                        
                        double share = Double.parseDouble(item.optString("transaction_split_ratio"));
                        
                        share /= sum;
                        
                        share *= 100;
                        
                        item.put("transaction_split_ratio", share);
                        item.put("transaction_charge_type", "percentage");
                        item.put("transaction_charge", parentCommission);
                        
                        newSubAccount.put(item);
                    }
                    
                    subAccounts = newSubAccount.toString();
//                }
            }
            
            return subAccounts;
            

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }
    
    private double getSummation(JSONArray array){
        
        int length  = array.length();
        
        double sum = 0.0;
        
        for(int i = 0 ; i < length ; i++){
                        
            JSONObject item = array.optJSONObject(i);

            sum += Double.parseDouble(item.optString("transaction_split_ratio"));
        }
        
        return sum;
    }
    
    public String getSubAccounts(String merchantCode, String productCode) {

        try {
            
            if(merchantCode == null || productCode == null)
                return null;
            
            String prefix = productCode.substring(0, 2);

            PosMerchantProductSettlement settlement = posMerchantProductSettlementDao.findByMerchantAndProductPrefix(merchantCode, prefix);

            if (settlement == null) {
                return null;
            }
            
            double parentCommission = settlement.getParentCommission() == null ? 0.0 : settlement.getParentCommission().doubleValue();
                
            String subAccounts = settlement.getSubAccount();
            
            if( Utility.nullToEmpty(subAccounts) != null){
                
                JSONArray sONArray = new JSONArray(subAccounts);
                
                int length  = sONArray.length();
                
//                if(length > 1){
                
                   parentCommission = parentCommission / (length * 1.0);
                    
                    parentCommission /= 100.0;
                    
                    JSONArray newSubAccount = new JSONArray();
                    
                    double sum = getSummation(sONArray);
                    
                    for(int i = 0 ; i < length ; i++){
                        
                        JSONObject item = sONArray.optJSONObject(i);
                        
                        double share = Double.parseDouble(item.optString("transaction_split_ratio"));
                        
                        share /= sum;
                        
                        share *= 100;
                        
                        item.put("transaction_split_ratio", share);
                        item.put("transaction_charge_type", "percentage");
                        item.put("transaction_charge", parentCommission);
                        
                        newSubAccount.put(item);
                    }
                    
                    subAccounts = newSubAccount.toString();
//                }
            }
            
            return subAccounts;

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return null;
    }

    @Lock(LockType.READ)
    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void callBackUser(PosTransaction posTransaction, String orderId, String metaData) {

        try {
            if (posTransaction == null) {
                return;
            }
            if (posTransaction.isCallBack() == true & !posTransaction.isReversed()) {
                return;
            }
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());

            if (posTerminal == null) {
                return;
            }

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("terminalid", posTransaction.getTerminalId());

            String merchantId = null;

            PosMerchant merchant = null;

            if (Utility.emptyToNull(posTransaction.getPosId()) == null) {

                if (posTerminal.getMerchant() != null) {
                    jSONObject.put("merchantid", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantId()));
                    jSONObject.put("merchantcode", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantCode()));

                    merchantId = posTerminal.getMerchant().getMerchantId();

                    merchant = posTerminal.getMerchant();
                } else {

                    jSONObject.put("merchantid", "");
                    jSONObject.put("merchantcode", "");
                }
            } else {

                PosMerchant posMerchant = posMerchantDao.findByKey("posId", posTransaction.getPosId());

                if (posMerchant == null) {
                    return;
                }

                jSONObject.put("merchantid", Utility.nullToEmpty(posMerchant.getMerchantId()));
                jSONObject.put("merchantcode", Utility.nullToEmpty(posMerchant.getMerchantCode()));

                merchant = posMerchant;
                merchantId = posMerchant.getMerchantId();
            }

            if (merchantId == null || merchant == null) {
                return;
            }

            if (posTerminal.getProvider() != null) {
                jSONObject.put("custom_ptsp", posTerminal.getProvider().getName());
            } else {
                jSONObject.put("custom_ptsp", "");
            }

            if ("144091".equalsIgnoreCase(merchantId) || "123456".equalsIgnoreCase(merchantId)) {

//                if (!"00".equalsIgnoreCase(posTransaction.getResponseCode())) {
//                    return;
//                }
                jSONObject.put("custom_chname", "");

                jSONObject.put("custom_ptspref", Utility.nullToEmpty(posTransaction.getRefCode()));

                
            } else if ("144163".equalsIgnoreCase(merchantId)) {

                jSONObject.put("flw_reference", Utility.nullToEmpty(posTransaction.getTransRef()));
                JSONArray array = new JSONArray();

                if (orderId != null) {

                    JSONObject meta = new JSONObject();
                    meta.put("metaname", "order_id")
                            .put("metavalue", orderId);

                    array.put(meta);
                }

                if (Utility.nullToEmpty(posTransaction.getTraceref()) != null) {

                    JSONObject meta = new JSONObject();
                    meta.put("metaname", "revenue_code")
                            .put("metavalue", posTransaction.getTraceref());

                    array.put(meta);
                }

                double fee = getPosFee(merchant, posTransaction.getAmount());

                jSONObject.put("applicable_fee", fee);

                if (metaData != null) {

                    try {
                        JSONObject meta = new JSONObject();

                        JSONObject objData = new JSONObject(metaData);

                        Iterator keys = objData.keys();

                        while (keys.hasNext()) {

                            String keyValue = (String) keys.next();

                            JSONObject metaItem = new JSONObject();
                            metaItem.put("metaname", keyValue)
                                    .put("metavalue", objData.getString(keyValue));

                            array.put(meta);
                        }

                    } catch (Exception ex) {
                    }
                }

                jSONObject.put("meta", array);
            }

            jSONObject.put("posid", Utility.nullToEmpty(posTransaction.getPosId()));
            jSONObject.put("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
            jSONObject.put("datetime", Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
            jSONObject.put("responsecode", Utility.nullToEmpty(posTransaction.getResponseCode()));
            jSONObject.put("responsemessage", Utility.nullToEmpty(posTransaction.getResponseMessage()));
            jSONObject.put("mask", Utility.nullToEmpty(posTransaction.getPan()));
            jSONObject.put("type", Utility.nullToEmpty(posTransaction.getType()));
            jSONObject.put("status", Utility.nullToEmpty(posTransaction.getStatus()));
            jSONObject.put("amount", posTransaction.getAmount());
            jSONObject.put("currency", "NGN");
            jSONObject.put("fileid", posTransaction.getFileId());
            jSONObject.put("address", Utility.nullToEmpty(merchant.getAddress()));
            jSONObject.put("merchantname", Utility.nullToEmpty(merchant.getName()));
            jSONObject.put("reversed", posTransaction.isReversed());

            Log log = new Log();
            log.setAction("MPOS Call back for terminalId " + posTransaction.getTerminalId());
            log.setCreatedOn(new Date());
            log.setDescription(jSONObject.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.STARTED);
            log.setUsername("API");
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            List<CallbackConfiguration> configurations = callbackConfigurationDao.getConfiguration("pos", merchantId);

            if (configurations == null || configurations.isEmpty()) {
                return;
            }

            CallbackConfiguration callbackConfiguration = configurations.get(0);
            String urls = callbackConfiguration.getUrls();
            String[] urlArray = urls.split(",");

            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");

            String result = "";

            String status = "failed", statusMessage = "Failed";

            for (String urlString : urlArray) {

                result += " U: " + urlString;
                Map<String, String> map = httpUtil.doHttpPostC(urlString, jSONObject.toString(), header);

                if (map == null) {
                    result += " S: failed,";
                } else {

                    status = map.getOrDefault("status", "failed");
                    result += " S: " + status;
                    result += " R: " + map.getOrDefault("result", "");

                    String dataPayload = map.getOrDefault("result", null);

                    try {

                        if (dataPayload != null) {
                            JSONObject sONObject = new JSONObject(dataPayload);

                            statusMessage = sONObject.optString("message", null);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            if (merchant.getNotificationUrl() != null) {

                try {

                    Map<String, String> map = httpUtil.doHttpPostC(merchant.getNotificationUrl(), jSONObject.toString(), header);

                } catch (Exception ex) {

                    ex.printStackTrace();
                }
            }

            if ("success".equalsIgnoreCase(status)) {
                posTransaction.setCallBack(true);
//                posTransaction.setCallBackResponse(result);
            } else if ("failed".equalsIgnoreCase(status)) {

                if (statusMessage != null) {

                    if (statusMessage.toLowerCase().contains("duplicate")) {
                        posTransaction.setCallBack(true);
                    }
                }
            }

            posTransaction.setCallbackOn(new Date());
            posTransaction.setReschedule(false);
            posTransactionDao.update(posTransaction);

        } catch (DatabaseException ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }
    }

    @Lock(LockType.READ)
    @Asynchronous
    @AccessTimeout(value = 120000)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void callBackUserReschedule(PosTransaction posTransaction) {

        try {
            if (posTransaction == null) {
                return;
            }

            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", posTransaction.getTerminalId());

            if (posTerminal == null) {
                return;
            }

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("terminalid", posTransaction.getTerminalId());

            String merchantId = null;

            PosMerchant merchant = null;

            if (Utility.emptyToNull(posTransaction.getPosId()) == null) {

                if (posTerminal.getMerchant() != null) {
                    jSONObject.put("merchantid", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantId()));
                    jSONObject.put("merchantcode", Utility.nullToEmpty(posTerminal.getMerchant().getMerchantCode()));

                    merchantId = posTerminal.getMerchant().getMerchantId();

                    merchant = posTerminal.getMerchant();
                } else {

                    jSONObject.put("merchantid", "");
                    jSONObject.put("merchantcode", "");
                }
            } else {

                PosMerchant posMerchant = posMerchantDao.findByKey("posId", posTransaction.getPosId());

                if (posMerchant == null) {
                    return;
                }

                jSONObject.put("merchantid", Utility.nullToEmpty(posMerchant.getMerchantId()));
                jSONObject.put("merchantcode", Utility.nullToEmpty(posMerchant.getMerchantCode()));

                merchant = posMerchant;
                merchantId = posMerchant.getMerchantId();
            }

            if (merchantId == null || merchant == null) {
                return;
            }
            
            if (posTerminal.getProvider() != null) {
                jSONObject.put("custom_ptsp", posTerminal.getProvider().getName());
            } else {
                jSONObject.put("custom_ptsp", "");
            }

            if ("144091".equalsIgnoreCase(merchantId) || "123456".equalsIgnoreCase(merchantId)) {

//                if (!"00".equalsIgnoreCase(posTransaction.getResponseCode())) {
//                    return;
//                }
                jSONObject.put("custom_chname", "");

                jSONObject.put("custom_ptspref", Utility.nullToEmpty(posTransaction.getRefCode()));
            }

            if ("144163".equalsIgnoreCase(merchantId) || "123456".equalsIgnoreCase(merchantId)) {
//                jSONObject.put("custom_chname", "");

                double fee = getPosFee(merchant, posTransaction.getAmount());

                jSONObject.put("applicable_fee", fee);

                jSONObject.put("flw_reference", Utility.nullToEmpty(posTransaction.getTransRef()));
                
                 try{
                    jSONObject.put("provider", posTerminal.getProvider().getShortName());
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }

            jSONObject.put("posid", Utility.nullToEmpty(posTransaction.getPosId()));
            jSONObject.put("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
            jSONObject.put("datetime", Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
            jSONObject.put("responsecode", Utility.nullToEmpty(posTransaction.getResponseCode()));
            jSONObject.put("responsemessage", Utility.nullToEmpty(posTransaction.getResponseMessage()));
            jSONObject.put("mask", Utility.nullToEmpty(posTransaction.getPan()));
            jSONObject.put("type", Utility.nullToEmpty(posTransaction.getType()));
            jSONObject.put("status", Utility.nullToEmpty(posTransaction.getStatus()));
            jSONObject.put("amount", posTransaction.getAmount());
            jSONObject.put("currency", Utility.nullToEmpty("NGN"));
            jSONObject.put("fileid", Utility.nullToEmpty(posTransaction.getFileId() == null ? posTransaction.getTransRef() : posTransaction.getFileId()));
            jSONObject.put("address", Utility.nullToEmpty(merchant.getAddress()));
            jSONObject.put("merchantname", Utility.nullToEmpty(merchant.getName()));
            jSONObject.put("reversed", posTransaction.isReversed());
            
            if("internalmobile".equalsIgnoreCase(posTransaction.getSource())){
                
                jSONObject.put("channel", "MOBILE");
            }
            else
                jSONObject.put("channel", "POS");

            Log log = new Log();
            log.setAction("MPOS Call back for terminalId " + posTransaction.getTerminalId());
            log.setCreatedOn(new Date());
            log.setDescription(jSONObject.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.STARTED);
            log.setUsername("API");
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            List<CallbackConfiguration> configurations = callbackConfigurationDao.getConfiguration("pos", merchantId);

            if (configurations == null || configurations.isEmpty()) {
                return;
            }

            CallbackConfiguration callbackConfiguration = configurations.get(0);
            String urls = callbackConfiguration.getUrls();
            String[] urlArray = urls.split(",");
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");

            String result = "";
            for (String urlString : urlArray) {

                result += " U: " + urlString;
                Map<String, String> map = httpUtil.doHttpPostC(urlString, jSONObject.toString(), header);

                if (map == null) {
                    result += " S: failed,";
                } else {

                    String status = map.getOrDefault("status", "failed");

                    String statusMessage = null;

                    result += " S: " + status;
                    result += " R: " + map.getOrDefault("result", "");

                    if ("success".equalsIgnoreCase(status)) {

                        posTransaction.setCallBack(true);
//                       posTransaction.setCallBackResponse(result);
                    } else {

                        String dataPayload = map.getOrDefault("result", null);

                        try {

                            if (dataPayload != null) {
                                JSONObject sONObject = new JSONObject(dataPayload);

                                statusMessage = sONObject.optString("message", null);
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if (statusMessage != null) {

                            if (statusMessage.toLowerCase().contains("duplicate")) {
                                posTransaction.setCallBack(true);
                            }
                        }
                    }

                }

                posTransaction.setCallbackOn(new Date());
                posTransaction.setReschedule(false);
                posTransaction.setRescheduleOn(new Date());

                posTransactionDao.update(posTransaction);
            }

        } catch (Exception ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

    }

    @Lock(LockType.READ)
    @Asynchronous
    @AccessTimeout(value = 120000)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void callBackUserReschedule(PosTransactionViewModelWeb posTransaction) {

        try {
            if (posTransaction == null) {
                return;
            }

            JSONObject jSONObject = new JSONObject();
            jSONObject.put("terminalid", posTransaction.getTerminalId());
            jSONObject.put("merchantid", Utility.nullToEmpty(posTransaction.getMerchantId()));
            jSONObject.put("merchantcode", Utility.nullToEmpty(posTransaction.getMerchantCode()));

            String merchantId = posTransaction.getMerchantId();

            PosMerchant merchant = null;

            {

                PosMerchant posMerchant = posMerchantDao.findByKey("posId", posTransaction.getPosId());

                if (posMerchant == null) {
                    return;
                }

                jSONObject.put("merchantid", Utility.nullToEmpty(posMerchant.getMerchantId()));
                jSONObject.put("merchantcode", Utility.nullToEmpty(posMerchant.getMerchantCode()));

                merchant = posMerchant;
                merchantId = posMerchant.getMerchantId();
            }

            if (Utility.emptyToNull(merchantId) == null) {
                return;
            }
            
            jSONObject.put("custom_ptsp", posTransaction.getProvider());

            if ("144091".equalsIgnoreCase(merchantId) || "123456".equalsIgnoreCase(merchantId)) {

//                if (!"00".equalsIgnoreCase(posTransaction.getResponseCode())) {
//                    return;
//                }
                jSONObject.put("custom_chname", "");

                jSONObject.put("custom_ptspref", Utility.nullToEmpty(posTransaction.getRefCode()));

//                if (posTerminal.getProvider() != null) {
                
//                } else {
//                    jSONObject.put("custom_ptsp", "");
//                }
            }

//            if ("144091".equalsIgnoreCase(merchantId)) {
//
//                jSONObject.put("terminalid", posTransaction.getTerminalId());
//                jSONObject.put("posid", Utility.nullToEmpty(posTransaction.getPosId()));
//                jSONObject.put("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
//                jSONObject.put("datetime", Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
//                jSONObject.put("mask", Utility.nullToEmpty(posTransaction.getPan()));
//                jSONObject.put("status", Utility.nullToEmpty(posTransaction.isReversed() == true ? "reverse" : "successful"));
//                jSONObject.put("tellerpointref", "");
//                jSONObject.put("chname", "");
//
//            } else {
            jSONObject.put("posid", Utility.nullToEmpty(posTransaction.getPosId()));
            jSONObject.put("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
            jSONObject.put("datetime", Utility.nullToEmpty(posTransaction.getDatetime()));
            jSONObject.put("responsecode", Utility.nullToEmpty(posTransaction.getResponseCode()));
            jSONObject.put("responsemessage", Utility.nullToEmpty(posTransaction.getResponseMessage()));
            jSONObject.put("mask", Utility.nullToEmpty(posTransaction.getPan()));
            jSONObject.put("type", Utility.nullToEmpty(posTransaction.getType()));
            jSONObject.put("status", Utility.nullToEmpty(posTransaction.getStatus()));
            jSONObject.put("amount", posTransaction.getAmount());
            jSONObject.put("currency", Utility.nullToEmpty("NGN"));
            jSONObject.put("fileid", Utility.nullToEmpty(posTransaction.getFileId() == null ? posTransaction.getRefCode() : posTransaction.getFileId()));
            jSONObject.put("address", Utility.nullToEmpty(merchant.getAddress()));
            jSONObject.put("merchantname", Utility.nullToEmpty(posTransaction.getMerchantName()));
            jSONObject.put("reversed", posTransaction.isReversed());
            
            if("internalmobile".equalsIgnoreCase(posTransaction.getSource())){
                
                jSONObject.put("channel", "MOBILE");
            }
            else
                jSONObject.put("channel", "POS");
            //}

            Log log = new Log();
            log.setAction("MPOS Call back for terminalId " + posTransaction.getTerminalId());
            log.setCreatedOn(new Date());
            log.setDescription(jSONObject.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.STARTED);
            log.setUsername("API");
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            List<CallbackConfiguration> configurations = callbackConfigurationDao.getConfiguration("pos", merchantId);

            if (configurations == null || configurations.isEmpty()) {
                return;
            }

            CallbackConfiguration callbackConfiguration = configurations.get(0);
            String urls = callbackConfiguration.getUrls();
            String[] urlArray = urls.split(",");
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");

            PosTransaction transaction = posTransactionDao.find(posTransaction.getId());

            if ("144163".equalsIgnoreCase(merchantId) || "123456".equalsIgnoreCase(merchantId)) {
//                jSONObject.put("custom_chname", "");

                jSONObject.put("flw_reference", Utility.nullToEmpty(transaction.getTransRef()));

//                if (posTerminal.getProvider() != null) {
//                    jSONObject.put("custom_ptsp", posTerminal.getProvider().getName());
//                } else {
//                    jSONObject.put("custom_ptsp", "");
//                }
            }

            String result = "";
            for (String urlString : urlArray) {

                result += " U: " + urlString;
                Map<String, String> map = httpUtil.doHttpPostC(urlString, jSONObject.toString(), header);

                if (map == null) {
                    result += " S: failed,";
                } else {

                    String status = map.getOrDefault("status", "failed");

                    String statusMessage = null;

                    result += " S: " + status;
                    result += " R: " + map.getOrDefault("result", "");

                    if ("success".equalsIgnoreCase(status)) {

                        transaction.setCallBack(true);
                        transaction.setCallBackResponse(result);
                    } else {

                        String dataPayload = map.getOrDefault("result", null);

                        try {

                            if (dataPayload != null) {
                                JSONObject sONObject = new JSONObject(dataPayload);

                                statusMessage = sONObject.optString("message", null);
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if (statusMessage != null) {

                            if (statusMessage.toLowerCase().contains("duplicate")) {
                                transaction.setCallBack(true);
                            }
                        }
                    }
                }

                transaction.setCallbackOn(new Date());

                posTransactionDao.update(transaction);
//                posTransaction.setReschedule(false);
//                posTransaction.setRescheduleOn(new Date());

            }

        } catch (Exception ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

    }

    @Lock(LockType.READ)
    @Asynchronous
    @AccessTimeout(value = 12000)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void callBackUserETOP(PosTransaction posTransaction, String cardholder, boolean reversed) {

        try {
            if (posTransaction == null) {
                return;
            }

            if (posTransaction.isCallBack() == true && reversed == false) {
                return;
            }

            PosMerchant posMerchant = posMerchantDao.findByKey("posId", posTransaction.getPosId());

            if (posMerchant == null) {
                return;
            }

            JSONObject jSONObject = new JSONObject();
            
            jSONObject.put("custom_ptsp", "ETOP");

            if ("144091".equalsIgnoreCase(posMerchant.getMerchantId()) || "123456".equalsIgnoreCase(posMerchant.getMerchantId())) {

//                if (!"00".equalsIgnoreCase(posTransaction.getResponseCode())) {
//                    return;
//                }
                jSONObject.put("custom_chname", cardholder);

                jSONObject.put("custom_ptspref", Utility.nullToEmpty(posTransaction.getRefCode()));

                jSONObject.put("custom_ptsp", "ETOP");

            } else {
                
                

            }

//            if (!"144091".equalsIgnoreCase(posMerchant.getMerchantId())) {
            jSONObject.put("terminalid", posTransaction.getTerminalId());
            jSONObject.put("merchantid", Utility.nullToEmpty(posMerchant.getMerchantId()));
            jSONObject.put("merchantcode", Utility.nullToEmpty(posMerchant.getMerchantCode()));
            jSONObject.put("posid", Utility.nullToEmpty(posTransaction.getPosId()));
            jSONObject.put("rrn", Utility.nullToEmpty(posTransaction.getRrn()));
            jSONObject.put("datetime", Utility.formatDate(posTransaction.getRequestDate(), "yyyy-MM-dd HH:mm:ss"));
            jSONObject.put("responsecode", Utility.nullToEmpty(posTransaction.getResponseCode()));
            jSONObject.put("responsemessage", Utility.nullToEmpty(posTransaction.getResponseMessage()));
            jSONObject.put("mask", Utility.nullToEmpty(posTransaction.getPan()));
            jSONObject.put("type", Utility.nullToEmpty(posTransaction.getType()));
            jSONObject.put("status", Utility.nullToEmpty(posTransaction.getStatus()));
            jSONObject.put("amount", posTransaction.getAmount());
            jSONObject.put("currency", "NGN");
            jSONObject.put("fileid", Utility.nullToEmpty(posTransaction.getFileId()));
            jSONObject.put("address", Utility.nullToEmpty(posMerchant.getAddress()));
            jSONObject.put("merchantname", Utility.nullToEmpty(posMerchant.getName()));
            jSONObject.put("reversed", reversed);

            Log log = new Log();
            log.setAction("MPOS Call back for terminalId " + posTransaction.getTerminalId());
            log.setCreatedOn(new Date());
            log.setDescription(jSONObject.toString());
            log.setLevel(LogLevel.Info);
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.STARTED);
            log.setUsername("API");
            log.setStatus(LogStatus.SUCCESSFUL);
            logService.log(log);

            List<CallbackConfiguration> configurations = callbackConfigurationDao.getConfiguration("pos", posMerchant.getMerchantId());
            if (configurations == null || configurations.isEmpty()) {
                return;
            }
            CallbackConfiguration callbackConfiguration = configurations.get(0);
            String urls = callbackConfiguration.getUrls();
            String[] urlArray = urls.split(",");
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");

            String result = "";
            for (String urlString : urlArray) {

                result += " U: " + urlString;
                Map<String, String> map = httpUtil.doHttpPostC(urlString, jSONObject.toString(), header);

                if (map == null) {
                    result += " S: failed,";
                } else {

                    String status = map.getOrDefault("status", "failed");

                    String statusMessage = null;

                    result += " S: " + map.getOrDefault("status", "failed");
                    result += " R: " + map.getOrDefault("result", "");

                    if ("success".equalsIgnoreCase(status)) {
                        posTransaction.setCallBack(true);
                    } else {

                        String dataPayload = map.getOrDefault("result", null);

                        try {

                            if (dataPayload != null) {
                                JSONObject sONObject = new JSONObject(dataPayload);

                                statusMessage = sONObject.optString("message", null);
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if (statusMessage != null) {

                            if (statusMessage.toLowerCase().contains("duplicate")) {
                                posTransaction.setCallBack(true);
                            }
                        }
                    }
                }
            }

//            posTransaction.setCallBackResponse(result);
            posTransaction.setCallbackOn(new Date());
            posTransaction.setReschedule(false);
            posTransactionDao.update(posTransaction);

        } catch (DatabaseException ex) {
            Logger.getLogger(UtilityService.class.getName()).error(null, ex);
        }

    }

    @Lock(LockType.READ)
    @Asynchronous
    @AccessTimeout(value = 1200000)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void queueTransactions(List<PosTransaction> posTransactions) {

        if (posTransactions == null || posTransactions.isEmpty()) {
            return;
        }

        for (int i = 0; i < posTransactions.size(); i++) {

            callBackUserReschedule(posTransactions.get(i));

            if ((i % 50) == 49) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    java.util.logging.Logger.getLogger(UtilityService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Lock(LockType.READ)
    @Asynchronous
    @AccessTimeout(value = 600000)
    public void fetchTransationFromProvider(Date startDate, Date endDate, String posId) {

        if (startDate == null || endDate == null) {
            return;
        }

        JSONArray jSONArray = eTopPosService.getTransaction(Utility.emptyToNull(posId), startDate, endDate);

        if (jSONArray == null) {
            return;
        }

        int len = jSONArray.length();

        if (len == 0) {
            return;
        }

        for (int i = 0; i < len; i++) {

            JSONObject jSONObject = jSONArray.optJSONObject(i);

            if (jSONObject == null) {
                return;
            }

            TellerPointCallbackRequest callbackRequest = new TellerPointCallbackRequest();
            callbackRequest.setAcquiringBank(jSONObject.optString("acquiringBank", null));
            callbackRequest.setAmount(jSONObject.optDouble("amount", 0.0));
            callbackRequest.setApprovalCode(jSONObject.optString("approvalCode"));
            callbackRequest.setCardHolderIssuingBank(jSONObject.optString("cardHolderIssuingBank"));
            callbackRequest.setCardholderName(jSONObject.optString("cardholderName"));
            callbackRequest.setCreationTime(jSONObject.optString("creationTime"));
            callbackRequest.setMaskedCardPan(jSONObject.optString("maskedCardPan"));
            callbackRequest.setMerchantId(jSONObject.optString("merchantId"));
            callbackRequest.setMerchantName(jSONObject.optString("merchantName"));
            callbackRequest.setProviderRef(jSONObject.optString("providerRef"));
            callbackRequest.setStatus(jSONObject.optString("status"));
            callbackRequest.setTellerpointRef(jSONObject.optString("tellerpointRef"));
            callbackRequest.setTerminalId(jSONObject.optString("terminalId"));
            callbackRequest.setTraceRef(jSONObject.optString("traceRef"));
            callbackRequest.setTransactionCharge(jSONObject.optInt("transactionCharge", 0));

            savePosTransaction(callbackRequest);
        }

    }

    @Lock(LockType.READ)
    @Asynchronous
    @AccessTimeout(value = 600000)
    public void fetchFailedTransationFromProvider(Date startDate, Date endDate, String posId) {

        if (startDate == null || endDate == null) {
            return;
        }

        JSONArray jSONArray = eTopPosService.getTransaction(Utility.emptyToNull(posId), startDate, endDate);

        if (jSONArray == null) {
            return;
        }

        int len = jSONArray.length();

        if (len == 0) {
            return;
        }

        for (int i = 0; i < len; i++) {

            JSONObject jSONObject = jSONArray.optJSONObject(i);

            if (jSONObject == null) {
                return;
            }

            TellerPointCallbackRequest callbackRequest = new TellerPointCallbackRequest();
            callbackRequest.setAcquiringBank(jSONObject.optString("acquiringBank", null));
            callbackRequest.setAmount(jSONObject.optDouble("amount", 0.0));
            callbackRequest.setApprovalCode(jSONObject.optString("approvalCode"));
            callbackRequest.setCardHolderIssuingBank(jSONObject.optString("cardHolderIssuingBank"));
            callbackRequest.setCardholderName(jSONObject.optString("cardholderName"));
            callbackRequest.setCreationTime(jSONObject.optString("creationTime"));
            callbackRequest.setMaskedCardPan(jSONObject.optString("maskedCardPan"));
            callbackRequest.setMerchantId(jSONObject.optString("merchantId"));
            callbackRequest.setMerchantName(jSONObject.optString("merchantName"));
            callbackRequest.setProviderRef(jSONObject.optString("providerRef"));
            callbackRequest.setStatus(jSONObject.optString("status"));
            callbackRequest.setTellerpointRef(jSONObject.optString("tellerpointRef"));
            callbackRequest.setTerminalId(jSONObject.optString("terminalId"));
            callbackRequest.setTraceRef(jSONObject.optString("traceRef"));
            callbackRequest.setTransactionCharge(jSONObject.optInt("transactionCharge", 0));

            savePosTransaction(callbackRequest);
        }

    }

    @Lock(LockType.READ)
    @Asynchronous
    @AccessTimeout(value = 600000)
    public void fetchFailedNotificationTransationFromProvider(Date startDate, Date endDate, String posId) {

        JSONArray jSONArray = eTopPosService.getFailedNotifiedTransaction(posId, startDate, endDate);

        if (jSONArray == null) {
            return;
        }

        int len = jSONArray.length();

        if (len == 0) {
            return;
        }

        for (int i = 0; i < len; i++) {

            JSONObject jSONObject = jSONArray.optJSONObject(i);

            if (jSONObject == null) {
                return;
            }

            TellerPointCallbackRequest callbackRequest = new TellerPointCallbackRequest();
            callbackRequest.setAcquiringBank(jSONObject.optString("acquiringBank", null));
            callbackRequest.setAmount(jSONObject.optDouble("amount", 0.0));
            callbackRequest.setApprovalCode(jSONObject.optString("approvalCode"));
            callbackRequest.setCardHolderIssuingBank(jSONObject.optString("cardHolderIssuingBank"));
            callbackRequest.setCardholderName(jSONObject.optString("cardholderName"));
            callbackRequest.setCreationTime(jSONObject.optString("creationTime"));
            callbackRequest.setMaskedCardPan(jSONObject.optString("maskedCardPan"));
            callbackRequest.setMerchantId(jSONObject.optString("merchantId"));
            callbackRequest.setMerchantName(jSONObject.optString("merchantName"));
            callbackRequest.setProviderRef(jSONObject.optString("providerRef"));
            callbackRequest.setStatus(jSONObject.optString("status"));
            callbackRequest.setTellerpointRef(jSONObject.optString("tellerpointRef"));
            callbackRequest.setTerminalId(jSONObject.optString("terminalId"));
            callbackRequest.setTraceRef(jSONObject.optString("traceRef"));
            callbackRequest.setTransactionCharge(jSONObject.optInt("transactionCharge", 0));

            savePosTransaction(callbackRequest);
        }

    }

    @Lock(LockType.READ)
    @Asynchronous
    @AccessTimeout(value = 12000)
    private void savePosTransaction(TellerPointCallbackRequest request) {

        try {
            PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", request.getTerminalId());

            if (posTerminal == null) {
                return;
            }

            List<TellerPointTransaction> transactions = pointTransactionDao.findByTerminalAndReference(request.getTerminalId(), request.getMerchantId(), request.getTellerpointRef());

            TellerPointTransaction pointTransaction;

            if (transactions == null || transactions.isEmpty()) {
                pointTransaction = new TellerPointTransaction();
                pointTransaction.setAcquiringBank(request.getAcquiringBank());
                pointTransaction.setAmount(request.getAmount());
                pointTransaction.setApprovalCode(request.getApprovalCode());
                pointTransaction.setCreatedBy("Fetching");
                pointTransaction.setCardHolderIssuingBank(request.getCardHolderIssuingBank());
                pointTransaction.setCardholderName(request.getCardholderName());
                pointTransaction.setCreatedOn(new Date());
                pointTransaction.setCreationTime(Utility.parseDate(request.getCreationTime(), "yyyy-MM-dd HH:mm:ss Z"));
                pointTransaction.setMaskedCardPan(request.getMaskedCardPan());
                pointTransaction.setMerchantName(request.getMaskedCardPan());
                pointTransaction.setPosId(request.getMerchantId());
                pointTransaction.setProviderRef(request.getProviderRef());
                pointTransaction.setStatus(request.getStatus());
                pointTransaction.setTellerpointRef(request.getTellerpointRef());
                pointTransaction.setTerminalId(request.getTerminalId());
                pointTransaction.setTraceRef(request.getTraceRef());
                pointTransaction.setTransactionCharge(request.getTransactionCharge());

                String responseRef = "flw" + Utility.generateReference();

                pointTransaction.setReversalResponseRef(responseRef);

                pointTransaction = pointTransactionDao.create(pointTransaction);
            } else {
                pointTransaction = transactions.get(0);
            }

            PosTransaction posTransaction = posTransactionDao.findRRNTerminal(request.getTraceRef(), request.getTerminalId(), pointTransaction.getCreationTime());

            if (posTransaction == null) {

                posTransaction = new PosTransaction();
                posTransaction.setAmount(pointTransaction.getAmount());
                posTransaction.setCreatedBy(null);
                posTransaction.setCreatedOn(new Date());
                posTransaction.setCurrency("NGN");
                posTransaction.setLoggedBy("");
                posTransaction.setPan(pointTransaction.getMaskedCardPan());
                posTransaction.setRrn(Long.parseLong(pointTransaction.getTraceRef()) + "");
                posTransaction.setTerminalId(pointTransaction.getTerminalId());
                posTransaction.setRefCode(pointTransaction.getTellerpointRef());
                posTransaction.setFileId(pointTransaction.getProviderRef());
                posTransaction.setFileName("HexTremeLab");
                posTransaction.setCurrencyCode("566");
                posTransaction.setPosId(pointTransaction.getPosId());
                posTransaction.setRequestDate(pointTransaction.getCreationTime());
                posTransaction.setResponseDate(pointTransaction.getCreationTime());
                posTransaction.setResponseCode("00");
                posTransaction.setResponseMessage(pointTransaction.getStatus());
                posTransaction.setType("PURCHASE");
                posTransaction.setSource("HexTremeLab " + pointTransaction.getTraceRef());
                posTransaction.setStatus(pointTransaction.getStatus());
                posTransaction.setTraceref(pointTransaction.getTraceRef());

                String transRef = request.getTerminalId() + "" + posTransaction.getRrn() + "" + posTransaction.getAmount() + "" + Utility.formatDate(posTransaction.getRequestDate(), "yyyyMMdd");

                posTransaction.setTransRef(transRef);

                posTransaction = posTransactionDao.create(posTransaction);
            }

            PosMerchant merchant = null;

            if (Utility.emptyToNull(posTransaction.getPosId()) == null) {

                if (posTerminal.getMerchant() != null) {

                    merchant = posTerminal.getMerchant();
                    posTransaction.setMerchantName(merchant.getName());
                }
            } else {

                PosMerchant posMerchant = posMerchantDao.findByKey("posId", posTransaction.getPosId());

                if (posMerchant == null) {
                    return;
                }

                posTransaction.setMerchantName(merchant.getName());
                merchant = posMerchant;
            }

            try {
                PosTransactionNew posTransactionNew = null;

                if (merchant != null) {
                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, null, request.getAcquiringBank(),
                            request.getCardholderName(), null, merchant.getParent() == null ? null : merchant.getParent().getName(), merchant.getMerchantId(),
                            merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), request.getCardHolderIssuingBank(),
                            merchant, posTerminal.getProvider(), null);
                } else {
                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, null, request.getAcquiringBank(),
                            request.getCardholderName(), null, null, null,
                            null, posTerminal.getProvider().getName(), null, request.getCardHolderIssuingBank(), merchant, posTerminal.getProvider(), null);
                }

                PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(posTransaction.getRrn(),
                        request.getTerminalId(), posTransaction.getRequestDate());

                if (posTransactionN == null) {
                    posTransactionNewDao.create(posTransactionNew);
                } else {

                    posTransactionNew.setId(posTransactionN.getId());
                    posTransactionNewDao.update(posTransactionNew);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (posTransaction.getCallbackOn() == null) {
                callBackUserReschedule(posTransaction);
            }

        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(UtilityService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Lock(LockType.READ)
    @Asynchronous
    public void approveCompliance(MerchantCompliance compliance) {

        if ("moneywave".equalsIgnoreCase(compliance.getProducts())) {

            try {
                MoneywaveMerchant merchant = moneywaveMerchantDao.find(Long.parseLong(compliance.getMerchantId()));
                if (merchant != null) {

                    try {
                        JsonObject jsonObject;

                        jsonObject = httpUtil.complianceMoneywaveMerchant(merchant.getSecret(), true);

                        if (jsonObject == null) {
                            return;
                        }

                        compliance.setApprovedOn(new Date());

                        compliance.setStatus(Status.APPROVED);

                        compliance.setLastProcessed(new Date());
                        compliance.setProcessingCounter(compliance.getProcessingCounter() + 1);

                        MerchantComplianceLog merchantComplianceLog = MerchantComplianceLog.fromCompliance(compliance);

                        merchantCompliantLogDao.create(merchantComplianceLog);

                        merchantCompliantDao.update(compliance);

                        notificationManager.sendComplianceApprovedEmail(compliance.getContactEmail(),
                                compliance.getRegisteredName(), compliance.getProducts());
                    } catch (DatabaseException ex) {
                        java.util.logging.Logger.getLogger(UtilityService.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            } catch (DatabaseException ex) {
                java.util.logging.Logger.getLogger(UtilityService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public List<String> getBankNames() {

        if (bankNames.isEmpty()) {
            bankNames.add("Access Bank");
            bankNames.add("Citibank");
            bankNames.add("Diamond Bank");
            bankNames.add("Ecobank");
            bankNames.add("Fidelity Bank");
            bankNames.add("FCMB");
            bankNames.add("First Bank");
            bankNames.add("GtBank");
            bankNames.add("Heritage Bank");
            bankNames.add("Keystone Bank");
            bankNames.add("Providus Bank");
            bankNames.add("Polaris Bank");
            bankNames.add("Stanbic IBTC");
            bankNames.add("Standard Chartered");
            bankNames.add("Sterling Bank");
            bankNames.add("Suntrust Bank");
            bankNames.add("Union Bank");
            bankNames.add("UBA");
            bankNames.add("Unity Bank");
            bankNames.add("Wema Bank");
            bankNames.add("Zenith Bank");
        }

        return bankNames;
    }

    public void sendPOSReport(PosMerchant posMerchant, Date startDate, Date endDate, String... emails) {

        if (posMerchant == null) {
            return;
        }

        List<PosTransactionViewModelWeb> list = posTransactionNewDao.getSettlmentTransaction(posMerchant.getMerchantCode(), startDate, endDate);

        if (list == null || list.isEmpty()) {
            return;
        }

//        List<PosTransactionViewModelWeb> list = page.getContent();
        List<PosSettlementModel> posSettlementModels = new ArrayList<>();

        for (PosTransactionViewModelWeb viewModelWeb : list) {

            PosSettlementModel settlementModel = new PosSettlementModel();
            settlementModel.setAmount(viewModelWeb.getAmount());
            settlementModel.setCurrency("NGN");
            settlementModel.setDatetime(viewModelWeb.getDatetime());
            settlementModel.setMaskedPan(viewModelWeb.getPan());
            settlementModel.setMerchantName(posMerchant.getName());
            settlementModel.setStatus(viewModelWeb.getStatus());
            settlementModel.setTerminalId(viewModelWeb.getTerminalId());
            settlementModel.setType(viewModelWeb.getType());

            double fee = getPosFee(posMerchant, viewModelWeb.getAmount());

            settlementModel.setFee(fee);

            posSettlementModels.add(settlementModel);
        }

        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet worksheet = workbook.createSheet("Settlement Record");

        int startRowIndex = 0;
        int startColIndex = 0;

//        if(ema)
//        if ("admin@flutterwavego.com".equalsIgnoreCase(email)) {
//            email = "emmanuel@flutterwavego.com";
//        }
        String formattedName = Utility.removeNonLetterChar(posMerchant.getName());

        formattedName = formattedName.replace(",", "");

        int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) Utility.getClassFields(PosSettlementModel.class), formattedName + " POS Settlement", null);

        FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, (List) posSettlementModels);

        SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                true, Global.USE_STARTTLS, false);

        notificationManager.sendPOSSettlementFile(mailSender, startDate, "POS Settlement for " + Utility.formatDate(startDate, "yyyy-MM-dd"), formattedName, emails, workbook);
    }

    private double getPosFee(PosMerchant posMerchant, Double amount) {

        double fee = 0.0;

        if (posMerchant.getFeeType() != null) {
            switch (posMerchant.getFeeType()) {

                case BOTH:
                    fee = (posMerchant.getPercentageFee() / 100.0) * amount + posMerchant.getFlatFee();
                    break;
                case FLAT:
                    fee = posMerchant.getFlatFee();
                    break;
                case PERCENTAGE:
                    fee = (posMerchant.getPercentageFee() / 100.0) * amount;
                    break;

            }
        }

        if (fee <= 0.0) {
            fee = (0.75 / 100.0) * amount;
        }

        if (posMerchant.getFeeCap() > 0.0 && fee > posMerchant.getFeeCap()) {

            fee = posMerchant.getFeeCap();
        }

        return fee;
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @AccessTimeout(value = 120000)
    public void buildAndDropCoreReportToSlack(Date date) {

        try {

//            calendar.add(Calendar.DATE, -1);
//            Date date = calendar.getTime();
            List<CoreSettlement> coreSettlements = coreSettlementDao.findSummaryByQuery(date, null, null, null, null);

            List<CoreSettlementMerchant> report = coreSettlementDao.findSummaryMerchantByQuery(date, null, null, null, null);

            List<CoreSettlementProvider> providerReport = coreSettlementDao.findSummaryProviderByQuery(date, null, null, null, null);

            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet worksheet = workbook.createSheet("Settlement Transactions");

            int startRowIndex = 0;
            int startColIndex = 0;

            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) Utility.getClassFields(CoreSettlement.class), "Settlement Report", null);

            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, "dd/MM/yyyy HH:mm:ss", (List) coreSettlements);

            XSSFSheet worksheetSummary = workbook.createSheet("Settlement Transactions Summary");

            startRowIndex = 0;
            startColIndex = 0;

            usedRows = Layouter1.buildReport(worksheetSummary, startRowIndex, startColIndex, (List) Utility.getClassFields(CoreSettlementMerchant.class), "Settlement Summary Report", null);

            FillManager.fillTransactionReport(worksheetSummary, usedRows - 1, startColIndex, "dd/MM/yyyy HH:mm:ss", (List) report);

            XSSFSheet worksheetSummaryProvider = workbook.createSheet("Provider Summary");
//
            startRowIndex = 0;
            startColIndex = 0;

            usedRows = Layouter1.buildReport(worksheetSummaryProvider, startRowIndex, startColIndex, (List) Utility.getClassFields(CoreSettlementProvider.class), "Settlment Transaction Summary by Provider", null);

            FillManager.fillTransactionReport(worksheetSummaryProvider, usedRows - 1, startColIndex, "dd/MM/yyyy HH:mm:ss", (List) providerReport);

            System.out.println("pushing core report");

            OutputStream out = null;
            try {

                File tempFile = File.createTempFile("Settlement_" + Utility.formatDate(date, "yyyyMMdd") + "_transaction_", ".xlsx");

                out = new FileOutputStream(tempFile);
                workbook.write(out);

                String token = configurationDao.getConfig("settlement_slack_api_key");
                String channel = configurationDao.getConfig("settlement_slack_api_channel");

                notificationManager.uploadToSlack(tempFile, "Settlement Report for " + Utility.formatDate(date, "dd-MM-yyyy"), token, channel);

            } catch (IOException err) {
                err.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                }
            }
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(UtilityService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Asynchronous
    public void processFailedNotificationTransaction(Date startDate, Date endDate) {

        List<PosTransactionViewModelWeb> transactions = posTransactionDao.getFailedCallbacks(startDate, endDate);

        if (transactions == null) {
            return;
        }

        transactions.forEach(transaction -> {

            callBackUserReschedule(transaction);
        });
    }

//    public void fetchTransactionFromGA
}

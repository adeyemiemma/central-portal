/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.controller.api.ReportApiController;
import com.flutterwave.flutter.clientms.dao.AirtimeSettlementBatchDao;
import com.flutterwave.flutter.clientms.dao.AirtimeSettlementDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.ConfigurationFeeDao;
import com.flutterwave.flutter.clientms.dao.CoreMerchantDao;
import com.flutterwave.flutter.clientms.dao.CountryDao;
import com.flutterwave.flutter.clientms.dao.CurrencyDao;
import com.flutterwave.flutter.clientms.dao.ExchangeRateDao;
import com.flutterwave.flutter.clientms.dao.MerchantFeeDao;
import com.flutterwave.flutter.clientms.dao.ProductDao;
import com.flutterwave.flutter.clientms.dao.ProviderDao;
import com.flutterwave.flutter.clientms.dao.TransactionDao;
import com.flutterwave.flutter.clientms.dao.UserDao;
import com.flutterwave.flutter.clientms.model.AirtimeSettlement;
import com.flutterwave.flutter.clientms.model.AirtimeSettlementBatch;
import com.flutterwave.flutter.clientms.model.ConfigurationFee;
import com.flutterwave.flutter.clientms.model.Country;
import com.flutterwave.flutter.clientms.model.Currency;
import com.flutterwave.flutter.clientms.model.ExchangeRate;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.MerchantFee;
import com.flutterwave.flutter.clientms.model.Product;
import com.flutterwave.flutter.clientms.model.Provider;
import com.flutterwave.flutter.clientms.model.Transaction;
import com.flutterwave.flutter.clientms.model.User;
import com.flutterwave.flutter.clientms.model.products.CoreMerchant;
import com.flutterwave.flutter.clientms.util.CryptoUtil;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.GenericTransactionExportModel;
import com.flutterwave.flutter.clientms.viewmodel.SettlementViewModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import com.flutterwave.flutter.core.util.PageResult;
import com.monitorjbl.xlsx.StreamingReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class SettlementService {

    @EJB
    private HttpUtil httpUtil;
    @EJB
    private UserDao userDao;
    @Resource
    ManagedExecutorService managedExecutorService;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private AirtimeSettlementDao airtimeSettlementDao;
    @EJB
    private LogService logService;
    @EJB
    private AirtimeSettlementBatchDao airtimeSettlementBatchDao;
    @EJB
    private NotificationManager notificationManager;
    @EJB
    private CurrencyDao currencyDao;
    @EJB
    private ExchangeRateDao exchangeRateDao;
    @EJB
    private ConfigurationFeeDao configurationFeeDao;
    @EJB
    private ProviderDao providerDao;
    @EJB
    private ProductDao productDao;
    @EJB
    private CoreMerchantDao coreMerchantDao;
    @EJB
    private TransactionDao transactionDao;
    @EJB
    private CountryDao countryDao;
    @EJB
    private MerchantFeeDao merchantFeeDao;
    @EJB
    private AWSQueueService aWSQueueService;
    
    @Inject
    private SimpleCache simpleCache;

    private final String username = Global.EMAIL_USERNAME;
    private final String password = Global.EMAIL_PASSWORD;

    List<String> duplicateList = new ArrayList<>();

    Logger logger = Logger.getLogger(SettlementService.class);

    private String clickatellAccount, pwcAccount, flwAccount, suspenseAccount;

    private String baseUrl;

    private static long processedCounter = 0;

    private static long batchSize = 0;

    double totalSum = 0.0, duplicateSum = 0.0;

//    @Asynchronous
//    public void processTransactionsCSV(InputStream inputStream, String fileName, String username) {
//
//        try {
//            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
//
//            String line = null;
//
//            List<AirtimeSettlement> airtimeList = new ArrayList<>();
//
//            List<String> duplicateList = new ArrayList<>();
//
//            User user = userDao.findByKey("username", username);
//
//            int counter = 0;
//
//            double totalSum = 0;
//
//            String privateKey = configurationDao.getConfig("core_private_key");
//            String merchantId = configurationDao.getConfig("core_merchant_id");
//            clickatellAccount = configurationDao.getConfig("click_atell_account");
//            pwcAccount = configurationDao.getConfig("pwc_account");
////            flwAccount = configurationDao.getConfig("flw_account");
//            suspenseAccount = configurationDao.getConfig("suspense_account");
//            baseUrl = configurationDao.getConfig("core_settelement_url");
//
//            int index = 0;
//
//            while ((line = reader.readLine()) != null) {
//
//                if (index++ == 0) {
//                    continue;
//                }
//
//                String[] value = line.split(",");
//
//                String amount = value[3];
//                String reference = value[7];
//                String datetime = value[5];
//                String narration = value[8];
//
//                AirtimeSettlement airtimeSettlement;
//
//                airtimeSettlement = airtimeSettlementDao.findByKey("reference", reference);
//
//                if (airtimeSettlement != null) {
//                    duplicateList.add(reference);
//
//                    continue;
//                }
//
//                airtimeSettlement = new AirtimeSettlement();
//                airtimeSettlement.setAmount(Double.parseDouble(amount));
//                airtimeSettlement.setCreatedBy(user);
//                airtimeSettlement.setApprovedBy(user);
//                airtimeSettlement.setCreatedOn(new Date());
//                airtimeSettlement.setApprovedOn(new Date());
//                airtimeSettlement.setFileName(username);
//                airtimeSettlement.setReference(reference);
//                airtimeSettlement.setDatetime(datetime);
//                airtimeSettlement.setNarration(narration);
//
//                counter++;
//                airtimeList.add(airtimeSettlement);
//
//                totalSum += airtimeSettlement.getAmount();
//
////                if(counter == 100){
//                airtimeSettlementDao.create(airtimeSettlement);
//
//                
//                managedExecutorService.submit(new MySettlementTask(airtimeSettlement, merchantId, privateKey, fileName));
////                    counter = 0;
////                    airtimeList.clear();
////                }
//            }
//
//            Log log = new Log();
//            log.setAction("Airtime Duplicate data");
//            log.setCreatedOn(new Date());
//            log.setDescription(duplicateList.toString());
//            log.setLevel(LogLevel.Info);
//            log.setLogState(LogState.FINISH);
//            log.setUsername(user.getUsername());
//
//            logService.log(log);
//
//        } catch (IOException ex) {
//            Logger.getLogger(SettlementService.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(SettlementService.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (Exception ex) {
//            Logger.getLogger(SettlementService.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    @Asynchronous
    public void processTransactionsXlsx(InputStream inputStream, String fileName, String username) {

        try {

            processedCounter = 0;

//            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
//            String line = null;
            List<AirtimeSettlement> airtimeList = new ArrayList<>();

            duplicateList = new ArrayList<>();

            User user = userDao.findByKey("username", username);

            int counter = 0;

            double totalSum = 0.0, duplicateSum = 0.0;

            String privateKey = configurationDao.getConfig("core_private_key");
            String merchantId = configurationDao.getConfig("core_merchant_id");
            clickatellAccount = configurationDao.getConfig("click_atell_account");
            pwcAccount = configurationDao.getConfig("pwc_account");
//            flwAccount = configurationDao.getConfig("flw_account");
            suspenseAccount = configurationDao.getConfig("suspense_account");
            baseUrl = configurationDao.getConfig("core_settelement_url");

            Global.mailSend = false;

            int index = 0;

//            OPCPackage container;
            Workbook workbook = StreamingReader.builder()
                    .rowCacheSize(100)
                    .bufferSize(4096)
                    .open(inputStream);

            Map<String, Object> result = new LinkedHashMap<>();

//            // This assumes the sheet is first sheet
            Sheet sheet = workbook.getSheetAt(0);

            int rowCount = sheet.getLastRowNum() + 1;

            batchSize = rowCount;

            AirtimeSettlementBatch airtimeSettlementBatch = new AirtimeSettlementBatch();
            airtimeSettlementBatch.setCount(rowCount);
            airtimeSettlementBatch.setCreatedBy(user);
            airtimeSettlementBatch.setCreatedOn(new Date());
            airtimeSettlementBatch.setFileName(fileName);

            // This is called to create settlement batch card
            airtimeSettlementBatchDao.create(airtimeSettlementBatch);

            Iterator<Row> rowIterator = sheet.rowIterator();

            int empty = 0;

            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                // This implies that we will not pick the header 

                processedCounter += 1;

                if (index == 0) {
                    index++;
                    continue;
                }
//                int lastCellNumber = row.getLastCellNum();

                String amount = row.getCell(3, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getNumericCellValue() + "";
                String reference = row.getCell(7, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
                String datetime = row.getCell(5, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
                String narration = row.getCell(8, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();

                amount = amount.replace(",", "");

                totalSum += Double.parseDouble(amount);

                if (reference == null || "".equalsIgnoreCase(reference)) {
                    empty++;

                    if (empty >= 10) {

                        sendStatus(fileName, true);
                        break;
                    }
                    sendStatus(fileName);
                    continue;
                }

                empty = 0;

                boolean status = airtimeSettlementDao.exists(reference);

                if (status == true) {
                    duplicateList.add(reference);

                    duplicateSum += Double.parseDouble(amount);

                    sendStatus(fileName);

                    continue;
                }

                AirtimeSettlement airtimeSettlement = new AirtimeSettlement();
                airtimeSettlement.setAmount(Double.parseDouble(amount));
                airtimeSettlement.setCreatedBy(user);
                airtimeSettlement.setApprovedBy(user);
                airtimeSettlement.setCreatedOn(new Date());
                airtimeSettlement.setApprovedOn(new Date());
                airtimeSettlement.setFileName(fileName);
                airtimeSettlement.setReference(reference);
                airtimeSettlement.setDatetime(datetime);
                airtimeSettlement.setNarration(narration);

//                counter++;
                airtimeList.add(airtimeSettlement);

                airtimeSettlement.getAmount();

//                if(counter == 100){
                aWSQueueService.queueAirtimeTransaction(airtimeSettlement);

                airtimeSettlement.setSettled(true);

                airtimeSettlementDao.create(airtimeSettlement);

//                managedExecutorService.submit(new MySettlementTask(airtimeSettlement, merchantId, privateKey, fileName));
            }

            Log log = new Log();
            log.setAction("Airtime Duplicate data");
            log.setCreatedOn(new Date());
            log.setDescription(duplicateList.toString());
            log.setLevel(LogLevel.Info);
            log.setLogState(LogState.FINISH);
            log.setUsername(user.getUsername());
            log.setLogDomain(LogDomain.ADMIN);
            log.setLogState(LogState.FINISH);
            log.setStatus(LogStatus.SUCCESSFUL);

            sendStatus(fileName, true);
//            log.set

//            logService.log(log);
        } catch (DatabaseException ex) {
            logger.error(ex);
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    class MySettlementTask implements Runnable {

        private final AirtimeSettlement airtimeSettlement;
        private final String merchantId;
        private final String privateKey;
        private final String fileName;

        public MySettlementTask(AirtimeSettlement settlement, String merchantId,
                String privateKey, String fileName) {
            airtimeSettlement = settlement;
            this.merchantId = merchantId;
            this.privateKey = privateKey;
            this.fileName = fileName;
        }

        @Override
        public void run() {

            try {

                createAndSchedule(airtimeSettlement, merchantId, privateKey, fileName);

//                counter += 1;
                String narration = airtimeSettlement.getNarration();

                double amount = airtimeSettlement.getAmount();

                double clickatelShare = Utility.convertTo2DP(amount * 96 / 100);

                double pwcShare = amount - clickatelShare;

//                double flwShare = Utility.convertTo2DP(pwcShare * 50 / 100);
                int index = airtimeSettlement.getNarration().indexOf("|");

                String ref = airtimeSettlement.getNarration().substring(0, index);

                String tRef = "" + ref + "_ACCT_1";

                String temp = clickatelShare + "" + clickatellAccount + "" + suspenseAccount + "" + tRef;

                //Logger.getLogger(SettlementService.class.getName()).info("Token component"+temp+" private key: "+privateKey);
                String token = CryptoUtil.generateHash(temp, privateKey);

                //Logger.getLogger(SettlementService.class.getName()).info("Token component"+token);
                String clickNarration = narration + ": ACCT";

                String body = "narration=" + clickNarration;
                body += "&merchantid=" + merchantId;
                body += "&transferamount=" + clickatelShare;
                body += "&transactionReference=" + tRef;
                body += "&creditAccountNumber=" + clickatellAccount;
                body += "&debitAccountNumber=" + suspenseAccount;
                body += "&token=" + token;

                Map<String, String> header = new HashMap<>();
                header.put("Content-Type", "application/x-www-form-urlencoded");

                //Logger.getLogger(SettlementService.class.getName()).info(body);
                String response = httpUtil.doHttpPost(baseUrl, body, header);

                JsonObject responseObject = Json.createReader(new ByteArrayInputStream(response.getBytes())).readObject();

                String responseCode = responseObject.getString("responseCode", null);
                String responseMessage = responseObject.getString("responseMessage", null);

                airtimeSettlement.setTransactionReference(tRef);

                if (!"00".equalsIgnoreCase(responseCode)) {
                    airtimeSettlement.setResponseCode(responseCode);
                    airtimeSettlement.setResponseMessage(responseMessage);

                    airtimeSettlementDao.update(airtimeSettlement);

                    sendStatus(fileName);

                    return;
                }

                airtimeSettlement.setResponseCode(responseCode);
                airtimeSettlement.setResponseMessage(responseMessage);

                tRef = "" + ref + "_FEE_1";

                temp = pwcShare + "" + pwcAccount + "" + suspenseAccount + "" + tRef;

                //Logger.getLogger(SettlementService.class.getName()).info("Token component"+temp+" private key: "+privateKey);
                token = CryptoUtil.generateHash(temp, privateKey);

                //Logger.getLogger(SettlementService.class.getName()).info("Token component"+token);
                clickNarration = narration + ": FEE";

                body = "narration=" + clickNarration;
                body += "&merchantid=" + merchantId;
                body += "&transferamount=" + pwcShare;
                body += "&transactionReference=" + tRef;
                body += "&creditAccountNumber=" + pwcAccount;
                body += "&debitAccountNumber=" + suspenseAccount;
                body += "&token=" + token;

                response = httpUtil.doHttpPost(baseUrl, body, header);

                responseObject = Json.createReader(new ByteArrayInputStream(response.getBytes())).readObject();

                responseCode = responseObject.getString("responseCode", null);
                responseMessage = responseObject.getString("responseMessage", null);

                airtimeSettlement.setFeeReference(tRef);

                if (!"00".equalsIgnoreCase(responseCode)) {
                    airtimeSettlement.setFeeResponseCode(responseCode);
                    airtimeSettlement.setFeeResponseMessage(responseMessage);

                    airtimeSettlementDao.update(airtimeSettlement);

                    sendStatus(fileName);

                    return;
                }

//                tRef = ""+ref+"_FEE_1";
//
//                temp = flwShare + "" + flwAccount + "" + pwcAccount + "" + tRef;
//
//                token = CryptoUtil.generateHash(temp, privateKey);
//
//                body = "narration=" + clickNarration;
//                body += "&merchantid=" + merchantId;
//                body += "&transferamount=" + flwShare;
//                body += "&transactionReference=" + tRef;
//                body += "&creditAccountNumber=" + flwAccount;
//                body += "&debitAccountNumber=" + pwcAccount;
//                body += "&token=" + token;
//
//                response = httpUtil.doHttpPost(baseUrl, body, header);
//
//                responseObject = Json.createReader(new ByteArrayInputStream(response.getBytes())).readObject();
//
//                responseCode = responseObject.getString("responseCode", null);
//                responseMessage = responseObject.getString("responseMessage", null);
//
//                airtimeSettlement.setTransactionReference(tRef);
//                
//                if (!"00".equalsIgnoreCase(responseCode)) {
//                    airtimeSettlement.setResponseCode(responseCode);
//                    airtimeSettlement.setResponseMessage(responseMessage);
//
//                    airtimeSettlementDao.update(airtimeSettlement);
//                }
                airtimeSettlement.setSettled(true);
                airtimeSettlement.setFeeResponseCode(responseCode);
                airtimeSettlement.setFeeResponseMessage(responseMessage);

                airtimeSettlementDao.update(airtimeSettlement);

                sendStatus(fileName);

            } catch (DatabaseException ex) {
                logger.error(ex);
            } catch (Exception ex) {
                logger.error(ex);
            }
        }

    }

    public synchronized void sendStatus(String fileName) {

        if (((batchSize - processedCounter) <= 4 && (batchSize - processedCounter) > 0) && Global.mailSend == false) {

            String username = Global.EMAIL_USERNAME;
            String password = Global.EMAIL_PASSWORD;
            SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER,
                    Global.EMAIL_PORT, true, Global.USE_STARTTLS, false);

            String emails = configurationDao.getConfig("airtime_settlement_email");

            if (emails == null) {
                emails = "emmanuel@flutterwavego.com";
            }

            Global.mailSend = true;
            notificationManager.notifyAirtimeAdmin(emails.split(","), mailSender, fileName);
        }
    }

    public synchronized void sendStatus(String fileName, boolean state) {

//        if ( ((batchSize - processedCounter) <= 4 && (batchSize - processedCounter) > 0 )  && Global.mailSend == false) {
        String username = Global.EMAIL_USERNAME;
        String password = Global.EMAIL_PASSWORD;
        SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER,
                Global.EMAIL_PORT, true, Global.USE_STARTTLS, false);

        String emails = configurationDao.getConfig("airtime_settlement_email");

        if (emails == null) {
            emails = "emmanuel@flutterwavego.com";
        }

        Global.mailSend = true;
        notificationManager.notifyAirtimeAdmin(emails.split(","), mailSender, fileName);
//            }
    }

    public synchronized void createAndSchedule(AirtimeSettlement airtimeSettlement, String merchantId,
            String privateKey, String fileName) {

        try {

            boolean status = airtimeSettlementDao.exists(airtimeSettlement.getReference());

            if (status == true) {
                duplicateList.add(airtimeSettlement.getReference());

                duplicateSum += airtimeSettlement.getAmount();

                sendStatus(fileName);

                return;
            }

            airtimeSettlementDao.create(airtimeSettlement);

            airtimeSettlementDao.exists(airtimeSettlement.getReference());

        } catch (DatabaseException ex) {
            logger.error(ex);
        }
    }

    @Asynchronous
    @AccessTimeout(value = 120000)
    public void processMerchTransactions(Date date) {

        try {

            Properties properties = Utility.getConfigProperty();
//            Map<String, String> emailReceipients = new HashMap<>();
//            emailReceipients.put("144209", "fogundapo@flydanaair.com,oatunbi@flydanaair.com,sbajaj@danagroup.com,yharuna@flydanaair.com,ofasina@flydanaair.com");
//            emailReceipients.put("144106", "vivian@kudi.ai,pelumi@kudi.ai,yinka@kudi.ai,teniola@kudi.ai");
//            emailReceipients.put("144127", "bi.pay@jumia.com,desmond.okocha@jumia.com,afolake.akinboro@jumia.com");
//            emailReceipients.put("144020", "kakinola@pagatech.com,takinrinola@pagatech.com,iezebuiro@pagatech.com,morubu@pagatech.com,jadebayo@pagatech.com");
//            emailReceipients.put("144108", "olanrewaju.morakinyo@9mobile.com.ng,tessy.adams@9mobile.com.ng,olubunmi.falaki@9mobile.com.ng,electronicchannel@9mobile.com.ng,rukayat.arowogbadamu@9mobile.com.ng");
//            emailReceipients.put("144157", "info@dusupay.com");
//            emailReceipients.put("144159", "finance@wallet.ng");

            String cc = "settlement@flutterwavego.com,operations@flutterwavego.com";

            Calendar calendar = Calendar.getInstance();

            if (date == null) {
                calendar.add(Calendar.DAY_OF_MONTH, -1);
            } else {
                calendar.setTime(date);
            }

            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            Date startDate = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));

            Date endDate = calendar.getTime();

            List<Product> list = productDao.findAll();

            if (list == null) {
                return;
            }

            String merchantString = configurationDao.getConfig("merchant_get_settlement_report");

            if (merchantString == null || "".equalsIgnoreCase(merchantString)) {
                return;
            }

            String[] merchants = merchantString.split(",");

            Product product = list.stream().filter(x -> "core".equalsIgnoreCase(x.getName()) || "flutterwave core".equalsIgnoreCase(x.getName())).findFirst().orElse(null);

            ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);

            String[] ccRecipients = (cc == null) ? null : cc.split(",");

            SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);

            for (String merchantS : merchants) {

                String[] merchantInfo = merchantS.split(":");

                String merchantId = merchantInfo[0].trim();

                String email = properties.getProperty(merchantId, "emmanuel@flutterwavego.com");

                String[] emails = email.split(",");

                String category = merchantInfo[1];

                List<CoreMerchant> coreMerchants = coreMerchantDao.find("merchantID", merchantId);

                CoreMerchant coreMerchant = null;

                if (coreMerchants != null) {
                    coreMerchant = coreMerchants.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
                }

                List<Transaction> transactions = transactionDao.getAllSettleableTransanctions(startDate, endDate, null, null, merchantId);

                MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(product, coreMerchant == null ? 0 : coreMerchant.getId());

                String merchantName = "";

                boolean airline = false;

                if ("airline".equalsIgnoreCase(category)) {
                    airline = true;
                }

                List<GenericTransactionExportModel> exportModels = new ArrayList<>();
                for (Transaction t : transactions) {

                    try {
                        GenericTransactionExportModel exportModel = new GenericTransactionExportModel();

                        Provider provider = providerDao.findByKey("shortName", t.getProvider());

                        Currency providerCurrency = provider.getCurrency();

                        exportModel.setCurrency(t.getTransactionCurrency());
                        exportModel.setTransactionAmount(t.getAmount());
                        exportModel.setMerchantId(t.getMerchantId());

                        String tempReference = t.getFlwTxnReference();

                        int index = tempReference.indexOf("FLW");
                        String subStr = tempReference.substring(index > -1 ? index : 0);

                        exportModel.setReference(subStr);
                        exportModel.setTransactionDate(t.getDatetime());
                        exportModel.setMerchantName(coreMerchant != null ? coreMerchant.getCompanyname() : null);

                        if (!"144127".equalsIgnoreCase(merchantId)) {
                            exportModel.setMerchantTransactionReference(t.getMerchantTxnReference());
                        } else {
                            exportModel.setMerchantTransactionReference(t.getTransactionNarration());
                        }

                        merchantName = exportModel.getMerchantName() == null ? "" : exportModel.getMerchantName().replaceAll(" ", "_");
                        String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                        Country currencyCountry = null;

                        if (providerCurrency != null) {
                            currencyCountry = countryDao.findByCurrency(providerCurrency);
                        }

                        String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                        String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                        String cardCountry = t.getCardCountry() == null ? "NG" : t.getCardCountry();

                        boolean local = t.getTransactionCurrency().equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));

                        double configFee = getFee(configurationFee, t.getAmount(), local, t.getTransactionCurrency(), 1);

                        double merchantFe = getFee(merchantFee, t.getAmount(), local, t.getTransactionCurrency(), 1);

//                value += t.getAmount();
                        double fee = merchantFe == 0 ? configFee : merchantFe;

                        exportModel.setTransactionFee(fee);

                        if (airline == true) {

                            String narration = t.getTransactionNarration();

                            if (narration != null && !"".equals(narration)) {

                                index = narration.toUpperCase().indexOf("PNR");

                                if (index < 0) {

                                    narration = narration.trim();

                                    if (narration.length() > 6) {
                                        exportModel.setPnr(narration.substring(0, 6));
                                    } else {
                                        exportModel.setPnr(narration);
                                    }

                                } else {
                                    String pnr = narration.substring(index + 3).trim();

                                    if (pnr.length() > 6) {

                                        pnr = pnr.substring(0, 6);
                                    }

                                    exportModel.setPnr(pnr.trim());
                                }

                            }

                        }

                        exportModels.add(exportModel);

                    } catch (Exception ex) {
                        java.util.logging.Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                if (airline == false) {
                    if (!"144127".equalsIgnoreCase(merchantId)) {
                        notificationManager.buildExcelAndSend(exportModels, endDate, merchantName, merchantId,
                                emails, ccRecipients, false, mailSender, "Transaction Settlement Report", "Pnr");
                    } else {
                        notificationManager.buildExcelAndSendXlsx(exportModels, endDate, merchantName, merchantId,
                                emails, ccRecipients, false, mailSender, "Transaction Settlement Report", "Pnr");
                    }
                } else {

                    notificationManager.buildExcelAndSend(exportModels, endDate, merchantName, merchantId,
                            emails, ccRecipients, false, mailSender, "Transaction Settlement Report");
                }
            }
        } catch (DatabaseException ex) {
            java.util.logging.Logger.getLogger(SchedulerService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Asynchronous
    @AccessTimeout(value = 120000)
    public void processMerchTransactions(Date date, String merchantId, String emailString, String category) {

        try {

            if (emailString == null || merchantId == null) {
                return;
            }

            String cc = null;

//            SimpleDateFormat dateFormat =
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
//            calendar.set(Calendar.DAY_OF_MONTH, 5);
//            calendar.set(Calendar.MONTH, Calendar.APRIL);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            Date startDate = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));

            Date endDate = calendar.getTime();

            List<Product> list = productDao.findAll();

            if (list == null) {
                return;
            }

            Product product = list.stream().filter(x -> "core".equalsIgnoreCase(x.getName()) || "flutterwave core".equalsIgnoreCase(x.getName())).findFirst().orElse(null);

            ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);

            String[] ccRecipients = (cc == null) ? null : cc.split(",");

            SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT,
                    true, Global.USE_STARTTLS, false);

            String[] emails = emailString.split(",");

            List<CoreMerchant> coreMerchants = coreMerchantDao.find("merchantID", merchantId);

            CoreMerchant coreMerchant = null;

            if (coreMerchants != null) {
                coreMerchant = coreMerchants.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
            }

            List<Transaction> transactions = transactionDao.getAllSettleableTransanctions(startDate, endDate, null, null, merchantId);

            if (transactions == null || transactions.isEmpty()) {
                return;
            }

            MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(product, coreMerchant == null ? 0 : coreMerchant.getId());

            String merchantName = "";

            boolean airline = false;

            if (category != null && "airline".equalsIgnoreCase(category)) {
                airline = true;
            }

            List<GenericTransactionExportModel> exportModels = new ArrayList<>();
            for (Transaction t : transactions) {

                try {
                    GenericTransactionExportModel exportModel = new GenericTransactionExportModel();

                    Provider provider = providerDao.findByKey("shortName", t.getProvider());

                    Currency providerCurrency;

                    if (provider == null) {

                        providerCurrency = currencyDao.findByKey("shortName", "NGN");

                    } else {
                        providerCurrency = provider.getCurrency();
                    }

                    exportModel.setCurrency(t.getTransactionCurrency());
                    exportModel.setTransactionAmount(t.getAmount());
                    exportModel.setMerchantId(t.getMerchantId());

                    String tempReference = t.getFlwTxnReference();

                    int index = tempReference.indexOf("FLW");
                    String subStr = tempReference.substring(index > -1 ? index : 0);

                    exportModel.setNarration(t.getTransactionNarration());
                    exportModel.setReference(subStr);
                    exportModel.setTransactionDate(t.getDatetime());
                    exportModel.setMerchantName(coreMerchant != null ? coreMerchant.getCompanyname() : null);

                    merchantName = exportModel.getMerchantName() == null ? "" : exportModel.getMerchantName().replaceAll(" ", "_");
                    String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                    Country currencyCountry = null;

                    if (providerCurrency != null) {
                        currencyCountry = countryDao.findByCurrency(providerCurrency);
                    }

                    String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                    String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                    String cardCountry = t.getCardCountry() == null ? "NG" : t.getCardCountry();

                    boolean local = t.getTransactionCurrency().equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));

                    double configFee = getFee(configurationFee, t.getAmount(), local, t.getTransactionCurrency(), 1);

                    double merchantFe = getFee(merchantFee, t.getAmount(), local, t.getTransactionCurrency(), 1);

//                value += t.getAmount();
                    double fee = merchantFe == 0 ? configFee : merchantFe;

                    exportModel.setTransactionFee(fee);
                    
                    if(!"144127".equalsIgnoreCase(merchantId))
                        exportModel.setMerchantTransactionReference(t.getMerchantTxnReference());
                    else
                        exportModel.setMerchantTransactionReference(t.getTransactionNarration());
                            
                    if (airline == true) {

                        String narration = t.getTransactionNarration();

                        if (narration != null && !"".equals(narration)) {

                            index = narration.toUpperCase().indexOf("PNR");

                            if (index < 0) {

                                narration = narration.trim();

                                if (narration.length() > 6) {
                                    exportModel.setPnr(narration.substring(0, 6));
                                } else {
                                    exportModel.setPnr(narration);
                                }

                            } else {
                                String pnr = narration.substring(index + 3).trim();

                                if (pnr.length() > 6) {

                                    pnr = pnr.substring(0, 6);
                                }

                                exportModel.setPnr(pnr.trim());
                            }

                        }

                    }

                    exportModels.add(exportModel);

                } catch (Exception ex) {
                    logger.error(ex);
                }
            }

            System.out.println(exportModels);

            if (airline == false) {
                notificationManager.buildExcelAndSend(exportModels, endDate, merchantName, merchantId,
                        emails, ccRecipients, true, mailSender, "Transaction Settlement Report", "Pnr");
            } else {
                notificationManager.buildExcelAndSend(exportModels, endDate, merchantName, merchantId,
                        emails, ccRecipients, false, mailSender, "Transaction Settlement Report");
            }
//            }
        } catch (DatabaseException ex) {
            logger.error(ex);
        }
    }

    private double getFee(ConfigurationFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    private double getFee(MerchantFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    @Asynchronous
    public Future<String> processSettlementTransaction(String provider, String merchant,
            String currency, String category, Date startDate, Date endDate, String grouping) {

        try {
            List<Product> list = productDao.findAll();

            if (list == null) {
                return null;
            }

            List<SettlementViewModel> transactions = new ArrayList<>();

            Product product = list.stream().filter(x -> "core".equalsIgnoreCase(x.getName()) || "flutterwave core".equalsIgnoreCase(x.getName())).findFirst().orElse(null);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            if ("any".equalsIgnoreCase(provider)) {

                List<Provider> providers = providerDao.findAll();

                for (Provider p : providers) {

                    if (merchant != null && !"".equalsIgnoreCase(merchant)) {

                        CoreMerchant coreMerchant = coreMerchantDao.find(Long.parseLong(merchant));

                        if (coreMerchant != null) {
                            merchant = coreMerchant.getMerchantID();
                        }

                    } else {
                        merchant = null;
                    }

                    List<Transaction> transaction;

                    if ("ACCOUNT".equalsIgnoreCase(category)) {
                        transaction = transactionDao.getUnsettledTransanctionAccount(startDate, endDate,
                                p, currency, merchant);
                    } else if ("EBILLS".equalsIgnoreCase(category)) {
                        transaction = transactionDao.getUnsettledTransanctionAccount(startDate, endDate,
                                p, currency, merchant);
                    } else {
                        transaction = transactionDao.getUnsettledTransanctions(startDate, endDate,
                                p, currency, merchant);
                    }

                    Map<String, Map<String, List<Transaction>>> dataMapping = transaction.stream().collect(Collectors
                            .groupingBy(Transaction::getMerchantId, Collectors.groupingBy(Transaction::getCardCountry)));

                    ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);

                    for (Map.Entry<String, Map<String, List<Transaction>>> entry : dataMapping.entrySet()) {

                        int volume = 0;

                        String mid = entry.getKey();
                        Map<String, List<Transaction>> values = entry.getValue();

                        List<CoreMerchant> coreMer = coreMerchantDao.find("merchantID", mid);

                        CoreMerchant coreMerchant = null;

                        if (coreMer != null) {
                            coreMerchant = coreMer.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
                        }

                        double fee = 0.0, cost = 0.0, value = 0.0;

                        MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(product, coreMerchant == null ? 0 : coreMerchant.getId());

                        Currency providerCurrency = p.getCurrency();

                        String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                        Country currencyCountry = null;

                        if (providerCurrency != null) {
                            currencyCountry = countryDao.findByCurrency(providerCurrency);
                        }

                        String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                        String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                        for (Map.Entry<String, List<Transaction>> val : values.entrySet()) {

                            String cardCountry = val.getKey();

                            Map<String, List<Transaction>> currencyValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency));

//                            Map<String, Long> countValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency, Collectors.counting()));
//                            boolean local = false;
                            for (String currencyS : currencyValues.keySet()) {

//                                long count = countValues.getOrDefault(currencyS, 1L);
                                boolean local = currencyS.equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));

                                List<Transaction> txns = currencyValues.get(currencyS);

                                for (Transaction t : txns) {

                                    cost += getCost(p, t.getAmount(), local, 1);

                                    double configFee = 0.0;
                                    double merchantFe = 0.0;

                                    if ("ACCOUNT".equalsIgnoreCase(category) || "EBILLS".equalsIgnoreCase(category)) {
                                        configFee = getAccountFee(configurationFee, t.getAmount(), local, currencyS);
                                        merchantFe = getAccountFee(merchantFee, t.getAmount(), local, currencyS);
                                    } else {
                                        configFee = getFeeNew(configurationFee, t.getAmount(), local, currencyS, 1);
                                        merchantFe = getFeeNew(merchantFee, t.getAmount(), local, currencyS, 1);
                                    }

                                    value += t.getAmount();

                                    volume += 1;

                                    fee += merchantFe == 0 ? configFee : merchantFe;
                                }

                            }
//                        value = val.getValue().stream().mapToDouble(x -> x.getAmount()).sum();

//fee = getFee(configurationFee, value, cardCountry.equalsIgnoreCase(merchantCountry));  
                        }

                        SettlementViewModel settlementViewModel = new SettlementViewModel();
                        settlementViewModel.setAmount(value);
                        settlementViewModel.setAmountToSettled(value - fee);
                        settlementViewModel.setMerchantId(mid);
                        settlementViewModel.setFee(fee);
                        settlementViewModel.setMerchantName(coreMerchant != null ? coreMerchant.getCompanyname() : "");
                        settlementViewModel.setId(p.getId());
                        settlementViewModel.setCost(cost);
                        settlementViewModel.setProvider(p.getName());
                        settlementViewModel.setVolume(volume);

//                        Map<String, Object> data = new HashMap<>();
//                        data.put("name", p.getName());
//                        data.put("merchantId", mid);
//                        data.put("merchantName", coreMerchant != null ? coreMerchant.getCompanyname() : "");
//                        data.put("amount", totalValue.doubleValue());
//                        data.put("amountToSettled", (totalValue.doubleValue() - totalFee.doubleValue()));
//                        data.put("id", p.getId());
//                        data.put("fee", totalFee.doubleValue());
//                        data.put("cost", totalCost.doubleValue());
                        transactions.add(settlementViewModel);
                    }

                    if ("merchant".equalsIgnoreCase(grouping)) {

                        Map<String, List<SettlementViewModel>> groupedTransactions = transactions.stream().collect(Collectors.groupingBy(SettlementViewModel::getMerchantId));

                        transactions = new ArrayList<>();

                        for (Map.Entry<String, List<SettlementViewModel>> m : groupedTransactions.entrySet()) {

                            String merchantName = "";

                            double dCost = 0, dFee = 0, dValue = 0, dSettlementAmount = 0;

                            int dVolume = 0;

                            for (SettlementViewModel model : m.getValue()) {

                                merchantName = model.getMerchantName();

                                dCost += model.getCost();
                                dValue += model.getAmount();
                                dSettlementAmount += model.getAmountToSettled();
                                dFee += model.getFee();
                                dVolume += model.getVolume();
                            }

                            SettlementViewModel viewModel = new SettlementViewModel();
                            viewModel.setAmount(dValue);
                            viewModel.setAmountToSettled(dSettlementAmount);
                            viewModel.setId(0);
                            viewModel.setProvider("All");
                            viewModel.setMerchantName(merchantName);
                            viewModel.setFee(dFee);
                            viewModel.setCost(dCost);
                            viewModel.setMerchantId(m.getKey());
                            viewModel.setVolume(dVolume);

                            transactions.add(viewModel);
                        }

                    }

                    System.out.println(transactions);
                }
            } else {

                Provider p = providerDao.findByKey("name", provider);

                if (merchant != null && !"".equalsIgnoreCase(merchant)) {

                    CoreMerchant coreMerchant = coreMerchantDao.find(Long.parseLong(merchant));

                    if (coreMerchant != null) {
                        merchant = coreMerchant.getMerchantID();
                    }

                } else {
                    merchant = null;
                }

                List<Transaction> transaction;

                if ("ACCOUNT".equalsIgnoreCase(category)) {
                    transaction = transactionDao.getUnsettledTransanctionAccount(startDate, endDate,
                            p, currency, merchant);
                } else {
                    transaction = transactionDao.getUnsettledTransanctions(startDate, endDate,
                            p, currency, merchant);
                }
//                List<Transaction> transaction = transactionDao.getUnsettledTransanctions(startDate, endDate,
//                        p, currency, merchant);

//Map<String, Double> dataMerchant = transaction.stream().collect(Collectors.groupingBy(Transaction::getMerchantId, Collectors.summingDouble(Transaction::getAmount)));
// This is called ot group by merchant id and then by country
                Map<String, Map<String, List<Transaction>>> dataMapping = transaction.stream().collect(Collectors
                        .groupingBy(Transaction::getMerchantId, Collectors.groupingBy(Transaction::getCardCountry)));

                ConfigurationFee configurationFee = configurationFeeDao.findByProduct(product);

                for (Map.Entry<String, Map<String, List<Transaction>>> entry : dataMapping.entrySet()) {

                    String mid = entry.getKey();
                    Map<String, List<Transaction>> values = entry.getValue();

                    List<CoreMerchant> coreMer = coreMerchantDao.find("merchantID", mid);

                    CoreMerchant coreMerchant = null;

                    if (coreMer != null) {
                        coreMerchant = coreMer.stream().filter(x -> x.getOwnerID() == null).findFirst().orElse(null);
                    }

//                    JsonObject jsonObject = httpUtil.getMerchantInformation(mid);
                    double fee = 0.0, cost = 0.0, value = 0.0;

                    int volume = 0;
//                    totalFee = new BigDecimal(0.0);
//                    totalCost = totalValue = totalFee;

                    MerchantFee merchantFee = merchantFeeDao.findByProductAndMerchant(product, coreMerchant == null ? 0 : coreMerchant.getId());

                    Currency providerCurrency = p.getCurrency();

                    String currencyCode = providerCurrency == null ? null : providerCurrency.getShortName();

                    Country currencyCountry = null;

                    if (providerCurrency != null) {
                        currencyCountry = countryDao.findByCurrency(providerCurrency);
                    }

                    String countryCode = currencyCountry != null ? currencyCountry.getShortName() : null;

                    String countryName = currencyCountry != null ? currencyCountry.getName() : null;

                    for (Map.Entry<String, List<Transaction>> val : values.entrySet()) {

                        String cardCountry = val.getKey();

                        Map<String, List<Transaction>> currencyValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency));

//                            Map<String, Long> countValues = val.getValue().stream().collect(Collectors.groupingBy(Transaction::getTransactionCurrency, Collectors.counting()));
//                            boolean local = false;
                        for (String currencyS : currencyValues.keySet()) {

//                                long count = countValues.getOrDefault(currencyS, 1L);
                            boolean local = currencyS.equalsIgnoreCase(currencyCode) && (countryCode != null && cardCountry != null) && (cardCountry.contains(countryCode) || cardCountry.contains(countryName));

                            List<Transaction> txns = currencyValues.get(currencyS);

                            for (Transaction t : txns) {

                                cost += getCost(p, t.getAmount(), local, 1);

                                double merchantFe, configFee;

                                if ("ACCOUNT".equalsIgnoreCase(category)) {
                                    configFee = getAccountFee(configurationFee, t.getAmount(), local, currencyS);
                                    merchantFe = getAccountFee(merchantFee, t.getAmount(), local, currencyS);
                                } else {
                                    configFee = getFee(configurationFee, t.getAmount(), local, currencyS, 1);
                                    merchantFe = getFee(merchantFee, t.getAmount(), local, currencyS, 1);
                                }

//                                    double configFee = getFee(configurationFee, t.getAmount(), local, currencyS, 1);
//                                    double merchantFe = getFee(merchantFee, t.getAmount(), local, currencyS, 1);
                                value += t.getAmount();

                                fee += merchantFe == 0 ? configFee : merchantFe;

                                volume += 1;
                            }

                        }
                    }

                    SettlementViewModel settlementViewModel = new SettlementViewModel();
                    settlementViewModel.setAmount(value);
                    settlementViewModel.setAmountToSettled(value - fee);
                    settlementViewModel.setMerchantId(mid);
                    settlementViewModel.setFee(fee);
                    settlementViewModel.setMerchantName(coreMerchant != null ? coreMerchant.getCompanyname() : "");
                    settlementViewModel.setId(p.getId());
                    settlementViewModel.setCost(cost);
                    settlementViewModel.setProvider(p.getName());
                    settlementViewModel.setVolume(volume);

//                    settlementViewModel.setRefund(value);
                    transactions.add(settlementViewModel);
                    
                    System.out.println(transactions);                    

                }

            }
            
            simpleCache.add("settlementData",transactions);

            return new AsyncResult<>("Done");
            
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SettlementService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return new AsyncResult<>("Failed");

    }

    private double getAccountFee(MerchantFee configurationFee, double value, boolean local, String currency) throws DatabaseException {

        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobAccountFeeType()) {
                fee = configurationFee.getPobAccountFee();
            } else {
                switch (configurationFee.getPobAccountFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobAccountFee() + (configurationFee.getPobAccountFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobAccountFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobAccountFee();
                        break;
                }
            }

            double configFee = configurationFee.getPobAccountFeeCap();

            if (fee > configFee && configurationFee.getPobAccountFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterAccountFeeType()) {
                fee = configurationFee.getInterAccountFee();
            } else {
                switch (configurationFee.getInterAccountFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterAccountFee() * rate) + (configurationFee.getInterAccountFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterAccountFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterAccountFee();
                        break;
                }
            }

            double configFee = configurationFee.getInterAccountFeeCap();

            if (fee > configFee && configurationFee.getInterAccountFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    private double getFeeNew(ConfigurationFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    private double getFeeNew(MerchantFee configurationFee, double value, boolean local, String currency, long count) throws DatabaseException {
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobCardFeeType()) {
                fee = configurationFee.getPobCardFee() * count;
            } else {
                switch (configurationFee.getPobCardFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobCardFee() * count + (configurationFee.getPobCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getPobCardFeeCap() * count;

            if (fee > configFee && configurationFee.getPobCardFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterCardFeeType()) {
                fee = configurationFee.getInterCardFee() * count;
            } else {
                switch (configurationFee.getInterCardFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterCardFee() * rate * count) + (configurationFee.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterCardFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = configurationFee.getInterCardFeeCap() * count;

            if (fee > configFee && configurationFee.getInterCardFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
    }

    private double getCost(Provider p, double value, boolean local, long count) {

        double cost = 0.0;
        if (local == true) {

            if (null == p.getLocalCardFeeType()) {
                cost = p.getLocalCardFee() * count;
            } else {
                switch (p.getLocalCardFeeType()) {
                    case BOTH:
                        cost = p.getLocalCardFee() * count + (p.getLocalCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        cost = (p.getLocalCardFee() * value) / 100;
                        break;
                    default:
                        cost = p.getLocalCardFee();
                        break;
                }
            }

            double configFee = p.getLocalCardFeeCap() * count;

            if (cost > configFee && p.getLocalCardFeeCap() != 0.0) {
                cost = configFee;
            }

        } else {

            if (null == p.getInterCardFeeType()) {
                cost = p.getInterCardFee() * count;
            } else {
                switch (p.getInterCardFeeType()) {
                    case BOTH:
                        cost = p.getInterCardFee() * count + (p.getInterCardFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        cost = (p.getInterCardFee() * value) / 100;
                        break;
                    default:
                        cost = p.getInterCardFee() * count;
                        break;
                }
            }

            double configFee = p.getInterCardFeeCap() * count;

            if (cost > configFee && p.getInterCardFeeCap() != 0.0) {
                cost = configFee;
            }
        }

        return cost;
    }

    private double getAccountFee(ConfigurationFee configurationFee, double value, boolean local, String currency) throws DatabaseException{
        
        double fee = 0.0;

        if (configurationFee == null) {
            return fee;
        }

        if (local == true) {

            if (null == configurationFee.getPobAccountFeeType()) {
                fee = configurationFee.getPobAccountFee() ;
            } else {
                switch (configurationFee.getPobAccountFeeType()) {
                    case BOTH:
                        fee = configurationFee.getPobAccountFee()+ (configurationFee.getPobAccountFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getPobAccountFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getPobAccountFee() ;
                        break;
                }
            }

            double configFee = configurationFee.getPobAccountFeeCap();

            if (fee > configFee && configurationFee.getPobAccountFeeCap() != 0.0) {
                fee = configFee;
            }

        } else {

            if (null == configurationFee.getInterAccountFeeType()) {
                fee = configurationFee.getInterAccountFee() ;
            } else {
                switch (configurationFee.getInterAccountFeeType()) {
                    case BOTH:
                        Currency curr = currencyDao.findByKey("shortName", currency);
                        Currency destcurr = currencyDao.findByKey("shortName", "USD");

                        ExchangeRate exchangeRate = exchangeRateDao.find(destcurr, curr);

                        double rate = 1;

                        if (exchangeRate != null) {
                            rate = exchangeRate.getAmount();
                        }

                        fee = (configurationFee.getInterAccountFee() * rate ) + (configurationFee.getInterAccountFeeExtra() * value / 100);
                        break;
                    case PERCENTAGE:
                        fee = (configurationFee.getInterAccountFee() * value) / 100;
                        break;
                    default:
                        fee = configurationFee.getInterAccountFee();
                        break;
                }
            }

            double configFee = configurationFee.getInterAccountFeeCap();

            if (fee > configFee && configurationFee.getInterAccountFeeCap() != 0.0) {
                fee = configFee;
            }
        }

        return fee;
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.model;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanueladeyemi
 */
@XmlRootElement(name = "PaymentNotificationResponse")
public class WakanowPaymentNotificationResponse {

    /**
     * @return the paymentNotification
     */
    @XmlElement(name = "Payments")
    public WakanowPaymentNotification getPaymentNotification() {
        return paymentNotification;
    }

    /**
     * @param paymentNotification the paymentNotification to set
     */
    public void setPaymentNotification(WakanowPaymentNotification paymentNotification) {
        this.paymentNotification = paymentNotification;
    }
    
   
    private WakanowPaymentNotification paymentNotification;
}

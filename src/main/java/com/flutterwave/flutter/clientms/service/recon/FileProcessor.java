/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.recon;

import com.flutterwave.flutter.clientms.dao.recon.FlutterTransactionDao;
import com.flutterwave.flutter.clientms.dao.recon.ReconTransactionDao;
import com.flutterwave.flutter.clientms.model.TransactionOther;
import com.flutterwave.flutter.clientms.model.TransactionUploadModel;
import com.flutterwave.flutter.clientms.model.recon.Batch;
import com.flutterwave.flutter.clientms.model.recon.ReconTransaction;
import com.flutterwave.flutter.clientms.model.recon.UploadAnalysisModel;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.viewmodel.ReconUploadModel;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author adeyemi
 */
@Stateless
public class FileProcessor {

    
    private Map<String, Double> originCurrencies = new HashMap<>();
    private Map<String, Double> destinationCurrencies = new HashMap<>();
    private Map<String, Integer> merchantTransaction = new HashMap<>();
    
//    public boolean processXls(UploadModel model, Batch batch) throws IOException, DatabaseException{
//        
//        InputStream fileStream = model.getFile().getInputStream();
//        
//        if(fileStream == null)
//            return false;
//        
//        HSSFWorkbook workbook = new HSSFWorkbook(fileStream);
//        
//        // This assumes the sheet is first sheet
//        HSSFSheet sheet = workbook.getSheetAt(0);
//        
//        // This gets the row iteratir that allows user to go over the sheet
//        Iterator<Row> rowIterator = sheet.iterator();
//        
//        
//        
//        List<Transaction> list = processFile(rowIterator, model.isSettled(), model.getBank(), 
//                model.getScheme(), model.getDateSettled(), batch);
//       
//        reconTransactionDao.create(list);
//        return false;
//    }
//    
    
    public UploadAnalysisModel processXls(ReconUploadModel model, Batch batch) throws IOException, DatabaseException{
//        
        InputStream fileStream = model.getFile().getInputStream();
        
        if(fileStream == null)
            return null;
        
        HSSFWorkbook workbook = new HSSFWorkbook(fileStream);
        
        // This assumes the sheet is first sheet
        HSSFSheet sheet = workbook.getSheetAt(0);
        
        // This gets the row iteratir that allows user to go over the sheet
        Iterator<Row> rowIterator = sheet.iterator();
        
        
        
        UploadAnalysisModel analysisModel = processFile(rowIterator, model.isSettled(), model.getBank(), 
                model.getScheme(), model.getDateSettled());
       
//        reconTransactionDao.create(list);
        return analysisModel;
    }
    
    private UploadAnalysisModel processFile(Iterator<Row> rowIterator, boolean settled, String bank, 
            String scheme, Date settlementDate){
        
        UploadAnalysisModel analysisModel = new UploadAnalysisModel();
        // This gets the row iteratir that allows user to go over the sheet
//        Iterator<Row> rowIterator = sheet.iterator();
        
        int index = 0;
        
        Map<Long, String> headers = new HashMap<>();
        
        Map<String, String> headerMapper = TransactionUtility.populateIndex();
        
        List<ReconTransaction> transactions = new ArrayList<>();
//        batch.
        
        while(rowIterator.hasNext()){
            
            Row row = rowIterator.next();
            // This implies that we will not pick the header 
            
            ReconTransaction transaction = new ReconTransaction();
            
            Iterator<Cell> cellIterator = row.cellIterator();
            
            long cellIndex = 0;
            
            int lastCellNumber = row.getLastCellNum();

            for(int i =0 ; i < lastCellNumber; i++){
                
                Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                
//                String tempVal = cell.getStringCellValue();
                
                switch(cell.getCellType()){
                    case NUMERIC : {
                        
                        String header = headerMapper.get(headers.get(cellIndex));
                        
                        if(DateUtil.isCellDateFormatted(cell)){
                            
                            Date date =  DateUtil.getJavaDate(cell.getNumericCellValue());
                            
                            analysisModel.getDates().add(date);
                            
                            if(header != null)
                                TransactionUtility.set(transaction, header, date);
                            
                        }else{
                            double value = cell.getNumericCellValue();

                            long val = new BigDecimal(value).longValue();

                            if(header != null)
                                TransactionUtility.set(transaction, header, val+"");
                        }
                        break;
                        
                    }
                    
                    case STRING : {
                        
                        String value = cell.getStringCellValue();
                        
                        if(index == 0){
                            headers.put(cellIndex, value);
                        }else{
                            String header = headerMapper.get(headers.get(cellIndex));
                        
                            if(header != null)
                                TransactionUtility.set(transaction, header, value);
                        }
                        
                        break;
                    }
                }
                
                cellIndex++;
            }
            
            if(index > 0){
                transaction.setBank(bank);
                transaction.setCardScheme(scheme);
//                transaction.setSettled(settled);
//                transaction.setSettlementDate(settlementDate);

                transactions.add(transaction);
                
                int transactionCount = merchantTransaction.getOrDefault(""+transaction.getMerchantTitle(), 0);
                transactionCount++;
                merchantTransaction.put(transaction.getMerchantTitle(), transactionCount);
                
                String currency = TransactionUtility.getCurrencyName(transaction.getOriginClearanceCurrency()+"");
                double value = originCurrencies.getOrDefault(currency, 0.0);
                value += transaction.getOriginClearanceAmount();
                originCurrencies.put(currency, value);
                
                currency = TransactionUtility.getCurrencyName(transaction.getDestinationClearanceCurrency()+"");
                value = destinationCurrencies.getOrDefault(currency, 0.0);
                value += transaction.getDestinationClearanceAmount();
                destinationCurrencies.put(currency, value);
                
                
            }
            index++;  
        }
        
        analysisModel.setDestinationCurrencies(destinationCurrencies);
        analysisModel.setTransactionCount(transactions.size());
        analysisModel.setOriginCurrencies(originCurrencies);
        analysisModel.setTransactions(transactions);
        analysisModel.setMerchantTransaction(merchantTransaction);
        analysisModel.setCardScheme(scheme);
        
        return analysisModel;
    }
    
    private List<TransactionOther> processFile(Iterator<Row> rowIterator){
        
        List<TransactionOther> transactions = new ArrayList<>();
        
        // This gets the row iteratir that allows user to go over the sheet
//        Iterator<Row> rowIterator = sheet.iterator();
        
        int index = 0;
        
        Map<Long, String> headers = new HashMap<>();
        
        Map<String, String> headerMapper = TransactionUtility.populateTransactionHeaders();
        
//        List<ReconTransaction> transactions = new ArrayList<>();
//        batch.
        
        while(rowIterator.hasNext()){
            
            Row row = rowIterator.next();
            // This implies that we will not pick the header 
            
            TransactionOther transaction = new TransactionOther();
            
            Iterator<Cell> cellIterator = row.cellIterator();
            
            long cellIndex = 0;
            
            if(row.getRowNum() < 3){
                continue;
            }
                
            int lastCellNumber = row.getLastCellNum();

            for(int i =0 ; i < lastCellNumber; i++){
                
            Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                
//                String tempVal = cell.getStringCellValue();
                
                switch(cell.getCellType()){
                    case NUMERIC : {
                        
                        String header = headerMapper.get(headers.get(cellIndex));
                        
                        if(DateUtil.isCellDateFormatted(cell)){
                            
                            Date date =  DateUtil.getJavaDate(cell.getNumericCellValue());
                            
                            
                            if(header != null)
                                TransactionUtility.set(transaction, header, date);
                            
                        }else{
                            double value = cell.getNumericCellValue();

                            long val = new BigDecimal(value).longValue();

                            if(header != null)
                                TransactionUtility.set(transaction, header, val+"");
                        }
                        break;
                        
                    }
                    
                    case STRING : {
                        
                        String value = cell.getStringCellValue();
                        
                        if(index == 0){
                            headers.put(cellIndex, value);
                        }else{
                            String header = headerMapper.get(headers.get(cellIndex));
                        
                            if(header != null)
                                TransactionUtility.set(transaction, header, value);
                        }
                        
                        break;
                    }
                }
                
                cellIndex++;
            }
            
            if(index > 0){
//                transaction.setSettled(settled);
//                transaction.setSettlementDate(settlementDate);

                transactions.add(transaction);
                
            }
            
            index++;  
        }
        
        
        return transactions;
    }
    
//    @Transactional(rollbackOn = {RuntimeException.class, IOException.class, DatabaseException.class})
    public UploadAnalysisModel process(ReconUploadModel model, Batch batch) throws IOException, DatabaseException{
        
        InputStream fileStream = model.getFile().getInputStream();
        
        if(fileStream == null)
            return null;
        
        XSSFWorkbook workbook = new XSSFWorkbook(fileStream);
        
        // This assumes the sheet is first sheet
        XSSFSheet sheet = workbook.getSheetAt(0);
        
        // This gets the row iteratir that allows user to go over the sheet
        Iterator<Row> rowIterator = sheet.iterator();
        
        UploadAnalysisModel transactions = processFile(rowIterator, model.isSettled(), 
                model.getBank(), model.getScheme(), model.getDateSettled());
        
//        reconTransactionDao.create(transactions);

//        fileStream.close();
        
        return transactions;
    }
    
    public List<TransactionOther> process(TransactionUploadModel model) throws IOException, DatabaseException{
        
        InputStream fileStream = model.getFile().getInputStream();
        
        if(fileStream == null)
            return null;
        
        XSSFWorkbook workbook = new XSSFWorkbook(fileStream);
        
        // This assumes the sheet is first sheet
        XSSFSheet sheet = workbook.getSheetAt(0);
        
        // This gets the row iteratir that allows user to go over the sheet
        Iterator<Row> rowIterator = sheet.iterator();
        
        List<TransactionOther> transactions = processFile(rowIterator);
        
//        reconTransactionDao.create(transactions);
        
        return transactions;
    }
    
    
    public List<TransactionOther> processXls(TransactionUploadModel model) throws IOException, DatabaseException{
//        
        InputStream fileStream = model.getFile().getInputStream();
        
        if(fileStream == null)
            return null;
        
        HSSFWorkbook workbook = new HSSFWorkbook(fileStream);
        
        // This assumes the sheet is first sheet
        HSSFSheet sheet = workbook.getSheetAt(0);
        
        // This gets the row iteratir that allows user to go over the sheet
        Iterator<Row> rowIterator = sheet.iterator();
        
        List<TransactionOther> analysisModel = processFile(rowIterator);
       
//        reconTransactionDao.create(list);
        return analysisModel;
    }
    
}

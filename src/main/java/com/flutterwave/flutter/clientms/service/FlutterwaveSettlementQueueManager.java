/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.model.FlutterwaveSettlement;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogStatus;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

/**
 *
 * @author emmanueladeyemi
 */
@Startup
@Singleton
public class FlutterwaveSettlementQueueManager {
    
    @EJB
    private LogService logService;
    
    ActiveMQConnectionFactory connectionFactory;
    Session session;
    Connection connection;
    MessageProducer producer;
    
    Logger logger = Logger.getLogger(FlutterwaveSettlementQueueManager.class);
    
    @PostConstruct
    public void start() {

//        Thread thread = new Thread(() -> {
//        startUpQueue();
//        });

//        thread.start();
    }
    
    public void startUpQueue() {

        try {
            Log log = new Log();
            log.setAction("START UP QUEUE CONNECTION");
            log.setCreatedOn(new Date());
            log.setDescription("Starting up queue connection");
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLevel(LogLevel.Info);
            logService.log(log);

            connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616?jms.useAsyncSend=true");
            
            connectionFactory.setTrustAllPackages(true);
            // Create a Connection
            connection = connectionFactory.createConnection();
            
            // Create a Session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            
            // Create the destination (Topic or Queue)
            Destination destination = session.createQueue("SETTLEMENT_QUEUE");
            
            // Create a MessageProducer from the Session to the Topic or Queue
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);


            log = new Log();
            log.setAction("START UP QUEUE CONNECTION");
            log.setCreatedOn(new Date());
            log.setDescription("queue connection started successfully");
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLevel(LogLevel.Info);
            logService.log(log);
            
            connection.start();

        } catch (JMSException ex) {
            logger.log(Priority.ERROR, session);

            try {
                Log log = new Log();
                log.setAction("START QUEUE");
                log.setCreatedOn(new Date());
                log.setDescription("START QUEUE FAILED");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);

                if (ex != null) {
//                    log.setStatusMessage(ex.getMessage());

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }
    }
    
    
    
    public void queueSettlement(FlutterwaveSettlement request){
        
        try {

            Log log = new Log();
            log.setAction("SCHEDULING REQUEST");
            log.setCreatedOn(new Date());
            log.setDescription("scheduling request started");
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLevel(LogLevel.Info);
//            logService.log(log);

            if (session == null) {
                startUpQueue();
            }

            Message message = session.createObjectMessage(request);
            message.setLongProperty("AMQ_SCHEDULED_DELAY", 60 * 1000);

            // This is called to send the message to the queue
            producer.send(message);

            log = new Log();
            log.setAction("SCHEDULING REQUEST");
            log.setCreatedOn(new Date());
            log.setDescription("scheduling request ended");
            log.setStatus(LogStatus.SUCCESSFUL);
            log.setLevel(LogLevel.Info);
            logService.log(log);

        } catch (Exception ex) {
            logger.log(Priority.ERROR, session);

            try {
                Log log = new Log();
                log.setAction("SCHEDULING MESSAGE");
                log.setCreatedOn(new Date());
                log.setDescription("SCHEDULING MESSAGE FAILED");
                log.setLevel(LogLevel.Severe);
                log.setStatus(LogStatus.FAILED);

                if (ex != null) {

                    String detailedString = Stream.of(ex.getStackTrace())
                            .map(x -> x.toString()).collect(Collectors.joining(","));
                    logService.log(log, detailedString);
                } else {
                    logService.log(log);
                }
            } catch (Exception e) {
            }
        }
    }
    
    @PreDestroy
    public void cleanUp(){
        
        try {
            if(session != null)
                session.close();
            
            if(connection != null)
                connection.close();
        } catch (JMSException ex) {
            logger.log(Priority.ERROR, session);
        }
    }
}

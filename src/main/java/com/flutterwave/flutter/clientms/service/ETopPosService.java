/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.model.Configuration;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.util.CryptoUtil;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Utility;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class ETopPosService {
    
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private LogService logService;
    
    private final static String DATE_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss Z";
    
//    public String createMerchant(String merchantCode, String businessName){
//        
//        try{
//        JSONObject jSONObject = new JSONObject()
//                .put("businessName", businessName)
//                .put("otherId", merchantCode);
//        
//        String body = jSONObject.toString();
//        
//        String baseUrl = configurationDao.getConfig("core_pos_base_url");
//        
//        String environment = configurationDao.getConfig("environment");
//        
//        if(environment == null && baseUrl == null){
//            baseUrl = "https://flutterwaveprodv2.com";
//        }
//        
//        String url = baseUrl+"/flwvpos/api/etop/addmerchant";
//        
//        Map<String,String> header = getHeader(getHash(merchantCode));
//         
//        logData(body + "" + header.toString(), "Create ETOP POS Merchant", LogState.STARTED);
//       
//        String response = httpUtil.doHttpPost(url, body, header);
//        
//        logData(response, "Create ETOP POS Merchant", LogState.FINISH);
//        
//        jSONObject = new JSONObject(response);
//        
//        String status = jSONObject.optString("status", null);
//        
//        if(!"success".equalsIgnoreCase(status)){
//            
//            return null;
//        }
//        
//        JSONObject dataObject =  jSONObject.getJSONObject("data");
//        
//        JSONObject statusObject = dataObject.getJSONObject("status");
//        
//        int statusInt = statusObject.optInt("code", -1);
//        
//        if(statusInt != 0)
//            return null;
//        
//        JSONObject entityObject = dataObject.getJSONObject("entity");
//        
//        int posId = entityObject.optInt("id", -1);
//        
//        if(posId <= -1)
//            return null;
//        
//        return String.valueOf(posId);
//        }catch(Exception ex){
//            
//            return null;
//        }
//    }
    
    public String createMerchant(String merchantCode, String businessName){
        
        try{
            
            Configuration configuration = configurationDao.findByKey("name", "posid_counter");
            
            String value = configuration.getValue();
            
            if(Utility.emptyToNull(value) == null){
                
                value = "100000";
            }
           
            Long counter = Long.parseLong(value);
            
            counter++;
            
            configuration.setValue(counter+"");

            configurationDao.update(configuration);
            
//            Properties properties = Utility.getConfigProperty();
//            
//            if(properties == null)
//                return null;
//            
//            String id = properties.getProperty("posid_counter", "20000");
//            
//            Long counter = Long.parseLong(id);
//            
//            counter++;
//            
//            properties.setProperty("posid_counter", counter+"");
//            
//            
//            Utility.saveConfigProperty(properties);
//            
//            
            return counter+"";
        }catch(Exception ex){
            
            return null;
        }
        
        
    }
    
    
    public boolean updateMerchant(String merchantCode, String businessName, String posId){
        
        return true;
//        JSONObject jSONObject = new JSONObject()
//                .put("businessName", businessName)
//                .put("otherId", merchantCode)
//                .put("id", Integer.parseInt(posId));
//        
//        String body = jSONObject.toString();
//        
//        String baseUrl = configurationDao.getConfig("core_pos_base_url");
//        
//        String environment = configurationDao.getConfig("environment");
//        
//        if(environment == null && baseUrl == null){
//            baseUrl = "https://flutterwaveprodv2.com";
//        }
//        
//        String url = baseUrl+"/flwvpos/api/etop/updatemerchant";
//        
//        Map<String,String> header = getHeader(getHash(merchantCode+""+posId));
//         
//        logData(body + "" + header.toString(), "Update ETOP POS Merchant", LogState.STARTED);
//       
//        String response = httpUtil.doHttpPut(url, body, header);
//        
//        logData(response, "Update ETOP POS Merchant", LogState.FINISH);
//        
//        jSONObject = new JSONObject(response);
//        
//        String status = jSONObject.optString("status", null);
//        
//        if(!"success".equalsIgnoreCase(status)){
//            
//            return false;
//        }
//        
//        JSONObject dataObject =  jSONObject.getJSONObject("data");
//        
//        JSONObject statusObject = dataObject.getJSONObject("status");
//        
//        int statusInt = statusObject.optInt("code", -1);
//        
//        return statusInt == 0;
    }
    
    public JSONArray getMerchants(){
        
        String baseUrl = configurationDao.getConfig("core_pos_base_url");
        
        String environment = configurationDao.getConfig("environment");
        
        if(environment == null && baseUrl == null){
            baseUrl = "https://flutterwaveprodv2.com";
        }
        
        String url = baseUrl+"/flwvpos/api/etop/fetchmerchant";
        
        Map<String,String> header = getHeader(getHash(""));
         
        logData(header.toString(), "Fetch ETOP POS Merchant", LogState.STARTED);
       
        String response = httpUtil.doHttpGet(url, header);
        
        logData(response, "Update ETOP POS Merchant", LogState.FINISH);
        
        JSONObject jSONObject = new JSONObject(response);
        
        String status = jSONObject.optString("status", null);
        
        if(!"success".equalsIgnoreCase(status)){
            
            return null;
        }
        
        JSONObject dataObject =  jSONObject.getJSONObject("data");
        
        JSONObject statusObject = dataObject.getJSONObject("status");
        
        int statusInt = statusObject.optInt("code", -1);
        
        if(statusInt != 0)
            return null;
        
        JSONArray jSONArray = dataObject.getJSONArray("entity");
        
        return jSONArray;
    }
    
    public boolean deleteMerchant(String posId){
        
        String baseUrl = configurationDao.getConfig("core_pos_base_url");
        
        String environment = configurationDao.getConfig("environment");
        
        if(environment == null && baseUrl == null){
            baseUrl = "https://flutterwaveprodv2.com";
        }
        
        
        String url = baseUrl+"/flwvpos/api/etop/deletemerchant/"+posId;
        
        Map<String,String> header = getHeader(getHash(posId));
         
        logData(posId + "" + header.toString(), "DELETE ETOP POS Merchant", LogState.STARTED);
       
        String response = httpUtil.doHttpDelete(url, null, header);
        
        logData(response, "DELETE ETOP POS Merchant", LogState.FINISH);
        
        JSONObject jSONObject = new JSONObject(response);
        
        String status = jSONObject.optString("status", null);
        
        if(!"success".equalsIgnoreCase(status)){
            
            return false;
        }
        
        JSONObject dataObject =  jSONObject.getJSONObject("data");
        
        JSONObject statusObject = dataObject.getJSONObject("status");
        
        int statusInt = statusObject.optInt("code", -1);
        
        return statusInt == 0;
    }
    
    public JSONArray getTransaction(String posId){
        
        String baseUrl = configurationDao.getConfig("core_pos_base_url");
        
        String environment = configurationDao.getConfig("environment");
        
        if(environment == null && baseUrl == null){
            baseUrl = "https://flutterwaveprodv2.com";
        }
        
        
        String url = baseUrl+"/flwvpos/api/etop/listtransactions/";
        
        Map<String,String> header = getHeader(getHash(posId));
        
        JSONObject jSONObject = new JSONObject()
                .put("merchantId", Utility.nullToEmpty(posId))
                .put("cursor", "")
                .put("size", "");
         
        logData(jSONObject.toString() + "" + header.toString(), "GET ETOP POS TRANSACTIONS", LogState.STARTED);
       
        String response = httpUtil.doHttpPost(url, jSONObject.toString(), header);
        
        logData(response, "GET ETOP POS TRANSACTIONS", LogState.FINISH);
        
        jSONObject = new JSONObject(response);
        
        String status = jSONObject.optString("status", null);
        
        if(!"success".equalsIgnoreCase(status)){
            
            return null;
        }
        
        JSONObject dataObject =  jSONObject.getJSONObject("data");
        
        JSONObject statusObject = dataObject.getJSONObject("status");
        
        int statusInt = statusObject.optInt("code", -1);
        
        if(statusInt != 0)
            return null;
        
        JSONArray jSONArray = dataObject.getJSONArray("entity");
        
        return jSONArray;
    }
    
    public JSONArray getTransaction(String posId, Date startDate, Date endDate){
        
        String baseUrl = configurationDao.getConfig("core_pos_base_url");
        
        String environment = configurationDao.getConfig("environment");
        
        if(environment == null && baseUrl == null){
            baseUrl = "https://flutterwaveprodv2.com";
        }
        
        String url = baseUrl+"/flwvpos/api/etop/transactions";
        
        Map<String,String> header = getHeader(getHash(""));
        
        JSONObject jSONObject = new JSONObject()
                .put("merchantId", Utility.nullToEmpty(posId))
                .put("start", Utility.formatDate(startDate, DATE_FORMAT_STRING))
                .put("end", Utility.formatDate(endDate, DATE_FORMAT_STRING));
         
        logData(jSONObject.toString() + "" + header.toString(), "GET ETOP POS TRANSACTIONS", LogState.STARTED);
       
        String response = httpUtil.doHttpPost(url, jSONObject.toString(), header);
        
        logData(response, "GET ETOP POS TRANSACTIONS", LogState.FINISH);
        
        jSONObject = new JSONObject(response);
        
        String status = jSONObject.optString("status", null);
        
        if(!"success".equalsIgnoreCase(status)){
            
            return null;
        }
        
        JSONObject dataObject =  jSONObject.getJSONObject("data");
        
        JSONObject statusObject = dataObject.getJSONObject("status");
        
        int statusInt = statusObject.optInt("code", -1);
        
        if(statusInt != 0)
            return null;
        
        JSONArray jSONArray = dataObject.getJSONArray("entity");
        
        return jSONArray;
    }
    
//    public JSONArray getFailedNotifiedTransaction(String posId, Date date){
//        
//        String baseUrl = configurationDao.getConfig("core_pos_base_url");
//        
//        String environment = configurationDao.getConfig("environment");
//        
//        if(environment == null && baseUrl == null){
//            baseUrl = "https://flutterwaveprodv2.com";
//        }
//        
//        String url = baseUrl+"/flwvpos/api/etop/failedtransactions";
//        
//        Map<String,String> header = getHeader(getHash(""));
//        
//        JSONObject jSONObject = new JSONObject()
//                .put("merchantId", Utility.nullToEmpty(posId))
//                .put("cursor", (date == null) ? "" :  Utility.formatDate(date, DATE_FORMAT_STRING));
////                .put("end", Utility.formatDate(endDate, DATE_FORMAT_STRING));
//         
//        logData(jSONObject.toString() + "" + header.toString(), "GET FAILED ETOP POS TRANSACTIONS", LogState.STARTED);
//       
//        String response = httpUtil.doHttpPost(url, jSONObject.toString(), header);
//        
//        
//        if(response == null)
//            return null;
//        
//        logData(response, "GET ETOP POS TRANSACTIONS", LogState.FINISH);
//        
//        jSONObject = new JSONObject(response);
//        
//        String status = jSONObject.optString("status", null);
//        
//        if(!"success".equalsIgnoreCase(status)){
//            
//            return null;
//        }
//        
//        JSONObject dataObject =  jSONObject.getJSONObject("data");
//        
//        JSONObject statusObject = dataObject.getJSONObject("status");
//        
//        int statusInt = statusObject.optInt("code", -1);
//        
//        if(statusInt != 0)
//            return null;
//        
//        JSONArray jSONArray = dataObject.getJSONArray("entity");
//        
//        return jSONArray;
//    }
    
    public JSONArray getFailedNotifiedTransaction(String posId, Date startDate, Date endDate){
        
        String baseUrl = configurationDao.getConfig("core_pos_base_url");
        
        String environment = configurationDao.getConfig("environment");
        
        if(environment == null && baseUrl == null){
            baseUrl = "https://flutterwaveprodv2.com";
        }
        
        String url = baseUrl+"/flwvpos/api/etop/failedtransactions";
        
        Map<String,String> header = getHeader(getHash(""));
        
        JSONObject jSONObject = new JSONObject()
                .put("merchantId", Utility.nullToEmpty(posId))
                .put("start", (startDate == null) ? "" :  Utility.formatDate(startDate, DATE_FORMAT_STRING))
                .put("end", (endDate == null) ? "" :  Utility.formatDate(endDate, DATE_FORMAT_STRING));
//                .put("end", Utility.formatDate(endDate, DATE_FORMAT_STRING));
         
        logData(jSONObject.toString() + "" + header.toString(), "GET FAILED ETOP POS TRANSACTIONS", LogState.STARTED);
       
        String response = httpUtil.doHttpPost(url, jSONObject.toString(), header);
        
        
        if(response == null)
            return null;
        
        logData(response, "GET ETOP POS TRANSACTIONS", LogState.FINISH);
        
        jSONObject = new JSONObject(response);
        
        String status = jSONObject.optString("status", null);
        
        if(!"success".equalsIgnoreCase(status)){
            
            return null;
        }
        
        JSONObject dataObject =  jSONObject.getJSONObject("data");
        
        JSONObject statusObject = dataObject.getJSONObject("status");
        
        int statusInt = statusObject.optInt("code", -1);
        
        if(statusInt != 0)
            return null;
        
        JSONArray jSONArray = dataObject.getJSONArray("entity");
        
        return jSONArray;
    }
    
    public InputStream downloadReceipt(String tellerpoint){
        
        if(tellerpoint == null)
            return null;
        
        String environment = configurationDao.getConfig("environment");
            
//        if(Utility.emptyToNull(environment) != null || "test".equalsIgnoreCase(environment) )
//            return null;
        
        String url = Global.ETOP;
        
        url += ""+tellerpoint;
        
        Map<String, String> header = new HashMap<>();
        header.put("appId", "FLUTTERWAVE");
        header.put("apiSecret", "_pR0d_FlutT3rW@v3_");
        header.put("Content-Type", "application/json");
        
        InputStream response = httpUtil.doHttpGetCRaw(url, header);
        
        return response;
    }
    
    private String getHash(String body){
        
        String key = configurationDao.getConfig("core_pos_key");
        
        if(key == null){
            key = "9821419111fababb";
        }
        
        String hashTemp = key + ""+body;
        
        String hash= CryptoUtil.sha512(hashTemp, null);
        
        return hash.toUpperCase();
    }
    
    private Map<String, String> getHeader(String hash){
        
        Map<String, String> header = new  HashMap<>();
        header.put("Authorization", hash);
        header.put("Content-Type", "application/json");
        
        return header;
    }
    
    private void logData(String body, String action, LogState logState){
        
        Log log = new Log();
        log.setAction(action);
        log.setCreatedOn(new Date());
        log.setDescription(body);
        log.setLevel(LogLevel.Info);
        log.setLogDomain(LogDomain.ADMIN);
        log.setLogState(logState);
        log.setStatus(LogStatus.SUCCESSFUL);
        
        logService.log(log);
    }
}

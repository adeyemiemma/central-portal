/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.ejb.Singleton;

/**
 *
 * @author emmanueladeyemi
 */
@Singleton
public class PosReversalCache {
    
    private final Map<String, Object> dataMap = new ConcurrentHashMap<>();
    
    public void add(String key, Object value){
        
        dataMap.put(key, value);
    }
    
    public Object get(String key){
        
        return dataMap.get(key);
    }
    
    public Object get(String key, Object defaultValue){
        
        return dataMap.getOrDefault(key, defaultValue);
    }
    
    public int getSize(){
        
        return dataMap.size();
    }
    
    public Map<String, Object> getMap(){
        
        return dataMap;
    }
}

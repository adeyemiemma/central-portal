/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.PosMerchantCustomerDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.dao.PosOrderDao;
import com.flutterwave.flutter.clientms.dao.PosTerminalDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionDao;
import com.flutterwave.flutter.clientms.dao.PosTransactionNewDao;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.clientms.model.PosMerchantCustomer;
import com.flutterwave.flutter.clientms.model.PosOrder;
import com.flutterwave.flutter.clientms.model.PosTerminal;
import com.flutterwave.flutter.clientms.model.PosTransaction;
import com.flutterwave.flutter.clientms.model.PosTransactionNew;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.TransactionUtility;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.core.Response;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class GAPosService {

    @EJB
    private HttpUtil httpUtil;

    @EJB
    private PosTransactionDao posTransactionDao;
    @EJB
    private PosTransactionNewDao posTransactionNewDao;
    @EJB
    private PosMerchantDao posMerchantDao;
    @EJB
    private PosOrderDao posOrderDao;
    @EJB
    private PosTerminalDao posTerminalDao;
    @EJB
    private UtilityService utilityService;
    @EJB
    private PosMerchantCustomerDao posMerchantCustomerDao;

    static String BASE_URL = "https://api.globalaccelerex.com/posvasdata/api";

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @AccessTimeout(value = 600000)
    public void getTransactionAndSave(Date startDate, Date endDate, String terminalId) {

//        String url = BASE_URL + "/reports/flutterwave";
        int page = 0;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.DATE, -1);

        startDate = calendar.getTime();

        calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.add(Calendar.DATE, -1);

        endDate = calendar.getTime();

        String response = getTransactions(startDate, endDate, terminalId, page++, 300);

        if (response == null) {
            return;
        }

        JSONObject jSONObject = new JSONObject(response);

        int total = jSONObject.optInt("RecordsTotal", 0);

        if (total > 0) {

            JSONArray data = jSONObject.optJSONArray("Data");

            saveTransaction(data);

            boolean hasNextPage = jSONObject.optBoolean("HasNextPage", false);

            while (hasNextPage == true) {

                //            url = BASE_URL + "/reports/flutterwave";
                response = getTransactions(startDate, endDate, terminalId, page++, 300);

                if (response == null) {
                    return;
                }

                jSONObject = new JSONObject(response);

                total = jSONObject.optInt("RecordsTotal", 0);

                if (total <= 0) {
                    return;
                }

                data = jSONObject.optJSONArray("Data");

                saveTransaction(data);

                hasNextPage = jSONObject.optBoolean("HasNextPage", false);
            }
        } else {

            page = 0;
            
            response = getTransactionsV2(startDate, endDate, terminalId, page++, 300);

            if (response == null) {
                return;
            }

            jSONObject = new JSONObject(response);

            total = jSONObject.optInt("RecordsTotal", 0);

            if (total > 0) {

                JSONArray data = jSONObject.optJSONArray("Data");

                saveTransactionV2(data);

                boolean hasNextPage = jSONObject.optBoolean("HasNextPage", false);

                while (hasNextPage == true) {

                    //            url = BASE_URL + "/reports/flutterwave";
                    response = getTransactionsV2(startDate, endDate, terminalId, page++, 300);

                    if (response == null) {
                        return;
                    }

                    jSONObject = new JSONObject(response);

                    total = jSONObject.optInt("RecordsTotal", 0);

                    if (total <= 0) {
                        return;
                    }

                    data = jSONObject.optJSONArray("Data");

                    saveTransactionV2(data);

                    hasNextPage = jSONObject.optBoolean("HasNextPage", false);
                }
            }
        }

    }

    private void saveTransactionV2(JSONArray data) {

        for (int i = 0; i < data.length(); i++) {

            try {

                JSONObject payload = data.getJSONObject(i);

                PosMerchant merchant = null;

                String orderId = null, customerId = null;

//                JSONObject nObject = new JSONObject(payload.optString("Request"));
                String posId = payload.optString("Posid", null);

                PosMerchantCustomer merchantCustomer = null;

                String terminalId = payload.optString("Terminalid");

                PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", terminalId);

                if (posTerminal == null) {

                    continue;
                }

//                if (posId != null && !"0".equalsIgnoreCase(posId)) {
//                    merchant = posMerchantDao.findByKey("posId", posId);
//
//                    if (merchant == null) {
//
//                        PosOrder order = posOrderDao.findByKey("posOrderId", posId);
//
//                        if (order != null) {
//
//                            merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());
//
//                            orderId = order.getCustomerOrderId();
//
//                        }  else {
//                            
//                            merchantCustomer = posMerchantCustomerDao.findByKey("customerId", posId );
//                            
//                            if(merchantCustomer != null){
//                                
//                                merchant = posMerchantDao.findByKey("merchantCode", merchantCustomer.getMerchantCode());
//
//                                customerId = merchantCustomer.getCustomerId();
//                            }
//                        }
//                    }
//                }
//                if (request.getPosId() != null && !"0".equalsIgnoreCase(request.getPosId())) {
//                merchant = posMerchantDao.findByKey("posId", request.getPosId());
//
//                if (merchant == null) {
//
//                    PosOrder order = posOrderDao.findByKey("posOrderId", request.getPosId());
//                
//                    if(order != null){
//
//                        merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());
//                        
//                        orderId = order.getCustomerOrderId();
//
//                    }else {
//                        
//                        posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", request.getPosId());
//                        
//                        if(posMerchantCustomer == null){
//                            response.put("responsecode", "06");
//                            response.put("responsemessage", "Merchant with pos id does not exists");
//
//                            return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
//                        }
//                        
//                        merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());
//                        
//                        customerId = posMerchantCustomer.getCustomerId();
//                        
//                    }
//                }
//            }else{
//                
//                merchant = posTerminal.getMerchant();
//            }
//                
                if (posTerminal.getMerchant() != null && merchant == null) {
                    merchant = posTerminal.getMerchant();

                }

                PosTransaction posTransaction = new PosTransaction();
                posTransaction.setAmount(payload.optDouble("Amount"));
                posTransaction.setCreatedOn(new Date());
//            posTransaction.setCurrency(request.getCurrencyCode());

                posTransaction.setCurrency(payload.optString("Currency", payload.optString("Currency")));

                String rrn = payload.optString("RetrievalReferenceNumber");

                try {
                    Long rrnLong = Long.parseLong(rrn);

                    rrn = String.valueOf(rrnLong);
                } catch (Exception ex) {

                    continue;
                }

                Date requestDate = null, responseDate = null;

                requestDate = Utility.parseDate(payload.optString("Paymentdate"), "yyyy-MM-dd'T'HH:mm:ss");

                responseDate = requestDate;

                PosTransaction transaction = posTransactionDao.findRRNTerminal(rrn, terminalId, requestDate);

                String customerName = payload.optString("Customername");

                String parentName = null;

                if (merchant != null) {
                    posTransaction.setPosId(merchant.getPosId());
                    posTransaction.setMerchantName(merchant.getName());

                    posId = merchant.getPosId();

                    if (merchant.getParent() != null) {
                        parentName = merchant.getParent().getName();
                    }
                }

                posTransaction.setRrn(rrn);
                posTransaction.setRequestDate(requestDate);

                String transRef = terminalId + "" + posTransaction.getRrn() + "" + posTransaction.getAmount() + "" + Utility.formatDate(posTransaction.getRequestDate(), "yyyyMMdd");

                posTransaction.setTransRef(transRef);

                if (transaction != null) {

                    PosTransactionNew posTransactionNew;
                    if (merchant != null) {
                        posTransactionNew = TransactionUtility.buildPosTransaction(transaction, orderId, merchant.getBankName(),
                                customerName, null, parentName, merchant.getMerchantId(),
                                merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null,
                                merchant, posTerminal.getProvider(), merchantCustomer);
                    } else {
                        posTransactionNew = TransactionUtility.buildPosTransaction(transaction, orderId, null,
                                customerName, null, parentName, null,
                                null, posTerminal.getProvider().getName(), null, null,
                                merchant, posTerminal.getProvider(), merchantCustomer);
                    }

                    if (posTransactionNew != null) {

                        PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(transaction.getRrn(),
                                transaction.getTerminalId(), transaction.getRequestDate());

                        if (posTransactionN == null) {
                            posTransactionNewDao.create(posTransactionNew);
                        } else {

                            posTransactionNew.setId(posTransactionN.getId());
                            posTransactionNewDao.update(posTransactionNew);
                        }
                    }

                    if (transaction.isCallBack() == false) {
                        utilityService.callBackUser(posTransaction, orderId, merchantCustomer);
                    }

                    continue;
                }

                posTransaction.setSource("GLOBALACCELEREX");
                posTransaction.setFileId(payload.optString("Id"));
                posTransaction.setLoggedBy("System");
                posTransaction.setPan(payload.optString("Maskedpan"));
                posTransaction.setPosId(posId);

                
//            PosMerchant merchant = posMerchantDao.findByKey("posId", request.getMerchantId());
                posTransaction.setRefCode(transRef);
                posTransaction.setRequestDate(requestDate);
                posTransaction.setResponseDate(responseDate);
                posTransaction.setResponseCode(payload.optString("StatusCode"));
                posTransaction.setResponseMessage(payload.optString("StatusDescription"));
//            posTransaction.setRrn(request.getRrn());
                posTransaction.setScheme(payload.optString("CardScheme"));
//                posTransaction.setSource(payload.optString("source"));
                posTransaction.setStatus(payload.optString("Status"));
                posTransaction.setRrn(rrn);
                posTransaction.setTerminalId(terminalId);
                posTransaction.setType(payload.optString("Type"));
                posTransaction.setTraceref(payload.optString("BillerReference"));

                
                if(!"Success".equalsIgnoreCase(posTransaction.getStatus())
                        && !"Retry".equalsIgnoreCase(posTransaction.getStatus())){
                
                    continue;
                }
                
                posTransaction = posTransactionDao.create(posTransaction);

                PosTransactionNew posTransactionNew = null;

                if (merchant != null) {
                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, merchant.getBankName(),
                            customerName, null, parentName, merchant.getMerchantId(),
                            merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null,
                            merchant, posTerminal.getProvider(), merchantCustomer);
                } else {
                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, null,
                            customerName, null, parentName, null,
                            null, posTerminal.getProvider().getName(), null, null, merchant, posTerminal.getProvider(), merchantCustomer);
                }

                if (posTransactionNew != null) {

                    PosTransactionNew transactionNew = posTransactionNewDao.findRRNTerminal(rrn, terminalId, requestDate);

                    if (transactionNew == null) {
                        posTransactionNewDao.create(posTransactionNew);
                    } else {

                        posTransactionNew.setId(transactionNew.getId());
                        posTransactionNewDao.update(posTransactionNew);
                    }
                }

                utilityService.callBackUser(posTransaction, orderId, merchantCustomer);

            } catch (DatabaseException ex) {

                Logger.getLogger(GAPosService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void saveTransaction(JSONArray data) {

        for (int i = 0; i < data.length(); i++) {

            try {

                JSONObject payload = data.getJSONObject(i);

                PosMerchant merchant = null;

                String orderId = null, customerId = null;

                JSONObject nObject = new JSONObject(payload.optString("Request"));

                String posId = nObject.optString("posid");

                PosMerchantCustomer merchantCustomer = null;

                String terminalId = nObject.optString("terminalid");

                PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", terminalId);

                if (posTerminal == null) {

                    continue;
                }

                if (posId != null && !"0".equalsIgnoreCase(posId)) {
                    merchant = posMerchantDao.findByKey("posId", posId);

                    if (merchant == null) {

                        PosOrder order = posOrderDao.findByKey("posOrderId", posId);

                        if (order != null) {

                            merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());

                            orderId = order.getCustomerOrderId();

                        } else {

                            merchantCustomer = posMerchantCustomerDao.findByKey("customerId", posId);

                            if (merchantCustomer != null) {

                                merchant = posMerchantDao.findByKey("merchantCode", merchantCustomer.getMerchantCode());

                                customerId = merchantCustomer.getCustomerId();
                            }
                        }
                    }
                }

//                if (request.getPosId() != null && !"0".equalsIgnoreCase(request.getPosId())) {
//                merchant = posMerchantDao.findByKey("posId", request.getPosId());
//
//                if (merchant == null) {
//
//                    PosOrder order = posOrderDao.findByKey("posOrderId", request.getPosId());
//                
//                    if(order != null){
//
//                        merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());
//                        
//                        orderId = order.getCustomerOrderId();
//
//                    }else {
//                        
//                        posMerchantCustomer = posMerchantCustomerDao.findByKey("customerId", request.getPosId());
//                        
//                        if(posMerchantCustomer == null){
//                            response.put("responsecode", "06");
//                            response.put("responsemessage", "Merchant with pos id does not exists");
//
//                            return returnResponse(Response.Status.BAD_REQUEST, response, title, uniqueId);
//                        }
//                        
//                        merchant = posMerchantDao.findByKey("merchantCode", posMerchantCustomer.getMerchantCode());
//                        
//                        customerId = posMerchantCustomer.getCustomerId();
//                        
//                    }
//                }
//            }else{
//                
//                merchant = posTerminal.getMerchant();
//            }
//                
                if (posTerminal.getMerchant() != null && merchant == null) {
                    merchant = posTerminal.getMerchant();

                }

                PosTransaction posTransaction = new PosTransaction();
                posTransaction.setAmount(nObject.optDouble("amount"));
                posTransaction.setCreatedOn(new Date());
//            posTransaction.setCurrency(request.getCurrencyCode());

                posTransaction.setCurrency(nObject.optString("currency", nObject.optString("currencycode")));

                String rrn = nObject.optString("rrn");

                try {
                    Long rrnLong = Long.parseLong(rrn);

                    rrn = String.valueOf(rrnLong);
                } catch (Exception ex) {

                    continue;
                }

                Date requestDate = null, responseDate = null;

                requestDate = Utility.parseDate(nObject.optString("requestdate"), "yyyy-MM-dd HH:mm:ss");

                responseDate = Utility.parseDate(nObject.optString("responsedate"), "yyyy-MM-dd HH:mm:ss");

                PosTransaction transaction = posTransactionDao.findRRNTerminal(rrn, terminalId, requestDate);

                String customerName = nObject.optString("customername");

                String parentName = null;

                if (merchant != null) {
                    posTransaction.setPosId(merchant.getPosId());
                    posTransaction.setMerchantName(merchant.getName());

                    posId = merchant.getPosId();

                    if (merchant.getParent() != null) {
                        parentName = merchant.getParent().getName();
                    }
                }

                posTransaction.setRrn(rrn);
                posTransaction.setRequestDate(requestDate);

                String transRef = terminalId + "" + posTransaction.getRrn() + "" + posTransaction.getAmount() + "" + Utility.formatDate(posTransaction.getRequestDate(), "yyyyMMdd");

                posTransaction.setTransRef(transRef);

                if (transaction != null) {

                    PosTransactionNew posTransactionNew;
                    if (merchant != null) {
                        posTransactionNew = TransactionUtility.buildPosTransaction(transaction, orderId, merchant.getBankName(),
                                customerName, null, parentName, merchant.getMerchantId(),
                                merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null,
                                merchant, posTerminal.getProvider(), merchantCustomer);
                    } else {
                        posTransactionNew = TransactionUtility.buildPosTransaction(transaction, orderId, null,
                                customerName, null, parentName, null,
                                null, posTerminal.getProvider().getName(), null, null,
                                merchant, posTerminal.getProvider(), merchantCustomer);
                    }

                    if (posTransactionNew != null) {

                        PosTransactionNew posTransactionN = posTransactionNewDao.findRRNTerminal(transaction.getRrn(),
                                transaction.getTerminalId(), transaction.getRequestDate());

                        if (posTransactionN == null) {
                            posTransactionNewDao.create(posTransactionNew);
                        } else {

                            posTransactionNew.setId(posTransactionN.getId());
                            posTransactionNewDao.update(posTransactionNew);
                        }
                    }

                    if (transaction.isCallBack() == false) {
                        utilityService.callBackUser(posTransaction, orderId, merchantCustomer);
                    }

                    continue;
                }

                posTransaction.setSource("GLOBALACCELEREX");
                posTransaction.setFileId(nObject.optString("id"));
                posTransaction.setLoggedBy("System");
                posTransaction.setPan(nObject.optString("pan"));
                posTransaction.setPosId(posId);

//            PosMerchant merchant = posMerchantDao.findByKey("posId", request.getMerchantId());
                posTransaction.setRefCode(nObject.optString("refcode"));
                posTransaction.setRequestDate(requestDate);
                posTransaction.setResponseDate(responseDate);
                posTransaction.setResponseCode(nObject.optString("responsecode"));
                posTransaction.setResponseMessage(nObject.optString("responsemessage"));
//            posTransaction.setRrn(request.getRrn());
                posTransaction.setScheme(nObject.optString("scheme"));
                posTransaction.setSource(nObject.optString("source"));
                posTransaction.setStatus(nObject.optString("status"));
                posTransaction.setRrn(rrn);
                posTransaction.setTerminalId(terminalId);
                posTransaction.setType(nObject.optString("type"));

                posTransaction = posTransactionDao.create(posTransaction);

                PosTransactionNew posTransactionNew = null;

                if (merchant != null) {
                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, merchant.getBankName(),
                            customerName, null, parentName, merchant.getMerchantId(),
                            merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null,
                            merchant, posTerminal.getProvider(), merchantCustomer);
                } else {
                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, null,
                            customerName, null, parentName, null,
                            null, posTerminal.getProvider().getName(), null, null, merchant, posTerminal.getProvider(), merchantCustomer);
                }

                if (posTransactionNew != null) {

                    PosTransactionNew transactionNew = posTransactionNewDao.findRRNTerminal(rrn, terminalId, requestDate);

                    if (transactionNew == null) {
                        posTransactionNewDao.create(posTransactionNew);
                    } else {

                        posTransactionNew.setId(transactionNew.getId());
                        posTransactionNewDao.update(posTransactionNew);
                    }
                }

                utilityService.callBackUser(posTransaction, orderId, merchantCustomer);

            } catch (DatabaseException ex) {

                Logger.getLogger(GAPosService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

//    private void saveTransaction(JSONArray data) {
//
//        for (int i = 0; i < data.length(); i++) {
//
//            try {
//
//                JSONObject nObject = data.getJSONObject(i);
//
//                PosMerchant merchant = null;
//
//                String orderId = null;
//
//                String merchantPosId = nObject.optString("Posid");
//
//                if (merchantPosId != null) {
//                    merchant = posMerchantDao.findByKey("posId", merchantPosId);
//
//                    if (merchant == null) {
//
//                        PosOrder order = posOrderDao.findByKey("posOrderId", merchantPosId);
//
//                        if (order != null) {
//
//                            merchant = posMerchantDao.findByKey("merchantCode", order.getMerchantCode());
//
//                            orderId = order.getCustomerOrderId();
//
//                        } else {
//
//                            continue;
//                        }
//                    }
//                }
//
//                String terminalId = nObject.optString("Terminalid");
//
//                PosTerminal posTerminal = posTerminalDao.findByKey("terminalId", terminalId);
//
//                if (posTerminal == null) {
//
//                    continue;
//                }
//
//                double amount = nObject.optDouble("Amount", 0.0);
//
//                PosTransaction posTransaction = new PosTransaction();
//                posTransaction.setAmount(amount);
//                posTransaction.setCreatedOn(new Date());
////            posTransaction.setCurrency(request.getCurrencyCode());
//
//                posTransaction.setCurrency("NGN");
//
////            try {
////                Currency currency = Currency.getInstance("NGN");
////                
////                posTransaction.setCurrency(currency.getNumericCode() + "");
////            } catch (Exception ex) {
////                ex.printStackTrace();
////
//////                posTransaction.setCurrency("NGN");
////            }
//                String rrn = nObject.optString("RetrievalReferenceNumber");
//
//                try {
//                    Long rrnLong = Long.parseLong(rrn);
//
//                    rrn = String.valueOf(rrnLong);
//                } catch (Exception ex) {
//
//                    continue;
//                }
//
//                posTransaction.setSource("GLOBALACCELEREX");
//
//                String date = nObject.optString("Requestdate");
//
//                Date requestDate = Utility.parseDate(date, "yyyy-MM-dd'T'HH:mm:ss");
//
//                PosTransaction transactions = posTransactionDao.findRRNTerminal(rrn, terminalId, requestDate);
//
//                if (transactions != null) {
//                    continue;
//                }
//
//                String cardHolderName = nObject.optString("Customername");
//
//                posTransaction.setFileId(nObject.optInt("Id") + "");
//                posTransaction.setLoggedBy("System");
//                posTransaction.setPan(nObject.optString("Maskedpan"));
//                posTransaction.setPosId(nObject.optString("Posid"));
//
//                String parentName = null;
//
//                if (merchant != null) {
//                    posTransaction.setPosId(merchant.getPosId());
//
//                    if (merchant.getParent() != null) {
//                        parentName = merchant.getParent().getName();
//                    }
//                }
////            PosMerchant merchant = posMerchantDao.findByKey("posId", request.getMerchantId());
//
//                String status = nObject.optString("Status");
//
//                posTransaction.setStatus(status);
//
//                String responseCode = null;
//
//                if ("Success".equalsIgnoreCase(status)) {
//                    responseCode = "00";
//                    posTransaction.setResponseCode(responseCode);
//                    posTransaction.setResponseMessage(status);
//                } else {
//                    posTransaction.setResponseCode("99");
//                    posTransaction.setResponseMessage(status);
//                }
//
////                posTransaction.setRefCode(status + "" + terminalId + "" + posTransaction.getFileId());
//                posTransaction.setRequestDate(requestDate);
//                posTransaction.setResponseDate(requestDate);
//
//                posTransaction.setRefCode(nObject.optString("RefCode"));
//                posTransaction.setScheme(null);
//                posTransaction.setSource("GLOBALACCELEREX");
////            posTransaction.setStatus(status);
//                posTransaction.setRrn(rrn);
//                posTransaction.setTerminalId(terminalId);
//                posTransaction.setType("Purchase");
//
//                posTransaction = posTransactionDao.create(posTransaction);
//
//                PosTransactionNew posTransactionNew = null;
//
//                if (merchant != null) {
//                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, merchant.getBankName(),
//                            cardHolderName, null, parentName, merchant.getMerchantId(),
//                            merchant.getMerchantId(), posTerminal.getProvider().getName(), merchant.getMerchantCode(), null);
//                } else {
//                    posTransactionNew = TransactionUtility.buildPosTransaction(posTransaction, orderId, null,
//                            cardHolderName, null, parentName, null,
//                            null, posTerminal.getProvider().getName(), null, null);
//                }
//
//                if (posTransactionNew != null) {
//                    posTransactionNewDao.create(posTransactionNew);
//                }
//
//                utilityService.callBackUser(posTransaction, orderId);
//
//            } catch (DatabaseException ex) {
//
//                Logger.getLogger(GAPosService.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    private String getTransactions(Date startDate, Date endDate, String terminalId, int pageNo, int pageSize) {

        String url = BASE_URL + "/reports/flutterwave";

        String startTime = Utility.formatDate(startDate, "yyyy-MM-dd");
        String endTime = Utility.formatDate(endDate, "yyyy-MM-dd");

        url += "?startTime=" + startTime;
        url += "&endTime=" + endTime;

        if (terminalId != null) {
            url += "&terminalId=" + terminalId;
        }

        if (pageNo < 0) {
            pageNo = 0;
        }

        if (pageSize == 0) {
            pageSize = 300;
        }

        url += "&pageIndex=" + pageNo;
        url += "&pageSize=" + pageSize;

        String response = httpUtil.doHttpGet(url, getHeader());

        if (response == null) {
            return null;
        }

        return response;
    }

    private String getTransactionsV2(Date startDate, Date endDate, String terminalId, int pageNo, int pageSize) {

        String url = BASE_URL + "/reports/list";//flutterwave";

        String startTime = Utility.formatDate(startDate, "yyyy-MM-dd");
        String endTime = Utility.formatDate(endDate, "yyyy-MM-dd");

        url += "?startTime=" + startTime;
        url += "&endTime=" + endTime;

        if (terminalId != null) {
            url += "&terminalId=" + terminalId;
        }

        if (pageNo < 0) {
            pageNo = 0;
        }

        if (pageSize == 0) {
            pageSize = 300;
        }

        url += "&pageIndex=" + pageNo;
        url += "&pageSize=" + pageSize;

        String response = httpUtil.doHttpGet(url, getHeaderv2());

        if (response == null) {
            return null;
        }

        return response;
    }

    private Map<String, String> getHeader() {

        Properties properties = Utility.getConfigProperty();

        Map<String, String> header = new HashMap<>();
        header.put("content-type", "application/json");
        header.put("Authorization", "Bearer " + properties.getProperty("ga_pos_api_key"));

        return header;
    }

    private Map<String, String> getHeaderv2() {

        Properties properties = Utility.getConfigProperty();

        Map<String, String> header = new HashMap<>();
        header.put("content-type", "application/json");
        header.put("Authorization", "Bearer " + properties.getProperty("ga_pos_api_key_v2"));

        return header;
    }
}

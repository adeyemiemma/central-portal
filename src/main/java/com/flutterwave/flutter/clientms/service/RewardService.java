/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.RewardTransactionDao;
import com.flutterwave.flutter.clientms.model.RewardTransaction;
import com.flutterwave.flutter.clientms.util.CryptoUtil;
import com.flutterwave.flutter.clientms.util.Global;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class RewardService {
    
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private UtilityService utilityService; 
    @EJB
    private RewardTransactionDao rewardTransactionDao;

    public JSONObject getService(String customerId, String startDate, String endDate){
        
        String requestString = Global.PRIVATE_KEY +""+customerId;
        
        String hash = CryptoUtil.sha512(requestString, null);
        
        String url = configurationDao.getConfig("core_fetch_transaction_url");
        
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/x-www-form-urlencoded");
        
        
        String content= "token="+hash;
        content +=  "&merchantid="+Global.MERCHANT_ID;
        content +=  "&mask="+customerId;
        content += "&startdate="+startDate;
        content += "&enddate="+endDate;
        
        String responseString = httpUtil.doHttpPost(url, content, header);
        
        JSONObject response = new JSONObject(responseString);
        
        String responseCode = response.optString("responsecode");
        
        if("00".equalsIgnoreCase(responseCode)){
            
            JSONArray array = response.optJSONArray("records");
            
            utilityService.uploadRewardTransaction(array);
        }
        
        return response;
    } 
    
    public RewardTransaction getTransaction(String transactionReference){
        
        try {
            RewardTransaction rewardTransaction = rewardTransactionDao.findByKey("transactionReference", transactionReference);
            
            return rewardTransaction;
        } catch (DatabaseException ex) {
            Logger.getLogger(RewardService.class.getName()).error(null, ex);
        }
        
        return null;
    }
    
    public boolean updateransaction(RewardTransaction rewardTransaction){
        
        try {
            boolean status = rewardTransactionDao.update(rewardTransaction);
            
            return status;
        } catch (DatabaseException ex) {
            Logger.getLogger(RewardService.class.getName()).error(null, ex);
        }
        
        return false;
    }
    
}

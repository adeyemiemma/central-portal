/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.dao.PosMerchantDao;
import com.flutterwave.flutter.clientms.model.PosMerchant;
import com.flutterwave.flutter.core.exception.DatabaseException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author emmanueladeyemi
 */
@Startup
@Singleton
@AccessTimeout(value = 60000)
public class SchedulerServiceNew {

    @EJB
    private UtilityService utilityService;
    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private PosMerchantDao posMerchantDao;

//    @Schedule(hour = "*", minute = "*/45", second = "0", persistent = false)
    public void fetchFailedTransactions() {

        String environment = configurationDao.getConfig("environment");

        if ("test".equalsIgnoreCase(environment)) {
            return;
        }

        Calendar calendar = Calendar.getInstance();

        Date endDate = calendar.getTime();

        calendar.add(Calendar.HOUR, -2);

        Date startDate = calendar.getTime();

        utilityService.fetchFailedNotificationTransationFromProvider(startDate, endDate, null);
    }

//    @Schedule(hour = "*", minute = "*/60", second = "0", persistent = false)
//    public void fetchFailedTransactionsGA() {
//
////        String environment = configurationDao.getConfig("environment");
////            
////        if(Utility.emptyToNull(environment) != null || "test".equalsIgnoreCase(environment) )
////            return;
//        Calendar calendar = Calendar.getInstance();
//
//        Date endDate = calendar.getTime();
//
//        calendar.add(Calendar.HOUR, -2);
//
//        Date startDate = calendar.getTime();
//
//        posService.getTransactionAndSave(startDate, endDate, null);
//    }

    @Schedule(hour = "*", minute = "*/20", second = "0", persistent = false)
    public void sendFailedNoTransactions() {

        String environment = configurationDao.getConfig("environment");

        if ("test".equalsIgnoreCase(environment)) {
            return;
        }

        Calendar calendar = Calendar.getInstance();

        Date endDate = new Date();

        calendar.add(Calendar.MINUTE, -30);
        Date startDate = calendar.getTime();

        utilityService.processFailedNotificationTransaction(startDate, endDate);
    }

    /**
     * *
     * This is called to callback all failed notification for the day
     */
    @Schedule(hour = "0", minute = "5", second = "0", persistent = false)
    public void sendFailedNoTransactionAll() {

        String environment = configurationDao.getConfig("environment");

        if ( "test".equalsIgnoreCase(environment)) {
            return;
        }

        Calendar calendar = Calendar.getInstance();

        Date endDate = new Date();

        calendar.add(Calendar.DAY_OF_MONTH, -1);

        Date startDate = calendar.getTime();

        utilityService.processFailedNotificationTransaction(startDate, endDate);
    }

    @Schedule(hour = "3", minute = "20", second = "0", persistent = false)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendCoreSettlementReport() {

        try {

            String environment = configurationDao.getConfig("allow_settlement_report");
            
            if(environment == null || !"yes".equalsIgnoreCase(environment) )
                return ;
//            
            Calendar calendar = Calendar.getInstance();

            calendar.add(Calendar.DATE, -1);

            Date date = calendar.getTime();

            utilityService.buildAndDropCoreReportToSlack(date);
            
//            List<CoreSettlement> coreSettlements = coreSettlementDao.findSummaryByQuery(date, null, null, null, null);
//
//            List<CoreSettlementMerchant> report = coreSettlementDao.findSummaryMerchantByQuery(date, null, null, null, null);
//            
//            List<CoreSettlementProvider> providerReport = coreSettlementDao.findSummaryProviderByQuery(date, null, null, null, null);
//
//            HSSFWorkbook workbook = new HSSFWorkbook();
//
//            HSSFSheet worksheet = workbook.createSheet("Settlement Transactions");
//
//            int startRowIndex = 0;
//            int startColIndex = 0;
//
//            int usedRows = Layouter1.buildReport(worksheet, startRowIndex, startColIndex, (List) Utility.getClassFields(CoreSettlement.class), "Settlement Report", null);
//
//            FillManager.fillTransactionReport(worksheet, usedRows - 1, startColIndex, "dd/MM/yyyy HH:mm:ss", (List) coreSettlements);
//
//            HSSFSheet worksheetSummary = workbook.createSheet("Settlement Transactions Summary");
//
//            startRowIndex = 0;
//            startColIndex = 0;
//
//            usedRows = Layouter1.buildReport(worksheetSummary, startRowIndex, startColIndex, (List) Utility.getClassFields(CoreSettlementMerchant.class), "Settlement Summary Report", null);
//
//            FillManager.fillTransactionReport(worksheetSummary, usedRows - 1, startColIndex, "dd/MM/yyyy HH:mm:ss", (List) report);
//            
//            HSSFSheet worksheetSummaryProvider = workbook.createSheet("Provider Summary");
//
//            startRowIndex = 0;
//            startColIndex = 0;
//
//            usedRows = Layouter1.buildReport(worksheetSummaryProvider, startRowIndex, startColIndex, (List) Utility.getClassFields(CoreSettlementProvider.class), "Settlment Transaction Summary by Provider", null);
//
//            FillManager.fillTransactionReport(worksheetSummaryProvider, usedRows - 1, startColIndex, "dd/MM/yyyy HH:mm:ss", (List) providerReport);
//
//            System.out.println("pushing core report");
//            
//            OutputStream out = null;
//            try {
//
//                File tempFile = File.createTempFile("Settlement_" + Utility.formatDate(date, "yyyyMMdd") + "_transaction_", ".xls");
//
//                out = new FileOutputStream(tempFile);
//                workbook.write(out);
//
//                String token = configurationDao.getConfig("settlement_slack_api_key");
//                String channel = configurationDao.getConfig("settlement_slack_api_channel");
//
//                notificationManager.uploadToSlack(tempFile, "Settlement Report for " + Utility.formatDate(date, "dd-MM-yyyy"), token, channel);
//
//            } catch (IOException err) {
//                err.printStackTrace();
//            } finally {
//                try {
//                    if (out != null) {
//                        out.close();
//                    }
//                } catch (IOException err) {
//                    err.printStackTrace();
//                }
//            }
        } catch (Exception ex) {
            Logger.getLogger(SchedulerServiceNew.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Schedule(hour = "2", minute = "10", second = "0", persistent = false)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendPosSettlement(){
        
        String environment = configurationDao.getConfig("environment");
        
        if("test".equalsIgnoreCase(environment))
                return ;
        
        String posIds = configurationDao.getConfig("pos_settlement_merchant");
        
        if(posIds == null)
            return;
        
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
        Date startDate = calendar.getTime();
        
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 59);
        
       Date endDate = calendar.getTime();
        
        String[] merchantList = posIds.split(",");
        
        for(String mchant : merchantList){
            
            PosMerchant posMerchant = null;
            try {
                posMerchant = posMerchantDao.findByKey("posId", mchant);
            } catch (DatabaseException ex) {
                Logger.getLogger(SchedulerServiceNew.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if(posMerchant == null)
                continue;
            
            utilityService.sendPOSReport(posMerchant, startDate, endDate, posMerchant.getEmail());
        }
    }
}

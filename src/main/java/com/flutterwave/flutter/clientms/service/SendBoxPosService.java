/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.flutterwave.flutter.clientms.model.PosTransactionNew;
import com.flutterwave.flutter.clientms.service.model.SendBoxQueryModel;
import com.flutterwave.flutter.clientms.util.CryptoUtil;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.Utility;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class SendBoxPosService {
    
    @Inject
    private HttpUtil httpUtil;
    
    public SendBoxQueryModel validateTransaction(String reference){
        
        Properties  properties = Utility.getConfigProperty();
        
        String hashKey = properties.getProperty("sendbox_hash_key");
        
        String baseUrl = properties.getProperty("sendbox_base_url");
        
        String hashValue = CryptoUtil.hmacSHA512(reference.getBytes(), hashKey.getBytes());
        
        JSONObject jSONObject = new JSONObject()
                .put("referenceID", reference)
                .put("Hash", hashValue);
        
        Map<String, String> header = new HashMap<>();
        header.put("content-type", "application/json");
        
        String response = httpUtil.doHttpPost(baseUrl+"/ussd_validate_reference", jSONObject.toString(), header);
        
        if(response == null)
            return null;
        
        jSONObject = new JSONObject(response);
        
        String responseCode = jSONObject.optString("responseCode");
        
        if(!"0".equalsIgnoreCase(responseCode) && !"00".equalsIgnoreCase(responseCode))
            return null;
        
        SendBoxQueryModel queryModel = new SendBoxQueryModel();
        queryModel.setCurrency(jSONObject.optString("Currency"));
        queryModel.setCustomerName(jSONObject.optString("CustomerName"));
        queryModel.setReferenceId(jSONObject.optString("referenceID"));
        queryModel.setResponseCode(jSONObject.optString("responseCode"));
        queryModel.setStatusMessage(jSONObject.optString("statusMessage"));
        queryModel.setTotalAmount(jSONObject.optString("totalAmount"));
        queryModel.setStatusCode(responseCode);

        return queryModel;
    }
   
    public boolean logTransaction(PosTransactionNew posTransactionNew){
        
        Properties  properties = Utility.getConfigProperty();
        
        String hashKey = properties.getProperty("sendbox_hash_key");
        
        String baseUrl = properties.getProperty("sendbox_base_url");
        
        
        String hashData =  posTransactionNew.getTraceref()+""+posTransactionNew.getRrn()+""+posTransactionNew.getAmount();
        
        String hashValue = CryptoUtil.hmacSHA512(hashData.getBytes(), hashKey.getBytes());
        
        JSONObject jSONObject = new JSONObject()
                .put("referenceID", posTransactionNew.getTraceref())
                .put("totalAmount", String.valueOf(posTransactionNew.getAmount()))
                .put("transReference", posTransactionNew.getRrn())
                .put("Currency", "NGN")
                .put("Hash", hashValue);
        
        Map<String, String> header = new HashMap<>();
        header.put("content-type", "application/json");
        
        String response = httpUtil.doHttpPost(baseUrl+"/pos_payment_update", jSONObject.toString(), header);
        
        if(response == null)
            return false;
        
        jSONObject = new JSONObject(response);
        
        String responseCode = jSONObject.optString("responseCode");
        
        return !(!"0".equalsIgnoreCase(responseCode) && !"00".equalsIgnoreCase(responseCode));
        
    }
    
}

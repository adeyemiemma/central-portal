/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service.recon;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.imageio.ImageIO;
import org.apache.poi.util.IOUtils;

/**
 *
 * @author emmanueladeyemi
 */
@Startup
@Singleton
public class ImageUploader {
    
    public static String IMAGE_FOLDER = "ars_image";
    
    @PostConstruct
    void init(){
        
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);
        executorService.scheduleAtFixedRate(new FileManager(), 0 , 1 , TimeUnit.HOURS);
    }
    
    
    public boolean saveImage(InputStream inputStream, String fileName){
        
        try {
            File file = new File(IMAGE_FOLDER+"/"+fileName);
            
//            if(file.exists())
//                file.delete();
            
//            file.createNewFile();
            
            Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
//            IOUtils.copy(inputStream, new FileOutputStream(file));
            
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ImageUploader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    public BufferedImage getImage(String filename){
        
        File file = new File(filename);
        
        if(!file.exists())
            return null;
        
        
//        Files.re
        
        return null;
    }
    
    private class FileManager implements Runnable{

        @Override
        public void run() {
            
            createOutputFolder();
        }
        
        private boolean createOutputFolder(){
        
        try {
                if(IMAGE_FOLDER == null)
                    return false;

                File file =new File(IMAGE_FOLDER);

                if(file.exists()){

                    String filePath = file.getAbsoluteFile().getAbsolutePath();
                    return true;
                }

                Path path = Files.createDirectories(file.toPath(), new FileAttribute<?>[0]);

                if(path == null){
                    System.out.println("Unable to create files");
                    return false;
                }

                //System.out.println("");

                return true;
            } catch (IOException ex) {
            }
        
            return false;
        }
    }
}

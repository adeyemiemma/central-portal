/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
//import com.flutterwave.flutter.clientms.dao.TransactionNewDao;
//import com.flutterwave.flutter.clientms.dao.TransactionWarehouseDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.util.LogDomain;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogState;
import com.flutterwave.flutter.clientms.util.LogStatus;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.jms.Connection;
import javax.jms.Session;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

/**
 *
 * @author emmanueladeyemi
 */
@Startup
@Singleton
public class AWSDeadSuccessTransactionQueueListenerManager {

//    private final static String URL_STRING = "tcp://localhost:61616";
    SQSConnectionFactory connectionFactory;

    private final static String QUEUE_NAME = "SETTLEMENT_SUCCESSFUL_TRANSACTION_QUEUE_2";

    Session session;
    Connection connection;

    Logger logger = Logger.getLogger(AWSDeadSuccessTransactionQueueListenerManager.class);

    @EJB
    private LogService logService;

    @EJB
    private ConfigurationDao configurationDao;
    @EJB
    private AWSQueueHelper aWSQueueHelper;

    @Resource
    private ManagedExecutorService executorService;
    
    Map<String, Future<Object>> futureTracker = new ConcurrentHashMap<>();
    

    boolean state = true;
   
    @Lock(LockType.WRITE)
    public void startNewService(){
        
        String environment = configurationDao.getConfig("environment");

        if ("test".equalsIgnoreCase(environment)) {
            return;
        }
        
        int size = futureTracker.size();
        
        size++;
        
        String name = "QUEUE-"+size;
        
        Future future = executorService.submit(new MyQueueRunner(name));
        
        futureTracker.put(name, future); 
    }
    
    @Lock(LockType.WRITE)
    public void stopAll(){
        
        futureTracker.forEach((name,tracker) -> {
        
            try{
                
                logger.info("Cancelling Thread "+name);
                
                if(tracker.isDone() == false &&  tracker.isCancelled() == false)
                    tracker.cancel(true);
                
            }catch(Exception ex){
                logger.error(ex);
            }
        });
    }
    
    class MyQueueRunner implements Runnable {

        String name;
        
        public MyQueueRunner(){
        }
        
        public MyQueueRunner(String name){
            this.name = name;
        }
        
        @Override
        public void run() {

            try {

                logger.info("STARTING A NEW QUEUE "+name);
                String url = configurationDao.getConfig("aws_airtime_queue");

                if (url == null) {
                    url = "https://sqs.eu-west-2.amazonaws.com";
                }

                String accessKey = configurationDao.getConfig("aws_access_key");

                if (accessKey == null) {
                    accessKey = "AKIAJ3CVNCSR5SKEUGYQ";
                }

                String secretKey = configurationDao.getConfig("aws_secret_key");

                if (secretKey == null) {
                    secretKey = "wR2HYAdVQeJ0waLd9vzkAg4tNhFezsCcY3sRdur0";
                }

                BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

                AmazonSQS sqs = new AmazonSQSClient(credentials);

                sqs.setEndpoint(url);

                GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest(QUEUE_NAME);

                GetQueueUrlResult queueUrlResult = sqs.getQueueUrl(getQueueUrlRequest);

                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrlResult.getQueueUrl());
                receiveMessageRequest.setMaxNumberOfMessages(10);
                receiveMessageRequest.setWaitTimeSeconds(4);

                while (state == true) {

                    List<com.amazonaws.services.sqs.model.Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();

                    for (com.amazonaws.services.sqs.model.Message message : messages) {

                        System.out.println("Received Success New T : " + message.getBody());

                        Log log = new Log();
                        log.setAction("New Success Transaction Queue Called back SQS");
                        log.setCreatedOn(new Date());
                        log.setLevel(LogLevel.Info);
                        log.setLogDomain(LogDomain.ADMIN);
                        log.setDescription(message.toString());
                        log.setLogState(LogState.STARTED);
                        log.setStatus(LogStatus.SUCCESSFUL);
                        
                        logService.log(log);

                        aWSQueueHelper.onMessage(message, queueUrlResult.getQueueUrl(), sqs);

                    }

                    try {
                        Thread.sleep(1000);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            } catch (Exception ex) {
                Logger.getLogger(AWSDeadSuccessTransactionQueueListenerManager.class.getName()).log(Priority.ERROR, ex);

                try {
                    if (state == true) {
                        executorService.execute(this);
                    }
                } catch (Exception e) {

                }
//            tidyUpAndRestart();
            }

        }
    }

    @PreDestroy
    public void tidyUp() {
        state = false;
        
        
        futureTracker.forEach((name,tracker) -> {
        
            try{
                if(tracker.isDone() == false &&  tracker.isCancelled() == false)
                    tracker.cancel(true);
            }catch(Exception ex){
                logger.error(ex);
            }
        });
        
    }

}

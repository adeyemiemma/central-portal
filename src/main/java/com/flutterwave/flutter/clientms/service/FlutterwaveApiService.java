/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

import com.flutterwave.flutter.clientms.dao.ConfigurationDao;
import com.flutterwave.flutter.clientms.model.Log;
import com.flutterwave.flutter.clientms.model.cardcharge.ChargeCard;
import com.flutterwave.flutter.clientms.util.CryptoUtil;
import com.flutterwave.flutter.clientms.util.HttpUtil;
import com.flutterwave.flutter.clientms.util.LogLevel;
import com.flutterwave.flutter.clientms.util.LogStatus;
import com.flutterwave.flutter.clientms.util.Utility;
import com.flutterwave.flutter.clientms.viewmodel.BvnResponseModel;
import com.flutterwave.flutter.clientms.viewmodel.CoreV2TransactionViewModel;
import com.flutterwave.flutter.core.util.PageResult;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanueladeyemi
 */
@Stateless
public class FlutterwaveApiService {

    @EJB
    private LogService logService;
    @EJB
    private HttpUtil httpUtil;
    @EJB
    private ConfigurationDao configurationDao;

    private static final String HOST = " http://staging1flutterwave.co:8080/pwc/rest";
    private static final String API_KEY = "tk_AzxcrhuULmnpDKf5WjE4";
    private static final String MERCHANT_KEY = "tk_h03GDKCVJA";

    public String processCardCharge(ChargeCard chargeCard) {

//        HttpURLConnection connection = (HttpURLConnection) (new URL(HOST+"/card/mvva/pay")).openConnection();
//            
//        connection.setDoOutput(true);
//        connection.setDoInput(true);
//        connection.setUseCaches(false);
//        connection.setRequestMethod("POST");
//        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//
//        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
//
//        Stream.of(new String[]{}).forEach(x->{
//            arrayBuilder.add(x);
//        });
//
//        JsonArray jsonArray = arrayBuilder.build();
//
//
////        {
////"amount": "Encrypted Amount",
////"authmodel": "Encrypted Authmodel (PIN | BVN | RANDOM_DEBIT | VBVSECURECODE | NOAUTH)",
////"cardno": "Encrypted Card Number",
////"currency": "Encrypted Currency",
////"custid": "Encrypted CustID",
////"country": "(Optional: Default is NG, see below for other options) Encrypted Country",
////"cvv": "Encrypted CVV",
////"pin": "(Optional:Only needed where authmodel is PIN) Encrypted PIN",
////"bvn": "(Optional:Only needed where authmodel is BVN) Encrypted BVN",
////"cardtype":" (Optional: Only needed where card is a Diamond Bank card) Encrypted CardType (Diamond) ",
////"expirymonth": "Encrypted Expiry Month",
////"expiryyear": "Encrypted Expiry Year",
////"merchantid": "Merchant Key",
////"narration": "Encrypted Narration",
////"responseurl": "(Optional:Only needed where authmodel is VBVSECURECODE) Encrypted Response URL"
////}
//        
//        JsonObject object = Json.createObjectBuilder()
//                .add("amount", CryptoUtil.harden(API_KEY, chargeCard.getAmount()))
//                .add("authmodel", CryptoUtil.harden(API_KEY, chargeCard.getAuthModel()))
//                .add("cardno", CryptoUtil.harden(API_KEY, chargeCard.getCardno()))
//                .add("currency", CryptoUtil.harden(API_KEY, chargeCard.getCurrency()))
//                .add("currency", CryptoUtil.harden(API_KEY, chargeCard.getCurrency()))
//                .add("token", token).build();
//
//        Log log = new Log();
//        log.setAction("Pulling limits from core request");
//        log.setCreatedOn(new Date());
//        log.setDescription(content);
//        log.setLevel(LogLevel.Info);
//        log.setStatus(LogStatus.SUCCESSFUL);
//        log.setStatus(LogStatus.SUCCESSFUL);
        return null;
    }

    public boolean accountEnquiry(String accountNo, String bankCode) {

        try {
            HttpURLConnection connection = (HttpURLConnection) (new URL(HOST + "/pay/resolveaccount")).openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            Stream.of(new String[]{}).forEach(x -> {
                arrayBuilder.add(x);
            });

            JsonObject object = Json.createObjectBuilder()
                    .add("destbankcode", CryptoUtil.harden(API_KEY, bankCode))
                    .add("recipientaccount", CryptoUtil.harden(API_KEY, accountNo))
                    .add("merchantid", MERCHANT_KEY).build();

            Log log = new Log();
            log.setAction("Account status query");
            log.setCreatedOn(new Date());
            log.setDescription(object.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            byte[] postData = object.toString().getBytes();
            OutputStream os = connection.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            long resultCode = (long) connection.getResponseCode();

            if (resultCode != 200 && resultCode != 201) {
                throw new Exception("Server not available");
            }

//            String result = null;
            InputStream is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            JsonObject response = Json.createReader(is).readObject();

            log = new Log();
            log.setAction("Account status query");
            log.setCreatedOn(new Date());
            log.setDescription(response.toString());
            log.setLevel(LogLevel.Info);
            log.setStatus(LogStatus.SUCCESSFUL);

            JsonObject data = response.getJsonObject("data");

            String responseCode = data.getString("responsecode", null);

            return "00".equalsIgnoreCase(responseCode);

        } catch (Exception ex) {

            return false;
        }
    }

    public PageResult<CoreV2TransactionViewModel> getTransactions(String userToken,
            Date startDate, Date endDate, int pageNo, int pageSize, String currency, String status) {

        String baseUrl = configurationDao.getConfig("core_pos_base_url");

        String environment = configurationDao.getConfig("environment");

        if (environment == null || baseUrl == null) {
            baseUrl = "https://flutterwaveprodv2.com";
        }

        String startDateString = Utility.formatDate(startDate, "yyyy-MM-dd");
        String endDateString = Utility.formatDate(startDate, "yyyy-MM-dd");

        JSONObject jSONObject = new JSONObject()
                .put("user", userToken)
                .put("currency", currency)
                .put("status", status)
                .put("page", pageNo)
                .put("from", startDateString)
                .put("to", endDateString)
                .put("size", pageSize);

        String url = baseUrl + "/TransactionStatus/flwv/dashboard/transaction";

        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");

        String response = httpUtil.doHttpPost(url, jSONObject.toString(), header);

        if (response == null) {
            return new PageResult<>(new ArrayList<>(), 0L, 0L);
        }

        JSONObject responseObject = new JSONObject(response);

        String queryStatus = responseObject.optString("status");

        if (!"success".equalsIgnoreCase(queryStatus)) {
            return new PageResult<>(new ArrayList<>(), 0L, 0L);
        }

        JSONObject dataObject = responseObject.optJSONObject("data");

        if (dataObject == null) {
            return new PageResult<>(new ArrayList<>(), 0L, 0L);
        }

        int total = dataObject.optInt("total");

        JSONArray reportArray = dataObject.optJSONArray("report");

        List<CoreV2TransactionViewModel> transactions = new ArrayList<>();

        for (int i = 0; i < reportArray.length(); i++) {

            JSONObject nObject = reportArray.optJSONObject(i);

            if (nObject == null) {
                continue;
            }

            CoreV2TransactionViewModel model = getTransaction(nObject);

            transactions.add(model);
        }

        long totatLong = Long.parseLong(total + "");

        PageResult<CoreV2TransactionViewModel> pageResult = new PageResult<>(transactions, totatLong, totatLong);

        return pageResult;
    }

    public BvnResponseModel validateBVN(String bvn) {

        try {
            BvnResponseModel responseModel = new BvnResponseModel();

            Properties properties = Utility.getConfigProperty();

            String key = properties.getProperty("core_merchant_bvn_key");//("core_merchant_bvn_key");
            String url = properties.getProperty("core_merchant_bvn_url");

            if (url == null) {
                return null;
            }

            url += "/" + bvn;

            Map<String, String> header = new HashMap<>();
            header.put("Authorization", "Basic " + key);

            String response = httpUtil.doHttpGet(url, header);

            if (response == null) {
                return null;
            }

            JSONObject jSONObject = new JSONObject(response);

            JSONObject dataObject = jSONObject.optJSONObject("data");

            String responseCode = dataObject.optString("responseCode");

            if (!"00".equalsIgnoreCase(responseCode)) {
                return null;
            }

            responseModel.setBvn(dataObject.optString("bvn"));
            responseModel.setEnrollmentBank(dataObject.optString("enrollmentBank"));
            responseModel.setEnrollmentBranch(dataObject.optString("enrollmentBranch"));
            responseModel.setFirstName(dataObject.optString("firstName"));
            responseModel.setLastName(dataObject.optString("lastName"));
            responseModel.setMiddleName(dataObject.optString("middleName"));
            responseModel.setRegistrationDate(dataObject.optString("registrationDate"));

            return responseModel;
        } catch (Exception ex) {
            ex.printStackTrace();;
        }
        
        return null;
    }

    public CoreV2TransactionViewModel getTransaction(String reference) {

        if (reference == null) {
            return null;
        }

        String baseUrl = configurationDao.getConfig("core_pos_base_url");

        String environment = configurationDao.getConfig("environment");

        if (environment == null || baseUrl == null) {
            baseUrl = "https://flutterwaveprodv2.com";
        }

        String url = baseUrl + "/TransactionStatus/flwv/dashboard/transaction/";

        if (reference.contains("/")) {
            reference = reference.substring(reference.indexOf("/"));
        }

        url += "" + reference;

        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");

        String response = httpUtil.doHttpGet(url, header);

        if (response == null) {
            return null;
        }

        JSONObject jSONObject = new JSONObject(response);

        String status = jSONObject.optString("status");

        if (!"success".equalsIgnoreCase(status)) {
            return null;
        }

        JSONObject dataObject = jSONObject.optJSONObject("data");

        return getTransaction(dataObject);
    }

    public CoreV2TransactionViewModel getTransaction(JSONObject object) {

        if (object == null) {
            return null;
        }

        CoreV2TransactionViewModel model = new CoreV2TransactionViewModel();
        model.setAccountNumber(object.optString("accountNumber"));
        model.setAmount(object.optDouble("amount"));
        model.setApiType(object.optString("apiType"));
        model.setBvn(object.optString("bvn"));
        model.setCallbackUrl(object.optString("callbackUrl"));
        model.setCardNo(object.optString("cardno"));
        model.setCurrency(object.optString("currency"));
        model.setCustomerName(object.optString("customerName"));
        model.setCustomerPhoneno(object.optString("customerPhoneno"));
        model.setDateOfTransaction(object.optString("dateOfTransaction"));
        model.setEmail(object.optString("email"));
        model.setMerchantId(object.optString("merchantId"));
        model.setMerchantName(object.optString("merchantName"));
        model.setMethod(object.optString("method"));
        model.setNarration(object.optString("narration"));
        model.setPan(object.optString("pan"));
        model.setProvider(object.optString("provider"));
        model.setPwcmerchantId(object.optString("pwcmerchantId"));
        model.setRequestStatus(object.optString("requestStatus"));
        model.setResponseDate(object.optString("responseDate"));
        model.setStatus(object.optString("status"));
        model.setStatuscode(object.optString("statuscode"));
        model.setTransactionIdentifier(object.optString("transactionIdentifier"));
        model.setTransactionReference(object.optString("transactionReference"));
        model.setTransactiontoken(object.optString("transactiontoken"));
        model.setUsertoken(object.optString("usertoken"));
        model.setValidateOption(object.optString("validateOption"));
        model.setFee(object.optDouble("fee"));
        model.setFeeValue(object.optDouble("feevalue"));
        model.setSourceBank(object.optString("frombank"));
        model.setBeneficiaryBank(object.optString("tobank"));
        model.setChannel(object.optString("channel"));
        model.setRecipientName(object.optString("recipientName"));
        model.setCustomerReference(object.optString("custidref"));

        return model;
    }
}

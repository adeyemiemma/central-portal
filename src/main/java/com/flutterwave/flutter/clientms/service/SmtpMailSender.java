/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flutterwave.flutter.clientms.service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.flutterwave.flutter.clientms.util.Global;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

/**
 *
 * @modified emmanueladeyemi
 */
public class SmtpMailSender {

    private final Properties properties;
    private final Session session;
    
    static Logger logger = Logger.getLogger(SmtpMailSender.class);

    public SmtpMailSender(final String smtpUsername, final String smtpPassword, String smtpHost, int smtpPort, boolean isSmtpAuth, boolean isSmtpTls) {
        properties = new Properties();
        properties.put("mail.smtp.auth", Boolean.toString(isSmtpAuth));
        
        if(isSmtpTls == true)
            properties.put("mail.smtp.starttls.enable", Boolean.toString(isSmtpTls));
        
        properties.put("mail.smtp.host", smtpHost.trim());
        properties.put("mail.smtp.port", Integer.toString(smtpPort));
        
//        if(port)
        properties.put("mail.smtp.socketFactory.port", smtpPort); //SSL Port
        properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        Authenticator authenticator = null;
        if (isSmtpAuth) {
            authenticator = new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpUsername.trim(), smtpPassword.trim());
                }
            };
        }
        session = Session.getInstance(properties, authenticator);
    }
    
    public SmtpMailSender(final String smtpUsername, final String smtpPassword, 
            String smtpHost, int smtpPort, boolean isSmtpAuth, boolean isSmtpTls,
            boolean useSSL) {
        
        properties = new Properties();
        properties.put("mail.smtp.auth", Boolean.toString(isSmtpAuth));
        
        if(isSmtpTls == true)
            properties.put("mail.smtp.starttls.enable", Boolean.toString(isSmtpTls));
        
        properties.put("mail.smtp.host", smtpHost.trim());
        properties.put("mail.smtp.port", Integer.toString(smtpPort));
        
        if(useSSL == true){
            properties.put("mail.smtp.socketFactory.port", smtpPort); //SSL Port
            properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        }
        
        properties.put("mail.smtp.socketFactory.fallback", "true");
        
        Authenticator authenticator = null;
        if (isSmtpAuth) {
            authenticator = new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpUsername.trim(), smtpPassword.trim());
                }
            };
        }
        session = Session.getInstance(properties, authenticator);
    }

    public void sendTextMail(String from, String subject, String messageBody, String[] recipients) throws MessagingException {

        InternetAddress[] recipientAddress = new InternetAddress[recipients.length];
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        int count = 0;
        for (String recipent : recipients) {
            recipientAddress[count] = new InternetAddress(recipent.trim());
            count++;
        }
        message.setRecipients(Message.RecipientType.TO, recipientAddress);
        message.setSubject(subject);
        message.setText(messageBody);
        Transport.send(message);

    }
    
    public void sendHtmlMail(String from, String title, String subject, String messageBody, String[] recipients) throws MessagingException{
        
//        InternetAddress[] recipientAddresses = new InternetAddress[recipients.length];
        
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        
         List<InternetAddress> repAddress = new ArrayList();
         
        try{
            message.setFrom(new InternetAddress(from, title));
        }catch(UnsupportedEncodingException ex){
            message.setFrom(new InternetAddress(from));
        }
            
        InternetAddress[] recipientAddresses = stringAddressToInternetAddress(recipients);
        
        message.setRecipients(Message.RecipientType.TO, recipientAddresses);
        message.setSubject(subject);
        
        message.setContent(messageBody, "text/html");
        
        Transport.send(message);
    }
    
    
    /**
     * This is used to send html emails
     * @param from  this is the sender text
     * @param fromEmail - this is the email address of the sender
     * @param subject - this is the subject of the su
     * @param messageBody
     * @param recipients
     * @param ccRecipients
     * @param bccRecipients
     * @throws MessagingException 
     */
    public void sendHtmlMail(String from, String fromEmail, String subject, String messageBody, String[] recipients, 
            String[] ccRecipients, String[] bccRecipients) throws MessagingException{
        
        
        List<InternetAddress> repAddress = new ArrayList();
        
        
        MimeMessage message = new MimeMessage(session);
        
        try {
            message.setFrom(new InternetAddress(fromEmail,from));
        } catch (UnsupportedEncodingException ex) {
            logger.error(ex);
            message.setFrom(new InternetAddress(fromEmail));
        }
        
        InternetAddress[] recipientAddresses = stringAddressToInternetAddress(recipients);

        message.setRecipients(Message.RecipientType.TO, recipientAddresses);
        
        message.setSubject(subject);
        
        if(ccRecipients != null)
            if(ccRecipients.length > 0){

                InternetAddress[] ccRecipientAddresses = stringAddressToInternetAddress(ccRecipients);

                message.setRecipients(Message.RecipientType.CC, ccRecipientAddresses);

            }
        
        if(bccRecipients != null)
            if(bccRecipients.length > 0){

                InternetAddress[] ccRecipientAddresses = stringAddressToInternetAddress(bccRecipients);

                message.setRecipients(Message.RecipientType.BCC, ccRecipientAddresses);

            }
        
        message.setContent(messageBody, "text/html");
        
        Transport.send(message);
    }
    
    public InternetAddress[] stringAddressToInternetAddress(String[] recipients){
        
        try{
            List<InternetAddress> repAddress = new ArrayList();

            for (String recipent : recipients) {

                if(recipent == null || recipent.equalsIgnoreCase("null"))
                    continue;

                InternetAddress address = new InternetAddress(recipent.trim());
                repAddress.add(address);

            }

            InternetAddress[] recipientAddresses = new InternetAddress[repAddress.size()]; 

            for(int i= 0; i < repAddress.size(); i++){
                recipientAddresses[i] = repAddress.get(i);
            }
            
            return recipientAddresses;
        }catch(AddressException ex){
        
        }
        
        return null;
    }
    
    public void sendHtmlMail(String from, String fromEmail, String subject, String messageBody, String[] recipients, 
            String[] ccRecipients, String[] bccRecipients, String attachmentPath) throws MessagingException, IOException{
        
        MimeMessage message = new MimeMessage(session);
        
        try {
            message.setFrom(new InternetAddress(fromEmail,from));
        } catch (UnsupportedEncodingException ex) {
            logger.error(ex);
            message.setFrom(new InternetAddress(fromEmail));
        }
        
        InternetAddress[] recipientAddresses = stringAddressToInternetAddress(recipients);

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(messageBody, "text/html");
        
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
        
        message.setRecipients(Message.RecipientType.TO, recipientAddresses);
        
        message.setSubject(subject);
        message.setSentDate(new Date());
        
        
        if(ccRecipients != null)
            if(ccRecipients.length > 0){

                InternetAddress[] ccRecipientAddresses = stringAddressToInternetAddress(ccRecipients);

                message.setRecipients(Message.RecipientType.CC, ccRecipientAddresses);

            }
        
        if(bccRecipients != null)
            if(bccRecipients.length > 0){

                InternetAddress[] ccRecipientAddresses = stringAddressToInternetAddress(bccRecipients);

                message.setRecipients(Message.RecipientType.BCC, ccRecipientAddresses);

            }
        
        // This is the section where attachment is added to the file
        if(attachmentPath != null && !"".equalsIgnoreCase(attachmentPath)){
            MimeBodyPart attachPart = new MimeBodyPart();
            attachPart.attachFile(attachmentPath);
            
            multipart.addBodyPart(attachPart);
        }
        
        message.setContent(multipart);
        
        Transport.send(message);
    }
    
    public static void main(String[] args){
        
        String username = Global.EMAIL_USERNAME;
        String password = Global.EMAIL_PASSWORD;
    
        SmtpMailSender mailSender = new SmtpMailSender(username, password, Global.EMAIL_SERVER, Global.EMAIL_PORT, Global.USE_STARTTLS, false, false);
        try {
            File file = File.createTempFile("Test", ".txt");
            
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write("This is a simple text".getBytes());
            
            fileOutputStream.close();
            
            mailSender.sendHtmlMail("Flutterwave Compliance Team", "account@flutterwavego.com", "Flutterwave Compliance", "Test Me", new String[]{"emmanuel@flutterwavego.com"}, null, null, file);
            
            file.delete();
            
        } catch (MessagingException ex) {
            logger.error(ex);
        } catch (Exception ex) {
            logger.error(ex);
        }
    }
    
    public void sendHtmlMail(String from, String fromEmail, String subject, String messageBody, String[] recipients, 
            String[] ccRecipients, String[] bccRecipients, File file) throws MessagingException, IOException{
        
        MimeMessage message = new MimeMessage(session);
        
        try {
            message.setFrom(new InternetAddress(fromEmail,from));
        } catch (UnsupportedEncodingException ex) {
            logger.error(ex);
            message.setFrom(new InternetAddress(fromEmail));
        }
        
        InternetAddress[] recipientAddresses = stringAddressToInternetAddress(recipients);

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(messageBody, "text/html");
        
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
        
        message.setRecipients(Message.RecipientType.TO, recipientAddresses);
        
        message.setSubject(subject);
        message.setSentDate(new Date());
        
        
        if(ccRecipients != null)
            if(ccRecipients.length > 0){

                InternetAddress[] ccRecipientAddresses = stringAddressToInternetAddress(ccRecipients);

                message.setRecipients(Message.RecipientType.CC, ccRecipientAddresses);

            }
        
        if(bccRecipients != null)
            if(bccRecipients.length > 0){

                InternetAddress[] ccRecipientAddresses = stringAddressToInternetAddress(bccRecipients);

                message.setRecipients(Message.RecipientType.BCC, ccRecipientAddresses);

            }
        
        // This is the section where attachment is added to the file
        if(file != null ){
            MimeBodyPart attachPart = new MimeBodyPart();
            attachPart.attachFile(file);
            attachPart.attachFile(file.getAbsolutePath());
            
            multipart.addBodyPart(attachPart);
        }
        
        message.setContent(multipart);
        
        Transport.send(message);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.rest.application.config;

//import com.flutterwave.flutter.vas.util.ApiResponse;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author emmanueladeyemi
 */
@Provider
public class BeanValUnrecognizedExceptionMapper implements ExceptionMapper<UnrecognizedPropertyException>{

    @Override
    public Response toResponse(UnrecognizedPropertyException e) {
        
        Map<String, String> error = new HashMap<>();
        error.put("status", "failed");
        
//        String message = Stream.of(e.getPropertyName().toArray(new ConstraintViolation[]{})).map(x-> x.getMessage()).collect(Collectors.joining(" \n "));
//        ConstraintViolation cv = (ConstraintViolation) e.getConstraintViolations().toArray()[0];
        //oh yeah... you need to shell out some $$$ !
 
        error.put("description", "field '"+e.getPropertyName()+"' is not permitted in this context");
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(error)
                .build();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.rest.application.config;

import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author emmanueladeyemi
 */
@Provider
public class BeanValConstrainViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException>{

    @Override
    public Response toResponse(ConstraintViolationException e) {
//        System.out.println("BeanValConstrainViolationExceptionMapper in action");

        ConstraintViolation cv = (ConstraintViolation) e.getConstraintViolations().toArray()[0];
        //oh yeah... you need to shell out some $$$ !
        
        Map<String, String> error = new HashMap<>();
        
        error.put("responsecode", "P01");
        error.put("responsemessage", cv.getMessage());
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(error)
                .build();
    }
    
}

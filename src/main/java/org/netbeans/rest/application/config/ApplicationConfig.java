/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.rest.application.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author emmanueladeyemi
 */
@javax.ws.rs.ApplicationPath("/api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.flutterwave.flutter.clientms.controller.api.AirtimeApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.ApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.ApiControllerNew.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.CallBackApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.ChargeApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.ConfigurationApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.ExternalApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.PosApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.RefundApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.ReportApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.RewardApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.SettlementApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.SmsApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.TransactionApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.UtilApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.WebApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.recon.ReconReportApiController.class);
        resources.add(com.flutterwave.flutter.clientms.controller.api.recon.ReconTransactionApiController.class);
        resources.add(org.netbeans.rest.application.config.BeanValConstrainViolationExceptionMapper.class);
        resources.add(org.netbeans.rest.application.config.BeanValUnrecognizedExceptionMapper.class);
        resources.add(org.netbeans.rest.application.config.CustomerRequestFilter.class);
        resources.add(org.netbeans.rest.application.config.CustomerResponseFilter.class);
        resources.add(org.netbeans.rest.application.config.MyJacksonJsonProvider.class);
    }
    
}

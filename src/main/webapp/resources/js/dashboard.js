/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function(){
    Morris.Area({
        element: "morris-likes"
        , data: [{
            y: "1"
            , a: 14
        }, {
            y: "2"
            , a: 12
        }, {
            y: "3"
            , a: 4
        }, {
            y: "4"
            , a: 9
        }, {
            y: "5"
            , a: 3
        }, {
            y: "6"
            , a: 6
        }, {
            y: "7"
            , a: 11
        }, {
            y: "8"
            , a: 10
        }, {
            y: "9"
            , a: 13
        }, {
            y: "10"
            , a: 9
        }, {
            y: "11"
            , a: 14
        }, {
            y: "12"
            , a: 11
        }, {
            y: "13"
            , a: 16
        }, {
            y: "14"
            , a: 20
        }, {
            y: "15"
            , a: 15
        }]
        , xkey: "y"
        , ykeys: ["a"]
        , labels: ["Likes"]
        , axes: !1
        , grid: !1
        , behaveLikeLine: !0
        , ymax: 20
        , resize: !0
        , pointSize: 0
        , smooth: !0
        , numLines: 6
        , lineWidth: 2
        , fillOpacity: .1
        , lineColors: ["#967ADC"]
        , hideHover: !0
        , hoverCallback: function (a, b, c, d) {
            return ""
        }
    }), Morris.Area({
        element: "morris-comments"
        , data: [{
            y: "1"
            , a: 15
        }, {
            y: "2"
            , a: 20
        }, {
            y: "3"
            , a: 16
        }, {
            y: "4"
            , a: 11
        }, {
            y: "5"
            , a: 14
        }, {
            y: "6"
            , a: 9
        }, {
            y: "7"
            , a: 13
        }, {
            y: "8"
            , a: 10
        }, {
            y: "9"
            , a: 11
        }, {
            y: "10"
            , a: 6
        }, {
            y: "11"
            , a: 3
        }, {
            y: "12"
            , a: 9
        }, {
            y: "13"
            , a: 4
        }, {
            y: "14"
            , a: 12
        }, {
            y: "15"
            , a: 14
        }]
        , xkey: "y"
        , ykeys: ["a"]
        , labels: ["Comments"]
        , axes: !1
        , grid: !1
        , behaveLikeLine: !0
        , ymax: 20
        , resize: !0
        , pointSize: 0
        , smooth: !0
        , numLines: 6
        , lineWidth: 2
        , fillOpacity: .1
        , lineColors: ["#37BC9B"]
        , hideHover: !0
        , hoverCallback: function (a, b, c, d) {
            return ""
        }
    }), Morris.Area({
        element: "morris-views"
        , data: [{
            y: "1"
            , a: 14
        }, {
            y: "2"
            , a: 12
        }, {
            y: "3"
            , a: 4
        }, {
            y: "4"
            , a: 9
        }, {
            y: "5"
            , a: 3
        }, {
            y: "6"
            , a: 6
        }, {
            y: "7"
            , a: 11
        }, {
            y: "8"
            , a: 10
        }, {
            y: "9"
            , a: 13
        }, {
            y: "10"
            , a: 9
        }, {
            y: "11"
            , a: 14
        }, {
            y: "12"
            , a: 11
        }, {
            y: "13"
            , a: 16
        }, {
            y: "14"
            , a: 20
        }, {
            y: "15"
            , a: 15
        }]
        , xkey: "y"
        , ykeys: ["a"]
        , labels: ["Views"]
        , axes: !1
        , grid: !1
        , behaveLikeLine: !0
        , ymax: 20
        , resize: !0
        , pointSize: 0
        , smooth: !0
        , numLines: 6
        , lineWidth: 2
        , fillOpacity: .1
        , lineColors: ["#DA4453"]
        , hideHover: !0
        , hoverCallback: function (a, b, c, d) {
            return "";
        }
    });
});

function drawGraph(label, success, failure){
 
    var f = $("#d-graph");
    
    var g = {
            responsive: !0
            , maintainAspectRatio: !1
            , pointDotStrokeWidth: 4
            , legend: {
                display: !1
                , labels: {
                    fontColor: "#FFF"
                    , boxWidth: 10
                }
                , position: "top"
            }
            , hover: {
                mode: "label"
            }
            , scales: {
                xAxes: [{
                    display: !1,
                }]
                , yAxes: [{
                    display: !0
                    , gridLines: {
                        color: "rgba(216,216,216, 0.5)"
                        , drawTicks: 1
                        , drawBorder: 1
                    }
                    , ticks: {
                        display: 1
                        , min: 0
//                        , max: 70
//                        , maxTicksLimit: 4
                    }
                }]
            }
            , title: {
                display: !1
                , text: "Chart.js Line Chart - Legend"
            }
        }
        , h = {
            labels: label
            , datasets: [{
                label: "Failed"
                , data: failure
                , backgroundColor: "transparent"
                , borderColor: "#FF6E40"
                , pointColor: "#fff"
                , pointBorderColor: "#FF6E40"
                , pointBackgroundColor: "#fff"
                , pointBorderWidth: 2
                , pointHoverBorderWidth: 2
                , pointRadius: 3
            }, {
                label: "Success"
                , data: success
                , backgroundColor: "transparent"
                , borderColor: "#1DE9B6"
                , pointColor: "#fff"
                , pointBorderColor: "#1DE9B6"
                , pointBackgroundColor: "#fff"
                , pointBorderWidth: 2
                , pointHoverBorderWidth: 2
                , pointRadius: 3
            }]
        }
        , i = {
            type: "line"
            , options: g
            , data: h
        }
        , j = new Chart(f, i);
        
        currentData = i;
    
}

function getGraphData(ctx){
    
    $.ajax({
       url : ctx+"/api/product/transaction/dashboard/graph" ,
       data : {
           "range" : $("#daterange").val(),
           "currency": $("#currencylistW").val(),
           "product" : $("#product").val()
       }
    }).done(function(data){
        
        var currency = getCurrencySymbol($("#currencylistW").val());
        
        $("#totalvolume").text(data.totalVolume);
        $("#totalsuccess").html(currency+""+amountFormatter(data.totalSuccessful));
        $("#totalvalue").html(currency+" "+amountFormatter(data.totalSum));
        $("#totalfee").html(currency+" "+amountFormatter(data.totalFee));
        
        var labels = [];
        var successful = [];
        var failed = [];
        
        for(var d in data.dateData){
                
            labels.push(moment(d).format("DD-MM-YYYY"));
            successful.push(parseFloat(data.dateData[d].successful).toFixed(2));
            failed.push(parseFloat(data.dateData[d].failed).toFixed(2));
        }
        
        if($("#product").val().toLowerCase() === 'flutterwave core' || $("#product").val().toLowerCase() === 'core'){
            $("#summarypanepie").hide();
            $("#summarypaneline").show();
            
            $("#trend-pane").removeClass('col-xl-8').addClass('col-xl-12');
            
//            drawPie2(dataPieMain, $("#product").val());
            
        }
        else{
            $("#summarypanepie").show();
            $("#summarypaneline").hide();
            
            $("#trend-pane").removeClass('col-xl-12').addClass('col-xl-8');
            
        }
        
        $("#chartjsgraph").append('<canvas id="d-graph" height="500" width="1500" style="display: block; height: 250px;"></canvas>');
        
        drawGraph(labels, successful, failed);
        
        var dataPieMain = [];
        var dataPie ;
//        dataPieMain.push(dataPie);
        
        var lineY = ["Values"];
        var lineX = [];
        
        for(var d in data.statusData){
            
            lineX.push(d);
            lineY.push(parseFloat(data.statusData[d]).toFixed(2));
            
            dataPie = [d , parseFloat(data.statusData[d]).toFixed(2)];
            dataPieMain.push(dataPie);
        }
        
        if($("#product").val().toLowerCase() === 'flutterwave core' || $("#product").val().toLowerCase() === 'core'){
            $("#summarypanepie").hide();
            $("#summarypaneline").show();
            
//            $("#trend-pane").removeClass('col-xl-8').addClass('col-xl-12');
            
//            drawPie2(dataPieMain, $("#product").val());
            
            drawLine(lineX, lineY, $("#product").val(), data.statusCodeMapper );
        }
        else{
            $("#summarypanepie").show();
            $("#summarypaneline").hide();
            
//            $("#trend-pane").removeClass('col-xl-12').addClass('col-xl-8');
            
            drawPie(dataPieMain, $("#product").val());
        }
        
    });
}

function amountFormatter(amount){
    
    return parseFloat(parseFloat(amount).toFixed(2)).toLocaleString();
}

function getGraphSummary(ctx){
    
    $.ajax({
       url : ctx+"/api/product/transaction/dashboard/summary" ,
       data : {
           "range" : $("#dateinterval").val(),
           "currency": $("#currencylist").val()
       }
    }).done(function(data){
        
        var currency = getCurrencySymbol($("#currencylist").val());
        
        $("#moneywavetotal").html(currency + " "+amountFormatter(data.moneywave.failed + data.moneywave.successful));
        $("#moneywavefailed").html(currency+" "+amountFormatter(data.moneywave.failed));
        $("#moneywavesuccessful").html(currency +" "+amountFormatter(data.moneywave.successful));
        
        $("#ravetotal").html(currency + " "+amountFormatter(data.rave.failed + data.rave.successful));
        $("#ravefailed").html(currency+" "+amountFormatter(data.rave.failed));
        $("#ravesuccessful").html(currency +" "+amountFormatter(data.rave.successful));
        
        $("#coretotal").html(currency + " "+amountFormatter(data.core.failed + data.core.successful));
        $("#corefailed").html(currency+" "+amountFormatter(data.core.failed));
        $("#coresuccessful").html(currency +" "+amountFormatter(data.core.successful));
    });
}

function getCurrencySymbol(currency){
    
    switch (currency){
        
        case 'USD' :
            return "&#x24;";
        case 'GBP' :
            return '&#xa3;';
        case 'NGN':
            return "&#x20A6;";
        case 'KES':
            return "KSh"; 
        case 'GHS':
            return "&#8373;";
        default:
            return currency;
    }
}

function drawPie(data, product){
    
    var color = [];
    if(product !== undefined && product.toLowerCase() === 'rave'){
        color = ["#FECEA8", "#c14242", "#c14242", "#613131", "#48771b"];
    }else
        color = [ "#c14242", "#48771b", "#FECEA8", "#613131"];
    
    var a = c3.generate({
        bindto: "#pie-chart"
        , color: {
            pattern: color
        }
        , data: {
            columns: data
            , type: "pie"
        }
    });
}


function drawPie2(data, product){
    
    var color = [];
    if(product !== undefined && product.toLowerCase() === 'rave'){
        color = ["#FECEA8", "#c14242", "#613131", "#48771b"];
    }else
        color = ["#c14242", "#48771b", "#FECEA8", "#613131"];
    
    var a = c3.generate({
        bindto: "#summary-chart"
        , color: {
            pattern: color
        },
        legend: {
            position: 'right'
        }
        , data: {
            columns: data
            , type: "pie"
        }
    });
}

var mapperObject;

function drawLine(dataX, dataY, product, mapper){
    
    var color = [];
//    if(product !== undefined && product.toLowerCase() === 'rave'){
//        color = ["#FECEA8", "#c14242", "#613131", "#48771b"];
//    }else
//        color = ["#FECEA8", "#48771b", "#c14242", "#613131"];
    mapperObject = mapper;
    
    function tooltip_contents(d, defaultTitleFormat, defaultValueFormat, color) {
        var $$ = this, config = $$.config, CLASS = $$.CLASS,
            titleFormat = config.tooltip_format_title || defaultTitleFormat,
            nameFormat = config.tooltip_format_name || function (name) { return name; },
            valueFormat = config.tooltip_format_value || defaultValueFormat,
            text, i, title, value, name, bgcolor;

        // You can access all of data like this:
        //console.log($$.data.targets);
//        console.log(dataX[d[i]]);

        for (i = 0; i < d.length; i++) {
            if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

            if (! text) {
                title = '<span class="small">' + mapperObject[dataX[d[0].x]]+'</span>';
                text = "<table class='" + CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
            }

            name = nameFormat(d[i].name );
            value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
            bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

            text += "<tr class='" + CLASS.tooltipName + "-" + d[i].id + "'>";
            text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
            text += "<td class='value'>" + value + "</td>";
            text += "</tr>";
        }
        return text + "</table>";
    }
    
    var a = c3.generate({
        bindto: "#summary-chart"
        , data: {
            columns: [dataY]
            , type: "spline"
        },
        axis: {
            y : {
                tick: {
                    format: d3.format("s")
                }
            },
            x: {
                type: 'category',
                categories: dataX
            }
        },
        tooltip: {
            contents: tooltip_contents
        }
    });
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function getUnauthFeeInformation(ctx, id) {

    $.ajax({
        url: ctx + "/api/fee/unauth",
        type: 'GET',
        data: {
            "id": id
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

    }).fail(function () {

    });
}



function getFeeInformation(ctx, id) {

    $.ajax({
        url: ctx + "/api/fee/unauth",
        type: 'GET',
        data: {
            "id": id
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

    }).fail(function () {

    });
}

function getFeeInformationAuth(ctx, id) {

    $.ajax({
        url: ctx + "/api/fee",
        type: 'GET',
        data: {
            "id": id
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

    }).fail(function () {

    });
}

function getConfigFeeInformation(ctx, id) {

    $.ajax({
        url: ctx + "/api/config/fee/unauth",
        type: 'GET',
        data: {
            "id": id
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

    }).fail(function () {

    });
}

function getConfigFeeInformationAuth(ctx, id) {

    $.ajax({
        url: ctx + "/api/config/fee",
        type: 'GET',
        data: {
            "id": id
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

    }).fail(function () {

    });
}

function getMerchantInformation(ctx, id, product) {

    $.ajax({
        url: ctx + "/api/merchant/info",
        type: 'GET',
        data: {
            "id": id,
            "product": "" + product
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id' || d === 'generic' || d === 'secret' || d.toLowerCase() === 'password' || d.toLowerCase() === 'testtoken'
                    || d.toLowerCase() === 'testapikey' || d.toLowerCase() === 'liveapikey' || d.toLowerCase() === 'livetoken'
                    || d.toLowerCase() === 'apikey')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'parent') {

                if (data[d].businessName !== null)
                    tempData += "<td>" + (data[d].businessName) + "</td></tr>";
                else if (data[d].name !== undefined)
                    tempData += "<td>" + (data[d].name) + "</td></tr>";
                else
                    tempData += "<td></td></tr>";

                continue;
            }

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "createdAt" || d === "deletedAt" || d === "updatedAt") {
                if (data[d] !== undefined && data[d] !== null)
                    tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";

                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        tempData += "<tr><td>product</td><td>" + product + "</td></tr>";

        $("#tbody").html(tempData);

    }).fail(function () {

    });
}

function getMerchantInformation(ctx, id, product) {

    $.ajax({
        url: ctx + "/api/merchant/info",
        type: 'GET',
        data: {
            "id": id,
            "product": "" + product
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id' || d === 'generic' || d === 'secret' || d.toLowerCase() === 'password' || d.toLowerCase() === 'testtoken'
                    || d.toLowerCase() === 'testapikey' || d.toLowerCase() === 'liveapikey' || d.toLowerCase() === 'livetoken'
                    || d.toLowerCase() === 'apikey')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'parent') {

                if (data[d].businessName !== null)
                    tempData += "<td>" + (data[d].businessName) + "</td></tr>";
                else if (data[d].name !== undefined)
                    tempData += "<td>" + (data[d].name) + "</td></tr>";
                else
                    tempData += "<td></td></tr>";

                continue;
            }

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "createdAt" || d === "deletedAt" || d === "updatedAt") {
                if (data[d] !== undefined && data[d] !== null)
                    tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";

                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        tempData += "<tr><td>product</td><td>" + product + "</td></tr>";

        $("#tbody").html(tempData);

    }).fail(function () {

    });
}

function getMerchantKYCInformation(ctx, id, product, admin) {

    console.log("permission: " + admin);

    $.ajax({
        url: ctx + "/api/merchant/kyc",
        type: 'GET',
        data: {
            "id": id,
            "product": "" + product
        }
    }).done(function (response) {

        var tempData = "";

        var rescode = response.responsecode;

        if (rescode !== "00") {

            $("#complainceModal").modal('hide');

            swal('Status', "Record not found", 'error');
            return;
        }

        var data = response.data;

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";


            if (d === "approvedBy") {
                tempData += "<td>Processed By</td>";
            } else
                tempData += "<td>" + d + "</td>";

            if (d === 'parent') {

                if (data[d].businessName !== null)
                    tempData += "<td>" + (data[d].businessName) + "</td></tr>";
                else if (data[d].name !== undefined)
                    tempData += "<td>" + (data[d].name) + "</td></tr>";
                else
                    tempData += "<td></td></tr>";

                continue;
            }

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "approvedOn") {

                if (admin === undefined || (admin === 'false' || admin === false)) {
                    $("#kycapprovebtn").hide();
                    $("#kycrejectbtn").hide();
                }

                var cstatus = data["status"];

                console.log(cstatus + " status");

                if (data[d] === undefined || data[d] === null || data[d] === "" || (cstatus === "PENDING")) {
                    $("#kycapprovebtn").show();
                    $("#kycrejectbtn").show();
                } else {
                    $("#kycapprovebtn").hide();
                    $("#kycrejectbtn").hide();
                }


                if (cstatus === "UNKNOWN" || cstatus === "APPROVED") {
                    $("#undokycapprovebtn").show();
                    $("#undokycrejectbtn").hide();
                } else if (cstatus === "REJECTED") {
                    $("#undokycapprovebtn").hide();
                    $("#undokycrejectbtn").show();
                } else {

                    $("#undokycapprovebtn").hide();
                    $("#undokycrejectbtn").hide();
                }
            }
            
            if(d === "bvnChecked" && (data[d] === true)){
                $("#verifybvnbtn").hide();
//                console.log("Verify chcked");
            } else if (d === "bvnChecked" && (data[d] === false)){
                $("#verifybvnbtn").show();
                
//                console.log("Verify shown");
            }
            
            if(d === "country" && (data[d] !== "NG")){
                $("#verifybvnbtn").hide();
            }

            if (d === "createdOn" || d === "approvedOn" || d === "lastProcessed" || d === "bvnCheckedOn") {
                if (data[d] !== undefined && data[d] !== null)
                    tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";

                continue;
            }

            if (d.toLowerCase().indexOf('path') >= 0) {

                tempData += "<td><a alt='image' href='" + data[d] + "'>" + data[d] + "</a></td></tr>";
            } else
                tempData += "<td>" + data[d] + "</td></tr>";

        }

        tempData += "<tr><td>product</td><td>" + product + "</td></tr>";

        $("#kyctbody").html(tempData);

    });

}

function getMerchantKYCInformationLog(ctx, id, product, admin) {

    console.log("permission: " + admin);

    $.ajax({
        url: ctx + "/api/merchant/kyc/log",
        type: 'GET',
        data: {
            "id": id,
            "product": "" + product
        }
    }).done(function (response) {

        var tempData = "";

        var rescode = response.responsecode;

        if (rescode !== "00") {

            $("#complainceModal").modal('hide');

            swal('Status', "Record not found", 'error');
            return;
        }

        var data = response.data;

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";


            if (d === "approvedBy") {
                tempData += "<td>Processed By</td>";
            } else
                tempData += "<td>" + d + "</td>";

            if (d === 'parent') {

                if (data[d].businessName !== null)
                    tempData += "<td>" + (data[d].businessName) + "</td></tr>";
                else if (data[d].name !== undefined)
                    tempData += "<td>" + (data[d].name) + "</td></tr>";
                else
                    tempData += "<td></td></tr>";

                continue;
            }

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "createdOn" || d === "approvedOn" || d === "lastProcessed") {
                if (data[d] !== undefined && data[d] !== null)
                    tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";

                continue;
            }

            if (d.toLowerCase().indexOf('path') >= 0) {

                tempData += "<td><a alt='image' href='" + data[d] + "'>" + data[d] + "</a></td></tr>";
            } else
                tempData += "<td>" + data[d] + "</td></tr>";

        }

        tempData += "<tr><td>product</td><td>" + product + "</td></tr>";

        $("#kyctbody").html(tempData);

    });

}

function getProviderInformation(ctx, id) {

    $.ajax({
        url: ctx + "/api/config/provider/unauth",
        type: 'GET',
        data: {
            "id": id
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

    }).fail(function () {

    });
}

function getProviderInformationAuth(ctx, id) {

    $.ajax({
        url: ctx + "/api/config/provider",
        type: 'GET',
        data: {
            "id": id
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

    }).fail(function () {

    });
}

function getFlwTransaction(product, reference) {
    $.ajax({
        url: ctx + "/api/chargeback/transaction",
        type: "POST",
        method: "POST",
        data: JSON.stringify({
            "product": product,
            "reference": reference
        }),
        contentType: "application/json",
        beforeSend: function (xhr) {
            showloader();
            $("#tbody").html("");
        },
        complete: function (jqXHR, textStatus) {
            hideloader();
        }
    }).done(function (response) {

        var tempData = "";

        $("#tbody").html(tempData);

        if (response.responsecode !== "00") {
            $('#modalValues').modal('hide');
            alert("Transaction Record not found on this product");

            return;
        }

        $('#modalValues').modal('show');

        var data = response.data;
        var merchantName = response.merchantName;


        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === 'provider' && (data[d] !== undefined && data[d] !== null)) {
                tempData += "<td>" + (data[d]) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        if ((merchantName !== undefined && merchantName !== null) && "" !== merchantName)
            tempData += "<tr><td>Merchant Name</td><td>" + data[d] + "</td></tr>";


        $("#tbody").html(tempData);

    });
}

function getSettlementInfo(ctx, reference) {
    $.ajax({
        url: ctx + "/api/settlement/find/unauth",
        data: {
            "id": reference
        }
    }).done(function (response) {

        var tempData = "";

        $("#tbody").html(tempData);

        if (response.responsecode !== "00") {
            $('#modalValues').modal('hide');
            alert("Transaction Record not found on this settlement");

            return;
        }

        var data = response.data;

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === 'provider' && (data[d] !== undefined || data[d] !== null)) {
                tempData += "<td>" + (data[d].name) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

    });
}

function getFlutterTransaction(ctx, reference) {

    $.ajax({
        url: ctx + "/api/product/transaction/reference",
        type: 'GET',
        data: {
            "reference": reference
        },
        beforeSend: function (xhr) {
            showloader();
        },
        complete: function (jqXHR, textStatus) {
            hideloader();
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

        $('#modalValues').modal('show');

    }).fail(function () {

    });
}

function getNIPTransaction(ctx, reference ) {

    $.ajax({
        url: ctx + "/api/product/transaction/nip/reference",
        type: 'GET',
        data: {
            "reference": reference
        },
        beforeSend: function (xhr) {
            showloader();
        },
        complete: function (jqXHR, textStatus) {
            hideloader();
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

        $('#modalValues').modal('show');

    }).fail(function () {

    });
}


function getPOSTransaction(ctx, id) {

    $.ajax({
        url: ctx + "/api/pos/transaction/" + id,
        type: 'GET',
        beforeSend: function (xhr) {
            showloader();
        },
        complete: function (jqXHR, textStatus) {
            hideloader();
        }
    }).done(function (response) {


        var tempData = "";

        var status = response.status;

        if (status === 'failed') {

            swal('No Transaction found', '', 'error');
            return;
        }

        var data = response.data;

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

        $('#modalValues').modal('show');

    }).fail(function () {

    });
}

function getFlutterAirtimeTransaction(ctx, reference) {

    $.ajax({
        url: ctx + "/api/product/transaction/airtime/reference",
        type: 'GET',
        data: {
            "reference": reference
        },
        beforeSend: function (xhr) {
            showloader();
        },
        complete: function (jqXHR, textStatus) {
            hideloader();
        }
    }).done(function (data) {


        var tempData = "";

        for (var d in data) {

            if (d === 'id')
                continue;

            tempData += "<tr>";
            tempData += "<td>" + d + "</td>";

            if (d === 'createdBy') {
                tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
                continue;
            }

            if (d === "product") {
                tempData += "<td>" + data[d].name + "</td></tr>";
                continue;
            }

            if (d === "created" || d === "createdOn" || d === "approvedOn") {
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";
                continue;
            }

            tempData += "<td>" + data[d] + "</td></tr>";

        }

        $("#tbody").html(tempData);

        $('#modalValues').modal('show');

    }).fail(function () {

    });
}

function getAllFields(ctx) {

    $.ajax({
        url: ctx + "/api/recon/transaction/allfield",
        type: 'GET'
    }).done(function (data) {

        var temp = "";

        for (var d in data) {

            if (d.toLowerCase().indexOf("date") < -1)
                continue;

            temp += "<option value=" + data[d] + ">" + data[d] + "</option>";
        }

        $("#paramTitle").html(temp);
    });
}

function isGreater(amount, comparer) {

    return parseFloat(amount) > parseFloat(comparer);
}

function dateDiff(startDate, endDate) {

    var date1 = moment(startDate, "DD/MM/YYYY HH:mm:ss").toDate();
    var date2 = moment(endDate, "DD/MM/YYYY HH:mm:ss").toDate();


//    return days;
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    return diffDays;
}

function getDayDifference(range) {

    var dates = range.split("-");

    var diff = dateDiff(dates[0].trim(), dates[1].trim());

    return diff;
}

function downloadFile() {
    var range = $("#dateRange").val();

    console.log(range);

    var diff = getDayDifference(range);

    if (diff > 31) {
        swal('Download status', 'Maximum of 31 day(s) record can be downloaded at once', 'error');
        return;
    }

    console.log(diff);

    if (diff < 8) {
        $("#smtbtn").click();
    } else {

        swal({
            title: '<small>Download status</small>'
            , text: 'Selected duration is more than 7 days, <br/> Do you want to download to you email? '
            , type: 'warning'
            , animation: "slide-from-top"
            , showCancelButton: true
            , cancelButtonText: 'Cancel'
            , closeOnConfirm: false
            , confirmButtonText: 'OK'
            , showLoaderOnConfirm: true
            , html: true
        }, function (isConfirm) {

            if (isConfirm === true || isConfirm === 'true') {

                $.ajax({
                    url: ctx + "/api/pos/transaction/download",
                    type: 'GET',
                    data: {
                        "range": "" + $("#dateRange").val(),
                        "currency": "" + $("#currency").val(),
                        "searchstring": "" + $("#searchValue").val(),
                        "provider": "" + $("#provider").val(),
                        "responsecode": "" + $("#status").val(),
                        "type": "" + $("#type").val(),
                        "merchantid": "" + $("#merchant").val(),
                        "merchantcode": "" + $("#posmerchant").val(),
                        "bankName": "" + $("#bankName").val()
                    }
                }).done(function (data) {

                    var statusCode = data['status_code'];

                    if (statusCode === "00" || statusCode == "00") {
                        swal({
                            title: '<span style="font-size: 80%">Download successful</span>'
                            , text: ''
                            , type: 'success'
                            , showCancelButton: false
                            , confirmButtonText: 'OK'
                            , closeOnConfirm: true
                            , html: true
                        }, function (isConfirm) {
//                                window.location.reload(true);
                        });
                    } else
                        swal('Authorization Failed', '' + data['status'], 'error');
                }).fail(function () {
                    swal('Authorization Failed', '', 'error');
                });
            }
        });
    }

}

function buildTable(data) {

    var tempData = "<table class='table table-striped table-bordered'><tr><th>Name</th><th>Value</th></tr>";
    for (var d in data) {

        if (d === 'id' || d === 'generic' || d === 'secret' || d.toLowerCase() === 'password' || d.toLowerCase() === 'testtoken'
                || d.toLowerCase() === 'testapikey' || d.toLowerCase() === 'liveapikey' || d.toLowerCase() === 'livetoken'
                || d.toLowerCase() === 'apikey')
            continue;

        tempData += "<tr>";
        tempData += "<td>" + d + "</td>";

        if (d === 'createdBy') {
            tempData += "<td>" + (data[d].firstName + ' ' + data[d].lastName) + "</td></tr>";
            continue;
        }

        if (d === "createdAt" || d === "deletedAt" || d === "updatedAt") {
            if (data[d] !== undefined && data[d] !== null)
                tempData += "<td>" + new Date(data[d]).toLocaleString() + "</td></tr>";

            continue;
        }

        tempData += "<td>" + data[d] + "</td></tr>";

    }

    tempData += "</table>";

    return     tempData;
}
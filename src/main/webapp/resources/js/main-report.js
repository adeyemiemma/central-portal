/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$("#sp-bar-total-cost").sparkline([5, 6, 7, 8, 9, 10, 12, 13, 15, 14, 13, 12, 10, 9, 8, 10, 12, 14, 15, 16, 17, 14, 12, 11, 10, 8], {
    type: "bar"
    , width: "100%"
    , height: "30px"
    , barWidth: 2
    , barSpacing: 4
    , barColor: "#FF5722"
});
    
$("#sp-tristate-bar-total-revenue").sparkline([1, 1, 0, 1, -1, -1, 1, -1, 0, 0, 1, 1, 0, -1, 1, -1], {
    type: "tristate"
    , height: "30"
    , posBarColor: "#00BCD4"
    , negBarColor: "#E91E63"
    , barWidth: 4
    , barSpacing: 5
    , zeroAxis: !1
});

$("#sp-stacked-bar-total-sales").sparkline([[8, 10], [12, 8], [9, 14], [8, 11], [10, 13], [7, 11], [8, 14], [9, 12], [10, 11], [12, 14], [8, 12], [9, 12], [9, 14]], {
    type: "bar"
    , width: "100%"
    , height: "30px"
    , barWidth: 4
    , barSpacing: 6
    , stackedBarColor: ["#FF5722", "#009688"]
});
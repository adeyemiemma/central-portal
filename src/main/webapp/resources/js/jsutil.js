/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function amountFormatter(amount) {

    return parseFloat(parseFloat(amount).toFixed(2)).toLocaleString();
}

function getMerchantN(ctx, product) {

    $.ajax({
        url: ctx + "/api/product/report/merchant",
        data: {
            "product": "" + product
        }
    }).done(function (data) {

        $("#merchant").select2({
            data: data,
            // query with pagination
            query: function (q) {
                var pageSize,
                        results;
                pageSize = 20; // or whatever pagesize
                results = [];
                if (q.term && q.term !== "") {
                    // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
                    results = _.filter(this.data, function (e) {
                        if (e.text === null || e.text === undefined)
                            return false;

                        return (e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0);
                    });
                } else if (q.term === "") {
                    results = this.data;
                }
                q.callback({
                    results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                    more: results.length >= q.page * pageSize
                });
            }
        });
    });
}

function getMerchantMID(ctx) {

    $.ajax({
        url: ctx + "/api/product/report/coremerchant"
    }).done(function (data) {

        $("#merchant").select2({
            data: data,
            // query with pagination
            query: function (q) {
                var pageSize,
                        results;
                pageSize = 20; // or whatever pagesize
                results = [];
                if (q.term && q.term !== "") {
                    // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
                    results = _.filter(this.data, function (e) {
                        if (e.text === null || e.text === undefined)
                            return false;

                        return (e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0);
                    });
                } else if (q.term === "") {
                    results = this.data;
                }
                q.callback({
                    results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                    more: results.length >= q.page * pageSize
                });
            }
        });
    });
}

function getPOSMerchantMID(ctx, id, owner, value) {

    $.ajax({
        url: ctx + "/api/pos/merchant",
         data: {
            "owner": "" + owner
        }
    }).done(function (data) {

        console.log(data);
        
        $("#"+id).select2({
            data: data,
            // query with pagination
            query: function (q) {
                var pageSize,
                        results;
                pageSize = 20; // or whatever pagesize
                results = [];
                if (q.term && q.term !== "") {
                    // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
                    results = _.filter(this.data, function (e) {
                        if (e.text === null || e.text === undefined)
                            return false;

                        return (e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0);
                    });
                } else if (q.term === "") {
                    results = this.data;
                }
                q.callback({
                    results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                    more: results.length >= q.page * pageSize
                });
            }
        });
        
        $("#"+id).select2("open");
        
        $("#"+id).on("select2-loaded", function(){
            
            if(value !== undefined){

                $("#"+id).val(value).trigger("change");
            }
        });
        
        
        
    });
}

function getMerchantMIDWithLoader(ctx) {

    $.ajax({
        url: ctx + "/api/product/report/coremerchant",
        beforeSend : function(){
            showloader();
        },
        complete : function() {
            hideloader();
        }
    }).done(function (data) {

        $("#merchant").select2({
            data: data,
            // query with pagination
            query: function (q) {
                var pageSize,
                        results;
                pageSize = 20; // or whatever pagesize
                results = [];
                if (q.term && q.term !== "") {
                    // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
                    results = _.filter(this.data, function (e) {
                        if (e.text === null || e.text === undefined)
                            return false;

                        return (e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0);
                    });
                } else if (q.term === "") {
                    results = this.data;
                }
                q.callback({
                    results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                    more: results.length >= q.page * pageSize
                });
            }
        });
    });
}

function getMerchantMIDWithLoaderPreset(ctx, value) {

    $.ajax({
        url: ctx + "/api/product/report/coremerchant",
        beforeSend : function(){
            showloader();
        },
        complete : function() {
            hideloader();
        }
    }).done(function (data) {

        $("#merchant").select2({
            data: data,
            // query with pagination
            query: function (q) {
                var pageSize,
                        results;
                pageSize = 20; // or whatever pagesize
                results = [];
                if (q.term && q.term !== "") {
                    // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
                    results = _.filter(this.data, function (e) {
                        if (e.text === null || e.text === undefined)
                            return false;

                        return (e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0);
                    });
                } else if (q.term === "") {
                    results = this.data;
                }
                q.callback({
                    results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                    more: results.length >= q.page * pageSize
                });
            }
        });
        
        if(value !== undefined){
            $("#merchant").val(value);
        }
    });
}

function getMerchantMIDPOS(ctx) {

    $.ajax({
        url: ctx + "/api/pos/merchant/all",
        beforeSend : function(){
            showloader();
        },
        complete : function() {
            hideloader();
        }
    }).done(function (data) {

        $("#merchant").select2({
            data: data,
            // query with pagination
            query: function (q) {
                var pageSize,
                        results;
                pageSize = 20; // or whatever pagesize
                results = [];
                if (q.term && q.term !== "") {
                    // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
                    results = _.filter(this.data, function (e) {
                        if (e.text === null || e.text === undefined)
                            return false;

                        return (e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0);
                    });
                } else if (q.term === "") {
                    results = this.data;
                }
                q.callback({
                    results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                    more: results.length >= q.page * pageSize
                });
            }
        });
    });
}

function getMerchantMIDPOSWithID(ctx, id) {

    $.ajax({
        url: ctx + "/api/pos/merchant/all",
        beforeSend : function(){
            showloader();
        },
        complete : function() {
            hideloader();
        }
    }).done(function (data) {

        $("#"+id).select2({
            data: data,
            // query with pagination
            query: function (q) {
                var pageSize,
                        results;
                pageSize = 20; // or whatever pagesize
                results = [];
                if (q.term && q.term !== "") {
                    // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
                    results = _.filter(this.data, function (e) {
                        if (e.text === null || e.text === undefined)
                            return false;

                        return (e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0);
                    });
                } else if (q.term === "") {
                    results = this.data;
                }
                q.callback({
                    results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                    more: results.length >= q.page * pageSize
                });
            }
        });
    });
}

function getMerchants(ctx, product, id) {

    $.ajax({
        url: ctx + "/api/product/report/merchant",
        data: {
            "product": "" + product
        },
        beforeSend: function (xhr) {
            showloader();
        },
        complete: function (jqXHR, textStatus) {
            hideloader();
        }
    }).done(function (data) {

        $("#" + id).select2({
            data: data,
            // query with pagination
            query: function (q) {
                var pageSize,
                        results;
                pageSize = 20; // or whatever pagesize
                results = [];
                if (q.term && q.term !== "") {
                    // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
                    results = _.filter(this.data, function (e) {
                        if (e.text === null || e.text === undefined)
                            return false;

                        return (e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0);
                    });
                } else if (q.term === "") {
                    results = this.data;
                }
                q.callback({
                    results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                    more: results.length >= q.page * pageSize
                });
            }
        });
    });
}

// function drawGraphMerchant(id, title, data){
//     Morris.Area({
//        element: id
//        , data: [{
//            year: "2016-12-01"
//            , AvgSessionDuration: 0
//            , PagesSession: 0
//        }, {
//            year: "2016-12-02"
//            , AvgSessionDuration: 150
//            , PagesSession: 90
//        }, {
//            year: "2016-12-03"
//            , AvgSessionDuration: 140
//            , PagesSession: 120
//        }, {
//            year: "2016-12-04"
//            , AvgSessionDuration: 105
//            , PagesSession: 240
//        }, {
//            year: "2016-12-05"
//            , AvgSessionDuration: 190
//            , PagesSession: 140
//        }, {
//            year: "2016-12-06"
//            , AvgSessionDuration: 230
//            , PagesSession: 250
//        }, {
//            year: "2016-12-07"
//            , AvgSessionDuration: 270
//            , PagesSession: 190
//        }]
//        , xkey: "value"
//        , ykeys: ["Successful", "Failed"]
//        , labels: ["Successful", "Failed"]
//        , behaveLikeLine: !0
//        , ymax: 300
//        , resize: !0
//        , pointSize: 0
//        , pointStrokeColors: ["#42ad2f", "#DA4453"]
//        , smooth: !1
//        , gridLineColor: "#e3e3e3"
//        , numLines: 6
//        , gridtextSize: 14
//        , lineWidth: 0
//        , fillOpacity: .6
//        , hideHover: "auto"
//        , lineColors: ["#C9BBAE", "#F44336"]
//    });
// }

function drawGraphMerchant2(id, label, success, failure) {

    c3.generate({
        bindto: "#" + id
        , size: {
            height: 400
        }
        , color: {
            pattern: ["#489F46", "#D34425", "#FF847C"]
        }
        , data: {
            columns: [success, failure]
            , type: "area"
        }
        , bar: {
            width: {
                ratio: .5
            }
        }
        , grid: {
            y: {
                show: !0
            }
        },
        axis: {
            x: {
                type: 'category',
                categories: label,
                tick: {
                    rotate: 75,
                    multiline: false
                }
            }
        }
    });

}

function drawGraphMerchantSingle(id, label, data) {

    c3.generate({
        bindto: "#" + id
        , size: {
            height: 400
        }
        , color: {
            pattern: ["#489F46", "#D34425", "#FF847C"]
        }
        , data: {
            columns: [data]
            , type: "area"
        }
        , bar: {
            width: {
                ratio: .5
            }
        }
        , grid: {
            y: {
                show: !0
            }
        },
        axis: {
            x: {
                type: 'category',
                categories: label,
                tick: {
                    rotate: 75,
                    multiline: false
                }
            }
        }
    });

}

function amountFormatter2(amount) {

    return parseFloat(parseFloat(amount).toFixed(2)).toLocaleString();
}

function getItem(title, value, volume){
        
    var temp = '<div class="col-md-6 col-lg-3"><div class="card text-xs-center"><div class="card-header"><span class="">'+title+'</span></div><div class="card-body">';
    temp += '<div class="card-block"><ul class="list-inline clearfix"><li class="border-right-grey border-right-lighten-2 pr-2">';
    temp += '<h6 class="grey darken-1 text-bold-400">'+value+'</h6><span class="success small">Value</span></li>';
    temp += '<li class="pl-2"><h6 class="grey darken-1 text-bold-400">'+volume+'</h6><span class="primary small">Volume</span></li></ul></div></div></div></div>';
    
    return temp;
}


function getCurrSymbol(currency){
    
    switch (currency){
        
        case 'USD' :
            return "&#x24;";
        case 'GBP' :
            return '&#xa3;';
        case 'NGN':
            return "&#x20A6;";
        case 'KES':
            return "KSh"; 
        case 'GHS':
            return "&#8373;";
    }
}

function getSummary(){
    $.ajax({
        url: ctx + "/api/chargeback/summary",
        data : {
            "range" : $("#dateRange").val(),
            "currency" : $("#currency").val()
        },
        beforeSend : function(){
            currency = $("#currency").val();
        }
    }).done(function( data ){

        $("#merchantcb").text(data.total.merchantcount === null ? 0 : data.total.merchantcount );

        var totalAmount = data.total.amount[0];

        if(totalAmount.length !== 0){
            
            
            $("#totalcb").html(getCurrSymbol(currency)+" "+((totalAmount[0] === null || totalAmount[0] === 'null') ?  0 : totalAmount[0].toLocaleString()));
            $("#processedcb").html(getCurrSymbol(currency)+" "+((totalAmount[1] === null || totalAmount[1] === 'null') ? 0:  totalAmount[1].toLocaleString()));
        }else {
            $("#totalcb").html(getCurrSymbol(currency)+" 0");
            $("#processedcb").html(getCurrSymbol(currency)+" 0");

        }

        totalAmount = data.monthly.amount[0];

        if(totalAmount.length !== 0){

            $("#monthcb").html(getCurrSymbol(currency)+" "+((totalAmount[0] === null || totalAmount[0] === 'null') ?  0 : totalAmount[0].toLocaleString()));
        }else
            $("#monthcb").html(getCurrSymbol(currency)+" 0");


        $("#monthname").html(data.month.toUpperCase());


    }).fail(function(err){

    });
}

function getBankChargebackSummary(){
    $.ajax({
        url: ctx + "/api/chargeback/bank/summary",
        data : {
            "range" : $("#dateRange").val(),
            "currency" : $("#currency").val()
        },
        beforeSend : function(){
            currency = $("#currency").val();
        }
    }).done(function( data ){

        $("#merchantcb").text(data.total.merchantcount === null ? 0 : data.total.merchantcount );

        var totalAmount = data.total.amount[0];

        if(totalAmount.length !== 0){
            
            
            $("#totalcb").html(getCurrSymbol(currency)+" "+((totalAmount[0] === null || totalAmount[0] === 'null') ?  0 : totalAmount[0].toLocaleString()));
            $("#processedcb").html(getCurrSymbol(currency)+" "+((totalAmount[1] === null || totalAmount[1] === 'null') ? 0:  totalAmount[1].toLocaleString()));
        }else {
            $("#totalcb").html(getCurrSymbol(currency)+" 0");
            $("#processedcb").html(getCurrSymbol(currency)+" 0");

        }

        totalAmount = data.monthly.amount[0];

        if(totalAmount.length !== 0){

            $("#monthcb").html(getCurrSymbol(currency)+" "+((totalAmount[0] === null || totalAmount[0] === 'null') ?  0 : totalAmount[0].toLocaleString()));
        }else
            $("#monthcb").html(getCurrSymbol(currency)+" 0");


        $("#monthname").html(data.month.toUpperCase());


    }).fail(function(err){

    });
}

function getMerchantChargeBack(merchantId, dateRange, currency ){
    
    $.ajax({
        url: ctx + "/api/chargeback/summary",
        data : {
            "range" : dateRange,
            "currency" : currency,
            "merchantId" : merchantId
        },
        beforeSend : function(){
            currency = $("#currency").val();
        }
    }).done(function( data ){

        $("#merchantcb").text(data.total.merchantcount === null ? 0 : data.total.merchantcount );

        var totalAmount = data.total.amount[0];

        if(totalAmount.length !== 0){
            $("#totalcb").html(getCurrSymbol(currency)+" "+((totalAmount[0] === null || totalAmount[0] === 'null') ?  0 : totalAmount[0].toLocaleString()));
        }else {
            $("#totalcb").html(getCurrSymbol(currency)+" 0");

        }

    }).fail(function(err){

    });
}

function getMerchantRefund(merchantId, dateRange, currency ){
    
    $.ajax({
        url: ctx + "/api/product/transaction/refund/summary",
        data : {
            "range" : dateRange,
            "currency" : currency,
            "merchantId" : merchantId
        },
        beforeSend : function(){
            currency = $("#currency").val();
        }
    }).done(function( data ){

        var totalAmount = data.total.amount[0];

        var totalAmount = data.total.amount;

        if(totalAmount !== null && totalAmount !== 'null'){
            $("#totalrf").html(getCurrSymbol(currency)+" "+ totalAmount.toLocaleString());
        }else
            $("#totalrf").html(getCurrSymbol(currency)+" 0");

    }).fail(function(err){

    });
}

function getRefundSummary(){
    
    $.ajax({
        url: ctx + "/api/product/transaction/refund/summary",
        data : {
            "range" : $("#dateRange").val(),
            "currency" : $("#currency").val()
        },
        beforeSend : function(){
            currency = $("#currency").val();
        }
    }).done(function( data ){


        $("#merchantrf").text(data.total.merchant === null ? 0 : data.total.merchant );

        var totalAmount = data.total.amount;

        if(totalAmount !== null && totalAmount !== 'null'){
            $("#totalrf").html(getCurrSymbol(currency)+" "+ totalAmount.toLocaleString());
        }else
            $("#totalrf").html(getCurrSymbol(currency)+" 0");

        totalAmount = data.monthly.amount;

        if(totalAmount !== null && totalAmount !== 'null'){
            $("#monthrf").html(getCurrSymbol(currency)+" "+ totalAmount.toLocaleString());
        }else
            $("#monthrf").html(getCurrSymbol(currency)+" 0");
        
        totalAmount = data.today.amount;

        if(totalAmount !== null && totalAmount !== 'null'){
            $("#todayrf").html(getCurrSymbol(currency)+" "+ totalAmount.toLocaleString());
        }else
            $("#todayrf").html(getCurrSymbol(currency)+" 0");


        $("#monthname").html(data.month.toUpperCase());


    }).fail(function(err){
        console.log(err);
    });
}

function getCurrencySymbol(currency){
    
    switch (currency){
        
        case 'USD' :
            return "&#x24;";
        case 'GBP' :
            return '&#xa3;';
        case 'NGN':
            return "&#x20A6;";
        case 'KES':
            return "KSh"; 
        case 'GHS':
            return "&#8373;";
        default:
            return currency;
    }
}


function getGraphMerchantData(ctx, date, curr, product, merchant){
    
    $.ajax({
       url : ctx+"/api/product/transaction/dashboard/graph" ,
       data : {
           "range" : date,
           "currency": curr,
           "product" : product,
           "merchant" : merchant
       }
    }).done(function(data){
        
        var currency = getCurrencySymbol(curr);
        
        console.log(currency);
        
        $("#totalvolume").text(data.totalVolume);
        $("#totalsuccess").html(currency+""+amountFormatter(data.totalSuccessful));
        $("#totalvalue").html(currency+" "+amountFormatter(data.totalSum));
        $("#totalfee").html(currency+" "+amountFormatter(data.totalFee));
        
        var labels = [];
        var successful = [];
        var failed = [];
        
        for(var d in data.dateData){
                
            labels.push(moment(d).format("DD-MM-YYYY"));
            successful.push(parseFloat(data.dateData[d].successful).toFixed(2));
            failed.push(parseFloat(data.dateData[d].failed).toFixed(2));
        }
        
        if(product.toLowerCase() === 'flutterwave core' || product.toLowerCase() === 'core'){
            $("#summarypanepie").hide();
            $("#summarypaneline").show();
            
//            $("#trend-pane").removeClass('col-xl-8').addClass('col-xl-12');
            
//            drawPie2(dataPieMain, $("#product").val());
            
        }
        else{
            $("#summarypanepie").show();
            $("#summarypaneline").hide();
            
//            $("#trend-pane").removeClass('col-xl-12').addClass('col-xl-8');
            
        }
        
        $("#chartjsgraph").append('<canvas id="d-graph" height="500" width="1500" style="display: block; height: 250px;"></canvas>');
        
        drawGraphMerchant(labels, successful, failed);
        
        var dataPieMain = [];
        var dataPie ;
//        dataPieMain.push(dataPie);
        
        var lineY = ["Values"];
        var lineX = [];
        
        for(var d in data.statusData){
            
            lineX.push(d);
            lineY.push(parseFloat(data.statusData[d]).toFixed(2));
            
            dataPie = [d , parseFloat(data.statusData[d]).toFixed(2)];
            dataPieMain.push(dataPie);
        }
        
    });
}

function getGraphProviderData(ctx, date, curr, provider){
    
    $.ajax({
       url : ctx+"/api/product/transaction/provider/graph" ,
       data : {
           "range" : date,
           "currency": curr,
           "provider" : provider
       }
    }).done(function(data){
        
        var currency = getCurrencySymbol(curr);
        
//        $("#totalvolume").text(data.totalVolume);
//        $("#totalsuccess").html(currency+""+amountFormatter(data.totalSuccessful));
//        $("#totalvalue").html(currency+" "+amountFormatter(data.totalSum));
//        $("#totalfee").html(currency+" "+amountFormatter(data.totalFee));
        
        var labels = [];
        var successful = [];
        var failed = [];
        
        for(var d in data.dateData){
                
            labels.push(moment(d).format("DD-MM-YYYY"));
            successful.push(parseFloat(data.dateData[d].successful).toFixed(2));
            failed.push(parseFloat(data.dateData[d].failed).toFixed(2));
        }
        
        $("#chartjsgraph").append('<canvas id="d-graph" height="500" width="1500" style="display: block; height: 250px;"></canvas>');
        
        drawGraphMerchant(labels, successful, failed);
        
//        var dataPieMain = [];
//        var dataPie ;
////        dataPieMain.push(dataPie);
//        
//        var lineY = ["Values"];
//        var lineX = [];
        
    });
}

function drawGraph2(id, label, success, failure) {

    c3.generate({
        bindto: "#" + id
        , size: {
            height: 400
        }
        , color: {
            pattern: ["#489F46", "#D34425", "#FF847C"]
        }
        , data: {
            columns: [success, failure]
            , type: "area"
        }
        , bar: {
            width: {
                ratio: .5
            }
        }
        , grid: {
            y: {
                show: !0
            }
        },
        axis: {
            x: {
                type: 'category',
                categories: label,
                tick: {
                    rotate: 75,
                    multiline: false
                }
            }
        }
    });

}

function drawGraphMerchant(label, success, failure){
 
    var f = $("#d-graph");
    
    var g = {
            responsive: !0
            , maintainAspectRatio: !1
            , pointDotStrokeWidth: 4
            , legend: {
                display: !1
                , labels: {
                    fontColor: "#FFF"
                    , boxWidth: 10
                }
                , position: "top"
            }
            , hover: {
                mode: "label"
            }
            , scales: {
                xAxes: [{
                    display: !1,
                }]
                , yAxes: [{
                    display: !0
                    , gridLines: {
                        color: "rgba(216,216,216, 0.5)"
                        , drawTicks: 1
                        , drawBorder: 1
                    }
                    , ticks: {
                        display: 1
                        , min: 0
//                        , max: 70
//                        , maxTicksLimit: 4
                    }
                }]
            }
            , title: {
                display: !1
                , text: "Chart.js Line Chart - Legend"
            }
        }
        , h = {
            labels: label
            , datasets: [{
                label: "Failed"
                , data: failure
                , backgroundColor: "transparent"
                , borderColor: "#FF6E40"
                , pointColor: "#fff"
                , pointBorderColor: "#FF6E40"
                , pointBackgroundColor: "#fff"
                , pointBorderWidth: 2
                , pointHoverBorderWidth: 2
                , pointRadius: 3
            }, {
                label: "Success"
                , data: success
                , backgroundColor: "transparent"
                , borderColor: "#1DE9B6"
                , pointColor: "#fff"
                , pointBorderColor: "#1DE9B6"
                , pointBackgroundColor: "#fff"
                , pointBorderWidth: 2
                , pointHoverBorderWidth: 2
                , pointRadius: 3
            }]
        }
        , i = {
            type: "line"
            , options: g
            , data: h
        }
        , j = new Chart(f, i);
        
        currentData = i;
    
}

function drawGraphSingle(id, label, data) {

    c3.generate({
        bindto: "#" + id
        , size: {
            height: 400
        }
        , color: {
            pattern: ["#489F46", "#D34425", "#FF847C"]
        }
        , data: {
            columns: [data]
            , type: "area"
        }
        , bar: {
            width: {
                ratio: .5
            }
        }
        , grid: {
            y: {
                show: !0
            }
        },
        axis: {
            x: {
                type: 'category',
                categories: label,
                tick: {
                    rotate: 75,
                    multiline: false
                }
            }
        }
    });

}

function buildCard(value, title){
        
    var temp = '<div class="card"><div class="card-body"><div class="card-block"><div class="media"><div class="media-body text-xs-left"><h3 class="success">'+value+'</h3><span>'+title+'</span></div><div class="media-right media-middle"><i class="icon-bulb success font-large-2 float-xs-right"></i></div></div><progress class="progress progress-sm progress-success mt-1 mb-0" value="'+value+'" max="100"></progress></div></div></div>';
    
    return temp;
}
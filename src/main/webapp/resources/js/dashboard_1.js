/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function drawGraph(data){
    
    var originData = data.origin.result;
                   
    var destinationData = data.destination.result;

    var originDataArray = [];
    var originDataArrayC = [];
                   
    for(var i = 0; i < originData.length ; i++ ){
        originDataArrayC.push(originData[i][0]);
        originDataArray.push(originData[i][1]);
    }

    var destDataArray = [];
    var destDataArrayC = [];

    for(var i = 0; i < destinationData.length; i++ ){
        destDataArrayC.push(originData[i][0]);
        destDataArray.push(originData[i][1]);
    }

    //setPie("currencydestgraph",destDataArrayC,destDataArray);
    //setBar("currencyoriggraph",originDataArrayC,originDataArray);
}

function drawChart(data){
   
   var originDataArray = [];
    var originDataArrayC = [];
                   
    for(var d in data ){
        originDataArrayC.push(d);
        originDataArray.push(data[d]);
    }
    
    //setLine("currencydestgraph",originDataArrayC,originDataArray);
}

function getDashboardSummary(context, currency, dateRange){
    
    $.ajax({
        url : context+"/api/recon/transaction/dsummary",
        data : {
            "currency" : currency,
            "range" : dateRange
        }
    }).done(function(data){
        
        $("#txnvolume").text(parseFloat(data.transactionVolume).toLocaleString());
        $("#txnmerchant").text(parseFloat(data.merchant).toLocaleString());
//        $("#txncard").text(parseFloat(data.cards).toLocaleString());
        $("#txnvalue").html(getCurrencySymbol(currency)+" "+parseFloat(data.transactionValue).toLocaleString());
        $("#txnsettled").html(getCurrencySymbol(currency)+" "+parseFloat(data.transactionSettledValue).toLocaleString());
    });
    
}

function getCurrencySymbol(currency){
    
    switch (currency){
        
        case 'USD' :
            return "&#x24;";
        case 'GBP' :
            return '&#xa3;';
        case 'NGN':
            return "&#x20A6;";
    }
}
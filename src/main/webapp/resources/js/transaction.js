/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function(){
    
    $("#daterange").daterangepicker({
        timePicker: 0
        , timePickerIncrement: 5
        , timePicker24Hour: !0
        , timePickerSeconds: !0
        , startDate: moment().subtract('days', 10)
        , maxDate : moment()
        , locale: {
            format: "DD/MM/YYYY"
        }
    });    
    
//    $('#daterange').val(moment().subtract('days', 29).format('DD/MM/YYYY') + ' - ' + moment().format('DD/MM/YYYY'));
    
    $("#datepicker").pickadate({
        format: "dd/mm/yyyy"
    });
});

function reloadDate(id){
        
    $("#"+id).daterangepicker({
        timePicker: 0
        , timePickerIncrement: 5
        , timePicker24Hour: !0
        , timePickerSeconds: !0
        , startDate: moment().subtract('days', 10)
        , maxDate : moment()
        , locale: {
            format: "DD/MM/YYYY"
        }
    }); 
}

function doSettlement(ctx,myList ){
                    
    if( myList.length <= 0){
        alert("Please select at lease one item");
        return ;
    }


    $.ajax({
        url : ctx+"/api/settlement/create",
        type: "POST",
        contentType : "application/json",
        data : JSON.stringify({
            "currency" : $("#currency").val(),
            "id" : myList,
            "range" : $("#dateRange").val(),
            "operation" : "",
            "rejectionMessage" : ""
        }),
        beforeSend : function(){
            showloader();
        },
        complete: function (jqXHR, textStatus) {
            hideloader();
        }
    }).done(function(data){
        
        if(data['status-code'] === "00"){
            
            swal({
                title: '<span style="font-size: 80%">Settlement Added successfully</span>'
                , text: ''
                , type: 'success'
                , showCancelButton: false
                , confirmButtonText: 'OK'
                , closeOnConfirm: true
                , html: true
            }, function (isConfirm) {
                window.location.reload(true);
            });
        }else
            swal('Settlement Failed', '', 'error');
    });
}


function processResponse(data){
    
     var error = "\"tag tag-default tag-danger\"";
     var success = "\"tag tag-default tag-success\"";
    
    var index = $("#product").val().toLowerCase() === 'flutterwave core';
    
    if((data.toLowerCase() === "successful" || data.toLowerCase() === "completed") && index === false )
        return "<span class="+success+">"+data+"</span>" ;

    if((data.toLowerCase().indexOf("success") > -1 || data.toLowerCase().indexOf("approved") > -1 )  && index === true )
        return "<span class="+success+">"+data+"</span>" ;

    return "<span class="+error+">"+data+"</span>" ;
    
}


function processResponseWithProduct(data, product){
    
     var error = "\"tag tag-default tag-danger\"";
     var success = "\"tag tag-default tag-success\"";
    
    var index = product.toLowerCase() === 'flutterwave core' || product.toLowerCase() === 'core';
    
    if((data.toLowerCase() === "successful" || data.toLowerCase() === "completed") && index === false )
        return "<span class="+success+">"+data+"</span>" ;

    if((data.toLowerCase().indexOf("success") > -1 || data.toLowerCase().indexOf("approved") > -1 )  && index === true )
        return "<span class="+success+">"+data+"</span>" ;

    return "<span class="+error+">"+data+"</span>" ;
    
}

function getTransactionInformation(ctx, product, id, reference){
                    
    $.ajax({
        url: ctx + "/api/product/transaction/info",
        type: 'GET',
        data : {
            "id" : id,
            "product" : ""+product,
            "reference" : ""+reference
        }
    }).done(function( data ){


        var tempData = "";

        for(var d in data){

            if(d === 'id' ||d === 'generic' || d === 'secret' || d.toLowerCase() === 'password' || d.toLowerCase() === 'testtoken'
                    || d.toLowerCase() === 'testapikey' || d.toLowerCase() === 'liveapikey'|| d.toLowerCase() === 'livetoken' 
                    || d.toLowerCase() === 'apikey')
                continue;

            tempData += "<tr>" ;
            tempData += "<td>"+d+"</td>";

            if( d === 'parent'){
                
                if(data[d].businessName !== null)
                    tempData += "<td>"+(data[d].businessName)+"</td></tr>";
                else if( data[d].name !== undefined)
                    tempData += "<td>"+(data[d].name)+"</td></tr>";
                else
                    tempData += "<td></td></tr>";
                
                continue;
            }
            
            if( d === 'createdBy'){
                tempData += "<td>"+(data[d].firstName+' '+ data[d].lastName)+"</td></tr>";
                continue;
            }
            
            if( d === "createdAt" || d === "deletedAt" || d === "updatedAt" || d === "transactionDate"){
                if(data[d] !== undefined && data[d] !== null)
                    tempData += "<td>"+new Date(data[d]).toLocaleString()+"</td></tr>";
                
                continue;
            }
            
            tempData += "<td>"+data[d]+"</td></tr>";

        }
        
        tempData += "<tr><td>product</td><td>"+product+"</td></tr>";

        $("#tbody").html(tempData);
        
        $('#modalValues').modal('show');

    }).fail(function(){

    });
}